import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SitesRoutingModule } from './sites-routing.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SitesRoutingModule, TabsModule.forRoot()],
  declarations: [],
  providers: [],
  exports: [],
})
export class SitesModule {}
