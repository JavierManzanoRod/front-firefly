import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig } from 'src/app/modules/generic-list/models/generic-list.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { SubsiteService } from '../../subsite/services/subsite.service';
import { Site } from '../models/sites.model';
import { SiteService } from '../services/site.service';

@Component({
  selector: 'ff-site-list-container',
  template: `<ff-page-header pageTitle="{{ 'SITES.TITLE' | translate }}">
      <a [routerLink]="['/sites/site/new']" class="btn btn-primary">
        {{ 'SITES.CREATE' | translate }}
      </a>
    </ff-page-header>
    <ngx-simplebar class="page-container">
      <div class="page-scroll-wrapper">
        <ngx-simplebar class="page-scroll">
          <div class="page-container-padding">
            <ff-generic-list
              [list]="list$ | async"
              [loading]="loading"
              [page]="page"
              [showPagination]="false"
              [currentData]="currentData"
              (delete)="delete($event)"
              [arrayKeys]="arrayKeys"
              [route]="link"
            >
            </ff-generic-list>
          </div>
        </ngx-simplebar>
      </div>
    </ngx-simplebar> `,
})
export class SiteListContainerComponent extends BaseListContainerComponent<Site> implements OnInit {
  arrayKeys: GenericListConfig[] = [
    {
      key: 'id',
      headerName: 'SITES.ID',
    },
    {
      key: 'name',
      headerName: 'SITES.NAME',
    },
    {
      key: 'locale_default',
      headerName: 'SITES.DEFAULT_LANGUAGE',
      language: true,
      textCenter: true,
    },
    {
      key: 'total_subsites',
      headerName: 'SITES.SUBSITES',
      textCenter: true,
    },
  ];
  link = '/sites/site/edit';

  constructor(
    public apiService: SiteService,
    public utils: CrudOperationsService,
    public toast: ToastService,
    private subsiteService: SubsiteService
  ) {
    super(utils, apiService);
  }

  delete(item: Site) {
    this.subsiteService.list(item).subscribe((response) => {
      const isSubsiteActive = !!response.content.find((subsite) => subsite.active);
      if (isSubsiteActive) {
        this.toast.error('SITES.ERROR_SUBSITE_ACTIVE');
      } else {
        super.delete(item);
      }
    });
  }

  ngOnInit() {
    this.getDataList({ size: 100 });
  }
}
