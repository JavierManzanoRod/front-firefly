import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { UpdateResponse } from '@core/base/api.service';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { TranslateModule } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { BOSS405, BOSS407, MDL109 } from '../../utils/constError';
import { SiteService } from '../services/site.service';
import { BuildSiteService } from './../services/build-site.service';
import { SiteViewContainerComponent } from './site-view.container.component';

describe('SiteViewContainerComponent', () => {
  let component: SiteViewContainerComponent;
  let fixture: ComponentFixture<SiteViewContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SiteViewContainerComponent],
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: ToastService,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123, tab: 1 }),
          },
        },
        { provide: BsModalService, useFactory: () => ({}) },
        {
          provide: BuildSiteService,
          useFactory: () => ({
            buildSite: () => {},
          }),
        },
        {
          provide: CrudOperationsService,
          useFactory: () => ({
            updateOrSaveModal: () => {},
          }),
        },
        {
          provide: SiteService,
          useFactory: () => ({
            list: () => {
              return of({});
            },
            detail: () => {
              return of({});
            },
          }),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(SiteViewContainerComponent);
    component = fixture.componentInstance;
  });

  it('should create the component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('title create', () => {
    expect(component.getTitle('')).toContain('CREATE');
  });

  it('title edit', () => {
    expect(component.getTitle('1')).toContain('EDIT');
  });

  it('load header form', () => {
    component.mainForm = new FormGroup({});
    component.formHeaderLoaded(new FormGroup({}));
    expect(component.mainForm.controls.header).toBeTruthy();
  });

  it('load config form', () => {
    component.mainForm = new FormGroup({});
    component.formConfigLoaded(new FormGroup({}));
    expect(component.mainForm.controls.config).toBeTruthy();
  });

  it('submit succes redirect', () => {
    component.mainForm = new FormGroup({
      included_products: new FormGroup({}),
    });
    component.siteId = '';
    const router = fixture.debugElement.injector.get(Router);
    const spy = spyOn(router, 'navigate');
    const update = fixture.debugElement.injector.get(CrudOperationsService);
    const spyUpdate = spyOn(update, 'updateOrSaveModal').and.returnValue(
      of({
        status: 200,
      } as unknown as UpdateResponse<any>)
    );

    component.submit();
    expect(spy).toHaveBeenCalled();
    expect(spyUpdate).toHaveBeenCalled();
  });

  it('submit error toast', () => {
    component.mainForm = new FormGroup({
      included_products: new FormGroup({}),
      config: new FormGroup({}),
    });
    component.siteId = '';
    const toast = fixture.debugElement.injector.get<ToastService>(ToastService);
    const spy = spyOn(toast, 'error');
    const update = fixture.debugElement.injector.get(CrudOperationsService);
    const spyUpdate = spyOn(update, 'updateOrSaveModal').and.returnValue(
      of({
        status: 500,
        error: [BOSS407, BOSS405, MDL109],
      } as unknown as UpdateResponse<any>)
    );

    component.submit();
    expect(spy).toHaveBeenCalled();
    expect(spyUpdate).toHaveBeenCalled();
  });
  it('form no valid error toast', () => {
    component.mainForm = new FormGroup({
      included_products: new FormGroup({}),
      config: new FormGroup({}),
      test: new FormControl(null, Validators.required),
    });

    const toast = fixture.debugElement.injector.get(ToastService);
    const spy = spyOn(toast, 'error');

    component.submit();
    expect(spy).toHaveBeenCalled();
  });

  it('cancel and redirect', () => {
    const router = fixture.debugElement.injector.get(Router);
    const spy = spyOn(router, 'navigate');
    component.cancel();
    expect(spy).toHaveBeenCalled();
  });
});
