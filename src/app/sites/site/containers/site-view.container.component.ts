import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CanComponentDeactivate } from '@core/guards/can-deactivate-guard';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError, delay, switchMap, tap } from 'rxjs/operators';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { CrudOperationsService, groupType } from '../../../core/services/crud-operations.service';
import { FormUtilsService } from '../../../shared/services/form-utils.service';
import { BOSS405, BOSS407, localeDefault, MDL107, MDL109 } from '../../utils/constError';
import { SiteTabsComponent } from '../components/site-tabs/site-tabs.component';
import { SiteViewHeaderComponent } from '../components/site-view-header/site-view-header.component';
import { Site } from '../models/sites.model';
import { BuildSiteService } from '../services/build-site.service';
import { SitePreviewService } from '../services/site-preview.service';
import { SiteService } from '../services/site.service';

@Component({
  selector: 'ff-site-view-container',
  template: `<ff-page-header
      *ngIf="!loading"
      [pageTitle]="title | translate"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'COMMON.RETURN_TO_LIST'"
    >
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-site-view-header [item]="data" [loading]="loading" #detailHeaderElement (formLoaded)="formHeaderLoaded($event)">
      </ff-site-view-header>
      <ff-site-tabs
        [item]="data"
        [loading]="loading"
        (formLoaded)="formConfigLoaded($event)"
        [updateFormChanges]="subjectSave"
        [activeSubSite]="tab"
        [create]="create"
        #detailTabElement
      ></ff-site-tabs>
      <div class="page-footer text-right" *ngIf="!loading">
        <button type="button" class="btn btn-outline-primary btn-sm" (click)="cancel()">
          {{ 'COMMON.CANCEL' | translate }}
        </button>
        <button type="button" *ngIf="!hidePreviewButton" class="btn btn-primary ml-2 btn-lg" (click)="preview()" [disabled]="!hasPreview">
          {{ 'COMMON.PREVIEW' | translate }}
        </button>
        <button type="button" class="btn btn-primary ml-2 btn-lg" (click)="submit()">
          {{ 'COMMON.SAVE' | translate }}
        </button>
      </div>
    </ngx-simplebar> `,
})
export class SiteViewContainerComponent implements OnInit, CanComponentDeactivate {
  @ViewChild('detailTabElement', { static: false }) detailTabElement!: SiteTabsComponent;
  @ViewChild('detailHeaderElement', { static: false }) detailHeaderElement!: SiteViewHeaderComponent;

  urlListado = '/sites/site';
  data$!: Observable<Site>;
  site!: Site;
  loading = true;
  siteId!: string;
  title!: string;
  mainForm!: FormGroup;
  subjectSave = new Subject<any>();
  tab!: string;
  create!: boolean;
  data!: Site;
  hasPreview = false;
  hidePreviewButton = false;

  constructor(
    public route: ActivatedRoute,
    public siteService: SiteService,
    public buildSiteService: BuildSiteService,
    public router: Router,
    public toast: ToastService,
    public utils: CrudOperationsService,
    private sitePreviewSrv: SitePreviewService
  ) {}

  ngOnInit(): void {
    this.mainForm = new FormGroup({});
    this.getData();
    this.sitePreviewSrv.hasPreview.subscribe((res) => (this.hasPreview = res));
    this.sitePreviewSrv.hidePreviewButton.subscribe((res) => (this.hidePreviewButton = res));
  }

  getData() {
    this.route.params
      .pipe(
        switchMap(({ id, tab }: Params) => {
          this.title = this.getTitle(id);
          this.siteId = id;
          this.tab = tab;

          if (id) {
            return this.siteService.detail(id);
          } else {
            return of({} as Site).pipe(delay(250));
          }
        }),
        tap(() => {
          this.loading = false;
        })
      )
      .subscribe((data: Site) => (this.data = data));
  }

  formHeaderLoaded(event: FormGroup) {
    this.mainForm.addControl('header', event);
    this.create = !this.siteId;
  }

  formConfigLoaded(event: FormGroup) {
    this.mainForm.addControl('config', event);
  }

  getTitle(id: string): string {
    return !id ? 'SITES.CREATE' : 'SITES.EDIT';
  }

  submit() {
    FormUtilsService.markFormGroupTouched(this.mainForm);
    if (this.mainForm.valid) {
      const siteToSave: Site = this.buildSiteService.buildSite(this.mainForm, this.siteId);

      const config: any = this.mainForm.controls.config;
      const value: any = this.mainForm.value.config;
      const showWarning =
        (!config?.controls?.end_time?.pristine ||
          !config?.controls?.start_time?.pristine ||
          !config?.controls?.notification_url?.pristine) &&
        value?.end_time != null;

      this.utils
        .updateOrSaveModal(
          {
            apiService: this.siteService,
            methodToApply: this.siteId ? 'PUT' : 'POST',
          },
          siteToSave,
          groupType.SITE_SUBSITE,
          'COMMON_ERRORS.SAVE_ERROR',
          showWarning
        )
        .pipe(
          catchError((e: HttpErrorResponse) => {
            if (e.status === 409) {
              this.detailHeaderElement.form.controls.id.setErrors({ error_409: true });
            }
            return throwError(e);
          })
        )
        .subscribe((result) => {
          this.mainForm.markAsPristine();
          this.sitePreviewSrv.refreshTree();
          if ((result.status === 201 || result.status === 200) && !this.siteId) {
            this.router.navigate([`${this.urlListado}/edit/${result.data?.id}`]);
          }
          if ((result.status === 201 || result.status === 200) && !result.error) {
            this.subjectSave.next(null);
          }
          this.mainForm.markAsPristine();

          if (result.error) {
            // Cargar el form con el error
            for (const iterator of result.error) {
              const field = this.mainForm.controls.config.get(iterator);
              if (field) {
                field.setErrors({
                  required: true,
                });
              }
            }
            let errorMessage = 'SITES.FORM_ERROR.MESSAGE';
            if ((result.error as string).indexOf(BOSS407) >= 0) {
              errorMessage = 'SITES.BOSS-407';
            }
            if ((result.error as string).indexOf(BOSS405) >= 0) {
              errorMessage = 'SITES.BOSS-405';
            }
            if ((result.error as string).indexOf(MDL107) >= 0 || (result.error as string).indexOf(MDL109) >= 0) {
              errorMessage = 'SITES.MDL-107';
            }
            if ((result.error as string[])[0].indexOf(localeDefault) >= 0) {
              errorMessage = 'SITES.LOCALE_DEFAULT_ERROR';
            }
            this.toast.error(errorMessage, 'SITES.FORM_ERROR.TITLE');
          }
        });
    } else {
      this.toast.error('SITES.FORM_ERROR.MESSAGE', 'SITES.FORM_ERROR.TITLE');
    }
  }

  preview() {
    this.sitePreviewSrv.launchModalPreview();
  }

  cancel() {
    this.router.navigate([this.urlListado]);
  }

  canDeactivate = () => {
    if (this.detailTabElement.isDirty) {
      return this.detailTabElement.isDirty;
    }
    if (this.detailHeaderElement.isDirty) {
      return this.detailHeaderElement.isDirty;
    }
    return false;
  };
}
