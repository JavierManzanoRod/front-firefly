import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { SubsiteService } from '../../subsite/services/subsite.service';
import { SiteService } from '../services/site.service';
import { SiteListContainerComponent } from './site-list-container.component';

describe('SiteListContainerComponent', () => {
  let fixture: ComponentFixture<SiteListContainerComponent>;
  let component: SiteListContainerComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SiteListContainerComponent],
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: ToastService,
          useFactory: () => ({
            error: () => {},
          }),
        },
        { provide: ToastrService, useFactory: () => ({}) },
        { provide: BsModalService, useFactory: () => ({}) },
        {
          provide: SiteService,
          useFactory: () => ({
            list: () => {
              return of({});
            },
          }),
        },
        {
          provide: SubsiteService,
          useFactory: () => ({
            list: () => {
              return of({});
            },
          }),
        },
        { provide: CrudOperationsService, useFactory: () => ({}) },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(SiteListContainerComponent);
    component = fixture.componentInstance;
  });

  it('should create the component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });
});
