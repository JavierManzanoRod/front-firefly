import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SiteViewHeaderComponent } from './site-view-header.component';

describe('SiteViewHeader', () => {
  let component: SiteViewHeaderComponent;
  let fixture: ComponentFixture<SiteViewHeaderComponent>;

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteViewHeaderComponent);
    component = fixture.componentInstance;
  });

  it('init', () => {
    expect(fixture).toBeTruthy();
  });

  it('emit event form loaded', () => {
    const s = spyOn(component.formLoaded, 'emit');
    component.ngOnInit();
    expect(s).toHaveBeenCalled();
  });
});
