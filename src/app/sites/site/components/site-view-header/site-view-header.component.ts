import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { FormItemConfig, FormUtilsService } from '../../../../shared/services/form-utils.service';
import { Site } from '../../models/sites.model';

@Component({
  selector: 'ff-site-view-header',
  templateUrl: 'site-view-header.component.html',
  styleUrls: ['./site-view-header.component.scss'],
})
export class SiteViewHeaderComponent implements OnInit, OnChanges {
  @Input() item!: Site;
  @Input() loading!: boolean;
  @Input() readonly = false;
  @Output() formLoaded: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  isCollapsedDefinition = false;
  formConfig!: { [key: string]: FormItemConfig };
  formErrors: any = {};
  form!: FormGroup;

  constructor() {
    this.formConfig = {
      id: {
        value: null,
        validators: {
          required: true,
        },
      },
      name: {
        value: null,
        validators: {
          required: true,
          maxlength: 40,
        },
      },
    };

    const config = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig(this.formConfig);

    this.form = config.form;
    this.formErrors = config.errors;
  }

  ngOnInit() {
    this.formLoaded.emit(this.form);

    if (this.readonly) {
      this.form.disable();
    }
  }

  ngOnChanges(event: SimpleChanges) {
    if (event.item && this.item && this.item.id) {
      this.form.controls.id.setValue(this.item.id);
      // this.id.disable();
      this.form.controls.id.updateValueAndValidity({ emitEvent: false });
      this.form.patchValue({
        ...this.item,
      });
      // this.commonSiteSubSite.markFormPristine(this.form);
      this.form.markAsPristine();
    }
  }

  get isDirty(): false | UICommonModalConfig {
    if (this.form.pristine) {
      return false;
    }
    if (this.item?.id) {
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.item?.name,
      };
    } else {
      return {};
    }
  }
}
