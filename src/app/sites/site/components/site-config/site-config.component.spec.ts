import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { AdminGroupService } from './../../../../rule-engine/admin-group/services/admin-group.service';
import { LanguagesService } from './../../../../shared/services/languages/languages.service';
import { SiteConfigComponent } from './site-config.component';

describe('SiteConfigComponent', () => {
  let component: SiteConfigComponent;
  let fixture: ComponentFixture<SiteConfigComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: LanguagesService,
          useFactory: () => ({
            getLanguages: () => {
              return of(['idioma']);
            },
          }),
        },
        {
          provide: AdminGroupService,
          useFactory: () => ({
            detail: () => {},
            search: () => {},
          }),
        },
        {
          provide: TranslateService,
          useValue: {
            instant: () => {},
          },
        },
        {
          provide: BsModalService,
          useValue: {},
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SiteConfigComponent);
    component = fixture.componentInstance;

    component.formConfig = {
      locale_default: {
        value: null,
        validators: {
          required: true,
        },
      },
      locale_valid: {
        value: null,
      },
      included_products: {
        value: null,
        validators: {
          required: true,
        },
      },
    };

    component.item = {
      name: 'test',
      price: {},
      is_insurance_sold: false,
      is_the_cocktail_sold: false,
    };
  });

  it('init', () => {
    expect(component).toBeTruthy();
  });

  it(
    'set form values',
    waitForAsync(() => {
      const s = spyOn(component.formLoaded, 'emit');
      component.ngOnChanges({
        formConfig: { previousValue: null, currentValue: null, firstChange: false, isFirstChange: () => true },
        item: { previousValue: null, currentValue: null, firstChange: true, isFirstChange: () => true },
      });
      expect(s).toHaveBeenCalled();
    })
  );
});
