/* eslint-disable @typescript-eslint/unbound-method */
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { GenericApiRequest, MayHaveIdName } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { ExplodedTreeComponent } from 'src/app/modules/exploded-tree/exploded-tree.component';
import { ExplodedTree, ExplodedTreeElementType } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { StatusRulesPreview } from 'src/app/modules/ui-modal-select/components/ui-modal-rules-preview/models/modal-rules-preview.model';
import { UiModalSelectService } from 'src/app/modules/ui-modal-select/ui-modal-select.service';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { LanguagesService } from 'src/app/shared/services/languages/languages.service';
import { clone } from 'src/app/shared/utils/utils';
import { dateRangeValidator } from 'src/app/shared/validations/date-range.validator';
import { SubsiteListComponent } from 'src/app/sites/subsite/components/subsite-list/subsite-list.component';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../modules/chips-control/components/chips.control.model';
import { AdminGroupService } from '../../../../rule-engine/admin-group/services/admin-group.service';
import { FormItemConfig, FormUtilsService } from '../../../../shared/services/form-utils.service';
import { Props, SitesAttributes, SitesPreview, SITES_ATTRIBUTES } from '../../models/sites-tree-attributes.model';
import { Price, Promotion, ShippingMethod, Site, UpdateInfo } from '../../models/sites.model';
import { SitePreviewService } from '../../services/site-preview.service';
import { SitesTreeService } from '../../services/sites-tree.service';

const MERCHANDISE = 'merchandise';
@Component({
  selector: 'ff-site-config',
  templateUrl: './site-config.component.html',
  styleUrls: ['./site-config.component.scss'],
})
export class SiteConfigComponent implements OnChanges, OnInit, OnDestroy {
  @ViewChild(ExplodedTreeComponent) explodedTree!: ExplodedTreeNodeComponent;

  @Input() loading!: boolean;
  @Input() isSubsite = false;
  @Input() item!: Site;
  @Input() readonly = false;
  @Input() formConfig: { [key: string]: FormItemConfig } = {};
  @Input() subsites!: SubsiteListComponent;
  @Output()
  formContentLoaded: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() formLoaded: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  @Output()
  visibleAttribute: EventEmitter<string> = new EventEmitter<string>();

  tree: ExplodedTree[] = [];
  originalTree: ExplodedTree[] = [];
  filterItems = SITES_ATTRIBUTES.map((v) => this.translateSrv.instant(v.label || ''));
  // iconFilter = 'icon-atributos';
  activatedAttribute!: SitesAttributes;
  _haveChanges!: boolean;
  sitesPreview: SitesPreview | any = {};
  subscribe!: Subscription;
  refreshSubscribe!: Subscription;

  languagesList!: MayHaveIdName[];
  form!: FormGroup;
  formErrors: any = {};
  invalidStartHour = false;
  invalidEndHour = false;
  centerTypes = [
    { label: 'Centro fijo', value: 'FIXED_CENTER' },
    { label: 'Código postal', value: 'POST_CODE_CENTER' },
  ];
  priceCenters = [
    { label: '0090', value: '0090' },
    { label: '0202', value: '0202' },
  ];

  chipsConfig: { [key: string]: ChipsControlSelectConfig | null } = {
    locale_default: null,
    locale_valid: null,
    included_products: null,
    index_map: null,
    store_code_by_hierarchy: null,
    override_tags: null,
    available_centers: null,
    shipping_methods: null,
    hierarchy_special_ratio: null,
    price_available_centers: null,
    price_centers: null,
    excluded_categories: null,
  };

  fieldsLoaded: { [key: string]: boolean } = {
    included_products: false,
  };

  constructor(
    private languagesService: LanguagesService,
    private adminGroupService: AdminGroupService,
    private translateSrv: TranslateService,
    private sitesTreeSrv: SitesTreeService,
    private UiModalService: UiModalSelectService,
    private modalService: BsModalService,
    private sitePreviewSrv: SitePreviewService
  ) {
    this.initLocale();
    this.initSelect('index_map', ModalChipsTypes.i18n);
    this.initIncludedProducts();
    this.initSelect('store_code_by_hierarchy', ModalChipsTypes.keyvalue, undefined, undefined, 'SITES.DIGITAL_STORE', 'SITES.CATEGORY');
    this.initSelect('available_centers', ModalChipsTypes.free);
    this.initSelect('shipping_methods', ModalChipsTypes.free);
    this.initSelect('override_tags', ModalChipsTypes.free);
    this.initSelect('hierarchy_special_ratio', ModalChipsTypes.free);
    this.initSelect('price_available_centers', ModalChipsTypes.free);
    this.initSelect('price_centers', ModalChipsTypes.free);
    this.initSelect('excluded_categories', ModalChipsTypes.free, undefined, undefined, undefined, undefined, 'SITES.CATEGORIES');
  }

  set haveChanges(value: boolean) {
    this._haveChanges = value;
    this.explodedTree?.update();
  }

  public get isCollapsedConfig(): any {
    return this._isCollapsedConfig;
  }
  public set isCollapsedConfig(value: any) {
    this._isCollapsedConfig = value;
  }
  private _isCollapsedConfig: any = {};

  ngOnInit() {
    this.originalTree = this.sitesTreeSrv.getSitesTreeAttributes();
    this.tree = clone(this.originalTree);
    this.form.valueChanges.subscribe(() => {
      this.explodedTree.update();
      this.getPreview();
    });
    this.subscribe = this.sitePreviewSrv.modalPreview.subscribe(() => {
      this.getPreview();
      this.preview();
    });
    this.refreshSubscribe = this.sitePreviewSrv.refreshSiteTree.subscribe(() => {
      this.getSubsiteTreeBadges();
      this.explodedTree.update();
    });
  }

  ngOnDestroy() {
    this.subscribe.unsubscribe();
    this.refreshSubscribe.unsubscribe();
  }

  ngOnChanges(event: SimpleChanges) {
    if (event.formConfig) {
      const config = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig(this.formConfig);
      this.form = config.form;
      this.formErrors = config.errors;
      this.formErrors.update_info = [
        {
          type: 'updateRequired',
          message: 'COMMON.REQUIRED_FIELD',
        },
      ];
      this.formErrors.time_range = [
        {
          type: 'invalidRangeDate',
          message: 'SITES.ERROR_TIME_RANGE',
        },
      ];
      this.formErrors.start_invalid = [
        {
          type: 'invalidStartHour',
          message: 'SITES.ERROR_TIME_RANGE',
        },
      ];
      this.formErrors.end_invalid = [
        {
          type: 'invalidEndHour',
          message: 'SITES.ERROR_TIME_RANGE',
        },
      ];

      this.form.setValidators([dateRangeValidator('start_time', 'end_time', false) as any, this.updateInfoFieldValidator]);
      this.formLoaded.emit(this.form);

      if (this.readonly) {
        this.form.disable();
      }
    }

    if (event.item) {
      if (this.form && this.item && Object.keys(this.item).length > 0) {
        this.setFormValues();
      } else {
        this.fieldsLoaded.included_products = true;
      }
    }
  }

  updateInfoFieldValidator: ValidatorFn = (control: AbstractControl): null | ValidationErrors => {
    const start = control.get('start_time')?.value;
    const end = control.get('end_time')?.value;
    const url = control.get('notification_url')?.value;
    return (start && end && url) || (!start && !end && !url) ? null : { updateRequired: true };
  };

  resetHour(type: string) {
    if (type === 'start') {
      this.form.controls.start_time.setValue(new Date());
      this.form.controls.start_time.reset();
      this.invalidStartHour = false;
    }
    if (type === 'end') {
      this.form.controls.end_time.setValue(new Date());
      this.form.controls.end_time.reset();
      this.invalidEndHour = false;
    }
  }

  // ExplodedTree functions

  click(event: any) {
    this.activatedAttribute = event.data.identifier;
  }

  search(event: string | string[], select = false) {
    this.tree = clone(this.originalTree);
    let translatedTree = this.tree;
    translatedTree.map((node) => {
      node.label = this.translateSrv.instant(node.label as string);
    });

    if (select) {
      translatedTree = translatedTree.filter((node) => (event as string[]).indexOf(node.label as string) > -1);
    } else {
      translatedTree = translatedTree.filter((item) => item.label?.toLocaleLowerCase()?.includes((event as string).toLocaleLowerCase()));
    }

    this.tree = translatedTree;
    this.explodedTree.update();
  }

  clear() {
    this.tree = clone(this.originalTree);
  }

  clickFilter(node: ExplodedTreeNodeComponent) {
    return node.data.element_type !== ExplodedTreeElementType.EMBEDDED;
  }

  iconFilter() {
    return 'icon-agrupaciones';
  }

  badgeFilter(node: ExplodedTreeNodeComponent<Props>) {
    const key = node.data.identifier || '';
    const form = this.form;

    if (key === 'locale') {
      if (form.controls.locale_default?.dirty || form.controls.locale_valid?.dirty) {
        return 'icon-icon-pencil';
      } else if (form.controls.locale_default.value || form.controls.locale_valid.value?.length) {
        return 'icon-icon-check';
      }
    }

    if (key === 'merchandise') {
      if (form.controls.included_products?.status === 'INVALID' && form.controls.included_products?.dirty) {
        return 'icon-icon-multiply';
      } else if (
        form.controls.included_products?.dirty ||
        form.controls.override_tags?.value?.dirty ||
        form.controls.is_the_cocktail_sold?.dirty ||
        form.controls.is_insurance_sold?.dirty
      ) {
        return 'icon-icon-pencil';
      } else if (
        form.controls.included_products?.value ||
        form.controls.override_tags?.value?.length ||
        form.controls.is_the_cocktail_sold?.value ||
        form.controls.is_insurance_sold?.value
      ) {
        return 'icon-icon-check';
      }
    }

    if (key === 'map') {
      if (
        (form.controls.index_main?.status === 'INVALID' && form.controls.index_main?.dirty) ||
        (form.controls.index_map?.status === 'INVALID' && form.controls.index_map?.dirty)
      ) {
        return 'icon-icon-multiply';
      } else if (form.controls.index_main?.dirty || form.controls.index_map?.dirty) {
        return 'icon-icon-pencil';
      } else if (
        form.controls.index_main?.value ||
        (form.controls.index_map?.value && Object.keys(form.controls.index_map?.value).length !== 0)
      ) {
        return 'icon-icon-check';
      }
    }

    if (key === 'shippingMethods') {
      if (
        (form.controls.default_shipping_method?.status === 'INVALID' && form.controls.default_shipping_method.dirty) ||
        (form.controls.shipping_methods?.status === 'INVALID' && form.controls.shipping_methods?.dirty) ||
        (form.controls.available_centers?.status === 'INVALID' && form.controls.available_centers?.dirty)
      ) {
        return 'icon-icon-multiply';
      } else if (
        form.controls.default_shipping_method?.dirty ||
        form.controls.shipping_methods?.dirty ||
        form.controls.available_centers?.dirty
      ) {
        return 'icon-icon-pencil';
      } else if (
        form.controls.default_shipping_method?.value ||
        (form.controls.shipping_methods?.value && form.controls.shipping_methods?.value?.length !== 0) ||
        (form.controls.available_centers?.value && form.controls.available_centers?.value?.length !== 0)
      ) {
        return 'icon-icon-check';
      }
    }

    if (key === 'hierarchies') {
      if (
        (form.controls.hierarchy_sales?.status === 'INVALID' && form.controls.hierarchy_sales?.dirty) ||
        (form.controls.hierarchy_campaign?.status === 'INVALID' && form.controls.hierarchy_campaign?.dirty) ||
        (form.controls.store_default_code?.status === 'INVALID' && form.controls.store_default_code?.dirty)
      ) {
        return 'icon-icon-multiply';
      } else if (
        form.controls.hierarchy_sales?.dirty ||
        form.controls.hierarchy_campaign?.dirty ||
        form.controls.hierarchy_special_ratio?.dirty ||
        form.controls.store_default_code?.dirty ||
        form.controls.store_code_by_hierarchy?.dirty
      ) {
        return 'icon-icon-pencil';
      } else if (
        form.controls.hierarchy_sales?.value ||
        form.controls.hierarchy_campaign?.value ||
        form.controls.hierarchy_special_ratio?.value?.length ||
        form.controls.store_default_code?.value ||
        (form.controls.store_code_by_hierarchy?.value && Object.keys(form.controls.store_code_by_hierarchy?.value).length !== 0)
      ) {
        return 'icon-icon-check';
      }
    }

    if (key === 'prices') {
      if (form.controls.center_type?.status === 'INVALID' && form.controls.center_type?.dirty) {
        return 'icon-icon-multiply';
      } else if (form.controls.center_type?.dirty || form.controls.fixed_center?.dirty || form.controls.price_available_centers?.dirty) {
        return 'icon-icon-pencil';
      } else if (
        form.controls.center_type?.value ||
        form.controls.fixed_center?.value ||
        (form.controls.price_available_centers?.value && form.controls.price_available_centers?.value?.length !== 0)
      ) {
        return 'icon-icon-check';
      }
    }

    if (key === 'updateInfo') {
      const startValue = form.controls.start_time?.value;
      const endValue = form.controls.end_time?.value;
      const notificationValue = form.controls.notification_url?.value;
      const allValue = startValue && endValue && notificationValue;
      const noneValue = !startValue && !endValue && !notificationValue;

      if (!allValue && !noneValue) {
        return 'icon-icon-multiply';
      } else if (form.controls.start_time?.dirty || form.controls.end_time?.dirty || form.controls.notification_url?.dirty) {
        return 'icon-icon-pencil';
      } else if (allValue) {
        return 'icon-icon-check';
      }
    }

    if (key === 'promotions') {
      if (
        (form.controls.minimum_discount?.status === 'INVALID' && form.controls.minimum_discount?.dirty) ||
        (form.controls.promotional_logic?.status === 'INVALID' && form.controls.promotional_logic?.dirty)
      ) {
        return 'icon-icon-multiply';
      }
      if (
        form.controls.minimum_discount?.dirty ||
        form.controls.promotional_logic?.dirty ||
        form.controls.price_logic?.dirty ||
        form.controls.price_centers?.dirty ||
        form.controls.modified_price_center?.dirty
      ) {
        return 'icon-icon-pencil';
      } else if (
        form.controls.minimum_discount?.value ||
        form.controls.promotional_logic?.value ||
        form.controls.price_logic?.value ||
        (form.controls.price_centers?.value && form.controls.price_centers?.value?.length !== 0) ||
        form.controls.modified_price_center?.value
      ) {
        return 'icon-icon-check';
      }
    }

    if (key === 'orders') {
      if (
        (form.controls.order_enterprise_code?.status === 'INVALID' && form.controls.order_enterprise_code?.dirty) ||
        (form.controls.order_fixed_center?.status === 'INVALID' && form.controls.order_fixed_center?.dirty) ||
        (form.controls.order_channel?.status === 'INVALID' && form.controls.order_channel?.dirty) ||
        (form.controls.order_sub_channel?.status === 'INVALID' && form.controls.order_sub_channel?.dirty) ||
        (form.controls.order_business_line?.status === 'INVALID' && form.controls.order_business_line?.dirty) ||
        (form.controls.order_company_dvd?.status === 'INVALID' && form.controls.order_company_dvd?.dirty) ||
        (form.controls.order_channel_dvd?.status === 'INVALID' && form.controls.order_channel_dvd?.dirty) ||
        (form.controls.order_center_type?.status === 'INVALID' && form.controls.order_center_type?.dirty)
      ) {
        return 'icon-icon-multiply';
      } else if (
        form.controls.order_enterprise_code?.dirty ||
        form.controls.order_fixed_center?.dirty ||
        form.controls.order_channel?.dirty ||
        form.controls.order_sub_channel?.dirty ||
        form.controls.order_business_line?.dirty ||
        form.controls.order_company_dvd?.dirty ||
        form.controls.order_channel_dvd?.dirty ||
        form.controls.order_center_type?.dirty
      ) {
        return 'icon-icon-pencil';
      } else if (
        form.controls.order_enterprise_code?.value ||
        form.controls.order_fixed_center?.value ||
        form.controls.order_channel?.value ||
        form.controls.order_sub_channel?.value ||
        form.controls.order_business_line?.value ||
        form.controls.order_company_dvd?.value ||
        form.controls.order_channel_dvd?.value ||
        form.controls.order_center_type?.value
      ) {
        return 'icon-icon-check';
      }
    }

    if (key === 'excludedCategories') {
      if (form.controls.excluded_categories?.dirty) {
        return 'icon-icon-pencil';
      } else if (form.controls.excluded_categories?.value && form.controls.excluded_categories?.value?.length !== 0) {
        return 'icon-icon-check';
      }
    }

    if (key === 'others') {
      if (form.controls.amount_printed?.dirty || form.controls.registered_user?.dirty || form.controls.view_in_csc?.dirty) {
        return 'icon-icon-pencil';
      } else if (form.controls.amount_printed?.value || form.controls.registered_user?.value || form.controls.view_in_csc?.value) {
        return 'icon-icon-check';
      }
    }
  }

  getPreview() {
    Object.keys(this.form.controls).forEach((key) => {
      const control = this.form.get(key);
      if (control?.errors && control.touched) {
        this.sitesPreview[key] = {
          values: control.value,
          status: StatusRulesPreview.deleted,
        };
      } else if (control?.dirty) {
        this.sitesPreview[key] = {
          values: control.value,
          status: StatusRulesPreview.edited,
        };
      } else if (control?.value) {
        this.sitesPreview[key] = {
          values: control.value,
          status: StatusRulesPreview.saved,
        };
      }
    });
    this.sitePreviewSrv.setHasPreviewObject(this.sitesPreview as SitesPreview);
  }

  getSubsiteTreeBadges() {
    Object.keys(this.form.controls).forEach((key) => {
      const control = this.form.get(key);
      if (control?.errors) {
        control.markAsDirty();
      }
    });
  }

  preview() {
    if (this.sitesPreview) {
      this.UiModalService.showSitePreview(this.sitesPreview, this.modalService).subscribe(() => {});
    }
  }

  private async setFormValues() {
    this.form.patchValue({
      ...this.item,
    });
    this.setLocale(this.item);
    this.setShippingMethod(this.item.shipping_method);
    this.setPrice(this.item.price);
    this.setPromotion(this.item.promotion);
    this.setUpdateInfo(this.item.update_info);
    await this.setIncludedProducts(this.item.included_products);
    setTimeout(() => {
      this.formContentLoaded.emit();
    }, 100);
  }

  private initLocale(): void {
    this.languagesService.getLanguages().subscribe((result) => {
      this.languagesList = result;
      this.chipsConfig.locale_default = {
        label: 'SITES.LOCALE_DEFAULT',
        modalType: ModalChipsTypes.select,
        modalConfig: {
          title: 'SITES.LOCALE_DEFAULT',
          multiple: false,
          items: this.languagesList,
        },
      };
      this.chipsConfig.locale_valid = {
        label: 'SITES.LOCALE_VALID',
        modalType: ModalChipsTypes.select,
        modalConfig: {
          title: 'SITES.LOCALE_VALID',
          multiple: true,
          items: this.languagesList,
        },
      };
    });
  }

  private initSelect(
    key: string,
    modalType: any,
    validators?: ValidatorFn[],
    errors?: { type: string; message: string }[],
    keyLabel?: string,
    valueLabel?: string,
    label = 'SITES.' + key.toUpperCase()
  ) {
    this.chipsConfig[key] = {
      label,
      modalType,
      modalConfig: {
        title: label,
        valueLabel,
        keyLabel,
        inputValidators: validators,
        validatorErrors: errors,
      },
    };
  }

  private initIncludedProducts() {
    this.chipsConfig.included_products = {
      label: 'SITES.INCLUDED_PRODUCTS',
      modalType: ModalChipsTypes.select,
      modalConfig: {
        title: 'SITES.INCLUDED_PRODUCTS',
        multiple: false,
        searchFn: (x: GenericApiRequest) => {
          return this.adminGroupService.list({
            ...x,
            type: MERCHANDISE,
          });
        },
        labelFilter: (item: AdminGroup, value: string): string => {
          return value + (!item.active ? ' (inactivo)' : '');
        },
        clickFilter: (item: AdminGroup): boolean => {
          return item.active;
        },
        classFilter: (item: AdminGroup): string => {
          return !item.active ? 'disabled' : '';
        },
      },
    };
  }

  private setLocale(item: Site): void {
    if (item && this.languagesList && this.languagesList.length > 0) {
      let itemLocaleDefault = null;
      const localeValidSelectedNames: MayHaveIdName[] = [];
      this.languagesList.forEach((lang) => {
        if (lang.id === item.locale_default) {
          itemLocaleDefault = lang;
        }
        if (item.locale_valid && item.locale_valid.length > 0) {
          item.locale_valid.forEach((localeValid) => {
            if (localeValid === lang.id) {
              localeValidSelectedNames.push(lang);
            }
          });
        }
      });

      this.form.controls.locale_default.setValue(itemLocaleDefault);
      this.form.controls.locale_valid.setValue(localeValidSelectedNames);
    }
  }

  private setShippingMethod(shipping_method: ShippingMethod | undefined) {
    if (shipping_method) {
      this.form.patchValue({
        ...shipping_method,
      });
    }
  }

  private setPrice(price: Price | undefined) {
    if (!price) {
      return false;
    }
    const centers = price.available_centers;
    delete price.available_centers;
    if (price) {
      this.form.patchValue({
        price_available_centers: centers,
        ...price,
      });
    }
  }
  private setUpdateInfo(updateInfo: UpdateInfo | undefined) {
    if (updateInfo?.end_time && updateInfo?.start_time && updateInfo?.notification_url) {
      const start = new Date();
      start.setHours(...this.getTime(updateInfo.start_time));
      const end = new Date();
      end.setHours(...this.getTime(updateInfo.end_time));
      this.form.controls.start_time.setValue(start);
      this.form.controls.end_time.setValue(end);
      this.form.controls.notification_url.setValue(updateInfo?.notification_url);
    }
  }

  private getTime(time: string): [number, number] {
    const times = time.split(':');
    return [Number(times[0]) || 0, Number(times[1]) || 0];
  }

  private setPromotion(promotion: Promotion | undefined) {
    if (!promotion) {
      return false;
    }
    const centers = promotion.price_centers;

    if (promotion) {
      this.form.patchValue({
        promotion_centers: centers,
        ...promotion,
      });
    }
  }

  private async setIncludedProducts(included_products: string | undefined) {
    if (included_products) {
      this.fieldsLoaded.included_products = false;
      let response = null;
      try {
        response = await this.adminGroupService
          .detail(this.item.included_products || '')
          .pipe(
            catchError((e) => {
              if (e && e.status === 404) {
                const checkSubsiteProducts = this.subsites?.list?.filter((v) => v.included_products).length;
                if (checkSubsiteProducts !== this.subsites?.list?.length) {
                  this.form.controls.included_products.setValidators(Validators.required);
                }
                this.item.included_products = '';
              }
              return of();
            })
          )
          .toPromise();
        // eslint-disable-next-line no-empty
      } catch (_) {}

      this.form.controls.included_products.setValue(response);
      this.fieldsLoaded.included_products = true;
    } else {
      this.fieldsLoaded.included_products = true;
    }
  }

  get isDirty(): false | UICommonModalConfig {
    if (this.form.pristine) {
      return false;
    }
    if (this.item?.id) {
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.item?.name,
      };
    } else {
      return {};
    }
  }
}
