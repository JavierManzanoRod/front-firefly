import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { ExplodedTreeModule } from 'src/app/modules/exploded-tree/exploded-tree.module';
import { TimepickerModule } from 'src/app/modules/timepicker/timepicker.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChipsControlModule } from '../../../../modules/chips-control/chips-control.module';
import { FormErrorModule } from '../../../../modules/form-error/form-error.module';
import { SiteConfigComponent } from './site-config.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModalModule,
    FormErrorModule,
    ChipsControlModule,
    NgSelectModule,
    DatetimepickerModule,
    TimepickerModule,
    ExplodedTreeModule,
    SimplebarAngularModule,
  ],
  declarations: [SiteConfigComponent],
  providers: [],
  exports: [SiteConfigComponent],
})
export class SiteConfigModule {}
