import { ElementRef, Renderer2 } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { TabDirective, TabsetComponent } from 'ngx-bootstrap/tabs';
import { SiteSubsitesValidationConfigService } from './../../services/site-subsites-validation-config.service';
import { SiteTabsComponent } from './site-tabs.component';

describe('SiteTabsComponent', () => {
  let component: SiteTabsComponent;
  let fixture: ComponentFixture<SiteTabsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BsModalService,
        {
          provide: TranslateService,
          useFactory: () => ({}),
        },
        {
          provide: SiteSubsitesValidationConfigService,
          useFactory: () => ({
            getSiteConfigFormConfig: () => {
              return {
                locale_default: {
                  value: null,
                  validators: {
                    required: true,
                  },
                },
                locale_valid: {
                  value: null,
                },
                included_products: {
                  value: null,
                  validators: {
                    required: true,
                  },
                },
              };
            },
          }),
        },
      ],
      imports: [ModalModule.forRoot(), RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(SiteTabsComponent);
    component = fixture.componentInstance;
    component.item = {
      name: 'test',
      is_the_cocktail_sold: false,
      is_insurance_sold: false,
    };
  });

  it('init', () => {
    component.ngOnInit();
    component.ngAfterViewChecked();
    expect(component).toBeTruthy();
  });

  it('onFormLoaded emit event', () => {
    const s = spyOn(component.formLoaded, 'emit');
    component.onFormLoaded(new FormGroup({}));
    expect(s).toHaveBeenCalled();
  });

  it('setTotalSubsites', () => {
    component.totalSubsites = 1;
    component.setTotalSubsites(2);
    expect(component.totalSubsites).toEqual(2);
  });

  it('go new subSite', () => {
    const s = spyOn(component.router, 'navigate');
    component.goNewSubSite();
    expect(s).toHaveBeenCalled();
  });

  it('dont change tab', () => {
    expect(
      component.onSelect(
        new TabDirective(
          new TabsetComponent(
            {
              type: 'test',
              isKeysAllowed: false,
              ariaLabel: 'hey',
            },
            '' as unknown as Renderer2,
            null as any
          ),
          { nativeElement: { parentNode: 'a' } } as ElementRef,
          '' as unknown as Renderer2
        )
      )
    ).toBeFalsy();
  });

  it('formContentLoaded', () => {
    component.configForm = new FormGroup({
      test: new FormControl('hey'),
    });
    component.oldConfigFormValues = false;
    component.formContentLoaded();
    expect(component.oldConfigFormValues.test).toBeTruthy();
  });

  it('form content no loaded', () => {
    component.configForm = new FormGroup({
      test: new FormControl('hey'),
    });
    component.oldConfigFormValues = true;
    component.formContentLoaded();
    expect(component.oldConfigFormValues.test).toBeFalsy();
  });
});
