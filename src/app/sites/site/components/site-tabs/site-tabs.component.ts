import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UtilsComponent } from '@core/base/utils-component';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TabDirective, TabsetComponent } from 'ngx-bootstrap/tabs';
import { Observable, Subject } from 'rxjs';
import { ModalUnsavedTabComponent } from 'src/app/modules/common-modal/components/modal-unsaved-tab/modal-unsaved-tab.component';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { SubsiteListComponent } from 'src/app/sites/subsite/components/subsite-list/subsite-list.component';
import { FormItemConfig } from '../../../../shared/services/form-utils.service';
import { Site } from '../../models/sites.model';
import { SitePreviewService } from '../../services/site-preview.service';
import { SiteSubsitesValidationConfigService } from '../../services/site-subsites-validation-config.service';

@Component({
  selector: 'ff-site-tabs',
  templateUrl: './site-tabs.component.html',
  styleUrls: ['./site-tabs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class SiteTabsComponent implements OnInit, AfterViewChecked {
  @Input() loading!: boolean;
  @Input() activeSubSite!: boolean;
  @Input() updateFormChanges!: Subject<any>;
  @Input() item!: Site;
  @Input() create!: boolean;
  @Output() formLoaded: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @ViewChild('staticTabs', { static: false }) staticTabs!: TabsetComponent;
  @ViewChild(SubsiteListComponent) subsites!: SubsiteListComponent;
  configFormConfig: { [key: string]: FormItemConfig } = {};

  configForm!: FormGroup;
  configFormChanged = false;
  oldConfigFormValues: any = null;

  utils: UtilsComponent = new UtilsComponent();
  bsModalRef!: BsModalRef;
  totalSubsites!: number;

  constructor(
    private modalService: BsModalService,
    private translateSrv: TranslateService,
    private siteSubsitesValidationConfigService: SiteSubsitesValidationConfigService,
    private sitePreviewSrv: SitePreviewService,
    public router: Router
  ) {
    this.configFormConfig = this.siteSubsitesValidationConfigService.getSiteConfigFormConfig();
    // Eliminamos todos los required por que en sites los campos que antes eran requerido ya no lo son.
    // Mantenemos los required en el service ya que es necesario para las comprobaciones de subsites
    for (const prop in this.configFormConfig) {
      if (Object.prototype.hasOwnProperty.call(this.configFormConfig, prop)) {
        if (this.configFormConfig[prop].validators) {
          const validators = this.configFormConfig[prop].validators;
          if (validators) validators.required = false;
        }
      }
    }
  }

  ngOnInit(): void {
    if (this.updateFormChanges) {
      this.updateFormChanges.subscribe(() => (this.configFormChanged = false));
    }
  }

  ngAfterViewChecked() {
    if (this.activeSubSite && this.staticTabs) {
      this.activeSubSite = false;
      setTimeout(() => {
        this.staticTabs.tabs[1].active = true;
      }, 0);
    }
  }

  onFormLoaded(event: FormGroup) {
    event.valueChanges.subscribe(() => {
      if (this.oldConfigFormValues) {
        this.configFormChanged = true;
      }
    });
    this.formLoaded.emit(event);
    this.configForm = event;
  }

  onSelect(tab: TabDirective) {
    if (tab.id === 'subsites-tab') {
      this.sitePreviewSrv.hideSubsiteListPreviewButton(true);
    } else {
      this.sitePreviewSrv.hideSubsiteListPreviewButton(false);
    }
    if (tab.id === 'subsites-tab') {
      if (this.configFormChanged) {
        // Si el formulario a cambiado le sacamos la modal, entonces le decimos a los tabs que esconda el actual
        // (subsites) y que muestre el otro (config)
        setTimeout(() => {
          this.staticTabs.tabs[0].active = true;
        }, 0);
        this.bsModalRef = this.modalService.show(ModalUnsavedTabComponent as any);
        this.bsModalRef.content.tabName = this.translateSrv.instant('SITES.TABS.CONFIGURATION');
        this.bsModalRef.content.errorMessage = this.translateSrv.instant('COMMON_MODALS.UNSAVED_CHANGES_ERROR_MESSAGE');
        (this.bsModalRef.content.cancel as Observable<boolean>).subscribe((res: boolean) => {
          if (res) {
            this.staticTabs.tabs[0].active = true;
            this.bsModalRef.hide();
          }
        });
        (this.bsModalRef.content.confirm as Observable<boolean>).subscribe((res: boolean) => {
          if (res) {
            this.configForm.patchValue(JSON.parse(JSON.stringify(this.oldConfigFormValues)));
            this.configFormChanged = false;
            this.staticTabs.tabs[1].active = true;
            this.bsModalRef.hide();
            this.configForm.markAsPristine();
          }
        });
      }
    }
    return false;
  }

  formContentLoaded() {
    if (!this.oldConfigFormValues) {
      this.oldConfigFormValues = JSON.parse(JSON.stringify(this.configForm.getRawValue()));
      // this.commonSiteSubSite.markFormPristine(this.configForm);
      this.configForm.markAsPristine();
    }
  }

  setTotalSubsites(total: number) {
    this.totalSubsites = total;
  }

  goNewSubSite() {
    this.router.navigate(['sites/subsite/new/', this.item.id]);
  }

  get isDirty(): false | UICommonModalConfig {
    if (this.configForm.pristine) {
      return false;
    }
    if (this.item?.id) {
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.item?.name,
      };
    } else {
      return {};
    }
  }
}
