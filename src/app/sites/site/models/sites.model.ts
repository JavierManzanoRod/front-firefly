import { GenericApiResponse } from '@model/base-api.model';

export interface ShippingMethod {
  default_shipping_method?: string;
  shipping_methods?: string[];
  available_centers?: string[];
}

export interface Price {
  center_type?: string;
  fixed_center?: string;
  available_centers?: string[];
}

export interface UpdateInfo {
  start_time?: string;
  end_time?: string;
  notification_url?: string;
}

export interface Promotion {
  promotional_logic?: string;
  minimum_discount?: string;
  price_logic?: string;
  price_centers?: string[];
  modified_price_center?: string;
}
export interface IndexMap {
  locale: string;
  index: string;
}

export interface StoreCodeByHierarchy {
  store_code: string;
  hierarchy: string;
}

export interface SiteDTO {
  id?: string;
  siteId?: string;
  name?: string;
  locale_default?: string;
  locale_valid?: string[];
  included_products?: string;
  index_main?: string;
  index_map?: IndexMap[];
  shipping_method?: ShippingMethod;
  hierarchy_sales?: string;
  hierarchy_campaign?: string;
  hierarchy_special_ratio?: string[];
  override_tags?: string[];
  store_default_code?: string;
  store_code_by_hierarchy?: StoreCodeByHierarchy[];
  price?: Price;
  update_info?: UpdateInfo;
  promotion?: Promotion;
  order_enterprise_code?: string;
  order_fixed_center?: string;
  order_channel?: string;
  order_sub_channel?: string;
  order_business_line?: string;
  order_company_dvd?: string;
  order_channel_dvd?: string;
  order_center_type?: string;
  is_amount_printed?: boolean;
  is_registered_user?: boolean;
  is_view_in_csc?: boolean;
  is_tck_digital?: boolean;
  is_the_cocktail_sold: boolean;
  is_insurance_sold: boolean;
  total_subsites?: number;
  excluded_categories?: string[];
}
export interface Site {
  id?: string;
  siteId?: string;
  name?: string;
  locale_default?: string;
  locale_valid?: string[];
  included_products?: string;
  index_main?: string;
  index_map?: Record<string, unknown>;
  shipping_method?: ShippingMethod;
  hierarchy_sales?: string;
  hierarchy_campaign?: string;
  hierarchy_special_ratio?: string[];
  override_tags?: string[];
  store_default_code?: string;
  store_code_by_hierarchy?: Record<string, unknown>;
  price?: Price;
  update_info?: UpdateInfo;
  promotion?: Promotion;
  order_enterprise_code?: string;
  order_fixed_center?: string;
  order_channel?: string;
  order_sub_channel?: string;
  order_business_line?: string;
  order_company_dvd?: string;
  order_channel_dvd?: string;
  order_center_type?: string;
  amount_printed?: boolean;
  registered_user?: boolean;
  view_in_csc?: boolean;
  is_tck_digital?: boolean;
  is_the_cocktail_sold: boolean;
  is_insurance_sold: boolean;
  total_subsites?: number;
  excluded_categories?: string[];
}

export type SitesResponse = GenericApiResponse<Site>;

export type SitesINResponse = GenericApiResponse<SiteDTO>;

export interface Subsite extends Site {
  url?: string;
  active?: boolean;
  subsiteId?: string;
}
export interface SubsiteDTO extends SiteDTO {
  url?: string;
  is_active?: boolean;
  subsiteId?: string;
}

export type SubsiteResponse = GenericApiResponse<Subsite>;

export type SubsiteResponseDTO = GenericApiResponse<SubsiteDTO>;
export interface Validations {
  code: string;
  message: string;
}
