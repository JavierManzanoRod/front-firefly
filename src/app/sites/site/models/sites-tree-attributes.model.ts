import { MayHaveLabel } from '@model/base-api.model';
import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { ExplodedTree } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { StatusRulesPreview } from 'src/app/modules/ui-modal-select/components/ui-modal-rules-preview/models/modal-rules-preview.model';

export enum SitesAttributes {
  locale = 'locale',
  merchandise = 'merchandise',
  map = 'map',
  shippingMethods = 'shippingMethods',
  hierarchies = 'hierarchies',
  prices = 'prices',
  updateInfo = 'updateInfo',
  promotions = 'promotions',
  orders = 'orders',
  excludedCategories = 'excludedCategories',
  others = 'others',
}

export const SITES_ATTRIBUTES_ARRAY: string[] = [
  'locale',
  'merchandise',
  'map',
  'shippingMethods',
  'hierarchies',
  'prices',
  'updateInfo',
  'promotions',
  'orders',
  'excludedCategories',
  'others',
];

export interface SortedSitesSectionsAttributes {
  [key: string]: string[];
}

export const LOCALE_ATTRIBUTES: string[] = ['locale_default', 'locale_valid'];

export const MERCHANDISE_ATTRIBUTES: string[] = ['included_products', 'override_tags', 'is_the_cocktail_sold', 'is_insurance_sold'];

export const MAP_ATTRIBUTES: string[] = ['index_main', 'index_map'];

export const SHIPPING_METHODS_ATTRIBUTES: string[] = ['default_shipping_method', 'shipping_methods', 'available_centers'];

export const HIERARCHIES_ATTRIBUTES: string[] = [
  'hierarchy_sales',
  'hierarchy_campaign',
  'hierarchy_special_ratio',
  'store_default_code',
  'store_code_by_hierarchy',
];

export const PRICES_ATTRIBUTES: string[] = ['center_type', 'fixed_center', 'price_available_centers'];

export const UPDATE_INFO_ATTRIBUTES: string[] = ['start_time', 'end_time', 'notification_url'];

export const PROMOTIONS_ATTRIBUTES: string[] = [
  'minimum_discount',
  'promotional_logic',
  'price_logic',
  'price_centers',
  'modified_price_center',
];

export const ORDERS_ATTRIBUTES: string[] = [
  'order_enterprise_code',
  'order_fixed_center',
  'order_channel',
  'order_sub_channel',
  'order_business_line',
  'order_company_dvd',
  'order_channel_dvd',
  'order_center_type',
];

export const EXCLUDED_CATEGORIES: string[] = ['excluded_categories'];

export const OTHERS_ATTRIBUTES: string[] = ['amount_printed', 'registered_user', 'view_in_csc'];

export interface SitesPreview {
  [key: string]: {
    values: string[] | MayHaveLabel[] | string | MayHaveLabel;
    status: StatusRulesPreview;
  };
}

export const SITES_ATTRIBUTES: ExplodedTree[] = [
  {
    label: 'SITES.MODAL_SELECT_LOCALE_TITLE',
    name: 'SITES.MODAL_SELECT_LOCALE_TITLE',
    identifier: SitesAttributes.locale,
  },
  {
    label: 'SITES.WARE',
    name: 'SITES.WARE',
    identifier: SitesAttributes.merchandise,
  },
  {
    label: 'SITES.MAP',
    name: 'SITES.MAP',
    identifier: SitesAttributes.map,
  },
  {
    label: 'SITES.SHIPPING_METHODS',
    name: 'SITES.SHIPPING_METHODS',
    identifier: SitesAttributes.shippingMethods,
  },
  {
    label: 'SITES.HIERARCHIES',
    name: 'SITES.HIERARCHIES',
    identifier: SitesAttributes.hierarchies,
  },
  {
    label: 'SITES.PRICES',
    name: 'SITES.PRICES',
    identifier: SitesAttributes.prices,
  },
  {
    label: 'SITES.UPDATE_INFO',
    name: 'SITES.UPDATE_INFO',
    identifier: SitesAttributes.updateInfo,
  },
  {
    label: 'SITES.PROMOTIONS.TITLE',
    name: 'SITES.PROMOTIONS.TITLE',
    identifier: SitesAttributes.promotions,
  },
  {
    label: 'SITES.ORDERS',
    name: 'SITES.ORDERS',
    identifier: SitesAttributes.orders,
  },
  {
    label: 'SITES.EXCLUDED_CATEGORIES',
    name: 'SITES.EXCLUDED_CATEGORIES',
    identifier: SitesAttributes.excludedCategories,
  },
  {
    label: 'SITES.OTHERS',
    name: 'SITES.OTHERS',
    identifier: SitesAttributes.others,
  },
];

export type Props = {
  control: {
    pristine: boolean;
  };
  identifier: ControlPanelConditions;
};
