import { Injectable } from '@angular/core';
import { ExplodedTree } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { SITES_ATTRIBUTES } from '../models/sites-tree-attributes.model';

@Injectable({
  providedIn: 'root',
})
export class SitesTreeService {
  constructor() {}

  getSitesTreeAttributes(): ExplodedTree[] {
    const attributeList: ExplodedTree[] = SITES_ATTRIBUTES;
    return attributeList;
  }
}
