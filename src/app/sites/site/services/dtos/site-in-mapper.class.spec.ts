import { Site } from '../../models/sites.model';
import { SiteInMapper } from './site-in-mapper.class';

describe('sitemapper in', () => {
  it('parse an incoming site as expected', () => {
    const remoteItem: Site = {
      index_map: {
        es_ES: 'ecistore_es',
      },
      store_code_by_hierarchy: {
        60: '999.2680963013',
      },
      price: {
        center_type: 'FIXED_CENTER',
        fixed_center: '0090',
        available_centers: ['0090'],
      },
      override_tags: ['Comunes_ISS', 'Hipercor_ISS'],
      id: 'eciStore',
      hierarchy_special_ratio: ['5004.28432876022'],
      shipping_method: {
        default_shipping_method: 'HomeDelivery',
        shipping_methods: ['HomeDelivery'],
        available_centers: ['0090'],
      },
      amount_printed: false,
      registered_user: false,
      is_tck_digital: false,
      is_the_cocktail_sold: false,
      is_insurance_sold: false,
      view_in_csc: true,
    } as Site;

    const mapper = new SiteInMapper(remoteItem);
    const site = mapper.data;
    expect(site.index_map).toEqual([
      {
        locale: 'es_ES',
        index: 'ecistore_es',
      },
    ]);
    expect(site.store_code_by_hierarchy).toEqual([
      {
        store_code: '60',
        hierarchy: '999.2680963013',
      },
    ]);
    expect(site.price).toEqual({
      center_type: 'FIXED_CENTER',
      fixed_center: '0090',
      available_centers: ['0090'],
    });
    expect(site.override_tags).toEqual(['Comunes_ISS', 'Hipercor_ISS']);
    expect(site.id).toEqual('eciStore');
    expect(site.hierarchy_special_ratio).toEqual(['5004.28432876022']);
    expect(site.shipping_method).toEqual({
      default_shipping_method: 'HomeDelivery',
      shipping_methods: ['HomeDelivery'],
      available_centers: ['0090'],
    });
    expect(site.is_amount_printed).toEqual(false);
    expect(site.is_registered_user).toEqual(false);
    expect(site.is_view_in_csc).toEqual(true);
  });

  it('parse an incoming site with some fields null or undefined does not break anything', () => {
    const remoteItem: Site = {
      index_map: undefined,
      store_code_by_hierarchy: undefined,
    } as Site;

    const mapper = new SiteInMapper(remoteItem);
    const site = mapper.data;
    expect(site.index_map).toEqual(undefined);
    expect(site.store_code_by_hierarchy).toEqual(undefined);
  });
});
