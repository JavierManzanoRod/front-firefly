import { bidimensionalyToDictionary } from '../../../../shared/utils/maper-utils';
import { Site, SiteDTO } from '../../models/sites.model';
export class SiteOutMapper {
  data = {} as Site;

  constructor({ ...data }: SiteDTO) {
    const override: Site = {
      view_in_csc: data.is_view_in_csc,
      registered_user: data.is_registered_user,
      amount_printed: data.is_amount_printed,
      is_insurance_sold: data.is_insurance_sold,
      is_the_cocktail_sold: data.is_the_cocktail_sold,
      is_tck_digital: data.is_tck_digital,
      excluded_categories: data.excluded_categories,
      index_map: bidimensionalyToDictionary(data.index_map, 'locale', 'index'),
      store_code_by_hierarchy: bidimensionalyToDictionary(data.store_code_by_hierarchy, 'store_code', 'hierarchy'),
    };
    delete data.is_view_in_csc;
    delete data.is_registered_user;
    delete data.is_amount_printed;

    this.data = { ...data, ...override } as unknown as Site;
  }
}
