import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, UpdateResponse } from '@core/base/api.service';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_SITES } from 'src/app/configuration/tokens/api-versions.token';
import { errorSiteSubSite } from 'src/app/sites/utils/errorSiteSubSite';
import { Site, SiteDTO } from '../models/sites.model';
import { SiteInMapper } from './dtos/site-in-mapper.class';
import { SiteOutMapper } from './dtos/site-out-mapper.class';

@Injectable({
  providedIn: 'root',
})
export class SiteService extends ApiService<Site> {
  endPoint = `products/backoffice-sites-subsites/${this.version}sites`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_SITES) private version: string) {
    super();
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<Site>> {
    return super.list(filter).pipe(
      map((response) => {
        response.content = response.content.map((site) => new SiteOutMapper(site as unknown as SiteDTO).data);
        return response;
      })
    );
  }

  detail(id: string): Observable<Site> {
    return super.detail(id).pipe(
      map((site) => {
        const siteDTO = new SiteOutMapper(site as unknown as SiteDTO);
        return siteDTO.data;
      })
    );
  }

  update(data: Site): Observable<UpdateResponse<Site>> {
    const url = `${this.urlBase}/${data.siteId}`;
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .request('put', url, {
        body: new SiteInMapper(data).data,
        headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            status: res.status,
            data: new SiteOutMapper(res.body as unknown as SiteDTO).data as unknown as Site,
          };
        }),
        catchError((e: HttpErrorResponse) => {
          return errorSiteSubSite(e);
        })
      );
  }

  post(data: Site): Observable<UpdateResponse<Site>> {
    return super.post(new SiteInMapper(data).data as unknown as Site).pipe(
      map((res: UpdateResponse<Site>) => {
        return {
          ...res,
          data: new SiteOutMapper(res.data as unknown as SiteDTO).data as unknown as Site,
        };
      })
    );
  }
}
