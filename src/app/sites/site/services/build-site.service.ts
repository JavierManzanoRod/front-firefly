import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LanguagesService } from 'src/app/shared/services/languages/languages.service';
import { Price, Promotion, ShippingMethod, Site, Subsite, UpdateInfo } from '../models/sites.model';

@Injectable({
  providedIn: 'root',
})
export class BuildSiteService {
  constructor(protected languagesService: LanguagesService) {}

  buildSite(mainForm: FormGroup, siteId: string): Site {
    const siteToSave: Site = this.commonbuildSiteSubsite(mainForm);

    if (siteId) {
      siteToSave.id = undefined;
    }
    siteToSave.siteId = siteId;

    return siteToSave;
  }

  buildSubsite(mainForm: FormGroup, subsiteId: string | undefined): Subsite {
    const subsiteToSave: Subsite = this.commonbuildSiteSubsite(mainForm);
    if (subsiteId) {
      subsiteToSave.id = undefined;
    }
    subsiteToSave.subsiteId = subsiteId;

    return subsiteToSave;
  }

  private commonbuildSiteSubsite(mainForm: FormGroup): Site | Subsite {
    const toSave: Site | Subsite = {
      ...mainForm.value.header,
      ...mainForm.value.config,
    };

    toSave.included_products =
      mainForm.value.config.included_products && typeof mainForm.value.config.included_products === 'object'
        ? mainForm.value.config.included_products.id
        : null;
    toSave.locale_default =
      mainForm.value.config.locale_default && typeof mainForm.value.config.locale_default === 'object'
        ? mainForm.value.config.locale_default.id
        : null;
    toSave.locale_valid = Array.isArray(mainForm.value.config.locale_valid)
      ? (mainForm.value.config.locale_valid as { id: string }[]).map((e: { id: string }) => e.id)
      : [];
    toSave.price = this.buildPrice(mainForm.value.config);
    toSave.update_info = this.buildUpdateInfo(mainForm.value.config);
    toSave.promotion = this.buildPromotions(mainForm.value.config);
    toSave.shipping_method = this.buildShippingMethod(mainForm.value.config);
    toSave.index_map = toSave.index_map || {};
    toSave.order_center_type = toSave.order_center_type || undefined;
    toSave.hierarchy_special_ratio = this.buildMultipleValues(mainForm.value.config.hierarchy_special_ratio);
    toSave.excluded_categories = toSave.excluded_categories || [];

    return toSave;
  }

  private buildShippingMethod(config: any): ShippingMethod {
    return {
      default_shipping_method: config.default_shipping_method,
      shipping_methods: this.buildMultipleValues(config.shipping_methods),
      available_centers: this.buildMultipleValues(config.available_centers),
    };
  }

  private buildPrice(config: any): Price {
    return {
      center_type: config.center_type || null,
      fixed_center: config.fixed_center,
      available_centers: this.buildMultipleValues(config.price_available_centers),
    };
  }
  private buildPromotions(config: any): Promotion {
    return {
      promotional_logic: config.promotional_logic || null,
      minimum_discount: config.minimum_discount || null,
      price_logic: config.minimum_discount || null,
      modified_price_center: config.modified_price_center || null,
      price_centers: this.buildMultipleValues(config.price_centers),
    };
  }

  private buildUpdateInfo(config: any): UpdateInfo {
    const start = config.start_time ? new Date(config.start_time) : undefined;
    const end = config.end_time ? new Date(config.end_time) : undefined;
    return {
      start_time: start ? this.getPaddedTime(start) : undefined,
      end_time: end ? this.getPaddedTime(end) : undefined,
      notification_url: config.notification_url ? config.notification_url : undefined,
    };
  }

  private getPaddedTime(date: Date) {
    return `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`;
  }

  private buildMultipleValues(list: any): string[] {
    if (list && typeof list === 'string') {
      return list.length > 0 ? list.split(',') : [];
    } else {
      return list && list.length > 0 ? (list as string[]) : [];
    }
  }
}
