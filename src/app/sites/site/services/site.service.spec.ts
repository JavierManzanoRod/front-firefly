import { HttpClient } from '@angular/common/http';
import { SiteService } from './site.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: SiteService;

const version = 'v1/';

describe('SitesService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`SitesService points to products/backoffice-sites-subsites/${version}sites`, () => {
    myService = new SiteService(null as unknown as HttpClient, version);
    expect(myService.endPoint).toEqual(`products/backoffice-sites-subsites/${version}sites`);
  });
});
