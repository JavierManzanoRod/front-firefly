import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SitesPreview } from '../models/sites-tree-attributes.model';

@Injectable({
  providedIn: 'root',
})
export class SitePreviewService {
  launchModal: Subject<boolean> = new Subject();
  hasPreview: Subject<boolean> = new Subject();
  hidePreviewButton: Subject<boolean> = new Subject();
  refreshSiteTree: Subject<boolean> = new Subject();

  constructor() {}

  get modalPreview() {
    return this.launchModal.asObservable();
  }

  launchModalPreview() {
    return this.launchModal.next(true);
  }

  setHasPreviewObject(data: SitesPreview) {
    this.hasPreview.next(Object.keys(data).some((key) => data[key]?.values));
  }

  hideSubsiteListPreviewButton(hideButton: boolean) {
    return this.hidePreviewButton.next(hideButton);
  }

  refreshTree() {
    this.refreshSiteTree.next(true);
  }
}
