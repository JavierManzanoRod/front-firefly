import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { Site } from '../models/sites.model';

@Injectable({
  providedIn: 'root',
})
export class SiteSubsitesValidationConfigService extends ApiService<Site> {
  constructor(protected http: HttpClient) {
    super();
  }

  getSubSiteHeaderFormConfig() {
    return {
      id: {
        value: null,
        validators: {
          required: true,
        },
      },
      name: {
        value: null,
        validators: {
          required: true,
          maxlength: 40,
        },
      },
      url: {
        value: null,
        validators: {
          required: true,
        },
      },
      active: {
        value: false,
      },
    };
  }

  getSiteConfigFormConfig() {
    return {
      locale_default: {
        value: null,
        validators: {
          required: true,
        },
      },
      locale_valid: {
        value: null,
      },
      included_products: {
        value: null,
        validators: {
          required: true,
        },
      },
      index_main: {
        value: null,
        validators: {
          required: true,
        },
      },
      index_map: {
        value: null,
        validators: {
          required: true,
        },
      },
      is_the_cocktail_sold: {
        value: false,
        validators: {
          required: false,
        },
      },
      is_insurance_sold: {
        value: false,
        validators: {
          required: false,
        },
      },
      default_shipping_method: {
        value: null,
        validators: {
          required: true,
        },
      },
      shipping_methods: {
        value: null,
        validators: {
          required: true,
        },
      },
      available_centers: {
        value: null,
        validators: {
          required: true,
        },
      },
      hierarchy_sales: {
        value: null,
        validators: {
          required: true,
          numeric: true,
        },
      },
      override_tags: {
        value: null,
      },
      hierarchy_campaign: {
        value: null,
        validators: {
          required: true,
          numeric: true,
        },
      },
      hierarchy_special_ratio: {
        value: null,
      },
      store_default_code: {
        value: null,
        validators: {
          required: true,
          numeric: true,
        },
      },
      store_code_by_hierarchy: {
        value: null,
      },
      center_type: {
        value: null,
        validators: {
          required: true,
        },
      },
      promotional_logic: {
        value: null,
        validators: {
          promotional_logic: true,
          required: true,
          maxlength: 2,
        },
      },
      price_centers: {
        value: null,
      },
      price_logic: {
        value: null,
        validators: {
          maxlength: 10,
        },
      },
      minimum_discount: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 3,
        },
      },
      modified_price_center: {
        value: null,
      },
      fixed_center: {
        value: null,
      },
      price_available_centers: {
        value: null,
      },
      start_time: {
        value: null,
        validators: {
          required: false,
        },
      },
      end_time: {
        value: null,
        validators: {
          required: false,
        },
      },
      notification_url: {
        value: null,
        validators: {
          required: false,
        },
      },
      order_enterprise_code: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 5,
        },
      },
      order_fixed_center: {
        value: null,
        validators: {
          required: true,
          maxlength: 5,
        },
      },
      order_channel: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 5,
        },
      },
      order_sub_channel: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 5,
        },
      },
      order_business_line: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 5,
        },
      },
      order_company_dvd: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 5,
        },
      },
      excluded_categories: {
        value: null,
        validators: {
          required: false,
        },
      },
      order_channel_dvd: {
        value: null,
        validators: {
          required: true,
        },
      },
      order_center_type: {
        value: null,
        validators: {
          required: true,
        },
      },
      amount_printed: {
        value: false,
        validators: {
          required: true,
        },
      },
      registered_user: {
        value: false,
        validators: {
          required: true,
        },
      },
      view_in_csc: {
        value: false,
        validators: {
          required: true,
        },
      },
    };
  }
}
