import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { SiteListContainerComponent } from './containers/site-list-container.component';
import { SiteViewContainerComponent } from './containers/site-view.container.component';

const routes: Routes = [
  {
    path: '',
    component: SiteListContainerComponent,
    data: {
      // header_title: 'MENU_LEFT.SITES',
      breadcrumb: [
        {
          label: 'SITES.LIST_TITLE',
          url: '',
        },
      ],
    },
  },
  {
    path: 'new',
    component: SiteViewContainerComponent,
    canDeactivate: [CanDeactivateGuard],
    data: {
      header_title: 'SITES.HEADER_TITLE',
      breadcrumb: [
        {
          label: 'SITES.BREAD_CRUMB_TITLE',
          url: '/sites/site',
        },
        {
          label: 'SITES.BREADCRUMB_ADD_NEW',
          url: '',
        },
      ],
    },
  },
  {
    path: 'edit/:id',
    component: SiteViewContainerComponent,
    canDeactivate: [CanDeactivateGuard],

    data: {
      header_title: 'SITES.HEADER_TITLE',
      breadcrumb: [
        {
          label: 'SITES.BREAD_CRUMB_TITLE',
          url: '/sites/site',
        },
        {
          label: 'SITES.BREADCRUMB_ADD_NEW',
          url: '',
        },
      ],
    },
  },
  {
    path: 'edit/:id/:tab',
    component: SiteViewContainerComponent,
    canDeactivate: [CanDeactivateGuard],
    data: {
      header_title: 'SITES.HEADER_TITLE',
      breadcrumb: [
        {
          label: 'SITES.BREAD_CRUMB_TITLE',
          url: '/sites/site',
        },
        {
          label: 'SITES.BREADCRUMB_ADD_NEW',
          url: '',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SiteRoutingModule {}
