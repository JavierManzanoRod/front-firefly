import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChipsControlModule } from '../../modules/chips-control/chips-control.module';
import { FormErrorModule } from '../../modules/form-error/form-error.module';
import { ToastModule } from '../../modules/toast/toast.module';
import { SubsiteModule } from '../subsite/subsite.module';
import { GenericListComponentModule } from './../../modules/generic-list/generic-list.module';
import { SiteConfigModule } from './components/site-config/site-config.module';
import { SiteTabsComponent } from './components/site-tabs/site-tabs.component';
import { SiteViewHeaderComponent } from './components/site-view-header/site-view-header.component';
import { SiteListContainerComponent } from './containers/site-list-container.component';
import { SiteViewContainerComponent } from './containers/site-view.container.component';
import { SiteRoutingModule } from './site-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SiteRoutingModule,
    CommonModalModule,
    ToastModule,
    CoreModule,
    TabsModule.forRoot(),
    SubsiteModule,
    SiteConfigModule,
    GenericListComponentModule,
    FormErrorModule,
    ChipsControlModule,
    SimplebarAngularModule,
  ],
  declarations: [SiteListContainerComponent, SiteTabsComponent, SiteViewContainerComponent, SiteViewHeaderComponent],
  providers: [],
  exports: [SiteViewHeaderComponent],
})
export class SiteModule {}
