import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormErrorModule } from '../../modules/form-error/form-error.module';
import { ToastModule } from '../../modules/toast/toast.module';
import { SiteConfigModule } from '../site/components/site-config/site-config.module';
import { SubsiteDetailHeaderComponent } from './components/subsite-detail-header/subsite-detail-header.component';
import { SubsiteListComponent } from './components/subsite-list/subsite-list.component';
import { SubsiteDetailContainerComponent } from './containers/subsite-detail-container.component';
import { SubsiteListContainerComponent } from './containers/subsite-list-container.component';
import { SubsiteRoutingModule } from './subsite-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SubsiteRoutingModule,
    CommonModalModule,
    ToastModule,
    SiteConfigModule,
    FormErrorModule,
    SimplebarAngularModule,
    CoreModule,
  ],
  declarations: [SubsiteDetailContainerComponent, SubsiteDetailHeaderComponent, SubsiteListComponent, SubsiteListContainerComponent],
  providers: [],
  exports: [SubsiteListComponent],
})
export class SubsiteModule {}
