import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { BuildSiteService } from '../../site/services/build-site.service';
import { SiteSubsitesValidationConfigService } from '../../site/services/site-subsites-validation-config.service';
import { SiteService } from '../../site/services/site.service';
import { SubsiteService } from '../services/subsite.service';
import { SubsiteDetailContainerComponent } from './subsite-detail-container.component';

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

describe('SubSiteDetailContainerComponent', () => {
  let fixture: ComponentFixture<SubsiteDetailContainerComponent>;
  let component: SubsiteDetailContainerComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SubsiteDetailContainerComponent, TranslatePipeMock],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: ActivatedRoute,
          useFactory: () => ({
            snapshot: {
              paramMap: {
                get: () => ({}),
              },
            },
          }),
        },
        {
          provide: SubsiteService,
          useFactory: () => ({
            detail: () => of({}),
            setSite: () => {},
          }),
        },
        {
          provide: BuildSiteService,
          useFactory: () => ({}),
        },
        {
          provide: Router,
          useFactory: () => ({
            navigate: () => ({}),
          }),
        },
        {
          provide: BsModalService,
          useFactory: () => ({}),
        },
        {
          provide: ToastService,
          useFactory: () => ({}),
        },
        {
          provide: CrudOperationsService,
          useFactory: () => ({}),
        },
        {
          provide: SiteService,
          useFactory: () => ({
            detail: () => of({}),
            setSite: () => {},
          }),
        },
        {
          provide: SiteSubsitesValidationConfigService,
          useFactory: () => ({
            getSiteConfigFormConfig: () => {
              return {
                locale_default: {
                  value: null,
                  validators: {
                    required: true,
                  },
                },
                locale_valid: {
                  value: null,
                },
                included_products: {
                  value: null,
                  validators: {
                    required: true,
                  },
                },
              };
            },
            getSubSiteHeaderFormConfig: () => new FormGroup({}),
          }),
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SubsiteDetailContainerComponent);
    component = fixture.componentInstance;
    component.site = {
      name: 'test',
      index_map: {},
      is_insurance_sold: false,
      is_the_cocktail_sold: false,
    };
    component.id = 'test';
  });

  it('init', () => {
    expect(component).toBeTruthy();
  });

  it('load form', () => {
    const service: SiteSubsitesValidationConfigService = fixture.debugElement.injector.get(SiteSubsitesValidationConfigService);
    const s = spyOn(service, 'getSiteConfigFormConfig').and.returnValue({
      locale_default: {
        value: null,
        validators: {
          required: true,
        },
      },
      locale_valid: {
        value: null,
      },
      included_products: {
        value: null,
        validators: {
          required: true,
        },
      },
      index_main: {
        value: null,
        validators: {
          required: true,
        },
      },
      index_map: {
        value: null,
        validators: {
          required: true,
        },
      },
      is_the_cocktail_sold: {
        value: false,
        validators: {
          required: false,
        },
      },
      is_insurance_sold: {
        value: false,
        validators: {
          required: false,
        },
      },
      default_shipping_method: {
        value: null,
        validators: {
          required: true,
        },
      },
      shipping_methods: {
        value: null,
        validators: {
          required: true,
        },
      },
      available_centers: {
        value: null,
        validators: {
          required: true,
        },
      },
      hierarchy_sales: {
        value: null,
        validators: {
          required: true,
          numeric: true,
        },
      },
      override_tags: {
        value: null,
      },
      hierarchy_campaign: {
        value: null,
        validators: {
          required: true,
          numeric: true,
        },
      },
      hierarchy_special_ratio: {
        value: null,
      },
      store_default_code: {
        value: null,
        validators: {
          required: true,
          numeric: true,
        },
      },
      store_code_by_hierarchy: {
        value: null,
      },
      center_type: {
        value: null,
        validators: {
          required: true,
        },
      },
      promotional_logic: {
        value: null,
        validators: {
          promotional_logic: true,
          required: true,
          maxlength: 2,
        },
      },
      price_centers: {
        value: null,
      },
      price_logic: {
        value: null,
        validators: {
          maxlength: 10,
        },
      },
      minimum_discount: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 3,
        },
      },
      modified_price_center: {
        value: null,
      },
      fixed_center: {
        value: null,
      },
      price_available_centers: {
        value: null,
      },
      start_time: {
        value: null,
        validators: {
          required: false,
        },
      },
      end_time: {
        value: null,
        validators: {
          required: false,
        },
      },
      notification_url: {
        value: null,
        validators: {
          required: false,
        },
      },
      order_enterprise_code: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 5,
        },
      },
      order_fixed_center: {
        value: null,
        validators: {
          required: true,
          maxlength: 5,
        },
      },
      order_channel: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 5,
        },
      },
      order_sub_channel: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 5,
        },
      },
      order_business_line: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 5,
        },
      },
      order_company_dvd: {
        value: null,
        validators: {
          required: true,
          numeric: true,
          maxlength: 5,
        },
      },
      order_channel_dvd: {
        value: null,
        validators: {
          required: true,
        },
      },
      order_center_type: {
        value: null,
        validators: {
          required: true,
        },
      },
      excluded_categories: {
        value: null,
        validators: {
          required: false,
        },
      },
      amount_printed: {
        value: false,
        validators: {
          required: true,
        },
      },
      registered_user: {
        value: false,
        validators: {
          required: true,
        },
      },
      view_in_csc: {
        value: false,
        validators: {
          required: true,
        },
      },
    });
    component.ngOnInit();
    expect(s).toHaveBeenCalled();
  });

  it('load form header', () => {
    component.mainForm = new FormGroup({});
    component.formHeaderLoaded(new FormGroup({}));
    expect(component.mainForm.controls.header).toBeTruthy();
  });

  /*it('redirect to url', () => {
        const s = spyOn(component.router, 'navigate');
        component.cancel();
        expect(s).toHaveBeenCalled();
    });*/
});
