import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { SubsiteService } from '../services/subsite.service';
import { SubsiteListContainerComponent } from './subsite-list-container.component';

describe('SubsiteListContainerComponent', () => {
  let fixture: ComponentFixture<SubsiteListContainerComponent>;
  let component: SubsiteListContainerComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SubsiteListContainerComponent],
      imports: [RouterTestingModule.withRoutes([]), TranslateModule.forRoot()],
      providers: [
        {
          provide: SubsiteService,
          useFactory: () => ({
            list: () => of({}),
          }),
        },
        { provide: CrudOperationsService, useFactory: () => ({}) },
        { provide: BsModalService, useFactory: () => ({}) },
        { provide: ToastrService, useFactory: () => ({}) },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(SubsiteListContainerComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });
});
