import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { Subsite } from '../../site/models/sites.model';
import { SubsiteService } from '../services/subsite.service';

@Component({
  selector: 'ff-subsite-list-container',
  template: `<ff-page-header pageTitle="{{ 'SITES.TITLE' | translate }}">
      <a [routerLink]="['/sites/site/']" class="btn btn-primary">
        {{ 'SITES.ADD' | translate }}
      </a>
    </ff-page-header>
    <ngx-simplebar class="page-container">
      <div class="page-scroll-wrapper">
        <ngx-simplebar class="page-scroll">
          <div class="page-container-padding">
            <ff-subsite-list [list]="list$ | async" [loading]="loading" [currentData]="currentData" (delete)="delete($event)">
            </ff-subsite-list>
          </div>
        </ngx-simplebar>
      </div>
    </ngx-simplebar> `,
})
export class SubsiteListContainerComponent extends BaseListContainerComponent<Subsite> implements OnInit {
  constructor(public apiService: SubsiteService, public utils: CrudOperationsService) {
    super(utils, apiService);
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
