import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CanComponentDeactivate } from '@core/guards/can-deactivate-guard';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CrudOperationsService, groupType } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { FormItemConfig, FormUtilsService } from '../../../shared/services/form-utils.service';
import { SiteConfigComponent } from '../../site/components/site-config/site-config.component';
import { Site, Subsite } from '../../site/models/sites.model';
import { BuildSiteService } from '../../site/services/build-site.service';
import { SitePreviewService } from '../../site/services/site-preview.service';
import { SiteSubsitesValidationConfigService } from '../../site/services/site-subsites-validation-config.service';
import { SiteService } from '../../site/services/site.service';
import { BOSS405, BOSS407 } from '../../utils/constError';
import { SubsiteDetailHeaderComponent } from '../components/subsite-detail-header/subsite-detail-header.component';
import { SubsiteService } from '../services/subsite.service';

@Component({
  selector: 'ff-subsite-detail-container',
  template: `<ff-page-header
      [pageTitle]="title | translate"
      [breadcrumbLink]="urlSite"
      [breadcrumbTitle]="'SUBSITES.BREAD_CRUMB_TITLE' | translate"
    >
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-subsite-detail-header
        *ngIf="headerFormConfig"
        [loading]="loading"
        [item]="subsiteItem"
        [formConfig]="headerFormConfig"
        #detailHeaderElement
        (formLoaded)="formHeaderLoaded($event)"
      ></ff-subsite-detail-header>
      <ff-site-config
        *ngIf="configFormConfig"
        [formConfig]="configFormConfig"
        [loading]="loading"
        [isSubsite]="true"
        [item]="subsiteItem"
        #detailSubSiteElement
        (formLoaded)="formConfigLoaded($event)"
      ></ff-site-config>
      <div class="page-footer text-right" *ngIf="!loading">
        <button type="button" class="btn btn-outline-primary btn-sm" (click)="cancel()">
          {{ 'COMMON.CANCEL' | translate }}
        </button>
        <button type="button" class="btn btn-primary ml-2 btn-lg" (click)="preview()" [disabled]="!hasPreview">
          {{ 'COMMON.PREVIEW' | translate }}
        </button>
        <button type="button" class="btn btn-primary ml-2 btn-lg" (click)="submit()">
          {{ 'COMMON.SAVE' | translate }}
        </button>
      </div>
    </ngx-simplebar> `,
})
// extends BaseDetailContainerComponent<MayHaveLabel>
export class SubsiteDetailContainerComponent implements OnInit, CanComponentDeactivate {
  @ViewChild('detailHeaderElement', { static: false }) detailHeaderElement!: SubsiteDetailHeaderComponent;
  @ViewChild('detailSubSiteElement', { static: false }) detailSubSiteElement!: SiteConfigComponent;
  urlListado = 'sites/subsite/edit';
  urlSite!: string;
  subsite$!: Observable<Subsite>;
  site!: Site;
  subsiteItem!: Subsite;
  idSite: string | undefined;
  title: string;
  configFormConfig!: { [key: string]: FormItemConfig };
  headerFormConfig!: { [key: string]: FormItemConfig };
  id: string | undefined;
  tab!: string;
  loading = true;
  oldConfigFormValues: any = null;
  mainForm!: FormGroup;
  hasPreview = false;

  // utils = new UtilsComponent();

  constructor(
    public route: ActivatedRoute,
    public subsiteService: SubsiteService,
    public buildSiteService: BuildSiteService,
    public router: Router,
    public modalService: BsModalService,
    public toast: ToastService,
    public siteService: SiteService,
    private siteSubsitesValidationConfigService: SiteSubsitesValidationConfigService,
    private utils: CrudOperationsService,
    private sitePreviewSrv: SitePreviewService
  ) {
    this.idSite = this.route.snapshot.paramMap.get('idSite') || undefined;
    this.id = this.route.snapshot.paramMap.get('id') || undefined;
    this.urlSite = `/sites/site/edit/${this.idSite}/1`;
    this.title = this.getTitle(this.id);
  }

  ngOnInit() {
    this.mainForm = new FormGroup({});
    this.processConfigFormConfig();
    this.sitePreviewSrv.hasPreview.subscribe((res) => (this.hasPreview = res));
  }

  processConfigFormConfig() {
    if (this.idSite) {
      this.siteService.detail(this.idSite).subscribe((siteData) => {
        this.site = siteData;
        if (this.site.index_map && Object.keys(this.site.index_map).length === 0) {
          this.site.index_map = undefined;
        }
        // Comprobamos los campos del header que son required
        this.headerFormConfig = this.siteSubsitesValidationConfigService.getSubSiteHeaderFormConfig();

        const configFormConfig: any = this.siteSubsitesValidationConfigService.getSiteConfigFormConfig();
        // Creamos el formulario de sites con los required a true
        const config: any = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig(
          this.siteSubsitesValidationConfigService.getSiteConfigFormConfig()
        );

        // TODO Borrar esto, solo esta para comprobar que funciona
        // siteData.locale_default = null;
        // siteData.order_fixed_center = null;

        // Añadimos los valores de sites al formulario para posteriormente comprobar los errores del formulario
        const form: FormGroup = config.form;
        form.patchValue({
          ...siteData,
        });
        if (siteData.shipping_method) {
          form.patchValue({
            ...siteData.shipping_method,
          });
        }
        if (siteData.price) {
          form.patchValue({
            price_available_centers: siteData.price.available_centers,
            ...siteData.price,
          });
        }
        if (siteData.promotion) {
          form?.patchValue({
            promotion_centers: siteData.promotion.price_centers,
            ...siteData.promotion,
          });
        }
        const keyRequiredErrors: string[] = [];
        // Vamos a comprobar que campos tienen el error de required y los que lo tengan le ponemos el required
        // a ese mismo campo del subsite
        Object.keys(config.form.controls).forEach((key) => {
          if (config.form.controls[key].errors && config.form.controls[key].errors.required) {
            keyRequiredErrors.push(key);
          }
        });
        Object.keys(configFormConfig).forEach((key) => {
          // Por defecto ponemos todos los campos a required false, para despues poner a required solo los que no
          // estan rellenados por el padre (que es lo que tenemos almacenado en keyRequiredErrors)
          if (configFormConfig[key].validators) {
            configFormConfig[key].validators.required = false;
            if (keyRequiredErrors.indexOf(key) >= 0) {
              configFormConfig[key].validators.required = true;
            }
          }
        });
        this.configFormConfig = configFormConfig;
        this.subsiteService.setSite(this.site);
        if (this.id) {
          this.subsiteService.detail(this.id).subscribe((subsiteData) => {
            this.subsiteItem = subsiteData;
            this.loading = false;
          });
        } else {
          this.loading = false;
        }
      });
    }
  }

  getTitle(id: string | undefined): string {
    return !id ? 'SUBSITES.CREATE' : 'SUBSITES.EDIT';
  }

  formHeaderLoaded(event: FormGroup) {
    this.mainForm.addControl('header', event);
    this.mainForm.markAsPristine();
  }

  formConfigLoaded(event: FormGroup) {
    this.mainForm.addControl('config', event);
    this.mainForm.markAsPristine();
  }

  cancel() {
    this.router.navigate([this.urlSite]);
  }

  submit() {
    FormUtilsService.markFormGroupTouched(this.mainForm);
    if (this.mainForm.valid) {
      const subsiteToSave: Subsite = this.buildSiteService.buildSubsite(this.mainForm, this.id);

      const config: any = this.mainForm.controls.config;
      const value: any = this.mainForm.value.config;
      const showWarning = !config.controls.end_time.pristine && value.end_time != null;

      this.utils
        .updateOrSaveModal(
          {
            apiService: this.subsiteService,
            methodToApply: this.id ? 'PUT' : 'POST',
          },
          subsiteToSave,
          groupType.SITE_SUBSITE,
          'COMMON_ERRORS.SAVE_ERROR',
          showWarning
        )
        .pipe(
          catchError((e: HttpErrorResponse) => {
            if (e.status === 409) {
              this.detailHeaderElement.form.controls.id.setErrors({ error_409: true });
            }
            return throwError(e);
          })
        )
        .subscribe((result) => {
          if ((result.status === 201 || result.status === 200) && !this.id) {
            this.router.navigate([`${this.urlListado}/${this.idSite}/${result.data?.id}`]);
          }

          this.mainForm.markAsPristine();
          this.sitePreviewSrv.refreshTree();

          if (result.error) {
            let errorMessage = 'SITES.FORM_ERROR.MESSAGE';
            if (typeof result.error === 'string' && result.error.includes(BOSS407)) {
              errorMessage = 'SITES.BOSS-407';
            }

            if (typeof result.error === 'string' && result.error.includes(BOSS405)) {
              errorMessage = 'SITES.BOSS-405';
            }
            this.toast.error(errorMessage, 'SITES.FORM_ERROR.TITLE');
          }
        });
    } else {
      this.sitePreviewSrv.refreshTree();
      this.toast.error('SITES.FORM_ERROR.MESSAGE', 'SITES.FORM_ERROR.TITLE');
    }
  }

  canDeactivate = () => {
    if (this.detailHeaderElement.isDirty) {
      return this.detailHeaderElement.isDirty;
    }
    if (this.detailSubSiteElement.isDirty) {
      return this.detailSubSiteElement.isDirty;
    }
    return false;
  };

  preview() {
    this.sitePreviewSrv.launchModalPreview();
  }
}
