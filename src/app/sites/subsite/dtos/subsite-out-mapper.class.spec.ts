import { Subsite, SubsiteDTO } from '../../site/models/sites.model';
import { SubsiteInMapper } from './subsite-in-mapper.class';
import { SubsiteOutMapper } from './subsite-out-mapper.class';

describe('subsitemapper out', () => {
  it('parse a outgoing subsite to request as expected', () => {
    const remoteItem: SubsiteDTO = {
      index_map: [
        {
          locale: 'es_ES',
          index: 'ecistore_es',
        },
      ],
      store_code_by_hierarchy: [
        {
          store_code: '60',
          hierarchy: '999.2680963013',
        },
      ],
      price: {
        center_type: 'FIXED_CENTER',
        fixed_center: '0090',
        available_centers: ['0090'],
      },
      override_tags: ['Comunes_ISS', 'Hipercor_ISS'],
      id: 'eciStore',
      hierarchy_special_ratio: ['5004.28432876022'],
      shipping_method: {
        default_shipping_method: 'HomeDelivery',
        shipping_methods: ['HomeDelivery'],
        available_centers: ['0090'],
      },
      is_active: true,
      is_amount_printed: false,
      is_registered_user: false,
      is_view_in_csc: true,
    } as SubsiteDTO;

    const mapper = new SubsiteOutMapper(remoteItem);
    const site = mapper.data;
    expect(site.index_map).toEqual({
      es_ES: 'ecistore_es',
    });
    expect(site.store_code_by_hierarchy).toEqual({
      60: '999.2680963013',
    });
    expect(site.price).toEqual({
      center_type: 'FIXED_CENTER',
      fixed_center: '0090',
      available_centers: ['0090'],
    });
    expect(site.override_tags).toEqual(['Comunes_ISS', 'Hipercor_ISS']);
    expect(site.id).toEqual('eciStore');
    expect(site.hierarchy_special_ratio).toEqual(['5004.28432876022']);
    expect(site.shipping_method).toEqual({
      default_shipping_method: 'HomeDelivery',
      shipping_methods: ['HomeDelivery'],
      available_centers: ['0090'],
    });
    expect(site.active).toEqual(true);
    expect(site.amount_printed).toEqual(false);
    expect(site.registered_user).toEqual(false);
    expect(site.view_in_csc).toEqual(true);
  });

  it('parse an outgoing subsite to request with some fields undefined to check if it does not break anything', () => {
    const remoteItem: Subsite = {
      index_map: undefined,
      store_code_by_hierarchy: undefined,
    } as Subsite;

    const mapper = new SubsiteInMapper(remoteItem);
    const site = mapper.data;
    expect(site.index_map).toEqual(undefined);
    expect(site.store_code_by_hierarchy).toEqual(undefined);
  });
});
