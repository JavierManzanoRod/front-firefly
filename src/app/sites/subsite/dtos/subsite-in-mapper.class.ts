import { dictionaryToBidimensionaly, MapperDictionary } from 'src/app/shared/utils/maper-utils';
import { Subsite, SubsiteDTO } from '../../site/models/sites.model';

export class SubsiteInMapper {
  data = {} as SubsiteDTO;

  constructor(remoteData: Subsite) {
    this.data = {
      id: remoteData.id,
      siteId: remoteData.siteId,
      name: remoteData.name,
      locale_default: remoteData.locale_default,
      locale_valid: remoteData.locale_valid,
      included_products: remoteData.included_products,
      index_main: remoteData.index_main,
      index_map: dictionaryToBidimensionaly(remoteData.index_map as MapperDictionary, 'locale', 'index'),
      shipping_method: remoteData.shipping_method,
      hierarchy_sales: remoteData.hierarchy_sales,
      hierarchy_campaign: remoteData.hierarchy_campaign,
      hierarchy_special_ratio: remoteData.hierarchy_special_ratio,
      excluded_categories: remoteData.excluded_categories,
      override_tags: remoteData.override_tags,
      store_default_code: remoteData.store_default_code,
      store_code_by_hierarchy: dictionaryToBidimensionaly(
        remoteData.store_code_by_hierarchy as MapperDictionary,
        'store_code',
        'hierarchy'
      ),
      price: remoteData.price,
      promotion: remoteData.promotion,
      update_info: remoteData.update_info,
      order_enterprise_code: remoteData.order_enterprise_code,
      order_fixed_center: remoteData.order_fixed_center,
      order_channel: remoteData.order_channel,
      order_sub_channel: remoteData.order_sub_channel,
      order_business_line: remoteData.order_business_line,
      order_company_dvd: remoteData.order_company_dvd,
      order_channel_dvd: remoteData.order_channel_dvd,
      order_center_type: remoteData.order_center_type,
      is_amount_printed: remoteData.amount_printed,
      is_registered_user: remoteData.registered_user,
      is_view_in_csc: remoteData.view_in_csc,
      is_the_cocktail_sold: remoteData.is_the_cocktail_sold,
      is_insurance_sold: remoteData.is_insurance_sold,
      total_subsites: remoteData.total_subsites,
      url: remoteData.url,
      is_active: remoteData.active,
      subsiteId: remoteData.subsiteId,
    } as unknown as SubsiteDTO;
  }
}
