import { bidimensionalyToDictionary } from 'src/app/shared/utils/maper-utils';
import { Subsite, SubsiteDTO } from '../../site/models/sites.model';

export class SubsiteOutMapper {
  data = {} as Subsite;

  constructor({ ...data }: SubsiteDTO) {
    const override: Subsite = {
      view_in_csc: data.is_view_in_csc,
      registered_user: data.is_registered_user,
      amount_printed: data.is_amount_printed,
      active: data.is_active,
      is_insurance_sold: data.is_insurance_sold,
      is_the_cocktail_sold: data.is_the_cocktail_sold,
      is_tck_digital: data.is_tck_digital,
      index_map: bidimensionalyToDictionary(data.index_map, 'locale', 'index'),
      store_code_by_hierarchy: bidimensionalyToDictionary(data.store_code_by_hierarchy, 'store_code', 'hierarchy'),
      excluded_categories: data.excluded_categories,
    };
    delete data.is_view_in_csc;
    delete data.is_registered_user;
    delete data.is_amount_printed;
    delete data.is_active;

    this.data = { ...data, ...override } as unknown as Subsite;
  }
}
