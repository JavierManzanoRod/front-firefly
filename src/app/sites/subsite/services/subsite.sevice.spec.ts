import { GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { Site, Subsite } from '../../site/models/sites.model';
import { SubsiteService } from './subsite.service';
let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: SubsiteService;

const version = 'v1/';
const expectedData: Subsite = { idSubsite: '1', name: 'name', is_the_cocktail_sold: false, is_insurance_sold: false } as Subsite;
const expectedList: GenericApiResponse<Subsite> = {
  content: [expectedData],
  page: null as unknown as Page,
};

describe('SubsiteService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`SubsiteService points to products/backoffice-sites-subsites/${version}sites`, () => {
    myService = new SubsiteService(null as any, version);
    expect(myService.endPoint).toEqual(`products/backoffice-sites-subsites/${version}sites`);
  });

  it('SubsiteService.list calls to get api method', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new SubsiteService(httpClientSpy as any, version);
    const site = { idSubsite: '1', name: 'name', is_insurance_sold: false, is_the_cocktail_sold: false } as Site;
    myService.list(site).subscribe((response) => {});
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
