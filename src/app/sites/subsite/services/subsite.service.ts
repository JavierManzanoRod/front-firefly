import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, IApiService2, UpdateResponse } from '@core/base/api.service';
import { environment } from '@env/environment';
import { GenericApiResponse } from '@model/base-api.model';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_SITES } from 'src/app/configuration/tokens/api-versions.token';
import { errorSiteSubSite } from 'src/app/sites/utils/errorSiteSubSite';
import { Site, Subsite, SubsiteDTO } from '../../site/models/sites.model';
import { SubsiteInMapper } from '../dtos/subsite-in-mapper.class';
import { SubsiteOutMapper } from '../dtos/subsite-out-mapper.class';

const urlBase = `${environment.API_URL_BACKOFFICE}`;

@Injectable({
  providedIn: 'root',
})
export class SubsiteService extends ApiService<Subsite> implements IApiService2<Subsite> {
  endPoint = `products/backoffice-sites-subsites/${this.version}sites`;
  site: Site | undefined;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_SITES) private version: string) {
    super();
  }

  list(site: Site): Observable<GenericApiResponse<Subsite>> {
    const url = `${urlBase}/${this.endPoint}/${String(site.id)}/subsites/`;
    return this.http.get<GenericApiResponse<Subsite>>(url, { params: { size: 9999 } }).pipe(
      map((response) => {
        response.content = response.content.map((subsite) => new SubsiteOutMapper(subsite as unknown as SubsiteDTO).data);
        return response;
      })
    );
  }

  detail(id: string): Observable<Subsite> {
    if (this.site) {
      const url = `${urlBase}/${this.endPoint}/${String(this.site.id)}/subsites/${id}`;
      return this.http.get<Subsite>(url).pipe(
        map((subsite) => {
          const subsiteDTO = new SubsiteOutMapper(subsite as unknown as SubsiteDTO).data;
          return subsiteDTO;
        })
      );
    }
    return of();
  }

  delete(subsite: Subsite): Observable<Subsite> {
    if (this.site) {
      const url = `${urlBase}/${this.endPoint}/${this.site.id}/subsites/${subsite.id}`;
      return this.http.delete<Subsite>(url);
    }
    return of();
  }

  setSite(site: Site): void {
    this.site = site;
  }

  resetSite(): void {
    this.site = undefined;
  }

  post(subsite: Subsite): Observable<UpdateResponse<Subsite>> {
    if (this.site) {
      const url = `${urlBase}/${this.endPoint}/${this.site.id}/subsites/`;
      const headers = new HttpHeaders().set('Content-Type', 'application/json');
      return this.http
        .request('post', url, {
          body: new SubsiteInMapper(subsite).data,
          headers,
          observe: 'response',
          responseType: 'json',
        })
        .pipe(
          map((res: HttpResponse<any>) => {
            return {
              status: res.status,
              data: new SubsiteOutMapper(res.body).data,
            };
          }),
          catchError((e: HttpErrorResponse) => {
            return errorSiteSubSite(e);
          })
        );
    }
    return of();
  }

  update(subsite: Subsite): Observable<UpdateResponse<Subsite>> {
    if (this.site) {
      const url = `${this.urlBase}/${this.site.id}/subsites/${subsite.subsiteId}`;
      const headers = new HttpHeaders().set('Content-Type', 'application/json');
      return this.http
        .request('put', url, {
          body: new SubsiteInMapper(subsite).data,
          headers,
          observe: 'response',
          responseType: 'json',
        })
        .pipe(
          map((res: HttpResponse<any>) => {
            return {
              status: res.status,
              data: new SubsiteOutMapper(res.body).data,
            };
          }),
          catchError((e: HttpErrorResponse) => {
            return errorSiteSubSite(e);
          })
        );
    }
    return of();
  }
}
