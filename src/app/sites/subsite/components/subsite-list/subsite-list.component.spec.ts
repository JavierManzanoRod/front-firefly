import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { ToastModule } from '../../../../modules/toast/toast.module';
import { SubsiteService } from '../../services/subsite.service';
import { SubsiteListComponent } from './subsite-list.component';

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

describe('SubsiteListComponent', () => {
  let fixture: ComponentFixture<SubsiteListComponent>;
  let component: SubsiteListComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: SubsiteService,
          useFactory: () => ({
            list: () => {},
            resetSite: () => {},
            setSite: () => {},
          }),
        },
        BsModalService,
        {
          provide: ToastrService,
          useFactory: () => ({}),
        },
        {
          provide: TranslateService,
          useFactory: () => ({}),
        },
      ],
      imports: [ModalModule.forRoot(), RouterTestingModule, ToastModule],
      declarations: [
        SubsiteListComponent,
        // MockPaginationComponent,
        TranslatePipeMock,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SubsiteListComponent);
    component = fixture.componentInstance;
    component.site = {
      name: 'prueba',
      id: '1',
      is_insurance_sold: false,
      is_the_cocktail_sold: false,
    };
  });

  it('init', () => {
    expect(component).toBeTruthy();
  });

  it('load subsites when have site', () => {
    const subSiteService: SubsiteService = fixture.debugElement.injector.get(SubsiteService);
    const s = spyOn(subSiteService, 'list').and.returnValue(
      of({
        content: [],
        page: null as any,
      })
    );
    component.ngOnChanges();
    expect(s).toHaveBeenCalled();
  });

  it('navigate edit', () => {
    const s = spyOn(component.router, 'navigate');
    component.detailSubsite(component.site);
    expect(s).toHaveBeenCalled();
  });
});
