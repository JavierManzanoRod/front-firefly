import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UtilsComponent } from '@core/base/utils-component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BaseListComponent } from 'src/app/shared/components/base-component-list.component';
import { ToastService } from '../../../../modules/toast/toast.service';
import { Site, Subsite } from '../../../site/models/sites.model';
import { SubsiteService } from '../../services/subsite.service';

@Component({
  selector: 'ff-subsite-list',
  templateUrl: './subsite-list.component.html',
  styleUrls: ['./subsite-list.component.scss'],
})
export class SubsiteListComponent extends BaseListComponent<Subsite> implements OnChanges {
  @Input() site!: Site;
  @Output() totalSubsites = new EventEmitter<number>();

  utils: UtilsComponent = new UtilsComponent();

  constructor(
    private subsiteService: SubsiteService,
    private modalService: BsModalService,
    private toast: ToastService,
    public router: Router
  ) {
    super();
    this.loading = true;
  }

  ngOnChanges() {
    if (this.site && Object.keys(this.site).length > 0) {
      this.loadSubsites();
    }
  }

  loadSubsites(): void {
    this.subsiteService.list(this.site).subscribe((subsites) => {
      this.list = subsites.content;
      this.totalSubsites.emit(this.list.length);
      this.page = subsites.page;
      this.loading = false;
    });
  }

  detailSubsite(item: Subsite) {
    this.router.navigate(['sites/subsite/edit/', this.site.id, item.id]);
  }

  handleDelete2(item: Subsite): void {
    if (item.active) {
      this.toast.error('SUBSITES.ERROR_ACTIVE');
    } else {
      const next = () => {
        this.subsiteService.resetSite();
        this.loading = true;
        setTimeout(() => {
          this.loadSubsites();
        }, 2000);
      };
      this.subsiteService.setSite(this.site);
      this.utils.deleteActionModal(
        {
          modalService: this.modalService,
          apiService: this.subsiteService,
          toast: this.toast,
        },
        item,
        next
      );
    }
  }
}
