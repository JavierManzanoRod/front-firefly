import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { FormItemConfig, FormUtilsService } from '../../../../shared/services/form-utils.service';

@Component({
  selector: 'ff-subsite-detail-header',
  templateUrl: 'subsite-detail-header.component.html',
  styleUrls: ['./subsite-detail-header.component.scss'],
  providers: [],
})
export class SubsiteDetailHeaderComponent implements OnChanges {
  @Input() item: any;
  @Input() loading!: boolean;
  @Input() showErrors!: boolean;
  @Input() formConfig!: { [key: string]: FormItemConfig };
  @Output() formLoaded: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  isCollapsedDefinition = false;

  formErrors: any = {};
  form!: FormGroup;

  isIdDisabled!: boolean;

  constructor() {}

  ngOnChanges(event: SimpleChanges) {
    if (event.formConfig) {
      const config = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig(this.formConfig);
      this.form = config.form;
      this.formErrors = config.errors;
      this.formLoaded.emit(this.form);
    }
    if (event.item && this.item && this.item.id) {
      this.setFormValues();
    }
  }

  setFormValues() {
    this.form.controls.id.setValue(this.item.id);
    this.isIdDisabled = true;
    // this.id.disable();
    this.form.controls.id.updateValueAndValidity({ emitEvent: false });
    this.form.patchValue({
      ...this.item,
    });
  }

  get isDirty(): false | UICommonModalConfig {
    if (this.form.pristine) {
      return false;
    }
    if (this.item?.id) {
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.item?.name,
      };
    } else {
      return {};
    }
  }
}
