import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup } from '@angular/forms';
import { SubsiteDetailHeaderComponent } from './subsite-detail-header.component';

describe('SubSiteDetailHeaderComponent', () => {
  let fixture: ComponentFixture<SubsiteDetailHeaderComponent>;
  let component: SubsiteDetailHeaderComponent;

  beforeEach(() => {
    fixture = TestBed.createComponent(SubsiteDetailHeaderComponent);
    component = fixture.componentInstance;

    component.formConfig = {
      locale_default: {
        value: null,
        validators: {
          required: true,
        },
      },
      id: {
        value: null,
      },
      included_products: {
        value: null,
        validators: {
          required: true,
        },
      },
    };

    component.form = new FormGroup({
      id: new FormControl(),
    });
    component.item = {
      name: 'test',
      id: 1,
    };
  });

  it('init', () => {
    expect(component).toBeTruthy();
  });

  it('set form values and emit event', () => {
    const s = spyOn(component.formLoaded, 'emit');
    component.ngOnChanges({
      formConfig: { previousValue: null, currentValue: null, firstChange: null as any, isFirstChange: () => true },
      item: { previousValue: null, currentValue: null, firstChange: null as any, isFirstChange: () => true },
    });
    expect(component.form.controls.id.value).toEqual(1);
    expect(s).toHaveBeenCalled();
  });
});
