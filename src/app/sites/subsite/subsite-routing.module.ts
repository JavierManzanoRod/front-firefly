import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { SubsiteDetailContainerComponent } from './containers/subsite-detail-container.component';
import { SubsiteListContainerComponent } from './containers/subsite-list-container.component';

const routes: Routes = [
  {
    path: 'new/:idSite',
    component: SubsiteDetailContainerComponent,
    canDeactivate: [CanDeactivateGuard],
    data: {
      header_title: 'MENU_LEFT.SITES',
      breadcrumb: [
        {
          label: 'SUBSITES.LIST_TITLE',
          url: '/sites/site',
        },
      ],
    },
  },
  {
    path: 'edit/:idSite/:id',
    component: SubsiteDetailContainerComponent,
    canDeactivate: [CanDeactivateGuard],
    data: {
      header_title: 'MENU_LEFT.SITES',
      breadcrumb: [
        {
          label: 'SUBSITES.LIST_TITLE',
          url: '/sites/site',
        },
      ],
    },
  },
  {
    path: ':id',
    component: SubsiteListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.SITES',
      breadcrumb: [
        {
          label: 'SUBSITES.LIST_TITLE',
          url: '/sites/site',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubsiteRoutingModule {}
