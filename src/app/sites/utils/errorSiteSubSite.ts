import { HttpErrorResponse } from '@angular/common/http';
import { UpdateResponse } from '@core/base/api.service';
import { of, throwError } from 'rxjs';
import { Validations } from 'src/app/sites/site/models/sites.model';
import { BOSS405, BOSS407, MDL107, MDL109 } from './constError';

export const errorSiteSubSite = (e: HttpErrorResponse) => {
  let arrError = [];
  if (e.status === 400 && e.error && e.error.error && e.error.error.detail && e.error.error.detail.validations) {
    // Si quito el channelDVD el code que recibo es MDL-101
    // si quito otro veo el MDL-106
    const validations: Validations[] = e.error.error.detail.validations;
    arrError = validations.map((validation: Validations) => {
      if (validation.code === BOSS407) {
        return BOSS407;
      }

      if (validation.code === BOSS405) {
        return BOSS405;
      }

      if (validation.code === MDL107) {
        return MDL107;
      }

      if (validation.code === MDL109) {
        return MDL109;
      }

      const words = validation.message.split(' ');
      return words ? words[words.length - 1] : '';
    });

    // Campos en back que no coinciden con el nombre del campo del form
    // En el form se usan otros por evitar anidamientos y complejidad
    arrError = arrError.map((error) => {
      if (error === 'shipping_method.shipping_methods') {
        error = 'shipping_methods';
      }

      if (error === 'shipping_method.default_shipping_method') {
        error = 'default_shipping_method';
      }

      if (error === 'shipping_method.available_centers') {
        error = 'price_available_centers';
      }

      if (error === 'price.center_type') {
        error = 'center_type';
      }

      return error;
    });

    const temp: UpdateResponse<any> = {
      status: e.status,
      data: null,
      error: arrError,
    };

    return of(temp);
  }

  return throwError(e);
};
