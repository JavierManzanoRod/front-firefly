import { errorSiteSubSite } from 'src/app/sites/utils/errorSiteSubSite';
describe('ErrorSiteSubSite', () => {
  it('get UpdateResponse parse', () => {
    const a = errorSiteSubSite({
      status: 400,
      error: {
        error: {
          detail: {
            validations: [
              { message: 'shippingMethod.shippingMethods' },
              { message: 'shippingMethod.defaultShippingMethod' },
              { message: 'shippingMethod.availableCenters' },
            ],
          },
        },
      },
      message: 'test',
      headers: null as any,
      statusText: 'test',
      url: 'test',
      type: null as any,
      name: null as any,
      ok: null as any,
    });
    expect(a).toBeTruthy();
  });
});
