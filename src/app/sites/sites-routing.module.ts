import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'site',
    data: {
      header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
    },
    loadChildren: () => import('./site/site.module').then((m) => m.SiteModule),
  },
  {
    path: 'subsite',
    data: {
      header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
    },
    loadChildren: () => import('./subsite/subsite.module').then((m) => m.SubsiteModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SitesRoutingModule {}
