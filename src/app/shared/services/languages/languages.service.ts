import { Injectable } from '@angular/core';
import { MayHaveIdName } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LanguagesService {
  languages!: MayHaveIdName[];
  constructor(private translateService: TranslateService) {}

  getLanguages(): Observable<MayHaveIdName[]> {
    if (this.languages) {
      return of(this.languages);
    } else {
      return this.translateService.get(['LANGUAGES']).pipe(
        map((languages) => {
          return Object.keys(languages.LANGUAGES).map((lang) => {
            return { id: lang, name: languages.LANGUAGES[lang] };
          });
        })
      );
    }
  }
}
