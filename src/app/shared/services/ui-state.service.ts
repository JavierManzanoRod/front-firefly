import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UiStateService {
  menuIsOpen = new Subject<boolean>();
  private _menuIsOpen = true;

  constructor() {
    this.subscribeMenuIsOpen((isOpen: boolean) => {
      this._menuIsOpen = isOpen;
    });
  }

  subscribeMenuIsOpen(callback: (isOpen: boolean) => void) {
    this.menuIsOpen.subscribe(callback);
  }

  closeMenu() {
    this.menuIsOpen.next(false);
  }

  closeMenuIfWindowSizeSmall() {
    if (window.innerWidth < 1400) {
      this.menuIsOpen.next(false);
    }
  }

  openMenu() {
    this.menuIsOpen.next(true);
  }

  toggleMenu() {
    this.menuIsOpen.next(!this._menuIsOpen);
  }
}
