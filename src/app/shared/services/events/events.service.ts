import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class EventsService {
  private c: { [topic: string]: Array<() => void> } = [] as any;

  public subscribe(topic: string, ...handlers: Array<(data: any) => void>) {
    if (!this.c[topic]) {
      this.c[topic] = [];
    }
    handlers.forEach((handler: any) => {
      this.c[topic].push(handler);
    });
  }

  public unsubscribe(topic: string, handler?: (event?: any) => void) {
    const t = this.c[topic];
    if (!t) {
      // Wasn't found, wasn't removed
      return false;
    }

    if (!handler) {
      // Remove all handlers for this topic
      delete this.c[topic];
      return true;
    }

    // We need to find and remove a specific handler
    const i = t.indexOf(handler);

    if (i < 0) {
      // Wasn't found, wasn't removed
      return false;
    }

    t.splice(i, 1);

    // If the channel is empty now, remove it from the channel map
    if (!t.length) {
      delete this.c[topic];
    }

    return true;
  }

  public publish(topic: string, ...args: any[]) {
    const t = this.c[topic];
    if (!t) {
      return null;
    }

    const responses: any[] = [];
    t.forEach((handler: any) => {
      responses.push(handler(...args));
    });
    return responses;
  }
}

export function setupEvents() {
  const events = new EventsService();

  window.addEventListener('online', (ev) => events.publish('app:online', ev));

  window.addEventListener('offline', (ev) => events.publish('app:offline', ev));

  window.addEventListener('orientationchange', (ev) => events.publish('app:rotated', ev));

  return events;
}

export function setupProvideEvents() {
  return () => {
    return setupEvents();
  };
}
