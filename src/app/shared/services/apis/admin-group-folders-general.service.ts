import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { FF_API_PATH_VERSION_ADMIN_GROUPS } from 'src/app/configuration/tokens/api-versions.token';

@Injectable({
  providedIn: 'root',
})
export abstract class AdminGroupFoldersServiceGeneral<U> extends ApiService<U> {
  endPoint = `products/backoffice-admin-group-folders/${this.version}admin-group-folders`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS) protected version: string) {
    super();
  }
}
