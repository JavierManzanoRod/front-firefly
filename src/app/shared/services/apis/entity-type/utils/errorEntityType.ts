import { HttpErrorResponse } from '@angular/common/http';
import { UpdateResponse } from '@core/base/api.service';
import { of, throwError } from 'rxjs';
import { Validations } from 'src/app/sites/site/models/sites.model';
import { BUS406 } from './constError';

export const errorEntityType = (e: HttpErrorResponse) => {
  let arrError = [];
  if (e.status === 400 && e.error && e.error.error && e.error.error.detail && e.error.error.detail.validations) {
    // Si quito el channelDVD el code que recibo es MDL-101
    // si quito otro veo el MDL-106
    const validations: Validations[] = e.error.error.detail.validations;
    arrError = validations.map((validation: Validations) => {
      if (validation.code === BUS406) {
        return BUS406;
      }

      const words = validation.message.split(' ');
      return words ? words[words.length - 1] : '';
    });

    const temp: UpdateResponse<any> = {
      status: e.status,
      data: null,
      error: arrError,
    };

    return of(temp);
  }

  return throwError(e);
};
