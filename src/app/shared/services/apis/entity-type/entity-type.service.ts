import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, IApiSearchable, IApiService2, UpdateResponse } from '@core/base/api.service';
import { EntityTypeDTO } from '@core/models/entity-type.dto';
import { EntityType } from '@core/models/entity-type.model';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_ENTITY_TYPES } from 'src/app/configuration/tokens/api-versions.token';
import { EntityTypeInMapper } from './dto/entity-type/entity-type-in-mapper.class';
import { EntityTypeOutMapper } from './dto/entity-type/entity-type-out-mapper.class';
import { errorEntityType } from './utils/errorEntityType';

@Injectable({
  providedIn: 'root',
})
export class EntityTypeService extends ApiService<EntityType> implements IApiService2<EntityType>, IApiSearchable<EntityType> {
  endPoint = `products/backoffice-entity-types/${this.version}entity-types`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_ENTITY_TYPES) private version: string) {
    super();
  }

  delete(data: EntityType): Observable<any> {
    return super.delete(data).pipe(
      catchError((e: HttpErrorResponse) => {
        return errorEntityType(e);
      })
    );
  }

  post(entityTypeToSave: EntityType) {
    const normalizeEntity = new EntityTypeOutMapper(entityTypeToSave).data as unknown as EntityType;
    return super.post(normalizeEntity).pipe(
      map(({ data, status, error }) => ({
        error,
        status,
        data: new EntityTypeInMapper(data as unknown as EntityTypeDTO).data,
      }))
    );
  }

  update(entityTypeToSave: EntityType): Observable<UpdateResponse<EntityType>> {
    const normalizeEntity = new EntityTypeOutMapper(entityTypeToSave).data as unknown as EntityType;
    return super.update(normalizeEntity).pipe(
      map(({ data, status, error }) => ({
        error,
        status,
        data: new EntityTypeInMapper(data as unknown as EntityTypeDTO).data,
      }))
    );
  }

  detail(id: string): Observable<EntityType> {
    return super.detail(id).pipe(
      map((data) => new EntityTypeInMapper(data as unknown as EntityTypeDTO).data),
      map((entity) => {
        for (const att of entity.attributes) {
          if (att.data_type === 'ENTITY' || att.data_type === 'EMBEDDED') {
            att.entity_view = {
              id: att.entity_reference || '',
              label: att.entity_reference_name || '',
              name: att.entity_reference_name || '',
            };
          }
        }
        return entity;
      })
    );
  }

  search(nameToSearch: string, page?: any): Observable<EntityType[]> {
    return this.list({ name: nameToSearch, ...page }).pipe(map(({ content }) => content));
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<EntityType>> {
    return this.http
      .get<GenericApiResponse<EntityType>>(this.urlBase, {
        params: this.getParams(this.getFilter(filter)),
      })
      .pipe(
        map(({ content, page }) => ({
          page,
          content: content.map((element) => new EntityTypeInMapper(element as unknown as EntityTypeDTO).data),
        })),
        map((value) => {
          value.content.forEach((v) => {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            this.filterLookTable(v);
          });
          return value;
        })
      );
  }

  filterLookTable(value: EntityType) {
    value.attributes = value.attributes?.filter((att) => {
      return !att.entity_reference?.toUpperCase().startsWith('LOOKUPTABLE');
    });
    return value;
  }
}
