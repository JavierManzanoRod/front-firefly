import { HttpClient } from '@angular/common/http';
import { AttributeEntityType, EntityType } from '@core/models/entity-type.model';
import { GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { Attribute } from '../../../../core/models/attribute.model';
import { EntityTypeService } from './entity-type.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy; request: jasmine.Spy };

let myService: EntityTypeService;
const expectedData: EntityType = { id: '1', name: 'name' } as EntityType;
const expectedList: GenericApiResponse<EntityType> = {
  content: [expectedData],
  page: null as unknown as Page,
};
const version = 'v1/';

describe('EntityTypeService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete', 'request']);
  });

  it(`EntityTypeService points to products/backoffice-entity-types/${version}entity-types`, () => {
    myService = new EntityTypeService(null as unknown as HttpClient, version);
    expect(myService.endPoint).toEqual(`products/backoffice-entity-types/${version}entity-types`);
  });

  it('post calls to POST http method', () => {
    const att = {
      entity_reference: '1',
      entity_view: { id: 'entity_reference_name', label: '1', name: '1' },
      data_type: 'ENTITY',
    } as AttributeEntityType;
    const entity: EntityType = { name: 'a', attributes: [att], is_master: false } as EntityType;

    httpClientSpy.request.and.returnValue(of({} as EntityType));
    myService = new EntityTypeService(httpClientSpy as any, version);
    myService.post(entity);
    expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
    expect(httpClientSpy.request).toHaveBeenCalledWith('post', jasmine.any(String), jasmine.any(Object));
  });

  it('update calls to PUT http method', () => {
    const att = {
      entity_reference: '1',
      entity_view: { id: 'entity_reference_name', label: '1', name: '1' },
      data_type: 'ENTITY',
    } as AttributeEntityType;
    const entity: EntityType = { name: 'a', attributes: [att], is_master: false };

    httpClientSpy.request.and.returnValue(of({} as EntityType));
    myService = new EntityTypeService(httpClientSpy as any, version);
    myService.update(entity);
    expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
    expect(httpClientSpy.request).toHaveBeenCalledWith('put', jasmine.any(String), jasmine.any(Object));
  });

  it('detail calls to get http method and create adhoc fields if  data_type is ENTITY', () => {
    const att: Attribute = {
      entity_reference: '1',
      data_type: 'ENTITY',
      entity_reference_name: 'name referred',
    } as Attribute;
    const entity: EntityType = { name: 'a', attributes: [att], is_master: false };

    httpClientSpy.get.and.returnValue(of(entity));
    myService = new EntityTypeService(httpClientSpy as any, version);
    myService.detail('111111').subscribe((response) => {
      expect(response.attributes[0].entity_view).toBeDefined();
    });
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
