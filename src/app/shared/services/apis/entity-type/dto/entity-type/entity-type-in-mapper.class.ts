import { EntityTypeDTO } from '@core/models/entity-type.dto';
import { EntityType } from '@core/models/entity-type.model';
import { EntityTypeAttributeInMapper } from '../entity-type-attribute/entity-type-attribute-in-mapper.class';

export class EntityTypeInMapper {
  data = {} as EntityType;

  constructor(remoteData: EntityTypeDTO) {
    this.data = {
      id: remoteData.identifier,
      label: remoteData.label,
      name: remoteData.name,
      is_master: remoteData.is_master,
      define_rules: remoteData.is_define_rules,
      is_tree_attribute: remoteData.is_tree_attribute,
      attributes: remoteData?.attributes?.map((at) => new EntityTypeAttributeInMapper(at).data),
    };
  }
}
