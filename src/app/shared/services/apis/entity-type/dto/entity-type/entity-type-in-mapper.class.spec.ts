import { EntityTypeDTO } from '@core/models/entity-type.dto';
import { EntityTypeInMapper } from './entity-type-in-mapper.class';

const entityTypeDTO: EntityTypeDTO = {
  identifier: 'pivq72e33iy5ys',
  name: 'Telefonia_Movil_PreDemo_3_ISS',
  is_define_rules: false,
  label: 'Telefonia_Movil_PreDemo_3_ISS',
  is_master: false,
  attributes: [
    {
      label: 'Tactil',
      name: 'anrjg6trekoiww',
      weight: 0,
      data_type: 'BOOLEAN',
      is_label_attribute: false,
      is_multiple_cardinality: false,
      evaluation_info: {
        path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/Tactil',
      },
      identifier: 'anrjg6trekoiww',
      is_i18n: false,
      tooltip_label: 'label',
      attribute_data_type: 'string',
      source: 'eci',
      value: [1, 2, 3],
      entity_view: {
        identifier: '123',
        name: 'entity',
        label: 'entity_label',
      },
      node_type: 'node',
      is_sub_entity_type: true,
      is_tree_attribute: false,
    },
    {
      label: 'UOM!TamanoPulgada!Tamano',
      name: 'ayfnivsf3rlp7q',
      weight: 0,
      data_type: 'DOUBLE',
      is_label_attribute: false,
      is_multiple_cardinality: false,
      evaluation_info: {
        path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/UOM!TamanoPulgada!Tamano',
      },
      identifier: 'ayfnivsf3rlp7q',
      is_i18n: false,
      is_sub_entity_type: true,
      is_tree_attribute: false,
    },
    {
      label: 'UOM!ResolucionMinima!ResolucionMinima',
      name: 'ayimgihzkmvrig',
      weight: 0,
      data_type: 'DOUBLE',
      is_label_attribute: false,
      is_multiple_cardinality: false,
      evaluation_info: {
        path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/UOM!ResolucionMinima!ResolucionMinima',
      },
      identifier: 'ayimgihzkmvrig',
      is_i18n: false,
      is_sub_entity_type: true,
      is_tree_attribute: false,
    },
    {
      label: 'UOM!ResolucionMaxima!ResolucionMaxima',
      name: 'a7gpod4ljhvfla',
      weight: 0,
      data_type: 'DOUBLE',
      is_label_attribute: false,
      is_multiple_cardinality: false,
      evaluation_info: {
        path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/UOM!ResolucionMaxima!ResolucionMaxima',
      },
      identifier: 'a7gpod4ljhvfla',
      is_i18n: false,
      is_sub_entity_type: true,
      is_tree_attribute: false,
    },
    {
      label: 'Colores',
      name: 'aieysnanmj7j2m',
      weight: 0,
      data_type: 'STRING',
      is_label_attribute: false,
      is_multiple_cardinality: false,
      evaluation_info: {
        path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/Colores',
      },
      identifier: 'aieysnanmj7j2m',
      is_i18n: true,
      is_sub_entity_type: true,
      is_tree_attribute: false,
    },
  ],
};

describe('EntityType mapper out', () => {
  it('parse a outgoing request as expected', () => {
    const mapper = new EntityTypeInMapper(entityTypeDTO);

    const entityType = mapper.data;
    expect(entityType.id).toEqual('pivq72e33iy5ys');
    expect(entityType.label).toEqual('Telefonia_Movil_PreDemo_3_ISS');
    expect(entityType.name).toEqual('Telefonia_Movil_PreDemo_3_ISS');
    expect(entityType.define_rules).toEqual(false);

    const attribute = entityType.attributes[0];
    expect(attribute.weight).toEqual(0);
    expect(attribute.data_type).toEqual('BOOLEAN');
    expect(attribute.label_attribute).toEqual(false);
    expect(attribute.multiple_cardinality).toEqual(false);
    expect(attribute.entity_reference).toBeUndefined();
    expect(attribute.entity_reference_name).toBeUndefined();
    expect(attribute.is_i18n).toEqual(false);
    expect(attribute.tooltip_label).toEqual('label');
    expect(attribute.attribute_data_type).toEqual('string');
    expect(attribute.source).toEqual('eci');
    expect(attribute.value).toEqual([1, 2, 3]);
    expect(attribute.entity_view).toEqual({
      id: '123',
      name: 'entity',
      label: 'entity_label',
    });
    expect(attribute.node_type).toEqual('node');
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem = {} as EntityTypeDTO;

    const mapper = new EntityTypeInMapper(remoteItem);
    const entityType = mapper.data;
    expect(entityType.id).toBeUndefined();
    expect(entityType.label).toBeUndefined();
    expect(entityType.name).toBeUndefined();
    expect(entityType.define_rules).toBeUndefined();
    expect(entityType.attributes).toBeUndefined();
  });
});
