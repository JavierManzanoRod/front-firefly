import { EntityType } from '@core/models/entity-type.model';
import { EntityTypeOutMapper } from './entity-type-out-mapper.class';

const entityType: EntityType = {
  id: 'pivq72e33iy5ys',
  name: 'Telefonia_Movil_PreDemo_3_ISS',
  define_rules: false,
  label: 'Telefonia_Movil_PreDemo_3_ISS',
  is_master: false,
  attributes: [
    {
      label: 'Tactil',
      name: 'anrjg6trekoiww',
      weight: 0,
      data_type: 'BOOLEAN',
      label_attribute: false,
      multiple_cardinality: false,
      evaluation_info: {
        path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/Tactil',
      },
      id: 'anrjg6trekoiww',
      is_i18n: false,
      tooltip_label: 'label',
      attribute_data_type: 'string',
      source: 'eci',
      value: [1, 2, 3],
      entity_view: {
        id: '123',
        name: 'entity',
        label: 'entity_label',
      },
      node_type: 'node',
    },
    {
      label: 'UOM!TamanoPulgada!Tamano',
      name: 'ayfnivsf3rlp7q',
      weight: 0,
      data_type: 'DOUBLE',
      label_attribute: false,
      multiple_cardinality: false,
      evaluation_info: {
        path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/UOM!TamanoPulgada!Tamano',
      },
      id: 'ayfnivsf3rlp7q',
      is_i18n: false,
    },
    {
      label: 'UOM!ResolucionMinima!ResolucionMinima',
      name: 'ayimgihzkmvrig',
      weight: 0,
      data_type: 'DOUBLE',
      label_attribute: false,
      multiple_cardinality: false,
      evaluation_info: {
        path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/UOM!ResolucionMinima!ResolucionMinima',
      },
      id: 'ayimgihzkmvrig',
      is_i18n: false,
    },
    {
      label: 'UOM!ResolucionMaxima!ResolucionMaxima',
      name: 'a7gpod4ljhvfla',
      weight: 0,
      data_type: 'DOUBLE',
      label_attribute: false,
      multiple_cardinality: false,
      evaluation_info: {
        path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/UOM!ResolucionMaxima!ResolucionMaxima',
      },
      id: 'a7gpod4ljhvfla',
      is_i18n: false,
    },
    {
      label: 'Colores',
      name: 'aieysnanmj7j2m',
      weight: 0,
      data_type: 'STRING',
      label_attribute: false,
      multiple_cardinality: false,
      evaluation_info: {
        path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/Colores',
      },
      id: 'aieysnanmj7j2m',
      is_i18n: true,
    },
  ],
};

describe('EntityType mapper out', () => {
  it('parse a outgoing request as expected', () => {
    const mapper = new EntityTypeOutMapper(entityType);

    const entityDto = mapper.data;
    expect(entityDto.identifier).toEqual('pivq72e33iy5ys');
    expect(entityDto.label).toEqual('Telefonia_Movil_PreDemo_3_ISS');
    expect(entityDto.name).toEqual('Telefonia_Movil_PreDemo_3_ISS');
    const attribute = entityDto.attributes[0];
    expect(attribute.weight).toEqual(0);
    expect(attribute.data_type).toEqual('BOOLEAN');
    expect(attribute.is_label_attribute).toEqual(false);
    expect(attribute.is_multiple_cardinality).toEqual(false);
    expect(attribute.is_i18n).toEqual(false);
    expect(attribute.tooltip_label).toEqual('label');
    expect(attribute.attribute_data_type).toEqual('string');
    expect(attribute.source).toEqual('eci');
    expect(attribute.value).toEqual([1, 2, 3]);
    expect(attribute.node_type).toEqual('node');
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem = {} as EntityType;

    const mapper = new EntityTypeOutMapper(remoteItem);
    const attribute = mapper.data;
    expect(attribute.identifier).toBeUndefined();
    expect(attribute.label).toBeUndefined();
    expect(attribute.name).toBeUndefined();
    expect(attribute.is_define_rules).toBeUndefined();
    expect(attribute.attributes).toBeUndefined();
  });
});
