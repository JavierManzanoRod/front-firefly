import { EntityTypeDTO } from '@core/models/entity-type.dto';
import { EntityType } from '@core/models/entity-type.model';
import { EntityTypeAttributeOutMapper } from '../entity-type-attribute/entity-type-attribute-out-mapper.class';

export class EntityTypeOutMapper {
  data = {} as EntityTypeDTO;

  constructor(data: EntityType) {
    const override = {
      ...(data.id && { identifier: data.id }),
      is_define_rules: data.define_rules,
      attributes: data.attributes?.map((at) => new EntityTypeAttributeOutMapper(at).data),
    };
    delete data.id;
    delete data.define_rules;
    this.data = { ...data, ...override };
  }
}
