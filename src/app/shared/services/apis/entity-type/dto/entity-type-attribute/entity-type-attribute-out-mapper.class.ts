import { AttributeEntityTypeDTO } from '@core/models/entity-type.dto';
import { AttributeEntityType } from '@core/models/entity-type.model';

export class EntityTypeAttributeOutMapper {
  data = {} as AttributeEntityTypeDTO;

  constructor({ ...data }: AttributeEntityType) {
    const override = {
      ...(data.id && { identifier: data.id }),
      is_label_attribute: data.label_attribute,
      is_multiple_cardinality: data.multiple_cardinality,
      entity_type_reference_id: data.entity_view?.id,
      entity_type_reference_name: data.entity_view?.name,
      is_sub_entity_type: data.is_sub_entity_type,
      is_tree_attribute: data.is_tree_attribute,
      entity_view: data.entity_view && {
        identifier: data.entity_view.id,
        name: data.entity_view.name,
        label: data.entity_view.label,
      },
    };
    delete data.id;
    delete data.label_attribute;
    delete data.multiple_cardinality;

    this.data = { ...data, ...override };
  }
}
