import { AttributeEntityTypeDTO } from '@core/models/entity-type.dto';
import { AttributeEntityType } from '@core/models/entity-type.model';

export class EntityTypeAttributeInMapper {
  data = {} as AttributeEntityType;

  constructor(remoteData: AttributeEntityTypeDTO) {
    this.data = {
      ...remoteData,
      id: remoteData.identifier,
      label_attribute: remoteData.is_label_attribute,
      multiple_cardinality: remoteData.is_multiple_cardinality,
      entity_reference: remoteData.entity_type_reference_id,
      entity_reference_name: remoteData.entity_type_reference_name,
      entity_view: {
        id: remoteData.entity_view?.identifier || '',
        name: remoteData.entity_view?.name || '',
        label: remoteData.entity_view?.label || '',
      },
    };
    if (remoteData.node_type) {
      this.data.node_type = remoteData.node_type;
    }
  }
}
