import { Attribute } from '@core/models/attribute.model';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { EntityTypeAttributeOutMapper } from './entity-type-attribute-out-mapper.class';

const attributes: AttributeEntityType[] = [
  {
    label: 'Tactil',
    name: 'anrjg6trekoiww',
    weight: 0,
    data_type: 'BOOLEAN',
    label_attribute: false,
    multiple_cardinality: false,
    evaluation_info: {
      path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/Tactil',
    },
    entity_reference: '1234',
    entity_reference_name: 'refName',
    id: 'anrjg6trekoiww',
    is_i18n: false,
    tooltip_label: 'label',
    attribute_data_type: 'string',
    source: 'eci',
    value: [1, 2, 3],
    entity_view: {
      id: '1234',
      name: 'entity',
      label: 'entity_label',
    },
    node_type: 'node',
  },
  {
    label: 'UOM!TamanoPulgada!Tamano',
    name: 'ayfnivsf3rlp7q',
    weight: 0,
    data_type: 'DOUBLE',
    label_attribute: false,
    multiple_cardinality: false,
    evaluation_info: {
      path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/UOM!TamanoPulgada!Tamano',
    },
    id: 'ayfnivsf3rlp7q',
    is_i18n: false,
  },
  {
    label: 'UOM!ResolucionMinima!ResolucionMinima',
    name: 'ayimgihzkmvrig',
    weight: 0,
    data_type: 'DOUBLE',
    label_attribute: false,
    multiple_cardinality: false,
    evaluation_info: {
      path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/UOM!ResolucionMinima!ResolucionMinima',
    },
    id: 'ayimgihzkmvrig',
    is_i18n: false,
  },
  {
    label: 'UOM!ResolucionMaxima!ResolucionMaxima',
    name: 'a7gpod4ljhvfla',
    weight: 0,
    data_type: 'DOUBLE',
    label_attribute: false,
    multiple_cardinality: false,
    evaluation_info: {
      path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/UOM!ResolucionMaxima!ResolucionMaxima',
    },
    id: 'a7gpod4ljhvfla',
    is_i18n: false,
  },
  {
    label: 'Colores',
    name: 'aieysnanmj7j2m',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    evaluation_info: {
      path: '/Telefonia_Movil_PreDemo_3_ISS/PantallaPrincipal/Colores',
    },
    id: 'aieysnanmj7j2m',
    is_i18n: true,
  },
];

describe('EntityTypeAttributeMapper out', () => {
  it('parse a outgoing request as expected', () => {
    const attribute = attributes[0];
    const mapper = new EntityTypeAttributeOutMapper(attribute);

    const attributeDTO = mapper.data;
    expect(attributeDTO.identifier).toEqual('anrjg6trekoiww');
    expect(attributeDTO.label).toEqual('Tactil');
    expect(attributeDTO.name).toEqual('anrjg6trekoiww');
    expect(attributeDTO.weight).toEqual(0);
    expect(attributeDTO.data_type).toEqual('BOOLEAN');
    expect(attributeDTO.is_label_attribute).toEqual(false);
    expect(attributeDTO.is_multiple_cardinality).toEqual(false);
    expect(attributeDTO.entity_type_reference_id).toEqual('1234');
    expect(attributeDTO.is_i18n).toEqual(false);
    expect(attributeDTO.tooltip_label).toEqual('label');
    expect(attributeDTO.attribute_data_type).toEqual('string');
    expect(attributeDTO.source).toEqual('eci');
    expect(attributeDTO.value).toEqual([1, 2, 3]);
    expect(attributeDTO.entity_view).toEqual({
      identifier: '1234',
      name: 'entity',
      label: 'entity_label',
    });
    expect(attributeDTO.node_type).toEqual('node');
    expect((attributeDTO as any).label_attribute).toBeUndefined();
    expect((attributeDTO as any).multiple_cardinality).toBeUndefined();
    // expect((attributeDTO as any).entity_reference).toBeUndefined();
    // expect((attributeDTO as any).entity_reference_name).toBeUndefined();
    expect((attributeDTO as any).id).toBeUndefined();
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem = {} as Attribute;

    const mapper = new EntityTypeAttributeOutMapper(remoteItem);
    const attribute = mapper.data;
    expect(attribute.identifier).toBeUndefined();
    expect(attribute.label).toBeUndefined();
    expect(attribute.name).toBeUndefined();
    expect(attribute.weight).toBeUndefined();
    expect(attribute.attribute_label).toBeUndefined();
    expect(attribute.data_type).toBeUndefined();
    expect(attribute.entity_type_id).toBeUndefined();
    expect(attribute.attribute_id).toBeUndefined();
    expect(attribute.is_label_attribute).toBeUndefined();
    expect(attribute.entity_type_reference_id).toBeUndefined();
    expect(attribute.entity_type_reference_name).toBeUndefined();
    expect(attribute.is_multiple_cardinality).toBeUndefined();
    expect(attribute.is_i18n).toBeUndefined();
    expect(attribute.tooltip_label).toBeUndefined();
    expect(attribute.attribute_data_type).toBeUndefined();
    expect(attribute.evaluation_info).toBeUndefined();
  });
});
