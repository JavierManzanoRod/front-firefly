import { Inject, Injectable } from '@angular/core';
import { GenericApiRequest } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { AdminGroupService } from '../../../rule-engine/admin-group/services/admin-group.service';
import { HttpClient } from '@angular/common/http';
import { FF_API_PATH_VERSION_ADMIN_GROUPS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';

@Injectable({
  providedIn: 'root',
})
export class AdminGroupTypeService extends AdminGroupService {
  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS) protected version: string,
    @Inject('') protected type: string
  ) {
    super(http, version);
  }

  detail(id: string): Observable<AdminGroup> {
    return super.detail(id);
  }

  list(data: GenericApiRequest) {
    return super.list({
      ...data,
      type: this.type,
    });
  }

  post(data: AdminGroup) {
    const result = this.parseResult(data);

    data = {
      ...data,
      admin_group_type_id: this.type,
    };

    if (result) {
      data.result = result;
    }

    return super.post(data);
  }

  update(data: AdminGroup) {
    const result = this.parseResult(data);

    data = {
      ...data,
      admin_group_type_id: this.type,
    };

    if (result) {
      data.result = result;
    }

    return super.update(data);
  }

  delete(x: AdminGroup) {
    return super.delete(x);
  }

  protected parseResult(_data: AdminGroup): string {
    return '';
  }
}
