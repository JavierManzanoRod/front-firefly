import { Injectable } from '@angular/core';
import { AbstractControlOptions, FormControl, FormGroup, Validators } from '@angular/forms';
import { arrEveryValidator } from '../validations/array-every.validator';
import { dateTimeValidator } from '../validations/date-time.validator';
import { numericValidator } from '../validations/numeric.validator';
import { positiveNumberValidator } from '../validations/positive-number-validator';
import { promotionalLogicValidation } from '../validations/promotional-logic.validator';

export interface FormItemConfig {
  value?: any;
  validators?: {
    required?: boolean;
    numeric?: boolean;
    maxlength?: number;
    minlength?: number;
    min?: number;
    max?: number;
    numericPositive?: boolean;
    arrayNumeric?: boolean;
    pattern?: {
      expression: string;
      errorTranslateMessage: string;
    };
    dateTimeValidator?: boolean;
    promotional_logic?: boolean;
    error_409?: boolean;
    // Añadir otro para array numerico y su validador
  };
  updateOn?: 'change' | 'blur' | 'submit';
}

export interface FormConfig {
  [prop: string]: FormItemConfig;
}

export interface FormErrorsItemConfig {
  type?: string;
  message?: string;
  params?: Record<string, unknown>;
}

export interface FormsErrorsConfig {
  [prop: string]: FormErrorsItemConfig[];
}

@Injectable({
  providedIn: 'root',
})
export class FormUtilsService {
  constructor() {}

  static markFormGroupTouched(formGroup: FormGroup) {
    for (const key in formGroup.controls) {
      if (Object.prototype.hasOwnProperty.call(formGroup.controls, key)) {
        formGroup.controls[key].markAsTouched();

        if ((formGroup.controls[key] as any).controls) {
          this.markFormGroupTouched(formGroup.controls[key] as FormGroup);
        }
      }
    }
  }

  static markFormGroupPristine(formGroup: FormGroup) {
    for (const key in formGroup.controls) {
      if (Object.prototype.hasOwnProperty.call(formGroup.controls, key)) {
        formGroup.controls[key].setErrors(null);
        formGroup.controls[key].markAsPristine();
        formGroup.controls[key].markAsUntouched();

        if ((formGroup.controls[key] as any).controls) {
          this.markFormGroupTouched(formGroup.controls[key] as FormGroup);
        }
      }
    }
  }

  static generateFormGroupAndHisGeneralErrorsFromConfig(
    config: { [key: string]: FormItemConfig },
    options?:
      | AbstractControlOptions
      | {
          [key: string]: any;
        }
      | null
      | undefined
  ) {
    // Configuracion final necesaria para el FormGroup
    const formGroupConfig: any = {};
    // Configuracion final para los errores del formulario
    const formErrors: any = {};
    // Variable por la que iteramos
    let itemConfig: FormItemConfig;
    // Variable auxiliar para almacenar los validadores de la variable por la que iteramos
    let validators;

    // Vamos a montar el formulario y los mensajes de error genericos usando la configuración
    for (const prop in config) {
      if (Object.prototype.hasOwnProperty.call(config, prop)) {
        itemConfig = config[prop];
        validators = [];
        formErrors[prop] = [] as Record<string, unknown>[];

        if (Object.prototype.hasOwnProperty.call(itemConfig, 'validators')) {
          // Añado el mensaje de error a cada propiedad independientemente de que si tiene el error o no
          // Asi solo se hace una vez y no tengo que actualizar por cada error
          formErrors[prop].push({ type: 'required', message: 'COMMON_ERRORS.REQUIRED' });
          formErrors[prop].push({ type: 'numeric', message: 'COMMON_ERRORS.NUMBER' });
          formErrors[prop].push({
            type: 'nonPositive',
            message: 'COMMON_ERRORS.NUMBER_NO_POSITIVE',
          });
          formErrors[prop].push({ type: 'arrayNumeric', message: 'COMMON_ERRORS.ARRAY_NUMBER' });
          formErrors[prop].push({
            type: 'maxlength',
            message: 'COMMON_ERRORS.MAX_LENGTH',
            params: { maxLength: itemConfig.validators?.maxlength },
          });
          formErrors[prop].push({
            type: 'minlength',
            message: 'COMMON_ERRORS.MIN_LENGTH',
            params: { minLength: itemConfig.validators?.minlength },
          });
          formErrors[prop].push({
            type: 'min',
            message: 'COMMON_ERRORS.MIN_VALUE',
            params: { min: itemConfig.validators?.min },
          });

          formErrors[prop].push({
            type: 'max',
            message: 'COMMON_ERRORS.MAX_VALUE',
            params: { max: itemConfig.validators?.max },
          });

          formErrors[prop].push({
            type: 'promotional_logic',
            message: 'SITES.PROMOTIONS.ERRORS.PROMOTIONAL_LOGIC',
          });

          formErrors[prop].push({ type: 'invalidDate', message: 'COMMON.ERROR_DATE_TIME_INVALID' });

          formErrors[prop].push({ type: 'error_409', message: 'COMMON.ERROR_409' });

          if (itemConfig.validators?.required) {
            // eslint-disable-next-line @typescript-eslint/unbound-method
            validators.push(Validators.required);
          }
          if (itemConfig.validators?.numeric) {
            validators.push(numericValidator);
          }
          if (itemConfig.validators?.numericPositive) {
            validators.push(positiveNumberValidator());
          }
          if (itemConfig.validators?.arrayNumeric) {
            validators.push(arrEveryValidator((e: string) => /^\d+$/.test(e), 'arrayNumeric'));
          }
          if (itemConfig.validators?.maxlength) {
            validators.push(Validators.maxLength(itemConfig.validators.maxlength));
          }
          if (itemConfig.validators?.minlength) {
            validators.push(Validators.minLength(itemConfig.validators.minlength));
          }
          if (itemConfig.validators?.min) {
            validators.push(Validators.min(itemConfig.validators.min));
          }
          if (itemConfig.validators?.max) {
            validators.push(Validators.max(itemConfig.validators.max));
          }
          if (itemConfig.validators?.pattern) {
            formErrors[prop].push({
              type: 'pattern',
              message: itemConfig.validators.pattern.errorTranslateMessage,
            });
            validators.push(Validators.pattern(itemConfig.validators.pattern.expression));
          }
          if (itemConfig.validators?.dateTimeValidator) {
            validators.push(dateTimeValidator());
          }
          if (itemConfig.validators?.promotional_logic) {
            validators.push(promotionalLogicValidation());
          }
        }
        formGroupConfig[prop] = new FormControl(itemConfig.value, {
          validators,
          updateOn: itemConfig.updateOn,
        });
      }
    }

    return {
      form: new FormGroup(formGroupConfig, options),
      errors: formErrors,
    };
  }
}
