import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subject } from 'rxjs';
import { RolesUser, User } from '../../model/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  userSubject = new Subject<User>();

  constructor(private oauthService: OAuthService) {}

  getUser(): User {
    return this.oauthService.getIdentityClaims() as User;
  }

  get user(): User {
    return this.getUser();
  }

  getUserName(): string {
    return this.getUser().given_name || this.getUser().sub || '';
  }

  logOut() {
    return this.oauthService.logOut();
  }

  getRolesUser(): RolesUser[] {
    let roles: any = this.getUser().role;
    if (!Array.isArray(roles)) {
      roles = roles?.split(',');
    }
    return roles || [];
  }

  getAllRoles() {
    const tempRoles: any = RolesUser;
    return Object.keys(RolesUser).map((t) => tempRoles[t]);
  }
}
