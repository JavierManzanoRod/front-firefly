import { AbstractControl } from '@angular/forms';
import { districtCodeAndRangeValidator } from './district-code.validator';

describe('districtCodevalidator', () => {
  it('districtCodeValidator returns valid if value is 85', () => {
    const validatorToTest = districtCodeAndRangeValidator();
    const mockControl = { value: '85' } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeNull();
  });

  it('districtCodeValidator returns valid if value is 12-85', () => {
    const validatorToTest = districtCodeAndRangeValidator();
    const mockControl = { value: '12-85' } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeNull();
  });

  it('districtCodeValidator returns invalid if value is 1-515', () => {
    const validatorToTest = districtCodeAndRangeValidator();
    const mockControl = { value: '1-515' } as AbstractControl;
    const isInvalid = validatorToTest(mockControl);
    expect(isInvalid).toEqual({ invalidDistrict: true });
  });

  it('districtCodeValidator returns invalid if value is 132', () => {
    const validatorToTest = districtCodeAndRangeValidator();
    const mockControl = { value: '132' } as AbstractControl;
    const isInvalid = validatorToTest(mockControl);
    expect(isInvalid).toEqual({ invalidDistrict: true });
  });

  it('districtCodeValidator returns invalid if value is 1a', () => {
    const validatorToTest = districtCodeAndRangeValidator();
    const mockControl = { value: '1a' } as AbstractControl;
    const isInvalid = validatorToTest(mockControl);
    expect(isInvalid).toEqual({ invalidDistrict: true });
  });

  it('districtCodeValidator returns invalid if value is 10-8b', () => {
    const validatorToTest = districtCodeAndRangeValidator();
    const mockControl = { value: '10-8b' } as AbstractControl;
    const isInvalid = validatorToTest(mockControl);
    expect(isInvalid).toEqual({ invalidDistrict: true });
  });
});
