import { AbstractControl, ValidatorFn } from '@angular/forms';

export function promotionalLogicValidation(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (control.value !== '01' && control.value !== '02' && control.value !== '03' && control.value !== '' && control.value !== null) {
      return { promotional_logic: true };
    }
    return null;
  };
}
