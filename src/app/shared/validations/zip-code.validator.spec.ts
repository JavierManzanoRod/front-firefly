import { AbstractControl } from '@angular/forms';
import { zipCodeAndRangeValidator } from './zip-code.validator';

describe('zipCodevalidator', () => {
  it('zipCodeValidator returns valid if value is 21000', () => {
    const validatorToTest = zipCodeAndRangeValidator();
    const mockControl = { value: '21000' } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeNull();
  });

  it('zipCodeValidator returns valid if value is 20100-20391', () => {
    const validatorToTest = zipCodeAndRangeValidator();
    const mockControl = { value: '21000-20391' } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeNull();
  });

  it('zipCodeValidator returns invalid if value is 1531-151568', () => {
    const validatorToTest = zipCodeAndRangeValidator();
    const mockControl = { value: '1531-151568' } as AbstractControl;
    const isInvalid = validatorToTest(mockControl);
    expect(isInvalid).toEqual({ invalidZip: true });
  });

  it('zipCodeValidator returns invalid if value is 3515', () => {
    const validatorToTest = zipCodeAndRangeValidator();
    const mockControl = { value: '3515' } as AbstractControl;
    const isInvalid = validatorToTest(mockControl);
    expect(isInvalid).toEqual({ invalidZip: true });
  });

  it('zipCodeValidator returns invalid if value is 3515a', () => {
    const validatorToTest = zipCodeAndRangeValidator();
    const mockControl = { value: '3515a' } as AbstractControl;
    const isInvalid = validatorToTest(mockControl);
    expect(isInvalid).toEqual({ invalidZip: true });
  });

  it('zipCodeValidator returns invalid if value is 35151-2321b', () => {
    const validatorToTest = zipCodeAndRangeValidator();
    const mockControl = { value: '35151-2321b' } as AbstractControl;
    const isInvalid = validatorToTest(mockControl);
    expect(isInvalid).toEqual({ invalidZip: true });
  });
});
