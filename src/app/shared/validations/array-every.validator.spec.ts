import { AbstractControl } from '@angular/forms';
import { arrEveryValidator } from './array-every.validator';

describe('arrEveryValidator', () => {
  it('arrEveryValidator has to be truy every element if no arguments to the constructor', () => {
    const validatorToTest = arrEveryValidator();
    const mockControl = { value: [1, 1, 1] } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeNull();
  });

  it('arrEveryValidator  output object if one element is falsy', () => {
    const validatorToTest = arrEveryValidator();
    const mockControl = { value: [1, 1, 0] } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeTruthy();
  });
});
