import { AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export function dateTimeValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const val = control.value;
    if (val instanceof Date && isNaN(+val)) {
      return { invalidDate: true };
    }
    return null;
  };
}
