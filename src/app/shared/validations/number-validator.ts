import { AbstractControl, ValidatorFn } from '@angular/forms';

export function numberValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const val = control.value;
    const valAsString = String(control.value);
    if (val == null || valAsString === '') {
      return {};
    } else if (valAsString.indexOf(',') > -1) {
      // eslint-disable-next-line id-blacklist
      return { number: true };
    } else {
      // eslint-disable-next-line id-blacklist
      return isNaN(control.value) ? { number: { value: control.value } } : {};
    }
  };
}
