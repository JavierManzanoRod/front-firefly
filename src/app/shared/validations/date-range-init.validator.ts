import { FormGroup } from '@angular/forms';

export function dateRangeInitValidator(start_key: string = 'start_date', end_key: string = 'end_date') {
  return (form: FormGroup) => {
    const start = form.get(start_key)?.value;
    const end = form.get(end_key)?.value;
    if (start && end) {
      if (start > end) {
        return { invalidRangeDate: true };
      }
    }
    return null;
  };
}
