import { AbstractControl } from '@angular/forms';

export const numericValidator = (control: AbstractControl): { [key: string]: boolean } | null => {
  if (control.value === '' || !control.value) {
    return null;
  }

  if (control.value && control.value.toString().match && control.value.toString().match(/^[0-9]*\.?[0-9]*$/i)) {
    return null;
  }

  return { numeric: true };
};
