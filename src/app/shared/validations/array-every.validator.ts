import { AbstractControl, ValidatorFn } from '@angular/forms';

type PredicateValidatonFn = ((x: any) => boolean) | ((x: any, y: number) => boolean | null);
export function arrEveryValidator(cond: PredicateValidatonFn = (e: any) => !!e, errorKey = 'any'): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const val = control.value;
    let result = val ? val.every(cond) : true;

    if (result) {
      result = null;
    } else {
      result = { [errorKey]: true };
    }
    return result;
  };
}
