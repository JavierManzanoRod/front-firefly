import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function minMaxLengthValidator(minLength: number, maxLength: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;

    if (!control.value) {
      return null;
    }

    if (value.length < minLength || value.length > maxLength) {
      return { invalidLength: true };
    }

    return null;
  };
}
