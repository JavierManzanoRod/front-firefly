import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function zipCodeAndRangeValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;

    if (!control.value) {
      return null;
    }

    if (value.length !== 5 && value.length !== 11) {
      return { invalidZip: true };
    }

    if (value.length > 5 && value[5] !== '-') {
      return { invalidZip: true };
    }

    if (value.length === 5) {
      if (!Number(value.substr(0, 5))) {
        return { invalidZip: true };
      }
    }

    if (value.length === 11) {
      if (!Number(value.substr(0, 5)) || !Number(value.substr(6, 5))) {
        return { invalidZip: true };
      }
    }

    return null;
  };
}
