import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function districtCodeAndRangeValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;

    if (!control.value) {
      return null;
    }

    if (value.length !== 2 && value.length !== 5) {
      return { invalidDistrict: true };
    }

    if (value.length > 2 && value[2] !== '-') {
      return { invalidDistrict: true };
    }

    if (value.length === 2) {
      if (!Number(value.substr(0, 2))) {
        return { invalidDistrict: true };
      }
    }

    if (value.length === 5) {
      if (!Number(value.substr(0, 2)) || !Number(value.substr(3, 2))) {
        return { invalidDistrict: true };
      }
    }

    return null;
  };
}
