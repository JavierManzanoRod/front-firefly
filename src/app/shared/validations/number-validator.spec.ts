import { AbstractControl } from '@angular/forms';
import { numberValidator } from './number-validator';

describe('numberValidator', () => {
  it('numberValidator returns valid if value is zero', () => {
    const validatorToTest = numberValidator();
    const mockControl = { value: 0 } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toEqual({});
  });

  it('numberValidator  returns valid if value is one', () => {
    const validatorToTest = numberValidator();
    const mockControl = { value: 1 } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toEqual({});
  });

  it('numberValidator  returns valid if value is 5.34', () => {
    const validatorToTest = numberValidator();
    const mockControl = { value: 5.34 } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toEqual({});
  });

  it('numberValidator  returns valid if value is 5.34', () => {
    const validatorToTest = numberValidator();
    const mockControl = { value: 'someText' } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeTruthy();
  });

  it('numberValidator  returns valid if value is empty', () => {
    const validatorToTest = numberValidator();
    const mockControl = { value: null } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toEqual({});
  });
});
