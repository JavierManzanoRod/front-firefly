import { AbstractControl, ValidatorFn } from '@angular/forms';

type PredicateValidatonFn = ((x: any) => boolean) | ((x: any, y: number) => boolean | null);

export function arrSomeValidator(
  cond: PredicateValidatonFn = (e: any) => (typeof e === 'boolean' ? true : !!e),
  errorKey = 'some'
): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const val = control.value;
    let result = val.some(cond);

    if (result) {
      result = null;
    } else {
      result = { [errorKey]: true };
    }
    return result;
  };
}
