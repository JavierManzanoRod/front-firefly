import { FormGroup } from '@angular/forms';

export function dateRangeValidator(start_key: string = 'start_date', end_key: string = 'end_date', checkNow = true) {
  return (form: FormGroup) => {
    const now = new Date();
    const start = form.get(start_key)?.value;
    const end = form.get(end_key)?.value;
    if (start && end) {
      if (start > end) {
        return { invalidRangeDate: true };
      }
    }
    if (checkNow && end && end < now) {
      return { endMoreThanNow: true };
    }
    return null;
  };
}
