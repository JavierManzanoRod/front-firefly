import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function checkSpaceValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value: string = control.value;

    if (!control.value) {
      return null;
    }

    if (value.includes(' ')) {
      return { spaceValidator: true };
    }

    return null;
  };
}
