import { AbstractControl } from '@angular/forms';
import { arrSomeValidator } from './array-some.validator';

describe('arrSomeValidator', () => {
  it('arrSomeValidator returns true is some is true', () => {
    const validatorToTest = arrSomeValidator();
    const mockControl = { value: [1, 0, 0] } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeNull();
  });

  it('arrSomeValidator  returns false if every is false', () => {
    const validatorToTest = arrSomeValidator();
    const mockControl = { value: [0, 0, 0] } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeTruthy();
  });
});
