import { AbstractControl } from '@angular/forms';
import { positiveNumberValidator } from './positive-number-validator';

describe('positiveNumberValidator', () => {
  it('positiveNumberValidator returns valid if value is zero', () => {
    const validatorToTest = positiveNumberValidator();
    const mockControl = { value: 0 } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toEqual({});
  });

  it('positiveNumberValidator  returns valid if value is one', () => {
    const validatorToTest = positiveNumberValidator();
    const mockControl = { value: 1 } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toEqual({});
  });

  it('positiveNumberValidator  returns invalid if value is minus one', () => {
    const validatorToTest = positiveNumberValidator();
    const mockControl = { value: -1 } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeTruthy();
  });

  it('positiveNumberValidator  returns valid if value is null', () => {
    const validatorToTest = positiveNumberValidator();
    const mockControl = { value: null } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toEqual({});
  });
});
