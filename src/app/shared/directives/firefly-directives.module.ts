import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoBackDirective } from './goBack/go-back.directive';

@NgModule({
  declarations: [GoBackDirective],
  imports: [CommonModule],
  exports: [GoBackDirective],
})
export class FireflyDirectivesModule {}
