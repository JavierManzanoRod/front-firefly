import { Directive, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Directive({
  selector: '[ffGoBack]',
})
export class GoBackDirective implements OnInit, OnDestroy {
  subscription = new Subscription();

  constructor(private elRef: ElementRef) {}

  ngOnInit() {
    const el = this.elRef.nativeElement;
    this.subscription = fromEvent<PointerEvent>(el.parentNode, 'click', { capture: true })
      .pipe(filter((event: PointerEvent) => event.target === el))
      .subscribe((e: any) => {
        if (history?.state?.goBack) {
          e.stopPropagation();
          e.preventDefault();
          history.back();
        }
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
