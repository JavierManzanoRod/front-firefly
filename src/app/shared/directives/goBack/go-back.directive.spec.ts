import { Component, DebugElement, ElementRef } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { GoBackDirective } from './go-back.directive';

@Component({
  template: `<button type="text" ffGoBack (click)="log()"></button>`,
})
class DummyComponent {
  log() {
    console.log('Clicked');
  }
}

describe('GoBackDirective', () => {
  let component: DummyComponent;
  let fixture: ComponentFixture<DummyComponent>;
  let button: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DummyComponent, GoBackDirective],
    });
    fixture = TestBed.createComponent(DummyComponent);
    component = fixture.componentInstance;
    button = fixture.debugElement.query(By.css('button'));
  });
  it('should create an instance', () => {
    const directive = new GoBackDirective({ nativeElement: { parentNode: 'a' } } as ElementRef);
    expect(directive).toBeTruthy();
  });

  it('should allow method when clicked and no history.state', fakeAsync(() => {
    window.history.pushState(null, '<name>', '<url>');
    fixture.detectChanges();
    spyOn(component, 'log'); //method attached to the click.
    button.nativeElement.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    tick();
    expect(component.log).toHaveBeenCalled();
  }));

  it('should stop method when clicked and  history.state', fakeAsync(() => {
    window.history.pushState({ goBack: true }, '', '');
    fixture.detectChanges();
    spyOn(component, 'log'); //method attached to the click.
    button.nativeElement.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    tick();
    expect(component.log).not.toHaveBeenCalled();
  }));
});
