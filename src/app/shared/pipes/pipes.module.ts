import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderTranslatePipe } from './order-translate.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { cyTranslatePipe } from './cy-translate.pipe';

@NgModule({
  declarations: [OrderTranslatePipe, cyTranslatePipe],
  imports: [CommonModule, TranslateModule],
  exports: [OrderTranslatePipe, cyTranslatePipe],
})
export class PipesModule {}
