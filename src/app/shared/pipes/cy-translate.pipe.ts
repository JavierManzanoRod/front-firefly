/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'cyTranslate',
})
export class cyTranslatePipe implements PipeTransform {
  constructor(private translate: TranslateService) {}

  transform(value: string, args: any): string {
    const translated = this.translate.instant(value);
    const normalized = translated.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    return args.concat(normalized.replaceAll(' ', '-').toLowerCase());
  }
}
