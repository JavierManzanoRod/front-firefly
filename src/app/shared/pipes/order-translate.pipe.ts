import { Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'orderTranslate',
})
export class OrderTranslatePipe implements PipeTransform {
  constructor(private translate: TranslateService) {}

  transform<T>(array: T[], key: string | undefined = undefined): T[] {
    array.sort((a: any, b: any) => {
      const valueA: string = key ? a[key] : a;
      const valueB: string = key ? b[key] : b;
      if (this.translate.instant(valueA) < this.translate.instant(valueB)) return -1;
      else if (this.translate.instant(valueA) > this.translate.instant(valueB)) return 1;
      else return 0;
    });
    return array;
  }
}
