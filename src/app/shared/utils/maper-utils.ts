export type MapperDictionary = { [key: string]: string | number };

export const dictionaryToBidimensionaly = <T>(
  dictionary: MapperDictionary | undefined,
  keyLabel: string,
  valueLabel: string
): T[] | undefined => {
  if (dictionary) {
    return Object.keys(dictionary).map((key: string) => ({ [keyLabel]: key, [valueLabel]: dictionary[key] } as unknown as T));
  }
};

export const bidimensionalyToDictionary = <T>(bidimensional: T[] | undefined, keyLabel: string, valueLabel: string): MapperDictionary => {
  const obj: MapperDictionary = {};
  if (bidimensional) {
    bidimensional.forEach((item) => {
      const dictionary = item as unknown as MapperDictionary;
      obj[dictionary[keyLabel]] = dictionary[valueLabel];
    });
  }

  return obj;
};
