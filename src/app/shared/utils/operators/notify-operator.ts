import { MonoTypeOperatorFunction, of, pipe } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ToastService } from 'src/app/modules/toast/toast.service';

export function notifyRequestStatus<T>(toastService: ToastService, failResponse: T): MonoTypeOperatorFunction<T>;
export function notifyRequestStatus<T>(toastService: ToastService, failResponse: T[]): MonoTypeOperatorFunction<T[]> {
  return pipe(
    tap((prods) => {
      if (Array.isArray(prods) && prods?.length === 0) {
        toastService.warning('COMMON_ERRORS.SEARCH');
      }
    }),
    catchError(() => {
      toastService.error('COMMON_ERRORS.INTERNAL_ERROR');
      return of(failResponse);
    })
  );
}
