export type MapperDictionary<U> = { [key: string]: U };

export const dictionaryToBidimensionaly = <T, U>(
  dictionary: MapperDictionary<U> | undefined,
  keyLabel: string,
  valueLabel: string,
  cleaner?: boolean
): T[] | undefined => {
  if (dictionary) {
    return Object.keys(dictionary)
      .filter((key: string) => !cleaner || dictionary[key] !== null)
      .map((key: string) => ({ [keyLabel]: key, [valueLabel]: dictionary[key] } as unknown as T));
  }
};

export const bidimensionalyToDictionary = <T>(
  bidimensional: T[] | undefined,
  keyLabel: string,
  valueLabel: string
): MapperDictionary<string> => {
  const obj: MapperDictionary<string> = {};
  if (bidimensional) {
    bidimensional.forEach((item) => {
      const dictionary = item as unknown as MapperDictionary<string>;
      obj[dictionary[keyLabel]] = dictionary[valueLabel];
    });
  }

  return obj;
};
