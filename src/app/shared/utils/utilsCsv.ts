import { saveAs } from 'file-saver';

export function downloadFile(file: any[], name = 'file.csv') {
  const blob = new Blob(file, { type: 'text/csv' });
  saveAs(blob, name);
}

export function validatorCsvExtension(file: File) {
  return file.name?.endsWith('.csv');
}

export function validatorCsvSize(file: File) {
  return file.size < 1048576;
}

export function writeCsv(data: any[]) {
  const replacer = (key: any, value: any) => (value === null ? '' : value); // specify how you want to handle null values here
  const header = Object.keys(data[0]);
  const csv = data.map((row) => header.map((fieldName) => JSON.stringify(row[fieldName], replacer)).join(','));
  csv.unshift(header.join(','));
  const csvArray = csv.join('\r\n');
  return csvArray;
}
