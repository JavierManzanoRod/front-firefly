export function clone<T>(x: T): T {
  return JSON.parse(JSON.stringify(x)) as T;
}

export function deepEquals<T>(x: T, y: T): boolean {
  return JSON.stringify(x) === JSON.stringify(y);
}

export function checkDateActive(start_date: string, end_date: string, active: boolean): boolean {
  if (start_date && end_date) {
    const date = new Date();
    const dateStart = new Date(start_date);
    const dateEnd = new Date(end_date);
    return date >= dateStart && date <= dateEnd && active;
  }

  if (start_date && !end_date) {
    const date = new Date();
    const dateStart = new Date(start_date);
    return date >= dateStart && active;
  }

  if (!start_date && end_date) {
    const date = new Date();
    const dateEnd = new Date(end_date);
    return date <= dateEnd && active;
  }

  return active;
}
