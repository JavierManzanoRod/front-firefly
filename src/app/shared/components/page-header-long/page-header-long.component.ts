import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ff-page-header-long',
  templateUrl: './page-header-long.component.html',
  styleUrls: ['./page-header-long.component.scss'],
})
export class PageHeaderLongComponent {
  @Output() breadcrumbClick: EventEmitter<any> = new EventEmitter<any>();
  @Input() breadcrumbLink!: string;
  @Input() breadcrumbTitle!: string;
  @Input() pageTitle!: string;

  constructor(protected router: Router) {}

  breadCrumbClick() {
    if (this.breadcrumbLink) {
      this.router.navigateByUrl(this.breadcrumbLink);
    } else {
      this.breadcrumbClick.emit();
    }
  }
}
