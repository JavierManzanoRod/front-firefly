import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'ff-search-historic',
  templateUrl: './search-historic.component.html',
  styleUrls: ['./search-historic.component.scss'],
})
export class SearchHistoricComponent {
  @Output() eventSearch: EventEmitter<string> = new EventEmitter<string>();
  form: FormGroup;

  constructor(protected fb: FormBuilder) {
    this.form = this.fb.group({
      name: [null],
    });
  }

  submit() {
    this.eventSearch.emit(this.form.value.name);
  }
}
