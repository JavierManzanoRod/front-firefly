import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SearchHistoricComponent } from './search-historic.component';

describe('SearchHistoricComponent', () => {
  let component: SearchHistoricComponent;
  let fixture: ComponentFixture<SearchHistoricComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchHistoricComponent],
      imports: [TranslateModule.forRoot()],
      providers: [FormBuilder],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchHistoricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
