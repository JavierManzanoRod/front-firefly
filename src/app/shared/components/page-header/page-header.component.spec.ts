import { Router } from '@angular/router';
import { PageHeaderComponent } from './page-header.component';

describe('PageHeaderComponent', () => {
  it('it creates ', () => {
    const comp = new PageHeaderComponent(null as unknown as Router);
    expect(comp).toBeDefined();
  });
});
