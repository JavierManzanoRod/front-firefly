import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ff-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent {
  @Output() breadcrumbClick: EventEmitter<any> = new EventEmitter<any>();
  @Input() breadcrumbLink!: string;
  @Input() breadcrumbTitle!: string;
  @Input() pageTitle!: string;
  @Input() newBreadCrumb = false;
  back = history?.state?.goBack ?? false;
  backMessage = history?.state?.backMessage ?? false;

  constructor(protected router: Router) {}

  breadCrumbClick() {
    if (this.breadcrumbLink && !this.back) {
      this.router.navigateByUrl(this.breadcrumbLink);
    } else if (this.back) {
      history.back();
    } else {
      this.breadcrumbClick.emit();
    }
  }
}
