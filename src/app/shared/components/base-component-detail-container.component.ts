import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { IApiService2 } from '@core/base/api.service';
import { UtilsComponent } from '@core/base/utils-component';
import { MayHaveIdName } from '@model/base-api.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable, of, throwError } from 'rxjs';
import { catchError, delay, switchMap, tap } from 'rxjs/operators';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { ToastService } from '../../modules/toast/toast.service';

@Component({
  template: '',
})
export abstract class BaseDetailContainerComponent<T extends MayHaveIdName = any> implements OnInit {
  utils: UtilsComponent = new UtilsComponent();

  public urlListado!: string;
  public detail$!: Observable<T>;
  public detail!: T | undefined;
  public loading = true;
  public id!: string;
  public name!: string | undefined;

  protected trackDetail!: boolean;

  constructor(
    public menuLeft: MenuLeftService,
    public toastService: ToastService,
    @Inject('apiService') public apiService: IApiService2<T>,
    public modalService: BsModalService,
    public router: Router,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(({ id }) => {
      this.id = id;
    });
    this.detail$ = this.route.params.pipe(
      tap(() => (this.loading = true)),
      switchMap(({ id }: Params) => {
        if (id) {
          return this.apiService.detail(id);
        } else {
          return of(this.apiService.emptyRecord ? this.apiService.emptyRecord() : undefined).pipe(delay(250));
        }
      }),
      tap(() => (this.loading = false)),
      tap((detail) => {
        this.detail = detail;
        this.name = detail && detail.name;
      }),
      tap((detail: any) => {
        if (this.trackDetail) {
          this.detail = detail;
        }
      }),
      catchError((err) => {
        this.router.navigateByUrl(this.urlListado).then(() => {
          this.toastService.error('COMMON_ERRORS.NOT_FOUND_DESCRIPTION', 'COMMON_ERRORS.NOT_FOUND_TITLE');
        });
        return throwError(err);
      })
    );
  }

  clone() {
    const next = () => {
      this.navigateBack();
    };
    this.utils.cloneActionModal(this, this.detail as any, next);
  }

  handleSubmitForm(itemToSave: T) {
    let next;
    if (!itemToSave.id || !itemToSave.department_id || !itemToSave.priority) {
      next = () => {
        this.router.navigate([this.urlListado]);
      };
    }
    this.utils.updateOrSaveModal(this, itemToSave, next);
  }
  delete() {
    const next = () => {
      this.navigateBack();
    };
    this.utils.deleteActionModal(this, { id: this.id, name: this.name, priority: this.id, department_id: this.id }, next);
  }

  private navigateBack() {
    if (this.menuLeft.canBack) {
      window.history.back();
    } else {
      this.router.navigate([this.urlListado]);
    }
  }
}
