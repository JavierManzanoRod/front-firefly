import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SearchProvider {
  private _data: any;

  public get data() {
    return this._data || {};
  }

  public set data(d) {
    this._data = d;
  }
}
