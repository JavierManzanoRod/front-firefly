import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { ListSearchComponent } from './list-search.component';
import { SearchProvider } from './providers/search-provider';

// start mocks
@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}
// end mocks

describe('ListSearchComponent', () => {
  let component: ListSearchComponent;
  let fixture: ComponentFixture<ListSearchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule],
      providers: [
        {
          provide: SearchProvider,
          useFactory: () => ({
            data: () => {},
          }),
        },
      ],
      declarations: [ListSearchComponent, TranslatePipeMock],
    });
  });

  it(
    'the input name sets the search and emits a search by name',
    waitForAsync(() => {
      fixture = TestBed.createComponent(ListSearchComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        const input = fixture.debugElement.query(By.css('input'));
        const el = input.nativeElement;
        el.value = 'someValue';
        el.dispatchEvent(new Event('input'));
        expect(fixture.componentInstance.form.value.name).toBe('someValue');
        component.search.subscribe((data) => {
          expect(data.name).toBe('someValue', 'Emits object with name property equal to the field value');
        });

        component.submit();
      });
    })
  );

  it(
    'the clear data emits a search with null',
    waitForAsync(() => {
      fixture = TestBed.createComponent(ListSearchComponent);
      component = fixture.componentInstance;
      component.search.subscribe((data) => {
        expect(data).toBeUndefined('Emits null when clearing the form');
      });
      component.clearData();
    })
  );
});
