import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MayHaveIdName } from '@model/base-api.model';

@Component({
  selector: 'ff-list-search',
  template: `<form [formGroup]="form" (submit)="submit()" class="bg-white rounded box-shadow">
    <div class="page-header row">
      <div class="col-9">
        <em class="icon-search pointer" (click)="submit()"></em>
        <em class="icon-select-down"></em>
        <span class="text-search">{{ label | translate }} </span>
        <input
          class="form-control input-search"
          type="text"
          formControlName="name"
          placeholder="{{ 'COMMON.TYPE_SEARCH_PLACEHOLDER' | translate }}"
        />
      </div>
    </div>
  </form> `,
  styleUrls: ['./list-search.component.scss'],
})
export class ListSearchComponent {
  @Input() label!: string;
  @Output() search: EventEmitter<MayHaveIdName> = new EventEmitter<Partial<MayHaveIdName>>();
  form: FormGroup;

  constructor(protected fb: FormBuilder) {
    this.form = this.fb.group({
      name: [null],
    });
  }

  submit() {
    return this.search.emit({ name: this.form.value.name });
  }

  clearData() {
    this.form.reset();
    this.search.emit(undefined);
  }
}
