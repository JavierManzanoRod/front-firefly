import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Page } from '@model/base-api.model';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

@Component({
  template: '',
})
export class BaseListComponent<T = any> {
  @Input() list!: T[];
  @Input() loading!: boolean;

  @Input() page!: Page;
  @Input() currentPage!: number;
  @Input() currentData?: T;
  @Input() pageSize = 10;

  @Output() delete: EventEmitter<T> = new EventEmitter<T>();
  @Output() pagination: EventEmitter<T> = new EventEmitter<T>();

  maxPageSize = 20;

  constructor() {}

  handleDelete(department: T) {
    this.delete.emit(department);
  }

  handlePageChanged(event: PageChangedEvent) {
    const { page, itemsPerPage } = event;
    if (page !== this.currentPage) {
      this.currentPage = page;
      const data = {
        ...this.currentData,
        page: page - 1,
        size: itemsPerPage,
      } as any;
      this.pagination.emit(data);
    }
  }
}
