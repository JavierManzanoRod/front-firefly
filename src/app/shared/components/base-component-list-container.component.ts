import { Component, Inject, OnInit } from '@angular/core';
import { IApiService2 } from '@core/base/api.service';
import { UtilsComponent } from '@core/base/utils-component';
import { GenericApiRequest, MayHaveIdName } from '@model/base-api.model';
import { Page } from '@model/metadata.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ToastService } from '../../modules/toast/toast.service';

@Component({
  template: '',
})
export abstract class BaseListContainer2Component<T extends MayHaveIdName> implements OnInit {
  public tmpList!: T[];
  public list$!: Observable<T[]>;
  public loading?: boolean;

  searchName!: string;
  page!: Page;
  currentData: any;
  utils: UtilsComponent = new UtilsComponent();

  constructor(@Inject('apiService') public apiService: IApiService2<T>, public modalService: BsModalService, public toast: ToastService) {}

  ngOnInit() {
    this.getDataList(null);
  }

  getDataList(filter: any) {
    this.loading = true;
    this.list$ = this.apiService.list(filter).pipe(
      tap(({ page }) => (this.page = page)),
      tap(({ content }) => (this.tmpList = content)),
      map(({ content }) => content),
      catchError(() => {
        this.page = null as unknown as Page;
        return of(null as unknown as T[]);
      }),
      tap(() => (this.loading = false))
    );
  }

  search(criteria: any) {
    this.searchName = criteria.name;
    this.getDataList(criteria);
  }

  delete(item: T) {
    const next = () => {
      const dataPage = {
        name: this.searchName,
        page: this.page.page_number,
        size: this.page.page_size,
      };
      this.getDataList(dataPage);
    };
    this.utils.deleteActionModal(this, item, next);
  }

  handlePagination($event: any) {
    if ($event.size) {
      const dataToSearch = {
        name: this.searchName,
        page: $event.page - 1,
        size: $event.size,
      };
      this.getDataList(dataToSearch);
    }
  }
}
