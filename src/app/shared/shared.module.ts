import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ToastrModule } from 'ngx-toastr';
import { BreadcrumbModule } from '../modules/breadcrumb/breadcrumb.module';
import { FormErrorModule } from '../modules/form-error/form-error.module';
import { ToastModule } from '../modules/toast/toast.module';
import { BaseListComponent } from './components/base-component-list.component';
import { ListSearchComponent } from './components/list-search/list-search.component';
import { PageHeaderLongComponent } from './components/page-header-long/page-header-long.component';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { SearchHistoricComponent } from './components/search-historic/search-historic.component';
import { FireflyDirectivesModule } from './directives/firefly-directives.module';

const components = [PageHeaderComponent, BaseListComponent, PageHeaderLongComponent, ListSearchComponent, SearchHistoricComponent];

@NgModule({
  imports: [
    CommonModule,
    ToastrModule.forRoot({
      autoDismiss: false,
      timeOut: 2000,
      closeButton: true,
      positionClass: 'toast-top-full-width',
      enableHtml: true,
    }),
    ToastModule,
    RouterModule,
    // ChipsControlModule,
    TranslateModule,
    FormsModule,
    BreadcrumbModule,
    FormErrorModule,
    TimepickerModule,
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    CollapseModule,
    PopoverModule,
    ModalModule.forRoot(),
    NgSelectModule,
    FireflyDirectivesModule,
  ],
  declarations: [...components],
  exports: [CollapseModule, CommonModule, FormsModule, TranslateModule, ToastModule, PopoverModule, FireflyDirectivesModule, ...components],
})
export class SharedModule {}
