/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unused-expressions */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { AttributeTranslations } from 'src/app/catalog/attribute-translation/models/attribute-translation.model';
import { AttributeTranslationService } from 'src/app/catalog/attribute-translation/services/attribute-translation.service';
import { ProductTypeDTO } from 'src/app/catalog/product-type/models/product-type.model';
import { FF_COMPOSER_TREE } from 'src/app/configuration/tokens/configuracion';
import { FF_LANGUAGES } from 'src/app/configuration/tokens/language.token';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { ExplodedTree, ExplodedTreeNodeType } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { clone, deepEquals } from 'src/app/shared/utils/utils';
import {
  EventDrop,
  EventMove,
  FREE_TEXT,
  ItemsTechnical,
  KEYFRONT,
  ListTechnical,
  SECTION_WITH_LINE,
  TechicalCharacteristicsAttributeResponse,
  TechnicalCharacteristicsColumns,
  TechnicalCharacteristicsData,
  TechnicalCharacteristicsDetail,
  TechnicalCharacteristicsLayout,
} from './../../models/technical-characteristics.model';

@Component({
  selector: 'ff-technical-characteristics-config',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './technical-characteristics-config.component.html',
  styleUrls: ['./technical-characteristics-config.component.scss'],
})
export class TechnicalCharacteristicsConfigComponent implements OnInit {
  @Input() data: ProductTypeDTO | null = null;
  @Input() loading!: boolean;
  @Output() eventSubmit = new EventEmitter<any>();
  @Output() eventPublish = new EventEmitter();

  list: ListTechnical[] = [];
  _detail: TechnicalCharacteristicsDetail | null = null;
  get detail(): TechnicalCharacteristicsDetail {
    return this._detail as TechnicalCharacteristicsDetail;
  }

  @Input() set detail(value: TechnicalCharacteristicsDetail) {
    const detail = this.convertDetailToList((this._detail = value));
    this.list = detail.length ? detail : [this.itemDefault()];
    this.listOrginial = clone(this.list);
  }

  edit = {};
  listOrginial: ListTechnical[] = [];
  objTranslation: any = {};
  counter = 0;
  lang = 'es_ES';

  isCollapsedConfig: any = {};
  clear = false;
  filterKey = '';
  arrayToFilter: any[] = [];
  indexToScroll = 0;

  public readonly idSeccionConLinea = SECTION_WITH_LINE;
  succesConvert = true;

  constructor(
    public toast: ToastService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef,
    private serviceAdminTranslation: AttributeTranslationService,
    @Inject(FF_COMPOSER_TREE) private readonly composer: ExplodedTree,
    @Inject(FF_LANGUAGES) public arrayLang: string[]
  ) {}

  ngOnInit(): void {
    this.list = this.list.length ? this.list : [this.itemDefault()];
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.activeRoute.parent });
  }
  get haveSection() {
    return this.list.find((list) => list.children);
  }

  parseAttribute(element: ItemsTechnical): TechicalCharacteristicsAttributeResponse {
    let path = '';
    if (!['/', ''].includes(element?.offer_path || '')) {
      // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
      path += '/Offer' + element.offer_path;
    }

    if (!['/', ''].includes(element?.field_path || '')) {
      path += element.field_path;
    }

    const data: TechicalCharacteristicsAttributeResponse = {
      attribute_type: element.data_type || element.attribute_type || element.element_type || '',
      value: this.objTranslation[element.pathCache || element.data?.visual_path || element.visual_path || ''],
      is_i18n: !!element.is_i18n,
      path: element.data?.visual_path || element.path || path,
      type: element.type || 'ATTRIBUTE',
      name: !['/', ''].includes(element?.offer_path || '') ? '' : element.name || element.node_id,
      composer_type: element.composer_type,
      visual_path: element.data?.visual_path || element.visual_path,
    };

    if (!data.visual_path?.includes('_ISS')) {
      if (!data.path?.startsWith('/Offer') && data.path) {
        data.path = `/Offer${data.path}`;
      }
      delete data.name;
    } else {
      data.path = element.path || path;
    }

    if (element?.parent?.node_type === 'INCLUDE_ENTITY_TYPE' && element.parent?.field_path && element.parent?.field_path !== '/') {
      data.path = `${element.parent?.field_path}${data.path}`;
    }

    if (element.node_type === 'COMPOSITE_FIELD' && element.path === '/' && path) {
      data.path = path;
    }

    if (data.type === 'LABEL') {
      data.label = data.value;
      delete data.value;
    }

    if (element.type === 'COMPOSER') {
      return {
        type: 'COMPOSER',
        composer_type: element.composer_type,
      } as TechicalCharacteristicsAttributeResponse;
    }

    if (data.path === '/') {
      delete data.path;
    }

    if (data.path === '/Offer') {
      console.error(data, 'Se esta intentando enviar solo /Offer');
      delete data.path;
    }

    if (element.id === FREE_TEXT) {
      delete data.path;
      // delete data.visual_path;
    }

    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return (Object.keys(data) as (keyof TechicalCharacteristicsAttributeResponse)[]).reduce(
      (ends: any, key: keyof TechicalCharacteristicsAttributeResponse) => {
        if (data[key] !== '') {
          ends[key] = data[key];
        }
        return ends;
      },
      {} as TechicalCharacteristicsAttributeResponse
    ) as TechicalCharacteristicsAttributeResponse;
  }

  updateItem(data: AttributeTranslations) {
    const path = data.pathCache || data.data?.visual_path || data.visual_path || '';
    this.objTranslation[path] = data.translations;
    this.changeDetectorRef.detectChanges();
  }

  selectFilterData(filterCondition: any) {
    if (this.clear) {
      filterCondition.value = '';
      this.cleanValuesFilter();
    } else {
      this.filterKey = filterCondition.value?.trim();
      if (this.filterKey !== '') {
        this.arrayToFilter = this.findMatchFilter();
        this.clear = !this.clear;
        this.moveFiltering(true, true);
      }
    }
  }

  showLabel(item: ItemsTechnical): string {
    const translate = this.objTranslation[item.pathCache || item.data?.visual_path || item.visual_path || ''];
    if (translate) {
      return translate[this.lang] || translate[this.lang];
    }
    if (item.label instanceof Object) {
      return item.label[this.lang] || item.label[this.lang];
    }
    return item.label;
  }

  findMatchFilter() {
    const aTags: any = document.getElementById('content-list')?.getElementsByTagName('span');

    if (aTags) {
      return Array.from(aTags).filter((element: any, index: number) => {
        if (element.innerText.toLowerCase()?.includes(this.filterKey.toLowerCase())) {
          return element;
        }
      });
    }
    return [];
  }
  moveFiltering(next: boolean, isFirst: boolean = false): void {
    this.scrollItem(next, isFirst);
  }

  scrollItem(next: boolean = true, isFirst: boolean = false) {
    if (!isFirst) {
      this.arrayToFilter[this.indexToScroll]?.setAttribute('style', '');
      if (next) {
        this.indexToScroll === this.arrayToFilter?.length - 1 ? (this.indexToScroll = 0) : this.indexToScroll++;
      } else {
        this.indexToScroll === 0 ? (this.indexToScroll = this.arrayToFilter?.length - 1) : this.indexToScroll--;
      }
    }
    this.arrayToFilter[this.indexToScroll]?.setAttribute('style', 'background-color: #FFF7EA;');
    this.arrayToFilter[this.indexToScroll]?.scrollIntoView({
      behavior: 'smooth',
      block: 'nearest',
      inline: 'center',
    });
  }

  cleanValuesFilter() {
    this.arrayToFilter[this.indexToScroll]?.setAttribute('style', '');
    this.clear = false;
    this.filterKey = '';
    this.arrayToFilter = [];
    this.indexToScroll = 0;
  }

  inputChange(event: any, input: any) {
    if (event.key === 'Enter') {
      this.selectFilterData(input);
    } else {
      this.cleanValuesFilter();
    }
  }

  submit() {
    this.succesConvert = true;
    const list = this.convertListToDetail(this.list);

    if (this.succesConvert) {
      this.eventSubmit.emit(list);
    }
  }

  move(event: EventMove, id: number, root: ListTechnical[]) {
    if (event.id || !event.data.row || (event.data.children && root[id].children)) {
      return false;
    }

    if (!event.data.canMoveRoot) {
      root.splice(id, 0, root.splice(event.data.index, 1)[0]);
    } else {
      // Moviendo del hijo al un root si el destino es el root no se puede
      if ((!event.data.indexRoot && event.data.indexRoot !== 0) || id === event.data.indexRoot) {
        return false;
      }
      const listChildren = this.list[event.data.indexRoot].children as {
        etiquetas: ItemsTechnical[];
        atributos: ItemsTechnical[] | undefined;
      }[];
      const children = clone(listChildren[event.data.index]);
      this.list[event.data.indexRoot].children?.splice(event.data.index, 1);
      root.splice(id, 0, children);
    }
  }

  moveChildren(event: EventMove, id: number, root: ListTechnical[]) {
    if (event.id || !event.data.row) {
      return false;
    }

    if (event.data.children) {
      // muevo el children index por el indexRoot
      // event data children es el de arriba
      // root el de abajo
      const cloneRow = clone(event.data.children[event.data.index]);
      this.list[event.data.indexRoot || 0]?.children?.splice(event.data.index, 1);
      root.splice(id, 0, cloneRow);
      return;
    }

    // Comprobar que no sea seccion con linea, para mover de padre a hijo
    if (event.data.root) {
      if (!this.list[event.data.index].etiquetas.find((e) => e.id === this.idSeccionConLinea)) {
        const children = clone(root[id]);
        root.splice(id, 0, this.list[event.data.index]);
        this.list.splice(event.data.index, 1);
        this.changeDetectorRef.detectChanges();
      } else {
        this.toastMsgError();
      }
    } else {
      root.splice(id, 0, root.splice(event.data.index, 1)[0]);
    }
  }

  collapse(item: ItemsTechnical) {
    item.collapse = !item.collapse;
  }

  collapseAll(open = true) {
    this.list.forEach((l) => (l.collapse = open));
  }

  onDrop(event: any, tag: boolean, index: number) {
    let error = false;
    let tree = false;
    let empty = false;

    if (!event.data || event.data.row || (event.data.id === this.idSeccionConLinea && !tag)) {
      error = true;
    }
    if (!tag && event.data.children) {
      error = true;
      tree = true;
    }

    if (!this.validationsAttribute(this.list[index]) && !tag) {
      error = true;
      empty = true;
    }

    if (error) {
      if (empty) {
        this.toastMsgError();
      } else if (tree) {
        this.toastMsgErrorParent();
      } else {
        this.toastMsgError();
      }
      return;
    }

    if (event.data.move) {
      return;
    }
    this.parseEventDrop(event);

    if (event.data.id === this.idSeccionConLinea && !this.list[index].children) {
      const tempData = JSON.parse(JSON.stringify(this.list[index]));
      this.list[index].etiquetas = [event.data];
      // Es hasta el siguiente seccion con linea
      let tope = this.list.length;
      for (let indexItem = index + 1; indexItem < this.list.length; indexItem++) {
        if (this.list[indexItem].etiquetas.find((e) => e.id === this.idSeccionConLinea)) {
          tope = indexItem;
          break;
        }
      }

      let childrens = this.list.splice(index + 1, tope - 1);
      if (tempData.etiquetas?.length > 0) {
        childrens = [tempData, ...childrens];
      }
      this.list[index].children = [...childrens] || [this.itemDefault()];
    } else {
      this.list[index][this.getKey(tag)]?.push(event.data);
    }
    this.changeDetectorRef.detectChanges();
  }

  validationsAttribute(list: ListTechnical): boolean {
    if (this.getKey(true) === undefined) {
      return false;
    }
    const key = this.getKey(true);
    const safeList: ItemsTechnical[] = list[key] as ItemsTechnical[];
    return safeList.length > 0;
  }

  onDropChildren(event: EventDrop, tag: boolean, children: ListTechnical) {
    if (!event.data || event.data.row || event.data.id === this.idSeccionConLinea) {
      if (event.data.id === this.idSeccionConLinea) {
        this.toastMsgErrorSeccion();
      } else {
        this.toastMsgError();
      }
      return;
    }

    if (event.data.move) {
      return;
    }

    this.parseEventDrop(event);

    if (!this.validationsAttribute(children) && !tag) {
      this.toastMsgErrorAttributes();
      return;
    }

    children[this.getKey(tag)]?.push(event.data);
  }

  toastMsgError() {
    this.toast.error('TECHNICALCHARACTERISTICS.ERROR_EMPTY_LABEL');
  }

  toastMsgErrorSeccion() {
    this.toast.error('TECHNICALCHARACTERISTICS.ERROR_SECTION_WITH_LINE_CHILDREN');
  }

  toastMsgErrorParent() {
    this.toast.error('TECHNICALCHARACTERISTICS.ERROR_PARENT');
  }

  toastMsgErrorAttributes() {
    this.toast.error('TECHNICALCHARACTERISTICS.ERROR_ATTRIBUTES');
  }

  getKey(tag: boolean): 'etiquetas' | 'atributos' {
    return tag ? 'etiquetas' : 'atributos';
  }

  handleDelete(list: ListTechnical[], index: number) {
    list.splice(index, 1);
    if (!list.length) {
      list.push(this.itemDefault());
    }
  }

  addItem(list = this.list) {
    list.push(this.itemDefault());
  }

  publish() {
    if (this.isDirty) {
      this.toast.error('TECHNICALCHARACTERISTICS.CANNOT_PUBLISH');
    } else if (this.checkSectionWithoutValues()) {
      this.toast.error('TECHNICALCHARACTERISTICS.SECTION_WITHOUT_VALUES');
    } else {
      this.eventPublish.emit();
    }
  }

  checkSectionWithoutValues(): boolean {
    return this.list.some((value) => {
      return value.children && value.children.length === 0;
    });
  }

  get isDirty(): false | UICommonModalConfig {
    if (deepEquals(this.list, this.listOrginial)) {
      return false;
    }

    return {
      bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
      name: this.data?.name,
    };
  }

  private parseEventDrop(event: EventDrop) {
    this.counter++;
    const pathCache = this.getPath(event.data.data?.visual_path || `${KEYFRONT}${this.counter}`);
    event.data.path = event.data.path || event.data.offer_path;
    event.data.pathCache = pathCache;
    if (event.data.icon && event.data.id !== FREE_TEXT) {
      event.data = {
        ...event.data,
        composer_type: event.data.id,
        type: 'COMPOSER',
        pathCache: this.getPath(`${KEYFRONT}${this.counter}`),
        data: {
          visual_path: event.data.label,
        },
      };
    }

    if (event.data.id === FREE_TEXT) {
      event.data.pathCache = this.getPath(`${KEYFRONT}${this.counter}`);
    }
    // parent node_type "INCLUDE_ENTITY_TYPE" sera iss y necesitara el name y path supongo
    if (event.data?.parent) {
      if (event.data.parent.node_type === ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE) {
        event.data = {
          ...event.data,
          path: event.data.parent.node_id,
          name: event.data.node_id || event.data.attribute_id || '',
        };
      }
    }
    // parent node_type "INCLUDE_ENTITY_TYPE" sera iss y necesitara el name y path supongo
    if (event.data?.parent) {
      if (event.data.parent.node_type === ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE) {
        let path = `/${event.data.label}`;
        const name = event.data.value?.node_id_iss || event.data.parent?.value?.node_id_iss || event.data.node_id || '';
        if (event.data?.parent?.value?.namePath && event.data.data?.visual_path) {
          const label = clone(event.data.data?.visual_path || '');
          const arr = label.split(event.data?.parent?.value?.namePath || '');
          path = arr.length > 1 ? arr[1] : arr[0];
        }
        event.data = {
          ...event.data,
          name,
          path,
        };
      }
    }

    if (event.data.parent_data_type && ['ENTITY', 'EMBEDDED'].includes(event.data.parent_data_type)) {
      event.data.field_path = `/${event.data.name}`;
      event.data.name = event.data.attribute_id || event.data.name;
    }
    const key = pathCache;

    if (event.data.id === SECTION_WITH_LINE || event.data.id === FREE_TEXT) {
      this.objTranslation[event.data.pathCache] = {
        es_ES: event.data.label,
      };
      event.data.label = {
        es_ES: event.data.label,
      };
      return;
    }

    if (event.data.type !== 'COMPOSER') {
      event.data.loading = true;
      this.serviceAdminTranslation
        .detail(key)
        .pipe(
          finalize(() => {
            event.data.loading = false;
            this.changeDetectorRef.detectChanges();
          }),
          catchError((err) => {
            event.data.label = {
              es_ES: event.data.label,
            };
            this.objTranslation[key] = event.data.label;
            return throwError(err);
          })
        )
        .subscribe((v) => {
          event.data.canPut = true;
          event.data.label = this.objTranslation[key];
          this.objTranslation[key] = v.translations;
        });
    }
  }

  private convertColumns(items: ListTechnical, sectionWithLine = false): TechnicalCharacteristicsColumns[] {
    const arr = [
      {
        type: 'COLUMN',
        items: items.etiquetas?.map((etiqueta: any) => {
          const cloneEtiqueta = clone(etiqueta);
          if (sectionWithLine && cloneEtiqueta.composer_type === this.idSeccionConLinea) {
            cloneEtiqueta.type = 'LABEL';
          }
          if (cloneEtiqueta.data_type) {
            cloneEtiqueta.attribute_type = cloneEtiqueta.data_type;
          }
          cloneEtiqueta.type = cloneEtiqueta.type || 'LABEL';

          if (cloneEtiqueta.type === 'LABEL') {
            return {
              type: cloneEtiqueta.type,
              label: this.objTranslation[cloneEtiqueta.pathCache || cloneEtiqueta.data?.visual_path],
              visual_path: cloneEtiqueta.data?.visual_path || cloneEtiqueta.visual_path,
            };
          }

          if (cloneEtiqueta.type === 'ATTRIBUTE') {
            delete cloneEtiqueta.label;
          }

          if (cloneEtiqueta.type === 'COMPOSER') {
            return {
              type: 'COMPOSER',
              composer_type: cloneEtiqueta.composer_type,
            } as TechicalCharacteristicsAttributeResponse;
          }

          return cloneEtiqueta;
        }),
      },
    ];
    const columns: any = {
      type: 'COLUMN',
      items:
        items.atributos?.map((atributo: any) => {
          return this.parseAttribute(atributo);
        }) || [],
    };
    if (!sectionWithLine && items.atributos && items.atributos.length) {
      arr.push(columns);
    }
    return arr;
  }

  private convertDetailToList(detail: TechnicalCharacteristicsDetail): ListTechnical[] {
    const temp: ListTechnical[] = [];
    detail?.layouts?.forEach((rows, index) => {
      if (rows.type === SECTION_WITH_LINE) {
        const titleEtiquetas = this.parseItems(rows.title?.columns[0].items as TechnicalCharacteristicsData[], true);
        const children: ListTechnical[] = [];
        rows.items?.forEach((rowChildren, indexChildren) => {
          children.push({
            etiquetas: this.parseItems(rowChildren.columns[0]?.items || [], false),
            atributos: this.parseItems(rowChildren.columns[1]?.items as TechnicalCharacteristicsData[], false) || [],
          });
        });
        temp.push({
          etiquetas: titleEtiquetas,
          children,
        });
        return false;
      }

      const etiquetas = this.parseItems(rows.columns ? (rows.columns[0]?.items as TechnicalCharacteristicsData[]) : [], false) || [];
      const atributos = this.parseItems(rows.columns ? (rows.columns[1]?.items as TechnicalCharacteristicsData[]) : [], false) || [];

      temp.push({
        etiquetas,
        atributos,
      });
    });
    return temp;
  }

  private parseItems(items: TechnicalCharacteristicsData[], sectionWithLine = false): any[] {
    return items?.map((value, index) => {
      this.counter++;
      const pathCache = this.getPath(value.visual_path || `${KEYFRONT}${this.counter}`);
      this.objTranslation[pathCache] = value.label || value.value;
      return {
        label: this.objTranslation[pathCache],
        value: this.objTranslation[pathCache],
        type: value.type,
        attribute_type: value.attribute_type,
        loading: false,
        composer_type: value.composer_type,
        id: sectionWithLine ? value.composer_type || SECTION_WITH_LINE : value.composer_type,
        icon: this.composer.children?.find((compositor) => compositor.id === value.composer_type)?.icon,
        name: value.name,
        visual_path: value.visual_path || this.composer.children?.find((compositor) => compositor.id === value.composer_type)?.label,
        path: value.path,
        pathCache,
        is_i18n: value.is_i18n,
        canPut: true,
      };
    });
  }

  private getPath(path: string): string {
    const split = path?.split('/');
    if (split && split[1] && split[1].toLowerCase().indexOf('/offer/') !== -1) {
      split.splice(1, 1);
      return split.join('/');
    }
    return path || '';
  }

  private itemDefault(): ListTechnical {
    return {
      etiquetas: [],
      atributos: [],
    };
  }

  private convertListToDetail(list: ListTechnical[]): TechnicalCharacteristicsDetail | undefined {
    if (!this.data) {
      return;
    }

    const detail: TechnicalCharacteristicsDetail = {
      name: this.data.name,
      product_type_id: this.data.id,
      layouts: [],
    };
    list.forEach((row) => {
      const sectionWithLine = row.children;
      if (sectionWithLine) {
        if (row.etiquetas.length === 0) {
          this.toast.error('TECHNICALCHARACTERISTICS.ERROR_SECTION_WITH_LINE');
          this.succesConvert = false;
          return false;
        }
        const section: TechnicalCharacteristicsLayout = {
          type: SECTION_WITH_LINE,
          title: {
            type: 'ROW',
            columns: this.convertColumns(row, true),
          },
          items: [],
        };
        row.children?.forEach((rowChildren) => {
          if (rowChildren.etiquetas.length > 0) {
            section.items?.push({
              type: 'ROW',
              columns: this.convertColumns(rowChildren),
            });
          }
        });

        detail.layouts.push(section);

        return false;
      }

      if (row.etiquetas.length > 0) {
        detail.layouts.push({
          type: 'ROW',
          columns: this.convertColumns(row),
        });
      }
    });

    return detail;
  }
}
