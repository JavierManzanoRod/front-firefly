import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { AttributeTranslationService } from 'src/app/catalog/attribute-translation/services/attribute-translation.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { TechnicalCharacteristicsConfigComponent } from './technical-characteristics-config.component';

describe('TechnicalCharacteristicsConfigComponent', () => {
  let component: TechnicalCharacteristicsConfigComponent;
  let fixture: ComponentFixture<TechnicalCharacteristicsConfigComponent>;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TechnicalCharacteristicsConfigComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: Router,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: ActivatedRoute,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: AttributeTranslationService,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: ToastService,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: BsModalService,
          useFactory: () => ({
            error: () => {},
            show: () => {
              return {
                hide: () => {},
                content: {
                  confirm: of({}),
                  cancel: of({}),
                },
              };
            },
          }),
        },
        {
          provide: ChangeDetectorRef,
          useFactory: () => ({
            error: () => {},
          }),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(TechnicalCharacteristicsConfigComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
