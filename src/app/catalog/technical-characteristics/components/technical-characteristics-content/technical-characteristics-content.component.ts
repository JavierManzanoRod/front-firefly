import { ChangeDetectorRef, Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AttributeTranslations } from 'src/app/catalog/attribute-translation/models/attribute-translation.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { UiModalI18nTranslateComponent } from 'src/app/modules/ui-modal-select/components/ui-modal-i18n-translate/ui-modal-i18n-translate.component';
import { UIModalI18NTranslate } from 'src/app/modules/ui-modal-select/models/modal-select.model';
import { FREE_TEXT, ItemsTechnical, ListTechnical, SECTION_WITH_LINE } from '../../models/technical-characteristics.model';

@Component({
  selector: 'ff-technical-characteristics-content',
  templateUrl: './technical-characteristics-content.component.html',
  styleUrls: ['./technical-characteristics-content.component.scss'],
})
export class TechnicalCharacteristicsContentComponent implements OnInit {
  @Input() values: ItemsTechnical[] = [];
  @Input() list: ListTechnical | null = null;
  @Input() etiquetas!: boolean;
  @Input() lang = 'es_ES';
  @Input() filterKey = '';
  @Input() translations: { [key: string]: { [key: string]: string } } = {};
  @Output() updateItem = new EventEmitter<AttributeTranslations>();
  edit = {};
  hiddenTooltip = false;
  sortableOptions = {
    ghostClass: 'ghost',
    chosenClass: 'ghost',
    onStart: () => {
      this.hiddenTooltip = true;
      this.ref.detectChanges();
    },
    onEnd: () => {
      this.hiddenTooltip = false;
      this.ref.detectChanges();
    },
  };

  public readonly idSeccionConLinea = SECTION_WITH_LINE;
  public readonly idTextoLibre = FREE_TEXT;
  private readonly langDefault = 'es_ES';

  constructor(private toast: ToastService, private service: BsModalService, private ref: ChangeDetectorRef) {}

  ngOnInit(): void {}

  getKey(tag: boolean): 'etiquetas' | 'atributos' {
    return tag ? 'etiquetas' : 'atributos';
  }

  remove(children: ListTechnical | null, tag: boolean, indexElement: ItemsTechnical) {
    if (children) {
      const array = children[this.getKey(false)] as ItemsTechnical[];
      if (tag && children[this.getKey(true)]?.length === 1 && array?.length > 0) {
        this.toast.error('TECHNICALCHARACTERISTICS.ERROR_LAST_TAG');
        return false;
      }
      const index = children[this.getKey(tag)]?.findIndex((v) => v === indexElement);
      if (index !== undefined && index >= 0) {
        children[this.getKey(tag)]?.splice(index, 1);
      }
    }
  }

  showLabel(item: ItemsTechnical): string {
    const translate = this.translations[item.pathCache || item.data?.visual_path || item.visual_path || ''];
    if (translate) {
      return translate[this.lang] || translate[this.langDefault];
    }

    if (item.label instanceof Object) {
      return item.label[this.lang] || item.label[this.langDefault];
    }
    return item.label;
  }

  openModal(index: ItemsTechnical) {
    const value = index;
    const config: UIModalI18NTranslate = {
      title: 'TECHNICALCHARACTERISTICS.I18N',
      items: {
        ...value,
        translations: this.translations[value.pathCache || value.data?.visual_path || value.visual_path || ''] || value.label,
      },
    };
    const modal = this.service.show(UiModalI18nTranslateComponent, {
      initialState: {
        config,
        validatorEs: true,
      },
    });
    const content = modal.content;
    content?.selectItems.asObservable().subscribe((v: AttributeTranslations) => {
      value.label = v.translations || {};
      index.canPut = true;
      config.items.canPut = true;
      index.label = v.translations || {};
      config.items.translations = v.translations || {};
      this.updateItem.emit(config.items);
      modal.hide();
      this.ref.markForCheck();
    });

    content?.cancel.asObservable().subscribe(() => {
      modal.hide();
    });
  }
}
