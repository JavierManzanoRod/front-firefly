import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { ItemsTechnical, ListTechnical } from '../../models/technical-characteristics.model';
import { TechnicalCharacteristicsContentComponent } from './technical-characteristics-content.component';

describe('TechnicalCharacteristicsContentComponent', () => {
  let component: TechnicalCharacteristicsContentComponent;
  let fixture: ComponentFixture<TechnicalCharacteristicsContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TechnicalCharacteristicsContentComponent],
      providers: [
        {
          provide: ToastService,
          useFactory: () => ({
            error: () => ({}),
          }),
        },
        {
          provide: ChangeDetectorRef,
          useFactory: () => ({
            markForCheck: () => ({}),
          }),
        },
        {
          provide: BsModalService,
          useFactory: () => ({
            show: () => {
              return {
                content: {
                  selectItems: {
                    asObservable: () => of({ es_ES: 'español editado', en_US: 'ingles' }),
                  },
                  cancel: {
                    asObservable: () => of({}),
                  },
                },
                hide: () => {},
              };
            },
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalCharacteristicsContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('remove chip', () => {
    const list: ListTechnical = {
      etiquetas: [
        ({
          id: '1',
          label: {
            es_ES: 'test 1',
          },
          type: 'LABEL',
        } as unknown) as ItemsTechnical,
        ({
          id: '2',
          label: {
            es_ES: 'test 2',
          },
          type: 'LABEL',
        } as unknown) as ItemsTechnical,
      ],
    };
    component.remove(list, true, list.etiquetas ? list.etiquetas[0] : ({} as ItemsTechnical));
    expect(list.etiquetas.length).toEqual(1);
  });

  it('remove last chip error', () => {
    const toast = fixture.debugElement.injector.get(ToastService);
    const spy = spyOn(toast, 'error');
    const list: ListTechnical = {
      etiquetas: [
        ({
          id: '1',
          label: {
            es_ES: 'test 1',
          },
          type: 'LABEL',
        } as unknown) as ItemsTechnical,
      ],
      atributos: [
        ({
          id: '2',
          label: {
            es_ES: 'test 2',
          },
          type: 'ATRIBUTE',
        } as unknown) as ItemsTechnical,
      ],
    };
    component.remove(list, true, list.atributos ? list.atributos[0] : ({} as ItemsTechnical));
    expect(spy).toHaveBeenCalled();
  });

  it('show label by lang', () => {
    component.lang = 'es_ES';
    const label = {
      es_ES: 'spain',
      en_US: 'en_US',
    };
    const spain = component.showLabel(({
      label,
    } as unknown) as ItemsTechnical);

    component.lang = 'en_US';
    const en = component.showLabel(({
      label,
    } as unknown) as ItemsTechnical);

    const labelString = component.showLabel(({
      label: 'test',
    } as unknown) as ItemsTechnical);

    expect(spain).toEqual(label.es_ES);
    expect(en).toEqual(label.en_US);
    expect(labelString).toEqual('test');
  });
});
