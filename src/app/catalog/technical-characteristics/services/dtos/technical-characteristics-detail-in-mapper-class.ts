import { dictionaryToBidimensionaly } from 'src/app/shared/utils/mapers-utils';
import {
  KEYFRONT,
  TechnicalCharacteristicsDetail,
  TechnicalCharacteristicsDetailDTO,
  TechnicalCharacteristicsLayout,
  TechnicalCharacteristicsLayoutDTO,
} from '../../models/technical-characteristics.model';

export class TechnicalCharacteristicsDetailIn {
  data = {} as TechnicalCharacteristicsDetailDTO;

  constructor(remoteData: TechnicalCharacteristicsDetail) {
    this.data = {
      id: remoteData.id,
      product_type_id: remoteData.product_type_id,
      name: remoteData.name,
      layouts: this.transform(remoteData.layouts),
    } as unknown as TechnicalCharacteristicsDetailDTO;
  }

  private transform(layouts: TechnicalCharacteristicsLayout[]): TechnicalCharacteristicsLayoutDTO {
    layouts.forEach((layout) => {
      layout.columns?.forEach((column) => {
        column.items?.forEach((item) => {
          if (item.path?.indexOf(KEYFRONT) !== -1) {
            delete item.path;
          }

          if (item.path) {
            // Añadir path en los label y visual_path
            /*delete item.label;
            delete item.value;*/
          }

          if (item.label) {
            item.label = dictionaryToBidimensionaly(item.label, 'locale', 'value');
          }
          if (item.value) {
            item.value = dictionaryToBidimensionaly(item.value, 'locale', 'value');
          }
        });
      });
      if (layout.items) {
        this.transform(layout.items);
      }
      if (layout.title) {
        this.transform([layout.title] as TechnicalCharacteristicsLayoutDTO[]);
      }
    });

    return layouts as unknown as TechnicalCharacteristicsLayoutDTO;
  }
}
