import { bidimensionalyToDictionary } from 'src/app/shared/utils/mapers-utils';
import {
  TechnicalCharacteristicsDetail,
  TechnicalCharacteristicsDetailDTO,
  TechnicalCharacteristicsLayout,
  TechnicalCharacteristicsLayoutDTO,
} from '../../models/technical-characteristics.model';

export class TechnicalCharacteristicsDetailOut {
  data = {} as TechnicalCharacteristicsDetail;

  constructor(data: TechnicalCharacteristicsDetailDTO) {
    this.data = {
      product_type_id: data.product_type_id,
      name: data.name,
      layouts: this.transform(data.layouts),
    } as unknown as TechnicalCharacteristicsDetail;
  }

  private transform(layouts: TechnicalCharacteristicsLayoutDTO[]): TechnicalCharacteristicsLayout {
    const transform = layouts as unknown as TechnicalCharacteristicsLayout[];
    transform.forEach((layout) => {
      layout.columns?.forEach((column) => {
        column.items?.forEach((item) => {
          if (item.label) {
            item.label = bidimensionalyToDictionary(item.label as [], 'locale', 'value');
          }
          if (item.value) {
            item.value = bidimensionalyToDictionary(item.value as [], 'locale', 'value');
          }
        });
      });
      if (layout.title) {
        this.transform([layout.title] as TechnicalCharacteristicsLayoutDTO[]);
      }
      if (layout.items) {
        this.transform(layout.items as TechnicalCharacteristicsLayoutDTO[]);
      }
    });

    return layouts as unknown as TechnicalCharacteristicsLayout;
  }
}
