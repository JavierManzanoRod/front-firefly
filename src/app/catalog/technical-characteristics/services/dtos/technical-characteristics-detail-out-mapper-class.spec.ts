import { TechnicalCharacteristicsDetailDTO } from '../../models/technical-characteristics.model';
import { TechnicalCharacteristicsDetailOut } from './technical-characteristics-detail-out-mapper-class';

describe('technical characteristics out', () => {
  it('parse a outgoing request as expected', () => {
    const remoteItem: TechnicalCharacteristicsDetailDTO = {
      name: 'Lavadoras_ISS',
      layouts: [
        {
          type: 'ROW',
          columns: [
            {
              type: 'COLUMN',
              items: [
                {
                  type: 'LABEL',
                  label: [
                    {
                      locale: 'es_ES',
                      value: 'Temperaturas de lavado',
                    },
                  ],
                },
              ],
            },
            {
              type: 'COLUMN',
              items: [
                {
                  type: 'ATTRIBUTE',
                  name: 'au6lcl7mnq2aig',
                  value: [
                    {
                      locale: 'es_ES',
                      value: 'Temperaturas de lavado',
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          type: 'ROW',
          columns: [
            {
              type: 'COLUMN',
              items: [
                {
                  type: 'LABEL',
                  label: [
                    {
                      locale: 'es_ES',
                      value: 'Medidas',
                    },
                  ],
                },
                {
                  type: 'COMPOSER',
                  composer_type: 'OPENING_BRACKETS',
                },
                {
                  type: 'LABEL',
                  label: [
                    {
                      locale: 'es_ES',
                      value: 'Ancho',
                    },
                  ],
                },
                {
                  type: 'COMPOSER',
                  composer_type: 'SYMBOL_X',
                },
                {
                  type: 'LABEL',
                  label: [
                    {
                      locale: 'es_ES',
                      value: 'Alto',
                    },
                  ],
                },
                {
                  type: 'COMPOSER',
                  composer_type: 'SYMBOL_X',
                },
                {
                  type: 'LABEL',
                  label: [
                    {
                      locale: 'es_ES',
                      value: 'Fondo',
                    },
                  ],
                },
                {
                  type: 'COMPOSER',
                  composer_type: 'CLOSING_BRACKETS',
                },
              ],
            },
            {
              type: 'COLUMN',
              items: [
                {
                  type: 'COMPOSER',
                  composer_type: 'OPENING_BRACKETS',
                },
                {
                  type: 'ATTRIBUTE',
                  name: 'amebkd3cxnn4rg',
                  value: [
                    {
                      locale: 'es_ES',
                      value: 'Ancho',
                    },
                  ],
                },
                {
                  type: 'COMPOSER',
                  composer_type: 'SYMBOL_X',
                },
                {
                  type: 'ATTRIBUTE',
                  name: 'aakpdw4iabvqvw',
                  value: [
                    {
                      locale: 'es_ES',
                      value: 'Alto',
                    },
                  ],
                },
                {
                  type: 'COMPOSER',
                  composer_type: 'SYMBOL_X',
                },
                {
                  type: 'ATTRIBUTE',
                  name: 'ab37wkajv6llhy',
                  value: [
                    {
                      locale: 'es_ES',
                      value: 'Fondo',
                    },
                  ],
                },
                {
                  type: 'COMPOSER',
                  composer_type: 'CLOSING_BRACKETS',
                },
              ],
            },
          ],
        },
      ],
    } as TechnicalCharacteristicsDetailDTO;

    const mapper = new TechnicalCharacteristicsDetailOut(remoteItem);
    const technicalCharacter = mapper.data;
    expect(technicalCharacter.name).toEqual('Lavadoras_ISS');
    expect(technicalCharacter.layouts).toEqual([
      {
        type: 'ROW',
        columns: [
          {
            type: 'COLUMN',
            items: [
              {
                type: 'LABEL',
                label: {
                  es_ES: 'Temperaturas de lavado',
                },
              },
            ],
          },
          {
            type: 'COLUMN',
            items: [
              {
                type: 'ATTRIBUTE',
                name: 'au6lcl7mnq2aig',
                value: {
                  es_ES: 'Temperaturas de lavado',
                },
              },
            ],
          },
        ],
      },
      {
        type: 'ROW',
        columns: [
          {
            type: 'COLUMN',
            items: [
              {
                type: 'LABEL',
                label: {
                  es_ES: 'Medidas',
                },
              },
              {
                type: 'COMPOSER',
                composer_type: 'OPENING_BRACKETS',
              },
              {
                type: 'LABEL',
                label: {
                  es_ES: 'Ancho',
                },
              },
              {
                type: 'COMPOSER',
                composer_type: 'SYMBOL_X',
              },
              {
                type: 'LABEL',
                label: {
                  es_ES: 'Alto',
                },
              },
              {
                type: 'COMPOSER',
                composer_type: 'SYMBOL_X',
              },
              {
                type: 'LABEL',
                label: {
                  es_ES: 'Fondo',
                },
              },
              {
                type: 'COMPOSER',
                composer_type: 'CLOSING_BRACKETS',
              },
            ],
          },
          {
            type: 'COLUMN',
            items: [
              {
                type: 'COMPOSER',
                composer_type: 'OPENING_BRACKETS',
              },
              {
                type: 'ATTRIBUTE',
                name: 'amebkd3cxnn4rg',
                value: {
                  es_ES: 'Ancho',
                },
              },
              {
                type: 'COMPOSER',
                composer_type: 'SYMBOL_X',
              },
              {
                type: 'ATTRIBUTE',
                name: 'aakpdw4iabvqvw',
                value: {
                  es_ES: 'Alto',
                },
              },
              {
                type: 'COMPOSER',
                composer_type: 'SYMBOL_X',
              },
              {
                type: 'ATTRIBUTE',
                name: 'ab37wkajv6llhy',
                value: {
                  es_ES: 'Fondo',
                },
              },
              {
                type: 'COMPOSER',
                composer_type: 'CLOSING_BRACKETS',
              },
            ],
          },
        ],
      },
    ]);
  });
});
