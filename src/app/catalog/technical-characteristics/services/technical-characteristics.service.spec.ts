import { HttpClient } from '@angular/common/http';
import { GenericApiRequest, GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { TechnicalCharacteristicsDetailDTO } from '../models/technical-characteristics.model';
import { TechnicalCharacteristicsService } from './technical-characteristics.services';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: TechnicalCharacteristicsService;
const expectedData: TechnicalCharacteristicsDetailDTO = {
  product_type_id: 'pzz5o6ax6vjcvs',
  name: 'Lavadoras_ISS',
  layouts: [
    {
      type: 'ROW',
      columns: [
        {
          type: 'COLUMN',
          items: [
            {
              type: 'LABEL',
              label: [
                {
                  locale: 'es_ES',
                  value: 'Temperaturas de lavado',
                },
              ],
            },
          ],
        },
        {
          type: 'COLUMN',
          items: [
            {
              type: 'ATTRIBUTE',
              name: 'au6lcl7mnq2aig',
              value: [
                {
                  locale: 'es_ES',
                  value: 'Temperaturas de lavado',
                },
              ],
            },
          ],
        },
      ],
    },
  ],
};

const expectedList: GenericApiResponse<TechnicalCharacteristicsDetailDTO> = {
  content: [expectedData],
  page: (null as unknown) as Page,
};

describe('TechnicalCharacteristicsService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it('TechnicalCharacteristicsService points to the right endpoint', () => {
    myService = new TechnicalCharacteristicsService(null as any, 'v2/');
    expect(myService.endPoint).toEqual('products/backoffice-spec-templates/v2/products/templates');
  });

  it('TechnicalCharacteristicsService.list calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new TechnicalCharacteristicsService(httpClientSpy as any, '');
    myService.list((null as unknown) as GenericApiRequest).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });

  it('TechnicalCharacteristicsService.emptyRecord record gives me some data', () => {
    myService = new TechnicalCharacteristicsService((null as unknown) as HttpClient, '');
    const record = myService.emptyRecord();
    expect(record).toBeDefined();
  });
});
