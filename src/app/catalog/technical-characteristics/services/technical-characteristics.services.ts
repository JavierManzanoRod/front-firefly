import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, UpdateResponse } from '@core/base/api.service';
import { environment } from '@env/environment';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_TECHNICAL_CHARACTERISTICS } from 'src/app/configuration/tokens/api-versions.token';
import { TechnicalCharacteristicsDetail, TechnicalCharacteristicsDetailDTO } from '../models/technical-characteristics.model';
import { TechnicalCharacteristicsDetailIn } from './dtos/technical-characteristics-detail-in-mapper-class';
import { TechnicalCharacteristicsDetailOut } from './dtos/technical-characteristics-detail-out-mapper-class';

@Injectable({
  providedIn: 'root',
})
export class TechnicalCharacteristicsService extends ApiService<TechnicalCharacteristicsDetail> {
  endPoint = `products/backoffice-spec-templates/${this.apiPathVersion}products/templates`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_TECHNICAL_CHARACTERISTICS) private apiPathVersion?: string) {
    super();
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<TechnicalCharacteristicsDetail>> {
    return super.list(filter).pipe(
      map((response) => {
        response.content = response.content.map(
          (res) => new TechnicalCharacteristicsDetailOut(res as unknown as TechnicalCharacteristicsDetailDTO).data
        );
        return response;
      })
    );
  }

  detail(id: string): Observable<TechnicalCharacteristicsDetail> {
    return super
      .detail(id)
      .pipe(map((detail) => new TechnicalCharacteristicsDetailOut(detail as unknown as TechnicalCharacteristicsDetailDTO).data));
  }

  post(data: TechnicalCharacteristicsDetail): Observable<UpdateResponse<TechnicalCharacteristicsDetail>> {
    return super.post(new TechnicalCharacteristicsDetailIn(data).data as unknown as TechnicalCharacteristicsDetail).pipe(
      map((res: UpdateResponse<TechnicalCharacteristicsDetail>) => {
        return {
          ...res,
          data: new TechnicalCharacteristicsDetailOut(res.data as unknown as TechnicalCharacteristicsDetailDTO)
            .data as unknown as TechnicalCharacteristicsDetail,
        };
      })
    );
  }

  update(data: TechnicalCharacteristicsDetail): Observable<UpdateResponse<TechnicalCharacteristicsDetail>> {
    return super.update(new TechnicalCharacteristicsDetailIn(data).data as unknown as TechnicalCharacteristicsDetail).pipe(
      map((res: UpdateResponse<TechnicalCharacteristicsDetail>) => {
        return {
          ...res,
          data: new TechnicalCharacteristicsDetailOut(res.data as unknown as TechnicalCharacteristicsDetailDTO)
            .data as unknown as TechnicalCharacteristicsDetail,
        };
      })
    );
  }

  auditParseEndpoint(endpoint?: string) {
    return `${environment.API_URL_BACKOFFICE}/products/backoffice-spec-templates/${this.apiPathVersion}products-templates-audits/search`;
  }
}
