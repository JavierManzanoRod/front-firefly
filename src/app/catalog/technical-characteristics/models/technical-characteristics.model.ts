import { GenericApiResponse } from '@model/base-api.model';
import { ExplodedTreeNodeType } from 'src/app/modules/exploded-tree/models/exploded-tree.model';

export interface TechnicalCharacteristics {
  name: string;
  id: string;
}

export type TechnicalCharacteristicsResponse = GenericApiResponse<TechnicalCharacteristics>;

export interface ListTechnical {
  etiquetas: ItemsTechnical[];
  children?: {
    etiquetas: ItemsTechnical[];
    atributos?: ItemsTechnical[];
  }[];
  atributos?: ItemsTechnical[];
  collapse?: boolean;
}

export interface ItemsTechnical {
  type: string;
  value?: { [key: string]: string };
  label: { [key: string]: string };
  icon?: string;
  data?: {
    visual_path?: string;
  };
  canPut?: boolean;
  pathCache: string;
  is_i18n?: boolean;
  collapse?: boolean;
  visual_path?: string;
  data_type?: string;
  attribute_id?: string;
  parent_data_type?: string;
  offer_path?: string;
  parent?: any;
  field_path?: string;
  node_id?: string;
  element_type?: string;
  attribute_type?: string;
  path?: string;
  name: string;
  composer_type?: string;
  node_type?: ExplodedTreeNodeType;
  id: string;
}

export interface EventMove {
  data: {
    index: number;
    row: boolean;
    root?: boolean;
    indexRoot?: number;
    children: ListTechnical[];
    canMoveRoot?: boolean;
  };
  id?: string;
}

export interface EventDrop {
  data: ItemsTechnical & {
    row?: boolean;
    move?: boolean;
    label?: any;
    loading?: boolean;
    canPut?: boolean;
    data?: {
      visual_path: string;
    };
    parent?: ItemsTechnical;
  };
  id?: string;
}

export const SECTION_WITH_LINE = 'SECTION_WITH_LINE';
export const FREE_TEXT = 'FREE_TEXT';

export interface TechnicalCharacteristicsDetail {
  product_type_id?: string;
  id?: string;
  name: string;
  layouts: TechnicalCharacteristicsLayout[];
}
export interface TechnicalCharacteristicsDetailDTO {
  product_type_id: string;
  name: string;
  layouts: TechnicalCharacteristicsLayoutDTO[];
}

export interface Label {
  locale: string;
  value: string;
}

export interface Value {
  locale: string;
  value: string;
}

export interface TechnicalCharacteristicsLayout {
  type: string | undefined;
  columns?: TechnicalCharacteristicsColumns[]; // Primer elemento del array para etiqueta y segundo para atributos
  title?: {
    type: string;
    columns: TechnicalCharacteristicsColumns[];
  };
  items?: {
    type: string;
    columns: TechnicalCharacteristicsColumns[];
  }[];
}

export interface TechnicalCharacteristicsLayoutDTO {
  type: string;
  columns?: TechnicalCharacteristicsColumnsDTO[]; // Primer elemento del array para etiqueta y segundo para atributos
  title?: {
    type: string;
    columns: TechnicalCharacteristicsColumnsDTO[];
  };
  items?: {
    type: string;
    columns: TechnicalCharacteristicsColumnsDTO[];
  }[];
}

export interface TechnicalCharacteristicsColumns {
  type: string;
  items?: TechnicalCharacteristicsData[];
}

export interface TechnicalCharacteristicsColumnsDTO {
  type: string;
  items?: TechnicalCharacteristicsDataDTO[];
}

export interface TechnicalCharacteristicsData {
  type: string;
  composer_type?: string;
  // eslint-disable-next-line @typescript-eslint/ban-types
  label?: {};
  name?: string;
  // eslint-disable-next-line @typescript-eslint/ban-types
  value?: {};
  attribute_type?: string;
  is_i18n?: boolean;
  path?: string;
  visual_path?: string;
}

export interface TechnicalCharacteristicsDataDTO {
  type: string;
  composer_type?: string;
  label?: Label[];
  name?: string;
  value?: Value[];
  attribute_type?: string;
  is_i18n?: boolean;
  path?: string;
}

export interface TechicalCharacteristicsAttributeResponse {
  is_i18n: boolean;
  path?: string;
  value: any;
  label?: any;
  type: string;
  visual_path?: string;
  name?: string;
  attribute_type?: string;
  composer_type?: string;
}

export const KEYFRONT = 'Front_Libre';
export const IDCOMPOSITOR = 'compositorRoot';
