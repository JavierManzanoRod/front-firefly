import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ProductTypeApiService } from '../../product-type/services/apis/product-type-api.service';
import { ProductTypeService } from '../../product-type/services/product-type.service';
import { TechnicalCharacteristicsService } from '../services/technical-characteristics.services';
import { TechnicalCharacteristicsDetailContainerComponent } from './technical-characteristics-detail-container.component';

describe('TECHNICAL-CHARACTERISTICS-DETAIL-CONTAINER.COMPONENT', () => {
  let fixture: ComponentFixture<TechnicalCharacteristicsDetailContainerComponent>;
  let component: TechnicalCharacteristicsDetailContainerComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TechnicalCharacteristicsDetailContainerComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: Router,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: HttpClient,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: ProductTypeService,
          useFactory: () => ({
            productTypeAsTree: () => of({}),
          }),
        },
        {
          provide: ProductTypeApiService,
          useFactory: () => ({
            getDetailTechnicalCharacteristics: () => of({}),
            detail: () => of({}),
          }),
        },
        {
          provide: TechnicalCharacteristicsService,
          useFactory: () => ({
            detail: () => of({}),
          }),
        },
        {
          provide: CrudOperationsService,
          useFactory: () => ({
            updateOrSaveModal: () => of({}),
          }),
        },
        {
          provide: ActivatedRoute,
          useFactory: () => ({
            snapshot: {
              paramMap: {
                get: () => {},
              },
            },
          }),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(TechnicalCharacteristicsDetailContainerComponent);
    component = fixture.componentInstance;
  });

  it('init', () => {
    expect(component).toBeTruthy();
  });
});
