import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { TechnicalCharacteristics } from '../models/technical-characteristics.model';
import { ProductTypeApiService } from './../../product-type/services/apis/product-type-api.service';

@Component({
  selector: 'ff-technical-characteristics-list-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<router-outlet></router-outlet>
    <ff-page-header [pageTitle]="'TECHNICALCHARACTERISTICS.HEADER_LIST' | translate"></ff-page-header>
    <div class="page-container">
      <div class="page-scroll-wrapper">
        <div class="page-scroll">
          <div class="page-container-padding">
            <ff-generic-list
              [list]="list$ | async"
              [loading]="loading"
              [page]="page"
              [currentData]="currentData"
              (delete)="delete($event)"
              (pagination)="handlePagination($event)"
              (search)="search($event)"
              [type]="type"
              [title]="''"
              [arrayKeys]="arrayKeys"
              [route]="'/catalog/technical-characteristics'"
              [canDelete]="false"
              [placeHolder]="'TECHNICALCHARACTERISTICS.PLACEHOLDER'"
            ></ff-generic-list>
          </div>
        </div>
      </div>
    </div> `,
})
export class TechnicalCharacteristicsListContainerComponent extends BaseListContainerComponent<TechnicalCharacteristics> implements OnInit {
  type = TypeSearch.simple;
  arrayKeys: GenericListConfig[] = [
    {
      key: 'name',
      headerName: 'TECHNICALCHARACTERISTICS.LIST_NAME',
    },
  ];

  constructor(public apiService: ProductTypeApiService, public crud: CrudOperationsService) {
    super(crud, apiService);
  }
}
