import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ProductTypeResponse } from '../../product-type/models/product-type.model';
import { ProductTypeApiService } from '../../product-type/services/apis/product-type-api.service';
import { TechnicalCharacteristicsListContainerComponent } from './technical-characteristics-list-container.component';

describe('TECHNICALCHARACTERISTICSLIST-CONTAINER', () => {
  let fixture: ComponentFixture<TechnicalCharacteristicsListContainerComponent>;
  let component: TechnicalCharacteristicsListContainerComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TechnicalCharacteristicsListContainerComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: CrudOperationsService,
          useFactory: () => ({}),
        },
        {
          provide: ProductTypeApiService,
          useFactory: () => ({
            list: () => {},
          }),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(TechnicalCharacteristicsListContainerComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('init', () => {
    expect(component).toBeTruthy();
  });

  it('call list', () => {
    const spy = spyOn(component.apiService, 'list').and.returnValue(
      of(({
        page: null,
        content: null,
      } as unknown) as ProductTypeResponse)
    );
    component.getDataList(null);
    component.list$.subscribe();
    expect(spy).toHaveBeenCalled();
  });
});
