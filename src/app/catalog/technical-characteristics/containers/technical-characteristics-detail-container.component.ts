import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UpdateResponse } from '@core/base/api.service';
import { CanComponentDeactivate } from '@core/guards/can-deactivate-guard';
import { finalize } from 'rxjs/operators';
import { FF_COMPOSER_TREE, FF_ENTITY_TYPE_ID_OFFER } from 'src/app/configuration/tokens/configuracion';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { UiModalCommonService } from 'src/app/modules/common-modal/services/ui-modal-common.service';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { ExplodedTree, ExplodedTreeNode, ExplodedTreeNodeType } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { OfferExplodedTreeComponent } from '../../../modules/exploded-tree/components/adapters/offer-exploded-tree/offer-exploded-tree.component';
import { UiStateService } from '../../../shared/services/ui-state.service';
import { ProductTypeDTO } from '../../product-type/models/product-type.model';
import { ProductTypeApiService } from '../../product-type/services/apis/product-type-api.service';
import { ProductTypeTreeService } from '../../product-type/services/product-type-tree.service';
import { TechnicalCharacteristicsConfigComponent } from '../components/technical-characteristics-config/technical-characteristics-config.component';
import { IDCOMPOSITOR, TechnicalCharacteristicsDetail } from '../models/technical-characteristics.model';
import { TechnicalCharacteristicsService } from '../services/technical-characteristics.services';

@Component({
  selector: 'ff-technical-characteristics-detail-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<ff-page-header-long
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'TECHNICALCHARACTERISTICS.BREAD_CRUMB_TITLE' | translate"
    ></ff-page-header-long>
    <ngx-simplebar class="page-container page-container-padding h-100">
      <div class="row fit-row">
        <div class="col-4 h-100 pb-5">
          <ff-offer-exploded-tree
            [dragFilter]="drag.bind(this)"
            [rootCollapsed]="true"
            [staticTrees]="[composerTree]"
            [draggable]="true"
            [filterHidden]="true"
            autoCollapse="false"
            [dataFilter]="dataFilter"
            [showFilter]="show.bind(this)"
            [initialExpandPaths]="['/Offer']"
            [rootAutoCollapse]="false"
            [hiddenBundleOf]="true"
          ></ff-offer-exploded-tree>
        </div>
        <div class="col-8 h-100 pb-5">
          <div class="page-container-long-section-right">
            <div class="row loading" *ngIf="loadingConfig">
              <div class="col-12">
                <div class="text-center pt-3 pb-3">
                  <div class="loader"></div>
                </div>
              </div>
            </div>
            <ff-technical-characteristics-config
              #detailElement
              *ngIf="!loadingConfig"
              [detail]="technicalCharacteristics"
              [loading]="loading"
              [data]="product"
              (eventSubmit)="submit($event)"
              (eventPublish)="publish()"
            ></ff-technical-characteristics-config>
          </div>
        </div>
      </div>
    </ngx-simplebar> `,
  styleUrls: ['./technical-characteristics-detail-container.component.scss'],
  providers: [UiModalCommonService],
})
export class TechnicalCharacteristicsDetailContainerComponent implements OnInit, CanComponentDeactivate {
  @ViewChild('detailElement', { static: false })
  detailElement!: TechnicalCharacteristicsConfigComponent;

  @ViewChild(OfferExplodedTreeComponent) offerTree!: OfferExplodedTreeComponent;
  urlListado = '/catalog/technical-characteristics';
  id: string | null = null;
  loading = true;
  loadingConfig = true;
  product: ProductTypeDTO | null = null;
  technicalCharacteristics: TechnicalCharacteristicsDetail | null = null;

  constructor(
    public productType: ProductTypeTreeService,
    public productTypeApiService: ProductTypeApiService,
    public apiService: TechnicalCharacteristicsService,
    private route: ActivatedRoute,
    private ref: ChangeDetectorRef,
    private utils: CrudOperationsService,
    private uiStateService: UiStateService,
    @Inject(FF_COMPOSER_TREE) public readonly composerTree: ExplodedTree,
    @Inject(FF_ENTITY_TYPE_ID_OFFER) public readonly idOffer: string
  ) {
    this.uiStateService.closeMenuIfWindowSizeSmall();
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    console.log(this.idOffer);
    this.apiService
      .detail(this.id || '')
      .pipe(
        finalize(() => {
          this.loadingConfig = false;
          this.ref.markForCheck();
        })
      )
      .subscribe((v) => {
        this.technicalCharacteristics = v;
      });

    this.productTypeApiService
      .detail(this.id || '')
      .pipe(
        finalize(() => {
          this.loading = false;
          this.ref.markForCheck();
        })
      )
      .subscribe((productBackend) => {
        this.product = productBackend;
        this.offerTree.filterValue = this.product?.name || '';
      });
  }

  submit(event: TechnicalCharacteristicsDetail) {
    if (this.technicalCharacteristics?.product_type_id) {
      delete event.product_type_id;
    }

    this.utils
      .updateOrSaveModal(
        {
          apiService: this.apiService,
          methodToApply: this.technicalCharacteristics?.product_type_id ? 'PUT' : 'POST',
        },
        {
          ...event,
          id: this.technicalCharacteristics?.product_type_id,
        }
      )
      .subscribe((res: UpdateResponse<any>) => {
        this.technicalCharacteristics = res.data;
        this.ref.detectChanges();
      });
  }

  canDeactivate() {
    if (this.detailElement.isDirty) {
      return this.detailElement.isDirty;
    }
    return false;
  }

  drag({ data }: ExplodedTreeNodeComponent) {
    if (
      (data.node_type && [ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE].includes(data.node_type)) ||
      data.id === IDCOMPOSITOR ||
      data.id === this.idOffer
    ) {
      return false;
    }

    return true;
  }

  show(node: ExplodedTreeNode) {
    if (node.node_type === ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE && `exploded_tree||${this.id}` !== node.node_id) {
      return false;
    }

    return true;
  }

  dataFilter(target: ExplodedTreeNodeComponent) {
    return {
      ...target.data,
      parent: target.parent?.data,
      data: {
        visual_path: target.generateVisualPath(),
      },
    };
  }

  publish() {
    if (this.technicalCharacteristics) {
      this.utils
        .publishModal(this.apiService, {
          ...this.technicalCharacteristics,
          id: this.technicalCharacteristics.product_type_id,
        })
        .subscribe((v) => {});
    }
  }
}
