import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { TechnicalCharacteristicsDetailContainerComponent } from './containers/technical-characteristics-detail-container.component';
import { TechnicalCharacteristicsListContainerComponent } from './containers/technical-characteristics-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: TechnicalCharacteristicsListContainerComponent,
    data: {
      header_title: 'TECHNICALCHARACTERISTICS.HEADER_MENU',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
    children: [
      {
        path: ':id',
        component: TechnicalCharacteristicsDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'TECHNICALCHARACTERISTICS.HEADER_MENU',
          breadcrumb: [
            {
              label: '',
              url: '',
            },
          ],
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TechnicalCharacteristicsRoutingModule {}
