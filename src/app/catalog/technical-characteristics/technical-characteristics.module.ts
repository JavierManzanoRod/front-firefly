import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { DndModule } from 'ngx-drag-drop';
import { SortablejsModule } from 'ngx-sortablejs';
import { ToastrModule } from 'ngx-toastr';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { ExplodedTreeModule } from 'src/app/modules/exploded-tree/exploded-tree.module';
import { ToastModule } from 'src/app/modules/toast/toast.module';
import { UiTreeOfferModule } from 'src/app/modules/ui-tree-offer/ui-tree-offer.module';
import { UiTreeModule } from 'src/app/modules/ui-tree/ui-tree.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { UiTreeListModule } from '../../modules/ui-tree-list/ui-tree-list.module';
import { SharedModule } from '../../shared/shared.module';
import { ProductTypeModule } from '../product-type/product-type.module';
import { GenericListComponentModule } from './../../modules/generic-list/generic-list.module';
import { TechnicalCharacteristicsConfigComponent } from './components/technical-characteristics-config/technical-characteristics-config.component';
import { TechnicalCharacteristicsContentComponent } from './components/technical-characteristics-content/technical-characteristics-content.component';
import { TechnicalCharacteristicsDetailContainerComponent } from './containers/technical-characteristics-detail-container.component';
import { TechnicalCharacteristicsListContainerComponent } from './containers/technical-characteristics-list-container.component';
import { TechnicalCharacteristicsRoutingModule } from './technical-characteristics-routing.module';

@NgModule({
  declarations: [
    TechnicalCharacteristicsListContainerComponent,
    TechnicalCharacteristicsDetailContainerComponent,
    TechnicalCharacteristicsConfigComponent,
    TechnicalCharacteristicsContentComponent,
  ],
  exports: [TechnicalCharacteristicsConfigComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    SimplebarAngularModule,
    ChipsControlModule,
    ProductTypeModule,
    CommonModalModule,
    ToastModule,
    PipesModule,
    CoreModule,
    UiTreeOfferModule,
    GenericListComponentModule,
    SortablejsModule.forRoot({ animation: 150 }),
    ToastrModule.forRoot({
      autoDismiss: false,
      timeOut: 2000,
      closeButton: true,
      positionClass: 'toast-top-full-width',
      enableHtml: true,
    }),
    TechnicalCharacteristicsRoutingModule,
    UiTreeModule,
    DndModule,
    NgSelectModule,
    UiTreeListModule,
    TooltipModule,
    ExplodedTreeModule,
  ],
})
export class TechnicalCharacteristicsModule {}
