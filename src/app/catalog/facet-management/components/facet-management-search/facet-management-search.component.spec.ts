import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { FacetSearcherService } from './../../services/facet-searcher.service';
import { FacetManagementSearchComponent } from './facet-management-search.component';

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

describe('FacetManagementSearchComponent', () => {
  let component: FacetManagementSearchComponent;
  let fixture: ComponentFixture<FacetManagementSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FacetManagementSearchComponent, TranslatePipeMock],
      providers: [
        {
          provide: FacetSearcherService,
          useFactory: () => ({
            search: () => {
              return of({});
            },
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FacetManagementSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
