/* eslint-disable @typescript-eslint/no-unsafe-call */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { Facet } from 'src/app/catalog/facet/models/facet.model';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { FacetSearcherService } from '../../services/facet-searcher.service';

@Component({
  selector: 'ff-facet-management-search',
  templateUrl: './facet-management-search.component.html',
  styleUrls: ['./facet-management-search.component.scss'],
})
export class FacetManagementSearchComponent implements OnInit {
  @Input() defaultValue: any;
  @Input() set isCleanSearcher(value: boolean) {
    this._isCleanSeacher = value;
    if (value) {
      this.form?.reset();
      this.listLoad = false;
    }
  }
  @Output() consultAgain = new EventEmitter<boolean>();
  @Output() eventSearch = new EventEmitter<Facet>();
  _isCleanSeacher = false;
  item$!: Observable<any>;
  itemInput$ = new Subject<string>();
  itemLoading!: boolean;
  form!: FormGroup;
  formErrors: any = {};
  typeCode: any;
  listLoad = false;
  collapsed = false;

  configConsultFacets: ChipsControlSelectConfig = {
    label: 'FACETMANAGEMENT.FACET',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'FACETMANAGEMENT.SEARCH_FACET',
      multiple: false,
      itemValueKey: 'identifier',
      searchFnOnInit: false,
      inputFilterPlaceholder: 'FACETMANAGEMENT.INPUT_MODAL',
      filterFn: (val) => val.length > 0,
      searchFn: (criteria: any): any => {
        criteria.name = criteria.name.trim();
        return this.apiFacetSearcher.search(criteria);
      },
    },
  };

  constructor(private apiFacetSearcher: FacetSearcherService) {}

  ngOnInit() {
    if (this.defaultValue) {
      this.apiFacetSearcher.detail(this.defaultValue).subscribe((response) => {
        this.form.patchValue({
          facet: response,
        });
        this.listLoad = true;
      });
    }
    const headerConfigForm = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      facet: {
        value: null,
        validators: {
          required: true,
        },
      },
    });
    this.form = headerConfigForm.form;
    this.formErrors = headerConfigForm.errors;
  }

  cancel() {
    this.form.reset({
      type: 'item_code',
    });
  }

  back() {
    this.form.enable();
    this.listLoad = false;
    this.consultAgain.emit(true);
  }

  search() {
    FormUtilsService.markFormGroupTouched(this.form);
    if (this.form.valid) {
      this.eventSearch.emit(this.form.value.facet);
      this.listLoad = true;
    }
  }
}
