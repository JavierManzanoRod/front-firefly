import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FacetManagementSearchComponent } from './components/facet-management-search/facet-management-search.component';
import { FacetManagementContainerComponent } from './containers/facet-management-container.component';
import { FacetManagementRoutingModule } from './facet-management-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';

@NgModule({
  declarations: [FacetManagementSearchComponent, FacetManagementContainerComponent],
  imports: [
    CommonModule,
    FacetManagementRoutingModule,
    SharedModule,
    ChipsControlModule,
    NgSelectModule,
    CommonModalModule,
    GenericListComponentModule,
    FormErrorModule,
    ReactiveFormsModule,
    SimplebarAngularModule,
  ],
})
export class FacetManagementModule {}
