import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FacetManagementContainerComponent } from './containers/facet-management-container.component';

const routes: Routes = [
  {
    path: '',
    component: FacetManagementContainerComponent,
    data: {
      header_title: 'FACETMANAGEMENT.HEADER_MENU',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: ':id',
    component: FacetManagementContainerComponent,
    data: {
      header_title: 'FACETMANAGEMENT.HEADER_MENU',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: '/facet-management/:id',
    component: FacetManagementContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacetManagementRoutingModule {}
