import { GenericApiRequest } from '@model/base-api.model';
import { AttributeFacetDTO, ProductTypeFacetDTO } from '../../facet/models/facet.model';

export interface FacetManagement {
  attribute_id: string;
  attribute_name: string;
  product_type_id: string;
  product_type_name: string;
  path: string;
  field_path?: string;
  offer_path?: string;
}
export interface FacetManagementDTO {
  attribute: AttributeFacetDTO;
  product_type: ProductTypeFacetDTO;
  path: string;
  field_path?: string;
  offer_path?: string;
}

export interface FacetManagementApiRequest extends GenericApiRequest {
  facet_id?: string;
  filter?: any;
}
