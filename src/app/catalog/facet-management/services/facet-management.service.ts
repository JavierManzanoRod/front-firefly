import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { GenericApiResponse } from '@model/base-api.model';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_FACET_ATTRIBUTES } from 'src/app/configuration/tokens/api-versions.token';
import { Facet, FacetAttributeDTO } from '../../facet/models/facet.model';
import { FacetAttributeInMapper } from '../../facet/services/dto/facet-attribute-in-mapper';
import { FacetManagementApiRequest, FacetManagementDTO } from '../models/facet-management.model';

@Injectable({
  providedIn: 'root',
})
export class FaceManagementService extends ApiService<FacetManagementDTO> {
  endPoint = `products/backoffice-facet-attributes/${this.version}facet-attributes`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_FACET_ATTRIBUTES) private version: string) {
    super();
  }

  list(x: FacetManagementApiRequest) {
    const params: any = x.filter ? x.filter : { size: '10' };

    if (x.facet_id) {
      params.facet_id = x.facet_id;
    }

    return this.http.get<GenericApiResponse<FacetAttributeDTO>>(this.urlBase, { params }).pipe(
      map(({ page, content }) => ({
        page,
        content: content.map((c) => new FacetAttributeInMapper(c).data),
      }))
    ) as Observable<GenericApiResponse<any>>;
  }

  usage(facet: Facet): Observable<FacetAttributeDTO[]> {
    if (facet && (facet.id || facet.identifier)) {
      return this.list({ facet_id: facet.id || facet.identifier }).pipe(
        map((list) => {
          return list.content || [];
        }),
        catchError(() => {
          return of([]);
        })
      );
    } else {
      return of([]);
    }
  }
}
