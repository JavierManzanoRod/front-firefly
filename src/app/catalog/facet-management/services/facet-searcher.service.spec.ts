import { GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { FacetManagement } from '../models/facet-management.model';
import { FacetSearcherService } from './facet-searcher.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: FacetSearcherService;

const expectedData: FacetManagement = {
  attribute_id: '1',
  attribute_name: 'name',
  product_type_id: '123',
  product_type_name: 'asdas',
  path: 'string',
} as FacetManagement;
const expectedList: GenericApiResponse<FacetManagement> = {
  content: [expectedData],
  page: (null as unknown) as Page,
};

const version = 'v1/';

describe('FacetSearcherService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`FacetSearcherService points to products/backoffice-facets/${version}facets`, () => {
    myService = new FacetSearcherService(null as any, version);
    expect(myService.endPoint).toEqual(`products/backoffice-facets/${version}facets`);
  });

  it('FacetSearcherService.search calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new FacetSearcherService(httpClientSpy as any, version);
    myService.search({}).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });
});
