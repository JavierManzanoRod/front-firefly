import { HttpClient } from '@angular/common/http';
import { GenericApiRequest, GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { FacetManagement, FacetManagementApiRequest, FacetManagementDTO } from '../models/facet-management.model';
import { FaceManagementService } from './facet-management.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: FaceManagementService;

const version = 'v1/';
const remoteData = {
  facet_id: 'adad',
};

const expectedData: FacetManagementDTO = {
  attribute: {
    identifier: 'adad',
    name: 'adadd',
  },
  product_type: {
    identifier: 'adad',
    name: 'adadd',
  },
  offer_path: 'asdfa',
  path: 'string',
} as FacetManagementDTO;
const expectedList: GenericApiResponse<FacetManagementDTO> = {
  content: [expectedData],
  page: (null as unknown) as Page,
};

describe('FaceManagementService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`FaceManagementService points to products/backoffice-facet-attributes/${version}facet-attributes`, () => {
    myService = new FaceManagementService((null as unknown) as HttpClient, version);
    expect(myService.endPoint).toEqual(`products/backoffice-facet-attributes/${version}facet-attributes`);
  });

  it('FaceManagementService.list calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new FaceManagementService(httpClientSpy as any, version);
    myService.list(remoteData as FacetManagementApiRequest).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });

  it('FacetManagementService.usage calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new FaceManagementService(httpClientSpy as any, version);
    myService.usage((null as unknown) as GenericApiRequest).subscribe((res) => {
      expect(httpClientSpy.get.calls.count()).toBe(0, '');
      done();
    });
  });
});
