import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { FF_API_PATH_VERSION_FACETS } from 'src/app/configuration/tokens/api-versions.token';
import { FacetManagement } from '../models/facet-management.model';

@Injectable({
  providedIn: 'root',
})
export class FacetSearcherService extends ApiService<FacetManagement> {
  endPoint = `products/backoffice-facets/${this.version}facets`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_FACETS) private version: string) {
    super();
  }

  search(criteria: GenericApiRequest): Observable<GenericApiResponse<FacetManagement>> {
    return this.http.get<GenericApiResponse<FacetManagement>>(this.urlBase, { params: this.getParams(criteria) });
  }

  detail(id: string): Observable<any> {
    return super.detail(id);
  }
}
