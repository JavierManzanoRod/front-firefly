import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Page } from '@model/base-api.model';
import { Observable, of } from 'rxjs';
import { finalize, map, tap } from 'rxjs/operators';
import { GenericListConfig, TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { Facet, FacetAttribute } from '../../facet/models/facet.model';
import { FaceManagementService } from '../services/facet-management.service';

@Component({
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ff-page-header [pageTitle]="'FACETMANAGEMENT.TITLE' | translate"></ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-facet-management-search
        [isCleanSearcher]="isCleanSearcher"
        [defaultValue]="defaultValue"
        (consultAgain)="cleanList($event)"
        (eventSearch)="eventSearch($event)"
      ></ff-facet-management-search>
      <div *ngIf="list">
        <div class="page">
          <div class="page-header row">
            <div class="col title">
              <h3>{{ 'FACETMANAGEMENT.DEFINITION' | translate | uppercase }}</h3>
            </div>
          </div>
          <div class="m-4 p-1">
            <ff-generic-list
              [route]="'/facet-management'"
              [showPagination]="true"
              [arrayKeys]="listConfigs"
              [loading]="loading"
              [currentData]="currentData"
              [page]="page"
              [list]="list | async"
              [canDelete]="false"
              [title]="''"
              [canView]="false"
              [type]="type"
              (pagination)="handlePagination($event)"
            >
            </ff-generic-list>
          </div>
        </div>
      </div>
    </ngx-simplebar>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./facet-management-container.component.scss'],
})
export class FacetManagementContainerComponent {
  currentData: any;
  searchName!: string;
  facet!: Facet;
  type = TypeSearch.none;
  listConfigs: GenericListConfig[] = [
    { key: 'attribute_name', headerName: 'FACETMANAGEMENT.NAME', showOrder: false },
    { key: 'path', headerName: 'FACETMANAGEMENT.PATH', width: '50%' },
  ];
  list: Observable<any[]> | undefined;
  defaultValue: any;
  page!: Page | undefined;
  defaultChip: any;
  filter: any;
  loading!: boolean;
  isCleanSearcher = false;

  constructor(
    public apiService: FaceManagementService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService
  ) {
    if (this.route.snapshot.queryParams.facet_id) {
      this.defaultValue = this.route.snapshot.queryParams.facet_id;
      this.route.queryParams.subscribe((params) => {
        if (params.facet_id) {
          this.list = this.apiService.list({ facet_id: params.facet_id }).pipe(
            tap((response) => {
              this.page = response.page;
              if (this.page.total_elements === 0) {
                this.toast.warning('FACETMANAGEMENT.NO_RESULTS');
              }
            }),

            map((response) => {
              response.content.forEach((item) => {
                item.path = this.getPath(item);
                return item;
              });
              return response.content;
            }),
            finalize(() => (this.loading = false))
          );
        } else {
          this.list = of([]);
        }
      });
    }
  }

  cleanList(event: boolean) {
    if (event) {
      this.list = undefined;
      this.isCleanSearcher = false;
    }
  }

  getPath(item: FacetAttribute) {
    if (item.visual_path) {
      return item.visual_path;
    }
    if (item.product_type_name.endsWith('_ISS')) {
      return `${item.offer_path}/${item.product_type_name}/${item.attribute_name}${item.field_path}`;
    } else {
      return `${item.offer_path}${item.field_path}`;
    }
  }

  eventSearch(facet: Facet, filter?: any) {
    this.facet = facet;
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: facet.id || facet.identifier ? { facet_id: facet.id || facet.identifier } : {},
    });
    this.list = of([]);
    this.loading = true;
    this.isCleanSearcher = false;
    if (facet) {
      this.list = this.apiService.list({ facet_id: facet.id || facet.identifier, filter }).pipe(
        tap((response) => {
          this.page = response.page;
          if (this.page.total_elements === 0) {
            this.toast.warning('FACETMANAGEMENT.NO_RESULTS');
          }
        }),
        map((response) => {
          response.content.forEach((item) => {
            item.path = this.getPath(item);
            return item;
          });
          return response.content;
        }),
        finalize(() => (this.loading = false))
      );
    }
  }

  handlePagination($event: any) {
    if ($event.size) {
      const dataToSearch = {
        ...this.filter,
        page: $event.page - 1,
        size: $event.size,
      };
      this.eventSearch(this.facet, dataToSearch);
    }
  }
}
