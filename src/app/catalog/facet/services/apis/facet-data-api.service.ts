import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, IApiService2, UpdateResponse } from '@core/base/api.service';
import { Observable } from 'rxjs';
import { FF_API_PATH_VERSION_FACETS } from 'src/app/configuration/tokens/api-versions.token';
import { FacetDataDTO } from '../../models/facet.model';
@Injectable({
  providedIn: 'root',
})
export class FacetDataApiService extends ApiService<FacetDataDTO> implements IApiService2<FacetDataDTO> {
  endPoint = `products/backoffice-facets/${this.version}facets`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_FACETS) private version: string) {
    super();
  }

  public saveFacetData(facetData: FacetDataDTO): Observable<UpdateResponse<FacetDataDTO>> {
    return this.post(facetData);
  }
}
