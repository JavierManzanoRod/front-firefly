import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, IApiService2 } from '@core/base/api.service';
import { GenericApiResponse } from '@model/base-api.model';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_FACET_LINKS } from 'src/app/configuration/tokens/api-versions.token';
import { HttpUrlEncodingCodec } from 'src/app/shared/utils/http-url-encoding-codec';
import { FacetLinkDTO, FactetLinkApiRequest } from '../../models/facet.model';

@Injectable({
  providedIn: 'root',
})
export class FacetLinkApiService extends ApiService<FacetLinkDTO> implements IApiService2<FacetLinkDTO> {
  endPoint = `products/backoffice-facet-links/${this.version}facet-links`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_FACET_LINKS) private version: string) {
    super();
  }

  list({ attribute_id, facet_id, field_path, offer_path }: FactetLinkApiRequest) {
    let params: HttpParams = new HttpParams({ encoder: new HttpUrlEncodingCodec() });
    if (attribute_id) {
      params = params.set('attribute_id', attribute_id);
    }
    if (field_path) {
      params = params.set('field_path', field_path);
    }
    if (offer_path) {
      params = params.set('offer_path', offer_path);
    }
    if (facet_id) {
      params = params.set('facet_id', facet_id);
    }
    return this.http.get<GenericApiResponse<FacetLinkDTO>>(this.urlBase, { params });
  }

  relateds(facet_id: string): Observable<number> {
    let params: HttpParams = new HttpParams();
    params = params.set('facet_id', facet_id);
    return this.http.get<GenericApiResponse<FacetLinkDTO>>(this.urlBase, { params }).pipe(
      map((responseApi) => {
        return responseApi.content?.length || 0;
      }),
      catchError(() => {
        return of(0);
      })
    );
  }

  delete({ facetLink_id }: FacetLinkDTO): Observable<any> {
    return this.http.delete<any>(`${this.urlBase}/${facetLink_id}`);
  }
}
