import { HttpClient } from '@angular/common/http';
import { GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { FacetLinkDTO, FactetLinkApiRequest } from '../../models/facet.model';
import { FacetLinkApiService } from './facet-link-data-api.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: FacetLinkApiService;

const version = 'v1/';

const remoteData = {
  facet_id: 'adad',
};

const expectedData: FacetLinkDTO = {
  facet_id: '1',
  attribute_id: 'name',
  field_path: '123',
  offer_path: 'asdas',
} as FacetLinkDTO;
const expectedList: GenericApiResponse<FacetLinkDTO> = {
  content: [expectedData],
  page: null as unknown as Page,
};

describe('FacetLinkApiService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`FacetLinkApiService points to products/backoffice-facet-links/${version}facet-links`, () => {
    myService = new FacetLinkApiService(null as unknown as HttpClient, version);
    expect(myService.endPoint).toEqual(`products/backoffice-facet-links/${version}facet-links`);
  });

  it('FacetLinkApiService.list calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new FacetLinkApiService(httpClientSpy as any, version);
    myService.list(remoteData as FactetLinkApiRequest).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });

  it('FacetLinkApiService.relateds calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new FacetLinkApiService(httpClientSpy as any, version);
    myService.relateds('').subscribe((res) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'One call');
      done();
    });
  });

  it('FacetLinkApiService.delete delete to get api method', (done) => {
    httpClientSpy.delete.and.returnValue(of(null));
    myService = new FacetLinkApiService(httpClientSpy as any, version);
    myService.delete(expectedData).subscribe((res) => {
      expect(httpClientSpy.delete.calls.count()).toBe(1, 'One call');
      done();
    });
  });
});
