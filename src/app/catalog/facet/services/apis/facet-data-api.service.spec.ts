import { HttpClient } from '@angular/common/http';
import { GenericApiResponse, Page } from '@model/base-api.model';
import { FacetDataDTO } from '../../models/facet.model';
import { FacetDataApiService } from './facet-data-api.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: FacetDataApiService;

const remoteData = {
  facet_id: 'adad',
};

const expectedData: FacetDataDTO = {
  name: '1',
  data_type: 'name',
  identifier: '123',
} as FacetDataDTO;
const expectedList: GenericApiResponse<FacetDataDTO> = {
  content: [expectedData],
  page: null as unknown as Page,
};
const version = 'v1/';

describe('FacetDataApiService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`FacetDataApiService points to products/backoffice-facets/${version}facets`, () => {
    myService = new FacetDataApiService(null as unknown as HttpClient, version);
    expect(myService.endPoint).toEqual(`products/backoffice-facets/${version}facets`);
  });
});
