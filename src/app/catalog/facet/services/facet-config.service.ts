import { Injectable } from '@angular/core';
import { IApiService2, UpdateResponse } from '@core/base/api.service';
import { Observable, of, throwError } from 'rxjs';
import { catchError, concatMap, map, switchMap, tap } from 'rxjs/operators';
import { AttributeFacet, Facet, FacetDataDTO, FacetLinkDTO } from '../models/facet.model';
import { FacetDataApiService } from './apis/facet-data-api.service';
import { FacetLinkApiService } from './apis/facet-link-data-api.service';

@Injectable({
  providedIn: 'root',
})
export class FacetConfigService implements IApiService2<Facet> {
  endPoint = '';

  constructor(private facetDataApiService: FacetDataApiService, private facetLinkApiService: FacetLinkApiService) {}

  get defaultPayLoadFacetLink(): Partial<FacetLinkDTO> {
    return {
      field_path: '/',
    };
  }

  list(): any {
    return of(null);
  }

  detail(): any {
    return of(null);
  }

  public getFacetLink(att: AttributeFacet) {
    return this.facetLinkApiService
      .list({ attribute_id: att.idAux || att.id, offer_path: att.offer_path, field_path: att.field_path })
      .pipe(
        map((list) => {
          return (
            (list?.content || []).find(
              (e) => e.attribute_id === att.id && e.field_path === att.field_path && e.offer_path === att.offer_path
            ) || null
          );
        })
      );
  }

  public getFacet(att: AttributeFacet): Observable<Facet | null> {
    // console.log('looking for ', att);
    let facetLinkIdentifier: string | undefined;

    return this.getFacetLink(att).pipe(
      tap((facetLink) => (facetLinkIdentifier = facetLink?.identifier)),
      switchMap((facetLink) => {
        return facetLink ? this.facetDataApiService.detail(facetLink.facet_id) : of(null);
      }),
      map((facetData) => {
        if (facetData) {
          return {
            attribute_id: att.id,
            facetLink_id: facetLinkIdentifier,
            ...facetData,
          } as any;
        }
      }),
      catchError(() => of(null))
    );
  }

  public post(facet: Facet) {
    return this.saveFacet(facet);
  }

  public saveFacetPromise(facet: Facet): Observable<any> {
    let hasOnlyChangeTypeOfFacet = false;
    let obs = of({});
    const facetDataPayLoad: FacetDataDTO = {
      data_type: facet.data_type || '',
      name: facet.name || '',
      id: facet.id,
    };

    if (facet.old_facet && (facet.old_facet.identifier || facet.old_facet.id)) {
      const facetLinkPayloadOldFacet: FacetLinkDTO = {
        ...this.defaultPayLoadFacetLink,
        attribute_id: facet.idAux || facet.attribute_id || '',
        facet_id: facet.old_facet.identifier || facet.old_facet.id || '',
        field_path: facet.field_path,
        offer_path: facet.offer_path,
        visual_path: facet.visual_path,

        facetLink_id: facet.old_facet.facetLink_id,
      };
      obs = obs.pipe(
        concatMap(() => {
          return this.facetLinkApiService.delete(facetLinkPayloadOldFacet);
        })
      );
    }

    if (facet.old_facet) {
      hasOnlyChangeTypeOfFacet = facet.name === facet.old_facet.name && facet.data_type !== facet.old_facet.data_type;
    }

    if (!facet.selected_facet || hasOnlyChangeTypeOfFacet) {
      if (hasOnlyChangeTypeOfFacet) {
        obs = obs.pipe(
          concatMap(() => {
            return this.facetDataApiService.update(facetDataPayLoad);
          })
        );
      } else {
        obs = obs.pipe(
          concatMap(() => {
            return this.facetDataApiService.post({ ...facetDataPayLoad, id: undefined });
          })
        );
      }
    }

    return obs.pipe(
      concatMap((res: any) => {
        const facetLinkPayload = {
          ...this.defaultPayLoadFacetLink,
          attribute_id: facet.idAux || facet.attribute_id || '',
          facet_id: facet.selected_facet ? facet.selected_facet.id || facet.selected_facet.identifier : res ? res.data.identifier : '',
          field_path: facet.field_path,
          offer_path: facet.offer_path,
          visual_path: facet.visual_path,
        };
        return this.facetLinkApiService.post(facetLinkPayload as any).pipe(
          catchError((v) => {
            return throwError(v);
          }),
          map((v: any) => {
            return {
              data: {
                id: v.data.facet_id,
                attribute_id: facet.attribute_id,
                data_type: facet.data_type,
                field_path: facet.field_path,
                offer_path: facet.offer_path,
                name: facet.name,
                facetLink_id: v.data.identifier,
              },
              status: v.status,
            };
          })
        );
      })
    );
  }

  public saveFacet(facet: Facet): Observable<UpdateResponse<Facet>> {
    return this.saveFacetPromise(facet);
  }

  public update(facet: Facet): Observable<UpdateResponse<Facet>> {
    return this.saveFacet(facet);
  }

  delete(facet: FacetLinkDTO): Observable<any> {
    return this.facetLinkApiService.delete(facet);
  }
}
