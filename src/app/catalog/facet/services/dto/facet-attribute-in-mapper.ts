import { FacetAttribute, FacetAttributeDTO } from '../../models/facet.model';

export class FacetAttributeInMapper {
  data = {} as FacetAttribute;

  constructor(remoteData: FacetAttributeDTO) {
    this.data = {
      attribute_id: remoteData.attribute?.identifier,
      attribute_name: remoteData.attribute?.name,
      path: remoteData.path,
      product_type_id: remoteData.product_type?.identifier,
      product_type_name: remoteData.product_type?.name,
      field_path: remoteData.field_path,
      offer_path: remoteData.offer_path,
      visual_path: remoteData.visual_path,
    };
  }
}
