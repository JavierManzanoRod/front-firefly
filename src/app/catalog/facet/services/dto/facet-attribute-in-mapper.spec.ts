import { FacetAttributeDTO } from '../../models/facet.model';
import { FacetAttributeInMapper } from './facet-attribute-in-mapper';

const facetAttribute: FacetAttributeDTO = {
  attribute: {
    identifier: 'attribute_id',
    name: 'attribute_name',
  },
  path: 'path',
  product_type: {
    identifier: 'product_type_id',
    name: 'product_type_name',
  },
  field_path: 'field_path',
  offer_path: 'offer_path',
  visual_path: 'visual_path',
};

describe('Facet Attribute mapper in', () => {
  it('parse a incoming request as expected', () => {
    const mapper = new FacetAttributeInMapper(facetAttribute);

    const facetAttributeDto = mapper.data;
    expect(facetAttributeDto.attribute_name).toEqual('attribute_name');
    expect(facetAttributeDto.attribute_id).toEqual('attribute_id');
    expect(facetAttributeDto.field_path).toEqual('field_path');
    expect(facetAttributeDto.offer_path).toEqual('offer_path');
    expect(facetAttributeDto.visual_path).toEqual('visual_path');
    expect(facetAttributeDto.path).toEqual('path');
    expect(facetAttributeDto.product_type_name).toEqual('product_type_name');
    expect(facetAttributeDto.product_type_id).toEqual('product_type_id');
  });
});
