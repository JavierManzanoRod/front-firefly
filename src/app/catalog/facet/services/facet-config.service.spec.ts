import { of } from 'rxjs';
import { AttributeFacet, Facet, FacetLinkDTO } from '../models/facet.model';
import { FacetDataApiService } from './apis/facet-data-api.service';
import { FacetLinkApiService } from './apis/facet-link-data-api.service';
import { FacetConfigService } from './facet-config.service';

xdescribe('FacetConfigService', () => {
  let serviceToTest: FacetConfigService;
  type api = {
    get: jasmine.Spy;
    put: jasmine.Spy;
    delete: jasmine.Spy;
    post: jasmine.Spy;
    detail: jasmine.Spy;
    update: jasmine.Spy;
    list: jasmine.Spy;
  };

  beforeEach(() => {});

  it('delete only delete on facetLInkApi', () => {
    const facetApi: api = jasmine.createSpyObj('FacetDataApiService', ['get', 'post', 'put', 'delete']);
    const facetLinkApi: api = jasmine.createSpyObj('FacetLinkApiService', ['get', 'post', 'put', 'delete']);
    facetLinkApi.delete.and.returnValue(of(null));

    serviceToTest = new FacetConfigService((facetApi as unknown) as FacetDataApiService, (facetLinkApi as unknown) as FacetLinkApiService);

    const facetToDelete: FacetLinkDTO = {
      identifier: 'id',
      facet_id: '',
      attribute_id: '',
    };
    serviceToTest.delete(facetToDelete);
    expect(facetLinkApi.delete).toHaveBeenCalled();
    const [[argsDelete]] = facetLinkApi.delete.calls.allArgs();

    expect(argsDelete.attribute_id).toBe(facetToDelete.attribute_id);
    expect(argsDelete.facet_id).toBe(facetToDelete.identifier);

    expect(facetApi.delete).not.toHaveBeenCalled();
  });

  it('getFacet', async () => {
    const facetApi: api = jasmine.createSpyObj('FacetDataApiService', ['get', 'post', 'put', 'delete', 'detail']);
    const facetLinkApi: api = jasmine.createSpyObj('FacetLinkApiService', ['get', 'post', 'put', 'delete', 'detail', 'list']);
    facetApi.detail.and.returnValue(of({ attribute_id: 'attribute_id' }));
    facetLinkApi.list.and.returnValue(of({ content: [{ facet_id: 'facet_id' }] }));

    serviceToTest = new FacetConfigService((facetApi as unknown) as FacetDataApiService, (facetLinkApi as unknown) as FacetLinkApiService);

    const facetBackend: Facet = (await serviceToTest.getFacet((null as unknown) as AttributeFacet).toPromise()) as Facet;
    expect(facetLinkApi.list).toHaveBeenCalled();
    expect(facetApi.detail).toHaveBeenCalled();
    expect(facetBackend.attribute_id).toBe('attribute_id');
  });

  it('saveFacet OLD FACET 1', () => {
    const facetApi: api = jasmine.createSpyObj('FacetDataApiService', ['get', 'post', 'put', 'delete', 'detail', 'update']);
    const facetLinkApi: api = jasmine.createSpyObj('FacetLinkApiService', ['get', 'post', 'put', 'delete', 'detail']);
    facetLinkApi.post.and.returnValue(of({ status: 200, data: { facet_id: 'facet_id' } }));
    facetLinkApi.delete.and.returnValue(of(null));
    facetApi.update.and.returnValue(of({ data: { id: 'id' } }));

    serviceToTest = new FacetConfigService((facetApi as unknown) as FacetDataApiService, (facetLinkApi as unknown) as FacetLinkApiService);

    serviceToTest
      .saveFacetPromise({
        id: 'id',
        name: 'pepe',
        data_type: 'typ1',
        old_facet: {
          id: 'old_id',
          name: 'pepe',
          data_type: 'typ2',
        },
      })
      .subscribe((saveResponse) => {
        expect(facetLinkApi.delete).toHaveBeenCalled();
        expect(facetApi.update).toHaveBeenCalled();
        expect(facetLinkApi.post).toHaveBeenCalled();
        expect(saveResponse.data.id).toBe('facet_id');
        expect(saveResponse.data.data_type).toBe('typ1');
        expect(saveResponse.data.name).toBe('pepe');
      });
  });

  it('saveFacet OLD FACET 2', () => {
    const facetApi: api = jasmine.createSpyObj('FacetDataApiService', ['get', 'post', 'put', 'delete', 'detail', 'update']);
    const facetLinkApi: api = jasmine.createSpyObj('FacetLinkApiService', ['get', 'post', 'put', 'delete', 'detail']);
    facetLinkApi.post.and.returnValue(of({ status: 200, data: { facet_id: 'facet_id' } }));
    facetLinkApi.delete.and.returnValue(of(null));
    facetApi.post.and.returnValue(of({ data: { id: 'id' } }));

    serviceToTest = new FacetConfigService((facetApi as unknown) as FacetDataApiService, (facetLinkApi as unknown) as FacetLinkApiService);

    serviceToTest
      .saveFacetPromise({
        id: 'id',
        name: 'pepe',
        data_type: 'typ1',
        old_facet: {
          id: 'old_id',
          name: 'juan',
          data_type: 'typ2',
        },
      })
      .subscribe((saveResponse) => {
        expect(facetLinkApi.delete).toHaveBeenCalled();
        expect(facetApi.post).toHaveBeenCalled();
        expect(facetLinkApi.post).toHaveBeenCalled();
        expect(saveResponse.data.id).toBe('facet_id');
        expect(saveResponse.data.data_type).toBe('typ1');
        expect(saveResponse.data.name).toBe('pepe');
      });
  });

  it('saveFacet SELECTED FACET', async () => {
    const facetApi: api = jasmine.createSpyObj('FacetDataApiService', ['get', 'post', 'put', 'delete', 'detail', 'update']);
    const facetLinkApi: api = jasmine.createSpyObj('FacetLinkApiService', ['get', 'post', 'put', 'delete', 'detail']);
    facetLinkApi.post.and.returnValue(of({ status: 200, data: { facet_id: 'facet_id' } }));
    facetLinkApi.delete.and.returnValue(of(null));
    facetApi.post.and.returnValue(of(null));
    facetApi.update.and.returnValue(of(null));

    serviceToTest = new FacetConfigService((facetApi as unknown) as FacetDataApiService, (facetLinkApi as unknown) as FacetLinkApiService);

    serviceToTest
      .saveFacetPromise({
        id: 'id',
        name: 'pepe',
        data_type: 'typ1',
        selected_facet: {
          id: 'GATITO',
        },
        old_facet: {
          id: 'old_id',
          name: 'juan',
          data_type: 'typ2',
        },
      })
      .subscribe((saveResponse) => {
        expect(facetLinkApi.delete).toHaveBeenCalled();
        expect(facetLinkApi.post).toHaveBeenCalled();
        expect(facetApi.post).not.toHaveBeenCalled();
        expect(facetApi.update).not.toHaveBeenCalled();
        expect(saveResponse.data.id).toBe('facet_id');
        expect(saveResponse.data.data_type).toBe('typ1');
        expect(saveResponse.data.name).toBe('pepe');
      });
  });

  it('saveFacet', () => {
    const facetApi: api = jasmine.createSpyObj('FacetDataApiService', ['get', 'post', 'put', 'delete', 'detail', 'update']);
    const facetLinkApi: api = jasmine.createSpyObj('FacetLinkApiService', ['get', 'post', 'put', 'delete', 'detail']);
    facetLinkApi.post.and.returnValue(of({ status: 200, data: { facet_id: 'facet_id' } }));
    facetLinkApi.delete.and.returnValue(of(null));
    facetApi.post.and.returnValue(of({ data: { id: 'id' } }));
    facetApi.update.and.returnValue(of(null));

    serviceToTest = new FacetConfigService((facetApi as unknown) as FacetDataApiService, (facetLinkApi as unknown) as FacetLinkApiService);

    serviceToTest
      .saveFacetPromise({
        id: 'id',
        name: 'pepe',
        data_type: 'typ1',
      })
      .subscribe((saveResponse) => {
        expect(facetLinkApi.delete).not.toHaveBeenCalled();
        expect(facetApi.post).toHaveBeenCalled();
        expect(facetApi.update).not.toHaveBeenCalled();
        expect(facetLinkApi.post).toHaveBeenCalled();
        expect(saveResponse.data.id).toBe('id_ffacet_idacet');
        expect(saveResponse.data.data_type).toBe('typ1');
        expect(saveResponse.data.name).toBe('pepe');
      });
  });
});
