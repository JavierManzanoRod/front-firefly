import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { FacetDetailContainerComponent } from './facet-detail-container.component';
import { FacetDetailContainerService } from './facet-detail-container.service';

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };
  public get errorMessage() {
    return this.content.errorMessage;
  }
}

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}
  public get(key: any): any {
    of(key);
  }
}
@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

describe('FacetDetailContainerComponent', () => {
  let fixture: ComponentFixture<FacetDetailContainerComponent>;
  let component: FacetDetailContainerComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FacetDetailContainerComponent, TranslatePipeMock],
      imports: [CommonModule, HttpClientModule],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceStub },
        { provide: BsModalService, useClass: ModalServiceMock },
        {
          provide: FacetDetailContainerService,
          useFactory: () => ({
            init: () => {},
            vmFacetConfig$: of({}),
          }),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(FacetDetailContainerComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });
});
