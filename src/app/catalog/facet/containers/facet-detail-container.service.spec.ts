import { of } from 'rxjs';
import { FaceManagementService } from '../../facet-management/services/facet-management.service';
import { FacetDetailContainerService } from './facet-detail-container.service';

xdescribe('FacetDetailContainerServiceComponent', () => {
  it('init', () => {
    const dataServiceServiceSpy = jasmine.createSpyObj('FacetConfigService', {
      getFacet: () => {
        of({});
      },
      relateds: () => {
        of({});
      },
    });

    const a: any = {
      data_type: 'BOOLEAN',
      label: 'test',
      name: 'name',
    };

    const subs = of(a);
    const component = new FacetDetailContainerService(dataServiceServiceSpy, null as unknown as FaceManagementService);
    component.init(subs);

    component.vmFacetConfig$.subscribe(() => {});
    component.usageFacetSubject.next({});
    expect(component).toBeTruthy();
  });
});
