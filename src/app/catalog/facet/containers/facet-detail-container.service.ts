import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { map, share, startWith, switchMap, tap } from 'rxjs/operators';
import { FaceManagementService } from 'src/app/catalog/facet-management/services/facet-management.service';
import { AttributeFacet, Facet, VmFacetDetail } from '../models/facet.model';
import { FacetConfigService } from '../services/facet-config.service';

@Injectable()
export class FacetDetailContainerService {
  public vmFacetConfig$!: Observable<VmFacetDetail>;
  public usageFacetSubject = new Subject<Facet>();

  private selectedFacet$!: Observable<Facet | null>;
  private readonly loadingSubject = new BehaviorSubject<boolean>(false);
  private readonly showConfigSection = new BehaviorSubject<boolean>(false);
  private readonly loadingUsageSubject = new BehaviorSubject<boolean>(false);
  private facetUsage$!: Observable<Facet[]>;

  constructor(private facetConfigService: FacetConfigService, private apiServiceFacetAttribute: FaceManagementService) {}

  init(seletedAttribute$: Observable<AttributeFacet>) {
    this.selectedFacet$ = seletedAttribute$.pipe(
      switchMap((attSelected) => {
        this.loadingSubject.next(true);
        this.loadingUsageSubject.next(true);

        return this.facetConfigService.getFacet(attSelected).pipe(
          tap(() => this.loadingSubject.next(false)),
          tap((facet) => {
            this.usageFacetSubject.next(facet as any);
          })
        );
      })
    );

    this.facetUsage$ = this.usageFacetSubject.asObservable().pipe(
      tap(() => this.loadingUsageSubject.next(true)),
      switchMap((facet) => this.apiServiceFacetAttribute.usage(facet)),
      tap(() => this.loadingUsageSubject.next(false))
    );

    this.vmFacetConfig$ = combineLatest([
      this.selectedFacet$.pipe(startWith({})),
      this.loadingSubject.asObservable(),
      this.showConfigSection.asObservable(),
      this.facetUsage$.pipe(startWith(0)),
      this.loadingUsageSubject.asObservable(),
    ]).pipe(
      map(
        ([selectedFacet, loading, showConfigSection, usageFacet, loadingUsage]) =>
          ({
            selectedFacet,
            loading,
            showConfigSection,
            usageFacet,
            loadingUsage,
          } as any)
      ),
      share() // two components (config and usage) use this observable, so we share data
    );
  }
}
