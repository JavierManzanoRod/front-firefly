import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { UiModalWarningUnChangesService } from 'src/app/modules/common-modal/services/ui-modal-warning-unchanges.service';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import {
  ExplodedClickEvent,
  ExplodedTreeElementType,
  ExplodedTreeNodeType,
} from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { OfferExplodedTreeComponent } from '../../../modules/exploded-tree/components/adapters/offer-exploded-tree/offer-exploded-tree.component';
import { UiStateService } from '../../../shared/services/ui-state.service';
import { FacetConfigComponent } from '../components/facet-config/facet-config.component';
import { AttributeFacet, Facet } from '../models/facet.model';
import { FacetDetailContainerService } from './facet-detail-container.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<ff-page-header-long [pageTitle]="'FACETS.HEADER_TITLE' | translate"></ff-page-header-long>
    <ngx-simplebar class="page-container-long pr-3">
      <div class="row tree">
        <div class="col-4 h-100">
          <ff-offer-exploded-tree
            [iconFilter]="iconFilter"
            [clickFilter]="clickFilter.bind(this)"
            (clickNode)="click($event)"
            [hiddenBundleOf]="true"
          ></ff-offer-exploded-tree>
        </div>
        <div class="col-8 h-100">
          <div class="page-container-long-section-right" [hidden]="hideForm">
            <ff-facet-detail [selectedAttribute]="selectedAttribute$ | async"></ff-facet-detail>
            <ff-facet-config
              [hidden]="hideConfig"
              [selectedAttribute]="selectedAttribute$ | async"
              [facet]="selectedFacet"
              [loading]="loading"
              [showConfigSection]="showConfigSection"
              (dirty)="dirty($event)"
            >
            </ff-facet-config>
          </div>
        </div>
      </div>
    </ngx-simplebar> `,
  styleUrls: ['./facet-detail-container.component.scss'],
  providers: [FacetDetailContainerService],
})
export class FacetDetailContainerComponent {
  @ViewChild(OfferExplodedTreeComponent) explodedeTree!: OfferExplodedTreeComponent;
  @ViewChild(FacetConfigComponent) config!: FacetConfigComponent;
  loading = false;
  showConfigSection = true;
  selectedFacet!: Facet;
  hideForm = true;
  hideConfig = false;
  isDirty = false;
  attrName = '';

  selectedAttribute$ = new Subject<AttributeFacet>();

  constructor(
    private state: FacetDetailContainerService,
    private uiStateService: UiStateService,
    private uiModalWarningUnChangesService: UiModalWarningUnChangesService,
    private cd: ChangeDetectorRef
  ) {
    this.uiStateService.closeMenuIfWindowSizeSmall();
    this.state.init(this.selectedAttribute$);
    this.state.vmFacetConfig$.subscribe((data) => {
      this.loading = data.loading;
      this.selectedFacet = data.selectedFacet;
      this.cd.markForCheck();
    });
    this.selectedAttribute$.subscribe((data) => {
      this.attrName = data.label;
    });
  }

  clickFilter(node: ExplodedTreeNodeComponent) {
    if (
      (node.data.node_type === ExplodedTreeNodeType.ATTRIBUTE && node.data.element_type === ExplodedTreeElementType.ENTITY) ||
      node.data.value?.node_id_iss ||
      (ExplodedTreeElementType.ENTITY === node.data.element_type &&
        node.data.children?.length === 1 &&
        node.data.children[0].node_type === ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE)
    ) {
      node.data.children = [];

      if (node.data?.value?.node_id_iss) {
        node.data.node_id = node.data.value.node_id_iss;
      }

      this.load({ data: node.data, target: node });
      return false;
    }

    return true;
  }

  iconFilter(node: ExplodedTreeNodeComponent) {
    if (
      (node.data.node_type === ExplodedTreeNodeType.ATTRIBUTE && node.data.element_type === ExplodedTreeElementType.ENTITY) ||
      node.data.value?.node_id_iss ||
      (ExplodedTreeElementType.ENTITY === node.data.element_type &&
        node.data.children?.length === 1 &&
        node.data.children[0].node_type === ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE)
    ) {
      return 'icon-agrupaciones';
    }
  }

  dirty(dirty: boolean) {
    this.isDirty = dirty;
  }

  click(event: ExplodedClickEvent<ExplodedTreeNodeComponent>) {
    if (this.isDirty) {
      this.uiModalWarningUnChangesService
        .show({
          bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
          name: this.attrName,
        })
        .subscribe((confirm) => {
          if (confirm) {
            this.isDirty = false;
            this.config.form.markAsPristine();
            this.load(event);
          } else {
            event.target.root?.prevActive?.activate();
            event.target.root?.update();
          }
        });
      return;
    }

    this.load(event);
  }

  load(event: ExplodedClickEvent<ExplodedTreeNodeComponent>) {
    const node: AttributeFacet = {
      id: event.data.node_id,
      label: event.data.label || '',
      name: event.data.label || '',
      data_type: event.data.element_type as any,
      offer_path: event.target.generateOfferPath(),
      field_path: event.target.generateFieldPath() || '/',
      visual_path: event.target.getVisualPath(),
    };

    this.selectedAttribute$.next(node);
    this.hideForm = event.target.isRoot;
    this.hideConfig =
      event.data.node_type === ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE ||
      event.data.node_type === ExplodedTreeNodeType.ATTRIBUTE_TREE_NODE ||
      event.target.isRoot;
  }

  canDeactivate(): UICommonModalConfig | false {
    if (this.isDirty) {
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.attrName,
      };
    }
    return false;
  }
}
