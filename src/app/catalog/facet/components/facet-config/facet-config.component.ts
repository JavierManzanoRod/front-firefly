/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { UpdateResponse } from '@core/base/api.service';
import { UtilsComponent } from '@core/base/utils-component';
import { GenericApiRequest, MayHaveLabel } from '@model/base-api.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { UIModalSelectView } from 'src/app/modules/ui-modal-select/models/modal-select.model';
import { UiModalSelectService } from 'src/app/modules/ui-modal-select/ui-modal-select.service';
import { ToastService } from '../../../../modules/toast/toast.service';
import { FacetDetailContainerService } from '../../containers/facet-detail-container.service';
import { AttributeFacet, Facet, FacetDataDTO } from '../../models/facet.model';
import { FacetDataApiService } from '../../services/apis/facet-data-api.service';
import { FacetConfigService } from '../../services/facet-config.service';

@Component({
  selector: 'ff-facet-config',
  styleUrls: ['facet-config.component.scss'],
  templateUrl: 'facet-config.component.html',
})
export class FacetConfigComponent implements OnInit, OnChanges, OnDestroy {
  @Input() loading = false;
  @Input() showConfigSection = false;
  @Input() facet!: Facet;
  @Input() selectedAttribute!: AttributeFacet;
  @Input() readonly = false;
  @Input() showUse = true;
  @Output()
  formSubmit: EventEmitter<any> = new EventEmitter<any>();
  @Output() delete: EventEmitter<any> = new EventEmitter<any>();
  @Output() dirty: EventEmitter<boolean> = new EventEmitter<boolean>();

  form: FormGroup;
  isCollapsedConfig = false;
  isCollapsedConfigUsage = false;
  facetUsageValue!: number | null;
  subscription = new Subscription();

  types = [
    { label: 'STRING', value: 'STRING' },
    { label: 'INTEGER', value: 'INTEGER' },
    { label: 'FLOAT', value: 'FLOAT' },
    // { label: 'DOUBLE', value: 'DOUBLE' }
  ];
  currentFacet!: Facet;
  facetSelected!: Facet | undefined;
  hasFacetBackend = false;

  utils: UtilsComponent = new UtilsComponent();
  attributePrefix = 'attributes.';
  nameRegex = /^[a-z0-9-_]+$/;
  get hasChanges() {
    const { name, data_type } = this.form.value || {};
    const { name: name_old, data_type: data_type_old } = this.currentFacet || {};
    return !(name === name_old && data_type === data_type_old);
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly facetConfigService: FacetConfigService,
    private readonly facetDataApiService: FacetDataApiService,
    private readonly modalService: BsModalService,
    private readonly toast: ToastService,
    private readonly uiModalSelectService: UiModalSelectService,
    private readonly state: FacetDetailContainerService
  ) {
    this.form = this.fb.group({
      name: [null, { validators: [Validators.required, this.validatorsName.bind(this)] }],
      data_type: ['STRING', { validators: Validators.required }],
    });

    this.form.valueChanges.subscribe(() => {
      if (this.form.dirty) {
        this.dirty.emit(true);
      }
    });

    this.subscription.add(
      this.state.usageFacetSubject.subscribe((facet) => {
        if (facet) {
          this.facet = { ...facet, ...{ facet_id: facet?.identifier } };
          this.form.patchValue({
            name: this.formatName(facet?.name) || null,
            data_type: facet?.data_type || 'STRING',
          });
        } else {
          this.facet = {};
          this.form.patchValue({
            name: null,
            data_type: 'STRING',
          });
        }
      })
    );
  }

  ngOnInit() {
    if (this.readonly) {
      this.form.disable();
    }
  }

  validatorsName(control: AbstractControl): ValidationErrors | null {
    const inp = control.value;
    // Allow numbers, alpahbets, space, underscore
    if (!inp) {
      return null;
    }
    if (!this.nameRegex.exec(inp)) {
      return {
        nameRegex: true,
      };
    }
    return null;
  }

  ngOnChanges(): void {
    this.currentFacet = this.facet;
    this.facetUsageValue = null;
    this.hasFacetBackend = !!this.currentFacet && !!Object.keys(this.currentFacet).length;
    this.form.patchValue({
      name: this.formatName(this.currentFacet?.name) || null,
      data_type: this.currentFacet?.data_type || 'STRING',
    });
    this.form.markAsPristine();
    this.form.markAsUntouched();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  submit(): void {
    this.form.markAllAsTouched();
    this.form.updateValueAndValidity();
    if (this.form.valid) {
      // TODO Setear path (this.relativePathEmbebbed) cuando sepas el nombre de la propiedad
      let facetToSave: Facet;

      if (this.facetSelected) {
        facetToSave = {
          attribute_id: this.selectedAttribute.id,
          idAux: this.selectedAttribute.idAux,
          id: this.facetSelected.id,
          name: this.formatAttributesName(this.facetSelected.name),
          data_type: this.facetSelected.data_type,
          selected_facet: this.facetSelected,
          old_facet: this.currentFacet,
          field_path: this.selectedAttribute.field_path,
          offer_path: this.selectedAttribute.offer_path,
          visual_path: this.selectedAttribute.visual_path,
        };

        if (
          this.form.get('data_type')?.value &&
          this.form.get('data_type')?.value !== this.facetSelected.data_type &&
          !facetToSave.old_facet?.data_type
        ) {
          facetToSave.old_facet = { ...this.form.value, data_type: this.facetSelected.data_type };
          facetToSave.data_type = this.form.get('data_type')?.value;
        }
      } else {
        facetToSave = {
          attribute_id: this.selectedAttribute.id,
          idAux: this.selectedAttribute.idAux,
          id: this.currentFacet ? this.currentFacet.id || this.currentFacet.identifier : undefined,
          old_facet: this.currentFacet,
          field_path: this.selectedAttribute.field_path,
          offer_path: this.selectedAttribute.offer_path,
          visual_path: this.selectedAttribute.visual_path,
          is_facet: true,
          ...this.form.value,
          name: this.formatAttributesName(this.form.value.name),
        };
      }

      this.utils.updateOrSaveModal(
        {
          modalService: this.modalService,
          apiService: this.facetConfigService as any,
          toast: this.toast,
        },
        facetToSave,
        (result: UpdateResponse<Facet>) => {
          if (result.status <= 202) {
            this.facet = this.currentFacet = result.data;
            this.facetSelected = undefined;
            this.hasFacetBackend = true;
            this.state.usageFacetSubject.next(result.data);
            this.form.markAsPristine();
            this.dirty.emit(false);
          }
        },
        (resp: HttpErrorResponse, modal: any, method): string => {
          if (resp.status === 409 && resp.url?.endsWith('facets')) {
            let selectedItem = {
              selected_facet: {
                identifier: resp.error.error.detail.explanation,
              },
              offer_path: this.selectedAttribute.offer_path,
              field_path: this.selectedAttribute.field_path,
              attribute_id: this.selectedAttribute.id,
              visual_path: this.selectedAttribute.visual_path,
              is_facet: true,
            } as Facet;

            modal.content.confirm.pipe(switchMap(() => method(selectedItem))).subscribe((response: any) => {
              if (response.status === 500) {
                modal.hide();
                return this.toast.warning('COMMON_ERRORS.INTERNAL_ERROR');
              }

              if (selectedItem.selected_facet?.identifier) {
                this.facetDataApiService.detail(selectedItem.selected_facet?.identifier).subscribe((data) => {
                  selectedItem = { ...selectedItem, ...data };

                  if (selectedItem.selected_facet && this.form.controls.data_type.value !== selectedItem.data_type) {
                    this.facetDataApiService
                      .update({
                        identifier: selectedItem.selected_facet.identifier,
                        data_type: this.form.controls.data_type.value,
                        name: selectedItem.name,
                      } as FacetDataDTO)
                      .subscribe((facet: UpdateResponse<FacetDataDTO>) => {
                        this.updateForm({ ...selectedItem, ...facet.data });
                        modal.hide();

                        this.toast.success('COMMON.SUCCESS');
                      });
                  } else {
                    this.updateForm({ ...response.data, ...selectedItem });
                    modal.hide();

                    this.toast.success('COMMON.SUCCESS');
                  }
                });
              } else {
                this.form.markAsPristine();
                this.dirty.emit(false);

                modal.hide();
              }
            });

            return 'FACETS.DUPLICATED_FACET';
          }

          return 'COMMON_ERRORS.SAVE_ERROR';
        }
      );
    }
  }

  updateForm(facet: FacetDataDTO) {
    this.form.patchValue(facet);
    this.state.usageFacetSubject.next(facet);
    this.facet = this.currentFacet = facet;
    this.facetSelected = undefined;
    this.hasFacetBackend = true;

    this.form.markAsPristine();
    this.dirty.emit(false);
  }

  select(): void {
    const config: UIModalSelectView = {
      label: 'FACETS.MODAL_SELECT_LABEL',
      searchFn: (x: GenericApiRequest) => this.facetDataApiService.list(x),
      suffixResults: 'FACETS.MODAL_SUFIX_RESULTS',
      title: 'FACETS.MODAL_SELECT_TITLE',
      itemValueKey: 'identifier',
    };

    this.uiModalSelectService.show(config, this.modalService).subscribe(
      (selectedItem: MayHaveLabel | false) => {
        if (selectedItem) {
          this.facetSelected = {
            attribute_id: this.selectedAttribute.id,
            id: selectedItem.id || selectedItem.identifier,
            ...selectedItem,
          };

          this.form.controls.name.setValue(selectedItem.name);
          this.form.controls.data_type.setValue(selectedItem.data_type);
          this.state.usageFacetSubject.next({
            ...selectedItem,
            ...{ facet_id: selectedItem.identifier },
          } as Facet);
        }
      },
      (err) => console.log(err)
    );
  }

  cancel(): void {
    if (this.currentFacet) {
      this.form.patchValue({
        name: this.currentFacet.name,
        data_type: this.currentFacet.data_type,
      });
    }

    this.form.markAsPristine();
    this.form.markAsUntouched();
    this.dirty.next(false);
  }

  deleteFacet(): void {
    const next = () => {
      this.form.patchValue({
        name: null,
        data_type: 'STRING',
      });

      this.facetSelected = undefined;
      this.hasFacetBackend = false;
      this.state.usageFacetSubject.next({} as Facet);
      this.currentFacet = this.facet = {};

      this.form.markAsPristine();
      this.form.markAsUntouched();
      this.dirty.next(false);
    };

    this.utils.deleteActionModal(
      {
        modalService: this.modalService,
        apiService: this.facetConfigService as any,
        toast: this.toast,
      },
      this.currentFacet,
      next
    );
  }

  changed() {
    this.facetSelected = undefined;
  }

  validatePaste(event: ClipboardEvent) {
    const clipboardData = event.clipboardData;
    const pastedText = clipboardData?.getData('text') || '';
    if (this.nameRegex.test(pastedText)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  private formatAttributesName(name: string | undefined) {
    if (!name) {
      return name;
    }
    const prefixedName = name.startsWith(this.attributePrefix) ? name : this.attributePrefix + name;
    return prefixedName.toLowerCase();
  }

  private formatName(name: string | undefined) {
    if (!name) {
      return name;
    }
    return name.startsWith(this.attributePrefix) ? name.slice(this.attributePrefix.length) : name;
  }
}
