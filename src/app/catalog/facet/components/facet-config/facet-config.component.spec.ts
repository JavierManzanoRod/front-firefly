import { FormBuilder } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { UiModalSelectService } from 'src/app/modules/ui-modal-select/ui-modal-select.service';
import { FacetDetailContainerService } from '../../containers/facet-detail-container.service';
import { Facet } from '../../models/facet.model';
import { FacetDataApiService } from '../../services/apis/facet-data-api.service';
import { FacetConfigService } from '../../services/facet-config.service';
import { FacetConfigComponent } from './facet-config.component';

describe('FacetConfigComponent', () => {
  let componentToTest: FacetConfigComponent;

  beforeEach(() => {
    let spyFacetConfigService = jasmine.createSpyObj('FacetConfigService', ['getFacet']);
    componentToTest = new FacetConfigComponent(
      new FormBuilder(),
      null as unknown as FacetConfigService,
      null as unknown as FacetDataApiService,
      null as unknown as BsModalService,
      null as unknown as ToastService,
      null as unknown as UiModalSelectService,
      null as unknown as FacetDetailContainerService
    );

    it('ngOnchanges sets the formvalue with the facet backend data ', () => {
      let facet: Facet = { identifier: 'id', data_type: 'x', id_attribute: 'idatt', name: 'name' };
      spyFacetConfigService.getFacet.and.returnValue(of(facet));
      componentToTest.ngOnChanges();
      expect(componentToTest.form.controls.name.value).toBe(facet.name);
    });

    it('ngOnchanges sets the formvalue with default data if no facet found', () => {
      spyFacetConfigService.getFacet.and.returnValue(of(null));
      componentToTest.ngOnChanges();
      expect(componentToTest.form.controls.name.value).toBeNull();
      expect(componentToTest.form.controls.data_type.value).toBe('STRING');
    });
  });
});
