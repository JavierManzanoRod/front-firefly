import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AttributeFacet, Facet } from '../../models/facet.model';

@Component({
  selector: 'ff-facet-detail',
  styleUrls: ['facet-detail.component.scss'],
  templateUrl: 'facet-detail.component.html',
})
export class FacetDetailComponent implements OnInit {
  @Input() readonly = false;

  form: FormGroup;
  isCollapsedDefinition = false;
  isCollapsedDetail = false;

  @Input()
  public set selectedAttribute(newVal: AttributeFacet) {
    this.form.patchValue({
      att_wpc_key: newVal?.label,
      att_id: newVal?.id,
      field_path: newVal?.field_path,
      offer_path: newVal?.offer_path,
    });
  }

  @Input()
  public set selectedFacet(newVal: Facet) {}

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      att_wpc_key: '',
      att_id: '',
      field_path: '',
      offer_path: '',
    });
  }

  ngOnInit() {
    if (this.readonly) {
      this.form.disable();
    }
  }
}
