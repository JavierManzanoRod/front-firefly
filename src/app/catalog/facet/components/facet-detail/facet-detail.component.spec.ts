import { FormBuilder } from '@angular/forms';
import { FacetDetailComponent } from './facet-detail.component';

describe('FacetDetailComponent', () => {
  it('it should create ', () => {
    const component = new FacetDetailComponent(new FormBuilder());
    expect(component).toBeDefined();
  });
});
