import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { FaceManagementService } from 'src/app/catalog/facet-management/services/facet-management.service';
import { UiModalSelectService } from 'src/app/modules/ui-modal-select/ui-modal-select.service';
import { FacetDetailContainerService } from '../../containers/facet-detail-container.service';
import { FacetUsageComponent } from './facet-usage.component';

describe('FacetUsageComponent', () => {
  it('it takes the fields of the state ', () => {
    const mockState = {
      vmFacetConfig$: of({
        loadingUsage: true,
        usageFacet: [{ test: 'test' }],
      }),
    };
    const component = new FacetUsageComponent(
      mockState as unknown as FacetDetailContainerService,
      null as unknown as UiModalSelectService,
      null as unknown as FaceManagementService,
      null as unknown as BsModalService
    );

    expect(component.loadingUsage).toBe(true);
    expect(component.usageFacets.length).toBe(1);
  });
});
