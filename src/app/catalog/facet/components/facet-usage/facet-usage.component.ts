import { Component, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { map } from 'rxjs/operators';
import { FaceManagementService } from 'src/app/catalog/facet-management/services/facet-management.service';
import { UIModalInfo } from 'src/app/modules/ui-modal-select/models/modal-select.model';
import { UiModalSelectService } from 'src/app/modules/ui-modal-select/ui-modal-select.service';
import { FacetDetailContainerService } from '../../containers/facet-detail-container.service';
import { Facet, FacetAttribute } from '../../models/facet.model';

@Component({
  selector: 'ff-facet-usage',
  styleUrls: ['facet-usage.component.scss'],
  templateUrl: 'facet-usage.component.html',
})
export class FacetUsageComponent {
  @Input() showConfigSection = false; // this component is created once. We hide it to avoid multiple subscriptions.
  @Input() facet!: Facet;
  loadingUsage = false;
  usageFacet = 0;
  usageFacets: FacetAttribute[] = [];
  isCollapsedConfigUsage = false;

  constructor(
    private state: FacetDetailContainerService,
    private uiModalSelectService: UiModalSelectService,
    private apiServiceFacetAttribute: FaceManagementService,
    private readonly modalService: BsModalService
  ) {
    this.state.vmFacetConfig$.subscribe((data) => {
      this.loadingUsage = data.loadingUsage;
      this.usageFacets = data.usageFacet;
      this.usageFacet = this.usageFacets.length;
    });
  }

  consultFacet() {
    const config: UIModalInfo = {
      title: 'FACETS.USAGE.MODAL.TITLE',
      prefix: 'FACETS.USAGE.MODAL.PREFIX',
      headLabels: ['FACETS.USAGE.MODAL.PATH'],
      searchPlaceholder: 'FACETS.USAGE.MODAL.SEARCH_PLACEHOLDER',
      searchFn: () =>
        this.apiServiceFacetAttribute.list({ facet_id: this.facet.id || this.facet.identifier }).pipe(
          map(({ content }) =>
            content.map((item) => {
              return {
                key: this.getPath(item),
              };
            })
          )
        ),
    };

    this.uiModalSelectService.showInfo(config, this.modalService);
  }

  getPath(item: FacetAttribute) {
    if (item.visual_path) {
      return item.visual_path;
    }
    if (item.product_type_name.endsWith('_ISS')) {
      return `${item.offer_path}/${item.product_type_name}/${item.attribute_name}${item.field_path}`;
    } else {
      return `${item.offer_path}${item.field_path}`;
    }
  }
}
