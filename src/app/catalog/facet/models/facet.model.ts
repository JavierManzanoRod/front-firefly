import { AttributeEntityType } from '@core/models/entity-type.model';
import { GenericApiRequest } from '@model/base-api.model';

export interface FacetDataDTO {
  name: string;
  data_type: string;
  /**
   * @deprecated
   */
  id?: string;
  identifier?: string;
}

export interface FacetLinkDTO {
  facet_id: string;
  facetLink_id?: string;
  attribute_id: string;
  field_path?: string;
  offer_path?: string;
  visual_path?: string;
  identifier?: string;
  attribute_name?: string;
  attribute_type?: string;
  facet_name?: string;
  facet_type?: string;
}

export interface AttributeFacet extends AttributeEntityType {
  field_path?: string;
  offer_path?: string;
  visual_path?: string;
  idAux?: string;
}

export interface FactetLinkApiRequest extends GenericApiRequest {
  facet_id?: string;
  field_path?: string;
  offer_path?: string;
  visual_path?: string;
}

export interface FacetAttribute {
  attribute_id: string;
  attribute_name: string;
  path: string;
  product_type_id: string;
  product_type_name: string;
  field_path?: string;
  offer_path?: string;
  visual_path?: string;
}
export interface FacetAttributeDTO {
  attribute: AttributeFacetDTO;
  path: string;
  product_type: ProductTypeFacetDTO;
  field_path?: string;
  offer_path?: string;
  visual_path?: string;
}

export interface ProductTypeFacetDTO {
  identifier: string;
  name: string;
}
export interface AttributeFacetDTO {
  identifier: string;
  name: string;
}
export interface Facet {
  identifier?: string;
  id?: string; // helper field to determine if is new or now.
  name?: string;
  data_type?: string;
  attribute_id?: string;
  id_attribute?: string;
  idAux?: string;
  old_facet?: Facet;
  selected_facet?: Facet;
  field_path?: string;
  offer_path?: string;
  visual_path?: string;
  facetLink_id?: string;
  facet_id?: string;
}

export interface VmFacetDetail {
  selectedFacet: Facet;
  loading: boolean;
  showConfigSection: boolean;
  usageFacet: FacetAttribute[];
  loadingUsage: boolean;
}
