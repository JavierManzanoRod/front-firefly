import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { UiModalSelectModule } from 'src/app/modules/ui-modal-select/ui-modal-select.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ExplodedTreeModule } from '../../modules/exploded-tree/exploded-tree.module';
import { UiTreeListModule } from '../../modules/ui-tree-list/ui-tree-list.module';
import { ProductTypeModule } from '../product-type/product-type.module';
import { FacetConfigComponent } from './components/facet-config/facet-config.component';
import { FacetDetailComponent } from './components/facet-detail/facet-detail.component';
import { FacetUsageComponent } from './components/facet-usage/facet-usage.component';
import { FacetDetailContainerComponent } from './containers/facet-detail-container.component';
import { FacetRoutingModule } from './facet-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModalModule,
    FacetRoutingModule,
    UiModalSelectModule,
    ProductTypeModule,
    UiTreeListModule,
    SimplebarAngularModule,
    NgSelectModule,
    ExplodedTreeModule,
  ],
  declarations: [FacetDetailContainerComponent, FacetDetailComponent, FacetConfigComponent, FacetUsageComponent],
  providers: [],
  exports: [FacetDetailComponent, FacetConfigComponent, FacetUsageComponent],
})
export class FacetModule {}
