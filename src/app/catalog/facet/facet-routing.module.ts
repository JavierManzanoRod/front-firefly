import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { FacetDetailContainerComponent } from './containers/facet-detail-container.component';

const routes: Routes = [
  {
    path: '',
    component: FacetDetailContainerComponent,
    canDeactivate: [CanDeactivateGuard],
    data: {
      header_title: 'FACETS.HEADER_MENU',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacetRoutingModule {}
