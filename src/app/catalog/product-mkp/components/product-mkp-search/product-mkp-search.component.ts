import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductMkp } from './../../models/product-mkp.model';

@Component({
  selector: 'ff-product-mkp-search',
  templateUrl: './product-mkp-search.component.html',
  styleUrls: ['./product-mkp-search.component.scss'],
})
export class ProductMkpSearchComponent {
  @Output() search: EventEmitter<ProductMkp> = new EventEmitter<ProductMkp>();
  form: FormGroup;

  constructor(protected fb: FormBuilder) {
    this.form = this.fb.group({
      name: [null],
      product_atg_key: null,
      is_market_place: null,
      parent_product_key: null,
    });
  }

  submit() {
    this.search.emit(this.form.value);
  }

  searchButton() {
    this.search.emit(this.form.value);
  }

  clearData() {
    this.form.reset();
    this.search.emit();
  }
}
