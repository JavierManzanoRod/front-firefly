import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductMkpDetailComponent } from './product-mkp-detail.component';

xdescribe('ProductMkpDetailComponent', () => {
  let component: ProductMkpDetailComponent;
  let fixture: ComponentFixture<ProductMkpDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductMkpDetailComponent],
      providers: [FormBuilder, Router],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductMkpDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
