import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericApiRequest } from '@model/base-api.model';
import { map } from 'rxjs/operators';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { ProductMkp } from '../../models/product-mkp.model';
import { ProductMkpService } from './../../services/product-mkp.service';

@Component({
  selector: 'ff-product-mkp-detail',
  templateUrl: './product-mkp-detail.component.html',
  styleUrls: ['./product-mkp-detail.component.scss'],
})
export class ProductMkpDetailComponent {
  @Output() formSubmit: EventEmitter<ProductMkp> = new EventEmitter<ProductMkp>();

  @Input() set details(value: ProductMkp) {
    this._details = value;
    if (value) {
      this.loading = false;
      this.form.patchValue({
        ...value,
        parent_product_key: value.parent_name?.id ? { name: value.parent_name?.id } : null,
        name: value.name[this.spainKey],
      });
    }
  }
  get details(): ProductMkp {
    return this._details;
  }
  _details!: ProductMkp;

  form: FormGroup;
  loading = true;
  parent_product_key_config: ChipsControlSelectConfig;

  private readonly spainKey = 'es_ES';

  constructor(
    private fb: FormBuilder,
    protected router: Router,
    private productService: ProductMkpService,
    private activeRoute: ActivatedRoute
  ) {
    this.form = this.fb.group({
      name: [{ value: null, disabled: true }],
      id: [{ value: null, disabled: true }],
      market_place: null,
      parent_product_key: [null, [Validators.required]],
    });

    this.parent_product_key_config = {
      label: 'PRODUCTMKP.PARENT_PRODUCT_KEY',
      modalType: ModalChipsTypes.select,
      modalConfig: {
        title: 'PRODUCTMKP.SEARCH',
        items: [],
        searchFn: (x: GenericApiRequest) =>
          this.productService
            .list({
              ...x,
              is_market_place: true,
            })
            .pipe(
              map((v) => {
                v.content = v.content.map((d) => {
                  d.name = d.name[this.spainKey];
                  return d;
                });
                return v;
              })
            ),
      },
    };
  }

  submit() {
    this.form.markAllAsTouched();

    if (!this.form.valid) {
      return false;
    }
    const product: ProductMkp = {
      ...this.details,
      name: this.details.name[this.spainKey],
      ...this.form.value,
      parent_product_key: this.form.value.parent_product_key?.id,
    };
    this.formSubmit.emit(product);

    // TODO: lanzar evento , parsear parent_product
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.activeRoute.parent });
  }
}
