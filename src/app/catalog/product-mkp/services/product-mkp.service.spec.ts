import { HttpClient } from '@angular/common/http';
import { BusinessApiResponse, GenericApiRequest, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { ProductMkpDTO } from '../models/product-mkp.model';
import { ProductMkpService } from './product-mkp.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: ProductMkpService;
const version = 'v1/';
const expectedData: ProductMkpDTO = {
  identifier: '00002',
  name: [
    {
      locale: 'en_GB',
      value: 'Lens Accessories',
    },
    {
      locale: 'pt_PT',
      value: 'Acessórios para objectivas',
    },
  ],
  is_market_place: true,
  parent: {
    identifier: '00013',
  },
};

const expectedList: BusinessApiResponse<ProductMkpDTO> = {
  business_object: [expectedData],
  pagination_object: (null as unknown) as Page,
};

describe('ProductMkpService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`ProductMkpService points to products/marketplace-product-types/${version}`, () => {
    myService = new ProductMkpService((null as unknown) as HttpClient, version);
    expect(myService.endPoint).toEqual(`products/marketplace-product-types/${version}`);
  });

  it('ProductMkpService.list calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new ProductMkpService(httpClientSpy as any, version);
    myService.list((null as unknown) as GenericApiRequest).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });

  it('ProductMkpService.emptyRecord record gives me some data', () => {
    myService = new ProductMkpService((null as unknown) as HttpClient, version);
    const record = myService.emptyRecord();
    expect(record).toBeDefined();
  });
});
