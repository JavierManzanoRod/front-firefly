import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BusinessApiService } from '@core/base/api-business.service';
import { UpdateResponse } from '@core/base/api.service';
import { environment } from '@env/environment';
import { BusinessApiResponse, GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_PRODUCT_TYPES } from 'src/app/configuration/tokens/api-versions.token';
import { ProductMkp, ProductMkpDTO } from '../models/product-mkp.model';
import { ProductMkpInMapper } from './dto/product-mkp-in-mapper-class';
import { ProductMkpOutMapper } from './dto/product-mkp-out-mapper-class';

const urlBaseMarketPlace = `${environment.API_URL_BACKOFFICE}`;

@Injectable({
  providedIn: 'root',
})
export class ProductMkpService extends BusinessApiService<ProductMkp> {
  endPoint = `products/marketplace-product-types/${this.version}`;
  private _urlBase = urlBaseMarketPlace + `/${this.endPoint}`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_PRODUCT_TYPES) private version: string) {
    super();
  }

  public get urlBase() {
    return this._urlBase;
  }

  public set urlBase(value) {
    this._urlBase = value;
  }

  getParams(filter: any): HttpParams {
    let params: HttpParams = new HttpParams();
    const keys = Object.keys(filter || {});
    if (keys.length > 1) {
      keys[0] = 'identifier';
      filter.identifier = filter.id;
    }
    keys.forEach((value) => {
      params =
        filter[value] !== undefined && filter[value] !== null && filter[value] !== ''
          ? params.set(value, filter[value].toString())
          : params;
    });
    params = params.set('sort', 'asc');
    if (filter.name) {
      params = params.set('name', filter.name);
    }
    return params;
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<ProductMkp>> {
    return super.list(filter).pipe(
      map((data) => this.toGeneric(data as unknown as BusinessApiResponse<ProductMkp>)),
      map(({ content, page }) => ({
        content: content.map((product) => new ProductMkpOutMapper(product as unknown as ProductMkpDTO).data),
        page,
      }))
    );
  }

  detail(id: string): Observable<ProductMkp> {
    return super.detail(id, true).pipe(map((product) => new ProductMkpOutMapper(product as unknown as ProductMkpDTO).data));
  }

  patch(data: ProductMkp): Observable<UpdateResponse<ProductMkp>> {
    const url = `${this.urlBase}${data.id}`;
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .request('patch', url, {
        body: new ProductMkpInMapper(data).data,
        headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            status: res.status,
            data: new ProductMkpOutMapper(res.body).data,
          };
        }),
        catchError((e: HttpErrorResponse) => {
          if (e.error && e.error.error && e.error.error.detail) {
            const temp: UpdateResponse<any> = {
              status: e.status,
              data: null,
              error: e.error.error.detail,
            };
            return of(temp);
          }
          return throwError(e);
        })
      );
  }
}
