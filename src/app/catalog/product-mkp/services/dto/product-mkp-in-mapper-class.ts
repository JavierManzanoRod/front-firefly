import { Parent, ParentDTO, ProductMkp, ProductMkpDTO } from '../../models/product-mkp.model';

export class ProductMkpInMapper {
  data = {} as ProductMkpDTO;

  constructor(remoteData: ProductMkp) {
    this.data = {
      identifier: remoteData.id,
      name: remoteData.name,
      is_market_place: remoteData.market_place,
      parent_product_key: remoteData.parent_product_key,
      parent: remoteData.parent_product_key
        ? {
            identifier: remoteData.parent_product_key || null,
          }
        : this.transformParent(remoteData.parent_name, remoteData.parent_product_key),
    } as unknown as ProductMkpDTO;
  }

  private transformParent({ id, ...item }: Parent, parentKey: string | undefined): ParentDTO {
    const response = item as ParentDTO;

    if (id) {
      response.identifier = id ? id : parentKey;
    }
    return response;
  }
}
