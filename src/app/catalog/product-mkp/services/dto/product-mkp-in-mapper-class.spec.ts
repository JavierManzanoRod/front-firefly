import { ProductMkp } from '../../models/product-mkp.model';
import { ProductMkpInMapper } from './product-mkp-in-mapper-class';

describe('product-type mapper in', () => {
  it('parse an incoming product type as expected', () => {
    const remoteItem = {
      id: '00002',
      name: [
        {
          locale: 'en_GB',
          value: 'Lens Accessories',
        },
        {
          locale: 'es_ES',
          value: 'Accesorios para objetivos',
        },
        {
          locale: 'fr_FR',
          value: 'Accessoires pour objectifs',
        },
        {
          locale: 'pt_PT',
          value: 'Acessórios para objectivas',
        },
      ],
      market_place: true,
      parent_name: {
        id: '323232',
      },
    } as ProductMkp;

    const mapper = new ProductMkpInMapper(remoteItem);
    const product = mapper.data;
    expect(product.identifier).toEqual('00002');
    expect(product.name).toEqual([
      {
        locale: 'en_GB',
        value: 'Lens Accessories',
      },
      {
        locale: 'es_ES',
        value: 'Accesorios para objetivos',
      },
      {
        locale: 'fr_FR',
        value: 'Accessoires pour objectifs',
      },
      {
        locale: 'pt_PT',
        value: 'Acessórios para objectivas',
      },
    ]);
    expect(product.is_market_place).toEqual(true);
    expect(product.parent.identifier).toEqual('323232');
  });

  it('parse an incoming product with some fields null or undefined does not break anything', () => {
    const remoteItem = {
      id: undefined,
      name: [
        {
          locale: 'en_GB',
          value: undefined,
        },
        {
          locale: 'es_ES',
          value: undefined,
        },
      ],
      parent_name: {
        id: undefined,
      },
    } as unknown as ProductMkp;

    const mapper = new ProductMkpInMapper(remoteItem);
    const product = mapper.data;
    expect(product.identifier).toEqual(undefined as any);
    expect(product.name[0].value).toEqual(undefined as any);
    expect(product.parent.identifier).toEqual(undefined as any);
  });
});
