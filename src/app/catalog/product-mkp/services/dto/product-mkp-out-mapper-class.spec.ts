import { ProductMkpDTO } from '../../models/product-mkp.model';
import { ProductMkpOutMapper } from './product-mkp-out-mapper-class';

describe('product-type mapper out', () => {
  it('parse an outgoing req product type as expected', () => {
    const remoteItem = {
      identifier: '00002',
      name: [
        {
          locale: 'en_GB',
          value: 'Lens Accessories',
        },
        {
          locale: 'es_ES',
          value: 'Accesorios para objetivos',
        },
        {
          locale: 'fr_FR',
          value: 'Accessoires pour objectifs',
        },
        {
          locale: 'pt_PT',
          value: 'Acessórios para objectivas',
        },
      ],
      is_market_place: true,
      parent: {
        identifier: '323232',
      },
    } as ProductMkpDTO;

    const mapper = new ProductMkpOutMapper(remoteItem);
    const product = mapper.data;
    expect(product.id).toEqual('00002');
    expect(product.name).toEqual({
      en_GB: 'Lens Accessories',
      es_ES: 'Accesorios para objetivos',
      fr_FR: 'Accessoires pour objectifs',
      pt_PT: 'Acessórios para objectivas',
    });
    expect(product.market_place).toEqual(true);
    expect(product.parent_name.id).toEqual('323232');
  });

  it('parse an outgoing req product with some fields null or undefined does not break anything', () => {
    const remoteItem = {
      identifier: undefined,
      parent: {
        identifier: undefined,
      },
    } as unknown as ProductMkpDTO;

    const mapper = new ProductMkpOutMapper(remoteItem);
    const product = mapper.data;
    expect(product.id).toEqual(undefined as any);
    expect(product.parent_name.id).toEqual(undefined as any);
  });
});
