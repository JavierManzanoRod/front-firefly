import { bidimensionalyToDictionary } from 'src/app/shared/utils/maper-utils';
import { Parent, ParentDTO, ProductMkp, ProductMkpDTO } from '../../models/product-mkp.model';

export class ProductMkpOutMapper {
  data = {} as ProductMkp;

  constructor(remoteData: ProductMkpDTO) {
    this.data = {
      id: remoteData.identifier,
      name: bidimensionalyToDictionary(remoteData.name, 'locale', 'value'),
      market_place: remoteData.is_market_place,
      parent_product_key: remoteData.parent.identifier,
      parent_name: remoteData.parent_product_key ? { identifier: remoteData.parent_product_key } : this.transformParent(remoteData.parent),
    } as unknown as ProductMkp;
  }

  private transformParent({ identifier, ...item }: ParentDTO): Parent {
    const response = item as Parent;

    if (identifier) {
      response.id = identifier;
    }
    return response;
  }
}
