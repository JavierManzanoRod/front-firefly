import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductMkpListContainerComponent } from './containers/product-mkp-list-container.component';
import { ProductMkpViewContainerComponent } from './containers/product-mkp-view-container/product-mkp-view-container.component';

const routes: Routes = [
  {
    path: '',
    component: ProductMkpListContainerComponent,
    data: {
      header_title: 'PRODUCTMKP.HEADER_MENU',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
    children: [
      {
        path: ':id',
        component: ProductMkpViewContainerComponent,
        data: {
          header_title: 'PRODUCTMKP.HEADER_MENU',
          breadcrumb: [
            {
              label: 'PRODUCTMKP.BREADCRUMB_LIST_TITLE',
              url: '/products-mkp',
            },
          ],
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductMkpRoutingModule {}
