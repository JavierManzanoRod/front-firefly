import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { SearchMultipleModule } from 'src/app/modules/search-multiple/search-multiple.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductMkpDetailComponent } from './components/product-mkp-detail/product-mkp-detail.component';
import { ProductMkpSearchComponent } from './components/product-mkp-search/product-mkp-search.component';
import { ProductMkpListContainerComponent } from './containers/product-mkp-list-container.component';
import { ProductMkpViewContainerComponent } from './containers/product-mkp-view-container/product-mkp-view-container.component';
import { ProductMkpRoutingModule } from './product-mkp-routing.module';

@NgModule({
  declarations: [ProductMkpDetailComponent, ProductMkpListContainerComponent, ProductMkpSearchComponent, ProductMkpViewContainerComponent],
  imports: [
    CommonModule,
    SharedModule,
    CommonModalModule,
    GenericListComponentModule,
    FormsModule,
    SimplebarAngularModule,
    SearchMultipleModule,
    CoreModule,
    ReactiveFormsModule,
    FormErrorModule,
    ProductMkpRoutingModule,
    ChipsControlModule,
  ],
})
export class ProductMkpModule {}
