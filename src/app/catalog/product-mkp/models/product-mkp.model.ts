export interface ProductMkp {
  id: string;
  name: any;
  market_place: boolean;
  parent_product_key?: string;
  parent_name: Parent;
}

export interface ProductMkpDTO {
  identifier: string;
  name: Name[];
  is_market_place: boolean;
  parent_product_key?: string;
  parent: ParentDTO;
}

export interface Name {
  locale: string;
  value: string;
}

export interface ParentDTO {
  identifier?: string | null;
}

export interface Parent {
  id?: string | null;
}
