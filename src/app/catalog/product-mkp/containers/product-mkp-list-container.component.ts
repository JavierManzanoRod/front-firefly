import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { ProductMkp } from '../models/product-mkp.model';
import { ProductMkpService } from '../services/product-mkp.service';
import { ConfigMultiple } from './../../../modules/search-multiple/models/config.models';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <ff-page-header [pageTitle]="'PRODUCTMKP.TITLE' | translate"></ff-page-header>
    <ngx-simplebar class="page-container" *ngIf="!hide">
      <div class="page-scroll-wrapper">
        <ngx-simplebar class="page-scroll">
          <div class="page-container-padding">
            <ff-generic-list
              [list]="list$ | async"
              [arrayKeys]="arrayKeys"
              [loading]="loading"
              [currentData]="currentData"
              (delete)="delete($event)"
              [page]="page"
              [valueSearch]="filter"
              (search)="search($event)"
              (pagination)="handlePagination($event)"
              [type]="type"
              [configMultiple]="config"
              [canDelete]="false"
              [route]="'/catalog/products-mkp'"
            >
            </ff-generic-list>
          </div>
        </ngx-simplebar>
      </div>
    </ngx-simplebar> `,
  styleUrls: ['./product-mkp-list-container.component.scss'],
})
export class ProductMkpListContainerComponent extends BaseListContainerComponent<ProductMkp> implements OnInit {
  config: ConfigMultiple[] = [
    {
      key: 'id',
      type: 'TEXT',
      placeHolder: 'PRODUCTMKP.PLACEHOLDER_LABEL_CODE',
      label: 'PRODUCTMKP.LABEL_CODE',
      class: 'col-2',
    },
    {
      key: 'name',
      type: 'TEXT',
      label: 'PRODUCTMKP.NAME',
      placeHolder: 'PRODUCTMKP.PLACEHOLDER_NAME',
      class: 'col-4',
    },
    {
      key: 'is_market_place',
      type: 'SELECT',
      class: 'col-2',
      label: 'PRODUCTMKP.LABEL_MKP',
      selectOptions: [
        {
          label: 'COMMON.YES',
          value: true,
        },
        {
          label: 'COMMON.NO',
          value: false,
        },
      ],
      placeHolder: 'PRODUCTMKP.MARKET_PLACE',
    },
    {
      key: 'parent_product_key',
      type: 'TEXT',
      placeHolder: 'PRODUCTMKP.PLACEHOLDER_LABEL_CODE',
      label: 'PRODUCTMKP.LABEL_CODE_PARENT',
      class: 'col-3',
    },
  ];

  hide = false;
  type = TypeSearch.multiple;

  arrayKeys: GenericListConfig[] = [
    {
      key: 'id',
      headerName: 'PRODUCTMKP.ID',
    },
    {
      key: 'name.es_ES',
      headerName: 'PRODUCTMKP.NAME',
    },
    {
      key: 'market_place',
      headerName: 'PRODUCTMKP.MARKET_PLACE',
      canActiveClass: true,
      textCenter: true,
    },
    {
      key: 'parent_name.id',
      headerName: 'PRODUCTMKP.PARENT_PRODUCT_KEY',
      textCenter: true,
    },
  ];

  constructor(public apiService: ProductMkpService, public utils: CrudOperationsService) {
    super(utils, apiService);
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({});
  }
}
