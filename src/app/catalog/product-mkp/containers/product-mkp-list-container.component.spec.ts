import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { ProductMkpService } from '../services/product-mkp.service';
import { ProductMkpListContainerComponent } from './product-mkp-list-container.component';

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}

  public get(key: any): any {
    of(key);
  }
}
@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

describe('ProductMkpListContainerComponent', () => {
  let component: ProductMkpListContainerComponent;
  let fixture: ComponentFixture<ProductMkpListContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductMkpListContainerComponent, TranslatePipeMock],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceStub },
            {
          provide: ProductMkpService,
          useFactory: () => ({}),
          list: () => {},
        },
        { provide: CrudOperationsService, useFactory: () => {} },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductMkpListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
