import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { ProductMkpViewContainerComponent } from './product-mkp-view-container.component';

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}

  public get(key: any): any {
    of(key);
  }
}
@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}
class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };

  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }

  public hide() {
    return true;
  }
}

class ToastrServiceStub {
  public success() {}
}

describe('ProductMkpViewContainerComponent', () => {
  let component: ProductMkpViewContainerComponent;
  let fixture: ComponentFixture<ProductMkpViewContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductMkpViewContainerComponent, TranslatePipeMock],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceStub },
        { provide: BsModalService, useClass: ModalServiceMock },
        { provide: ToastService, useClass: ToastrServiceStub },
        {
          provide: Router,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: ActivatedRoute,
          useFactory: () => ({
            snapshot: {
              paramMap: {
                get: () => {},
              },
            },
          }),
        },
        {
          provide: HttpClient,
          useFactory: () => ({
            error: () => {},
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductMkpViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
