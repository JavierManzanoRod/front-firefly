import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsComponent } from '@core/base/utils-component';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable, of, throwError } from 'rxjs';
import { catchError, delay } from 'rxjs/operators';
import { ToastService } from '../../../../modules/toast/toast.service';
import { ProductMkp } from './../../models/product-mkp.model';
import { ProductMkpService } from './../../services/product-mkp.service';

@Component({
  selector: 'ff-product-mkp-view-container',
  template: `<ff-page-header
      [pageTitle]="'PRODUCTMKP.DETAIL' | translate"
      breadcrumbLink="/catalog/products-mkp"
      breadcrumbTitle="Volver al listado"
    >
    </ff-page-header>
    <div class="page-container page-container-padding">
      <ff-product-mkp-detail [details]="detail$ | async" (formSubmit)="formSubmit($event)"></ff-product-mkp-detail>
    </div> `,
  styleUrls: ['./product-mkp-view-container.component.scss'],
})
export class ProductMkpViewContainerComponent implements OnInit {
  detail$!: Observable<ProductMkp>;
  id: string;
  loading = true;

  utils: UtilsComponent = new UtilsComponent();

  constructor(
    protected route: ActivatedRoute,
    public apiService: ProductMkpService,
    protected router: Router,
    public modalService: BsModalService,
    public toast: ToastService,
    public translateService: TranslateService
  ) {
    this.id = this.route.snapshot.paramMap.get('id') || '';
  }

  ngOnInit() {
    let detail$: Observable<ProductMkp>;
    if (this.id) {
      detail$ = this.apiService.detail(this.id).pipe(
        catchError((e) => {
          this.toast.error('COMMON_ERRORS.SEARCH_ERROR');
          this.router.navigate(['/catalog/products-mkp']);
          return throwError(e);
        })
      );
    } else {
      detail$ = of({} as ProductMkp).pipe(delay(250));
    }
    this.detail$ = detail$;
  }

  formSubmit(event: ProductMkp) {
    this.utils.updateOrSaveModal(
      {
        modalService: this.modalService,
        apiService: this.apiService,
        toast: this.toast,
      },
      event,
      (e: any) => {
        if (e.error && e.error.error_code === 'F-400') {
          this.toast.error('PRODUCTMKP.ERROR.F-400');
        }
      },
      'COMMON_ERRORS.SAVE_ERROR',
      true
    );
  }
}
