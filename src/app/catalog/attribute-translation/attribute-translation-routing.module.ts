import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { AttributeTranslationContainerComponent } from './containers/attribute-translation-container.component';

const routes: Routes = [
  {
    path: '',
    canDeactivate: [CanDeactivateGuard],
    component: AttributeTranslationContainerComponent,
    data: {
      header_title: 'MENU_LEFT.CATALOG',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttributeTranslationRoutingModule {}
