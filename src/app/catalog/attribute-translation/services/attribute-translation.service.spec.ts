import { HttpClient } from '@angular/common/http';
import { GenericApiRequest, GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { AttributeTranslationsDTO } from '../models/attribute-translation.model';
import { AttributeTranslationService } from './attribute-translation.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: AttributeTranslationService;
const version = 'v1/';
const expectedData: AttributeTranslationsDTO = {
  id: '1',
  name: 'name',
  translations: [
    {
      locale: 'en_GB',
      value: '3D',
    },
  ],
};

const expectedList: GenericApiResponse<AttributeTranslationsDTO> = {
  content: [expectedData],
  page: null as unknown as Page,
};

describe('AttributeTranslationService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`AttributeTranslationService points to products/backoffice-attribute-translates/${version}attributes-translations`, () => {
    myService = new AttributeTranslationService(null as unknown as HttpClient, version);
    expect(myService.endPoint).toEqual(`products/backoffice-attributes-translations/${version}attributes-translations`);
  });

  it('AttributeTranslationService.list calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new AttributeTranslationService(httpClientSpy as any, version);
    myService.list(null as unknown as GenericApiRequest).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });
});
