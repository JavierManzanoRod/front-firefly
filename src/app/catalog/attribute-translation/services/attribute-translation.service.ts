import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, UpdateResponse } from '@core/base/api.service';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_ATTRIBUTE_TRANSLATION } from 'src/app/configuration/tokens/api-versions.token';
import { AttributeTranslations, AttributeTranslationsDTO } from '../models/attribute-translation.model';
import { AttributeInTranslations } from './dtos/attribute-in-translation';
import { AttributeOutTranslations } from './dtos/attribute-out-translation';

@Injectable({
  providedIn: 'root',
})
export class AttributeTranslationService extends ApiService<AttributeTranslations> {
  endPoint = `products/backoffice-attributes-translations/${this.version}attributes-translations`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_ATTRIBUTE_TRANSLATION) private version: string) {
    super();
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<AttributeTranslations>> {
    return super.list(filter).pipe(
      map((response) => {
        response.content = response.content.map((res) => new AttributeOutTranslations(res as unknown as AttributeTranslationsDTO).data);
        return response;
      })
    );
  }

  detail(path: string): Observable<AttributeTranslations> {
    return this.http
      .get<AttributeTranslations>(this.urlBase, {
        params: this.getParams({ path }),
      })
      .pipe(map((detail) => new AttributeOutTranslations(detail as unknown as AttributeTranslationsDTO).data));
  }

  post(data: AttributeTranslations): Observable<UpdateResponse<AttributeTranslations>> {
    return super.post(new AttributeInTranslations(data).data as unknown as AttributeTranslations).pipe(
      map((res: UpdateResponse<AttributeTranslations>) => {
        return {
          ...res,
          data: new AttributeOutTranslations(res.data as unknown as AttributeTranslationsDTO).data as unknown as AttributeTranslations,
        };
      })
    );
  }

  update(data: AttributeTranslations): Observable<UpdateResponse<AttributeTranslations>> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .request('put', this.urlBase, {
        body: new AttributeInTranslations(data).data as unknown as AttributeTranslations,
        headers,
        observe: 'response',
        responseType: 'json',
        params: { path: data.path || '' },
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            status: res.status,
            data: new AttributeOutTranslations(res.body as unknown as AttributeTranslationsDTO).data as unknown as AttributeTranslations,
          };
        })
      );
  }

  delete(data: AttributeTranslations): Observable<AttributeTranslations> {
    return this.http
      .delete<AttributeTranslations>(this.urlBase, {
        params: { path: data.path || '' },
      })
      .pipe(
        catchError((e) => {
          if (e.status === 404) {
            return of(data);
          } else return throwError(null);
        })
      );
  }
}
