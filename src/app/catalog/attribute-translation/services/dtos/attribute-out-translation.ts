import { bidimensionalyToDictionary } from 'src/app/shared/utils/mapers-utils';
import { AttributeTranslations, AttributeTranslationsDTO } from '../../models/attribute-translation.model';

export class AttributeOutTranslations {
  data = {} as AttributeTranslations;

  constructor(data: AttributeTranslationsDTO) {
    const override: AttributeTranslations = {
      translations: data.translations && bidimensionalyToDictionary(data?.translations, 'locale', 'value'),
    };

    this.data = { ...data, ...override } as unknown as AttributeTranslations;
  }
}
