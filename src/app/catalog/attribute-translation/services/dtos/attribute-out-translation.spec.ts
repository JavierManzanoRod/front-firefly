import { AttributeTranslations, AttributeTranslationsDTO } from '../../models/attribute-translation.model';
import { AttributeInTranslations } from './attribute-in-translation';
import { AttributeOutTranslations } from './attribute-out-translation';

describe('Attribute translation mapper out', () => {
  it('parse a outgoing request as expected', () => {
    const remoteItem: AttributeTranslationsDTO = {
      id: 'a3cvqzztgp4raq',
      path: '/TV_Plana_ISS/3D',
      translations: [
        {
          locale: 'en_GB',
          value: '3D',
        },
        {
          locale: 'en_US',
          value: '3D',
        },
        {
          locale: 'es_ES',
          value: '3D',
        },
        {
          locale: 'fr_FR',
          value: '3D',
        },
        {
          locale: 'pt_PT',
          value: '3D',
        },
        {
          locale: 'de_DE',
          value: '3D',
        },
        {
          locale: 'ca_ES',
          value: '3D',
        },
      ],
    } as AttributeTranslationsDTO;

    const mapper = new AttributeOutTranslations(remoteItem);
    const site = mapper.data;
    expect(site.id).toEqual('a3cvqzztgp4raq');
    expect(site.path).toEqual('/TV_Plana_ISS/3D');
    expect(site.translations).toEqual({
      en_GB: '3D',
      en_US: '3D',
      es_ES: '3D',
      fr_FR: '3D',
      pt_PT: '3D',
      de_DE: '3D',
      ca_ES: '3D',
    });
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem: AttributeTranslations = {
      id: undefined,
      path: undefined,
    } as unknown as AttributeTranslations;

    const mapper = new AttributeInTranslations(remoteItem);
    const attribute = mapper.data;
    expect(attribute.id).toEqual(undefined);
    expect(attribute.path).toEqual(undefined);
  });
});
