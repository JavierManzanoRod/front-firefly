import { dictionaryToBidimensionaly } from 'src/app/shared/utils/mapers-utils';
import { AttributeTranslations, AttributeTranslationsDTO } from '../../models/attribute-translation.model';

export class AttributeInTranslations {
  data = {} as AttributeTranslationsDTO;

  constructor(remoteData: AttributeTranslations) {
    this.data = {
      id: remoteData.id,
      path: remoteData.path,
      name: remoteData.name,
      translations: remoteData.translations && dictionaryToBidimensionaly(remoteData.translations, 'locale', 'value', true),
    } as unknown as AttributeTranslationsDTO;
  }
}
