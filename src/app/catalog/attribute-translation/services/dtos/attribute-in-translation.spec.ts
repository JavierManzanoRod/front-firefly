import { AttributeTranslations } from '../../models/attribute-translation.model';
import { AttributeInTranslations } from './attribute-in-translation';

describe('attribute mapper in', () => {
  it('parse an incoming attribute translation as expected', () => {
    const remoteItem: AttributeTranslations = {
      id: 'a3cvqzztgp4raq',
      path: '/TV_Plana_ISS/3D',
      translations: {
        en_GB: '3D',
        en_US: '3D',
        es_ES: '3D',
        fr_FR: '3D',
        pt_PT: '3D',
        de_DE: '3D',
        ca_ES: '3D',
      },
    } as AttributeTranslations;

    const mapper = new AttributeInTranslations(remoteItem);
    const site = mapper.data;
    expect(site.id).toEqual('a3cvqzztgp4raq');
    expect(site.path).toEqual('/TV_Plana_ISS/3D');
    expect(site.translations).toEqual([
      {
        locale: 'en_GB',
        value: '3D',
      },
      {
        locale: 'en_US',
        value: '3D',
      },
      {
        locale: 'es_ES',
        value: '3D',
      },
      {
        locale: 'fr_FR',
        value: '3D',
      },
      {
        locale: 'pt_PT',
        value: '3D',
      },
      {
        locale: 'de_DE',
        value: '3D',
      },
      {
        locale: 'ca_ES',
        value: '3D',
      },
    ]);
  });

  it('parse an incoming Attribute with some fields null or undefined does not break anything', () => {
    const remoteItem: AttributeTranslations = {
      translations: undefined,
    } as unknown as AttributeTranslations;

    const mapper = new AttributeInTranslations(remoteItem);
    const site = mapper.data;
    expect(site.translations).toEqual(undefined as any);
  });
});
