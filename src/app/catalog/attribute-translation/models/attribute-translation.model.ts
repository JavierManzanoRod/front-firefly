import { GenericApiResponse } from '@model/base-api.model';

export interface AttributeTranslations {
  visual_path?: any;
  data?: any;
  id?: string;
  path?: string;
  pathCache?: string;
  name?: string;
  canPut?: boolean;
  translations: {
    [key: string]: string;
  } | null;
}

export interface Translations {
  locale: string;
  value: string;
}
export interface AttributeTranslationsDTO {
  id?: string;
  path?: string;
  name?: string;
  translations: Translations[];
}

export type AttributeResponse = GenericApiResponse<AttributeTranslations>;

export type AttributeInResponse = GenericApiResponse<AttributeTranslationsDTO>;
