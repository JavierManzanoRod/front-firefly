import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ExplodedTreeModule } from 'src/app/modules/exploded-tree/exploded-tree.module';
import { ToastModule } from 'src/app/modules/toast/toast.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModalModule } from '../../modules/common-modal/common-modal.module';
import { FieldI18nModule } from '../../modules/field-i18n/field-i18n.module';
import { ProductTypeModule } from '../product-type/product-type.module';
import { AttributeTranslationRoutingModule } from './attribute-translation-routing.module';
import { AttributeTranslationConfigComponent } from './components/attribute-config-translation/attribute-config-translation.component';
import { AttributeTranslationContainerComponent } from './containers/attribute-translation-container.component';

@NgModule({
  declarations: [AttributeTranslationConfigComponent, AttributeTranslationContainerComponent],
  imports: [
    CommonModule,
    AttributeTranslationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModalModule,
    CoreModule,
    FieldI18nModule,
    ToastModule,
    ProductTypeModule,
    SimplebarAngularModule,
    ExplodedTreeModule,
  ],
  exports: [AttributeTranslationConfigComponent],
  providers: [],
})
export class AttributeTranslationModule {}
