import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { CanComponentDeactivate } from '@core/guards/can-deactivate-guard';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { MayHaveLabel } from '@model/base-api.model';
import { merge, Observable, of, Subject } from 'rxjs';
import { catchError, filter, switchMap, tap } from 'rxjs/operators';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { UiModalCommonService } from 'src/app/modules/common-modal/services/ui-modal-common.service';
import { UiModalWarningUnChangesService } from 'src/app/modules/common-modal/services/ui-modal-warning-unchanges.service';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { ExplodedClickEvent, ExplodedTreeNodeType } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { configuration } from '../../../configuration/configuration';
import { UiTreeClickEvent } from '../../../modules/ui-tree-list/models/ui-tree-list.model';
import { UiStateService } from '../../../shared/services/ui-state.service';
import { AttributeTranslationConfigComponent } from '../components/attribute-config-translation/attribute-config-translation.component';
import { AttributeTranslations } from '../models/attribute-translation.model';
import { AttributeTranslationService } from '../services/attribute-translation.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<ff-page-header-long [pageTitle]="'ATTRIBUTETRANSLATION.HEADER_MENU' | translate"></ff-page-header-long>
    <ngx-simplebar class="page-container-long pr-3">
      <div class="row tree">
        <div class="col-4 h-100">
          <ff-offer-exploded-tree [showFilter]="showFilter" [hiddenBundleOf]="true" (clickNode)="click($event)"></ff-offer-exploded-tree>
        </div>
        <!--<div class="col-4">
          <ff-product-type-list2
            [expandOnAtrtributesEntity]="true"
            (eventClickProductTree)="onClickedTree($event)"
          ></ff-product-type-list2>
        </div>-->
        <div class="col-8 h-100">
          <div class="page-container-long-section-right">
            <ff-attribute-config-translation
              #formDetail
              [hidden]="hiddenForm"
              [hiddenConfig]="hiddenConfig"
              [selectedAttribute]="selectedAttribute$ | async"
              [translation]="selectedTranslation$ | async"
              [loading]="loading$ | async"
              (saved)="postSubmitted($event)"
            >
            </ff-attribute-config-translation>
          </div>
        </div>
      </div>
    </ngx-simplebar> `,
  providers: [UiModalCommonService],
  styleUrls: ['./attribute-translation-container.scss'],
})
export class AttributeTranslationContainerComponent implements CanComponentDeactivate {
  @ViewChild(AttributeTranslationConfigComponent, { static: false })
  formComponent!: AttributeTranslationConfigComponent;
  selectedAttributeSubject = new Subject<Partial<AttributeEntityType> | null>();
  selectedAttribute$ = this.selectedAttributeSubject.asObservable().pipe(tap((v) => (this.nameAtt = v?.label || '')));
  loading$ = new Subject<boolean>();

  selectedTranslateionSubject = new Subject<AttributeTranslations>();
  hiddenForm = true;
  hiddenConfig = true;
  nameAtt = '';

  selectedTranslation$: Observable<any> = merge(
    this.selectedAttribute$.pipe(
      filter((att) => !!att),
      switchMap((att) => {
        this.loading$.next(true);
        return this.attributeTranslationService.detail(att?.evaluation_info?.path as string).pipe(
          catchError(() => {
            this.loading$.next(false);
            return of(null);
          })
        );
      }),
      tap(() => this.loading$.next(false))
    ),
    this.selectedTranslateionSubject
  );

  constructor(
    public attributeTranslationService: AttributeTranslationService,
    public uiModalCommonService: UiModalCommonService,
    private uiModalWarningUnChangesService: UiModalWarningUnChangesService,
    private uiStateService: UiStateService
  ) {
    this.uiStateService.closeMenuIfWindowSizeSmall();
  }

  onClickedTree(item: UiTreeClickEvent<MayHaveLabel>) {
    this.hiddenForm = false;

    const value = {
      id: item.selectedItem.id,
      label: item.selectedItem.label,
      evaluation_info: {
        path: item.selectedItem.name,
      },
    };

    if (this.formComponent.formTranslations.dirty) {
      this.uiModalCommonService.showWarningAttributesTranslationsUnSave().subscribe((flagAccept) => {
        if (flagAccept) {
          this.selectedAttributeSubject.next(value as any);
        }
      });
    } else {
      this.selectedAttributeSubject.next(value as any);
    }
  }

  computeHideForm(item: UiTreeClickEvent<MayHaveLabel>): boolean {
    const selectedItem = item.selectedItem;

    if (selectedItem.id === configuration.offerEntityType) {
      // ENTITY
      return true;
    }

    if (selectedItem.subtype) {
      // PRODUCT
      return true;
    }
    if (selectedItem.parent_attribute_tree_node_id) {
      // ATTRIBUTE-TREE-NODE
      return true;
    }
    return false;
  }

  onNewProductSearch() {
    this.selectedAttributeSubject.next(null);
  }

  postSubmitted(event: AttributeTranslations) {
    this.selectedTranslateionSubject.next(event);
  }

  showFilter() {
    return true;
  }

  click({ data, target }: ExplodedClickEvent<ExplodedTreeNodeComponent>) {
    this.hiddenForm = false;
    this.hiddenConfig = data.node_type === ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE;

    const value = {
      id: data.node_id,
      label: data.label,
      evaluation_info: {
        path: target.getVisualPath(),
      },
    };

    if (this.formComponent.formTranslations.dirty) {
      this.uiModalWarningUnChangesService
        .show({
          bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
          name: this.nameAtt,
        })
        .subscribe((flagAccept) => {
          if (flagAccept) {
            this.selectedAttributeSubject.next(value as any);
          } else {
            target.root?.prevActive?.activate();
            target.root?.update();
          }
        });
    } else {
      this.selectedAttributeSubject.next(value as any);
    }
  }

  canDeactivate(): UICommonModalConfig | false {
    if (this.formComponent?.formTranslations.dirty) {
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.nameAtt,
      };
    }
    return false;
  }
}
