import { ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { MayHaveLabel } from '@model/base-api.model';
import { of } from 'rxjs';
import { UiModalCommonService } from 'src/app/modules/common-modal/services/ui-modal-common.service';
import { UiModalWarningUnChangesService } from 'src/app/modules/common-modal/services/ui-modal-warning-unchanges.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { UiTreeClickEvent } from '../../../modules/ui-tree-list/models/ui-tree-list.model';
import { UiStateService } from '../../../shared/services/ui-state.service';
import { AttributeTranslationConfigComponent } from '../components/attribute-config-translation/attribute-config-translation.component';
import { AttributeTranslations } from '../models/attribute-translation.model';
import { AttributeTranslationService } from '../services/attribute-translation.service';
import { AttributeTranslationContainerComponent } from './attribute-translation-container.component';

describe('AttributeTranslationContainerComponent', () => {
  const clickedData: UiTreeClickEvent<MayHaveLabel> = {
    selectedItem: { id: 'id' },
    path: [],
    selectedItemHasChildrens: false,
  };

  const translation: AttributeTranslations = {
    path: 'path',
    translations: { es_ES: 'test español', ca_ES: 'catalan', en_US: 'ingles' },
  };

  let _apySpy: { detail: jasmine.Spy };
  let componentToTest: AttributeTranslationContainerComponent;

  let _modalWarning: { showWarningAttributesTranslationsUnSave: jasmine.Spy };
  let _uiStateService: { closeMenuIfWindowSizeSmall: jasmine.Spy };
  let _modalWarningUnchanges: { showWarningAttributesTranslationsUnSave: jasmine.Spy };
  beforeEach(() => {
    _apySpy = jasmine.createSpyObj('attributeTranslationService', ['detail']);
    _apySpy.detail.and.returnValue(of(translation));

    _modalWarning = jasmine.createSpyObj('UiModalCommonService', ['showWarningAttributesTranslationsUnSave']);
    _modalWarning.showWarningAttributesTranslationsUnSave.and.returnValue(of(true));

    _modalWarningUnchanges = jasmine.createSpyObj('UiModalWarningUnChangesService', ['showWarningAttributesTranslationsUnSave']);
    _modalWarningUnchanges.showWarningAttributesTranslationsUnSave.and.returnValue(of(true));

    _uiStateService = jasmine.createSpyObj('UiStateService', ['closeMenuIfWindowSizeSmall']);
    _uiStateService.closeMenuIfWindowSizeSmall.and.returnValue(undefined);

    componentToTest = new AttributeTranslationContainerComponent(
      _apySpy as unknown as AttributeTranslationService,
      _modalWarning as unknown as UiModalCommonService,
      _modalWarningUnchanges as unknown as UiModalWarningUnChangesService,
      _uiStateService as unknown as UiStateService
    );

    componentToTest.formComponent = new AttributeTranslationConfigComponent(
      new FormBuilder(),
      null as unknown as AttributeTranslationService,
      null as unknown as CrudOperationsService,
      null as unknown as ChangeDetectorRef,
      null as unknown as ToastService,
      []
    );
  });

  it('it should create ', () => {
    expect(componentToTest).toBeTruthy();
  });

  it('onClickedTree actualize selectedAttribute and compute translations', () => {
    componentToTest.selectedAttribute$.subscribe((att) => {
      expect(att?.id).toBe('id');
    });
    componentToTest.selectedTranslation$.subscribe((tr) => {
      expect(tr.path).toBe('path');
    });
    componentToTest.onClickedTree(clickedData);
  });

  it('onClickedTree if we click on a root then we hides the right region', () => {
    const i = 0;
    componentToTest.onClickedTree(clickedData);
  });

  it('if form translations is dirty show modal when changing att ', () => {
    componentToTest.formComponent.formTranslations.markAsDirty();
    componentToTest.selectedAttribute$.subscribe((att) => {
      expect(att?.id).toBe('id2');
    });
    componentToTest.onClickedTree({
      ...clickedData,
      selectedItem: { id: 'id2' },
    });
  });

  it('if form translations is dirty and cancel modal does not change the right form translations ', () => {
    const _modalWarningCancel = jasmine.createSpyObj('UiModalCommonService', ['showWarningAttributesTranslationsUnSave']);
    _modalWarningCancel.showWarningAttributesTranslationsUnSave.and.returnValue(of(false));
    componentToTest.uiModalCommonService = _modalWarningCancel;

    componentToTest.formComponent.formTranslations.markAsDirty();
    componentToTest.selectedAttribute$.subscribe((att) => {
      expect(att?.id).toBe('idOld');
    });
    componentToTest.selectedAttributeSubject.next({ id: 'idOld' });
    componentToTest.onClickedTree({
      ...clickedData,
      selectedItem: { id: 'id2' },
    });
  });

  it('reset the search of products reset the form ', () => {
    componentToTest.selectedAttribute$.subscribe((att) => {
      expect(att).toBeNull();
    });
    componentToTest.onNewProductSearch();
  });

  it('postSubmitted refresh the translations to the saved data ', () => {
    componentToTest.selectedTranslation$.subscribe((data) => {
      expect(data).toEqual(translation);
    });
    componentToTest.postSubmitted(translation);
  });
});
