import { ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { AttributeTranslations } from '../../models/attribute-translation.model';
import { AttributeTranslationService } from '../../services/attribute-translation.service';
import { AttributeTranslationConfigComponent } from './attribute-config-translation.component';

describe('AttributeTranslationConfigComponent', () => {
  let componentToTest: AttributeTranslationConfigComponent;

  const translation: AttributeTranslations = {
    path: 'path',
    translations: { es_ES: 'test español', ca_ES: 'catalan', en_US: 'ingles' },
  };

  beforeEach(() => {
    componentToTest = new AttributeTranslationConfigComponent(
      new FormBuilder(),
      null as unknown as AttributeTranslationService,
      null as unknown as CrudOperationsService,
      null as unknown as ChangeDetectorRef,
      null as unknown as ToastService,
      []
    );
  });

  it('component set formAttribute with the incoming attribute ', () => {
    componentToTest.selectedAttribute = {
      id: 'id',
      label: 'label',
      evaluation_info: {
        path: 'path',
      },
    } as AttributeEntityType;

    const formAttributeValue = componentToTest.formAttribute.value;
    expect(formAttributeValue.attributeId).toBe('id');
    expect(formAttributeValue.attributeLabel).toBe('label');
  });

  it('component set formTranslations with the incoming translation ', () => {
    componentToTest.translation = translation;
    const formValue = componentToTest.formTranslations.value.translations;
    expect(formValue).toEqual(translation.translations);
  });

  it('parse remove empty fields and with only spaces', () => {
    const empty: AttributeTranslations['translations'] = {
      a: '',
      b: '   ',
    };
    let val = componentToTest.parse(empty);
    expect(val).toBeNull();

    const trimmed: AttributeTranslations['translations'] = {
      a: 'a ',
      b: '   b  ',
    };
    let trimmedVal = componentToTest.parse(trimmed);
    if (trimmedVal) {
      expect(trimmedVal?.a).toBe('a');
      expect(trimmedVal?.b).toBe('b');
    } else {
      expect(true).toBeFalsy();
    }
  });

  it('submit event save ', () => {
    const _utilsComponent: { updateOrSaveModal: jasmine.Spy } = jasmine.createSpyObj('UtilsComponent', ['updateOrSaveModal']);
    _utilsComponent.updateOrSaveModal.and.returnValue(of(true));
    componentToTest.utils = _utilsComponent as unknown as CrudOperationsService;

    componentToTest.selectedAttribute = {
      id: 'id',
      label: 'label',
      evaluation_info: {
        path: 'path',
      },
    } as AttributeEntityType;
    componentToTest.translation = translation;

    componentToTest.submit();
    const formTranslationValue = componentToTest.formTranslations.value;
    const formAttributeValue = componentToTest.formAttribute.value;

    expect(_utilsComponent.updateOrSaveModal).toHaveBeenCalled();
    const [[_, sendedValue, __]] = _utilsComponent.updateOrSaveModal.calls.allArgs();
    const screenValue = {
      id: formAttributeValue.attributeId,
      name: formAttributeValue.attributeLabel,
      path: formAttributeValue.path,
      translations: formTranslationValue.translations,
    };
    expect(sendedValue).toEqual(screenValue);
  });

  it('submit event save with empty record calls to delete', () => {
    const _utilsComponent: { deleteActionModal: jasmine.Spy } = jasmine.createSpyObj('UtilsComponent', ['deleteActionModal']);
    _utilsComponent.deleteActionModal.and.returnValue(of(true));
    componentToTest.utils = _utilsComponent as unknown as CrudOperationsService;

    componentToTest.selectedAttribute = {
      id: 'id',
      label: 'label',
      evaluation_info: {
        path: 'path',
      },
    } as AttributeEntityType;
    componentToTest.translation = translation;

    componentToTest.formTranslations.controls['translations'].setValue({ a: '' });
    componentToTest.submit();
    expect(_utilsComponent.deleteActionModal).toHaveBeenCalled();
  });
});
