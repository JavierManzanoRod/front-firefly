import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { FF_LANGUAGES } from 'src/app/configuration/tokens/language.token';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { AttributeTranslations } from '../../models/attribute-translation.model';
import { AttributeTranslationService } from '../../services/attribute-translation.service';
@Component({
  selector: 'ff-attribute-config-translation',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './attribute-config-translation.component.html',
  styleUrls: ['./attribute-config-translation.component.scss'],
})
export class AttributeTranslationConfigComponent implements OnInit {
  @Input() loading!: boolean;
  @Input() hiddenConfig!: boolean;
  @Input() readonly = false;
  @Output() saved = new EventEmitter<AttributeTranslations>();

  @Input()
  public set selectedAttribute(newVal: AttributeEntityType) {
    this.formAttribute.patchValue({
      attributeLabel: newVal?.label,
      attributeId: newVal?.id,
      path: newVal?.evaluation_info?.path,
    });
    this.formTranslations.patchValue({
      translations: null,
    });
    this.formTranslations.markAsPristine();
    this.hasAttribute = !!newVal;
  }

  @Input()
  public set translation(newVal: AttributeTranslations) {
    this.hasTranslations = !!newVal;
    this.formTranslations.patchValue({
      translations: newVal?.translations || null,
    });
    this.formTranslations.markAsPristine();
  }

  numberLang = this.languages.length;

  formAttribute: FormGroup;
  formTranslations: FormGroup;
  isCollapsedDefinition = false;
  isCollapsedDetail = false;
  hasAttribute = false;
  hasTranslations = false;

  constructor(
    private fb: FormBuilder,
    private readonly apiService: AttributeTranslationService,
    public utils: CrudOperationsService,
    private change: ChangeDetectorRef,
    private toast: ToastService,
    @Inject(FF_LANGUAGES) public languages: string[]
  ) {
    this.formAttribute = this.fb.group({
      attributeId: '',
      attributeLabel: '',
      path: null,
    });

    this.formTranslations = this.fb.group({
      translations: [null],
    });

    this.formTranslations.valueChanges.subscribe((values) => {
      if (values.translations === null) {
        this.formTranslations.markAsPristine();
      }
    });
  }

  ngOnInit() {
    if (this.readonly) {
      this.formAttribute.disable();
      this.formTranslations.disable();
    }
  }

  public parse(translation: AttributeTranslations['translations']): AttributeTranslations['translations'] | null {
    const parsedTranslation: AttributeTranslations['translations'] = {};
    let hasValue = false;
    if (translation) {
      // eslint-disable-next-line guard-for-in
      for (const lang in translation) {
        const val = translation[lang];
        if (val !== null && val !== undefined && val !== '') {
          const trimmed = val.trim();
          if (trimmed.length) {
            parsedTranslation[lang] = trimmed;
            hasValue = true;
          }
        }
      }
    }
    return hasValue ? parsedTranslation : null;
  }

  submit() {
    const parsedTranslation: AttributeTranslations = {
      id: this.formAttribute.value.attributeId,
      name: this.formAttribute.value.attributeLabel,
      path: this.formAttribute.value.path,
      translations: this.parse(this.formTranslations.value.translations),
    };

    if (parsedTranslation.translations === null) {
      this.utils.deleteActionModal(this.apiService, parsedTranslation).subscribe((error) => {
        if (!error) {
          this.formTranslations.markAsPristine();
        }
        this.hasTranslations = false;
      });
    } else {
      this.utils
        .updateOrSaveModal(
          {
            idKey: 'path',
            apiService: this.apiService,
            methodToApply: this.hasTranslations ? 'PUT' : 'POST',
          },
          parsedTranslation
        )
        .subscribe(() => {
          this.formTranslations.markAsPristine();
          this.saved.emit(parsedTranslation);
        });
    }
  }

  expanded() {
    this.change.markForCheck();
  }
}
