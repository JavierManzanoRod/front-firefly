import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { ContentGroupDetailRuleContainerComponent } from './containers/content-group-detail-rule-container.component';
import { ContentGroupListContainerComponent } from './containers/content-group-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: ContentGroupListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'rule/:routeId',
        component: ContentGroupDetailRuleContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          breadcrumb: [
            {
              label: 'SHIPPING_COST.BREADCRUMB_LIST_TITLE',
              url: '/catalog/content-group',
            },
            {
              label: 'SHIPPING_COST.SHIPPING_COST',
              url: '',
            },
          ],
        },
      },
      {
        path: 'rule/:routeId/:id',
        component: ContentGroupDetailRuleContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          breadcrumb: [
            {
              label: 'SHIPPING_COST.BREADCRUMB_LIST_TITLE',
              url: '/catalog/content-group',
            },
            {
              label: 'SHIPPING_COST.SHIPPING_COST',
              url: '',
            },
          ],
        },
      },
    ],
  },
  {
    path: ':routeId',
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
    },
    canDeactivate: [CanDeactivateGuard],
    component: ContentGroupListContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContentGroupRoutingModule {}
