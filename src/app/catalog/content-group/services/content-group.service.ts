import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { FF_CONTENT_GROUP } from 'src/app/configuration/tokens/admin-group-type.token';
import { FF_API_PATH_VERSION_CONTENT_GROUPS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { AdminGroupTypeService } from 'src/app/shared/services/apis/admin-group-type.service';
@Injectable({
  providedIn: 'root',
})
export class ContentGroupService extends AdminGroupTypeService {
  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_CONTENT_GROUPS) protected version: string,
    @Inject(FF_CONTENT_GROUP) protected idContentGroup: string
  ) {
    super(http, version, idContentGroup);
  }

  protected parseResult(_data: AdminGroup): string {
    return `{"content_group":"content_group"}`;
  }
}
