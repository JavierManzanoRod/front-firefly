import { HttpClient } from '@angular/common/http';
import { GenericApiResponse } from '@model/base-api.model';
import { ContentGroupDetailDTO } from '../model/content-group-detail-dto.models';
import { ContentGroupFolderService } from './content-group-folder.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: ContentGroupFolderService;

const expectedData = {
  folders: {
    identifier: 'a',
    name: 'a',
    parent_id: 'a',
  },
} as unknown as ContentGroupDetailDTO;
const expectedList: GenericApiResponse<ContentGroupDetailDTO> = {
  content: [expectedData],
  page: null as any,
};

const version = 'v1/';

describe('ContentGroupFolderService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`ContentGroupFolderService points to products/backoffice-admin-group-folders/${version}admin-group-folders`, () => {
    myService = new ContentGroupFolderService(null as unknown as HttpClient, version);
    expect(myService.endPoint).toEqual(`products/backoffice-admin-group-folders/${version}admin-group-folders`);
  });
});
