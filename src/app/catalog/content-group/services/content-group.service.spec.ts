import { HttpClient } from '@angular/common/http';
import { GenericApiResponse } from '@model/base-api.model';
import { of } from 'rxjs';
import { ContentGroupRule } from '../model/content-group.models';
import { ContentGroupService } from './content-group.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: ContentGroupService;

const expectedData = {
  folders: {
    id: 'a',
    name: 'a',
    is_active: true,
  },
} as unknown as ContentGroupRule;
const expectedList: GenericApiResponse<ContentGroupRule> = {
  content: [expectedData],
  page: null as any,
};

const version = 'v1/';

describe('ContentGroupService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it('ContentGroupService.list calls to get api method', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new ContentGroupService(httpClientSpy as any, version, 'id');
    myService.list(null as any).subscribe((response) => {});
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
