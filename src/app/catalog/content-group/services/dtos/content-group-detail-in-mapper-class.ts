import { ContentGroupAdminGroupDTO, ContentGroupDetailDTO, ContentGroupFolderSaveDTO } from '../../model/content-group-detail-dto.models';
import { ContentGroupAdminGroup, ContentGroupDetail, ContentGroupFolderSave } from '../../model/content-group.models';

export class ContentGroupDetailInMapper {
  data = {} as ContentGroupDetailDTO;

  constructor(remoteData: ContentGroupDetail) {
    this.data = {
      identifier: remoteData.id,
      name: remoteData.name,
      category_rules: remoteData.category_rules,
      parent_id: remoteData.parent_id,
      expanded: remoteData.expanded,
      folders: this.transformFolders(remoteData?.folders),
      admin_groups: this.transformAdminGroup(remoteData.admin_groups),
    } as unknown as ContentGroupDetailDTO;
  }

  private transformAdminGroup(item: ContentGroupAdminGroup[] | undefined): ContentGroupAdminGroupDTO[] | undefined {
    if (item) {
      return item.map(({ id, ...data }) => {
        return { ...data, identifier: id };
      });
    }
  }

  private transformFolders(item: ContentGroupFolderSave[] | undefined): ContentGroupFolderSaveDTO[] | undefined {
    if (item) {
      return item.map(({ id, ...data }) => {
        return { ...data, identifier: id };
      });
    }
  }
}
