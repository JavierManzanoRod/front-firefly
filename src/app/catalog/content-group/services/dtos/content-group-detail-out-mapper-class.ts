import { ContentGroupAdminGroupDTO, ContentGroupDetailDTO, ContentGroupFolderSaveDTO } from '../../model/content-group-detail-dto.models';
import { ContentGroupAdminGroup, ContentGroupDetail, ContentGroupFolderSave } from '../../model/content-group.models';
export class ContentGroupDetailOutMapper {
  data = {} as ContentGroupDetail;

  constructor(data: ContentGroupDetailDTO) {
    this.data = {
      id: data.identifier,
      name: data.name,
      category_rules: data.category_rules,
      folders: this.transformFolders(data.folders),
      admin_groups: this.transformAdminGroups(data.admin_groups),
      parent_id: data.parent_id,
    } as unknown as ContentGroupDetail;
  }

  private transformAdminGroups(items: ContentGroupAdminGroupDTO[] | undefined): ContentGroupAdminGroup[] | undefined {
    if (items) {
      return items.map(({ identifier, ...data }) => {
        return { ...data, id: identifier };
      });
    }
  }

  private transformFolders(items: ContentGroupFolderSaveDTO[] | undefined): ContentGroupFolderSave[] | undefined {
    if (items) {
      return items.map(({ identifier, ...data }) => {
        return { ...data, id: identifier };
      });
    }
  }
}
