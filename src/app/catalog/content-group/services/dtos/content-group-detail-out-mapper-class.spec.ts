import { ContentGroupDetailDTO } from '../../model/content-group-detail-dto.models';
import { ContentGroupDetailOutMapper } from './content-group-detail-out-mapper-class';

describe('content-group mapper out', () => {
  it('parse a outgoing request as expected', () => {
    const remoteItem: ContentGroupDetailDTO = {
      identifier: '3',
      name: 'Subcarpeta 1',
      folders: [
        {
          identifier: '3432',
          name: 'Ejemplo',
          parent_id: 'parent_1',
        },
        {
          identifier: '7894',
          name: 'Ejemplo2',
          parent_id: 'parent_1',
        },
      ],
      admin_groups: [
        {
          identifier: 'admin_groups_1',
          name: 'Regla 1',
        },
        {
          identifier: 'admin_groups_2',
          name: 'Regla 2',
        },
      ],
    } as ContentGroupDetailDTO;
    const mapper = new ContentGroupDetailOutMapper(remoteItem);
    const group = mapper.data;
    expect(group.id).toEqual('3');
    expect(group.name).toEqual('Subcarpeta 1');
    expect(group.folders).toEqual([
      {
        id: '3432',
        name: 'Ejemplo',
        parent_id: 'parent_1',
      },
      {
        id: '7894',
        name: 'Ejemplo2',
        parent_id: 'parent_1',
      },
    ]);
    expect(group.admin_groups).toEqual([
      {
        id: 'admin_groups_1',
        name: 'Regla 1',
      },
      {
        id: 'admin_groups_2',
        name: 'Regla 2',
      },
    ]);
  });

  it('parse an incoming group with some fields null or undefined does not break anything', () => {
    const remoteItem: ContentGroupDetailDTO = {
      parent_id: undefined,
      folders: [],
      admin_groups: null,
    } as unknown as ContentGroupDetailDTO;

    const mapper = new ContentGroupDetailOutMapper(remoteItem);
    const group = mapper.data;
    expect(group.parent_id).toEqual(undefined);
    expect(group.folders).toEqual([]);
    expect(group.admin_groups).toEqual(undefined);
  });
});
