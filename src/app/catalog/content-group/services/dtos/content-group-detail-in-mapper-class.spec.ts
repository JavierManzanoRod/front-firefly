import { ContentGroupDetail } from '../../model/content-group.models';
import { ContentGroupDetailInMapper } from './content-group-detail-in-mapper-class';

describe('content-group-detail mapper in', () => {
  it('parse an incoming content group as expected', () => {
    const remoteItem: ContentGroupDetail = {
      id: '3',
      name: 'Subcarpeta 1',
      folders: [
        {
          id: '3432',
          name: 'Ejemplo',
          parent_id: 'parent_1',
        },
        {
          id: '7894',
          name: 'Ejemplo2',
          parent_id: 'parent_1',
        },
      ],
      admin_groups: [
        {
          id: 'admin_groups_1',
          name: 'Regla 1',
        },
        {
          id: 'admin_groups_2',
          name: 'Regla 2',
        },
      ],
    } as ContentGroupDetail;

    const mapper = new ContentGroupDetailInMapper(remoteItem);
    const group = mapper.data;
    expect(group.identifier).toEqual('3');
    expect(group.name).toEqual('Subcarpeta 1');
    expect(group.folders).toEqual([
      {
        identifier: '3432',
        name: 'Ejemplo',
        parent_id: 'parent_1',
      },
      {
        identifier: '7894',
        name: 'Ejemplo2',
        parent_id: 'parent_1',
      },
    ]);
    expect(group.admin_groups).toEqual([
      {
        identifier: 'admin_groups_1',
        name: 'Regla 1',
      },
      {
        identifier: 'admin_groups_2',
        name: 'Regla 2',
      },
    ]);
  });

  it('parse an incoming group with some fields null or undefined does not break anything', () => {
    const remoteItem: ContentGroupDetail = {
      parent_id: undefined,
      folders: [],
      admin_groups: null,
    } as unknown as ContentGroupDetail;

    const mapper = new ContentGroupDetailInMapper(remoteItem);
    const group = mapper.data;
    expect(group.parent_id).toEqual(undefined);
    expect(group.folders).toEqual([]);
    expect(group.admin_groups).toEqual(undefined);
  });
});
