import { ContentGroupDTO } from '../../model/content-group-detail-dto.models';
import { ContentGroup } from '../../model/content-group.models';

export class ContentGroupInMapper {
  data = {} as ContentGroupDTO;

  constructor(remoteData: ContentGroup) {
    this.data = {
      identifier: remoteData.id,
      name: remoteData.name,
      parent_id: remoteData.parent_id,
      expanded: remoteData.expanded,
    } as unknown as ContentGroupDTO;
  }
}
