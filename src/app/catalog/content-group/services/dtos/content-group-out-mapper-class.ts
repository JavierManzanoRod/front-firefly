import { ContentGroupDTO } from '../../model/content-group-detail-dto.models';
import { ContentGroup } from '../../model/content-group.models';

export class ContentGroupOutMapper {
  data = {} as ContentGroup;

  constructor(remoteData: ContentGroupDTO) {
    this.data = {
      id: remoteData.identifier,
      name: remoteData.name,
      parent_id: remoteData.parent_id,
      expanded: remoteData.expanded,
    } as unknown as ContentGroup;
  }
}
