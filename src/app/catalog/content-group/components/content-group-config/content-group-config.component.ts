import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { ContentGroupTreeEvents } from '../../model/content-group-tree.models';
import { ContentGroupDetail, ContentGroupsParams } from '../../model/content-group.models';
import { ContentGroupFolderService } from '../../services/content-group-folder.service';
import { ContentGroupService } from '../../services/content-group.service';

@Component({
  selector: 'ff-content-group-config',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './content-group-config.component.html',
  styleUrls: ['./content-group-config.component.scss'],
})
export class ContentGroupConfigComponent implements OnInit {
  changeValue = false;
  @Input()
  get params(): ContentGroupsParams | null {
    return this._params;
  }

  set params(params: ContentGroupsParams | null) {
    this._params = params;
    this.ngOnInit();
  }

  loading = false;

  data: ContentGroupDetail | null = null;

  type = ContentGroupTreeEvents;
  folderParent$: Observable<any> | null = null;
  private _params: ContentGroupsParams | null = null;
  constructor(public service: ContentGroupService, public serviceFolder: ContentGroupFolderService, private ref: ChangeDetectorRef) {}

  idNumber(index = 1) {
    const arr = this._params?.routeId?.split('|');
    return arr ? arr[arr.length - index] : false;
  }

  loadData() {
    if (this._params?.id && !this._params?.new && this.data?.id !== this._params?.id) {
      this.loading = true;
      const obs = this.serviceFolder.detail(this._params?.id);
      obs
        .pipe(
          finalize(() => {
            this.loading = false;
            this.folderParent$ = this.data?.parent_id ? this.serviceFolder.detail(this.data?.parent_id) : of(null);
            this.ref.detectChanges();
          })
        )
        .subscribe((v) => {
          this.data = v;
          this.ref.detectChanges();
        });
    }
  }

  get isDirty(): UICommonModalConfig | false {
    if (!this.changeValue) {
      return false;
    }

    if (this._params?.new) {
      return {};
    }

    return {
      name: this.data?.name,
    };
  }

  ngOnInit(): void {
    if (this._params?.new) {
      this.folderParent$ = this.serviceFolder.detail(this.idNumber() || '');
    }
    this.loadData();
  }
}
