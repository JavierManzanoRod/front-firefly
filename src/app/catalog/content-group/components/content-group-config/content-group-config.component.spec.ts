import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { TranslateModule } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { ContentGroupsParams } from '../../model/content-group.models';
import { ContentGroupFolderService } from '../../services/content-group-folder.service';
import { ContentGroupService } from '../../services/content-group.service';
import { ContentGroupConfigComponent } from './content-group-config.component';

describe('ContentGroupConfigComponent', () => {
  let component: ContentGroupConfigComponent;
  let fixture: ComponentFixture<ContentGroupConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ContentGroupConfigComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: ContentGroupService,
          useFactory: () => ({
            eventsTree: new Subject(),
            params: {},
            detail: () => {},
          }),
        },
        {
          provide: ContentGroupFolderService,
          useFactory: () => ({}),
        },
        {
          provide: CrudOperationsService,
          useFactory: () => ({}),
        },
        {
          provide: Router,
          useFactory: () => ({}),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentGroupConfigComponent);
    component = fixture.componentInstance;
    component.params = {} as unknown as ContentGroupsParams;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
