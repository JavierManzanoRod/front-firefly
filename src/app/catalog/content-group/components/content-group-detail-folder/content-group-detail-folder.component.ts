/* eslint-disable @typescript-eslint/no-unsafe-call */
import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, Output, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { ExplodedTreeNode } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { ContentGroupFolderService } from '../../services/content-group-folder.service';

@Component({
  selector: 'ff-content-group-folder-detail',
  templateUrl: './content-group-detail-folder.component.html',
  styleUrls: ['./content-group-detail-folder.component.scss'],
})
export class ContentGroupFolderDetailComponent implements OnChanges {
  @ViewChild('inputRef') inputRef!: ElementRef<HTMLInputElement>;
  @Input() folder!: ExplodedTreeNodeComponent;
  @Input() showConfigSection = false;
  @Input() link = '/catalog/content-group/';
  @Output() valueChange = new EventEmitter();
  @Output() folderChange = new EventEmitter();
  @Output() deleteFolder = new EventEmitter();
  @Output() cleanInput = new EventEmitter();
  focused = false;
  isEditable = false;
  dbclickData!: {
    timeStamp: number;
    target?: EventTarget;
  };

  constructor(
    public router: Router,
    private change: ChangeDetectorRef,
    private renderer: Renderer2,
    public apiService: ContentGroupFolderService,
    public crud: CrudOperationsService
  ) {}

  ngOnChanges() {}

  newRule() {
    this.folder.clickAction({ type: 'new' });
  }

  newFolder() {
    if (this.folder) {
      this.folder.data.value = { child: true };
      this.cleanInput.emit(true);
      this.isEditable = !this.isEditable;
    }
    this.change.markForCheck();
    setTimeout(() => {
      this.renderer.selectRootElement(this.inputRef.nativeElement).focus();
    });
  }

  collapseParent() {
    this.isEditable = false;
  }

  save() {
    const folderToSend = {
      name: this.inputRef.nativeElement.value,
      parent_id: this.folder.data.id,
    };
    this.crud
      .updateOrSaveModal(
        {
          methodToApply: 'POST',
          apiService: this.apiService,
          captureCancel: true,
        },
        folderToSend
      )
      .subscribe((response) => {
        if (!response) {
          this.isEditable = true;
        } else {
          this.isEditable = false;

          this.folder.data.children?.unshift({
            id: response.data.id,
            name: response.data.name,
            parent_id: response.data.parent_id,
          });
          this.change.markForCheck();
          this.valueChange.emit(this.folder);
        }
      });
    this.change.markForCheck();
  }

  input(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.save();
    }
    if (event.key === 'Escape') {
      this.isEditable = false;
    }
  }

  blur() {
    this.focused = false;
    if (this.isEditable) {
      return false;
    }
    setTimeout(() => {
      this.save();
    }, 100);
  }

  ruleDetail(element: ExplodedTreeNodeComponent, event: MouseEvent) {
    element.clickAction({ type: 'detail' }, event);
  }

  deleteRule(element: ExplodedTreeNode) {
    this.deleteFolder.emit(element);
  }

  dbclick(event: MouseEvent, element: ExplodedTreeNodeComponent) {
    const { timeStamp, target } = this.dbclickData || { timeStamp: 0, target: undefined };

    if (!element.data.value) {
      if (event.timeStamp - timeStamp < 400 && target === event.target) {
        this.folderChange.emit(element);
      }
    }

    this.dbclickData = {
      timeStamp: event.timeStamp,
      target: event.target ? event.target : undefined,
    };
  }

  update() {
    this.change.markForCheck();
  }
}
