import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { ContentGroupFolderDetailComponent } from './content-group-detail-folder.component';

describe('ContentGroupDetailFolderComponent', () => {
  let router: Router;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ContentGroupFolderDetailComponent],
        imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule],
        providers: [{ provide: CrudOperationsService, useValue: {} }],
      }).compileComponents();

      router = TestBed.inject(Router);
    })
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(ContentGroupFolderDetailComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });
});
