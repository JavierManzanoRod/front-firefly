import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { ContentGroupFolderService } from 'src/app/catalog/content-group/services/content-group-folder.service';
import { ContentGroupService } from 'src/app/catalog/content-group/services/content-group.service';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { ContentGroupListContainerComponent } from './content-group-list-container.component';

// starts global mocks

class TranslateServiceStub {
  public setDefaultLang() {}
  public get(key: any): any {
    of(key);
  }
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string): any {
    return query;
  }
}

@Component({
  selector: 'ff-content-group-list',
  template: '<p>Mock Product Editor Component</p>',
})
class MockListComponent {
  @Input() loading!: boolean;
  @Input() list: any;
  @Input() page: any;
  @Input() currentData: any;
}

@Component({
  selector: 'ff-page-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockHeaderComponent {
  @Input() pageTitle: any;
  @Input() breadcrumbTitle: any;
  @Input() breadcrumbLink: any;
}

@Component({
  selector: 'ff-list-search',
  template: '<p>Mock Product Editor Component</p>',
})
class MockSearchComponent {
  @Input() pageTitle: any;
}

class ToastrServiceStub {
  public success() {}
}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };
  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }
  public hide() {
    return true;
  }
}

// eslint-disable-next-line prefer-const
let apiSpy: { list: jasmine.Spy; detail: jasmine.Spy; post: jasmine.Spy; update: jasmine.Spy; delete: jasmine.Spy };

// end global mocks

describe('ContentGroupListContainerComponent', () => {
  beforeEach(
    waitForAsync(() => {
      apiSpy = jasmine.createSpyObj('ContentGroupService', ['list', 'detail', 'post', 'update', 'delete']);
      apiSpy.list.and.returnValue(of([{ a: 1 }]));

      TestBed.configureTestingModule({
        declarations: [ContentGroupListContainerComponent, TranslatePipeMock, MockListComponent, MockHeaderComponent, MockSearchComponent],
        imports: [RouterTestingModule],
        providers: [
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: ToastService, useClass: ToastrServiceStub },
          { provide: BsModalService, useClass: ModalServiceMock },
          { provide: ContentGroupService, useValue: apiSpy },
          { provide: CrudOperationsService, useFactory: () => ({}) },
          { provide: ContentGroupService, useFactory: () => ({}) },
          {
            provide: ContentGroupFolderService,
            useFactory: () => ({
              listFolders: () => of(null),
            }),
          },
        ],
      }).compileComponents();
    })
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(ContentGroupListContainerComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });
});
