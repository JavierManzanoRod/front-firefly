import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { CanComponentDeactivate } from '@core/guards/can-deactivate-guard';
import { EntityType } from '@core/models/entity-type.model';
import { CrudOperationsService, groupType } from '@core/services/crud-operations.service';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, delay, tap } from 'rxjs/operators';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { FF_CONTENT_GROUP } from 'src/app/configuration/tokens/admin-group-type.token';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { RulesType } from 'src/app/modules/conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { TreeFoldersStateService } from 'src/app/modules/tree-folders/services/tree-folders-state.service';
import { AdminGroupTypeService } from 'src/app/rule-engine/admin-group-type/services/admin-group-type.service';
import { ContentGroupRule, ContentGroupsParams } from '../model/content-group.models';
import { ContentGroupService } from '../services/content-group.service';
import { ConditionsBasicAndAdvanceComponent } from './../../../modules/conditions-basic-and-advance/conditions-basic-and-advance.component';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<ff-page-header
      [pageTitle]="'CONTENT_GROUP.HEADER_MENU'"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'CONTENTGROUP.BREAD_CRUMB_TITLE' | translate"
    >
      <button class="btn btn-dark mr-2" (click)="clone(detail)" *ngIf="state.params?.id">
        {{ 'COMMON.CLONE' | translate }}
      </button>
      <button class="btn btn-dark mr-2" (click)="delete(detail)" *ngIf="state.params?.id">
        {{ 'COMMON.DELETE' | translate }}
      </button>
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <div class="row">
        <div class="col-12">
          <ff-conditions-basic-and-advance
            #detailElement
            (formSubmit)="handleSubmitForm($event)"
            (cancel)="cancel()"
            [loading]="loading"
            [item]="detail || {}"
            [idAdminGroup]="idContentGroup"
            [entityTypes]="entityTypes"
            [type]="type"
          ></ff-conditions-basic-and-advance>
        </div>
      </div>
    </ngx-simplebar> `,
  providers: [{ provide: TreeFoldersStateService, useClass: TreeFoldersStateService }],
})
export class ContentGroupDetailRuleContainerComponent
  extends BaseDetailContainerComponent<ContentGroupRule>
  implements OnInit, CanComponentDeactivate
{
  @ViewChild('detailElement') detailElement!: ConditionsBasicAndAdvanceComponent;
  @Output() changed = new EventEmitter();
  // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
  urlListado = '/catalog/content-group/';
  params: ContentGroupsParams | null = null;
  entityTypes: EntityType[] = [];

  type = RulesType.content_group;

  constructor(
    public service: ContentGroupService,
    public state: TreeFoldersStateService,
    public serviceAdminGroup: AdminGroupTypeService,
    public activeRoute: ActivatedRoute,
    private ref: ChangeDetectorRef,
    public crudSrv: CrudOperationsService,
    public menuLeft: MenuLeftService,
    public toastService: ToastService,
    public router: Router,
    @Inject(FF_CONTENT_GROUP) public idContentGroup: string
  ) {
    super(crudSrv, menuLeft, activeRoute, router, service, toastService);
  }

  ngOnInit() {
    this.state.params = this.activeRoute.snapshot.params as ContentGroupsParams;
    this.activeRoute.paramMap.subscribe((data) => {
      this.state.params = {
        routeId: data.get('routeTree'),
        tempRoute: data.get('routeTree'),
        new: !!data.get('new'),
        id: data.get('id'),
      };
      this.params = this.state.params;
      this.ref.detectChanges();
    });
    // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
    this.urlListado = '/catalog/content-group';
    let detail$: Observable<ContentGroupRule | null>;
    if (this.state.params?.id) {
      detail$ = this.service.detail(this.state.params?.id);
    } else {
      detail$ = of(null).pipe(delay(250));
    }

    forkJoin({
      detail: detail$,
      entityTypes: this.serviceAdminGroup.entityTypes(this.idContentGroup),
    }).subscribe(
      ({ detail, entityTypes }) => {
        this.detail = detail as ContentGroupRule;
        this.entityTypes = entityTypes;
        this.loading = false;
        this.ref.detectChanges();
      },
      () => {
        this.ref.detectChanges();
        this.router.navigateByUrl(this.urlListado).then(() => {
          this.toastService.error('COMMON_ERRORS.NOT_FOUND_DESCRIPTION', 'COMMON_ERRORS.NOT_FOUND_TITLE');
        });
      }
    );
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.activeRoute.parent });
  }

  folderId(item: ContentGroupRule) {
    const arrayId = this.router.url.split('/') || [];
    if (item.id) {
      return arrayId[arrayId.length - 2];
    } else {
      return arrayId[arrayId.length - 1];
    }
  }

  handleSubmitForm(itemToSave: ContentGroupRule) {
    itemToSave.folder_id = this.folderId(itemToSave);
    this.crudSrv
      .updateOrSaveModal(
        {
          ...this,
          methodToApply: itemToSave.id ? 'PUT' : 'POST',
        },
        itemToSave,
        groupType.RULE
      )
      .pipe(
        tap((response) => {
          this.detailElement.haveChanges = false;
          if (!itemToSave.id) {
            this.router.navigate([`${this.urlListado}/rule/${itemToSave.folder_id}/${response.data?.id}`]);
          }
          this.changed.emit(response.data);
        }),
        catchError((e: HttpErrorResponse) => {
          if (e.status === 409) {
            this.detailElement.form.controls.name.setErrors({ 409: true });
          }
          return of(null);
        })
      )
      .subscribe();
  }

  canDeactivate(): UICommonModalConfig | false {
    if (this.detailElement.isDirty) {
      if (!this.detail) {
        return {};
      }
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.detailElement?.item.name,
      };
    }
    return false;
  }

  clone(detail: ContentGroupRule | undefined) {
    if (detail) {
      detail = this.detailElement.getDataToSend() as unknown as ContentGroupRule;
      detail.folder_id = this.folderId(detail);
    }
    if (this.detailElement.isDirty) {
      this.toastService.error('COMMON_ERRORS.CANNOT_CLONE');
      return;
    }
    if (this.detailElement?.form?.valid) {
      super.clone(detail, `${this.urlListado}/rule/${detail?.folder_id}`);
    } else {
      this.crudSrv.toast.warning('COMMON_ERRORS.CLONE_FORM_ERROR');
    }
  }
}
