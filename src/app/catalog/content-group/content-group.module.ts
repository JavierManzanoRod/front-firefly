import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ControlPanelSharedModule } from 'src/app/control-panel/shared/control-panel-shared.module';
import { ConditionsBasicAndAdvanceModule } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.module';
import { ConditionsBasicModule } from 'src/app/modules/conditions-basic/conditions-basic.module';
import { Conditions2Module } from 'src/app/modules/conditions2/conditions2.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { ExplodedTreeModule } from 'src/app/modules/exploded-tree/exploded-tree.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { TreeFoldersModule } from 'src/app/modules/tree-folders/tree-folders.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ContentGroupConfigComponent } from './components/content-group-config/content-group-config.component';
import { ContentGroupFolderDetailComponent } from './components/content-group-detail-folder/content-group-detail-folder.component';
import { ContentGroupDetailRuleContainerComponent } from './containers/content-group-detail-rule-container.component';
import { ContentGroupListContainerComponent } from './containers/content-group-list-container.component';
import { ContentGroupRoutingModule } from './content-group-routing.module';

@NgModule({
  declarations: [
    ContentGroupConfigComponent,
    ContentGroupListContainerComponent,
    ContentGroupDetailRuleContainerComponent,
    ContentGroupFolderDetailComponent,
  ],
  imports: [
    CommonModule,
    ControlPanelSharedModule,
    ContentGroupRoutingModule,
    SharedModule,
    SimplebarAngularModule,
    FormsModule,
    ConditionsBasicAndAdvanceModule,
    Conditions2Module,
    FormErrorModule,
    TreeFoldersModule,
    ReactiveFormsModule,
    DatetimepickerModule,
    ExplodedTreeModule,
    ConditionsBasicModule,
  ],
  providers: [CrudOperationsService],
})
export class ContentGroupModule {}
