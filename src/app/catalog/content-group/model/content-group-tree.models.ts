import { CategoryRulesTree } from 'src/app/modules/tree-folders/models/tree-folders.model';
import { ContentGroupAdminGroup } from './content-group.models';

export interface ContentGroupTree {
  name: string;
  id: string;
  folders?: ContentGroupTree[];
  category_rules?: CategoryRulesTree[];
  admin_groups?: ContentGroupAdminGroup[];
  expanded?: boolean;
  root?: boolean;
  tempFolder?: boolean;
  tempRule?: boolean;
}

export interface ContentGroupTreeDataEvents {
  data?: ContentGroupTree | null;
  parent?: ContentGroupTree | null;
  event: ContentGroupTreeEvents;
}

export enum ContentGroupTreeEvents {
  folder = 'folder',
  rule = 'rule',
  cancelTree = 'cancelTree',
  cancelConfig = 'cancelConfig',
  refreshRoot = 'refreshRoot',
  saveFolder = 'saveFolder',
  refreshFolder = 'refreshFolder',
}
