import {
  Condition2SimpleDataTypes,
  Conditions2,
  Conditions2LeafTypes,
  Conditions2NodeTypes,
  Conditions2ReferenceNodeType,
  ConditionsSimpleEntityInfo,
} from 'src/app/modules/conditions2/models/conditions2.model';

export interface Conditions2NodeDTO {
  [key: string]: any;
  type: Conditions2NodeTypes.and | Conditions2NodeTypes.or;
  range?: boolean;
  nodes: Conditions2DTO[];
}

export interface Condition2AttributeDTO {
  identifier: string;
  reference?: Condition2AttributeDTO;
  is_multiple_cardinality?: boolean;
  is_recursive_filter?: boolean;
  entity_type_reference_id?: string;
  is_i18n?: boolean;
  label?: string;
  name?: string;
  is_label_attribute?: boolean;
  data_type?: string;
}

export interface Condition2SimpleBaseDTO {
  attribute_id: string;
  attribute_label: string;
  entity_type_id: string;
  entity_type_name: string;
  tooltip_label?: string;
  is_multiple_cardinality: boolean;
  attribute: Condition2AttributeDTO;
  range?: boolean;
  type:
    | Conditions2LeafTypes.eq
    | Conditions2LeafTypes.like
    | Conditions2LeafTypes.distinct
    // | Conditions2LeafTypes.NOT_START_WITH
    // | Conditions2LeafTypes.NOT_START_WITH_IGNORE_CASE
    | Conditions2LeafTypes.gt
    | Conditions2LeafTypes.lt
    | Conditions2LeafTypes.GREATER_THAN_OR_EQUALS
    | Conditions2LeafTypes.LESS_THAN_OR_EQUALS
    | Conditions2LeafTypes.in
    | Conditions2LeafTypes.notin
    | Conditions2LeafTypes.NOT_LIKE
    | Conditions2LeafTypes.INCLUDE_ALL
    | Conditions2LeafTypes.INCLUDE_ANY
    | Conditions2LeafTypes.NOT_INCLUDE_ALL
    | Conditions2LeafTypes.NOT_INCLUDE_ANY
    | Conditions2LeafTypes.START_WITH
    | Conditions2LeafTypes.START_WITH_IGNORE_CASE
    | Conditions2LeafTypes.IS_DEFINED
    | Conditions2LeafTypes.IS_NOT_DEFINED
    | Conditions2LeafTypes.LIKE_IGNORE_CASE
    | Conditions2LeafTypes.MATCH_ALL
    | Conditions2LeafTypes.MATCH_ANY
    | Conditions2LeafTypes.NOT_LIKE_IGNORE_CASE;
}

export interface Condition2SimpleStringDTO extends Condition2SimpleBaseDTO {
  attribute_data_type: Condition2SimpleDataTypes.STRING;
  value?: string | string[] | null;
}

export interface Condition2SimpleDoubleDTO extends Condition2SimpleBaseDTO {
  attribute_data_type: Condition2SimpleDataTypes.DOUBLE;
  value?: number | number[] | null;
}

export interface Condition2SimpleBooleanDTO extends Condition2SimpleBaseDTO {
  attribute_data_type: Condition2SimpleDataTypes.BOOLEAN;
  value?: boolean | null;
}

export interface Condition2SimpleChildsDTO extends Condition2SimpleBaseDTO {
  attribute_data_type: Condition2SimpleDataTypes.EMBEDDED | Condition2SimpleDataTypes.ENTITY;
  nodes: Conditions2[];
  value?: any;
}

export interface Condition2SimpleEntityDTO extends Condition2SimpleBaseDTO {
  attribute_data_type: Condition2SimpleDataTypes.ENTITY;
  entity_reference: string;
  entity_reference_name: string;
  value?: string | string[] | null | { id: string; name: string } | { id: string; name: string }[];
  is_recursive_filter: boolean;
  entity_info: {
    [label: string]: ConditionsSimpleEntityInfo;
  };
}

export interface Condition2ReferenceDTO {
  type: Conditions2ReferenceNodeType.reference;
  reference_id: string;
  name: string;
  range?: boolean;
  reference_name?: string;
}

export type Condition2SimpleDTO =
  | Condition2SimpleStringDTO
  | Condition2SimpleBooleanDTO
  | Condition2SimpleDoubleDTO
  | Condition2SimpleEntityDTO
  | Condition2SimpleChildsDTO;

export const enum Conditions2ReferenceNodeTypeDTO {
  reference = 'reference',
}

export enum Conditions2LeafTypesDTO {
  eq = 'eq',
  distinct = 'distinct',
  like = 'like',
  NOT_LIKE = 'NOT_LIKE',
  gt = 'gt',
  lt = 'lt',
  in = 'in',
  notin = 'notin',
  INCLUDE_ALL = 'INCLUDE_ALL',
  NOT_INCLUDE_ALL = 'NOT_INCLUDE_ALL',
  INCLUDE_ANY = 'INCLUDE_ANY',
  NOT_INCLUDE_ANY = 'NOT_INCLUDE_ANY',
  START_WITH = 'START_WITH',
  IS_DEFINED = 'IS_DEFINED',
  IS_NOT_DEFINED = 'IS_NOT_DEFINED',
  LIKE_IGNORE_CASE = 'LIKE_IGNORE_CASE',
  NOT_LIKE_IGNORE_CASE = 'NOT_LIKE_IGNORE_CASE',
  START_WITH_IGNORE_CASE = 'START_WITH_IGNORE_CASE',
  MATCH_ANY = 'MATCH_ANY',
  MATCH_ALL = 'MATCH_ALL',
}

export type Conditions2DTO = Conditions2NodeDTO | Condition2SimpleDTO | Condition2ReferenceDTO | Condition2MultipleDTO;

export interface Condition2MultipleDTO {
  type:
    | Conditions2LeafTypes.MATCH_ALL
    | Conditions2LeafTypes.MATCH_ANY
    | Conditions2LeafTypes.NOT_INCLUDE_ANY
    | Conditions2LeafTypes.INCLUDE_ANY;
  attribute: Condition2AttributeDTO;
  nodes?: Conditions2NodeDTO[];
  is_recursive_filter?: boolean;
  value?: string[];
  entity_type_name?: string;
  entity_type_id?: string;
  is_multiple_cardinality?: boolean;
  entity_reference?: string;
  entity_type_reference_id?: string;
  label?: string;
  data_type?: Condition2SimpleDataTypes;
  identifier?: string;
}

export interface ContentGroupRuleDTO {
  identifier?: string;
  name: string;
  is_basic: boolean;
  start_date?: string | Date;
  start_time?: string | Date;
  end_date?: string | Date;
  end_time?: string | Date;
  is_active: boolean;
  node?: Conditions2DTO;
  result: string;
  folder_id: string;
  parent_id: string;
}
