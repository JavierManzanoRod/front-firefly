import { CategoryRulesTree } from 'src/app/modules/tree-folders/models/tree-folders.model';

export interface ContentGroupFolderSaveDTO {
  identifier: string;
  name: string;
  parent_id: string;
  expanded?: boolean;
}

export interface ContentGroupDTO {
  identifier: string;
  name: string;
  parent_id?: string;
  expanded?: boolean;
}

export interface ContentGroupAdminGroupDTO {
  identifier: string;
  name: string;
  tempRule?: boolean;
}

export interface ContentGroupDetailDTO extends ContentGroupDTO {
  folders: ContentGroupFolderSaveDTO[];
  admin_groups?: ContentGroupAdminGroupDTO[];
  category_rules?: CategoryRulesTree[];
}
