import { Conditions2 } from 'src/app/modules/conditions2/models/conditions2.model';
import { CategoryRulesTree } from 'src/app/modules/tree-folders/models/tree-folders.model';

export interface ContentGroupFolderSave {
  id: string;
  name: string;
  parent_id: string;
  expanded?: boolean;
}
export interface ContentGroup {
  id: string;
  name: string;
  parent_id?: string;
  expanded?: boolean;
}
export interface ContentGroupDetail extends ContentGroup {
  folders: ContentGroupFolderSave[];
  admin_groups?: ContentGroupAdminGroup[];
  category_rules?: CategoryRulesTree[];
}

export interface ContentGroupAdminGroup {
  id: string;
  name: string;
  tempRule?: boolean;
}
export interface ContentGroupsParams {
  categoryId?: string | null;
  routeId: string | null;
  tempRoute: string | null;
  loadTree?: boolean;
  new: boolean | null;
  id: string | null;
}
export interface ContentGroupRule {
  id?: string;
  name: string;
  start_date?: string | Date;
  is_basic: boolean;
  start_time?: string | Date;
  end_date?: string | Date;
  end_time?: string | Date;
  active: boolean;
  node?: Conditions2;
  result: string;
  folder_id?: string;
  parent_id?: string;
  admin_group_type_id?: string;
}
