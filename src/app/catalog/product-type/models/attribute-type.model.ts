import { AttributeEntityType } from '@core/models/entity-type.model';

export interface AttributeType {
  id?: string;
  label?: string;
  children?: AttributeChildren[];
  entity_id?: string;
}

export interface AttributeChildren {
  node_id?: string;
  field_path?: string;
  label?: string;
  node_type?: string;
  element_type?: string;
  children?: AttributeChildren[];
}

export interface AttributeEntityTypeTreeFacet extends AttributeEntityType {
  file_path?: string;
}
