import { AttributeEntityType, EntityType } from '@core/models/entity-type.model';
import { GenericApiResponse, MayHaveLabel } from '@model/base-api.model';
import { UITree, UITreeEvent } from 'src/app/modules/ui-tree/models/ui-tree.model';

export interface ProductTypeDTO {
  id: string;
  label: string;
  name: string;
  product_type_id: string;
  attribute_ids: string[];
}

export interface ProductTypeDTOIN {
  identifier: string;
  label: string;
  name: string;
  subtype: string;
  entity_type_ids: string[];
}

export interface ProductTypeTmpResolved {
  id: string;
  label: string;
  name: string;
  product_type_id: string;
  attributes?: MayHaveLabel[];
}

export interface AttributeTreeNodeDTO1 {
  id: string;
  path: string;
  name: string;
  parent_attribute_tree_node_id?: string;
  attributes_id?: string[];
}
export interface AttributeTreeNodeDTOIN {
  identifier: string;
  path: string;
  name: string;
  parent_attribute_tree_node_id?: string;
  attributes_id?: string[];
}

export interface AttributeTreeNodeTmpReslolved {
  id: string;
  attribute_id?: string;
  path: string;
  name: string;
  parent_attribute_tree_node_id?: string;
  attributes?: MayHaveLabel[];
}

export interface ProductTypeView {
  id?: string;
  name?: string;
  attributes?: MayHaveLabel[];
  children?: ProductTypeView[];
}

export interface AtrributeTreeResolvedView {
  entityType: EntityType;
  productType: ProductTypeView;
  attsTreeNodes: AttributeTreeNodeTmpReslolved[];
  attsAsTree?: UITree;
}

export interface AtrributeTreeResolvedViewContainer {
  [key: string]: AtrributeTreeResolvedView;
}

export interface SelectedLeaf {
  productTypeId: string;
  atributeId: string;
}

export type ProductTypeResponse = GenericApiResponse<ProductTypeDTO>;
export type ProductTypeResponseIn = GenericApiResponse<ProductTypeDTOIN>;
export type AttributeTreeNodeDTO = GenericApiResponse<AttributeTreeNodeDTO1>;
export type ListAttributeTreeNodeDTOIN = GenericApiResponse<AttributeTreeNodeDTOIN>;

export interface VmProductTypeAdditionalData {
  entityType: EntityType;
  productType: ProductTypeView;
}
export interface VmProducTypeList {
  treeProducts: UITree[];
  loadingSearchProducts: boolean;
  productList: ProductTypeDTO[];
  productListRelatedData?: VmProductTypeAdditionalData[];
}

interface EventProductListOutputI {
  productList: ProductTypeDTO;
  entityType: EntityType;
  attributeSelected: AttributeEntityType;
  event: UITreeEvent;
}

export type EventProductListOutput = Partial<EventProductListOutputI>;
