import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UiTreeListModule } from 'src/app/modules/ui-tree-list/ui-tree-list.module';
import { UiTreeModule } from 'src/app/modules/ui-tree/ui-tree.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductTypeList2Component } from './components/product-type-list-2/product-type-list2.component';
import { ProductTypeListComponent } from './components/product-type-list/product-type-list.component';

@NgModule({
  imports: [SharedModule, CommonModule, UiTreeModule, FormsModule, ReactiveFormsModule, UiTreeListModule],
  declarations: [ProductTypeListComponent, ProductTypeList2Component],
  providers: [],
  exports: [ProductTypeListComponent, ProductTypeList2Component],
})
export class ProductTypeModule {}
