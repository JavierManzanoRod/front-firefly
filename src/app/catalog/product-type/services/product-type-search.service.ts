import { Injectable } from '@angular/core';
import { EntityType } from '@core/models/entity-type.model';
import { forkJoin, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { configuration } from '../../../configuration/configuration';
import { ProductTypeDTO } from '../models/product-type.model';
import { ProductTypeApiService } from './apis/product-type-api.service';

@Injectable({
  providedIn: 'root',
})
export class ProductTypeSearchService {
  private cachedEntityType: EntityType | null = null;
  private cachedProductCPS: ProductTypeDTO | null = null;
  private cacheSearchProductCPS = '';
  private idProductCPS = 'idProductCPS';

  constructor(private productTypeApi: ProductTypeApiService, private entityTypeApiService: EntityTypeService) {}

  searchWithEntityType(term: string): Observable<Array<ProductTypeDTO | EntityType>> {
    return forkJoin({
      entityType: this.searchEntityType(term),
      rest: this.searchNoProductCPS(term),
    }).pipe(
      map(({ entityType, rest }) => {
        return entityType ? [entityType, ...rest] : [...rest];
      })
    );
  }

  search(term: string): Observable<Array<ProductTypeDTO>> {
    return forkJoin({
      productCPS: this.searchProductCPS(term),
      rest: this.searchNoProductCPS(term),
    }).pipe(
      map(({ productCPS, rest }) => {
        return productCPS ? [productCPS, ...rest] : [...rest];
      })
    );
  }

  private getProductCPS(): Observable<ProductTypeDTO> {
    if (this.cachedProductCPS) {
      return of(this.cachedProductCPS);
    } else {
      return this.productTypeApi.detail(this.idProductCPS).pipe(
        tap((product) => {
          this.cachedProductCPS = product;
          this.cacheSearchProductCPS = ((product.label || '') + product.name).toLowerCase();
        })
      );
    }
  }

  private searchProductCPS(term: string): Observable<ProductTypeDTO | null> {
    return this.getProductCPS().pipe(
      map((product) => {
        const term1 = term.toLowerCase();
        if (this.cacheSearchProductCPS.indexOf(term1) > -1) {
          return product;
        } else {
          return null;
        }
      })
    );
  }

  private sortCaseInsensitive(xs: ProductTypeDTO[]): ProductTypeDTO[] {
    interface ProductTypeDTOSorteable extends ProductTypeDTO {
      labelLower: string;
    }

    const comparatorInsensitive = (a: ProductTypeDTOSorteable, b: ProductTypeDTOSorteable): number => {
      if (a.labelLower < b.labelLower) {
        return -1;
      } else if (b.labelLower > a.labelLower) {
        return 1;
      } else {
        return 0;
      }
    };

    const dataToSort: ProductTypeDTOSorteable[] = xs.map((x) => ({
      ...x,
      labelLower: (x.label || '').toLowerCase(),
    }));

    const sorted = [...dataToSort].sort(comparatorInsensitive);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return sorted.map(({ labelLower, ...rest }) => rest);
  }

  private getEntityType() {
    if (this.cachedEntityType) {
      return of(this.cachedEntityType);
    }
    return this.entityTypeApiService.detail(configuration.offerEntityType || '').pipe(
      tap((entityType) => {
        this.cachedEntityType = entityType;
      })
    );
  }

  private searchEntityType(term: string) {
    return this.getEntityType().pipe(
      map((entityType) => {
        const name = ((entityType.label || '') + entityType.name).toLowerCase();
        if (name.indexOf(term.toLowerCase()) > -1) {
          return entityType;
        }
        return null;
      })
    );
  }

  private searchNoProductCPS(term: string): Observable<ProductTypeDTO[]> {
    return forkJoin({
      secondaries: this.productTypeApi.list({ name: term, size: 9999, page: 0, subtype: 'SECONDARY' }),
      store: this.productTypeApi.list({ name: term, size: 9999, page: 0, subtype: 'STORE' }),
    }).pipe(
      map(({ secondaries, store }) => {
        return [...secondaries.content, ...store.content];
      }),
      map((arrProducts) => {
        // return this.sortCaseInsensitive(arrProducts);
        return arrProducts.sort((a, b) => {
          if (a.label > b.label) {
            return 1;
          } else if (a.label < b.label) {
            return -1;
          } else {
            return 0;
          }
        });
      })
    );
  }
}
