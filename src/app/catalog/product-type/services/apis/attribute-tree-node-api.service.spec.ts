import { of } from 'rxjs';
import { AttributeTreeNodeApiService } from './attribute-tree-node-api.service';

const version = 'v1/';
describe('AttributeTreeNodeApiService', () => {
  let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };
  let serviceToTest: AttributeTreeNodeApiService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`AttributeTreeNodeApiService points to products/backoffice-admin-groups/${version}admin-groups`, () => {
    const service = new AttributeTreeNodeApiService(null as any, version);
    expect(service.endPoint).toEqual(`products/backoffice-attribute-tree-nodes/${version}tree-nodes-types`);
  });

  it('AttributeTreeNodeApiService detail calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(
      of({
        content: [],
      })
    );
    serviceToTest = new AttributeTreeNodeApiService(httpClientSpy as any, version);
    serviceToTest.detail('someId').subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });
});
