import { of } from 'rxjs';
import { ProductTypeApiService } from './product-type-api.service';

const version = 'v1/';

describe('ProductTypeApiService', () => {
  let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };
  let serviceToTest: ProductTypeApiService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`ProductTypeApiService points to products/backoffice-product-types/${version}product-types`, () => {
    const service = new ProductTypeApiService(null as any, version);
    expect(service.endPoint).toEqual(`products/backoffice-product-types/${version}product-types`);
  });

  it('ProductTypeApiService detail calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(
      of({
        content: [],
      })
    );
    serviceToTest = new ProductTypeApiService(httpClientSpy as any, version);
    serviceToTest.detail('someId').subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });
});
