/* eslint-disable @typescript-eslint/no-unsafe-call */
import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, IApiService2 } from '@core/base/api.service';
import { environment } from '@env/environment';
import { GenericApiRequest } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_PRODUCT_TYPES } from 'src/app/configuration/tokens/api-versions.token';
import { ProductTypeDTO, ProductTypeDTOIN, ProductTypeResponse, ProductTypeResponseIn } from '../../models/product-type.model';

const urlBase = `${environment.API_URL_BACKOFFICE}`;

interface ProductTypeDTORequest extends GenericApiRequest {
  subtype?: 'STORE' | 'PRIMARY' | 'SECONDARY';
}

@Injectable({
  providedIn: 'root',
})
export class ProductTypeApiService extends ApiService<ProductTypeDTO> implements IApiService2<ProductTypeDTO> {
  endPoint = `products/backoffice-product-types/${this.version}product-types`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_PRODUCT_TYPES) private version: string) {
    super();
  }

  getParams(filter: ProductTypeDTORequest): HttpParams {
    let params: HttpParams = new HttpParams();
    if (filter) {
      params = filter.page_number ? params.set('page_number', filter.page_number.toString()) : params;
      params = filter.page_size ? params.set('page_size', filter.page_size.toString()) : params;
      params = filter.size ? params.set('size', filter.size.toString()) : params;
      params = filter.page ? params.set('page', filter.page.toString()) : params;
      params = filter.name ? params.set('name', filter.name.toString()) : params;
      params = filter.subtype ? params.set('subtype', filter.subtype.toString()) : params;
    }

    return params;
  }

  list(filter: ProductTypeDTORequest): Observable<ProductTypeResponse> {
    const params = super.getFilter(filter);
    const url = urlBase + '/' + this.endPoint;
    return this.http.get<ProductTypeResponseIn>(url, { params: this.getParams(params) }).pipe(
      map((res: any) => {
        res.content = res.content.map((value: ProductTypeDTOIN) => this.convertIn(value));
        return res as ProductTypeResponse;
      })
    );
  }

  detail(id: string): Observable<ProductTypeDTO> {
    const options = {
      headers: {
        'Accept-Language': 'es-ES,es;q=0.9,en;q=0.8',
      },
    };
    const urlDetail = `${this.urlBase}/${id}`;
    return this.http.get<ProductTypeDTOIN>(urlDetail, options).pipe(map((v) => this.convertIn(v)));
  }

  private convertIn(value: ProductTypeDTOIN): ProductTypeDTO {
    return {
      id: value.identifier,
      label: value.label,
      name: value.name,
      product_type_id: value.subtype,
      attribute_ids: value.entity_type_ids,
    };
  }
}
