import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_ATTRIBUTE_TREE_NODE } from 'src/app/configuration/tokens/api-versions.token';
import { AttributeTreeNodeDTO, AttributeTreeNodeDTO1, AttributeTreeNodeDTOIN } from '../../models/product-type.model';

const urlBase = `${environment.API_URL_BACKOFFICE}`;

@Injectable({
  providedIn: 'root',
})
export class AttributeTreeNodeApiService {
  endPoint = `products/backoffice-attribute-tree-nodes/${this.version}tree-nodes-types`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_ATTRIBUTE_TREE_NODE) private version: string) {}

  get urlBase() {
    return urlBase + '/' + this.endPoint;
  }

  detail(id: string): Observable<AttributeTreeNodeDTO> {
    const urlDetail = `${this.urlBase}/${id}`;
    return this.http.get<any>(urlDetail).pipe(
      map((v: any) => {
        v.content = v.content.map((value: AttributeTreeNodeDTOIN) => {
          return {
            id: value.identifier,
            path: value.path,
            name: value.name,
            attributes_id: value.attributes_id,
            parent_attribute_tree_node_id: value.parent_attribute_tree_node_id,
          } as AttributeTreeNodeDTO1;
        });
        return v as AttributeTreeNodeDTO;
      })
    );
  }
}
