import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { GenericApiRequest } from '@model/base-api.model';
import { FF_API_PATH_VERSION_EXPLODED_TREE } from 'src/app/configuration/tokens/api-versions.token';
import { AttributeType } from '../../models/attribute-type.model';

@Injectable({
  providedIn: 'root',
})
export class FacetAttributeService extends ApiService<AttributeType> {
  endPoint = `products/backoffice-exploded-trees/${this.version}exploded-trees`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_EXPLODED_TREE) private version: string) {
    super();
  }

  find(filter: GenericApiRequest) {
    return super.list(filter);
  }
}
