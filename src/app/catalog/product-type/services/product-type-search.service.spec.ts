// import { GenericApiResponse } from '@model/base-api.model';
// import { of } from 'rxjs';
// import { ProductTypeDTO } from '../models/product-type.model';
// import { ProductTypeApiService } from './apis/product-type-api.service';
// import { ProductTypeSearchService } from './product-type-search.service';

// describe('ProductTypeSearchService', () => {

//   let _apySpy: { detail: jasmine.Spy, list: jasmine.Spy };
//   let serviceToTest: ProductTypeSearchService;
//   const productCPs = {
//     id: 'id',
//     label: 'Product_CPS'
//   } as ProductTypeDTO;

//   const responseList = {
//     content: [
//     {
//       id: 'Z',
//       label: 'Z'
//     },

//     {
//       id: 'P',
//       label: 'Product'
//     },
//     {
//       id: 'B',
//       label: 'B'
//     },
//     {
//       id: 'A',
//       label: 'A'
//     }
//   ]} as GenericApiResponse<ProductTypeDTO>;

//   beforeEach(() => {
//     _apySpy = jasmine.createSpyObj('ProductTypeApiService', ['detail', 'list']);
//   });

//   it('search with empty term retrieve all', () => {
//     _apySpy.detail.and.returnValue(of(productCPs));
//     _apySpy.list.and.returnValue(of(responseList));

//     serviceToTest = new ProductTypeSearchService(_apySpy as unknown as ProductTypeApiService);
//     serviceToTest.search('').subscribe(
//       data => {
//         expect(data[0].id).toBe(productCPs.id);
//         expect(data[1].label).toBe('A');
//         expect(data[7].label).toBe('Z');

//       }
//     );
//   });

//   it('search with some text filter exclude ProductCPS if not match', () => {
//     _apySpy.detail.and.returnValue(of(productCPs));
//     _apySpy.list.and.returnValue(of(responseList));

//     serviceToTest = new ProductTypeSearchService(_apySpy as unknown as ProductTypeApiService);
//     serviceToTest.search('xxxxxx').subscribe(
//       data => {
//         expect(data.length).toBe(responseList.content.length * 2);
//       }
//     );
//   });

//   it('search with some text filter includes  ProductCPS if match', () => {
//     _apySpy.detail.and.returnValue(of(productCPs));
//     _apySpy.list.and.returnValue(of(responseList));

//     serviceToTest = new ProductTypeSearchService(_apySpy as unknown as ProductTypeApiService);
//     serviceToTest.search('pro').subscribe(
//       data => {
//         expect(data.length).toBe(responseList.content.length * 2 + 1);
//       }
//     );
//   });

// });
