import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { AttributeTreeNodeTmpReslolved } from '../models/product-type.model';
import { AttributeTreeNodeApiService } from './apis/attribute-tree-node-api.service';
import { ProductTypeService } from './product-type.service';

describe('ProductTypeService', () => {
  let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy; request: jasmine.Spy };

  let myService: ProductTypeService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete', 'request']);
  });

  it('ProductTypeService.attributeTreeAsNode record gives me some data', () => {
    myService = new ProductTypeService((null as unknown) as AttributeTreeNodeApiService, (null as unknown) as EntityTypeService);
    const data = {
      id: 'a',
      path: 'as',
      name: 'asd',
    };
    const record = myService.attributeTreeAsNode((data as unknown) as AttributeTreeNodeTmpReslolved);
    expect(record).toBeDefined();
  });
});
