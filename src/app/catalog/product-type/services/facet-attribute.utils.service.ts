import { Injectable } from '@angular/core';
import { UITreeListItem2 } from 'src/app/modules/ui-tree-list/models/ui-tree-list.model';
import { AttributeChildren, AttributeType } from '../models/attribute-type.model';

@Injectable({
  providedIn: 'root',
})
export class FacetAttributeUtilsService {
  constructor() {}

  parseTree(attribute: AttributeType[] | AttributeChildren[]): UITreeListItem2[] {
    const parsedAtt: UITreeListItem2[] = [];
    attribute.forEach((element: any) => {
      if (element.element_type) {
        element.data_type = element.element_type;
        delete element.element_type;
      }
      if (element.node_id) {
        element.name = element.node_id;
        delete element.node_id;
      }
    });
    attribute.forEach((element: AttributeChildren | AttributeType) => {
      parsedAtt.push({
        value: element,
        icon: Object.prototype.hasOwnProperty.call(element, 'node_labels') ? 'icon-folder' : this.iconType(element),
        isOpen: true,
        hide: false,
        childrens: element.children ? this.parseTree(element.children) : undefined,
      });
    });

    return parsedAtt;
  }

  iconType(item: AttributeChildren): string {
    return item.children ? 'icon-atributos' : 'icon-agrupaciones';
  }
}
