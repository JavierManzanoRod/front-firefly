import { Injectable } from '@angular/core';
import { EntityType } from '@core/models/entity-type.model';
import { MayHaveLabel } from '@model/base-api.model';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UITree } from 'src/app/modules/ui-tree/models/ui-tree.model';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import {
  AtrributeTreeResolvedView,
  AttributeTreeNodeTmpReslolved,
  ProductTypeDTO,
  ProductTypeTmpResolved,
} from '../models/product-type.model';
import { AttributeTreeNodeApiService } from './apis/attribute-tree-node-api.service';

@Injectable({
  providedIn: 'root',
})
export class ProductTypeService {
  constructor(private attributeTreeNodeApi: AttributeTreeNodeApiService, private entityTypeApi: EntityTypeService) {}

  public resolveAtts(entityType: EntityType, ids: string[]): MayHaveLabel[] {
    const _idsToResolve = (ids || []).filter((e) => e !== 'assortment');
    return _idsToResolve.map((id) => {
      const attDefintion = entityType.attributes.find((att) => att.id === id);
      if (attDefintion) {
        return {
          id,
          ...attDefintion,
        };
      } else {
        return {
          id,
          name: 'not found' + id,
          label: 'not found' + id,
        };
      }
    });
  }

  productTypeAsTree(productType: ProductTypeDTO): Observable<AtrributeTreeResolvedView> {
    return forkJoin({
      entityType: this.entityTypeApi.detail(productType.id),
      attributeTree: this.attributeTreeNodeApi.detail(productType.id),
    }).pipe(
      map(({ entityType, attributeTree }): AtrributeTreeResolvedView => {
        const attsTreeNodes: AttributeTreeNodeTmpReslolved[] = [];

        const goodAttrIds = entityType.attributes?.map((attr: any) => attr.id);
        console.log(attributeTree, goodAttrIds);
        attributeTree.content.forEach((attTree) => {
          // Limpio de los id q no existen por tanto eran de looktable
          // attTree.attributes_id = attTree.attributes_id.filter((v) => {
          //   return goodAttrIds.indexOf(v) >= 0;
          // });

          attsTreeNodes.push({
            id: attTree.id,
            name: attTree.name,
            path: attTree.path,
            parent_attribute_tree_node_id: attTree.parent_attribute_tree_node_id,
            attributes: sortByLabel(this.resolveAtts(entityType, attTree.attributes_id || [])),
          });
        });
        const productTypeResolved: ProductTypeTmpResolved = {
          id: productType.id,
          label: productType.label,
          name: productType.name,
          product_type_id: productType.product_type_id,
          attributes: sortByLabel(this.resolveAtts(entityType, productType.attribute_ids)),
        };

        return { entityType, productType: productTypeResolved, attsTreeNodes };
      }),
      map((data) => {
        return { ...data, attsAsTree: this.convertArrayToTree(data.attsTreeNodes, data.productType.name || '') };
      })
    );
  }

  public attributeTreeAsNode(data: AttributeTreeNodeTmpReslolved): UITree {
    const { attributes, path, ...rest } = data;
    return {
      value: { label: path, ...rest },
      childrens: attributes ? attributes.map((att) => ({ value: att })) : undefined,
    };
  }

  private rootTreeAsNode(data: AttributeTreeNodeTmpReslolved, rootName: string): UITree {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { attributes, name, ...rest } = data;
    return {
      value: { name: rootName, ...rest },
      childrens: attributes ? attributes.map((att) => ({ value: att })) : undefined,
    };
  }

  private convertArrayToTree(arrToFormat: AttributeTreeNodeTmpReslolved[], rootName: string): UITree {
    const arr = clone(arrToFormat).sort((a, b) => (a.name > b.name ? 1 : -1));

    const tree: UITree = this.rootTreeAsNode(arr[0], rootName);

    for (let i = 1; i < arr.length; i++) {
      const current = arr[i];
      const parentNode = this.find(tree, current.parent_attribute_tree_node_id || '');
      if (parentNode) {
        const currentNode = this.attributeTreeAsNode(current);
        if (Array.isArray(parentNode.childrens)) {
          parentNode.childrens.push(currentNode);
        }
      }
    }

    /*
    tree.childrens = tree.childrens?.filter((v) => {
      return v.childrens && v.childrens?.length > 0;
    });

     */

    return tree;
  }

  private find(tree: UITree, id: string): UITree | null {
    const it = (treeToIterate: UITree) => {
      return {
        *[Symbol.iterator]() {
          yield treeToIterate;
          if (treeToIterate.childrens) {
            for (const children of treeToIterate.childrens) {
              yield* it(children);
            }
          }
        },
      } as any;
    };

    const find = function* (fn: (el: any) => boolean, iterable: any) {
      for (const element of iterable) {
        if (fn(element)) {
          yield element;
          break;
        }
      }
    };

    const data = find((e: any) => e.value?.id === id, it(tree));
    const result = Array.from(data);
    return (result && result[0]) || null;
  }
}

function clone<T>(x: T): T {
  return JSON.parse(JSON.stringify(x)) as T;
}

function sortByLabel(xs: MayHaveLabel[] | null): MayHaveLabel[] | undefined {
  if (!xs) {
    return undefined;
  } else {
    const comparatorInsensitive = (a: MayHaveLabel, b: MayHaveLabel): number => {
      if (a.label && b.label) {
        if (a.label < b.label) {
          return -1;
        } else if (b.label > a.label) {
          return 1;
        } else {
          return 0;
        }
      }
      return 0;
    };
    return xs.sort(comparatorInsensitive);
  }
}
