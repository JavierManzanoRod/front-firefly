/* eslint-disable @typescript-eslint/no-unsafe-return */
import { Injectable } from '@angular/core';
import { AttributeEntityType, EntityType } from '@core/models/entity-type.model';
import { MayHaveLabel } from '@model/base-api.model';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UITree } from 'src/app/modules/ui-tree/models/ui-tree.model';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { UITreeListItem } from '../../../modules/ui-tree-list/models/ui-tree-list.model';
import { UITreeOfferService } from '../../../modules/ui-tree-offer/ui-tree-offer.service';
import {
  AtrributeTreeResolvedView,
  AttributeTreeNodeTmpReslolved,
  ProductTypeDTO,
  ProductTypeTmpResolved,
  ProductTypeView,
} from '../models/product-type.model';
import { AttributeTreeNodeApiService } from './apis/attribute-tree-node-api.service';

@Injectable({
  providedIn: 'root',
})
export class ProductTypeTreeService {
  constructor(
    private attributeTreeNodeApi: AttributeTreeNodeApiService,
    private entityTypeApi: EntityTypeService,
    private uITreeOfferService: UITreeOfferService
  ) {}

  public resolveAtts(entityType: EntityType, ids: string[]): MayHaveLabel[] {
    const _idsToResolve = (ids || []).filter((e) => e !== 'assortment');
    return _idsToResolve.map((id) => {
      const attDefintion = entityType.attributes.find((att) => att.id === id);
      if (attDefintion) {
        return {
          id,
          ...attDefintion,
        } as any;
      }
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  fillResponse(entityType: EntityType, productType: ProductTypeView) {}

  productTypeAsTree(productType: ProductTypeDTO, parentsDragabble = false): Observable<AtrributeTreeResolvedView> {
    // no se utiliza. const t0 = performance.now();
    return forkJoin({
      entityType: this.entityTypeApi.detail(productType.id),
      attributeTree: this.attributeTreeNodeApi.detail(productType.id),
    }).pipe(
      map(({ entityType, attributeTree }): AtrributeTreeResolvedView => {
        const attsTreeNodes: AttributeTreeNodeTmpReslolved[] = [];
        attributeTree.content.forEach((attTree) => {
          // Limpio de los id q no existen por tanto eran de looktable
          /*
          attTree.attributes_id = attTree.attributes_id?.filter((v) => {
            return entityType.attributes.findIndex((e) => e.id === v) !== -1;
          });
           */

          attsTreeNodes.push({
            id: attTree.id,
            attribute_id: attTree.id,
            name: attTree.name,
            path: attTree.path,
            parent_attribute_tree_node_id: attTree.parent_attribute_tree_node_id,
            attributes: sortByLabel(this.resolveAtts(entityType, attTree.attributes_id || [])),
          });
        });
        const productTypeResolved: ProductTypeTmpResolved = {
          id: productType.id,
          label: productType.label,
          name: productType.name,
          product_type_id: productType.product_type_id,
          attributes: sortByLabel(this.resolveAtts(entityType, productType.attribute_ids)),
        };

        return { entityType, productType: productTypeResolved, attsTreeNodes };
      }),
      map((data) => {
        return { ...data, attsAsTree: this.convertArrayToTree(data.attsTreeNodes, data.productType.name || '', parentsDragabble) };
      })
    );
  }

  private attributeTreeAsNode(data: AttributeTreeNodeTmpReslolved, parentsDragabble = false): UITreeListItem {
    const { attributes, path, ...rest } = data;
    return {
      value: {
        label: path,
        ...rest,
      },
      childrens: attributes?.map((att) =>
        this.uITreeOfferService.asTree({ ...att, tooltip_label: data.name } as AttributeEntityType, false, parentsDragabble)
      ),
      draggable: false,
      icon: attributes?.length ? 'icon-atributos' : 'icon-agrupaciones',
    };
  }

  private rootTreeAsNode(data: AttributeTreeNodeTmpReslolved, rootName: string, parentsDragabble = false): UITreeListItem {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { attributes, name, ...rest } = data;
    return {
      value: {
        name: rootName,
        ...rest,
      },
      childrens: attributes?.map((att) =>
        this.uITreeOfferService.asTree({ ...att, tooltip_label: data.name } as AttributeEntityType, false, parentsDragabble)
      ),
      draggable: false,
      icon: attributes?.length ? 'icon-atributos' : 'icon-agrupaciones',
    };
  }

  private convertArrayToTree(arrToFormat: AttributeTreeNodeTmpReslolved[], rootName: string, parentsDragabble = false): UITree {
    const arr = clone(arrToFormat).sort((a, b) => (a.name > b.name ? 1 : -1));

    const tree: UITreeListItem = this.rootTreeAsNode(arr[0], rootName, parentsDragabble);

    for (let i = 1; i < arr.length; i++) {
      const current = arr[i];
      const parentNode = this.find(tree, current.parent_attribute_tree_node_id || '');

      if (parentNode) {
        const currentNode = this.attributeTreeAsNode(current, parentsDragabble);

        if (Array.isArray(parentNode.childrens)) {
          parentNode.childrens.push(currentNode);
          parentNode.icon = 'icon-atributos';
          // parentNode.draggable = false;
        }
      }
    }

    /*
    tree.childrens = tree.childrens?.filter((v) => {
      return v.childrens && v.childrens?.length > 0;
    });
     */

    return tree;
  }

  private find(tree: UITree, id: string): UITreeListItem | null {
    const it = (treeToIterate: UITree) => {
      return {
        *[Symbol.iterator]() {
          yield treeToIterate;
          if (treeToIterate.childrens) {
            for (const children of treeToIterate.childrens) {
              yield* it(children);
            }
          }
        },
      } as any;
    };

    const find = function* (fn: (el: any) => boolean, iterable: any) {
      for (const element of iterable) {
        if (fn(element)) {
          yield element;
          break;
        }
      }
    };

    const data = find((e: any) => e.value?.id === id, it(tree));
    const result = Array.from(data);
    return (result && result[0]) || null;
  }
}

function clone<T>(x: T): T {
  return JSON.parse(JSON.stringify(x)) as T;
}

function sortByLabel(xs: MayHaveLabel[] | null): MayHaveLabel[] | undefined {
  if (!xs) {
    return undefined;
  } else {
    const comparatorInsensitive = (a: MayHaveLabel, b: MayHaveLabel): number => {
      if (a.label && b.label) {
        if (a.label < b.label) {
          return -1;
        } else if (b.label > a.label) {
          return 1;
        } else {
          return 0;
        }
      }
      return 0;
    };
    return xs.sort(comparatorInsensitive);
  }
}
