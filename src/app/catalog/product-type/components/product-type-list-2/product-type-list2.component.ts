import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { Page } from '@model/metadata.model';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { UiTreeClickEvent, UITreeListItem } from 'src/app/modules/ui-tree-list/models/ui-tree-list.model';
import { FacetAttributeService } from '../../services/apis/facet-attribute.service';
import { FacetAttributeUtilsService } from '../../services/facet-attribute.utils.service';
import { ProductTypeSearchService } from '../../services/product-type-search.service';
import { ProductTypeList2StateService } from './product-type-list2.state.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ff-product-type-list2',
  styleUrls: ['product-type-list2.component.scss'],
  templateUrl: 'product-type-list2.component.html',
  providers: [ProductTypeList2StateService],
})
export class ProductTypeList2Component {
  @Input() expandOnAtrtributesEntity = false;
  @Output() eventClickProductTree = new EventEmitter<UiTreeClickEvent<any>>();

  items: UITreeListItem[] | null = [];
  loading = false;
  searchSubject = new Subject<number>();

  productText = '';
  attributeText: string | undefined = '';

  pagination: Page | undefined;

  lastCall = 0;

  constructor(
    public state: ProductTypeList2StateService,
    private facetAttributeApiSrv: FacetAttributeService,
    private productTypeSearchService: ProductTypeSearchService,
    private faceAttributeUtilsSrv: FacetAttributeUtilsService,
    private ref: ChangeDetectorRef
  ) {
    this.searchSubject.pipe(debounceTime(300), distinctUntilChanged()).subscribe((n) => {
      this.loading = true;
      if (!this.pagination) {
        this.items = [];
      }
      this.ref.markForCheck();
      // Si hay algo escrito en attributo lanzamos la busqueda de atributo
      if (this.attributeText) {
        this.facetAttributeApiSrv
          .find({
            attribute_name: this.attributeText,
            products_name: this.productText,
            size: 10,
            page: this.pagination ? this.pagination.page_number + 1 : 0,
          })
          .subscribe((data) => {
            // Solo se meteran los elementos en el arbol si esta llamada es la ultima
            if (this.lastCall === n) {
              this.pagination = data.page;
              if (this.items) {
                this.items = this.items.concat(this.faceAttributeUtilsSrv.parseTree(data.content));
              }
              this.loading = false;
              this.ref.markForCheck();
            }
          });
      } else {
        // Si no, lanzamos la de producto
        this.productTypeSearchService.searchWithEntityType(this.productText).subscribe((productList) => {
          // Solo se meteran los elementos en el arbol si esta llamada es la ultima
          if (this.lastCall === n) {
            this.pagination = undefined;
            this.items = productList.map((product) => this.state.productAsTree(product, this.expandOnAtrtributesEntity));
            this.loading = false;
            this.ref.markForCheck();
          }
        });
      }
    });

    this.search(0);
  }

  search(n: number) {
    this.searchSubject.next(n);
  }

  onSearchByAttributes(text: string) {
    if (text.length > 2) {
      this.pagination = undefined;
      this.attributeText = text;
      this.lastCall++;
      this.search(this.lastCall);
    } else {
      if (text.length === 0 && this.productText.length > 0) {
        this.pagination = undefined;
        this.lastCall++;
        this.search(this.lastCall);
      }
      this.attributeText = undefined;
    }
  }

  onSearchByProduct(text: string) {
    this.pagination = undefined;
    this.productText = text;
    this.lastCall++;
    this.search(this.lastCall);
  }

  loadMore() {
    this.lastCall++;
    this.search(this.lastCall);
  }

  onClickedTree(item: UiTreeClickEvent<any>) {
    this.eventClickProductTree.emit(item);
  }
}
