import { Injectable } from '@angular/core';
import { AttributeEntityType, EntityType } from '@core/models/entity-type.model';
import { Observable, of, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { UITreeListItem } from 'src/app/modules/ui-tree-list/models/ui-tree-list.model';
import { UITree } from 'src/app/modules/ui-tree/models/ui-tree.model';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { AttributeEntityTypeTreeFacet } from '../../models/attribute-type.model';
import { ProductTypeDTO, ProductTypeView } from '../../models/product-type.model';
import { ProductTypeSearchService } from '../../services/product-type-search.service';
import { ProductTypeService } from '../../services/product-type.service';

@Injectable()
export class ProductTypeList2StateService {
  searchProductsSubject = new Subject<string | null>();
  treeProducts$: Observable<UITreeListItem[] | null> = this.searchProductsSubject.asObservable().pipe(
    switchMap((textToSearch) => this.productTypeSearchService.search(textToSearch || '')),
    map((productList) => productList.map((product) => this.productAsTree(product)))
  );
  constructor(
    private productTypeSearchService: ProductTypeSearchService,
    private productTypeService: ProductTypeService,
    private entityTypeApiService: EntityTypeService
  ) {}

  start() {
    this.searchProductsSubject.next('');
  }

  public productAsTree(product: ProductTypeDTO | EntityType, expandOnAtrtributesEntity?: boolean): UITreeListItem {
    if ('attributes' in product) {
      return {
        value: { ...product },
        icon: 'icon-folder',
        childrens: product.attributes.map((e) => this.entityTypeAttributeAsTree(undefined, e)),
      };
    }
    return {
      value: { ...product },
      icon: 'icon-folder',
      loadChildrens: (p: ProductTypeDTO) => this.loadProductTreeHandler(p, expandOnAtrtributesEntity),
    };
  }

  private stampFieldPathEntityType(parent: any, item: any) {
    const parentFilepath = parent?.offer_path || '/offer';
    item.offer_path = `${parentFilepath}/${item.name}`;
    return item;
  }

  private entityTypeAttributeAsTree(attributeToExpand: AttributeEntityTypeTreeFacet | undefined, attribute: AttributeEntityTypeTreeFacet) {
    const result: UITreeListItem = {
      value: attribute,
      icon: 'icon-agrupaciones',
    };

    if (attribute.data_type === 'EMBEDDED' || attribute.data_type === 'ENTITY') {
      result.loadChildrens = (e: AttributeEntityType) => this.loadEntityTypeAttributeChildrens(e);
      result.icon = 'icon-atributos';
    }

    result.value = this.stampFieldPathEntityType(attributeToExpand, attribute);
    return result;
  }

  private loadEntityTypeAttributeChildrens(attributeToExpand: AttributeEntityTypeTreeFacet) {
    console.log('Hola?');
    if (attributeToExpand.entity_reference) {
      return this.entityTypeApiService.detail(attributeToExpand.entity_reference).pipe(
        map((entityType) => {
          console.log(entityType);
          return entityType.attributes.map((att) => this.entityTypeAttributeAsTree(attributeToExpand, att));
        })
      );
    }
    return of([]);
  }

  private loadProductTreeHandler(p: ProductTypeDTO, expandOnAtrtributesEntity?: boolean) {
    return this.resolveTree(p).pipe(
      map(([uiTreeData]) => uiTreeData.childrens),
      map((childrens) => {
        return this.decorateTree(childrens || [], expandOnAtrtributesEntity || false);
      })
    );
  }

  private decorateTree(items: UITreeListItem[], expandOnAtrtributesEntity: boolean): UITreeListItem[] {
    return items.map((item) => {
      item.icon = 'icon-atributos';
      if (
        (item.value.data_type && item.value.data_type.toUpperCase() === 'EMBEDDED') ||
        (expandOnAtrtributesEntity && item.value.data_type && item.value.data_type.toUpperCase() === 'ENTITY')
      ) {
        item.loadChildrens = (itemToExpand) => {
          return this.resolveEmbebbedHandler(itemToExpand, expandOnAtrtributesEntity);
        };
      }
      if (item.childrens) {
        item.childrens = this.decorateTree(item.childrens, expandOnAtrtributesEntity);
      } else if (
        !item.value.data_type ||
        item.value.data_type.toUpperCase() !== 'EMBEDDED' ||
        (expandOnAtrtributesEntity && item.value.data_type.toUpperCase() !== 'ENTITY' && item.value.data_type.toUpperCase() !== 'EMBEDDED')
      ) {
        item.icon = 'icon-agrupaciones';
      }
      return item;
    });
  }

  private resolveEmbebbedHandler(itemToExpand: any, expandOnAtrtributesEntity?: boolean) {
    return this.entityTypeApiService.detail(itemToExpand.entity_reference).pipe(
      map((entityType) => {
        let r: any;
        return entityType.attributes.map((item: any) => {
          r = {
            value: item,
            icon: 'icon-agrupaciones',
          };
          if (
            itemToExpand.data_type &&
            (itemToExpand.data_type.toUpperCase() === 'EMBEDDED' ||
              (expandOnAtrtributesEntity && itemToExpand.data_type.toUpperCase() === 'ENTITY'))
          ) {
            r.value = this.stampFieldPath(itemToExpand, item);
          }
          if (
            item.data_type &&
            (item.data_type.toUpperCase() === 'EMBEDDED' || (expandOnAtrtributesEntity && item.data_type.toUpperCase() === 'ENTITY'))
          ) {
            r.icon = 'icon-atributos';
            r.loadChildrens = (itemToExpand2: any) => {
              console.log('heeelooo loadiong', expandOnAtrtributesEntity);
              return this.resolveEmbebbedHandler(itemToExpand2, expandOnAtrtributesEntity);
            };
          }

          return r;
        });
      })
    );
  }

  private stampFieldPath(parent: any, item: any) {
    let fieldPath = '';
    if (parent.field_path && parent.field_path !== '/') {
      fieldPath = `${parent.field_path}/${item.name}`;
    } else if (!parent.field_path || parent.field_path === '/') {
      fieldPath = `/${item.name}`;
    }
    const result = {
      ...item,
      idAux: parent.idAux || parent.id || '',
      field_path: fieldPath,
    };
    return result;
  }

  private resolveTree(product: ProductTypeDTO): Observable<[UITree, EntityType, ProductTypeView]> {
    return this.productTypeService.productTypeAsTree(product).pipe(
      map((data) => {
        return [data.attsAsTree, data.entityType, data.productType] as any;
      })
    );
  }
}
