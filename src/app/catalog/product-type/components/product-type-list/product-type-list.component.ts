import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { UITree, UITreeEvent } from 'src/app/modules/ui-tree/models/ui-tree.model';
import { EventProductListOutput, ProductTypeDTO, VmProductTypeAdditionalData } from '../../models/product-type.model';
import { ProductTypeStateService } from './product-type-state.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ff-product-type-list',
  styleUrls: ['product-type-list.component.scss'],
  templateUrl: 'product-type-list.component.html',
  providers: [ProductTypeStateService],
})
export class ProductTypeListComponent {
  @Output() eventClickProductTree = new EventEmitter<EventProductListOutput>();
  @Output() eventNewSearch = new EventEmitter<void>();
  @ViewChild('search')
  input!: ElementRef;

  products!: ProductTypeDTO[];
  treeProducts: UITree[] | null = [];
  productList: ProductTypeDTO[] = [];
  productListRelatedData: VmProductTypeAdditionalData[] = [];
  loadingSearchProducts = true;
  errorResolvingProducts: boolean[] = [];
  loadingProducts: boolean[] = [];

  searchSubject = new Subject<string>();

  search$ = this.searchSubject.pipe(debounceTime(300), distinctUntilChanged());

  constructor(private state: ProductTypeStateService, private ref: ChangeDetectorRef) {
    this.state.vm.subscribe((data) => {
      this.treeProducts = data.treeProducts;
      this.productList = data.productList;
      this.productListRelatedData = new Array(this.productList.length);
      this.errorResolvingProducts = new Array(this.productList.length);
      this.loadingProducts = new Array(this.productList.length);
      this.loadingSearchProducts = !!data.loadingSearchProducts;
      this.ref.markForCheck();
    });

    state.createEmptyTree();

    this.search$.subscribe((text) => {
      this.state.searchProductsSubject.next(text);
      this.eventNewSearch.emit();
      this.ref.markForCheck();
    });
  }

  searchProduct(text: string) {
    this.searchSubject.next(text);
    this.ref.markForCheck();
  }

  resolveProduct(index: number) {
    this.errorResolvingProducts[index] = false;
    this.loadingProducts[index] = true;

    this.state.resolveTree(this.productList[index]).subscribe(
      ([tree, additionalData]) => {
        if (this.treeProducts) {
          this.treeProducts[index] = tree;
        }
        this.productListRelatedData[index] = additionalData;
        this.errorResolvingProducts[index] = false;
        this.loadingProducts[index] = false;
        this.ref.markForCheck();
      },
      () => {
        this.errorResolvingProducts[index] = true;
        this.loadingProducts[index] = false;
        this.ref.markForCheck();
      }
    );
  }

  onClickTree(index: number, $event: UITreeEvent) {
    if ($event.eventName === 'init') {
      this.eventClickProductTree.emit({
        productList: undefined,
        entityType: undefined,
        attributeSelected: undefined,
        event: $event,
      });

      return;
    }

    const entityType = this.productListRelatedData[index].entityType;
    const idAttribute = $event.data.id;
    const attributeSelected = entityType.attributes.find((att) => att.id === idAttribute);

    this.eventClickProductTree.emit({
      productList: this.productList[index],
      entityType,
      attributeSelected,
      event: $event,
    });
  }

  trackByFn(index: number, data: UITree): string {
    return data.value.id || '';
  }
}
