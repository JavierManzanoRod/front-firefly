import { Injectable } from '@angular/core';
import { MayHaveLabel } from '@model/base-api.model';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { UITree } from 'src/app/modules/ui-tree/models/ui-tree.model';
import { ProductTypeDTO, VmProductTypeAdditionalData } from '../../models/product-type.model';
import { ProductTypeSearchService } from '../../services/product-type-search.service';
import { ProductTypeService } from '../../services/product-type.service';

const asTree = (x: MayHaveLabel): UITree => ({ value: { ...x } });

@Injectable()
export class ProductTypeStateService {
  loadingSearchProducts$ = new Subject<boolean | null>();
  searchProductsSubject = new Subject<string | null>();
  treeProducts$: Observable<UITree[] | null> = this.searchProductsSubject.asObservable().pipe(
    tap(() => this.loadingSearchProducts$.next(true)),
    switchMap((textToSearch) => this.productTypeSearchService.search(textToSearch || '')),
    tap(() => this.loadingSearchProducts$.next(false)),
    tap((productList) => this.productsSubject.next(productList)),
    map((productList) => productList.map(asTree))
  );

  productsSubject = new Subject<ProductTypeDTO[]>();

  vm = combineLatest([this.treeProducts$, this.loadingSearchProducts$.asObservable(), this.productsSubject.asObservable()]).pipe(
    map(([treeProducts, loadingSearchProducts, productList]) => ({
      treeProducts,
      loadingSearchProducts,
      productList,
    }))
  );
  constructor(private productTypeSearchService: ProductTypeSearchService, private productTypeService: ProductTypeService) {}

  start() {
    this.searchProductsSubject.next('');
  }

  // @Deprecated
  createEmptyTree() {
    this.searchProductsSubject.next('');
  }

  resolveTree(product: ProductTypeDTO): Observable<[UITree, VmProductTypeAdditionalData]> {
    return this.productTypeService.productTypeAsTree(product).pipe(
      map((data) => {
        return [
          data.attsAsTree,
          {
            entityType: data.entityType,
            productType: data.productType,
          },
        ] as any;
      })
    );
  }
}
