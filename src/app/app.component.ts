import { Component, Inject, LOCALE_ID } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PATH_ASSETS } from '@env/custom-variables';
import { TranslateService } from '@ngx-translate/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { map } from 'rxjs/operators';
import { authConfig } from './config/auth.config';
import { ToastService } from './modules/toast/toast.service';
import { UserService } from './shared/services/user.service';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

@Component({
  selector: 'ff-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'front-firefly-backoffice';
  isMenuOpen = true;
  code = this.route.queryParams.pipe(map((params) => params.code && params.state));

  constructor(
    private translate: TranslateService,
    private oauthService: OAuthService,
    protected router: Router,
    private toastService: ToastService,
    private route: ActivatedRoute,
    public userService: UserService,
    @Inject(LOCALE_ID) private localeID: string
  ) {
    translate.setDefaultLang(this.localeID);
    registerLocaleData(localeEs, this.localeID);
    if (window.self === window.top) {
      this.configure();
      this.oauthService.setupAutomaticSilentRefresh();
      this.initAuth();
    }
    document.getElementById('favicon')?.setAttribute('href', `${PATH_ASSETS}favicon.ico`);
  }

  private configure() {
    this.oauthService.configure(authConfig);
  }

  private initAuth() {
    this.oauthService
      .tryLogin()
      .then((success) => {
        const expires_at = this.oauthService.getAccessTokenExpiration();
        this.userService.userSubject.next(this.userService.getUser());

        if (!this.haveAnyRoles) {
          this.router.navigateByUrl('unauthorized');
          return;
        }
        if (success) {
          if (this.oauthService.state && this.oauthService.state !== 'undefined' && this.oauthService.state !== 'null') {
            this.router.navigateByUrl(decodeURIComponent(this.oauthService.state));
          }
        }

        if (!expires_at || expires_at < new Date().getTime()) {
          this.oauthService.initCodeFlow(this.router.url);
        }
      })
      .catch((err) => {
        if (!err?.ok) {
          if (this.userService.user) {
            console.log('Error conectándose al servidor de autenticación');
            this.toastService.error(this.translate.instant('AUTH.ERROR'), '', { disableTimeOut: true });
          }
          setTimeout(() => {
            // wait one second to show properly the toast msg.
            this.oauthService.initCodeFlow(this.router.url);
          }, 1000);
        } else {
          console.log('ok: ', err.ok);
        }
      });
    //
  }

  get haveAnyRoles(): boolean {
    return this.userService.getRolesUser().some((r) => this.userService.getAllRoles().indexOf(r) >= 0);
  }
}
