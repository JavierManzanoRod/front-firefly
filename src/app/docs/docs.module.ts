import { NgModule } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { HighlightModule } from 'ngx-highlightjs';
import { SharedModule } from '../shared/shared.module';
import { MenuDocsModule } from './components/menu-docs/menu-docs.module';
import { DocsContainerComponent } from './containers/doc-container/docs-container.component';
import { DocsRoutingModule } from './docs-routing.module';

@NgModule({
  declarations: [DocsContainerComponent],
  imports: [DocsRoutingModule, SharedModule, MenuDocsModule, TabsModule, HighlightModule],

  exports: [],
  providers: [],
})
export class DocsModule {}
