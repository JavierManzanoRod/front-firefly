import { Component, OnInit } from '@angular/core';
import * as example1HTML from './examples/1/doc-form-error-1.component.html?raw';
import * as example1TS from './examples/1/doc-form-error-1.component.ts?raw';
import * as example2HTML from './examples/2/doc-form-error-2.component.html?raw';
import * as example2TS from './examples/2/doc-form-error-2.component.ts?raw';

@Component({
  selector: 'ff-doc-form-error',
  templateUrl: './doc-form-error.component.html',
  styleUrls: ['./doc-form-error.component.scss'],
})
export class DocFormErrorComponent implements OnInit {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example2 = {
    ts: example2TS.default,
    html: example2HTML.default,
  };

  example1Description = `Ejemplo de como manejar los errores de los formularios usando esta componente.`;

  example2Description = `Ejemplo de como manejar los errores de los formularios usando esta componente con chipComponent. Observa que hay validaciones.`;

  constructor() {}

  ngOnInit(): void {}
}
