import { NgModule } from '@angular/core';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';

@NgModule({
  imports: [FormErrorModule, ChipsControlModule],
})
export class AppModule {}
