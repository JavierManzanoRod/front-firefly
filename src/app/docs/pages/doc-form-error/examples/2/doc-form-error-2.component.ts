import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';

@Component({
  selector: 'ff-doc-form-error-2',
  templateUrl: './doc-form-error-2.component.html',
})
export class DocFormError2Component implements OnInit {
  form = new FormGroup({
    example: new FormControl('', Validators.required),
  });

  chipsConfigExample: ChipsControlSelectConfig = {
    label: 'Añadir textos de 0',
    modalType: ModalChipsTypes.free,
    modalConfig: {
      title: 'Añade los textos que quieras',
    },
  };

  arrayMessage = [
    {
      type: 'required',
      message: 'El campo es requerido',
    },
  ];

  ngOnInit() {
    this.form.markAllAsTouched();
  }

  submit() {}
}
