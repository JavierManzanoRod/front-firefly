import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'ff-doc-form-error-1',
  templateUrl: './doc-form-error-1.component.html',
})
export class DocFormError1Component implements OnInit {
  form = new FormGroup({
    example: new FormControl('', Validators.required),
  });

  arrayMessage = [
    {
      type: 'required',
      message: 'El campo es requerido',
    },
  ];

  ngOnInit() {
    this.form.markAllAsTouched();
  }
}
