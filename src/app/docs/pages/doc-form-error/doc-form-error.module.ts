import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormErrorModule } from '../../../modules/form-error/form-error.module';
import { ImportExampleModule } from '../../components/import-example/import-example.module';
import { DocExampleModule } from './../../components/doc-example/doc-example.module';
import { DocFormErrorComponent } from './doc-form-error.component';
import { DocFormError1Component } from './examples/1/doc-form-error-1.component';
import { DocFormError2Component } from './examples/2/doc-form-error-2.component';

@NgModule({
  declarations: [DocFormErrorComponent, DocFormError1Component, DocFormError2Component],
  imports: [
    CommonModule,
    SharedModule,
    FormErrorModule,
    FormsModule,
    ReactiveFormsModule,
    DocExampleModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocFormErrorComponent,
      },
    ]),
    ImportExampleModule,
    ChipsControlModule,
  ],
})
export class DocFormErrorModule {}
