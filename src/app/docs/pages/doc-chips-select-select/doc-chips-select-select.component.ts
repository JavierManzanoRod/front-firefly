import { Component } from '@angular/core';
import * as example1HTML from './examples/1/doc-chips-select-select-1.component.html?raw';
import * as example1TS from './examples/1/doc-chips-select-select-1.component.ts?raw';
import * as example2HTML from './examples/2/doc-chips-select-select-2.component.html?raw';
import * as example2TS from './examples/2/doc-chips-select-select-2.component.ts?raw';
import * as example3HTML from './examples/3/doc-chips-select-select-3.component.html?raw';
import * as example3TS from './examples/3/doc-chips-select-select-3.component.ts?raw';

@Component({
  selector: 'ff-doc-chips-select',
  templateUrl: './doc-chips-select-select.component.html',
  styleUrls: ['./doc-chips-select-select.component.scss'],
})
export class DocChipsSelectSelectComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example1Description = `Al pinchar en el botón se muestra una modal desde la que poder seleccionar una opción de las que se le pasan.`;

  example2 = {
    ts: example2TS.default,
    html: example2HTML.default,
  };

  example2Description = `Al pinchar en el botón se muestra una modal desde la que poder seleccionar múltiples opciones de las que se le pasan.`;

  example3 = {
    ts: example3TS.default,
    html: example3HTML.default,
  };

  example3Description = `Al pinchar en el botón se muestra una modal vacía, por que esta
  esperando a que escribas en el input y así lanzar una llamada al backend para mostrar
  las opciones devueltas. La tabla de debajo describe que hace cada opción.`;

  constructor() {}
}
