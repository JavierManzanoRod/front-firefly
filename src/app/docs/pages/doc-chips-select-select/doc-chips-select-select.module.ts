import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChipsControlModule } from '../../../modules/chips-control/chips-control.module';
import { FormErrorModule } from '../../../modules/form-error/form-error.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { ImportExampleModule } from '../../components/import-example/import-example.module';
import { DocChipsSelectSelectComponent } from './doc-chips-select-select.component';
import { DocChipsSelectSelect1Component } from './examples/1/doc-chips-select-select-1.component';
import { DocChipsSelectSelect2Component } from './examples/2/doc-chips-select-select-2.component';
import { DocChipsSelectSelect3Component } from './examples/3/doc-chips-select-select-3.component';

@NgModule({
  declarations: [
    DocChipsSelectSelectComponent,
    DocChipsSelectSelect1Component,
    DocChipsSelectSelect2Component,
    DocChipsSelectSelect3Component,
  ],
  imports: [
    SharedModule,
    DocExampleModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocChipsSelectSelectComponent,
      },
    ]),
    ReactiveFormsModule,
    FormErrorModule,
    ChipsControlModule,
    ImportExampleModule,
  ],
  exports: [],
  providers: [],
})
export class DocChipsSelectSelectModule {}
