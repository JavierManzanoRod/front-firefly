import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GenericApiRequest } from '@model/base-api.model';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../../modules/chips-control/components/chips.control.model';

@Component({
  selector: 'ff-doc-chips-select-select-3',
  templateUrl: './doc-chips-select-select-3.component.html',
})
export class DocChipsSelectSelect3Component {
  form: FormGroup;
  chipsSelectConfig: ChipsControlSelectConfig = {
    label: 'Seleccionar',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'Selecciona lo que quieras',

      /** Si es true se pueden seleccionar multiples items */
      multiple: true,

      /** Oculta/Muestras el input que se usa para los filtros/busquedas (Default: true) */
      showInputFilter: true,

      /** Label del input */
      inputFilterLabel: 'Busca por el nombre',

      /** Label del numero de elementos (Default: X Elementos) */
      nItemsLabel: 'items',

      /** Label del numero de elementos seleccionados (Default: X Elementos) */
      nItemsSelectedLabel: 'items',

      /** Listado de posibles items a seleccionar, Ignorado si se especifica searchFn */
      items: [],

      /** Listado de valores ya seleccionados, se ignora si se usan los formularios de angular */
      selected: [],

      /** Define un array de posibles claves a usar para pintar el label de los posibles items a seleccionar (Default: ['name', 'label']) */
      itemLabelKey: ['testname', 'flipaaaaaname'],

      /** Define la clave a buscar que se usara como campo identificador para diferenciar los items seleccionados (Default: 'id') */
      itemValueKey: 'laid',

      /** Define el texto del boton de cerrar modal (Default: 'Cancelar') */
      cancelBtnText: 'Cerrar modal',

      /** Define el texto del boton de aceptar los cambios (Default: 'Seleccionar') */
      okBtnText: 'Aceptar cambios',

      /** Define el icono que se va a mostrar a la izquierda del texto de los items (Default: 'icon-file') */
      icon: 'icon-house',

      /** Función a la que se llama cada vez que el usuario escribe en el input, que debe devolver los resultados */
      searchFn: (x: GenericApiRequest) => {
        return of({
          content: [
            { laid: '1', testname: 'Perro 1' },
            { laid: '2', testname: 'Perro 2' },
            { laid: '3', flipaaaaaname: 'Perro 3' },
            { laid: '4', testname: 'Perro 4' },
          ],
          page: {
            page_number: 0,
            page_size: 20,
            total_elements: 20,
            total_pages: 1,
          },
        }).pipe(delay(500));
      },
    },
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      chipsSelect: [null],
    });
  }
}
