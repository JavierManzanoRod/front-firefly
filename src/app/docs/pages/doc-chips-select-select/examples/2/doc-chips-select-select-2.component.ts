import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../../modules/chips-control/components/chips.control.model';

@Component({
  selector: 'ff-doc-chips-select-select-2',
  templateUrl: './doc-chips-select-select-2.component.html',
})
export class DocChipsSelectSelect2Component {
  form: FormGroup;
  chipsSelectConfig: ChipsControlSelectConfig = {
    label: 'Seleccionar',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'Selecciona lo que quieras',
      multiple: true,
      /** Listado de posibles items a seleccionar */
      items: [
        { id: '1', name: 'Gatito 1' },
        { id: '2', name: 'Gatito 2' },
        { id: '3', name: 'Gatito 3' },
        { id: '4', name: 'Gatito 4' },
      ],
    },
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      chipsSelect: [null],
    });
  }
}
