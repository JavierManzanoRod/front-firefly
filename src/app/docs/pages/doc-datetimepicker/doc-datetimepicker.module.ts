import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FormErrorModule } from '../../../modules/form-error/form-error.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { ImportExampleModule } from '../../components/import-example/import-example.module';
import { DatetimepickerModule } from './../../../modules/datetimepicker/datetimepicker.module';
import { DocDatetimepickerComponent } from './doc-datetimepicker.component';
import { DocDatetimepicker1Component } from './examples/1/doc-datetimepicker-1.component';
import { DocDatetimepicker2Component } from './examples/2/doc-datetimepicker-2.component';

@NgModule({
  declarations: [DocDatetimepickerComponent, DocDatetimepicker1Component, DocDatetimepicker2Component],
  imports: [
    SharedModule,
    DocExampleModule,
    DatetimepickerModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocDatetimepickerComponent,
      },
    ]),
    ReactiveFormsModule,
    FormErrorModule,
    ImportExampleModule,
  ],
  exports: [],
  providers: [],
})
export class DocDatetimepickerModule {}
