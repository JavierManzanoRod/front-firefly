import { Component } from '@angular/core';
import * as example1HTML from './examples/1/doc-datetimepicker-1.component.html?raw';
import * as example1TS from './examples/1/doc-datetimepicker-1.component.ts?raw';
import * as example2HTML from './examples/2/doc-datetimepicker-2.component.html?raw';
import * as example2TS from './examples/2/doc-datetimepicker-2.component.ts?raw';

@Component({
  selector: 'ff-doc-datetimepicker',
  templateUrl: './doc-datetimepicker.component.html',
})
export class DocDatetimepickerComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example1Description = `Selector de fecha y tiempo, el tiempo (horas/minutos) no se puede
  especificar hasta que se seleccione una fecha y cada vez que se selecciona una fecha
  se resetea el tiempo especificado`;

  example2 = {
    ts: example2TS.default,
    html: example2HTML.default,
  };

  example2Description = `Un ejemplo donde se le especifica a la componentes las
  horas/minutos/segundos por defecto, es decir, los que se pondrán cada vez que se
  seleccione una fecha`;

  constructor() {}
}
