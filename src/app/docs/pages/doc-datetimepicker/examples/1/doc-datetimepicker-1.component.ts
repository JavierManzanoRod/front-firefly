import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { dateTimeValidator } from 'src/app/shared/validations/date-time.validator';
import { FormUtilsService } from '../../../../../shared/services/form-utils.service';

@Component({
  selector: 'ff-doc-datetimepicker-1',
  templateUrl: './doc-datetimepicker-1.component.html',
})
export class DocDatetimepicker1Component {
  form: FormGroup;
  formMessages = {
    date: [
      { type: 'required', message: 'Campo fecha necesario', params: {} },
      { type: 'invalidDate', message: 'Fecha esta mal', params: {} },
    ],
    name: [{ type: 'required', message: 'Campo nombre necesario', params: {} }],
  };

  get dateValue() {
    return this.form.controls.date.value;
  }

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      name: [null, [Validators.required]],
      // date: [null, [dateTimeValidator()]],
      date: [new Date('2020-11-15T14:00Z'), [dateTimeValidator()]],
    });

    this.form.controls.date.valueChanges.subscribe((newVal) => {
      // console.log('newVal of date', newVal);
    });
  }

  send() {
    FormUtilsService.markFormGroupTouched(this.form);
  }
}
