import { NgModule } from '@angular/core';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';

@NgModule({
  imports: [DatetimepickerModule],
})
export class AppModule {}
