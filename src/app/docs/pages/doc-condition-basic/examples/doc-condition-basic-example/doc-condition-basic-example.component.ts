import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ff-doc-condition-basic-example',
  templateUrl: './doc-condition-basic-example.component.html',
  styleUrls: ['./doc-condition-basic-example.component.scss'],
})
export class DocConditionBasicExampleComponent implements OnInit {
  data = {
    departments: ['Departamento de Prueba'],
  };

  valor = null;

  constructor() {}

  ngOnInit(): void {}
}
