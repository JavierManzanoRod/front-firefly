import { Component, OnInit } from '@angular/core';
import * as example1HTML from './examples/doc-condition-basic-example/doc-condition-basic-example.component.html?raw';
import * as example1TS from './examples/doc-condition-basic-example/doc-condition-basic-example.component.ts?raw';

@Component({
  selector: 'ff-doc-condition-basic',
  templateUrl: './doc-condition-basic.component.html',
  styleUrls: ['./doc-condition-basic.component.scss'],
})
export class DocConditionBasicComponent implements OnInit {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  constructor() {}

  ngOnInit(): void {}
}
