import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ConditionsBasicModule } from 'src/app/modules/conditions-basic/conditions-basic.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocConditionBasicComponent } from './doc-condition-basic.component';
import { DocConditionBasicExampleComponent } from './examples/doc-condition-basic-example/doc-condition-basic-example.component';

@NgModule({
  declarations: [DocConditionBasicComponent, DocConditionBasicExampleComponent],
  imports: [
    CommonModule,
    SharedModule,
    DocExampleModule,
    ConditionsBasicModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocConditionBasicComponent,
      },
    ]),
    ReactiveFormsModule,
    FormErrorModule,
  ],
})
export class DocConditionBasicModule {}
