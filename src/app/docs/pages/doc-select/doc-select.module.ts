import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormErrorModule } from '../../../modules/form-error/form-error.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocSelectComponent } from './doc-select.component';
import { DocSelect1Component } from './examples/1/doc-select-1.component';
import { DocSelect2Component } from './examples/2/doc-select-2.component';

@NgModule({
  declarations: [DocSelectComponent, DocSelect1Component, DocSelect2Component],
  imports: [
    SharedModule,
    DocExampleModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocSelectComponent,
      },
    ]),
    ReactiveFormsModule,
    FormErrorModule,
    NgSelectModule,
  ],
  exports: [],
  providers: [],
})
export class DocSelectModule {}
