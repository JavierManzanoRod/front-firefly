import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormUtilsService } from '../../../../../shared/services/form-utils.service';

@Component({
  selector: 'ff-doc-select-2',
  templateUrl: './doc-select-2.component.html',
})
export class DocSelect2Component {
  form: FormGroup;
  formMessages = {
    select: [{ type: 'required', message: 'Select necesario', params: {} }],
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      select: [null, [Validators.required]],
    });
  }

  send() {
    FormUtilsService.markFormGroupTouched(this.form);
  }
}
