import * as example1HTML from './examples/1/doc-select-1.component.html?raw';
import * as example1TS from './examples/1/doc-select-1.component.ts?raw';
import * as example2HTML from './examples/2/doc-select-2.component.html?raw';
import * as example2TS from './examples/2/doc-select-2.component.ts?raw';
import { Component } from '@angular/core';

@Component({
  selector: 'ff-doc-select',
  templateUrl: './doc-select.component.html',
})
export class DocSelectComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example1Description = `Lo mas básico, donde se almacena el valor seleccionado en una
  variable usando ngModel.`;

  example2 = {
    ts: example2TS.default,
    html: example2HTML.default,
  };

  example2Description = `El select es requerido, y
  si no se selecciona ningún valor muestra un error al intentar mandar el formulario.`;

  constructor() {}
}
