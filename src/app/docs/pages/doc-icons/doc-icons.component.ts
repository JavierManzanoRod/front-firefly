import * as example1HTML from './examples/1/doc-icons-1.component.html?raw';
import * as example1TS from './examples/1/doc-icons-1.component.ts?raw';
import { Component } from '@angular/core';
import { ToastService } from 'src/app/modules/toast/toast.service';
import iconsJson from '../../../../assets/fonts/icons/config.json';

@Component({
  selector: 'ff-doc-icons',
  templateUrl: './doc-icons.component.html',
  styleUrls: ['./doc-icons.component.scss'],
})
export class DocIconsComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example1Description = `Usamos iconos usando la herramienta <a href="https://fontello.com/">https://fontello.com/</a><br/>
  "¿Quieres añadir un nuevo icono? En la página <a href="https://confluence.almeci.io/pages/viewpage.action?pageId=143366386" target="_blank">Añadir iconos en Backoffice</a> podrás aprender como añadir nuevos iconos al aplicativo."
  `;

  iconsJSON = iconsJson;

  constructor(private toastService: ToastService) {}

  clipboard(text: string) {
    const copyText: HTMLInputElement = document.createElement('input');

    copyText.value = text;

    document.body.appendChild(copyText);

    copyText.select();
    copyText.setSelectionRange(0, 99999);

    document.execCommand('copy');

    document.body.removeChild(copyText);
    this.toastService.success('Clase del icono copiada al portapapeles (' + text + ')');
  }
}
