import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastModule } from 'src/app/modules/toast/toast.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocIconsComponent } from './doc-icons.component';
import { DocIcons1Component } from './examples/1/doc-icons-1.component';

@NgModule({
  declarations: [DocIconsComponent, DocIcons1Component],
  imports: [
    SharedModule,
    DocExampleModule,
    ToastModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocIconsComponent,
      },
    ]),
  ],
  exports: [],
  providers: [],
})
export class DocIconsModule {}
