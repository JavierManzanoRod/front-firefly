import { Component } from '@angular/core';
import * as example1HTML from './examples/1/doc-condition-readonly-1.component.html?raw';
import * as example1TS from './examples/1/doc-condition-readonly-1.component.ts?raw';
import * as example2HTML from './examples/2/doc-condition-readonly-2.component.html?raw';
import * as example2TS from './examples/2/doc-condition-readonly-2.component.ts?raw';

@Component({
  selector: 'ff-doc-condition-readonly',
  templateUrl: './doc-condition-readonly.component.html',
})
export class DocConditionReadonlyComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example2 = {
    ts: example2TS.default,
    html: example2HTML.default,
  };

  constructor() {}
}
