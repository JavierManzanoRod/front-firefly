import { Component } from '@angular/core';
import { Conditions2 } from '../../../../../modules/conditions2/models/conditions2.model';

@Component({
  selector: 'ff-doc-condition-readonly-1',
  templateUrl: './doc-condition-readonly-1.component.html',
})
export class DocConditionReadonly1Component {
  node: Conditions2 | null = null;

  constructor() {}
}
