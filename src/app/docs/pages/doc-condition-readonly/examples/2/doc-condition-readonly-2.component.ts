import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormUtilsService } from '../../../../../shared/services/form-utils.service';

@Component({
  selector: 'ff-doc-condition-readonly-2',
  templateUrl: './doc-condition-readonly-2.component.html',
})
export class DocConditionReadonly2Component {
  form: FormGroup;
  formMessages = {
    date: [{ type: 'required', message: 'Campo fecha necesario', params: {} }],
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      date: [null, [Validators.required]],
    });
  }

  send() {
    FormUtilsService.markFormGroupTouched(this.form);
  }
}
