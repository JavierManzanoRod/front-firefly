import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ConditionsReadonlyModule } from '../../../modules/conditions-readonly/conditions-readonly.module';
import { FormErrorModule } from '../../../modules/form-error/form-error.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocConditionReadonlyComponent } from './doc-condition-readonly.component';
import { DocConditionReadonly1Component } from './examples/1/doc-condition-readonly-1.component';
import { DocConditionReadonly2Component } from './examples/2/doc-condition-readonly-2.component';

@NgModule({
  declarations: [DocConditionReadonlyComponent, DocConditionReadonly1Component, DocConditionReadonly2Component],
  imports: [
    SharedModule,
    DocExampleModule,
    ConditionsReadonlyModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocConditionReadonlyComponent,
      },
    ]),
    ReactiveFormsModule,
    FormErrorModule,
  ],
  exports: [],
  providers: [],
})
export class DocConditionReadonlyModule {}
