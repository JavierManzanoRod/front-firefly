import * as example1HTML from './examples/1/doc-ui-tree-list-1.component.html?raw';
import * as example1TS from './examples/1/doc-ui-tree-list-1.component.ts?raw';
import * as example2HTML from './examples/2/doc-ui-tree-list-2.component.html?raw';
import * as example2TS from './examples/2/doc-ui-tree-list-2.component.ts?raw';
import { Component } from '@angular/core';

@Component({
  selector: 'ff-doc-ui-tree-list',
  templateUrl: './doc-ui-tree-list.component.html',
})
export class DocUiTreeListComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example2 = {
    ts: example2TS.default,
    html: example2HTML.default,
  };

  constructor() {}
}
