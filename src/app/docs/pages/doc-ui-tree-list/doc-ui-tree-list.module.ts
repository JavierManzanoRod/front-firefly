import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocUiTreeListComponent } from './doc-ui-tree-list.component';
import { DocUiTreeList1Component } from './examples/1/doc-ui-tree-list-1.component';
import { DocUiTreeList2Component } from './examples/2/doc-ui-tree-list-2.component';

@NgModule({
  declarations: [DocUiTreeListComponent, DocUiTreeList1Component, DocUiTreeList2Component],
  imports: [
    SharedModule,
    DocExampleModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocUiTreeListComponent,
      },
    ]),
  ],
  exports: [],
  providers: [],
})
export class DocUiTreeListModule {}
