import { Component } from '@angular/core';
import * as example1HTML from './examples/1/doc-chips-select-i18n-1.component.html?raw';
import * as example1TS from './examples/1/doc-chips-select-i18n-1.component.ts?raw';
import * as example2HTML from './examples/2/doc-chips-select-i18n-2.component.html?raw';
import * as example2TS from './examples/2/doc-chips-select-i18n-2.component.ts?raw';

@Component({
  selector: 'ff-doc-chips-select-i18n',
  templateUrl: './doc-chips-select-i18n.component.html',
  styleUrls: ['./doc-chips-select-i18n.component.scss'],
})
export class DocChipsSelectI18nComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example1Description = `Al pinchar en el botón se abre una modal desde la que se pueden añadir
   textos en los diferentes idiomas preestablecidos que despues se pintan en forma de chips y la
  componente te devuelve un objeto idioma:valor`;

  example2 = {
    ts: example2TS.default,
    html: example2HTML.default,
  };

  example2Description = `Un ejemplo con las configuraciones adicionales. La tabla de debajo describe que hace cada opción.`;

  constructor() {}
}
