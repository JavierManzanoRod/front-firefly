import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChipsControlModule } from '../../../modules/chips-control/chips-control.module';
import { FormErrorModule } from '../../../modules/form-error/form-error.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { ImportExampleModule } from '../../components/import-example/import-example.module';
import { DocChipsSelectI18nComponent } from './doc-chips-select-i18n.component';
import { DocChipsSelectI18n1Component } from './examples/1/doc-chips-select-i18n-1.component';
import { DocChipsSelectI18n2Component } from './examples/2/doc-chips-select-i18n-2.component';

@NgModule({
  declarations: [DocChipsSelectI18nComponent, DocChipsSelectI18n1Component, DocChipsSelectI18n2Component],
  imports: [
    SharedModule,
    DocExampleModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocChipsSelectI18nComponent,
      },
    ]),
    ReactiveFormsModule,
    FormErrorModule,
    ChipsControlModule,
    ImportExampleModule,
  ],
  exports: [],
  providers: [],
})
export class DocChipsSelectI18nModule {}
