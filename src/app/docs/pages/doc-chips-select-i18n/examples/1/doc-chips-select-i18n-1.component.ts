import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../../modules/chips-control/components/chips.control.model';

@Component({
  selector: 'ff-doc-chips-select-i18n-1',
  templateUrl: './doc-chips-select-i18n-1.component.html',
})
export class DocChipsSelectI18n1Component {
  form: FormGroup;
  chipsSelectConfig: ChipsControlSelectConfig = {
    label: 'Añadir textos',
    modalType: ModalChipsTypes.i18n,
    modalConfig: {
      title: 'Añade los textos en los idiomas',
    },
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      chipsSelect: [null],
    });
  }
}
