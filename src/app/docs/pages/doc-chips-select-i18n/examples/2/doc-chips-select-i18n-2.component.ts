import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../../modules/chips-control/components/chips.control.model';

@Component({
  selector: 'ff-doc-chips-select-i18n-2',
  templateUrl: './doc-chips-select-i18n-2.component.html',
})
export class DocChipsSelectI18n2Component {
  form: FormGroup;
  chipsSelectConfig: ChipsControlSelectConfig = {
    label: 'Añadir textos',
    modalType: ModalChipsTypes.i18n,
    modalConfig: {
      title: 'Añade los textos en los idiomas',

      /** Define el texto del boton de cerrar modal (Default: 'Cancelar') */
      cancelBtnText: 'Cerrar modal',

      /** Define el texto del boton de aceptar los cambios (Default: 'Seleccionar') */
      okBtnText: 'Aceptar cambios',
    },
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      chipsSelect: [null],
    });
  }
}
