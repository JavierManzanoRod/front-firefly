import { NgModule } from '@angular/core';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';

@NgModule({
  imports: [ChipsControlModule],
})
export class AppModule {}
