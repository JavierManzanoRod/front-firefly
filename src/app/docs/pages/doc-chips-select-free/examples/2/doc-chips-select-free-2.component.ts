import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../../modules/chips-control/components/chips.control.model';
import { numericValidator } from '../../../../../shared/validations/numeric.validator';

@Component({
  selector: 'ff-doc-chips-select-free-2',
  templateUrl: './doc-chips-select-free-2.component.html',
})
export class DocChipsSelectFree2Component {
  form: FormGroup;
  chipsSelectConfig: ChipsControlSelectConfig = {
    label: 'Añadir textos de 0',
    modalType: ModalChipsTypes.free,
    modalConfig: {
      title: 'Añade los textos que quieras',
      description: 'Breve descripción para decir al usuario que hacer',

      /** Muestra el boton para ordenar los textos */
      showOrder: true,

      /** Cambia el boton de ordenar al texto especificado */
      orderText: 'Ordename',

      /** Funcion custom para ordenar los textos añadidos por el usuario */
      orderFunction: (a, b) => {
        return parseInt(a, 10) - parseInt(b, 10);
      },

      /** Array de validaciones que tiene que pasar el texto escrito por el usuario para poder ser añadido */
      inputValidators: [numericValidator],

      /** Array de mensajes de error, necesario junto con inputValidators */
      validatorErrors: [{ type: 'numeric', message: 'COMMON_ERRORS.NUMBER' }],
    },
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      chipsSelect: [null],
    });
  }
}
