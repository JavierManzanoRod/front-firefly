import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../../modules/chips-control/components/chips.control.model';

@Component({
  selector: 'ff-doc-chips-select-free-1',
  templateUrl: './doc-chips-select-free-1.component.html',
})
export class DocChipsSelectFree1Component {
  form: FormGroup;
  chipsSelectConfig: ChipsControlSelectConfig = {
    label: 'Añadir textos de 0',
    modalType: ModalChipsTypes.free,
    modalConfig: {
      title: 'Añade los textos que quieras',
    },
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      chipsSelect: [null],
    });
  }
}
