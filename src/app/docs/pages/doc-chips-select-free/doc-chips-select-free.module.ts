import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChipsControlModule } from '../../../modules/chips-control/chips-control.module';
import { FormErrorModule } from '../../../modules/form-error/form-error.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { ImportExampleModule } from '../../components/import-example/import-example.module';
import { DocChipsSelectFreeComponent } from './doc-chips-select-free.component';
import { DocChipsSelectFree1Component } from './examples/1/doc-chips-select-free-1.component';
import { DocChipsSelectFree2Component } from './examples/2/doc-chips-select-free-2.component';

@NgModule({
  declarations: [DocChipsSelectFreeComponent, DocChipsSelectFree1Component, DocChipsSelectFree2Component],
  imports: [
    SharedModule,
    DocExampleModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocChipsSelectFreeComponent,
      },
    ]),
    ReactiveFormsModule,
    FormErrorModule,
    ChipsControlModule,
    ImportExampleModule,
  ],
  exports: [],
  providers: [],
})
export class DocChipsSelectFreeModule {}
