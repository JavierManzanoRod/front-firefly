import { Component } from '@angular/core';
import * as example1HTML from './examples/1/doc-chips-select-free-1.component.html?raw';
import * as example1TS from './examples/1/doc-chips-select-free-1.component.ts?raw';
import * as example2HTML from './examples/2/doc-chips-select-free-2.component.html?raw';
import * as example2TS from './examples/2/doc-chips-select-free-2.component.ts?raw';

@Component({
  selector: 'ff-doc-chips-select-free',
  templateUrl: './doc-chips-select-free.component.html',
  styleUrls: ['./doc-chips-select-free.component.scss'],
})
export class DocChipsSelectFreeComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example1Description = `Al pinchar en el botón se muestra una modal desde la que poder
  añadir textos libres que una vez fuera de la modal se mostraran como un listado de
  chips los cuales podrás borrar uno a uno o editar volviendo a abrir la modal.`;

  example2 = {
    ts: example2TS.default,
    html: example2HTML.default,
  };

  example2Description = `Un ejemplo con todas las opciones que tiene la modal. La tabla de debajo describe que hace cada opción.`;

  constructor() {}
}
