import { Component } from '@angular/core';

@Component({
  templateUrl: './browser-support.component.html',
  styleUrls: ['./browser-support.scss'],
})
export class DocBrowserSupportComponent {}
