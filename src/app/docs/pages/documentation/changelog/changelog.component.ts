import { Component } from '@angular/core';

@Component({
  templateUrl: './changelog.component.html',
  styleUrls: ['./changelog.component.scss'],
})
export class DocChangelogComponent {
  code = '<feat|bugfix|config>(<jira-ticket>): <bloque-funcional>: <subject>';
}
