import { Component, OnInit } from '@angular/core';
import { Functionality } from 'src/app/components/menu-left/model/menu-left-model';

@Component({
  selector: 'ff-config-manager',
  templateUrl: './config-manager.component.html',
  styleUrls: ['./config-manager.component.scss'],
})
export class ConfigManagerComponent implements OnInit {
  valores = Object.keys(Functionality).map((e) => Functionality[e as unknown as Functionality]);

  constructor() {}

  ngOnInit(): void {}
}
