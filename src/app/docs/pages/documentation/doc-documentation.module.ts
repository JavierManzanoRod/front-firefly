import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HighlightModule } from 'ngx-highlightjs';
import { ImportExampleModule } from 'src/app/docs/components/import-example/import-example.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocBrowserSupportComponent } from './browser-support/browser-support.component';
import { DocChangelogComponent } from './changelog/changelog.component';
import { ConfigManagerComponent } from './config-manager/config-manager.component';
import { DocGettingStartedComponent } from './getting-started/doc-getting-started.component';
import { DocQualityToolsComponent } from './quality-tools/doc-quality-tools.component';

@NgModule({
  imports: [
    SharedModule,
    DocExampleModule,
    ImportExampleModule,
    HighlightModule,
    RouterModule.forChild([
      {
        path: 'getting-started',
        component: DocGettingStartedComponent,
      },
      {
        path: 'browser-support',
        component: DocBrowserSupportComponent,
      },
      {
        path: 'quality-tools',
        component: DocQualityToolsComponent,
      },
      {
        path: 'config-manager',
        component: ConfigManagerComponent,
      },
      {
        path: 'changelog',
        component: DocChangelogComponent,
      },
    ]),
  ],
  declarations: [
    DocGettingStartedComponent,
    DocBrowserSupportComponent,
    DocQualityToolsComponent,
    DocChangelogComponent,
    ConfigManagerComponent,
  ],
  exports: [],
  providers: [],
})
export class DocDocumentationModule {}
