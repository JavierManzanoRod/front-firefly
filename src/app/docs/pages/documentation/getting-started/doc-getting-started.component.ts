import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: './doc-getting-started.component.html',
  styleUrls: ['./doc-getting-started.scss'],
})
export class DocGettingStartedComponent {
  currentUrl: string;
  code = '<feat|bugfix|config>(<jira-ticket>): <bloque-funcional>: <subject>';

  constructor(private router: Router) {
    this.currentUrl = this.router.url;
  }

  // eslint-disable-next-line @typescript-eslint/member-ordering
  otherCommands = `npm test  --> ejectua tests unitarios
npm lint  --> ejecuta linter
npm run build:stats --> crea auditoría de rendimiento (tamaño de budgets)
npm run analyze --> muestra informe de auditoría de rendimiento (tamaño de budgets)
`;

  // eslint-disable-next-line @typescript-eslint/member-ordering
  commitCommands = `git config --list --> comprueba que tu usuario sea el usuario X asignado y que el email corresponfa al asignado en el repositorio

En caso de no ser correctos deberas ir al archivo .git/config.git y setear tu usuario y email como en el siguiente ejemplo:
[user]
name = X11111AA
email = usuario@colaborador.elcorteingles.es
deberias colocarlo solo en el config del proyecto, pero para asegurarte lo puedes introducir en el config general en equipo/usuarios/git`;

  getLink(hash: string) {
    return this.currentUrl + '#' + hash;
  }
}
