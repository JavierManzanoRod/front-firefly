import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ChipsControlModule } from '../../../modules/chips-control/chips-control.module';
import { FormErrorModule } from '../../../modules/form-error/form-error.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { ImportExampleModule } from '../../components/import-example/import-example.module';
import { DocChipsSelectKeyValueComponent } from './doc-chips-select-key-value.component';
import { DocChipsSelectKeyValue1Component } from './examples/1/doc-chips-select-key-value-1.component';
import { DocChipsSelectKeyValue2Component } from './examples/2/doc-chips-select-key-value-2.component';

@NgModule({
  declarations: [DocChipsSelectKeyValueComponent, DocChipsSelectKeyValue1Component, DocChipsSelectKeyValue2Component],
  imports: [
    SharedModule,
    DocExampleModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocChipsSelectKeyValueComponent,
      },
    ]),
    ReactiveFormsModule,
    FormErrorModule,
    ChipsControlModule,
    ImportExampleModule,
  ],
  exports: [],
  providers: [],
})
export class DocChipsSelectKeyValueModule {}
