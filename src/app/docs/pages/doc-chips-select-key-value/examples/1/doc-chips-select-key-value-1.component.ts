import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../../modules/chips-control/components/chips.control.model';

@Component({
  selector: 'ff-doc-chips-select-key-value-1',
  templateUrl: './doc-chips-select-key-value-1.component.html',
})
export class DocChipsSelectKeyValue1Component {
  form: FormGroup;
  chipsSelectConfig: ChipsControlSelectConfig = {
    label: 'Añadir valores',
    modalType: ModalChipsTypes.keyvalue,
    modalConfig: {
      title: 'Añade los valores',
    },
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      chipsSelect: [null],
    });
  }
}
