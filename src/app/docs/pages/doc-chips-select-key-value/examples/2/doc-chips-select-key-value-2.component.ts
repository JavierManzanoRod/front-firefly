import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../../modules/chips-control/components/chips.control.model';

@Component({
  selector: 'ff-doc-chips-select-key-value-2',
  templateUrl: './doc-chips-select-key-value-2.component.html',
})
export class DocChipsSelectKeyValue2Component {
  form: FormGroup;
  chipsSelectConfig: ChipsControlSelectConfig = {
    label: 'Añadir textos',
    modalType: ModalChipsTypes.keyvalue,
    modalConfig: {
      title: 'Añade los textos en los idiomas',

      /** Define el texto del boton de cerrar modal (Default: 'Cancelar') */
      cancelBtnText: 'Cerrar modal',

      /** Define el texto del boton de aceptar los cambios (Default: 'Seleccionar') */
      okBtnText: 'Aceptar cambios',

      /** Define el label de la columna clave (Default: 'Clave') */
      keyLabel: 'ID',

      /** Define el label de la columna value (Default: 'Valor') */
      valueLabel: 'Name',
    },
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      chipsSelect: [null],
    });
  }
}
