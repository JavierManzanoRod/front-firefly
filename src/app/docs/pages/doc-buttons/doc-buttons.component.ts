import * as example1HTML from './examples/1/doc-buttons-1.component.html?raw';
import * as example1TS from './examples/1/doc-buttons-1.component.ts?raw';
import * as example2HTML from './examples/2/doc-buttons-2.component.html?raw';
import * as example2TS from './examples/2/doc-buttons-2.component.ts?raw';
import { Component } from '@angular/core';

@Component({
  selector: 'ff-doc-buttons',
  templateUrl: './doc-buttons.component.html',
})
export class DocButtonsComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example1Description = `
  Está basado en bootstrap, se han customizado de la siguiente manera
  `;

  example2 = {
    ts: example2TS.default,
    html: example2HTML.default,
  };

  example2Description = `También tenemos el diseño del botón de tipo outline de la siguiente manera`;

  constructor() {}
}
