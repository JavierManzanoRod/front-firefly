import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocButtonsComponent } from './doc-buttons.component';
import { DocButtons1Component } from './examples/1/doc-buttons-1.component';
import { DocButtons2Component } from './examples/2/doc-buttons-2.component';

@NgModule({
  declarations: [DocButtonsComponent, DocButtons1Component, DocButtons2Component],
  imports: [
    SharedModule,
    DocExampleModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocButtonsComponent,
      },
    ]),
  ],
  exports: [],
  providers: [],
})
export class DocButtonsModule {}
