import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FieldI18nModule } from '../../../modules/field-i18n/field-i18n.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { ImportExampleModule } from '../../components/import-example/import-example.module';
import { DocFieldI18nComponent } from './doc-field-i18n.component';
import { DocFieldI18n1Component } from './examples/1/doc-field-i18n-1.component';

@NgModule({
  declarations: [DocFieldI18nComponent, DocFieldI18n1Component],
  imports: [
    SharedModule,
    DocExampleModule,
    FieldI18nModule,
    ImportExampleModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocFieldI18nComponent,
      },
    ]),
    ReactiveFormsModule,
  ],
  exports: [],
  providers: [],
})
export class DocFieldI18nModule {}
