import { Component } from '@angular/core';
import * as example1HTML from './examples/1/doc-field-i18n-1.component.html?raw';
import * as example1TS from './examples/1/doc-field-i18n-1.component.ts?raw';

@Component({
  selector: 'ff-doc-field-i18n',
  templateUrl: './doc-field-i18n.component.html',
})
export class DocFieldI18nComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example1Description = `Añade textos en los diferentes idiomas preestablecidos y la
  componente te devuelve un objeto idioma:valor usando formularios de angular`;

  constructor() {}
}
