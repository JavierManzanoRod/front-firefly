import { NgModule } from '@angular/core';
import { FieldI18nModule } from 'src/app/modules/field-i18n/field-i18n.module';

@NgModule({
  imports: [FieldI18nModule],
})
export class AppModule {}
