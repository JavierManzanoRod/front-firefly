import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'ff-doc-field-i18n-1',
  templateUrl: './doc-field-i18n-1.component.html',
})
export class DocFieldI18n1Component {
  formTranslations: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formTranslations = this.fb.group({
      translations: [null],
    });
  }
}
