import { Component, OnInit } from '@angular/core';
import { UITree } from '../../../../../modules/ui-tree/models/ui-tree.model';

@Component({
  selector: 'ff-doc-ui-tree-example',
  templateUrl: './doc-ui-tree-example.component.html',
  styleUrls: ['./doc-ui-tree-example.component.scss'],
})
export class DocUiTreeExampleComponent implements OnInit {
  tree: UITree = {
    childrens: [
      {
        value: {
          name: 'Hijo',
        },
        expanded: true,
      },
    ],
    value: {
      name: 'Padre',
    },
  };

  constructor() {}

  ngOnInit(): void {}
}
