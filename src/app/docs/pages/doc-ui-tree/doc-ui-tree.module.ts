import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UiTreeModule } from 'src/app/modules/ui-tree/ui-tree.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocUiTreeComponent } from './doc-ui-tree.component';
import { DocUiTreeExampleComponent } from './examples/doc-ui-tree-example/doc-ui-tree-example.component';

@NgModule({
  declarations: [DocUiTreeExampleComponent, DocUiTreeComponent],
  imports: [
    CommonModule,
    DocExampleModule,
    SharedModule,
    UiTreeModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocUiTreeComponent,
      },
    ]),
  ],
})
export class DocUiTreeModule {}
