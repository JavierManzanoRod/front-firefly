import * as example1HTML from './examples/doc-ui-tree-example/doc-ui-tree-example.component.html?raw';
import * as example1TS from './examples/doc-ui-tree-example/doc-ui-tree-example.component.ts?raw';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ff-doc-ui-tree',
  templateUrl: './doc-ui-tree.component.html',
  styleUrls: ['./doc-ui-tree.component.scss'],
})
export class DocUiTreeComponent implements OnInit {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  constructor() {}

  ngOnInit(): void {}
}
