import * as example1HTML from './examples/doc-loading-example/doc-loading-example.component.html?raw';
import * as example1TS from './examples/doc-loading-example/doc-loading-example.component.ts?raw';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ff-doc-loading',
  templateUrl: './doc-loading.component.html',
  styleUrls: ['./doc-loading.component.scss'],
})
export class DocLoadingComponent implements OnInit {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  exampleDescription = `Llamar al modulo LoadingModule para el uso del loading.`;

  constructor() {}

  ngOnInit(): void {}
}
