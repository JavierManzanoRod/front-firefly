import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoadingModule } from 'src/app/modules/loading/loading.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DocExampleModule } from './../../components/doc-example/doc-example.module';
import { DocLoadingComponent } from './doc-loading.component';
import { DocLoadingExampleComponent } from './examples/doc-loading-example/doc-loading-example.component';

@NgModule({
  declarations: [DocLoadingComponent, DocLoadingExampleComponent],
  imports: [
    CommonModule,
    SharedModule,
    DocExampleModule,
    LoadingModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocLoadingComponent,
      },
    ]),
  ],
})
export class DocLoadingModule {}
