import { Component } from '@angular/core';

interface Color {
  class: string;
  description?: string;
  code: string;
}

@Component({
  selector: 'ff-doc-colors-1',
  templateUrl: './doc-colors-1.component.html',
  styleUrls: ['./doc-colors-1.component.scss'],
})
export class DocColors1Component {
  colors: Color[] = [
    {
      class: 'orange-light',
      code: '$orange-light',
    },
    {
      class: 'red',
      code: '$red',
      description: 'Color para los mensajes de error',
    },
    {
      class: 'red-delete',
      code: '$red-delete',
    },
    {
      class: 'dark-green',
      code: '$dark-green',
      description: 'Color principal',
    },
    {
      class: 'green',
      code: '$green',
    },
    {
      class: 'green-white',
      code: '$green-white',
    },
    {
      class: 'green-white-2',
      code: '$green-white-2',
    },
    {
      class: 'white',
      code: '$white',
    },
    {
      class: 'white-2',
      code: '$white-2',
    },
    {
      class: 'white-3',
      code: '$white-3',
    },
    {
      class: 'white-4',
      code: '$white-4',
    },
    {
      class: 'white-6',
      code: '$white-6',
    },
    {
      class: 'grey',
      code: '$grey',
    },
    {
      class: 'grey-2',
      code: '$grey-2',
    },
    {
      class: 'grey-3',
      code: '$grey-3',
    },
    {
      class: 'grey-4',
      code: '$grey-4',
    },
    {
      class: '$grey-5',
      code: '$grey-5',
    },
    {
      class: 'black',
      code: '$black',
      description: 'Color secundario',
    },
    {
      class: 'black-2',
      code: '$black-2',
    },
    {
      class: 'black-3',
      code: '$black-3',
    },
    {
      class: 'black-4',
      code: '$black-4',
    },
  ];

  constructor() {}
}
