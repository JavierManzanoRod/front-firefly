import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastModule } from 'src/app/modules/toast/toast.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocColorsComponent } from './doc-colors.component';
import { DocColors1Component } from './examples/1/doc-colors-1.component';

@NgModule({
  declarations: [DocColorsComponent, DocColors1Component],
  imports: [
    SharedModule,
    DocExampleModule,
    ToastModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocColorsComponent,
      },
    ]),
  ],
  exports: [],
  providers: [],
})
export class DocColorsModule {}
