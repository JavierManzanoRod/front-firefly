import { Component } from '@angular/core';
import * as example1Scss from './examples/1/doc-colors-1-example.component.scss?raw';
import * as example1HTML from './examples/1/doc-colors-1.component.html?raw';
import * as example1TS from './examples/1/doc-colors-1.component.ts?raw';

@Component({
  selector: 'ff-doc-colors',
  templateUrl: './doc-colors.component.html',
  styleUrls: ['./doc-colors.component.scss'],
})
export class DocColorsComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
    scss: example1Scss.default,
  };

  example1Description = `Ejemplo de los colores principales de la web`;

  constructor() {}
}
