import * as example1HTML from './examples/1/doc-toast-1.component.html?raw';
import * as example1TS from './examples/1/doc-toast-1.component.ts?raw';
import { Component } from '@angular/core';

@Component({
  selector: 'ff-doc-toast',
  templateUrl: './doc-toast.component.html',
})
export class DocToastComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example1Description = `Usa el servicio de toast para mostrar alertas flotantes para informar al
  usuario de algún cambio/error`;

  constructor() {}
}
