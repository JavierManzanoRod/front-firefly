import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FormErrorModule } from '../../../modules/form-error/form-error.module';
import { ToastModule } from '../../../modules/toast/toast.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocToastComponent } from './doc-toast.component';
import { DocToast1Component } from './examples/1/doc-toast-1.component';

@NgModule({
  declarations: [DocToastComponent, DocToast1Component],
  imports: [
    SharedModule,
    DocExampleModule,
    ToastModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocToastComponent,
      },
    ]),
    ReactiveFormsModule,
    FormErrorModule,
  ],
  exports: [],
  providers: [],
})
export class DocToastModule {}
