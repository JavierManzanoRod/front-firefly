import { NgModule } from '@angular/core';
import { ToastModule } from 'src/app/modules/toast/toast.module';

@NgModule({
  imports: [ToastModule],
})
export class AppModule {}
