import { Component } from '@angular/core';
import { ToastService } from 'src/app/modules/toast/toast.service';

@Component({
  selector: 'ff-doc-toast-1',
  templateUrl: './doc-toast-1.component.html',
})
export class DocToast1Component {
  constructor(private toastService: ToastService) {}

  error() {
    this.toastService.error('Mensaje de del toast', 'Esto es el título');
  }

  warning() {
    this.toastService.warning('Mensaje de del toast', 'Esto es el título');
  }

  success() {
    this.toastService.success('Mensaje de del toast', 'Esto es el título');
  }

  info() {
    this.toastService.info('Mensaje de del toast', 'Esto es el título');
  }
}
