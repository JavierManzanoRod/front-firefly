import * as example1HTML from './examples/1/doc-scroll-1.component.html?raw';
import * as example1TS from './examples/1/doc-scroll-1.component.ts?raw';
import { Component } from '@angular/core';

@Component({
  selector: 'ff-doc-scroll',
  templateUrl: './doc-scroll.component.html',
  styleUrls: ['./doc-scroll.component.scss'],
})
export class DocScrollComponent {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  example1Description = `Si queremos poner scroll en alguna parte, la capa encarga del scroll debera de ser la de ngx-simplebar`;

  constructor() {}
}
