import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SimplebarAngularModule } from 'simplebar-angular';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocScrollComponent } from './doc-scroll.component';
import { DocScroll1Component } from './examples/1/doc-scroll-1.component';

@NgModule({
  declarations: [DocScrollComponent, DocScroll1Component],
  imports: [
    SharedModule,
    DocExampleModule,
    SimplebarAngularModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocScrollComponent,
      },
    ]),
  ],
  exports: [],
  providers: [],
})
export class DocScrollModule {}
