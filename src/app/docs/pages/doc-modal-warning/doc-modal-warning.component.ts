import { Component, OnInit } from '@angular/core';
import * as example1HTML from './examples/doc-modal-warning-example/doc-modal-warning-example.component.html?raw';
import * as example1TS from './examples/doc-modal-warning-example/doc-modal-warning-example.component.ts?raw';

@Component({
  selector: 'ff-doc-modal-warning',
  templateUrl: './doc-modal-warning.component.html',
  styleUrls: ['./doc-modal-warning.component.scss'],
})
export class DocModalWarningComponent implements OnInit {
  example1 = {
    ts: example1TS.default,
    html: example1HTML.default,
  };

  constructor() {}

  ngOnInit(): void {}
}
