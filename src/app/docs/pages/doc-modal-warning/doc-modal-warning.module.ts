import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModalModule } from '../../../modules/common-modal/common-modal.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocModalWarningComponent } from './doc-modal-warning.component';
import { DocModalWarningExampleComponent } from './examples/doc-modal-warning-example/doc-modal-warning-example.component';

@NgModule({
  declarations: [DocModalWarningComponent, DocModalWarningExampleComponent],
  imports: [
    CommonModule,
    SharedModule,
    DocExampleModule,
    CommonModalModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocModalWarningComponent,
      },
    ]),
  ],
  providers: [BsModalRef],
})
export class DocModalWarningModule {}
