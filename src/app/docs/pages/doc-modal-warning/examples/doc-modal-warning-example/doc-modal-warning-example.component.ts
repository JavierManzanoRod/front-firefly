import { Component, OnInit } from '@angular/core';
import { UICommonModalConfig } from '../../../../../modules/common-modal/components/modal-warning/modal-warning.model';

@Component({
  selector: 'ff-doc-modal-warning-example',
  templateUrl: './doc-modal-warning-example.component.html',
  styleUrls: ['./doc-modal-warning-example.component.scss'],
})
export class DocModalWarningExampleComponent implements OnInit {
  config: UICommonModalConfig = {
    title: 'Test',
    errorMessage: 'Advertencia esto es un ejemplo',
    okButtonText: 'Confirmar',
    cancelButtonText: 'Cancelar',
    bodyMessage: 'Cuerpo del modal',
  };

  evento = null;
  primerEvento = false;

  constructor() {}

  ngOnInit(): void {}
}
