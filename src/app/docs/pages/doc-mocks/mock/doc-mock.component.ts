import { Component } from '@angular/core';

@Component({
  selector: 'ff-doc-mock',
  templateUrl: './doc-mock.component.html',
  styleUrls: ['./doc-mock.component.scss'],
})
export class DocMockComponent {
  fastController = `import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../utils/router';
export default createRouterCrud('/ruta-del-endpoint', new CrudApiDefaultHandler('nombre-de-la-carpeta-dentro-de-reposiotry', 'nombre-del-fichero-json'));
  `;
  controller = `import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../utils/router';
export default createRouterCrud('/products/backoffice-sites-subsites/v1/sites', new CrudApiDefaultHandler('sites'));
  `;
  routes = `const routes = [
    excludeSearchRoutes,
    limitSalesRoutes,
    entityTypeRoutes,
    attributeRoutes,
    adminGroupTypeRoutes,
    adminGroupRoutes,
    categoryRulesRoutes,
    entitiesRoutes,
    contentGroupRoutes,
    orderRoutes,
    sitesRoutes,
    subSitesRoutes,
    sellerRoutes,
    productTypesRoutes,
  ];`;

  extendedController = `import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';
  
export class ExcludeSearchController extends CrudApiDefaultHandler<any> {
    // Metodo creado para gestionar los attributos
    attributes(req: Request, res: Response) {
      //Podemos leer otro fichero json a parte del por defecto del controlador y pasarle una funcion para formatearlo
      const data = this.getFileReader('attributes')(null);
      return res.status(200).send(data);
  }
}
const controller = new ExcludeSearchController('stock/exclude-search', 'exclude-search');
const router = createRouterCrud('/products/backoffice-item-flags/v1/exclude-searches', controller);
router.get('/products/backoffice-item-flags/v1/exclude-searches/types/attributes', controller.attributes.bind(controller));
export default router;`;
  extendedController2 = `import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../../utils/CrudApiDefaultHandler';
import { wrapBussinessApiResponse } from '../../../../utils/readFile';
import { createRouterCrud } from '../../../../utils/router';
  
class MarketplaceController extends CrudApiDefaultHandler<any> {
    // Sobreescribir el metodo por defecto para devoler otro tipo de respuesta paginadad
    list(req: Request, res: Response) {
      return res.status(200).send(wrapBussinessApiResponse(this.data));
    }
    // Nuevo metodo para responder a un caso especial
    empty(req: Request, res: Response) {
      return res.status(200).send([]);
    }
}
const controller = new MarketplaceController('catalog/facets/marketplace', 'marketplace');
const router = createRouterCrud('/products/marketplace-product-types/v1/1.0.0', controller);
router.patch('/products/marketplace-product-types/v1/1.0.0/:id', controller.empty.bind(this));
export default router;
`;
  otroCaso = `process.env.USECASE = "otro-caso"`;
  constructor() {}
}
