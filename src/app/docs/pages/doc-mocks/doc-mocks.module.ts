import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HighlightModule } from 'ngx-highlightjs';
import { ImportExampleModule } from 'src/app/docs/components/import-example/import-example.module';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleModule } from '../../components/doc-example/doc-example.module';
import { DocMockComponent } from './mock/doc-mock.component';

@NgModule({
  imports: [
    SharedModule,
    ImportExampleModule,
    DocExampleModule,
    HighlightModule,
    RouterModule.forChild([
      {
        path: '',
        component: DocMockComponent,
      },
    ]),
  ],
  declarations: [DocMockComponent],
  exports: [],
  providers: [],
})
export class DocMocksModule {}
