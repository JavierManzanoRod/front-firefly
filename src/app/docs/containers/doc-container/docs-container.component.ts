import { Component } from '@angular/core';
import { MenuLeft } from 'src/app/components/menu-left/model/menu-left-model';

@Component({
  templateUrl: './docs-container.component.html',
  styleUrls: ['./docs-container.component.scss'],
})
export class DocsContainerComponent {
  isMenuOpen = true;

  menu: MenuLeft[] = [
    {
      label: 'Documentación',
      link: '',
      subitems: [
        {
          label: 'Getting started',
          link: '/docs/documentation/getting-started',
        },
        {
          label: 'Navegadores compatibles',
          link: '/docs/documentation/browser-support',
        },
        {
          label: 'Herramientas de calidad',
          link: '/docs/documentation/quality-tools',
        },
        {
          label: 'Config Manager',
          link: '/docs/documentation/config-manager',
        },
        {
          label: 'Changelog',
          link: '/docs/documentation/changelog',
        },
      ],
    },
    {
      label: 'UI',
      link: '',
      subitems: [
        {
          label: 'Iconos',
          link: '/docs/icons',
        },
        {
          label: 'Botones',
          link: '/docs/buttons',
        },
        {
          label: 'Colores',
          link: '/docs/colors',
        },
      ],
    },
    {
      label: 'Componentes',
      link: '',
      subitems: [
        {
          label: 'Select',
          link: '/docs/select',
        },
        {
          label: 'FieldI18N',
          link: '/docs/field-i18n',
        },
        {
          label: 'Datetimepicker',
          link: '/docs/datetimepicker',
        },
        {
          label: 'Toast',
          link: '/docs/toast',
        },
        {
          label: 'Modales',
          link: '/docs/chips-select-free',
          subitems: [
            {
              label: 'Free',
              link: '/docs/chips-select-free',
            },
            {
              label: 'Select',
              link: '/docs/chips-select-select',
            },
            {
              label: 'I18N',
              link: '/docs/chips-select-i18n',
            },
            {
              label: 'Key Value',
              link: '/docs/chips-select-key-value',
            },
          ],
        },
        {
          label: 'Conditions',
          link: '/docs/condition-readonly',
          subitems: [
            {
              label: 'Readonly',
              link: '/docs/condition-readonly',
            },
            {
              label: 'Basic',
              link: '/docs/condition-basic',
            },
          ],
        },
        {
          label: 'Form Error',
          link: '/docs/form-error',
        },
        {
          label: 'Ui Tree',
          link: '/docs/ui-tree',
        },
        {
          label: 'Ui Tree List',
          link: '/docs/ui-tree-list',
        },
        {
          label: 'Loading',
          link: '/docs/loading',
        },
        {
          label: 'Modal Warning',
          link: '/docs/modal-warning',
        },
      ],
    },
    {
      label: 'Mocks',
      link: '',
      subitems: [
        {
          label: 'Mocks',
          link: '/docs/mocks',
        },
      ],
    },
  ];

  constructor() {}
}
