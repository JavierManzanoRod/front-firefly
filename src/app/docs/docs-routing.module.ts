import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocsContainerComponent } from './containers/doc-container/docs-container.component';

const routes: Routes = [
  {
    path: '',
    // redirectTo: 'docs/getting-started',
    component: DocsContainerComponent,
    children: [
      {
        path: 'documentation',
        loadChildren: () => import('./pages/documentation/doc-documentation.module').then((m) => m.DocDocumentationModule),
      },
      {
        path: 'browser-support',
        loadChildren: () => import('./pages/documentation/doc-documentation.module').then((m) => m.DocDocumentationModule),
      },
      {
        path: 'config-manager',
        loadChildren: () => import('./pages/documentation/doc-documentation.module').then((m) => m.DocDocumentationModule),
      },
      {
        path: 'buttons',
        loadChildren: () => import('./pages/doc-buttons/doc-buttons.module').then((m) => m.DocButtonsModule),
      },
      {
        path: 'field-i18n',
        loadChildren: () => import('./pages/doc-field-i18n/doc-field-i18n.module').then((m) => m.DocFieldI18nModule),
      },
      {
        path: 'datetimepicker',
        loadChildren: () => import('./pages/doc-datetimepicker/doc-datetimepicker.module').then((m) => m.DocDatetimepickerModule),
      },
      {
        path: 'toast',
        loadChildren: () => import('./pages/doc-toast/doc-toast.module').then((m) => m.DocToastModule),
      },
      {
        path: 'select',
        loadChildren: () => import('./pages/doc-select/doc-select.module').then((m) => m.DocSelectModule),
      },
      {
        path: 'chips-select-free',
        loadChildren: () => import('./pages/doc-chips-select-free/doc-chips-select-free.module').then((m) => m.DocChipsSelectFreeModule),
      },
      {
        path: 'chips-select-select',
        loadChildren: () =>
          import('./pages/doc-chips-select-select/doc-chips-select-select.module').then((m) => m.DocChipsSelectSelectModule),
      },
      {
        path: 'chips-select-i18n',
        loadChildren: () => import('./pages/doc-chips-select-i18n/doc-chips-select-i18n.module').then((m) => m.DocChipsSelectI18nModule),
      },
      {
        path: 'chips-select-key-value',
        loadChildren: () =>
          import('./pages/doc-chips-select-key-value/doc-chips-select-key-value.module').then((m) => m.DocChipsSelectKeyValueModule),
      },
      {
        path: 'form-error',
        loadChildren: () => import('./pages/doc-form-error/doc-form-error.module').then((m) => m.DocFormErrorModule),
      },
      {
        path: 'ui-tree',
        loadChildren: () => import('./pages/doc-ui-tree/doc-ui-tree.module').then((m) => m.DocUiTreeModule),
      },
      {
        path: 'loading',
        loadChildren: () => import('./pages/doc-loading/doc-loading.module').then((m) => m.DocLoadingModule),
      },
      {
        path: 'modal-warning',
        loadChildren: () => import('./pages/doc-modal-warning/doc-modal-warning.module').then((m) => m.DocModalWarningModule),
      },
      {
        path: 'condition-basic',
        loadChildren: () => import('./pages/doc-condition-basic/doc-condition-basic.module').then((m) => m.DocConditionBasicModule),
      },
      {
        path: 'condition-readonly',
        loadChildren: () =>
          import('./pages/doc-condition-readonly/doc-condition-readonly.module').then((m) => m.DocConditionReadonlyModule),
      },
      {
        path: 'ui-tree-list',
        loadChildren: () => import('./pages/doc-ui-tree-list/doc-ui-tree-list.module').then((m) => m.DocUiTreeListModule),
      },
      {
        path: 'icons',
        loadChildren: () => import('./pages/doc-icons/doc-icons.module').then((m) => m.DocIconsModule),
      },
      {
        path: 'scroll',
        loadChildren: () => import('./pages/doc-scroll/doc-scroll.module').then((m) => m.DocScrollModule),
      },

      {
        path: 'colors',
        loadChildren: () => import('./pages/doc-colors/doc-colors.module').then((m) => m.DocColorsModule),
      },
      {
        path: 'mocks',
        loadChildren: () => import('./pages/doc-mocks/doc-mocks.module').then((m) => m.DocMocksModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocsRoutingModule {}
