declare module '*?raw' {
  const result: { default: string };

  export = result;
}
