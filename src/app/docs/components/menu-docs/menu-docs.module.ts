import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { MenuDocsComponent } from './menu-docs.component';

@NgModule({
  declarations: [MenuDocsComponent],
  imports: [SharedModule],
  providers: [],
  exports: [MenuDocsComponent],
})
export class MenuDocsModule {}
