import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, takeWhile } from 'rxjs/operators';
import { MenuDocs } from './model/menu-docs-model';

@Component({
  selector: 'ff-menu-docs',
  templateUrl: './menu-docs.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./menu-docs.component.scss'],
})
export class MenuDocsComponent implements OnInit {
  @Output() isMenuOpenChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() isMenuOpen = true;
  @Input() items!: MenuDocs[];

  urlActive$ = new Subject<string>();

  menuActive: MenuDocs | null = null;

  constructor(public router: Router) {}

  ngOnInit() {
    this.router.events
      .pipe(
        filter((e) => e instanceof NavigationEnd),
        takeWhile((e) => !this.menuActive)
      )
      .subscribe(() => {
        const urlActive = this.parseUrl(this.router.url);
        this.urlActive$.next(urlActive);
        /*
        this.menuActive = this.getMenuActive(this.items, urlActive);
        if (this.menuActive) {
          this.toogleMenuItem(this.menuActive);
        }
        */
      });
  }

  getMenuActive(items: MenuDocs[], url: string): MenuDocs | undefined {
    return items.find((value: MenuDocs) => {
      return (value.link.trim() && url.includes(value.link)) || value.subitems?.find((v: MenuDocs) => v.link && url.includes(v.link));
    });
  }

  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
    this.isMenuOpenChange.emit(this.isMenuOpen);
    if (!this.isMenuOpen) {
      this.items.forEach((item) => (item.expanded = false));
    }
  }

  toogleMenuItem(item: MenuDocs, event?: any) {
    console.log(item);
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }
    if (item.subitems?.length) {
      if (!item.expanded) {
        if (!event) {
          this.items.forEach((menuItem: MenuDocs) => {
            menuItem.expanded = false;
          });
        }
        item.expanded = true;
      } else {
        item.expanded = false;
      }
    } else {
      this.go(item, true);
    }
  }

  go(item: MenuDocs, hide: boolean, event?: any) {
    /*
    if (!this.isMenuOpen) {
      this.items.forEach((v) => {
        v.expanded = false;
      });
    }

     */

    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    this.router.navigate([item.link]);
    this.urlActive$.next(item.link);
  }

  parseUrl(url: string): string {
    const temp = url.split('/');
    return temp.length >= 3 ? `/${temp[1]}/${temp[2]}` : `/${temp[1]}`;
  }

  isSubItemActive(active: string | null, itemLink: string): boolean {
    if (!active) {
      return false;
    }

    return this.parseUrl(active) === itemLink;
  }
}
