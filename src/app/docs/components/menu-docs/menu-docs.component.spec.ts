import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Pipe, PipeTransform } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';
import { MenuDocsComponent } from './menu-docs.component';
import { MenuLeft } from 'src/app/components/menu-left/model/menu-left-model';

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

describe('MenuLeftComponent', () => {
  const itemsMenu: MenuLeft[] = [
    {
      label: 'Catalogo',
      link: '/catalogo',
      image: 'icon-t-shirt',
      subitems: [
        {
          label: 'Consulta',
          link: '/stock/pim',
        },
        {
          label: 'Ocultar en buscador',
          link: '/stock/exclude-search',
        },
        {
          label: 'Ocultar en buscador. Básica',
          link: '/stock/exclude-search-basic',
        },
        {
          label: 'Limitar ventas',
          link: '/stock/limit-sale',
        },
      ],
    },
    {
      label: 'Sites',
      link: '/sites/site',
      image: 'icon-promotions',
    },
  ];

  let fixture: ComponentFixture<MenuDocsComponent>;
  let component: MenuDocsComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: TranslateService,
          useFactory: () => ({}),
        },
      ],
      imports: [ModalModule.forRoot(), RouterTestingModule],
      declarations: [MenuDocsComponent, TranslatePipeMock],
    }).compileComponents();

    fixture = TestBed.createComponent(MenuDocsComponent);
    component = fixture.componentInstance;
  });

  it('it should create ', () => {
    expect(component).toBeDefined();
  });

  it('navigate link', () => {
    const s = spyOn(component.router, 'navigate');
    component.items = itemsMenu;
    component.go(itemsMenu[0], true);
    expect(s).toHaveBeenCalled();
  });

  it('toogleMenu modified the isMenuOpen flag', () => {
    component.items = itemsMenu;
    const isOpen = component.isMenuOpen;
    component.toggleMenu();
    expect(isOpen).not.toBe(component.isMenuOpen);
    component.toggleMenu();
    expect(isOpen).toBe(component.isMenuOpen);
  });

  it('toogleMenuItem, itemMenu add expanded property when has subitems property', () => {
    component.items = itemsMenu;

    const itemMenu: MenuLeft = {
      label: 'Catalogo',
      link: '/catalogo',
      image: 'icon-t-shirt',
      subitems: [
        {
          label: 'Consulta',
          link: '/stock/pim',
        },
        {
          label: 'Ocultar en buscador',
          link: '/stock/exclude-search',
        },
        {
          label: 'Ocultar en buscador. Básica',
          link: '/stock/exclude-search-basic',
        },
        {
          label: 'Limitar ventas',
          link: '/stock/limit-sale',
        },
      ],
    };

    component.toogleMenuItem(itemMenu);
    expect(itemMenu).toEqual({ ...itemMenu, expanded: true });
  });

  it('subitem is active', () => {
    const s = component.isSubItemActive(itemsMenu[0].link, itemsMenu[0].link);
    expect(s).toBeTruthy();
  });

  it('subitem no active', () => {
    const s = component.isSubItemActive('', itemsMenu[0].link);
    expect(s).toBeFalsy();
  });

  it('if menu is active', () => {
    let item = (itemsMenu[0].subitems && itemsMenu[0].subitems[0].link) as string;
    expect(component.getMenuActive(itemsMenu, item)).toBeTruthy();
  });

  it('navigate to link withouth children', () => {
    component.items = itemsMenu;
    const s = spyOn(component.router, 'navigate');
    component.toogleMenuItem(itemsMenu[1]);
    expect(s).toHaveBeenCalled();
  });

  it('parse url', () => {
    const url = '/ruta/subruta/metodo';
    expect(component.parseUrl(url)).toEqual('/ruta/subruta');
  });
});
