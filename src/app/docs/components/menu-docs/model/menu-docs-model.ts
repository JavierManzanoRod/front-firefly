export interface MenuDocs {
  link: string;
  label: string;
  image?: string;
  subitems?: MenuDocs[];
  expanded?: boolean;
}
