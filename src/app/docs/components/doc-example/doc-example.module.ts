import { NgModule } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { HighlightModule } from 'ngx-highlightjs';
import { SharedModule } from '../../../shared/shared.module';
import { DocExampleComponent } from './doc-example.component';

@NgModule({
  declarations: [DocExampleComponent],
  imports: [SharedModule, TabsModule, HighlightModule],
  exports: [DocExampleComponent],
  providers: [],
})
export class DocExampleModule {}
