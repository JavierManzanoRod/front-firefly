import { Component, Input } from '@angular/core';

@Component({
  selector: 'ff-doc-example',
  templateUrl: './doc-example.component.html',
  styleUrls: ['./doc-example.component.scss'],
})
export class DocExampleComponent {
  @Input() service = false;
  @Input() ngModel = false;
  @Input() angularForm = false;
  @Input() heading!: string;
  @Input() description!: string;
  @Input() data: { html?: string; ts?: string; scss?: string } = {};

  constructor() {}
}
