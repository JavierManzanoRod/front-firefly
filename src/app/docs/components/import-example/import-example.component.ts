import { Component, Input } from '@angular/core';

@Component({
  selector: 'ff-import-example',
  templateUrl: './import-example.component.html',
  styleUrls: ['./import-example.component.scss'],
})
export class ImportExampleComponent {
  @Input() code!: string;

  constructor() {}
}
