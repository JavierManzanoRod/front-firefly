import { NgModule } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { HighlightModule } from 'ngx-highlightjs';
import { SharedModule } from '../../../shared/shared.module';
import { ImportExampleComponent } from './import-example.component';

@NgModule({
  declarations: [ImportExampleComponent],
  imports: [SharedModule, TabsModule, HighlightModule],
  exports: [ImportExampleComponent],
  providers: [],
})
export class ImportExampleModule {}
