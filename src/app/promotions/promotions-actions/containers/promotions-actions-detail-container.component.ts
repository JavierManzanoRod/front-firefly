import { Component } from '@angular/core';

@Component({
  selector: 'ff-site-view-container',
  template: `<ff-page-header
      [pageTitle]="title | translate"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'PROMOTIONS.BREAD_CRUMB_TITLE'"
      [newBreadCrumb]="false"
    >
    </ff-page-header>

    <div class="page-container page-container-padding">
      <ff-promotions-products-detail [actions]="true"></ff-promotions-products-detail>
    </div> `,
})
export class PromotionsActionsDetailContainerComponent {
  urlListado = 'promotions/actions';
  title = 'PROMOTIONS.PRODUCTS.DETAILS.TITLE';

  constructor() {}
}
