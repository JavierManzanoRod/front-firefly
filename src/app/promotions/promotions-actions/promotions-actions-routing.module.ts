import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PromotionsActionsDetailContainerComponent } from './containers/promotions-actions-detail-container.component';
import { PromotionsActionsListContainerComponent } from './containers/promotions-actions-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: PromotionsActionsListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.QUERIES',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'action_id/:action_id',
    component: PromotionsActionsListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.QUERIES',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'action_id/:action_id/site/:site_id',
    component: PromotionsActionsListContainerComponent,
  },
  {
    path: 'action_id/:action_id/site/:site_id/site_name/:site_name',
    component: PromotionsActionsListContainerComponent,
  },
  {
    path: 'details',
    component: PromotionsActionsDetailContainerComponent,
    data: {
      header_title: 'MENU_LEFT.QUERIES',
      breadcrumb: [
        {
          label: 'Volver al listado',
          url: '/promotions/actions',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromotionsActionsRoutingModule {}
