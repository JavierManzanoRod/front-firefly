import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SimplebarAngularModule } from 'simplebar-angular';
import { BreadcrumbModule } from 'src/app/modules/breadcrumb/breadcrumb.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { UiEntityTypesSelectorsModule } from '../../modules/search-form-fields/search-form-fields.module';
import { ToastModule } from '../../modules/toast/toast.module';
import { PromotionsProductsModule } from '../promotions-products/promotions-products.module';
import { PromotionsActionsDetailContainerComponent } from './containers/promotions-actions-detail-container.component';
import { PromotionsActionsListContainerComponent } from './containers/promotions-actions-list-container.component';
import { PromotionsActionsRoutingModule } from './promotions-actions-routing.module';

@NgModule({
  declarations: [PromotionsActionsListContainerComponent, PromotionsActionsDetailContainerComponent],
  imports: [
    CommonModule,
    PromotionsActionsRoutingModule,
    PromotionsProductsModule,
    CommonModalModule,
    SharedModule,
    BreadcrumbModule,
    FormErrorModule,
    FormsModule,
    BreadcrumbModule,
    ReactiveFormsModule,
    CommonModalModule,
    ToastModule,
    BsDatepickerModule.forRoot(),
    SimplebarAngularModule,
    UiEntityTypesSelectorsModule,
  ],
})
export class PromotionsActionsModule {}
