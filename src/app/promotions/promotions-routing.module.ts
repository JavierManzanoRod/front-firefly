import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'products',
    loadChildren: () => import('./promotions-products/promotions-products.module').then((m) => m.PromotionsProductsModule),
  },
  {
    path: 'actions',
    loadChildren: () => import('./promotions-actions/promotions-actions.module').then((m) => m.PromotionsActionsModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromotionsRoutingModule {}
