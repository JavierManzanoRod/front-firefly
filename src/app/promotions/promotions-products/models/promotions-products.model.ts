import { GenericApiRequest } from '@model/base-api.model';

export interface PromotionsProducts {
  action_identifier: string;
  action_name: string;
  active_end_date: string;
  active_start_date: string;
  gtin: string;
  make_up_identifier: string;
  product_id: string;
  sales_reference: string;
  seller: string;
  site: string;
  subaction_identifier: string;
  subsite: string;
  type: string;
  unique_code: string;
  unique_c?: string;
}

export interface PromotionsProductsDetail {
  gtin: string;
  id: string;
  measure_unit?: string;
  price_specifications: {
    centers: {
      code: string;
      identifier: string;
    }[];
    centersName?: string;
    discount_percentage: string;
    start_date: string;
    end_date: string;
    is_sale_price_discounted: boolean;
    is_sale_price_permanent: boolean;
    measure_unit_price: string;
    price: string;
    price_currency: string;
    price_currency_code: string;
    sale_price: string;
    pricing_adjustments: {
      promotional_action: {
        make_up_identifier: string;
        active_end_date: string;
        active_start_date: string;
        identifier: string;
        start_happy_hour: string;
        end_happy_hour: string;
        type: string;
        is_active: boolean;
        periodicity: string;
        promotional_commodity: {
          bonus: {
            value: string;
            type: string;
          };
        };
        promotion: any;
      };
    }[];
    total_price: string;
  }[];
  product_id: string;
  sales_reference: string;
  seller: string;
  site: string;
  subsite: string;
  unique_code: string;
}

export interface PromotionsApiRequest extends GenericApiRequest {
  action_id: string;
  reference: string;
  site_id?: string;
}

export enum DiscountType {
  range = 'range',
  amount_segment = 'amount_segment',
}
