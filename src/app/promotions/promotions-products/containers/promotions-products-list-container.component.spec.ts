import { ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { SearchProvider } from 'src/app/shared/components/list-search/providers/search-provider';
import { PromotionsProductsService } from '../services/promotions-products.service';
import { PromotionsProductsListContainerComponent } from './promotions-products-list-container.component';
import { SubsiteService } from '../../../dashboard/dashboard-query/services/subsite-api.service';

let apiService: PromotionsProductsService;
let route: ActivatedRoute;
let toast: ToastService;
let searchProvider: SearchProvider;
let utils: CrudOperationsService;
let change: ChangeDetectorRef;
let router: Router;
let subsites: SubsiteService;

describe('PromotionsProductsListContainerComponent', () => {
  it('it creates ', () => {
    const comp = new PromotionsProductsListContainerComponent(apiService, route, router, toast, searchProvider, subsites, utils, change);
    expect(comp).toBeDefined();
  });
});
