import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { PromotionsProductsDetailContainerComponent } from './promotions-products-detail-container';

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}
  public get(key: any): any {
    of(key);
  }
}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };
  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }
  public hide() {
    return true;
  }
}

class ToastrServiceStub {
  public success() {}
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

describe('PromotionsProductsDetailContainerComponent', () => {
  let component: PromotionsProductsDetailContainerComponent;
  let fixture: ComponentFixture<PromotionsProductsDetailContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PromotionsProductsDetailContainerComponent, TranslatePipeMock],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceStub },
        { provide: BsModalService, useClass: ModalServiceMock },
        { provide: CrudOperationsService },
        { provide: ToastService, useClass: ToastrServiceStub },
        {
          provide: ActivatedRoute,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: HttpClient,
          useFactory: () => ({
            error: () => {},
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionsProductsDetailContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
