import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { GenericApiRequest, Page } from '@model/base-api.model';
import { combineLatest, of } from 'rxjs';
import { catchError, filter, map, tap } from 'rxjs/operators';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { SubsiteService } from 'src/app/dashboard/dashboard-query/services/subsite-api.service';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { SearchFormFieldsComponent } from 'src/app/modules/search-form-fields/search-form-fields.component';
import { SearchProvider } from 'src/app/shared/components/list-search/providers/search-provider';

import { SearchFormFields } from '../../../modules/search-form-fields/model/search-form-fields.model';
import { ToastService } from '../../../modules/toast/toast.service';
import { PromotionsProductsService } from '../services/promotions-products.service';

@Component({
  selector: 'ff-promotions-products-list-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<ff-page-header [pageTitle]="'PROMOTIONS.PRODUCTS.TITLE' | translate"></ff-page-header>

    <ngx-simplebar class="page-container page-container-padding">
      <section class="background-container">
        <div class="page-section">
          <header class="col page-header-collapse w-50" [class.page-header-collapse-open]="!collapsed" (click)="collapsed = !collapsed">
            <h3>{{ 'PROMOTIONS.PRODUCTS.DETAILS.DETAIL' | translate | uppercase }}</h3>
          </header>
          <section [collapse]="collapsed" [isAnimated]="true">
            <div class="search-container">
              <ff-search-form-fields
                [arrayConfig]="arrayConfig"
                [isReadOnlyOnsubmit]="isReadOnlyOnSubmit"
                (search)="search($event)"
                [showButton]="false"
              >
              </ff-search-form-fields>
            </div>
          </section>
        </div>
        <hr class="division" *ngIf="!collapsed" />
        <div class="btn-container" [class.collapsible]="collapsed">
          <div>
            <div class="page-footer text-right">
              <button type="button" class="btn btn-primary ml-2 btn-lg" (click)="submit()">
                {{ (searcher?.clear ? 'COMMON.CONSULT_AGAIN' : 'COMMON.CONSULT') | translate | uppercase }}
              </button>
            </div>
          </div>
        </div>
        <ff-promotions-products-list
          class="list-section"
          *ngIf="ready"
          [searchData]="filter.reference"
          [loading]="loading"
          [list]="list$ | async"
          [page]="page"
          [link]="link"
          [currentData]="currentData"
          (pagination)="handlePagination($event)"
        ></ff-promotions-products-list>
      </section>
    </ngx-simplebar> `,
  styleUrls: ['./promotions-products-list-container.scss'],
})
export class PromotionsProductsListContainerComponent extends BaseListContainerComponent<any> implements OnInit {
  @ViewChild(SearchFormFieldsComponent, { static: true }) searcher!: SearchFormFieldsComponent;
  ready = false;
  clearFilter: any;
  dataForm = null;
  isReadOnlyOnSubmit = false;
  filter: any;
  collapsed = false;

  link = '/promotions/products/details';
  arrayConfig: SearchFormFields[] = [
    {
      type: 'text',
      key: 'reference',
      label: 'PROMOTIONS.PRODUCTS.CODE',
      placeholder: 'PROMOTIONS.PRODUCTS.CODE_PLACEHOLDER',
      class: 'col-4',
      // eslint-disable-next-line @typescript-eslint/unbound-method
      validators: [Validators.required],
      error: [
        {
          type: 'required',
          message: 'COMMON_ERRORS.REQUIRED',
        },
      ],
    },
    {
      type: 'chips',
      key: 'site',
      label: 'DASHBOARD.SITE_TITLE',
      configChips: {
        label: 'DASHBOARD.SITE_TITLE',
        modalType: ModalChipsTypes.select,
        modalConfig: {
          title: 'DASHBOARD.INFO.PLACEHOLDER.SITE',
          items: [],
          searchFnOnInit: true,
          searchFn: (x: GenericApiRequest) => this.subsiteService.list({ ...x, status: 'true' }),
        },
      },
      class: 'col-4',
      error: [
        {
          type: 'required',
          message: 'COMMON_ERRORS.REQUIRED',
        },
      ],
    },
  ];

  constructor(
    public apiService: PromotionsProductsService,
    public route: ActivatedRoute,
    public router: Router,
    public toast: ToastService,
    public searchProvider: SearchProvider,
    private subsiteService: SubsiteService,
    public utils: CrudOperationsService,
    public change: ChangeDetectorRef
  ) {
    super(utils, apiService);
  }

  ngOnInit() {
    combineLatest([this.route.params, this.route.queryParams])
      .pipe(
        map(([params, { page }]) => [params, Number.parseInt(page, 10)] as [Params, number]),
        filter(([, page]) => page !== this.page?.page_number),
        tap(
          ([, page]) =>
            (this.page = page
              ? {
                  page_number: page - 1,
                  page_size: 10,
                  total_elements: 100,
                  total_pages: 10,
                }
              : this.page)
        )
      )
      .subscribe(([params]) => {
        if (params.reference) {
          this.filter = params;
          this.searchProvider.data = params.site_id ? { ...params, site: { id: params.site_id, name: params.site_name } } : params;
          this.filter = this.page
            ? {
                ...this.filter,
                page: this.page.page_number,
                size: this.page.page_size,
              }
            : this.filter;
          this.getDataList(this.filter);
          this.isReadOnlyOnSubmit = true;
          this.searcher.clear = true;
        } else {
          this.ready = false;
          this.list$ = of(null);
          this.searcher.clear = false;
        }
        this.change.markForCheck();
      });
  }

  getDataList(filterParams: any) {
    this.loading = true;
    this.ready = true;
    this.list$ = this.apiService.list(filterParams).pipe(
      tap(({ page }) => {
        this.page = page;
        this.updateQueryPage(page);
      }),
      tap(({ content }) => (this.tmpList = content)),
      tap(({ content }) => {
        if (content.length === 0) {
          this.utils.toast.warning('COMMON_ERRORS.SEARCH');
        }
      }),
      map(({ content }) => content),
      catchError((e) => {
        this.page = undefined;
        if (e.status === 404) {
          this.utils.toast.warning('COMMON_ERRORS.ERROR_404');
        } else if (e.status) {
          this.utils.toast.error('COMMON_ERRORS.INTERNAL_ERROR');
        }
        return of(null);
      }),
      tap(() => (this.loading = false))
    );
  }

  search(criteria: any) {
    if (criteria.site) {
      this.router.navigateByUrl(
        `/promotions/products/reference/${criteria.reference}/site/${criteria.site.site_id}/site_name/${criteria.site.name}`
      );
    } else this.router.navigateByUrl(`/promotions/products/reference/${criteria.reference}`);
  }

  handlePagination($event: any) {
    if ($event.size) {
      const dataToSearch = {
        ...this.filter,
        page: $event.page - 1,
        size: $event.size,
      };
      this.getDataList(dataToSearch);
    }
  }

  clean() {
    this.router.navigateByUrl(`/promotions/products`);
  }

  submit() {
    if (this.searcher.clear) {
      this.searcher.clearData();
      this.clean();
    } else {
      this.searcher.submit();
    }
  }

  private updateQueryPage(page: Page) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { page: page.page_number + 1 },
      queryParamsHandling: 'merge', // remove to replace all query params by provided
    });
  }
}
