import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ff-site-view-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<ff-page-header
      [pageTitle]="title | translate"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'PROMOTIONS.BREAD_CRUMB_TITLE'"
      [newBreadCrumb]="false"
    >
    </ff-page-header>

    <div class="page-container page-container-padding">
      <ff-promotions-products-detail></ff-promotions-products-detail>
    </div> `,
  styleUrls: ['./promotions-products-detail-container.component.scss'],
})
export class PromotionsProductsDetailContainerComponent {
  urlListado = 'promotions/products';
  title = 'PROMOTIONS.PRODUCTS.DETAILS.TITLE';

  constructor() {}
}
