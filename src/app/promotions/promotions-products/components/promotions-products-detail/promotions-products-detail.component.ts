/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { DatePipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { finalize, tap, map } from 'rxjs/operators';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { notifyRequestStatus } from 'src/app/shared/utils/operators/notify-operator';
import { DiscountType, PromotionsProductsDetail } from '../../models/promotions-products.model';
import { PromotionsProductsService } from '../../services/promotions-products.service';
import { UiModalInfoComponent } from '../../../../modules/ui-modal-info/ui-modal-info.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'ff-promotions-products-detail',
  templateUrl: './promotions-products-detail.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./promotions-products-detail.component.scss'],
})
export class PromotionsProductsDetailComponent implements OnInit {
  @Input() actions = false;

  @ViewChild('variantsModalTemplate') conditionModalElement!: UiModalInfoComponent;
  discountTypeEnum = DiscountType;
  public packType = '-';
  public packs: string[] = [];
  public collapsed = false;
  isCollapsedConfig: any = {};
  loadingModal = false;
  variantData: string[] = [];
  titleModal = 'PROMOTIONS.PRODUCTS.DETAILS.DISCOUNT';
  modalRef!: BsModalRef;
  detail: any;
  loading = true;
  params: any;
  packTranslate = {
    COLLECTION: 'PROMOTIONS.PRODUCTS.DETAILS.COLLECTION',
    CONFIGURABLE: 'PROMOTIONS.PRODUCTS.DETAILS.CONFIGURABLE',
  };
  constructor(
    private route: ActivatedRoute,
    private apiService: PromotionsProductsService,
    private translate: TranslateService,
    private datePipe: DatePipe,
    private ref: ChangeDetectorRef,
    private modalService: BsModalService,
    private toastService: ToastService
  ) {
    //Workaround because angular-oauth2-oidc removes "code" params
    const unique_code = this.route.snapshot.queryParams?.unique_code || this.route.snapshot.queryParams?.unique_c;
    this.params = { ...this.route.snapshot.queryParams, ...(unique_code && { unique_code }) };
  }

  get promotionalAction() {
    return this.detail.price_specifications[0]?.pricing_adjustments[0]?.promotional_action;
  }

  get bonus() {
    return this.promotionalAction?.promotional_commodity?.bonus || {};
  }

  get discount() {
    return this.bonus && Object.keys(this.bonus).length > 1 ? `${this.bonus.value} ${this.bonus.type}` : '0';
  }

  get discountType() {
    return this.bonus ? this.bonus.type : '';
  }

  get salesReference() {
    return (this.params?.sales_reference as string).split(',');
  }
  get actionIdentifier() {
    return ((this.params?.action_identifier as string) || '-').replace(',', ', ');
  }

  ngOnInit(): void {
    this.apiService
      .detail(this.params)
      .pipe(
        notifyRequestStatus(this.toastService, null as unknown as PromotionsProductsDetail),
        tap((val) => {
          if (!val) {
            this.back();
          }
        }),
        finalize(() => {
          this.loading = false;
          setTimeout(() => {
            this.ref.markForCheck();
          }, 0);
        })
      )
      .subscribe((data) => {
        if (data) {
          this.detail = data;
          this.packs = this.detail.item_offered?.pack_products
            ? this.detail.item_offered?.pack_products.map((pack: any) => pack?.product?.offer.sales_reference)
            : [];
          this.packType = this.parsePackType(this.detail.item_offered?.pack_type);
          this.detail.price_specifications = this.detail.price_specifications.map((value: any) => {
            value.centersName = value.centers.map((info: any) => info.identifier || info.code).join(', ');
            value.pricing_adjustments = value.pricing_adjustments.map((pricing: any) => {
              pricing.promotional_action.active_end_date = this.datePipe.transform(
                pricing.promotional_action.active_end_date,
                'dd-MM-yyyy'
              );

              pricing.promotional_action.active_start_date = this.datePipe.transform(
                pricing.promotional_action.active_start_date,
                'dd-MM-yyyy'
              );
              return pricing;
            });
            return value;
          });
        }
      });
  }

  back() {
    history.back();
  }

  parseHappyHours(start: string, end: string): string {
    return start === '00:00' && end === '23:59' ? this.translate.instant('PROMOTIONS.PRODUCTS.DETAILS.NO_HAPPY') : `${start} ${end} `;
  }

  parsePackType(type: 'COLLECTION' | 'CONFIGURABLE') {
    const packType = this.packTranslate[type];
    return packType ? packType : type || '-';
  }

  showModalInfo() {
    this.loadingModal = true;
    this.createDataToModal();
    this.modalRef = this.modalService.show(this.conditionModalElement as any, { class: 'mt-10' });
  }

  hideModal() {
    this.loadingModal = false;
    this.modalRef.hide();
  }

  createDataToModal() {
    const ranges = this.bonus.range.bonus_range_values;
    if (this.discountType === DiscountType.range) {
      this.variantData = ranges.map((range: any) =>
        range.max_type === '9999'
          ? this.translate.instant('PROMOTIONS.PRODUCTS.DETAILS.BONUS.RANGE_WITH_MIN', {
              value_type: range.value_type,
              min_type: range.min_type,
            })
          : this.translate.instant('PROMOTIONS.PRODUCTS.DETAILS.BONUS.RANGE_WITH_RANGE', {
              value_type: range.value_type,
              min_type: range.min_type,
              max_type: range.max_type,
            })
      );
    } else if (this.discountType === DiscountType.amount_segment) {
      this.variantData = ranges.map((range: any) =>
        this.translate.instant('PROMOTIONS.PRODUCTS.DETAILS.BONUS.AMOUNT_SEGMENT', {
          value_type: range.value_type,
          max_type: range.max_type,
        })
      );
    }
  }
}
