import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { PromotionsProductsService } from '../../services/promotions-products.service';
import { PromotionsProductsDetailComponent } from './promotions-products-detail.component';

xdescribe('PromotionsProductsDetailComponent', () => {
  let component: PromotionsProductsDetailComponent;
  let fixture: ComponentFixture<PromotionsProductsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PromotionsProductsDetailComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => of({ id: 123 }),
              },
              params: {},
            },
          },
        },
        {
          provide: DatePipe,
        },
        {
          provide: PromotionsProductsService,
          useFactory: () => ({
            error: () => {},
            detail: () => {
              of(null);
            },
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionsProductsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
