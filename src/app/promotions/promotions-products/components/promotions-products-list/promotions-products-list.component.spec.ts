import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { PromotionsProductsListComponent } from './promotions-products-list.component';

describe('PromotionsProductsListComponent', () => {
  let component: PromotionsProductsListComponent;
  let fixture: ComponentFixture<PromotionsProductsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PromotionsProductsListComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        DatePipe,
        {
          provide: Router,
          useFactory: () => ({
            error: () => {},
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionsProductsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
