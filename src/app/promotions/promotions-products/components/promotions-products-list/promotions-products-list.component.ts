import { DatePipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GenericListConfig } from 'src/app/modules/generic-list/models/generic-list.model';
import { BaseListComponent } from 'src/app/shared/components/base-component-list.component';
import { PromotionsProducts } from './../../models/promotions-products.model';

@Component({
  selector: 'ff-promotions-products-list',
  templateUrl: './promotions-products-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./promotions-products-list.component.scss'],
})
export class PromotionsProductsListComponent extends BaseListComponent<PromotionsProducts> implements OnChanges, OnInit {
  @Input() link = '/promotions/products/details';
  @Input() actions = false;
  @Input() searchData = '';

  arrayKeys: GenericListConfig[] = [];

  canDelete = false;

  constructor(public route: Router, public translate: TranslateService, public datePipe: DatePipe) {
    super();
  }

  get thisPage() {
    return this.page?.page_number + 1;
  }

  ngOnInit() {
    this.arrayKeys = [
      {
        key: this.actions ? 'sales_reference' : 'action_identifier',
        headerName: this.actions ? 'PROMOTIONS.PRODUCTS.SALES_REFERENCE' : 'PROMOTIONS.PRODUCTS.DETAILS.CODE',
      },
      {
        key: 'action_name',
        headerName: 'PROMOTIONS.PRODUCTS.NAME',
      },
      {
        key: 'type',
        headerName: 'PROMOTIONS.PRODUCTS.TYPE',
      },
      {
        key: 'site',
        headerName: 'PROMOTIONS.PRODUCTS.SITE',
      },
      {
        key: 'active_start_date',
        headerName: 'PROMOTIONS.PRODUCTS.START',
      },
      {
        key: 'active_end_date',
        headerName: 'PROMOTIONS.PRODUCTS.END',
      },
      {
        key: 'make_up_identifier',
        headerName: 'PROMOTIONS.PRODUCTS.MAKE_UP',
      },
    ];
  }

  routerFilter = (item: PromotionsProducts): void => {
    //Workaround because angular-oauth2-oidc removes "code" params
    if (item.unique_code) {
      item.unique_c = item.unique_code;
    }
    this.route.navigate([this.link], {
      queryParams: item,
      state: { goBack: true, page: this.page, id: this.searchData },
    });
  };

  ngOnChanges() {
    this.list?.forEach((value) => {
      value.active_start_date = value.active_start_date ? this.datePipe.transform(value.active_start_date, 'dd-MM-yyyy') || '' : '';
      value.active_end_date = value.active_end_date ? this.datePipe.transform(value.active_end_date, 'dd-MM-yyyy') || '' : '';
      value.type = this.translate.instant(value.type);
      return value;
    });
  }
}
