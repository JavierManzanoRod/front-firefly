import { HttpClient } from '@angular/common/http';
import { GenericApiResponse } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { PromotionsProducts } from '../models/promotions-products.model';
import { PromotionsProductsService } from './promotions-products.service';

let httpClientSpy: { get: jasmine.Spy };

let myService: PromotionsProductsService;
const expectedData: PromotionsProducts = {
  action_identifier: '1',
  action_name: 'name',
  active_end_date: '0000',
  active_start_date: '1111',
  gtin: 'gtin',
  make_up_identifier: 'id',
  product_id: '123',
  sales_reference: 'Eci',
} as PromotionsProducts;
const expectedList: GenericApiResponse<PromotionsProducts> = {
  content: [expectedData],
  page: null as any,
};

let translate: TranslateService;
let version: 'v1/';

describe('PromotionsProductsService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
  });

  it(`PromotionsProductsService points to promotions/backoffice-promotions/${version}`, () => {
    myService = new PromotionsProductsService((null as unknown) as HttpClient, (null as unknown) as TranslateService, version);
    expect(myService.endPoint).toEqual(`promotions/backoffice-promotions/${version}`);
  });

  it('PromotionsProductsService.list calls to get api method', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new PromotionsProductsService(httpClientSpy as any, translate, version);
    const product = {
      action_identifier: '1',
      action_name: 'name',
      active_end_date: '0000',
      active_start_date: '1111',
      gtin: 'gtin',
      make_up_identifier: 'id',
      product_id: '123',
      sales_reference: 'Eci',
      action_id: '1',
      reference: '3',
    };
    myService.list(product).subscribe((response) => {});
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
