import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { GenericApiResponse } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_PROMOTIONS } from 'src/app/configuration/tokens/api-versions.token';
import { PromotionsApiRequest, PromotionsProducts, PromotionsProductsDetail } from '../models/promotions-products.model';

@Injectable({
  providedIn: 'root',
})
export class PromotionsProductsService extends ApiService<any> {
  endPoint = `promotions/backoffice-promotions/${this.version}`;
  private readonly typePromotions = ['A', 'P', 'D', 'C', 'E', 'O', 'G', 'B'];

  constructor(
    protected http: HttpClient,
    private translateService: TranslateService,
    @Inject(FF_API_PATH_VERSION_PROMOTIONS) private version: string
  ) {
    super();
  }

  search(code: string | number): Observable<any> {
    return this.http.get(this.urlBase + `salesReference/${code}`).pipe(
      catchError(() => {
        return of(null);
      })
    );
  }

  detail(item: any): Observable<PromotionsProductsDetail> {
    return this.http.get<PromotionsProductsDetail>(`${this.urlBase}promotional-actions`, { params: item }).pipe(
      map((value) => {
        let typeBonus = value.price_specifications[0]?.pricing_adjustments[0]?.promotional_action?.promotional_commodity?.bonus.type;
        if (typeBonus === 'amount') {
          typeBonus = value.price_specifications[0].price_currency_code;
        }
        if (typeBonus === 'percentage') {
          typeBonus = '%';
        }
        if (typeBonus) {
          value.price_specifications[0].pricing_adjustments[0].promotional_action.promotional_commodity.bonus.type = typeBonus;
        }

        let type = value.price_specifications[0]?.pricing_adjustments[0]?.promotional_action.type;
        const search = this.typePromotions.includes(type);
        type = search ? 'PROMOTIONS.PRODUCTS.TYPEPROMOTIONS.' + type : type;
        if (type) {
          value.price_specifications[0].pricing_adjustments[0].promotional_action.type = this.translateService.instant(type);
        }

        return value;
      })
    );
  }

  parseParam(filter: PromotionsApiRequest) {
    let params: HttpParams = new HttpParams();
    if (filter) {
      params = filter.page_number ? params.set('page_number', filter.page_number.toString()) : params;
      params = filter.page_size ? params.set('page_size', filter.page_size.toString()) : params;
      params = filter.size ? params.set('size', filter.size.toString()) : params;
      params = filter.page ? params.set('page', filter.page.toString()) : params;
      params = filter.site_id ? params.set('site', filter.site_id.toString()) : params;
    }

    return params;
  }

  list(item: PromotionsApiRequest): Observable<GenericApiResponse<PromotionsProducts>> {
    const url = item.action_id
      ? `${this.urlBase}promotional-actions/${item.action_id}/offers`
      : `${this.urlBase}offers/${item.reference}/promotional-actions`;
    return this.http
      .get<GenericApiResponse<PromotionsProducts>>(url, {
        params: this.parseParam(item),
      })
      .pipe(
        map((value) => {
          value.content = this.getTypePromotions(value.content);
          return value;
        })
      );
  }

  getTypePromotions(array: PromotionsProducts[]): any[] {
    return array.map((value) => {
      const type = value.type;
      const search = this.typePromotions.includes(type);
      value.type = search ? 'PROMOTIONS.PRODUCTS.TYPEPROMOTIONS.' + type : type;
      return value;
    });
  }
}
