import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PromotionsProductsDetailContainerComponent } from './containers/promotions-products-detail-container';
import { PromotionsProductsListContainerComponent } from './containers/promotions-products-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: PromotionsProductsListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.QUERIES',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'reference/:reference',
    component: PromotionsProductsListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.QUERIES',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'reference/:reference/site/:site_id',
    component: PromotionsProductsListContainerComponent,
  },
  {
    path: 'reference/:reference/site/:site_id/site_name/:site_name',
    component: PromotionsProductsListContainerComponent,
  },
  {
    path: 'details',
    component: PromotionsProductsDetailContainerComponent,
    data: {
      header_title: 'MENU_LEFT.QUERIES',
      breadcrumb: [
        {
          label: 'Volver al listado',
          url: '/promotions/products',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromotionsProductsRoutingModule {}
