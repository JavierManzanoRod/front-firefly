import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { UiModalInfoModule } from 'src/app/modules/ui-modal-info/ui-modal-info.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BreadcrumbModule } from '../../modules/breadcrumb/breadcrumb.module';
import { UiEntityTypesSelectorsModule } from '../../modules/search-form-fields/search-form-fields.module';
import { ToastModule } from '../../modules/toast/toast.module';
import { GenericListComponentModule } from './../../modules/generic-list/generic-list.module';
import { PromotionsProductsDetailComponent } from './components/promotions-products-detail/promotions-products-detail.component';
import { PromotionsProductsListComponent } from './components/promotions-products-list/promotions-products-list.component';
import { PromotionsProductsDetailContainerComponent } from './containers/promotions-products-detail-container';
import { PromotionsProductsListContainerComponent } from './containers/promotions-products-list-container.component';
import { PromotionsProductsRoutingModule } from './promotions-products-routing.module';

@NgModule({
  declarations: [
    PromotionsProductsListContainerComponent,
    PromotionsProductsDetailContainerComponent,
    PromotionsProductsListComponent,
    PromotionsProductsDetailComponent,
  ],
  imports: [
    CommonModule,
    PromotionsProductsRoutingModule,
    CommonModalModule,
    SharedModule,
    FormErrorModule,
    FormsModule,
    GenericListComponentModule,
    CoreModule,
    BreadcrumbModule,
    ReactiveFormsModule,
    UiModalInfoModule,
    CommonModalModule,
    ToastModule,
    BsDatepickerModule.forRoot(),
    SimplebarAngularModule,
    TranslateModule,
    UiEntityTypesSelectorsModule,
  ],
  providers: [DatePipe],
  exports: [PromotionsProductsListComponent, PromotionsProductsDetailComponent],
})
export class PromotionsProductsModule {}
