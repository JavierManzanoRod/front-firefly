import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { PromotionsRoutingModule } from './promotions-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, PromotionsRoutingModule, CollapseModule],
})
export class PromotionsModule {}
