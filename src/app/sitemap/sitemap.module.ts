import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from '../modules/common-modal/common-modal.module';
import { ToastModule } from '../modules/toast/toast.module';
import { SharedModule } from '../shared/shared.module';
import { SitemapComponent } from './components/sitemap/sitemap.component';
import { SitemapContainerComponent } from './containers/sitemap-container.component';
import { SitemapRoutingModule } from './sitemap-routing.module';

@NgModule({
  declarations: [SitemapContainerComponent, SitemapComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SitemapRoutingModule,
    CommonModalModule,
    SharedModule,
    ToastModule,
    SimplebarAngularModule,
  ],
  exports: [SitemapContainerComponent, SitemapComponent],
})
export class SitemapModule {}
