import { Component } from '@angular/core';

@Component({
  selector: 'ff-sitemap-container',
  template: `<ff-page-header [pageTitle]="'SITEMAP.SITEMAP' | translate"> </ff-page-header>
    <ngx-simplebar class="page-container">
      <div class="page-container-padding">
        <ff-sitemap></ff-sitemap>
      </div>
    </ngx-simplebar> `,
})
export class SitemapContainerComponent {}
