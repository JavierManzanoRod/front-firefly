import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SitemapContainerComponent } from './containers/sitemap-container.component';

const routes: Routes = [
  {
    path: '',
    component: SitemapContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SitemapRoutingModule {}
