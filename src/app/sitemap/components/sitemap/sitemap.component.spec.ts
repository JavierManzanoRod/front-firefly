import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { SitemapComponent } from './sitemap.component';

describe('SitemapComponent', () => {
  let componentToTest: SitemapComponent;
  let router: Router;

  beforeEach(() => {
    router = null as unknown as Router;
    const translateServiceMock = {
      instant: (someText: string) => someText,
    } as unknown as TranslateService;

    const menuLeft: MenuLeftService = new MenuLeftService(router, []);

    componentToTest = new SitemapComponent(menuLeft, translateServiceMock, router);
  });
});
