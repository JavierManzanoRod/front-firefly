import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MenuLeft } from '../../../components/menu-left/model/menu-left-model';
import { MenuLeftService } from '../../../components/menu-left/services/menu-left.service';

interface MenuSiteMap extends MenuLeft {
  isActive?: boolean;
  subitems?: MenuSiteMap[];
}
@Component({
  selector: 'ff-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['../../../components/menu-left/menu-left.component.scss', './sitemap.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SitemapComponent {
  sectionFilter?: string = undefined;
  activeSectionFilter?: string = undefined;
  menu: MenuSiteMap[] = [];

  constructor(private menuLeftService: MenuLeftService, private translateService: TranslateService, private router: Router) {
    this.menu = this.getMenuConfig();
  }

  getMenuConfig(): MenuLeft[] {
    const menu = this.menuLeftService.getMenuItem() as MenuSiteMap[];
    const menuFiltered = menu.filter((e) => ['APP_MENU.HOME', 'APP_MENU.SITEMAP'].indexOf(e.label) < 0);

    menuFiltered.forEach((item: MenuLeft) => {
      item.short = item.short && this.translateService.instant(item.short);
      item.label = this.translateService.instant(item.label);
      if (Array.isArray(item.subitems)) {
        item.subitems.forEach((subitem: MenuLeft) => {
          subitem.short = subitem.short && this.translateService.instant(subitem.short);
          subitem.label = this.translateService.instant(subitem.label);
        });
      }
    });

    menuFiltered.forEach((item: MenuLeft) => {
      if (item.label === 'Envíos' || item.label === 'Administrador MKP') {
        menuFiltered.splice(menuFiltered.indexOf(item), 1);
      }
    });

    return menuFiltered;
  }

  go(item: MenuLeft) {
    if (!item?.subitems?.length) {
      this.router.navigate([item.link]);
    }
  }

  search() {
    this.reset();
    this.activeSectionFilter = this.sectionFilter || '';
    const strToSearch = this.activeSectionFilter.toLowerCase();
    this.menu.forEach((group) => {
      const itemResult = group.label.toLowerCase().indexOf(strToSearch) >= 0;
      group.isActive = itemResult;
      (group?.subitems || []).forEach((subItem) => {
        const subItemResult = subItem.label.toLowerCase().indexOf(strToSearch) >= 0;
        subItem.isActive = subItemResult;
      });
    });
    if (this.activeSectionFilter === '') {
      this.reset();
    }
  }

  reset() {
    this.menu.forEach((group) => {
      group.isActive = false;
      (group?.subitems || []).forEach((subItem) => {
        subItem.isActive = false;
      });
    });
  }
}
