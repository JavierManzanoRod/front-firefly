import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'ff-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FileUploadComponent,
      multi: true,
    },
  ],
})
export class FileUploadComponent implements OnInit {
  @Input() key!: string;
  @Input() acceptExt!: string;
  @Input() eventClean = new Subject<any>();
  @Output() fileLoad: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('input') input!: ElementRef;

  fileNameDefault = 'COMMON.SEARCH_FILE';
  fileName = this.fileNameDefault;
  haveFile: any;

  private file: File | null = null;
  constructor() {}

  @HostListener('change', ['$event.target.files']) emitFiles(event: FileList) {
    const file = event && event.item(0);
    this.onChange(file);
    this.fileName = file?.name || '';
    this.file = file;
  }

  ngOnInit() {
    if (this.eventClean) {
      this.eventClean.subscribe(() => this.writeValue(null));
    }
  }

  writeValue(value: null) {
    // clear file input
    this.fileName = this.fileNameDefault;
    this.fileLoad.emit(value);
    this.input.nativeElement.value = '';
    this.haveFile = false;
    this.file = null;
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  onChange(data: any) {
    // don't delete!
    this.fileLoad.emit(data);
    this.haveFile = true;
  }

  registerOnTouched() {}
}
