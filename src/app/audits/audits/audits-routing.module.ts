import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditDetailContainerComponent } from './containers/audit-detail.container';
import { AuditHistoryContainerComponent } from './containers/audit-history.container';
import { AuditListContainerComponent } from './containers/audit-list.container';

const data = {
  header_title: 'MENU_LEFT.AUDIT',
  breadcrumb: [
    {
      label: '',
      url: '',
    },
  ],
};

const details = [
  {
    path: 'entity_types/:index',
    loadChildren: () => import('./features/entity-types/entity-types.module').then((m) => m.AuditsEntityTypesModule),
    data,
  },
  {
    path: 'sites/:index',
    loadChildren: () => import('./features/sites/sites.module').then((m) => m.AuditsSitesModule),
    data,
  },
  {
    path: 'subsites/:index',
    loadChildren: () => import('./features/sites/sites.module').then((m) => m.AuditsSitesModule),
    data,
  },
  {
    path: 'facet_links/:index',
    loadChildren: () => import('./features/facet-links/facet-links.module').then((m) => m.AuditsFacetLinksModule),
    data,
  },
  {
    path: 'admin_group_folders/:index',
    loadChildren: () => import('./features/admin-group-folders/admin-group-folders.module').then((m) => m.AuditsAdminGroupFoldersModule),
    data,
  },
  {
    path: 'entities/:index',
    loadChildren: () => import('./features/entities/entities.module').then((m) => m.AuditsEntitiesModule),
    data,
  },
  {
    path: 'admin_group_types/:index',
    loadChildren: () => import('./features/admin-group-types/admin-group-types.module').then((m) => m.AuditsAdminGroupTypeModule),
    data,
  },
  {
    path: 'spec_templates/:index',
    loadChildren: () => import('./features/spec-templates/spec-templates.module').then((m) => m.AuditsSpecTemplatesModule),
    data,
  },
  {
    path: 'content_group/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'limit_sales/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'exclude_search/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'category_rules/:index',
    loadChildren: () => import('./features/category-rules/category-rules.module').then((m) => m.AuditsCategoryRulesModule),
    data,
  },
  {
    path: 'admin_groups/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'size_guide_url/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'related_services/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'eci_fluor_gases/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'experts/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'price_inheritance/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'loyalty/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'badge/:index',
    loadChildren: () => import('./features/admin-groups/admin-groups.module').then((m) => m.AuditsAdminGroupModule),
    data,
  },
  {
    path: 'attributes_translations/:index',
    loadChildren: () =>
      import('./features/attributes-translations/attributes-translations.module').then((m) => m.AuditsAttributesTranslationsModule),
    data,
  },
];

const routes: Routes = [
  {
    path: '',
    component: AuditListContainerComponent,
    data,
    children: [
      {
        path: 'history/:administration/:entity_id',
        component: AuditHistoryContainerComponent,
        data,
        children: [
          {
            path: 'detail',
            component: AuditDetailContainerComponent,
            children: details,
            data: {
              history: true,
            },
          },
        ],
      },
      {
        path: 'detail',
        component: AuditDetailContainerComponent,
        children: details,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsRoutingModule {}
