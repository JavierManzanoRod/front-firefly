import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { ExplodedTreeModule } from 'src/app/modules/exploded-tree/exploded-tree.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuditsRoutingModule } from './audits-routing.module';
import { AuditFiltersComponent } from './components/audit-filters/audit-filters.component';
import { AuditModalTreeComponent } from './components/audit-modal-tree/audit-modal-tree.component';
import { AuditDetailContainerComponent } from './containers/audit-detail.container';
import { AuditHistoryContainerComponent } from './containers/audit-history.container';
import { AuditListContainerComponent } from './containers/audit-list.container';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuditsRoutingModule,
    TranslateModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    NgSelectModule,
    DatetimepickerModule,
    FormErrorModule,
    ExplodedTreeModule,
  ],
  declarations: [
    AuditDetailContainerComponent,
    AuditListContainerComponent,
    AuditHistoryContainerComponent,
    AuditFiltersComponent,
    AuditModalTreeComponent,
  ],
  providers: [DatePipe],
  exports: [],
})
export class AuditsModule {}
