import { Component, OnInit } from '@angular/core';
import { Audit, AuditOperator } from '@core/models/audit.model';
import { AuditFilterData } from '../models/audits.model';
import { AuditDetailContainerComponent } from './audit-detail.container';
import { AuditListContainerComponent } from './audit-list.container';

@Component({
  selector: 'ff-audit-history-container',
  template: `
    <router-outlet (activate)="activate($event)" (deactivate)="deactivate()"></router-outlet>
    <div [hidden]="hide">
      <ff-page-header
        pageTitle="{{ 'AUDITS.HISTORY' | uppercase | translate: { administration: administrationLabel | translate | lowercase } }}"
        [breadcrumbLink]="backUrl"
        [breadcrumbTitle]="'AUDITS.BACK' | translate"
      >
      </ff-page-header>
      <ngx-simplebar class="page-container page-container-padding">
        <ff-audit-filters (filtering)="applyFilter($event)" [hideAdministration]="true" (clearEvent)="clear()"></ff-audit-filters>
        <ff-generic-list
          [hidden]="!tmpList || !tmpList.length"
          [arrayKeys]="arrayKeys"
          [list]="list$ | async"
          [loading]="loading"
          [page]="page"
          [currentData]="currentData"
          (delete)="delete($event)"
          (pagination)="handlePagination($event)"
          [type]="type"
          [isViewOnly]="true"
          [canView]="true"
          [canDelete]="false"
          [routerFilter]="routerFilter.bind(this)"
          [viewFilter]="viewFilter.bind(this)"
          [title]="''"
          [route]="'/rule-engine/admin-group/'"
        >
        </ff-generic-list>
        <div class="text-center pt-5 pb-5" *ngIf="loading">
          <div class="loader"></div>
        </div>
      </ngx-simplebar>
    </div>
  `,
})
export class AuditHistoryContainerComponent extends AuditListContainerComponent implements OnInit {
  backUrl = '/audits';
  id!: string;

  get administrationLabel() {
    return 'AUDITS.TITLES.' + this.administration.toUpperCase().replace(/-/g, '_');
  }
  ngOnInit() {
    this.id = this.route.snapshot.params.entity_id;
    this.administration = this.route.snapshot.params.administration;
    this.applyFilter({} as any);
  }

  applyFilter(data: AuditFilterData) {
    if (data) {
      data.custom_filters = (data?.custom_filters || []).concat([
        {
          field: 'id',
          operator: AuditOperator.EQUALS,
          field_value: this.id,
        },
      ]);

      super.applyFilter({ ...data, administration: this.administration });
    }
  }

  routerFilter(item: Audit, index: number) {
    this.router.navigate(['/audits', 'history', this.administration, this.id, 'detail', this.administration, index]);
  }

  activate(component: AuditDetailContainerComponent) {
    super.activate(component);

    component.historyBack = true;
    component.entity_id = this.id;
  }
}
