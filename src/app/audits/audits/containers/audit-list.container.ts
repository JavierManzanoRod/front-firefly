import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { Audit, AuditQuery, AuditType } from '@core/models/audit.model';
import { of, Subject } from 'rxjs';
import { catchError, map, takeUntil, tap } from 'rxjs/operators';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { GenericListComponent } from '../../../modules/generic-list/generic-list.component';
import { AuditFiltersComponent } from '../components/audit-filters/audit-filters.component';
import { AuditAdministrationTypes, AuditFilterData, AuditFilterDataKeys } from '../models/audits.model';
import { AuditsService } from '../services/audits.service';
import { AuditDetailContainerComponent } from './audit-detail.container';
import { AuditHistoryContainerComponent } from './audit-history.container';

@Component({
  selector: 'ff-audit-list-container',
  template: `
    <router-outlet (activate)="activate($event)" (deactivate)="deactivate()"></router-outlet>
    <div [hidden]="hide">
      <ff-page-header pageTitle="{{ 'AUDITS.TITLE' | translate }}"> </ff-page-header>
      <ngx-simplebar class="page-container page-container-padding">
        <ff-audit-filters (filtering)="applyFilter($event)" (clearEvent)="clear()"></ff-audit-filters>
        <ff-generic-list
          [hidden]="!tmpList || !tmpList.length"
          [arrayKeys]="arrayKeys"
          [list]="list$ | async"
          [loading]="loading"
          [page]="page"
          [currentData]="currentData"
          (delete)="delete($event)"
          (pagination)="handlePagination($event)"
          [type]="type"
          [canView]="true"
          [isViewOnly]="true"
          [canDelete]="false"
          [canHistory]="true"
          [routerFilter]="routerFilter.bind(this)"
          [viewFilter]="viewFilter.bind(this)"
          [historyFilter]="historyFilter.bind(this)"
          [title]="''"
          [route]="'/rule-engine/admin-group/'"
        >
        </ff-generic-list>
        <div class="text-center pt-5 pb-5" *ngIf="loading">
          <div class="loader"></div>
        </div>
      </ngx-simplebar>
    </div>
  `,
})
export class AuditListContainerComponent extends BaseListContainerComponent<Audit> implements OnInit {
  @ViewChild(AuditFiltersComponent) filters!: AuditFiltersComponent;
  @ViewChild(GenericListComponent) list!: GenericListComponent;

  hide = false;
  reset = false;
  type = TypeSearch.none;
  administration!: AuditAdministrationTypes;
  arrayKeys: GenericListConfig[] = [
    /*{
      key: 'site',
      headerName: 'AUDITS.SITE',
      textCenter: true,
      width: '15%',
    },
    */
    {
      key: 'revision.user',
      headerName: 'AUDITS.USER',
      width: '15%',
    },
    {
      key: 'revision.date',
      headerName: 'AUDITS.DATE',
      textCenter: false,
      canFormatDate: true,
      formatDate: 'dd-MM-yyyy HH:mm',
      width: '15%',
    },
    /*
    {
      key: 'revision.identifier',
      headerName: 'AUDITS.ID',
      textCenter: true,
      width: '15%',
    },
    */
    {
      key: 'entity.name',
      headerName: 'AUDITS.NAME',
      textCenter: false,
      width: '50%',
    },
    /*
    {
      key: 'revision.comments',
      headerName: 'AUDITS.COMMENTS',
      textCenter: true,
      width: '35%',
    },
    */
    {
      key: 'operation',
      headerName: 'AUDITS.OPERATION',
      textCenter: true,
      width: '5%',
      customFormat: (item: Audit) => {
        return `AUDITS.FILTER.ACTION.OPTIONS.${item.operation?.toUpperCase()}`;
      },
    },
  ];
  public destroy$ = new Subject<void>();
  constructor(public route: ActivatedRoute, public router: Router, public apiService: AuditsService, public crud: CrudOperationsService) {
    super(crud, apiService);
  }

  ngOnInit() {}

  clear() {
    this.loading = false;
    this.destroy$.next();
  }

  applyFilter(data: AuditFilterData) {
    if (data) {
      const { administration, ...filter } = (Object.keys(data) as AuditFilterDataKeys[]).reduce(
        (ends: AuditFilterData, key: AuditFilterDataKeys) => {
          if (data[key]) {
            (ends[key] as any) = data[key];
          }
          return ends;
        },
        {} as AuditFilterData
      );

      this.administration = administration;

      let name = this.arrayKeys.find((a) => a.key === 'entity.visual_path' || a.key === 'entity.path');
      if (name) {
        name.key = 'entity.name';
      }

      if (this.administration === 'facet_links') {
        name = this.arrayKeys.find((a) => a.key === 'entity.name');
        if (name) {
          name.key = 'entity.visual_path';
        }
      } else if (this.administration === 'attributes_translations') {
        name = this.arrayKeys.find((a) => a.key === 'entity.name');
        if (name) {
          name.key = 'entity.path';
        }
      }

      this.apiService.switchEndpoint(administration);
      this.getDataList(filter);
    } else {
      if (this.list.currentPage) {
        this.reset = true;
      }

      this.list.currentPage = 0;
      this.list$ = of(null);
      this.tmpList = [];
    }
  }

  getDataList(filter: AuditQuery) {
    if (this.reset) {
      // return (this.reset = false);
    }

    this.loading = true;
    this.filter = filter;
    this.tmpList = [];

    this.list$ = this.apiService.audit(filter).pipe(
      tap(({ page }) => (this.page = page)),
      tap(({ content }) => (this.tmpList = content)),
      tap(({ content }) => {
        if (content.length === 0) {
          this.utils.toast.warning('COMMON_ERRORS.SEARCH', undefined, { timeOut: 2500 });
        }
      }),

      map(({ content }) => content),
      catchError((err) => {
        if (err.status === 404) {
          this.utils.toast.error('COMMON_ERRORS.SEARCH_ERROR', undefined, { timeOut: 2500 });
        } else if (err.status === 400 || err.status === 403 || err.status === 500 || err.status === 503) {
          this.utils.toast.error('COMMON_ERRORS.INTERNAL_ERROR', undefined, { timeOut: 2500 });
        }
        if (!this.apiService.service) {
          this.utils.toast.error('AUDITS.ERRORS.ENDPOINT_NOT_FOUND', undefined, { timeOut: 2500 });
        }
        this.page = undefined;
        return of(null);
      }),
      tap(() => (this.loading = false)),
      takeUntil(this.destroy$)
    );
  }

  viewFilter(item: Audit) {
    if (item.operation && [AuditType.UPDATE, AuditType.INSERT].includes(item.operation)) {
      return true;
    }
  }

  routerFilter(_item: Audit, index: number) {
    this.router.navigate(['/audits', 'detail', this.administration, index]);
  }

  historyFilter(item: Audit) {
    this.router.navigate(['/audits', 'history', this.administration, item.entity?.identifier || item.entity?.id]);
  }

  activate(component: AuditDetailContainerComponent | AuditHistoryContainerComponent) {
    this.hide = true;

    if (component instanceof AuditDetailContainerComponent) {
      component.list = this.tmpList || [];
      component.backUrl = 'audits';
    }
    if (component instanceof AuditHistoryContainerComponent) {
      component.administration = this.administration;
    }
  }

  deactivate() {
    this.hide = false;
  }
}
