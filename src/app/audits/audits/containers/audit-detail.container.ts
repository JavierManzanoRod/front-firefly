import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, RouterOutlet } from '@angular/router';
import { Audit } from '@core/models/audit.model';
import { AuditComponentInterface } from '../models/audits.model';
@Component({
  selector: 'ff-audit-detail-container',
  template: ` <ff-page-header
      pageTitle="{{ administrationLabel | translate }}"
      [breadcrumbLink]="backUrl"
      [breadcrumbTitle]="backLabel | translate"
    >
      <a *ngIf="historyBack" slot="extra" class="back pointer" (click)="history()"
        ><span class="icon-select-right mirror-x"></span> {{ 'AUDITS.BACK_HISTORY' | translate }}
      </a>
    </ff-page-header>
    <router-outlet></router-outlet>`,
  styleUrls: ['./audit-detail.container.scss'],
})
export class AuditDetailContainerComponent implements OnInit, AfterViewInit {
  @ViewChild(RouterOutlet) outlet!: RouterOutlet;

  backUrl!: string;
  backLabel = 'AUDITS.BACK';
  historyBack = false;
  list: Audit[] = [];
  data: any = null;
  type!: string;
  entity_id!: string;
  index!: number;
  revision?: number;

  get administrationLabel() {
    return 'AUDITS.TITLES.' + (this.type && this.type.toUpperCase().replace(/-/g, '_'));
  }

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    if (!this.list.length) {
      this.back();
    }

    this.index = this.route.snapshot.children[0].params.index;
    if (this.index !== -1) {
      const audit = this.list[this.index];
      if (audit) {
        this.data = audit.entity;
        this.type = this.route.snapshot.children[0].url[0].path;
        this.revision = audit.revision?.identifier;
      } else {
        this.back();
      }
    }
  }

  ngAfterViewInit() {
    const component: AuditComponentInterface = this.outlet.component as AuditComponentInterface;

    setTimeout(() => {
      component.data = this.data;
      component.type = this.type;
      component.revision = this.revision;

      component.ngAfterLoadData();
    });
  }

  back() {
    this.router.navigate(['/audits']);
  }

  history() {
    this.router.navigate(['/audits', 'history', this.type, this.entity_id]);
  }
}
