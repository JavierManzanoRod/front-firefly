import { Injectable } from '@angular/core';
import { Audit, AuditOperator } from '@core/models/audit.model';
import { GenericApiResponse } from '@model/base-api.model';
import { forkJoin, Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import {
  Condition2Attribute,
  Condition2Reference,
  Condition2Simple,
  Condition2SimpleEntity,
} from 'src/app/modules/conditions2/models/conditions2.model';
import { CategoryRuleDTO } from 'src/app/rule-engine/category-rule/models/category-rule.dto';
import { CategoryRule } from 'src/app/rule-engine/category-rule/models/category-rule.model';
import { CategoryRuleService } from 'src/app/rule-engine/category-rule/services/category-rule.service';
import { CategoryRuleInMapper } from 'src/app/rule-engine/category-rule/services/dto/category-rule/category-rule-in-mapper.class';
import { EntityRepositoryDTO } from 'src/app/rule-engine/entity/models/entity.dto';
import { EntityRepository } from 'src/app/rule-engine/entity/models/entity.model';
import { EntityRepositoryInMapper } from 'src/app/rule-engine/entity/services/dto/entity-repository/entity-repository-in-mapper.class';
import { EntityService } from 'src/app/rule-engine/entity/services/entity.service';

@Injectable({
  providedIn: 'root',
})
export class AuditEntityMapperService {
  cacheEntity: { key: string; reference: Condition2SimpleEntity }[] = [];
  cacheReference: { key: string; reference: Condition2Reference }[] = [];

  constructor(private entity: EntityService, private reference: CategoryRuleService) {}

  checkEntity(
    node: Condition2Simple & Condition2SimpleEntity & { nodes: Condition2Simple[] & Condition2SimpleEntity[] },
    revision?: number
  ): Observable<Array<EntityRepository | CategoryRule | GenericApiResponse<Audit>>> {
    this.cacheEntity = [];
    this.cacheReference = [];

    this.checkNode(node);

    const subjects: Observable<EntityRepository | CategoryRule | GenericApiResponse<Audit>>[] = [];

    this.cacheEntity.forEach(({ key, reference }) => {
      subjects.push(
        this.entity
          .auditDetail({
            size: 1,
            custom_filters: [
              {
                field: 'id',
                operator: AuditOperator.EQUALS,
                field_value: key,
              },
              {
                field: 'revision',
                operator: AuditOperator.LESS_THAN_OR_EQUALS,
                field_value: revision?.toString() || '',
              },
            ],
          })
          .pipe(
            tap((audit: GenericApiResponse<Audit>) => {
              const data = new EntityRepositoryInMapper(audit.content[0].entity as EntityRepositoryDTO).data;

              if (!reference.entity_info) {
                reference.entity_info = {};
              }
              reference.entity_info[key] = {
                entity_name: data.label || data.name || '',
                entity_id: key,
                entity_type_id: data.entity_type_id,
                entity_type_name: '',
                labels: {
                  label: data.label || '',
                },
              };
            })
          )
      );
    });

    this.cacheReference.forEach(({ key, reference }) => {
      subjects.push(
        this.reference
          .audit({
            size: 1,
            custom_filters: [
              {
                field: 'id',
                operator: AuditOperator.EQUALS,
                field_value: key,
              },
              {
                field: 'revision',
                operator: AuditOperator.LESS_THAN_OR_EQUALS,
                field_value: revision?.toString() || '',
              },
            ],
          })
          .pipe(
            tap((audit: GenericApiResponse<Audit>) => {
              const data = new CategoryRuleInMapper(audit.content[0].entity as CategoryRuleDTO).data;

              reference.reference_name = data.name;
            })
          )
      );
    });

    if (subjects.length) {
      return forkJoin(subjects);
    } else {
      return of([]);
    }
  }

  private checkNode(node: Condition2Simple & Condition2SimpleEntity & { nodes: Condition2Simple[] & Condition2SimpleEntity[] }) {
    const attribute = node.attribute && this.getAttribute(node.attribute);

    if (attribute && attribute.data_type === 'ENTITY' && !node.entity_info) {
      if (node.value && node.value instanceof Array) {
        node.value.forEach((id: string) => {
          this.cacheEntity.push({
            key: id,
            reference: node,
          });
        });
      } else if (node.value) {
        this.cacheEntity.push({
          key: node.value,
          reference: node,
        });
      }
    }

    if (node.nodes && node.nodes instanceof Array) {
      node.nodes.forEach((n) => {
        this.checkNode(n as any);
      });
    }

    const reference = node as unknown as Condition2Reference;
    if (reference.node_type === 'reference' && reference.reference_id) {
      this.cacheReference.push({
        key: reference.reference_id,
        reference,
      });
    }
  }

  private getAttribute(attribute: Condition2Attribute): Condition2Attribute {
    let attr = attribute;
    if (attribute.reference) {
      attr = this.getAttribute(attribute.reference);
    }

    return attr as unknown as Condition2Attribute;
  }
}
