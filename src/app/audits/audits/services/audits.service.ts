import { Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { Audit, AuditOperator, AuditQuery } from '@core/models/audit.model';
import { GenericApiResponse } from '@model/base-api.model';
import { Observable, of } from 'rxjs';
import { AttributeTranslationService } from 'src/app/catalog/attribute-translation/services/attribute-translation.service';
import { ContentGroupService } from 'src/app/catalog/content-group/services/content-group.service';
import { FacetDataApiService } from 'src/app/catalog/facet/services/apis/facet-data-api.service';
import { FacetLinkApiService } from 'src/app/catalog/facet/services/apis/facet-link-data-api.service';
import { ProductTypeApiService } from 'src/app/catalog/product-type/services/apis/product-type-api.service';
import { TechnicalCharacteristicsService } from 'src/app/catalog/technical-characteristics/services/technical-characteristics.services';
import { BadgeService } from 'src/app/control-panel/badge/services/badge.service';
import { ExpertAdminService } from 'src/app/control-panel/expert-admin/services/expert-admin.service';
import { FluorinatedGasService } from 'src/app/control-panel/fluorinated-gas/services/fluorinated-gas.service';
import { LoyaltyService } from 'src/app/control-panel/loyalty/services/loyalty.service';
import { SizeGuideService } from 'src/app/control-panel/size-guide/services/size-guide.service';
import { SpecialProductService } from 'src/app/control-panel/special-product/services/special-product.service';
import { SubsiteService } from 'src/app/dashboard/dashboard-query/services/subsite-api.service';
import { AdminGroupTypeService } from 'src/app/rule-engine/admin-group-type/services/admin-group-type.service';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { CategoryRuleService } from 'src/app/rule-engine/category-rule/services/category-rule.service';
import { EntityService } from 'src/app/rule-engine/entity/services/entity.service';
import { PriceInheritanceService } from 'src/app/rule-engine/price-inheritance/services/price-inheritance.service';
import { AdminGroupFoldersServiceGeneral } from 'src/app/shared/services/apis/admin-group-folders-general.service';
import { AdminGroupServiceGeneral } from 'src/app/shared/services/apis/admin-group-general.service';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { SiteService } from 'src/app/sites/site/services/site.service';
import { ExcludeSearchService } from 'src/app/stock/exclude-search/services/exclude-search.service';
import { LimitSaleService } from 'src/app/stock/limit-sale/services/limit-sale.service';
import { AuditAdministrationTypes } from '../models/audits.model';

@Injectable({
  providedIn: 'root',
})
export class AuditsService extends ApiService<Audit> {
  service?: ApiService<any>;

  adminGroupTypes: { [key in keyof Partial<AuditsService>]: string } = {
    content_group: 'content_group',
    limit_sales: 'limit_sale',
    exclude_search: 'hidden',
    size_guide_url: 'size_guide_url',
    related_services: 'related_services',
    eci_fluor_gases: 'eci_fluor_gases',
    experts: 'experts',
    price_inheritance: 'price_inheritance',
    loyalty: 'loyalty',
    badge: 'badge',
  };

  administration!: keyof AuditsService;

  /**
   * Note:
   *
   * @see es.json
   * @see AuditsService
   * En el AuditService se debe definir el nombre del servicio en el constructor de la misma forma
   * constructor(private entity_type: EntityTypeService, ...)
   *
   * @see MenuLeftService
   * Los nombres de las auditorias deben ser igual que los de APP_MENU.
   * De esta forma tenemos unificado los nombres de los menus con las auditorias.
   * Por ejemplo entity types sería en el APP_MENU.ENTITY_TYPES pues debemos poner entity_type.
   *
   */
  constructor(
    public entity_types: EntityTypeService,
    public products_types: ProductTypeApiService,
    public entities: EntityService,
    public category_rules: CategoryRuleService,
    public facets: FacetDataApiService,
    public facet_links: FacetLinkApiService,
    public admin_group_types: AdminGroupTypeService,
    public admin_group_folders: AdminGroupFoldersServiceGeneral<any>,
    public subsites: SubsiteService,
    public sites: SiteService,
    public content_group: ContentGroupService,
    public spec_templates: TechnicalCharacteristicsService,
    public limit_sales: LimitSaleService,
    public exclude_search: ExcludeSearchService,
    public admin_groups: AdminGroupServiceGeneral<AdminGroup>,
    public size_guide_url: SizeGuideService,
    public related_services: SpecialProductService,
    public eci_fluor_gases: FluorinatedGasService,
    public experts: ExpertAdminService,
    public price_inheritance: PriceInheritanceService,
    public attributes_translations: AttributeTranslationService,
    public loyalty: LoyaltyService,
    public badge: BadgeService
  ) {
    super();
  }

  switchEndpoint(administration: AuditAdministrationTypes) {
    this.administration = administration as unknown as keyof AuditsService;

    if (this[this.administration] instanceof ApiService) {
      this.service = this[this.administration] as ApiService<any>;
    } else {
      this.service = undefined;
    }
  }

  audit(query: AuditQuery): Observable<GenericApiResponse<Audit>> {
    if (this.service) {
      if (this.adminGroupTypes[this.administration] && !query.custom_filters.find((f) => f.field === 'type')) {
        query.custom_filters = query.custom_filters.concat([
          {
            field: 'type',
            operator: AuditOperator.EQUALS,
            field_value: this.adminGroupTypes[this.administration] || '',
          },
        ]);
      }

      if (this.administration === 'facet_links') {
        const name = query.custom_filters.find((f) => f.field === 'name');
        if (name) {
          name.field = 'visualPath';
        }
      }

      if (this.administration === 'attributes_translations') {
        const name = query.custom_filters.find((f) => f.field === 'name');
        if (name) {
          name.field = 'path';
        }
      }

      return this.service.audit(query);
    }

    return of(null as unknown as GenericApiResponse<Audit>);
  }
}
