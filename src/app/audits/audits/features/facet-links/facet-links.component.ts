import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { FacetDetailContainerService } from 'src/app/catalog/facet/containers/facet-detail-container.service';
import { AttributeFacet, Facet, FacetLinkDTO } from 'src/app/catalog/facet/models/facet.model';
import { AuditComponentInterface } from '../../models/audits.model';

@Component({
  selector: 'ff-audit-facet-links',
  templateUrl: './facet-links.component.html',
  styleUrls: ['./facet-links.component.scss'],
  providers: [FacetDetailContainerService],
})
export class AuditFacetLinksComponent implements AuditComponentInterface<FacetLinkDTO> {
  identifier!: number;
  data!: FacetLinkDTO;
  type = '';
  attribute!: Partial<AttributeFacet>;
  facet!: Partial<Facet>;
  selectedAttribute$ = new Subject<AttributeFacet>();

  constructor(private state: FacetDetailContainerService) {
    this.state.init(this.selectedAttribute$);
  }

  ngAfterLoadData() {
    setTimeout(() => {
      this.attribute = {
        label: this.data.attribute_name,
        name: this.data.attribute_name,
        id: this.data.attribute_id,
        data_type:
          (this.data.attribute_type as 'STRING' | 'DOUBLE' | 'BOOLEAN' | 'ENTITY' | 'EMBEDDED' | 'DATE_TIME' | 'VIRTUAL') || undefined,
      };

      this.facet = {
        data_type: this.data.facet_type,
        id: this.data.identifier,
        name: this.data.facet_name,
      };

      // this.data = {};

      // this.state.usageFacetSubject.next(this.data);
    });
  }
}
