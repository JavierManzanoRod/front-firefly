import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { FacetModule } from 'src/app/catalog/facet/facet.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuditsFacetLinksRoutingModule } from './facet-links-routing.module';
import { AuditFacetLinksComponent } from './facet-links.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    AuditsFacetLinksRoutingModule,
    TranslateModule,
    FacetModule,
  ],
  declarations: [AuditFacetLinksComponent],
  providers: [DatePipe],
  exports: [],
})
export class AuditsFacetLinksModule {}
