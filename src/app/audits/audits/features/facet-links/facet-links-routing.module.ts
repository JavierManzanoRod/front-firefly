import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditFacetLinksComponent } from './facet-links.component';

const routes: Routes = [
  {
    path: '',
    component: AuditFacetLinksComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsFacetLinksRoutingModule {}
