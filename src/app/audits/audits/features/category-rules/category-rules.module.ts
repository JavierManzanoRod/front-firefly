import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { CategoryRuleModule } from 'src/app/rule-engine/category-rule/category-rule.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuditsCategoryRulesRoutingModule } from './category-rules-routing.module';
import { AuditCategoryRulesComponent } from './category-rules.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    AuditsCategoryRulesRoutingModule,
    TranslateModule,
    CategoryRuleModule,
  ],
  declarations: [AuditCategoryRulesComponent],
  providers: [DatePipe],
  exports: [],
})
export class AuditsCategoryRulesModule {}
