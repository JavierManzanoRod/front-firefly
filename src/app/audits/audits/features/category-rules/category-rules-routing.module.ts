import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditCategoryRulesComponent } from './category-rules.component';

const routes: Routes = [
  {
    path: '',
    component: AuditCategoryRulesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsCategoryRulesRoutingModule {}
