import { Component } from '@angular/core';
import { catchError, of } from 'rxjs';
import { Condition2Simple } from 'src/app/modules/conditions2/models/conditions2.model';
import { CategoryRuleDTO } from 'src/app/rule-engine/category-rule/models/category-rule.dto';
import { InputCategoryDetailComponent } from 'src/app/rule-engine/category-rule/models/category-rule.model';
import { CategoryRuleInMapper } from 'src/app/rule-engine/category-rule/services/dto/category-rule/category-rule-in-mapper.class';
import { AuditComponentInterface } from '../../models/audits.model';
import { AuditEntityMapperService } from '../../services/audits-entity-mapper.service';
import { Condition2SimpleEntity } from '../../../../modules/conditions2/models/conditions2.model';

@Component({
  selector: 'ff-audit-category-rules',
  templateUrl: './category-rules.component.html',
  styleUrls: ['./category-rules.component.scss'],
})
export class AuditCategoryRulesComponent implements AuditComponentInterface<InputCategoryDetailComponent> {
  identifier!: number;
  data!: InputCategoryDetailComponent;
  item!: InputCategoryDetailComponent;
  error = false;
  type = '';
  revision!: number;

  constructor(private entityMapper: AuditEntityMapperService) {}

  ngAfterLoadData() {
    this.data.categoryRule = new CategoryRuleInMapper(this.data as unknown as CategoryRuleDTO, true).data;
    this.entityMapper
      .checkEntity(
        this.data.categoryRule.node as Condition2Simple & Condition2SimpleEntity & { nodes: Condition2Simple[] & Condition2SimpleEntity[] },
        this.revision
      )
      .pipe(catchError(() => of((this.error = true))))
      .subscribe(() => {
        this.item = this.data;
      });
  }
}
