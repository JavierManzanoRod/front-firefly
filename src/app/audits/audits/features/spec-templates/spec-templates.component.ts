import { Component } from '@angular/core';
import {
  TechnicalCharacteristicsDetail,
  TechnicalCharacteristicsDetailDTO,
} from 'src/app/catalog/technical-characteristics/models/technical-characteristics.model';
import { TechnicalCharacteristicsDetailOut } from '../../../../catalog/technical-characteristics/services/dtos/technical-characteristics-detail-out-mapper-class';
import { AuditComponentInterface } from '../../models/audits.model';

@Component({
  selector: 'ff-audit-spec-templates',
  templateUrl: './spec-templates.component.html',
  styleUrls: ['./spec-templates.component.scss'],
})
export class AuditSpecTemplatesComponent implements AuditComponentInterface<TechnicalCharacteristicsDetail> {
  identifier!: number;
  data!: TechnicalCharacteristicsDetail;
  type = '';

  constructor() {}

  ngAfterLoadData() {
    this.data = new TechnicalCharacteristicsDetailOut(this.data as unknown as TechnicalCharacteristicsDetailDTO).data;
    console.log(this.data);
  }
}
