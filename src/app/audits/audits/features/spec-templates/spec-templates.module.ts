import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { TechnicalCharacteristicsModule } from 'src/app/catalog/technical-characteristics/technical-characteristics.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuditsSpecTemplatesRoutingModule } from './spec-templates-routing.module';
import { AuditSpecTemplatesComponent } from './spec-templates.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    AuditsSpecTemplatesRoutingModule,
    TranslateModule,
    TechnicalCharacteristicsModule,
  ],
  declarations: [AuditSpecTemplatesComponent],
  providers: [DatePipe],
  exports: [],
})
export class AuditsSpecTemplatesModule {}
