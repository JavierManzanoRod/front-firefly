import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditSpecTemplatesComponent } from './spec-templates.component';

const routes: Routes = [
  {
    path: '',
    component: AuditSpecTemplatesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsSpecTemplatesRoutingModule {}
