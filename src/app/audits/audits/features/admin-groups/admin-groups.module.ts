import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ConditionsBasicAndAdvanceModule } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { AdminGroupModule } from 'src/app/rule-engine/admin-group/admin-group.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuditsAdminGroupsRoutingModule } from './admin-groups-routing.module';
import { AuditAdminGroupsComponent } from './admin-groups.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    AuditsAdminGroupsRoutingModule,
    TranslateModule,
    AdminGroupModule,
    ConditionsBasicAndAdvanceModule,
  ],
  declarations: [AuditAdminGroupsComponent],
  providers: [DatePipe],
  exports: [],
})
export class AuditsAdminGroupModule {}
