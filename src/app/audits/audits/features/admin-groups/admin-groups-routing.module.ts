import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditAdminGroupsComponent } from './admin-groups.component';

const routes: Routes = [
  {
    path: '',
    component: AuditAdminGroupsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsAdminGroupsRoutingModule {}
