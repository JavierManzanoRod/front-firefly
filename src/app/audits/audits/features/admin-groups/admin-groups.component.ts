import { Component } from '@angular/core';
import { RulesType } from 'src/app/modules/conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { AdminGroup, AdminGroupDTO } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { AdminGroupOutMapper } from 'src/app/rule-engine/admin-group/services/DTO/admin-group-out-mapper-class';
import { AuditAdministrationTypes, AuditComponentInterface } from '../../models/audits.model';
import { AuditEntityMapperService } from '../../services/audits-entity-mapper.service';
import { AuditsService } from '../../services/audits.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'ff-audit-admin-groups',
  templateUrl: './admin-groups.component.html',
  styleUrls: ['./admin-groups.component.scss'],
})
export class AuditAdminGroupsComponent implements AuditComponentInterface<AdminGroup> {
  identifier!: number;
  data!: AdminGroup;
  item!: AdminGroup;
  ruleType?: RulesType;
  type!: AuditAdministrationTypes;
  error = false;
  revision?: number | undefined;

  ruleTypes: { [key in keyof Partial<AuditsService>]: RulesType } = {
    size_guide_url: RulesType.size_guide,
    related_services: RulesType.special_product,
    eci_fluor_gases: RulesType.fluorinated_gas,
    experts: RulesType.expert_admin,
    price_inheritance: RulesType.price_inheritance,
    badge: RulesType.badge,
  };

  constructor(private entityMapper: AuditEntityMapperService) {}

  ngAfterLoadData() {
    if (this.data) {
      this.data = new AdminGroupOutMapper(this.data as unknown as AdminGroupDTO, true).data;
      this.entityMapper
        .checkEntity(this.data.node as any, this.revision)
        .pipe(catchError(() => of((this.error = true))))
        .subscribe(() => {
          this.item = this.data;
        });

      if (this.type) {
        this.ruleType = this.ruleTypes[this.type] || undefined;
      }
    }
  }
}
