import { Component } from '@angular/core';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { AttributeTranslations, AttributeTranslationsDTO } from 'src/app/catalog/attribute-translation/models/attribute-translation.model';
import { AttributeOutTranslations } from 'src/app/catalog/attribute-translation/services/dtos/attribute-out-translation';
import { AuditComponentInterface } from '../../models/audits.model';

@Component({
  selector: 'ff-audit-attributes-translations',
  templateUrl: './attributes-translations.component.html',
  styleUrls: ['./attributes-translations.component.scss'],
})
export class AuditAttributesTranslationsComponent implements AuditComponentInterface<AttributeTranslations> {
  identifier!: number;
  data!: AttributeTranslations;
  selectedAttribute!: AttributeEntityType;
  type = '';

  constructor() {}

  ngAfterLoadData() {
    this.data = new AttributeOutTranslations(this.data as unknown as AttributeTranslationsDTO).data;

    this.selectedAttribute = {
      name: this.data.path || '',
      label: this.data.path || '',
      id: this.data.path || '',
      data_type: 'STRING',
    };
  }
}
