import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { AttributeTranslationModule } from 'src/app/catalog/attribute-translation/attribute-translation.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuditsAttributesTranslationsRoutingModule } from './attributes-translations-routing.module';
import { AuditAttributesTranslationsComponent } from './attributes-translations.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    AuditsAttributesTranslationsRoutingModule,
    TranslateModule,
    AttributeTranslationModule,
  ],
  declarations: [AuditAttributesTranslationsComponent],
  providers: [DatePipe],
  exports: [],
})
export class AuditsAttributesTranslationsModule {}
