import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditAttributesTranslationsComponent } from './attributes-translations.component';

const routes: Routes = [
  {
    path: '',
    component: AuditAttributesTranslationsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsAttributesTranslationsRoutingModule {}
