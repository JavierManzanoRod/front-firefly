import { ChangeDetectorRef, Component } from '@angular/core';
import { EntityTypeDTO } from '@core/models/entity-type.dto';
import { EntityType } from '@core/models/entity-type.model';
import { ExplodedTreeNode } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { EntityTypeInMapper } from 'src/app/shared/services/apis/entity-type/dto/entity-type/entity-type-in-mapper.class';
import { AuditComponentInterface } from '../../models/audits.model';

@Component({
  selector: 'ff-audit-entity-types',
  templateUrl: './entity-types.component.html',
  styleUrls: ['./entity-types.component.scss'],
})
export class AuditEntityTypesComponent implements AuditComponentInterface<EntityType> {
  identifier!: number;
  data!: EntityType;
  tree!: ExplodedTreeNode;

  constructor(private change: ChangeDetectorRef) {}

  ngAfterLoadData() {
    this.tree = {
      children: new EntityTypeInMapper(this.data as unknown as EntityTypeDTO).data.attributes.map((attribute) => ({
        value: attribute,
      })),
      value: {},
    };
    this.change.markForCheck();
  }
}
