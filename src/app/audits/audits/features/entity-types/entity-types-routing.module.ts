import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditEntityTypesComponent } from './entity-types.component';

const routes: Routes = [
  {
    path: '',
    component: AuditEntityTypesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsEntityTypesRoutingModule {}
