import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/core.module';
import { SimplebarAngularModule } from 'simplebar-angular';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { EntityTypeModule } from 'src/app/rule-engine/entity-type/entity-type.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuditsEntityTypesRoutingModule } from './entity-types-routing.module';
import { AuditEntityTypesComponent } from './entity-types.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    AuditsEntityTypesRoutingModule,
    EntityTypeModule,
  ],
  declarations: [AuditEntityTypesComponent],
  providers: [DatePipe],
  exports: [],
})
export class AuditsEntityTypesModule {}
