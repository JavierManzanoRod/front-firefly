import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditAdminGroupFoldersComponent } from './admin-group-folders.component';

const routes: Routes = [
  {
    path: '',
    component: AuditAdminGroupFoldersComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsAdminGroupFoldersRoutingModule {}
