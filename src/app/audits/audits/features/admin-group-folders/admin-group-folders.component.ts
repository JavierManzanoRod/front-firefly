import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ContentGroup } from 'src/app/catalog/content-group/model/content-group.models';
import { CategoryRule } from 'src/app/rule-engine/category-rule/models/category-rule.model';
import { FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { AuditComponentInterface } from '../../models/audits.model';

@Component({
  selector: 'ff-audit-admin-group-folders',
  templateUrl: './admin-group-folders.component.html',
  styleUrls: ['./admin-group-folders.component.scss'],
})
export class AuditAdminGroupFoldersComponent implements AuditComponentInterface<CategoryRule | ContentGroup> {
  identifier!: number;
  data!: CategoryRule | ContentGroup;
  type = '';

  form: FormGroup | undefined;

  constructor() {
    const form = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      name: {},
    });
    this.form = form.form;
  }

  ngAfterLoadData() {
    this.form?.patchValue(this.data);
  }
}
