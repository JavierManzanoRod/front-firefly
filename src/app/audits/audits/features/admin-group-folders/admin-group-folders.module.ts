import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuditsAdminGroupFoldersRoutingModule } from './admin-group-folders-routing.module';
import { AuditAdminGroupFoldersComponent } from './admin-group-folders.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    ReactiveFormsModule,
    CoreModule,
    AuditsAdminGroupFoldersRoutingModule,
    TranslateModule,
  ],
  declarations: [AuditAdminGroupFoldersComponent],
  providers: [DatePipe],
  exports: [],
})
export class AuditsAdminGroupFoldersModule {}
