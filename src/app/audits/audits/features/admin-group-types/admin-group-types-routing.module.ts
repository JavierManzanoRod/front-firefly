import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditAdminGroupTypesComponent } from './admin-group-types.component';

const routes: Routes = [
  {
    path: '',
    component: AuditAdminGroupTypesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsAdminGroupTypeRoutingModule {}
