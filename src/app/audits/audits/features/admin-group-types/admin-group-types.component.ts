import { Component } from '@angular/core';
import { AdminGroupTypeDTO } from 'src/app/rule-engine/admin-group-type/models/admin-group-type-dto.model';
import { AdminGroupType } from 'src/app/rule-engine/admin-group-type/models/admin-group-type.model';
import { AdminGroupTypeOutMapper } from 'src/app/rule-engine/admin-group-type/services/dto/admin-group-type-out-mapper-class';
import { AuditComponentInterface } from '../../models/audits.model';

@Component({
  selector: 'ff-audit-admin-group-types',
  templateUrl: './admin-group-types.component.html',
  styleUrls: ['./admin-group-types.component.scss'],
})
export class AuditAdminGroupTypesComponent implements AuditComponentInterface<AdminGroupType> {
  identifier!: number;
  data!: AdminGroupType;
  type = '';

  constructor() {}

  ngAfterLoadData() {
    this.data = { ...new AdminGroupTypeOutMapper(this.data as unknown as AdminGroupTypeDTO).data };
  }
}
