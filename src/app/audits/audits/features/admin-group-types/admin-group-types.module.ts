import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { AdminGroupTypeModule } from 'src/app/rule-engine/admin-group-type/admin-group-type.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuditsAdminGroupTypeRoutingModule } from './admin-group-types-routing.module';
import { AuditAdminGroupTypesComponent } from './admin-group-types.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    AuditsAdminGroupTypeRoutingModule,
    TranslateModule,
    AdminGroupTypeModule,
  ],
  declarations: [AuditAdminGroupTypesComponent],
  providers: [DatePipe],
  exports: [],
})
export class AuditsAdminGroupTypeModule {}
