import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditEntitiesComponent } from './entities.component';

const routes: Routes = [
  {
    path: '',
    component: AuditEntitiesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsEntitiesRoutingModule {}
