import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { EntityModule } from 'src/app/rule-engine/entity/entity.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuditsEntitiesRoutingModule } from './entities-routing.module';
import { AuditEntitiesComponent } from './entities.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    AuditsEntitiesRoutingModule,
    TranslateModule,
    EntityModule,
  ],
  declarations: [AuditEntitiesComponent],
  providers: [DatePipe],
  exports: [],
})
export class AuditsEntitiesModule {}
