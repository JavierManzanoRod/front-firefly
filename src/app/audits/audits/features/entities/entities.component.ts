/* eslint-disable @typescript-eslint/no-unsafe-call */
import { Component } from '@angular/core';
import { AuditOperator } from '@core/models/audit.model';
import { EntityType } from '@core/models/entity-type.model';
import { EntityTypeDTO } from '@core/models/entity-type.dto';
import { EntityRepositoryDTO } from 'src/app/rule-engine/entity/models/entity.dto';
import { Entity, EntityRepository } from 'src/app/rule-engine/entity/models/entity.model';
import { EntityRepositoryInMapper } from 'src/app/rule-engine/entity/services/dto/entity-repository/entity-repository-in-mapper.class';
import { EntityTypeInMapper } from 'src/app/shared/services/apis/entity-type/dto/entity-type/entity-type-in-mapper.class';
import { EntityService } from 'src/app/rule-engine/entity/services/entity.service';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { EntityApiFactoryService } from 'src/app/rule-engine/entity/services/entity-api-factory.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { AuditComponentInterface } from '../../models/audits.model';

@Component({
  selector: 'ff-audit-entities',
  templateUrl: './entities.component.html',
  styleUrls: ['./entities.component.scss'],
})
export class AuditEntitiesComponent implements AuditComponentInterface<Entity> {
  identifier!: number;
  data!: Entity;
  type = '';
  revision?: number;
  actualRevision?: number;
  entity!: Entity;
  entityType!: EntityType;
  original!: Entity;

  constructor(
    private entityService: EntityService,
    private entityTypeService: EntityTypeService,
    private toast: ToastrService,
    private translate: TranslateService,
    public entityApiFactoryService: EntityApiFactoryService
  ) {}

  ngAfterLoadData() {
    this.data = new EntityRepositoryInMapper({ ...this.data, attributes: [] } as unknown as EntityRepositoryDTO).data as unknown as Entity;

    this.entityService
      .auditDetail({
        size: 1,
        custom_filters: [
          {
            field: 'id',
            operator: AuditOperator.EQUALS,
            field_value: this.data.id || '',
          },
          {
            field: 'revision',
            operator: AuditOperator.EQUALS,
            field_value: this.revision?.toString() || '',
          },
        ],
      })
      .subscribe((data) => {
        const entity = new EntityRepositoryInMapper({ ...data.content[0].entity } as unknown as EntityRepositoryDTO)
          .data as unknown as Entity;
        this.entityTypeService
          .audit({
            size: 1,
            custom_filters: [
              {
                field: 'id',
                operator: AuditOperator.EQUALS,
                field_value: entity.entity_type_id || '',
              },
              {
                field: 'revision',
                operator: AuditOperator.LESS_THAN_OR_EQUALS,
                field_value: this.revision?.toString() || '',
              },
            ],
          })
          .subscribe((type) => {
            const entityType = new EntityTypeInMapper({ ...type.content[0].entity } as unknown as EntityTypeDTO)
              .data as unknown as EntityType;
            this.actualRevision = data.content[0].revision.identifier;

            if (this.actualRevision && this.revision) {
              if (this.actualRevision < this.revision) {
                this.toast.warning(this.translate.instant('AUDITS.OLD_REVISION'));
              }
            }
            this.entity = this.entityApiFactoryService.from(entityType, entity as unknown as EntityRepository);
            this.entity.values = (this.entityApiFactoryService.create(entityType) as any).parseAttributeDetail(entity.attributes);
          });
      });
  }
}
