import { ChangeDetectorRef, Component } from '@angular/core';
import { ExplodedTreeNode } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { FormItemConfig } from 'src/app/shared/services/form-utils.service';
import { Site, SiteDTO } from 'src/app/sites/site/models/sites.model';
import { SiteOutMapper } from 'src/app/sites/site/services/dtos/site-out-mapper.class';
import { SiteSubsitesValidationConfigService } from 'src/app/sites/site/services/site-subsites-validation-config.service';
import { AuditComponentInterface } from '../../models/audits.model';

@Component({
  selector: 'ff-audit-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.scss'],
})
export class AuditSitesComponent implements AuditComponentInterface<Site> {
  identifier!: number;
  data!: Site;
  tree!: ExplodedTreeNode;
  configFormConfig: { [key: string]: FormItemConfig } = {};
  type = '';

  constructor(private change: ChangeDetectorRef, private siteSubsitesValidationConfigService: SiteSubsitesValidationConfigService) {}

  ngAfterLoadData() {
    this.configFormConfig = this.siteSubsitesValidationConfigService.getSiteConfigFormConfig();

    this.data = new SiteOutMapper(this.data as unknown as SiteDTO).data;

    this.change.markForCheck();
  }
}
