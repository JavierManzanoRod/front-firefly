import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { SiteConfigModule } from 'src/app/sites/site/components/site-config/site-config.module';
import { SiteModule } from 'src/app/sites/site/site.module';
import { AuditsSitesRoutingModule } from './sites-routing.module';
import { AuditSitesComponent } from './sites.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    AuditsSitesRoutingModule,
    TranslateModule,
    SiteModule,
    SiteConfigModule,
  ],
  declarations: [AuditSitesComponent],
  providers: [DatePipe],
  exports: [],
})
export class AuditsSitesModule {}
