import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditSitesComponent } from './sites.component';

const routes: Routes = [
  {
    path: '',
    component: AuditSitesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditsSitesRoutingModule {}
