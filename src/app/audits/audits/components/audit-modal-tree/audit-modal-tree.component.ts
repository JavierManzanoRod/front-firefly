import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { OfferExplodedTreeComponent } from 'src/app/modules/exploded-tree/components/adapters/offer-exploded-tree/offer-exploded-tree.component';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';

@Component({
  selector: 'ff-audit-modal-tree',
  templateUrl: './audit-modal-tree.component.html',
  styleUrls: ['./audit-modal-tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuditModalTreeComponent implements OnInit {
  @ViewChild(OfferExplodedTreeComponent) tree!: OfferExplodedTreeComponent;

  control!: FormControl;

  constructor(public modalRef: BsModalRef) {}

  ngOnInit(): void {}

  badgeFilter(node: ExplodedTreeNodeComponent) {
    if (this.tree.explodedTree.active && node.data === this.tree.explodedTree.active.data) {
      return 'icon-icon-check';
    }

    return;
  }

  select() {
    const target = this.tree.explodedTree.active;
    this.control.setValue(target?.generateVisualPath());
    this.modalRef.hide();
  }
}
