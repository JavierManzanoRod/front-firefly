import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DateTimeService } from '@core/base/date-time.service';
import { AuditOperator, AuditType } from '@core/models/audit.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { dateRangeValidator } from 'src/app/shared/validations/date-range.validator';
import { AuditFilterData } from '../../models/audits.model';
import { AuditsService } from '../../services/audits.service';
import { AuditModalTreeComponent } from '../audit-modal-tree/audit-modal-tree.component';

@Component({
  selector: 'ff-audit-filters',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './audit-filters.component.html',
  styleUrls: ['./audit-filters.component.scss'],
})
export class AuditFiltersComponent implements OnInit {
  @Input() hideAdministration = false;
  @Output() filtering = new EventEmitter<AuditFilterData | null>();
  @Output() clearEvent = new EventEmitter<void>();

  canClear = false;
  form: FormGroup;
  formErrors: any = {};
  administration = null;

  collapsed = false;

  actionOptions = Object.values(AuditType);
  siteOptions = ['eci.es'];

  administrationTree: { [key: string]: Array<keyof Partial<AuditsService>> } = {
    business_rules: [
      'category_rules',
      'badge',
      'experts',
      'loyalty',
      'eci_fluor_gases',
      'content_group',
      'size_guide_url',
      'price_inheritance',
      'limit_sales',
      'exclude_search',
      'related_services',
    ],
    catalog: ['attributes_translations', 'facet_links', 'spec_templates'],
    firefly_administration: ['admin_group_types', 'admin_groups', 'entity_types', 'entities', 'sites', 'subsites'],
  };
  groupOptions = Object.keys(this.administrationTree);
  administrationOptions: string[] = [];

  constructor(private dateTimeService: DateTimeService, private change: ChangeDetectorRef, private modal: BsModalService) {
    const headerConfigForm = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      group: {
        value: null,
        validators: {
          required: true,
        },
      },
      administration: {
        value: null,
        validators: {
          required: true,
        },
      },
      user: {
        value: '',
        validators: {
          minlength: 3,
        },
      },
      name: {
        value: '',
        validators: {
          minlength: 3,
        },
      },
      id: {
        value: '',
        validators: {
          minlength: 3,
        },
      },
      type: {
        value: '',
        validators: {
          minlength: 3,
        },
      },
      site: {
        value: null,
      },
      from: {
        value: null,
        validators: {
          dateTimeValidator: true,
        },
      },
      to: {
        value: null,
        validators: {
          dateTimeValidator: true,
        },
      },
      action: {
        value: AuditType.ALL,
      },
    });
    this.form = headerConfigForm.form;
    this.formErrors = headerConfigForm.errors;
    this.form.setValidators([dateRangeValidator('from', 'to', false) as any]);

    this.canClear = Object.values(this.form.value).reduce<boolean>((a, b) => a || !!b, false);

    this.form.controls.group.valueChanges.subscribe((group) => {
      if (group && this.administration !== group) {
        this.administrationOptions = this.administrationTree[group];
        this.form.controls.administration.setValue(this.administrationOptions[0]);
      }
      this.administration = group;
    });
  }

  get canModalTree() {
    return ['facet_links', 'attributes_translations'].includes(this.form.get('administration')?.value);
  }

  ngOnInit() {
    if (this.hideAdministration) {
      this.form.removeControl('group');
      this.form.removeControl('administration');
    }
  }

  getDataForSave(): AuditFilterData {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { from, to, name, id, type, group, ...values } = this.form.value;

    const filter: AuditFilterData = values;

    filter.changes_since = from && this.dateTimeService.convertUTCDateToSend(from);
    filter.changes_to = to && this.dateTimeService.convertUTCDateToSend(to);
    filter.custom_filters = [].concat(
      name
        ? [
            {
              field: 'name',
              operator: AuditOperator.LIKE,
              field_value: name,
            },
          ]
        : ([] as any)
    );

    return filter;
  }

  refresh() {
    this.filtering.emit(null);
    this.canClear = false;
    this.form.enable({ emitEvent: false });
  }

  clear() {
    this.collapsed = false;

    this.form.reset({ emitEvent: false });
    this.form.get('action')?.setValue(AuditType.ALL, { emitEvent: false });
  }

  handleFilter() {
    this.collapsed = false;
    if (this.canClear) {
      this.refresh();
      this.clearEvent.emit();
    } else {
      this.filter();
    }
  }

  filter() {
    FormUtilsService.markFormGroupTouched(this.form);
    this.form.updateValueAndValidity();

    if (this.form.valid) {
      this.filtering.emit(this.getDataForSave());
      this.canClear = true;

      this.form.disable();
    }

    if (this.change) {
      this.change.markForCheck();
    }
  }

  formatLabel(value: string) {
    return value.replace(/-/g, '_');
  }

  showModalTree() {
    this.modal.show(AuditModalTreeComponent, {
      initialState: {
        control: this.form.get('name') as FormControl,
      },
    });
  }
}
