import { AuditQuery } from '@core/models/audit.model';
import { GenericApiResponse } from '@model/base-api.model';
import { AuditsService } from '../services/audits.service';

/**
 * Note:
 *
 * @see es.json
 *
 * @see MenuLeftService
 * Los nombres de las auditorias deben ser igual que los de APP_MENU.
 * De esta forma tenemos unificado los nombres de los menus con las auditorias.
 * Por ejemplo entity types sería en el APP_MENU.ENTITY_TYPES pues debemos poner entity_type.
 *
 * @see AuditsService
 * En el AuditService se debe definir el nombre del servicio en el constructor de la misma forma
 * constructor(private entity_type: EntityTypeService, ...)
 */
export type AuditAdministrationTypes = keyof Partial<AuditsService>;
export type AuditFilterData = {
  administration: AuditAdministrationTypes;
} & AuditQuery;

export type AuditFilterDataKeys = keyof AuditFilterData;

// TODO: Remove next models

export enum AuditActionTypes {
  CREATED = 'created',
  UPDATED = 'updated',
  DELETED = 'deleted',
}

export type AuditItem = {
  id?: string;
  user: string;
  name: string;
  date: string | Date;
  site: string;
  comments: string;
  action: AuditActionTypes;
};

export type AuditResponse = GenericApiResponse<AuditItem>;

export type AuditAdminGroup = {
  id?: string;
  user: string;
  name: string;
  date: string | Date;
  site: string;
  comments: string;
  action: AuditActionTypes;
};

export type AuditAdminGroupResponse = GenericApiResponse<AuditAdminGroup>;

export interface AuditComponentInterface<T = any> {
  data: T;
  revision?: number;
  identifier: number;
  type?: string;
  ngAfterLoadData(): void;
}
