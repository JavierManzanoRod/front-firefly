import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AuditsRoutingModule } from './audits-routing.module';

@NgModule({
  imports: [CommonModule, FormsModule, AuditsRoutingModule],
  declarations: [],
  providers: [],
  exports: [],
})
export class AuditsModule {}
