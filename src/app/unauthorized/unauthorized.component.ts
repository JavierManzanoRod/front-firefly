import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '@model/user';
import { UserService } from '../shared/services/user.service';
import { MenuTopComponent } from './../components/menu-top/menu-top.component';

@Component({
  selector: 'ff-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.scss'],
})
export class UnAuthorizedComponent implements OnInit {
  @ViewChild(MenuTopComponent) menuTop!: MenuTopComponent;
  isMenuOpen = true;
  user: User = this.userService.getUser();

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.userSubject.subscribe((user) => {
      this.user = user;
    });
  }

  logOut() {
    this.menuTop.logOut();
  }
}
