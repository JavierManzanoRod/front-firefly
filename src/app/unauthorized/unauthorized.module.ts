import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { MenuTopModule } from '../components/menu-top/menu-top.module';
import { UnAuthorizedRoutingModule } from './unauthorized-routing.module';
import { UnAuthorizedComponent } from './unauthorized.component';

@NgModule({
  declarations: [UnAuthorizedComponent],
  imports: [CommonModule, MenuTopModule, UnAuthorizedRoutingModule, TranslateModule],
})
export class UnauthorizedModule {}
