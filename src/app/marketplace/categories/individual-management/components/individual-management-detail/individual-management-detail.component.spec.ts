import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { IndividualManagementDetailComponent } from './individual-management-detail.component';

// activar cuando funcione el back
xdescribe('IndividualManagementDetailComponent', () => {
  let fixture: ComponentFixture<IndividualManagementDetailComponent>;
  let component: IndividualManagementDetailComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IndividualManagementDetailComponent],
      providers: [
        {
          provide: Router,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: ActivatedRoute,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: ToastService,
          useFactory: () => ({
            error: () => {},
          }),
        },
        {
          provide: BsModalService,
          useFactory: () => ({
            error: () => {},
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndividualManagementDetailComponent);
    fixture.detectChanges();
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
