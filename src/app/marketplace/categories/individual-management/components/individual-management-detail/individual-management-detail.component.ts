import { ChangeDetectorRef, Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsComponent } from '@core/base/utils-component';
import { MayHaveLabel } from '@model/base-api.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { UiModalSelectService } from '../../../../../modules/ui-modal-select/ui-modal-select.service';
import { UiTreeClickEvent, UITreeListItem } from '../../../../../modules/ui-tree-list/models/ui-tree-list.model';
import { FormsErrorsConfig, FormUtilsService } from '../../../../../shared/services/form-utils.service';

@Component({
  selector: 'ff-individual-management-detail',
  styleUrls: ['individual-management-detail.component.scss'],
  templateUrl: 'individual-management-detail.component.html',
})
export class IndividualManagementDetailComponent {
  loading = false;

  form: FormGroup;
  formErrors: FormsErrorsConfig;

  treeItemsProduct!: UITreeListItem[];
  treeItemsDestination!: UITreeListItem[];

  selectedProduct!: { selectedProducts?: any[]; parentProduct: any };
  selectedDestination: any;

  utils: UtilsComponent = new UtilsComponent();

  constructor(
    private ref: ChangeDetectorRef,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private toastService: ToastService,
    private modalSelectService: UiModalSelectService,
    private bsModalService: BsModalService
  ) {
    const form = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      category: {
        validators: {
          required: true,
        },
      },
      site: {
        validators: {
          required: true,
        },
      },
    });
    this.form = form.form;
    this.formErrors = form.errors;
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.category && params.site) {
        this.form.setValue(params);
        // TODO Realizar busqueda en el backend para coger los items del arbol que coincidan con los filtros
        // Simulamos que el backend no ha devuelto datos
        this.treeItemsProduct = this.getBackenDataSimulated();
        this.treeItemsDestination = this.getBackenDataSimulated(true);
      }
    });
  }

  // TODO Borrar cuando este el backend
  getBackenDataSimulated(isDestination?: boolean) {
    const randomNumber = Math.random() >= 0.5;
    const result = [];
    let hasChild;
    let item: UITreeListItem;
    for (let i = 0, l = 20; i < l; i++) {
      hasChild = isDestination ? true : randomNumber;
      item = {
        value: {
          id: i,
          name: `Deportes ${i}`,
          hasChild,
        },
        icon: hasChild ? 'icon-page' : 'icon-box',
      };

      if (hasChild) {
        item.loadChildrens = (event: UITreeListItem) => this.getBackenDataSimulatedChildrens(event, isDestination);
      }
      result.push(item);
    }
    return result;
  }

  // TODO Borrar cuando este el backend
  getBackenDataSimulatedChildrens(item: UITreeListItem, isDestination?: boolean) {
    return of(this.getBackenDataSimulated(isDestination)).pipe(delay(500));
  }

  search() {
    FormUtilsService.markFormGroupTouched(this.form);
    if (this.form.valid) {
      this.router.navigate(['/categories/individual-management'], {
        queryParams: this.form.value,
      });
    }
  }

  onSelectedProduct(item: UiTreeClickEvent<MayHaveLabel>) {
    let add = true;
    if (this.selectedProduct && this.selectedProduct.parentProduct && this.selectedProduct.parentProduct.id === item.selectedItem.id) {
      add = false;
    }
    if (add) {
      this.selectedProduct = {
        parentProduct: item.selectedItem,
      };
    }
  }

  onSelectedDestination(item: UiTreeClickEvent<MayHaveLabel>) {
    this.selectedDestination = item.selectedItem;
  }

  send() {
    if (!this.selectedProduct) {
      this.toastService.error('MARKETPLACE.CATEGORIES.INDIVIDUAL_MANAGEMENT.NO_SELECTED_PRODUCT_ERR');
      return;
    }
    if (!this.selectedDestination) {
      this.toastService.error('MARKETPLACE.CATEGORIES.INDIVIDUAL_MANAGEMENT.NO_SELECTED_DESTINATION_ERR');
      return;
    }
    if (this.selectedProduct.parentProduct.id === this.selectedDestination.id) {
      this.toastService.error('MARKETPLACE.CATEGORIES.INDIVIDUAL_MANAGEMENT.EQUAL_ITEMS_ERR');
      return;
    }
    /*
    this.utils.updateOrSaveModal({
      modalService: this.bsModalService
    }, itemToSave, next);
     */
  }

  openProductsModal() {
    setTimeout(() => {
      this.modalSelectService
        .showSelectProducts(
          {
            parentProduct: this.selectedProduct.parentProduct,
            selectedProducts: this.selectedProduct.selectedProducts,
          },
          this.bsModalService
        )
        .subscribe((selectedProducts: any[]) => {
          this.selectedProduct.selectedProducts = selectedProducts;
          this.ref.detectChanges();
        });
    }, 10);
  }
}
