import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<ff-page-header [pageTitle]="'MARKETPLACE.CATEGORIES.INDIVIDUAL_MANAGEMENT.TITLE' | translate"> </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-individual-management-detail></ff-individual-management-detail>
    </ngx-simplebar> `,
})
export class IndividualManagementDetailContainerComponent {}
