import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormErrorModule } from '../../../modules/form-error/form-error.module';
import { ToastModule } from '../../../modules/toast/toast.module';
import { UiModalSelectModule } from '../../../modules/ui-modal-select/ui-modal-select.module';
import { UiTreeListModule } from '../../../modules/ui-tree-list/ui-tree-list.module';
import { IndividualManagementDetailComponent } from './components/individual-management-detail/individual-management-detail.component';
import { IndividualManagementDetailContainerComponent } from './containers/individual-management-detail-container.component';
import { IndividualManagementRoutingModule } from './individual-management-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModalModule,
    ToastModule,
    IndividualManagementRoutingModule,
    FormErrorModule,
    UiTreeListModule,
    UiModalSelectModule,
    SimplebarAngularModule,
    NgSelectModule,
  ],
  declarations: [IndividualManagementDetailContainerComponent, IndividualManagementDetailComponent],
  providers: [],
  exports: [],
})
export class IndividualManagementModule {}
