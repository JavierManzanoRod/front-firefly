import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndividualManagementDetailContainerComponent } from './containers/individual-management-detail-container.component';

const routes: Routes = [
  {
    path: '',
    component: IndividualManagementDetailContainerComponent,
    data: {
      header_title: 'MARKETPLACE.CATEGORIES.HEADER_MENU',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndividualManagementRoutingModule {}
