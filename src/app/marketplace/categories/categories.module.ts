import { NgModule } from '@angular/core';
import { CategoriesRoutingModule } from './categories-routing.module';

@NgModule({
  imports: [CategoriesRoutingModule],
  declarations: [],
  providers: [],
  exports: [],
})
export class CategoriesModule {}
