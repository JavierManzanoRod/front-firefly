import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { Seller, SellerResponse } from '../models/seller.model';
import { SellerService } from '../services/seller.service';
import { SellerListContainerComponent } from './seller-list-container.component';

function getList(): SellerResponse {
  const seller: Seller = {
    id: '0734ff94-1007-4d79-8ce4-2a4f0ed5505e',
    name: 'Adolfo_Domínguez_Pre',
    external_ids: [
      {
        platform: 'MIRAKL',
        id: '2003',
      },
      {
        platform: 'SAP',
        id: '5555558',
      },
    ],
    sites: ['eciStore'],
  };

  return {
    content: [seller],
    page: {
      page_number: 1,
      page_size: 20,
      total_elements: 1,
      total_pages: 4,
    },
  };
}
// end global mocks

describe('SellerListContainerComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SellerListContainerComponent],
        imports: [RouterTestingModule, TranslateModule.forRoot()],
        providers: [
          {
            provide: CrudOperationsService,
            useFactory: () => ({}),
          },
          {
            provide: SellerService,
            useFactory: () => ({
              list: () => of({}),
            }),
          },
        ],
      }).compileComponents();
    })
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(SellerListContainerComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should call the proper api.list once after ngOnInit', (done) => {
    const fixture = TestBed.createComponent(SellerListContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const service = fixture.debugElement.injector.get(SellerService);
    const spy = spyOn(service, 'list').and.returnValue(of(getList()));
    component.ngOnInit();
    component.list$.subscribe(() => {
      expect(component.page).toEqual(getList().page);
      done();
    });
    expect(spy).toHaveBeenCalled();
  });
});
