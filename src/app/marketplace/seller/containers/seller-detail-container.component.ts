import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsComponent } from '@core/base/utils-component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { Seller } from '../models/seller.model';
import { SellerService } from '../services/seller.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<ff-page-header
      [pageTitle]="'SELLER.HEADER_DETAIL' | translate"
      breadcrumbLink="/seller"
      [breadcrumbTitle]="'COMMON.RETURN_TO_LIST' | translate"
    >
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-seller-detail (formSubmit)="handleSubmitForm($event)" [detail]="detail$ | async"> </ff-seller-detail>
    </ngx-simplebar> `,
})
export class SellerDetailContainerComponent implements OnInit {
  detail$!: Observable<Seller>;
  id: string;
  loading = true;

  utils: UtilsComponent = new UtilsComponent();

  constructor(
    protected route: ActivatedRoute,
    public apiService: SellerService,
    protected router: Router,
    public modalService: BsModalService,
    public toast: ToastService
  ) {
    this.id = this.route.snapshot.paramMap.get('id') || '';
  }

  ngOnInit() {
    let detail$: Observable<Seller>;
    if (this.id) {
      detail$ = this.apiService.detail(this.id);
    } else {
      detail$ = of({} as Seller).pipe(delay(250));
    }
    this.detail$ = detail$;
  }

  handleSubmitForm(data: [Seller, Partial<Seller>]) {
    const next = () => {
      this.router.navigate(['/seller']);
    };

    const dataToSend = {
      ...data[0],
      ...data[1],
    };
    this.utils.updateOrSaveModal(this, dataToSend, next);
  }
}
