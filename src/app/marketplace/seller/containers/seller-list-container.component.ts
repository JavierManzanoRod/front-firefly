import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { Seller } from '../models/seller.model';
import { SellerService } from '../services/seller.service';
import { ConfigMultiple } from './../../../modules/search-multiple/models/config.models';

@Component({
  selector: 'ff-exclude-search-list-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <ff-page-header [pageTitle]="'SELLER.HEADER_LIST' | translate"></ff-page-header>
    <ngx-simplebar class="page-container" *ngIf="!hide">
      <div class="page-scroll-wrapper">
        <div class="page-container-padding">
          <ff-generic-list
            [arrayKeys]="arrayKeys"
            [list]="list$ | async"
            [loading]="loading"
            [page]="page"
            [valueSearch]="filter"
            [currentData]="currentData"
            (delete)="delete($event)"
            (pagination)="handlePagination($event)"
            [type]="type"
            [configMultiple]="configMultiple"
            [canDelete]="canDelete"
            [prefix]="'SELLER'"
            [route]="'/seller'"
            (search)="search($event)"
          >
          </ff-generic-list>
        </div>
      </div>
    </ngx-simplebar> `,
  styleUrls: ['./seller-list-container.components.scss'],
})
export class SellerListContainerComponent extends BaseListContainerComponent<Seller> implements OnInit {
  arrayKeys: GenericListConfig[] = [
    {
      key: 'codeView',
      headerName: 'SELLER.CODE',
    },
    {
      key: 'name',
    },
    {
      key: 'externalIdView',
      headerName: 'SELLER.EXTERNAL_ID',
    },
    {
      key: 'sitesView',
      headerName: 'SELLER.SITES',
    },
    {
      key: 'statusView',
      headerName: 'SELLER.STATUS',
    },
  ];

  configMultiple: ConfigMultiple[] = [
    {
      key: 'name',
      type: 'TEXT',
      class: 'col-3',
      placeHolder: 'SELLER.PLACEHOLDER_NAME',
      label: 'SELLER.LABEL_NAME',
    },
    {
      key: 'code',
      type: 'TEXT',
      class: 'col-3',
      placeHolder: 'SELLER.PLACEHOLDER_CODE',
      label: 'SELLER.LABEL_CODE',
    },
    {
      key: 'miraklId',
      type: 'TEXT',
      class: 'col-3',
      placeHolder: 'SELLER.PLACEHOLDER_EXTERNAL_ID',
      label: 'SELLER.LABEL_MIRAKL',
    },
    {
      key: 'status',
      type: 'SELECT',
      label: 'SELLER.LABEL_STATUS',
      class: 'col-2',
      selectOptions: [
        {
          label: 'SELLER.STATUS_ACTIVATED',
          value: 'Active',
        },
        {
          label: 'SELLER.STATUS_CREATED',
          value: 'Created',
        },
        {
          label: 'SELLER.STATUS_SUSPENDED',
          value: 'Suspended',
        },
      ],
    },
  ];

  canDelete = false;
  hide = false;
  type = TypeSearch.multiple;

  constructor(public apiService: SellerService, public utils: CrudOperationsService) {
    super(utils, apiService);
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({});
  }
}
