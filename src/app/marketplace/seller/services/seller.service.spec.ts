import { TranslateService } from '@ngx-translate/core';
import { Seller } from '../models/seller.model';
import { SellerService } from './seller.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };
let myService: SellerService;

const fakeTranslator = { instant: () => 'fake value' } as unknown as TranslateService;

const version = 'v1/';

describe('SellerService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`SellerService points to products/marketplace-sellers/${version}`, () => {
    myService = new SellerService(null as any, fakeTranslator, version);
    expect(myService.endPoint).toEqual(`products/marketplace-sellers/${version}`);
  });

  it('parse add view fields', () => {
    myService = new SellerService(null as any, fakeTranslator, version);

    const sellerToParse: Seller = {
      id: '0734ff94-1007-4d79-8ce4-2a4f0ed5505e',
      name: 'Adolfo_Domínguez_Pre',
      external_ids: [
        {
          platform: 'MIRAKL',
          id: '2003',
        },
        {
          platform: 'SAP',
          id: '5555558',
        },
      ],
      sites: ['eciStore'],
    };

    const parsed = myService.parse(sellerToParse);
    // expect(parsed.codeView).toBe(sellerToParse.external_ids[0].id);
    expect(parsed.sitesView).toBe('eciStore');
    // expect(parsed.statusView).toBe('fake value');
  });
});
