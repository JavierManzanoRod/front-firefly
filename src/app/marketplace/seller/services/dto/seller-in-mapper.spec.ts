import { Seller } from '../../models/seller.model';
import { SellerInMapper } from './seller-in-mapper-class';

describe('seller mapper in', () => {
  it('parse an incoming seller response as expected', () => {
    const remoteItem: Seller = {
      id: '68d49087-9c28-44d9-85e3-ce4ae6bf8c4e',
      name: 'ADOLFO DOMINGUEZ',
      external_ids: [
        {
          platform: 'MIRAKL',
          id: '2049',
        },
        {
          platform: 'SAP',
          id: '2000014',
        },
      ],
      sites: ['eciStore'],
      status: 'Created',
      product_types_allowed: [
        {
          id: '1',
          name: {
            es_ES: 'Primero',
          },
        },
        {
          id: '1',
          name: {
            es_ES: 'Segundo',
          },
        },
      ],
    } as unknown as Seller;

    const mapper = new SellerInMapper(remoteItem);
    const seller = mapper.data;
    expect(seller.identifier).toEqual('68d49087-9c28-44d9-85e3-ce4ae6bf8c4e');
    expect(seller.external_ids[1].identifier).toEqual('2000014');
    expect(seller.sites).toEqual(['eciStore']);
    expect(seller.status).toEqual('Created');
    expect(seller.name).toEqual('ADOLFO DOMINGUEZ');
    expect(seller.product_types_allowed).toEqual([
      {
        identifier: '1',
        name: {
          es_ES: 'Primero',
        },
      },
      {
        identifier: '1',
        name: {
          es_ES: 'Segundo',
        },
      },
    ]);
  });

  it('parse an incoming seller response with some fields null or undefined does not break anything', () => {
    const remoteItem: Seller = {
      id: undefined,
      name: '',
      status: null,
      sites: [''],
    } as unknown as Seller;

    const mapper = new SellerInMapper(remoteItem);
    const group = mapper.data;
    expect(group.identifier).toEqual(undefined);
    expect(group.name).toEqual('');
    expect(group.status).toEqual(null);
    expect(group.sites).toEqual(['']);
  });
});
