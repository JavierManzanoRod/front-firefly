import { ExternalIds, ExternalIdsDTO, ProductTypesAllowed, ProductTypesAllowedDTO, Seller, SellerDTO } from '../../models/seller.model';

export class SellerOutMapper {
  data = {} as Seller;

  constructor(remoteData: SellerDTO) {
    this.data = {
      id: remoteData.identifier,
      name: remoteData.name,
      external_ids: this.transformExternalIds(remoteData.external_ids),
      codeView: remoteData.codeView,
      externalIdView: remoteData.externalIdView,
      sites: remoteData.sites,
      sitesView: remoteData.sitesView,
      exploitation_type: remoteData.exploitation_type,
      product_types_allowed: this.transformProductTypeAllowed(remoteData.product_types_allowed),
      product_types_allowedView: remoteData.product_types_allowedView,
      status: remoteData.status,
      show_offer: remoteData.is_show_offer,
      statusView: remoteData.statusView,
    } as unknown as Seller;
  }

  private transformExternalIds(item: ExternalIdsDTO[] | undefined): ExternalIds[] | undefined {
    if (item) {
      return item.map(({ identifier, ...data }) => {
        return { ...data, id: identifier };
      });
    }
  }

  private transformProductTypeAllowed(item: ProductTypesAllowedDTO[] | undefined): ProductTypesAllowed[] | undefined {
    if (item) {
      return item.map(({ identifier, ...data }) => {
        return { ...data, id: identifier };
      });
    }
  }
}
