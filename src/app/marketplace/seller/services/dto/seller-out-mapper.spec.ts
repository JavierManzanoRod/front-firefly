import { SellerDTO } from '../../models/seller.model';
import { SellerOutMapper } from './seller-out-mapper-class';

describe('seller mapper out', () => {
  it('parse an outgoing seller request as expected', () => {
    const remoteItem: SellerDTO = {
      identifier: '68d49087-9c28-44d9-85e3-ce4ae6bf8c4e',
      name: 'ADOLFO DOMINGUEZ',
      external_ids: [
        {
          platform: 'MIRAKL',
          identifier: '2049',
        },
        {
          platform: 'SAP',
          identifier: '2000014',
        },
      ],
      sites: ['eciStore'],
      status: 'Created',
      product_types_allowed: [
        {
          identifier: '1',
          name: {
            es_ES: 'Primero',
          },
        },
        {
          identifier: '1',
          name: {
            es_ES: 'Segundo',
          },
        },
      ],
    };

    const mapper = new SellerOutMapper(remoteItem);
    const seller = mapper.data;
    expect(seller.id).toEqual('68d49087-9c28-44d9-85e3-ce4ae6bf8c4e');
    expect(seller.external_ids[1].id).toEqual('2000014');
    expect(seller.sites).toEqual(['eciStore']);
    expect(seller.status).toEqual('Created');
    expect(seller.name).toEqual('ADOLFO DOMINGUEZ');
    expect(seller.product_types_allowed).toEqual([
      {
        id: '1',
        name: {
          es_ES: 'Primero',
        },
      },
      {
        id: '1',
        name: {
          es_ES: 'Segundo',
        },
      },
    ]);
  });

  it('parse an outgoing seller request with some fields null or undefined does not break anything', () => {
    const remoteItem = {
      identifier: undefined,
      name: '',
      status: null,
      sites: [''],
    } as unknown as SellerDTO;

    const mapper = new SellerOutMapper(remoteItem);
    const group = mapper.data;
    expect(group.id).toEqual(undefined);
    expect(group.name).toEqual('');
    expect(group.status).toEqual(null);
    expect(group.sites).toEqual(['']);
  });
});
