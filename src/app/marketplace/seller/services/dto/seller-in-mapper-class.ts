import { ExternalIds, ExternalIdsDTO, ProductTypesAllowed, ProductTypesAllowedDTO, Seller, SellerDTO } from '../../models/seller.model';

export class SellerInMapper {
  data = {} as SellerDTO;

  constructor(remoteData: Seller) {
    this.data = {
      identifier: remoteData.id,
      name: remoteData.name,
      external_ids: this.transformExternalIds(remoteData.external_ids),
      codeView: remoteData.codeView,
      externalIdView: remoteData.externalIdView,
      sites: remoteData.sites,
      sitesView: remoteData.sitesView,
      exploitation_type: remoteData.exploitation_type,
      product_types_allowed: this.transformProductTypeAllowed(remoteData.product_types_allowed),
      product_types_allowedView: remoteData.product_types_allowedView,
      status: remoteData.status,
      is_show_offer: remoteData.show_offer,
      statusView: remoteData.statusView,
    } as unknown as SellerDTO;
  }

  private transformExternalIds(item: ExternalIds[] | undefined): ExternalIdsDTO[] | undefined {
    if (item) {
      return item.map(({ id, ...data }) => {
        return { ...data, identifier: id };
      });
    }
  }

  private transformProductTypeAllowed(item: ProductTypesAllowed[] | undefined): ProductTypesAllowedDTO[] | undefined {
    if (item) {
      return item.map(({ id, ...data }) => {
        return { ...data, identifier: id };
      });
    }
  }
}
