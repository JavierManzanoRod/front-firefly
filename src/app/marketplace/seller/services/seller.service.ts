import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BusinessApiService } from '@core/base/api-business.service';
import { IApiService2, UpdateResponse } from '@core/base/api.service';
import { environment } from '@env/environment';
import { BusinessApiResponse } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_MARKETPLACE_SELLERS } from 'src/app/configuration/tokens/api-versions.token';
import { Seller, SellerDTO, SellerEditableFields, SellerRequest, SellerResponse } from '../models/seller.model';
import { SellerInMapper } from './dto/seller-in-mapper-class';
import { SellerOutMapper } from './dto/seller-out-mapper-class';

const urlBaseMarketPlace = `${environment.API_URL_BACKOFFICE}`;
@Injectable({
  providedIn: 'root',
})
export class SellerService extends BusinessApiService<Seller> implements IApiService2<Seller> {
  endPoint = `products/marketplace-sellers/${this.version}`;

  constructor(
    protected http: HttpClient,
    private translateService: TranslateService,
    @Inject(FF_API_PATH_VERSION_MARKETPLACE_SELLERS) private version: string
  ) {
    super();
  }

  getParams(filter: SellerRequest): HttpParams {
    let params: HttpParams = new HttpParams();

    if (filter) {
      params = filter.page_number ? params.set('page_number', filter.page_number.toString()) : params;
      params = filter.page_size ? params.set('page_size', filter.page_size.toString()) : params;
      params = filter.size ? params.set('size', filter.size.toString()) : params;
      params = filter.page ? params.set('page', filter.page.toString()) : params;
      params = filter.name ? params.set('name', filter.name.toString()) : params;
      params = filter.code ? params.set('code', filter.code.toString()) : params;
      params = filter.exploitationType ? params.set('exploitationType', filter.exploitationType.toString()) : params;
      params = filter.miraklId ? params.set('mirakl_id', filter.miraklId.toString()) : params;
      params = filter.site ? params.set('site', filter.site.toString()) : params;
      params = filter.status ? params.set('status', filter.status.toString()) : params;
    }

    return params;
  }

  list(filter: SellerRequest): Observable<SellerResponse> {
    const params = super.getFilter(filter);
    const urlBase = urlBaseMarketPlace + '/' + this.endPoint;
    return this.http.get<BusinessApiResponse<Seller>>(urlBase, { params: this.getParams(params) }).pipe(
      map((data) => this.toGeneric(data)),
      map(({ content, page }) => ({
        content: content.map((seller) => {
          const data = this.parse(new SellerOutMapper(seller as unknown as SellerDTO).data);
          return data;
        }),
        page,
      }))
    );
  }

  detail(id: string): Observable<Seller> {
    return super.detail(id, true).pipe(
      map((site) => {
        const sellerDTO = this.parse(new SellerOutMapper(site as unknown as SellerDTO).data);
        return sellerDTO;
      })
    );
  }

  update(seller: Seller): Observable<UpdateResponse<Seller>> {
    const urlBase = urlBaseMarketPlace + '/' + this.endPoint;
    const url = `${urlBase}${seller.id}`;
    const data = this.getSellerEditableFields(seller);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .request('patch', url, {
        body: new SellerInMapper(data as Seller).data,
        headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            status: res.status,
            data: new SellerOutMapper(res.body).data,
          };
        })
      );
  }

  parse(seller: Seller): Seller {
    if (seller.external_ids) {
      const val = seller.external_ids.find((e) => e.platform === 'MIRAKL');
      seller.externalIdView = (val && val.id) || '';
      const val2 = seller.external_ids.find((e) => e.platform === 'SAP');
      seller.codeView = (val2 && val2.id) || '';
    } else {
      seller.externalIdView = '';
      seller.codeView = '';
    }

    if (seller.product_types_allowed) {
      seller.product_types_allowedView = seller.product_types_allowed.join(', ');
    }
    if (seller.sites) {
      seller.sitesView = seller.sites.join(', ');
    }

    if (seller.status) {
      seller.statusView = this.translateService.instant('SELLER.STATUS_' + seller.status.toUpperCase());
    } else {
      seller.statusView = '';
    }
    return seller;
  }

  getSellerEditableFields(seller: Seller): SellerEditableFields {
    const sellerEditableFields = new SellerEditableFields();

    sellerEditableFields.exploitation_type = seller.exploitation_type;

    sellerEditableFields.product_types_allowed = seller.product_types_allowed;
    sellerEditableFields.show_offer = seller.show_offer;

    return sellerEditableFields;
  }
}
function clone<T>(x: T): T {
  return JSON.parse(JSON.stringify(x)) as T;
}
