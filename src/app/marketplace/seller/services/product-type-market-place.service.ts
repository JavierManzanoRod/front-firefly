import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, IApiService2 } from '@core/base/api.service';
import { environment } from '@env/environment';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_PRODUCT_TYPES } from 'src/app/configuration/tokens/api-versions.token';
import { ProductTypeMarketPlace } from '../models/product-type-market-place.model';

const urlBaseMarketPlace = `${environment.API_URL_BACKOFFICE}`;

@Injectable({
  providedIn: 'root',
})
export class ProductTypeMarketPlaceService extends ApiService<ProductTypeMarketPlace> implements IApiService2<ProductTypeMarketPlace> {
  endPoint = `products/marketplace-product-types/${this.version}`;
  url = urlBaseMarketPlace + '/' + this.endPoint;

  constructor(
    protected http: HttpClient,
    private translation: TranslateService,
    @Inject(FF_API_PATH_VERSION_PRODUCT_TYPES) private version: string
  ) {
    super();
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<ProductTypeMarketPlace>> {
    return this.http.get<GenericApiResponse<ProductTypeMarketPlace>>(this.url, { params: this.getParams(filter) }).pipe(
      map((v) => {
        v.content.forEach((product) => {
          product.name = product.name[this.getLang()];
        });
        return v;
      })
    );
  }

  getLang(): string {
    return 'es_ES';
  }

  detail(id: string): Observable<ProductTypeMarketPlace> {
    const urlDetail = `${this.url}/${id}`;
    return this.http.get<ProductTypeMarketPlace>(urlDetail).pipe(
      map((v) => {
        v.name = v.name[this.getLang()];
        return v;
      })
    );
  }
}
