export interface ProductTypeMarketPlace {
  name: any;
  market_place: boolean;
  parent_product_key: string;
  product_atgkey: string;
}
