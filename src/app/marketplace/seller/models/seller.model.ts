import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';

export interface Seller {
  id?: string;
  name: string;
  external_ids: ExternalIds[];
  codeView?: string;
  externalIdView?: string;
  sites: string[];
  sitesView?: string;
  exploitation_type?: string;
  product_types_allowed?: ProductTypesAllowed[];
  product_types_allowedView?: string;
  status?: string;
  show_offer?: boolean;
  statusView?: string;
}

export interface ProductTypesAllowed {
  id: string;
  name: any;
}
export interface ProductTypesAllowedDTO {
  identifier: string;
  name: any;
}
export interface ExternalIds {
  platform: 'MIRAKL' | 'SAP';
  id: string;
}

export interface ExternalIdsDTO {
  platform: 'MIRAKL' | 'SAP';
  identifier: string;
}

export interface SellerDTO {
  identifier?: string;
  name: string;
  external_ids: ExternalIdsDTO[];
  codeView?: string;
  externalIdView?: string;
  sites: string[];
  sitesView?: string;
  exploitation_type?: string;
  product_types_allowed?: ProductTypesAllowedDTO[];
  product_types_allowedView?: string;
  status?: string;
  is_show_offer?: boolean;
  statusView?: string;
}

export type SellerResponse = GenericApiResponse<Seller>;
export type SellerResponseDTO = GenericApiResponse<SellerDTO>;

interface SellerRequest1 extends GenericApiRequest {
  code: string;
  miraklId: string;
  site: string;
  exploitationType: string;
  status: string;
}

export type SellerRequest = Partial<SellerRequest1>;

export class SellerEditableFields {
  exploitation_type?: string;
  show_offer?: boolean;
  product_types_allowed?: any[];
}
