import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SellerDetailContainerComponent } from './containers/seller-detail-container.component';
import { SellerListContainerComponent } from './containers/seller-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: SellerListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.MKP_ADMINISTRATOR',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
    children: [
      {
        path: ':id',
        component: SellerDetailContainerComponent,
        data: {
          header_title: 'SELLER.HEADER_MENU',
          breadcrumb: [
            {
              label: 'SHIPPING_COST.BREADCRUMB_LIST_TITLE',
              url: '/seller',
            },
            {
              label: 'SHIPPING_COST.SHIPPING_COST',
              url: '',
            },
          ],
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SellerRoutingModule {}
