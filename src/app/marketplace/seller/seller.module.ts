import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { Pagination2Module } from 'src/app/modules/pagination-2/pagination-2.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ToastModule } from '../../modules/toast/toast.module';
import { SellerDetailComponent } from './components/seller-detail/seller-detail.component';
import { SellerListSearchComponent } from './components/seller-list-search/seller-list-search.component';
import { SellerListComponent } from './components/seller-list/seller-list.component';
import { SellerDetailContainerComponent } from './containers/seller-detail-container.component';
import { SellerListContainerComponent } from './containers/seller-list-container.component';
import { SellerRoutingModule } from './seller-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Pagination2Module,
    CommonModalModule,
    GenericListComponentModule,
    ToastModule,
    SellerRoutingModule,
    ChipsControlModule,
    CoreModule,
    FormErrorModule,
    CommonModalModule,
    SimplebarAngularModule,
    TranslateModule,
    NgSelectModule,
  ],
  declarations: [
    SellerListContainerComponent,
    SellerListComponent,
    SellerListSearchComponent,
    SellerDetailContainerComponent,
    SellerDetailComponent,
  ],
  providers: [],
  exports: [],
})
export class SellerModule {}
