import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericApiRequest } from '@model/base-api.model';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { Seller } from '../../models/seller.model';
import { ProductTypeMarketPlaceService } from '../../services/product-type-market-place.service';

@Component({
  selector: 'ff-seller-detail',
  styleUrls: ['seller-detail.component.scss'],
  templateUrl: 'seller-detail.component.html',
})
export class SellerDetailComponent implements OnChanges {
  @Input() detail!: Seller;
  @Output() formSubmit: EventEmitter<[Seller, Partial<Seller>]> = new EventEmitter<[Seller, Partial<Seller>]>();

  form: FormGroup;
  sended = false;

  showConditions = false;
  loading = true;
  productsConfig: ChipsControlSelectConfig;

  formErrors = {
    exploitation_type: [
      {
        type: 'required',
        message: 'COMMON_ERRORS.REQUIRED',
      },
    ],
  };

  constructor(
    private fb: FormBuilder,
    protected router: Router,
    private productTypeMarketService: ProductTypeMarketPlaceService,
    private activeRoute: ActivatedRoute
  ) {
    this.form = this.fb.group({
      name: [{ value: null, disabled: true }],
      externalIdView: [{ value: null, disabled: true }],
      codeView: [{ value: null, disabled: true }],
      sitesView: [{ value: null, disabled: true }],
      exploitation_type: [null, [Validators.required]],
      show_offer: null,
      product_types_allowed: [],
      statusView: [{ value: null, disabled: true }],
    });

    this.productsConfig = {
      label: 'SELLER.PRODUCT_TYPES_ALLOWED',
      modalType: ModalChipsTypes.select,
      modalConfig: {
        title: 'SELLER.PRODUCT_TYPES_ALLOWED',
        multiple: true,
        items: [],
        searchFn: (x: GenericApiRequest) =>
          this.productTypeMarketService.list({
            ...x,
            is_market_place: true,
          }),
      },
    };
  }

  selectChange() {
    const type = this.form.value.exploitation_type;
    this.checkTypeDigital(+type);

    if (+type === 2) {
      this.form.controls.show_offer.setValue(true);
      this.form.controls.show_offer.enable();
    }
  }

  checkTypeDigital(type: number) {
    if (type === 12) {
      this.form.controls.show_offer.setValue(false);
      this.form.controls.show_offer.disable();
    }
  }

  ngOnChanges() {
    if (this.detail) {
      this.loading = false;
      this.form.patchValue({
        ...this.detail,
        product_types_allowed: [],
      });

      this.detail.product_types_allowed = this.detail.product_types_allowed?.map((value) => {
        value.name = value.name.es_ES;
        return value;
      });
      this.form.controls.product_types_allowed.setValue(this.detail.product_types_allowed);

      const type = this.form.value.exploitation_type;
      this.checkTypeDigital(+type);
    }
  }

  getValues() {
    const value = this.form.getRawValue();
    value.product_types_allowed = value.product_types_allowed?.map((product: any) => {
      return product.id;
    });
    return value;
  }

  submit() {
    this.form.markAllAsTouched();
    this.form.updateValueAndValidity();
    this.sended = true;
    if (this.form.valid) {
      this.formSubmit.emit([this.detail, this.getValues()]);
    }
    return false;
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.activeRoute.parent });
  }
}
