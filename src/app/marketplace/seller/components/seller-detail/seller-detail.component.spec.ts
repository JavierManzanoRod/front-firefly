import { FormBuilder } from '@angular/forms';
import { Seller } from '../../models/seller.model';
import { SellerDetailComponent } from './seller-detail.component';

describe('SellerDetailComponent', () => {
  it('should create the component', () => {
    const comp = new SellerDetailComponent(new FormBuilder(), null as any, null as any, null as any);
    expect(comp).toBeDefined();
  });

  it('ngOnChanges initialize the form and set loading flag', () => {
    const comp = new SellerDetailComponent(new FormBuilder(), null as any, null as any, null as any);
    const detail = {
      name: 'name',
    } as Seller;
    comp.detail = detail;
    comp.ngOnChanges();
    expect(comp.form.controls.name.value).toBe(detail.name);
  });
});
