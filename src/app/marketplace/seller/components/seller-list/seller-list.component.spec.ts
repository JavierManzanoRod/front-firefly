import { SellerListComponent } from './seller-list.component';

describe('CategoryRuleListComponent', () => {
  it('should create the component', () => {
    const comp = new SellerListComponent();
    expect(comp).toBeDefined();
  });

  it('identify tracks by id ', () => {
    const comp = new SellerListComponent();
    expect(comp.identify(1, { id: '1' })).toBe('1');
  });
});
