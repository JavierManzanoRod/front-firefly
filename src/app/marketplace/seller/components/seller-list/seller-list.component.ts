import { Component } from '@angular/core';
import { MayHaveIdName } from '@model/base-api.model';
import { BaseListComponent } from 'src/app/shared/components/base-component-list.component';
import { Seller } from '../../models/seller.model';

@Component({
  selector: 'ff-seller-list',
  templateUrl: 'seller-list.component.html',
  styleUrls: ['./seller-list.component.scss'],
})
export class SellerListComponent extends BaseListComponent<Seller> {
  constructor() {
    super();
  }

  identify(index: number, item: MayHaveIdName) {
    return item.id;
  }
}
