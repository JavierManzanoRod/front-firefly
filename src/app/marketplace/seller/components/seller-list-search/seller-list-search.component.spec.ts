import { FormBuilder } from '@angular/forms';
import { SellerListSearchComponent } from './seller-list-search.component';

describe('SellerListSearchComponent', () => {
  it('should create the component and a form', () => {
    const comp = new SellerListSearchComponent(new FormBuilder());
    expect(comp).toBeDefined();
    expect(comp.form).toBeDefined();
  });

  it('submit emits a event with the form value', (done) => {
    const comp = new SellerListSearchComponent(new FormBuilder());
    comp.form.patchValue({ name: 'name', site: 'site' });
    comp.search.subscribe((data) => {
      expect(data.name).toEqual(comp.form.controls.name.value);
      expect(data.site).toEqual(comp.form.controls.site.value);
      done();
    });
    comp.submit();
  });

  it('clearData resets the form', () => {
    const comp = new SellerListSearchComponent(new FormBuilder());
    comp.form.patchValue({ name: 'name', site: 'site' });
    comp.clearData();
    expect(comp.form.controls.name.value).toBeNull();
  });
});
