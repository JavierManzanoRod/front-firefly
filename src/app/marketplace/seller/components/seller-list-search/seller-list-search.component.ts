import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SellerRequest } from '../../models/seller.model';

@Component({
  selector: 'ff-seller-list-search',
  templateUrl: 'seller-list-search.component.html',
  styleUrls: ['./seller-list-search.component.scss'],
})
export class SellerListSearchComponent {
  @Output() search: EventEmitter<SellerRequest> = new EventEmitter<SellerRequest>();

  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      name: [null],
      code: [null],
      miraklId: [null],
      site: [null],
      status: [null],
    });
  }

  // code, exploitationType,miraklId,site
  submit() {
    this.search.emit(this.form.value);
  }

  clearData() {
    this.form.reset();
    this.search.emit({});
  }
}
