import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { Pagination2Module } from 'src/app/modules/pagination-2/pagination-2.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ToastModule } from '../../../modules/toast/toast.module';
import { CrosellingQueryListComponent } from './components/croselling-query-list/croselling-query-list.component';
import { CrossellingQuerySearchComponent } from './components/crosselling-query-search/crosselling-query-search.component';
import { CrossellingQueryContainerComponent } from './containers/crosselling-query-container.component';
import { CrossellingQueryRoutingModule } from './crosselling-query-routing.module';

@NgModule({
  declarations: [CrossellingQuerySearchComponent, CrossellingQueryContainerComponent, CrosellingQueryListComponent],
  imports: [
    CommonModule,
    CrossellingQueryRoutingModule,
    FormsModule,
    ToastModule,
    FormErrorModule,
    ReactiveFormsModule,
    SharedModule,
    Pagination2Module,
    CommonModalModule,
    CoreModule,
    SimplebarAngularModule,
    NgSelectModule,
  ],
})
export class CrossellingQueryModule {}
