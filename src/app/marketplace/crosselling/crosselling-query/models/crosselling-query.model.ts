export interface CrosellingQuery {
  brand: string;
  id: string;
  related_products: RelatedProdutcs[];
  site: string;
  title: string;
  ean: string;
}

export interface RelatedProdutcs {
  brand: string;
  eans?: string[];
  id: string;
  title: string;
}

export interface CrosellingQueryDTO {
  brand: Brand;
  identifier: string;
  related_products: RelatedProductsDTO[];
  site: Site;
  title: string;
  ean?: string;
}

export interface RelatedProductsDTO {
  brand: Brand;
  internal_gtins: string[];
  identifier: string;
  title: string;
}

export interface Brand {
  identifier: string;
}

export interface Site {
  identifier: string;
}
