import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BusinessApiService } from '@core/base/api-business.service';
import { BusinessApiResponse, GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_MARKETPLACE_PRODUCT_RELATIONS } from 'src/app/configuration/tokens/api-versions.token';
import { CrosellingQuery, CrosellingQueryDTO } from '../models/crosselling-query.model';
import { CrossellingQueryOutMapper } from './dto/crosseling-query-out-mapper-class';

@Injectable({
  providedIn: 'root',
})
export class CrossellingQueryService extends BusinessApiService<CrosellingQuery> {
  endPoint = `products/marketplace-product-relations/${this.version}`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_MARKETPLACE_PRODUCT_RELATIONS) private version: string) {
    super();
  }

  getTemplate() {
    return this.http.post(`${this.urlBase}files/template_crossSelling.csv/download`, null, {
      responseType: 'text',
      observe: 'response',
    });
  }

  getFile(filter: any, warning = false) {
    const filename = warning ? 'warning.csv' : 'cross_selling.csv';
    return this.http.get(`${this.urlBase}file-uploads?${filename}`, {
      responseType: 'text',
      params: this.paramsFile(filter),
      observe: 'response',
    });
  }

  uploadFile(values: any) {
    const headers = new HttpHeaders({
      'eci-site-id': `${values.site}`,
      'eci-assignment-mode': `${values.assignment_mode}`,
    });
    const formData = new FormData();
    formData.append('file', values.file);
    return this.http.post(`${this.urlBase}files/upload`, formData, { headers });
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<CrosellingQuery>> {
    const params = super.getFilter(filter);
    return this.http
      .get<BusinessApiResponse<CrosellingQuery>>(this.urlBase, {
        params: this.paramsFile(params),
      })
      .pipe(
        map((data) => this.toGeneric(data)),
        map(({ content, page }) => ({
          content: content.map((product) => {
            const data = new CrossellingQueryOutMapper(product as unknown as CrosellingQueryDTO).data;
            return data;
          }),
          page,
        }))
      );
  }
  private paramsFile(filter: any): HttpParams {
    let params = new HttpParams();

    if (filter) {
      params = filter.brand ? params.set('brand', filter.brand.toString()) : params;
      params = filter.ean ? params.set('ean', filter.ean.toString()) : params;
      params = filter.id ? params.set('id', filter.id.toString()) : params;
      params = filter.mpid ? params.set('mpid', filter.mpid.toString()) : params;
      params = filter.site ? params.set('site_id', filter.site.toString()) : params;
    }
    return params;
  }
}
