import { of } from 'rxjs';
import { CrossellingQueryService } from './crosselling-query.service';

const version = 'v1/';
let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

describe('CrossellingService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`CrossellingService points to products/marketplace-product-relations/${version}`, () => {
    const service = new CrossellingQueryService(null as any, version);
    expect(service.endPoint).toEqual(`products/marketplace-product-relations/${version}`);
  });

  it('call for get template', () => {
    httpClientSpy.post.and.returnValue(of({}));
    const s = new CrossellingQueryService(httpClientSpy as any, version);
    s.getTemplate().subscribe(() => {});
    expect(httpClientSpy.post).toHaveBeenCalled();
  });

  it('call for upload file', () => {
    httpClientSpy.post.and.returnValue(of({}));
    const s = new CrossellingQueryService(httpClientSpy as any, version);
    s.uploadFile({}).subscribe(() => {});
    expect(httpClientSpy.post).toHaveBeenCalled();
  });
});
