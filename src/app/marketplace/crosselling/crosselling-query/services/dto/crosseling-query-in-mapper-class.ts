import { CrosellingQuery, CrosellingQueryDTO } from '../../models/crosselling-query.model';

export class CrossellingQueryInMapper {
  data = {} as CrosellingQueryDTO;

  constructor(remoteData: CrosellingQuery) {
    this.data = {
      brand: remoteData.brand,
      identifier: remoteData.id,
      related_products: remoteData.related_products,
      site: remoteData.site,
      title: remoteData.title,
      ean: remoteData.ean,
    } as unknown as CrosellingQueryDTO;
  }
}
