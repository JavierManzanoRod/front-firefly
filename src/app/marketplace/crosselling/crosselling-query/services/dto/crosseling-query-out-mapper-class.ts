import { CrosellingQuery, CrosellingQueryDTO, RelatedProductsDTO } from '../../models/crosselling-query.model';

export class CrossellingQueryOutMapper {
  data = {} as CrosellingQuery;

  constructor(remoteData: CrosellingQueryDTO) {
    this.data = {
      brand: remoteData.brand.identifier,
      id: remoteData.identifier,
      related_products: this.transformRelatedProducts(remoteData.related_products),
      site: remoteData.site.identifier,
      title: remoteData.title,
      ean: remoteData.ean,
    } as unknown as CrosellingQuery;
  }

  private transformRelatedProducts(item: RelatedProductsDTO[]) {
    if (item) {
      return item.map(({ identifier, ...data }: RelatedProductsDTO) => {
        return { ...data, id: identifier };
      });
    }
  }
}
