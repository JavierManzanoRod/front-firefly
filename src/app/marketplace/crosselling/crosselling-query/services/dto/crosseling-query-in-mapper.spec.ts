import { CrosellingQuery } from '../../models/crosselling-query.model';
import { CrossellingQueryInMapper } from './crosseling-query-in-mapper-class';

describe('product-relation mapper in', () => {
  it('parse an outgoing request product relation as expected', () => {
    const remoteItem = {
      id: 'MP_QA_Karate',
      brand: {
        identifier: 'QA TEST KARATE',
      },
      related_products: [
        {
          id: 'MP_QA_Karate',
          brand: {
            identifier: 'QA TEST KARATE',
          },
          title: 'QA TEST PRODUCT RELATION',
          internal_gtins: ['9516561641100'],
        },
      ],
      title: 'QA TEST PRODUCT RELATION',
      site: {
        identifier: 'eciStore',
      },
    } as unknown as CrosellingQuery;

    const mapper = new CrossellingQueryInMapper(remoteItem);
    const product = mapper.data;
    expect(product.identifier).toEqual('MP_QA_Karate');
    expect(product.brand.identifier).toEqual('QA TEST KARATE');
    expect(product.related_products[0].brand).toEqual({
      identifier: 'QA TEST KARATE',
    });
    expect(product.title).toEqual('QA TEST PRODUCT RELATION');
    expect(product.site.identifier).toEqual('eciStore');
  });

  it('parse an outgoing product relation with some fields null or undefined does not break anything', () => {
    const remoteItem = {
      id: undefined,
      brand: {
        id: undefined,
      },
    } as unknown as CrosellingQuery;

    const mapper = new CrossellingQueryInMapper(remoteItem);
    const product = mapper.data;
    expect(product.identifier).toEqual(undefined as any);
    expect(product.brand.identifier).toEqual(undefined as any);
  });
});
