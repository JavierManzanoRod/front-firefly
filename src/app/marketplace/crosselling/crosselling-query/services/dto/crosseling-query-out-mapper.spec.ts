import { CrosellingQueryDTO } from '../../models/crosselling-query.model';
import { CrossellingQueryOutMapper } from './crosseling-query-out-mapper-class';

describe('product-relation mapper out', () => {
  it('parse an incoming response product relation as expected', () => {
    const remoteItem = {
      identifier: 'MP_QA_Karate',
      brand: {
        identifier: 'QA TEST KARATE',
      },
      related_products: [
        {
          identifier: 'MP_QA_Karate',
          brand: {
            identifier: 'QA TEST KARATE',
          },
          title: 'QA TEST PRODUCT RELATION',
          internal_gtins: ['9516561641100'],
        },
      ],
      title: 'QA TEST PRODUCT RELATION',
      site: {
        identifier: 'eciStore',
      },
    } as unknown as CrosellingQueryDTO;

    const mapper = new CrossellingQueryOutMapper(remoteItem);
    const product = mapper.data;
    expect(product.id).toEqual('MP_QA_Karate');
    expect(product.brand).toEqual('QA TEST KARATE');
    expect(product.title).toEqual('QA TEST PRODUCT RELATION');
    expect(product.site).toEqual('eciStore');
  });
});
