import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrossellingQueryContainerComponent } from './containers/crosselling-query-container.component';

const routes: Routes = [
  {
    path: '',
    component: CrossellingQueryContainerComponent,
    data: {
      header_title: 'MENU_LEFT.MKP_ADMINISTRATOR',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrossellingQueryRoutingModule {}
