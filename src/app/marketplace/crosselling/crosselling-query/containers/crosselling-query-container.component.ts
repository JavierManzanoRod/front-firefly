import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { of, Subject } from 'rxjs';
import { catchError, finalize, map, tap } from 'rxjs/operators';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { downloadFile } from 'src/app/shared/utils/utilsCsv';
import { getFileName } from '../../utils/file-name';
import { CrossellingQueryService } from '../services/crosselling-query.service';
import { CrosellingQuery } from './../models/crosselling-query.model';

@Component({
  selector: 'ff-crosselling-query-container',
  template: `<ff-page-header [pageTitle]="'CROSSELLING.QUERY.TITLE' | translate"></ff-page-header>
    <ngx-simplebar class="page-container">
      <div class="page-container-padding">
        <ff-crosselling-query-search (eventSubmit)="search($event)" [subjectSubmit]="subjectSubmit" [subjectEnable]="subjectEnable">
        </ff-crosselling-query-search>
      </div>

      <div class="page-scroll-wrapper">
        <ngx-simplebar class="page-scroll">
          <div class="page-container-padding">
            <ff-croselling-query-list
              *ngIf="ready"
              [loading]="loading"
              [list]="list$ | async"
              [page]="page"
              [currentData]="currentData"
              (pagination)="handlePagination($event)"
            ></ff-croselling-query-list>
            <div class="text-right page-footer">
              <div class="loader" *ngIf="loadingCsv"></div>
              <button
                type="submit"
                *ngIf="tmpList && !loadingCsv"
                data-cy="query-csv-submit-btn"
                (click)="csv()"
                class="btn btn-outline-primary ml-2 btn-sm"
              >
                {{ 'CROSSELLING.CSV' | translate | uppercase }}
              </button>
              <button
                type="submit"
                *ngIf="!tmpList"
                data-cy="query-submit-btn"
                (click)="subjectSubmit.next(null)"
                class="btn btn-primary ml-2 btn-sm"
              >
                {{ 'CROSSELLING.SAVE' | translate | uppercase }}
              </button>
              <button type="submit" *ngIf="tmpList" (click)="retry()" data-cy="query-retry-btn" class="btn btn-primary ml-2 btn-sm">
                {{ 'CROSSELLING.RETRY' | translate | uppercase }}
              </button>
            </div>
          </div>
        </ngx-simplebar>
      </div>
    </ngx-simplebar> `,
  styleUrls: ['./crosselling-query-container.components.scss'],
})
export class CrossellingQueryContainerComponent extends BaseListContainerComponent<CrosellingQuery> implements OnInit {
  ready = false;
  oldValues!: any;

  subjectSubmit = new Subject<any>();
  subjectEnable = new Subject<any>();
  loadingCsv = false;

  constructor(public apiService: CrossellingQueryService, private toast: ToastService, public utils: CrudOperationsService) {
    super(utils, apiService);
  }

  csv() {
    this.loadingCsv = true;

    this.apiService
      .getFile(this.oldValues, false)
      .pipe(finalize(() => (this.loadingCsv = false)))
      .subscribe((v: any) => {
        console.log(v);
        downloadFile([v.body], getFileName(v.headers));
      });
  }

  retry() {
    this.subjectEnable.next(null);
    this.tmpList = undefined;
    this.list$ = of(null);
    this.ready = false;
  }

  search(value: any) {
    this.ready = true;
    this.oldValues = value;
    this.getDataList(value);
  }

  public getDataList(filter: any) {
    this.loading = true;
    this.list$ = this.apiService.list(filter).pipe(
      tap(({ page }) => (this.page = page)),
      tap(({ content }) => {
        if (content.length === 0) {
          this.toast.error('CROSSELLING.QUERY.EMPTY');
        }
        this.tmpList = content;
      }),
      map(({ content }) => content),
      catchError(() => {
        this.page = undefined;
        this.tmpList = [];
        this.toast.error('COMMON_ERRORS.SEARCH_ERROR');
        return of(null);
      }),
      tap(() => (this.loading = false))
    );
  }
}
