import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CrossellingQuerySearchComponent } from './crosselling-query-search.component';

describe('CrossellingQuerySearchComponent', () => {
  let component: CrossellingQuerySearchComponent;
  let fixture: ComponentFixture<CrossellingQuerySearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CrossellingQuerySearchComponent],
      imports: [TranslateModule.forRoot()],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrossellingQuerySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('emit event submit', () => {
    component.form = new FormGroup({});
    const s = spyOn(component.eventSubmit, 'emit');
    component.submit();
    expect(s).toHaveBeenCalled();
  });
});
