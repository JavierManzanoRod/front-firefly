import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'ff-crosselling-query-search',
  templateUrl: './crosselling-query-search.component.html',
  styleUrls: ['./crosselling-query-search.component.scss'],
})
export class CrossellingQuerySearchComponent implements OnInit {
  @Input() subjectSubmit = new Subject<any>();
  @Input() subjectEnable = new Subject<any>();
  @Output() eventSubmit = new EventEmitter<any>();

  form = new FormGroup({
    brand: new FormControl(),
    ean: new FormControl(),
    title: new FormControl(),
    code: new FormControl(),
    site: new FormControl(),
    type: new FormControl('id'),
  });

  formErrors = [
    {
      type: 'required',
      message: 'COMMON_ERRORS.REQUIRED',
    },
  ];

  constructor() {}

  ngOnInit(): void {
    this.subjectSubmit.subscribe(() => {
      this.submit();
    });

    this.subjectEnable.subscribe(() => this.form.enable());
  }

  submit() {
    this.form.markAllAsTouched();

    if (!this.form.valid) {
      return false;
    }
    this.form.disable();

    const values = this.form.value;
    this.eventSubmit.emit({
      ...values,
      [values.type]: values.code,
    });
  }
}
