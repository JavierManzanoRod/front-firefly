import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CrosellingQueryListComponent } from './croselling-query-list.component';

describe('CrosellingQueryListComponent', () => {
  let component: CrosellingQueryListComponent;
  let fixture: ComponentFixture<CrosellingQueryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CrosellingQueryListComponent],
      imports: [TranslateModule.forRoot()],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrosellingQueryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
