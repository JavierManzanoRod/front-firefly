import { Component } from '@angular/core';
import { BaseListComponent } from 'src/app/shared/components/base-component-list.component';
import { CrosellingQuery } from './../../models/crosselling-query.model';

@Component({
  selector: 'ff-croselling-query-list',
  templateUrl: './croselling-query-list.component.html',
  styleUrls: ['./croselling-query-list.component.scss'],
})
export class CrosellingQueryListComponent extends BaseListComponent<CrosellingQuery> {
  showSubitems: { [key: string]: boolean } = {};

  constructor() {
    super();
  }

  toggle(id: string) {
    this.showSubitems[id] = !this.showSubitems[id];
  }
}
