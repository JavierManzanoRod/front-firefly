import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { saveAs } from 'file-saver';
import { Subject } from 'rxjs';
import { validatorCsvExtension, validatorCsvSize } from 'src/app/shared/utils/utilsCsv';
import { CrossellingQueryService } from '../../../crosselling-query/services/crosselling-query.service';
import { getFileName } from '../../../utils/file-name';

@Component({
  selector: 'ff-crosselling-management-config',
  templateUrl: './crosselling-management-config.component.html',
  styleUrls: ['./crosselling-management-config.component.scss'],
})
export class CrossellingManagementConfigComponent implements OnInit {
  @Output() eventSumit = new EventEmitter<any>();
  @Input() loading = false;
  @Input() showSuccess = false;
  @Input() showError = false;
  @Input() errorMsg = 'CROSSELLING.MANAGEMENT.ERROR';

  form = new FormGroup({
    assignment_mode: new FormControl('', Validators.required),
    file: new FormControl('', Validators.required),
    site: new FormControl('', Validators.required),
  });

  errorMessage = [
    {
      type: 'required',
      message: 'COMMON_ERRORS.REQUIRED',
    },
    {
      type: 'extensionCsv',
      message: 'COMMON_ERRORS.CSV.EXTENSION',
    },
    {
      type: 'size',
      message: 'COMMON_ERRORS.CSV.SIZE',
    },
  ];

  blobTemplate!: any;
  file!: any;
  cleanSubject = new Subject<any>();
  nameCsv = '';

  constructor(private apiService: CrossellingQueryService) {}

  ngOnInit(): void {
    this.apiService.getTemplate().subscribe((v) => {
      this.nameCsv = getFileName(v.headers);
      this.blobTemplate = new Blob([v.body as string], { type: 'text/csv' });
    });
  }

  submit() {
    this.form.markAllAsTouched();

    if (this.form.valid) {
      this.eventSumit.emit({ ...this.form.value, file: this.file });
    }
  }

  download() {
    saveAs(this.blobTemplate, this.nameCsv);
  }

  fileLoad(event: File) {
    const formFile = this.form.controls.file;

    if (!event) {
      formFile.setValue(event);
      return false;
    }

    formFile.setValue(event);
    let errors = formFile.errors;

    if (!validatorCsvExtension(event)) {
      formFile.setErrors({
        ...errors,
        extensionCsv: true,
      });
      errors = formFile.errors;
    }

    if (!validatorCsvSize(event)) {
      formFile.setErrors({
        ...errors,
        size: true,
      });
    }

    this.file = event;
  }

  clean() {
    this.form.reset();
    this.showError = false;
    this.showSuccess = false;
    this.cleanSubject.next(null);
  }
}
