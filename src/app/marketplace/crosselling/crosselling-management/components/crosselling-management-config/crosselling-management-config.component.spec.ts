import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CrossellingQueryService } from '../../../crosselling-query/services/crosselling-query.service';
import { CrossellingManagementConfigComponent } from './crosselling-management-config.component';

xdescribe('CrossellingManagementConfigComponent', () => {
  let component: CrossellingManagementConfigComponent;
  let fixture: ComponentFixture<CrossellingManagementConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CrossellingManagementConfigComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: CrossellingQueryService,
          useFactory: () => ({
            getTemplate: () => {
              return of({
                headers: {},
              });
            },
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrossellingManagementConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('emit event submit', () => {
    component.form = new FormGroup({});
    const s = spyOn(component.eventSumit, 'emit');
    component.submit();
    expect(s).toHaveBeenCalled();
  });

  it('file with errors', () => {
    component.fileLoad({
      name: 'test',
      size: 1000000000000000,
    } as File);
    expect(component.form.controls.file.valid).toBeFalsy();
  });

  it('file without errors', () => {
    component.fileLoad({
      name: 'test.csv',
      size: 10,
    } as File);
    expect(component.form.controls.file.valid).toBeTruthy();
  });
});
