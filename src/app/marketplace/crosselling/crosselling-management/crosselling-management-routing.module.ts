import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrossellingManagementViewComponent } from './containers/crosselling-management-view/crosselling-management-view.component';

const routes: Routes = [
  {
    path: '',
    component: CrossellingManagementViewComponent,
    data: {
      header_title: 'MENU_LEFT.MKP_ADMINISTRATOR',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrossellingManagementRoutingModule {}
