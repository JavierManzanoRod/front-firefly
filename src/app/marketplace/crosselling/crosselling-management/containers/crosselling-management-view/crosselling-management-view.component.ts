import { Component, OnInit } from '@angular/core';
import { CrossellingQueryService } from '../../../crosselling-query/services/crosselling-query.service';

@Component({
  selector: 'ff-crosselling-management-view',
  template: `<ff-page-header [pageTitle]="'CROSSELLING.MANAGEMENT.TITLE' | translate"></ff-page-header>
    <ngx-simplebar class="page-container">
      <div class="page-container-padding">
        <ff-crosselling-management-config
          [loading]="loading"
          (eventSumit)="submit($event)"
          [showSuccess]="showSuccess"
          [showError]="showError"
          [errorMsg]="errorMsg"
        ></ff-crosselling-management-config>
      </div>

      <div class="page-scroll-wrapper">
        <ngx-simplebar class="page-scroll">
          <div class="page-container-padding"></div>
        </ngx-simplebar>
      </div>
    </ngx-simplebar> `,
  styleUrls: ['./crosselling-management-view.component.scss'],
})
export class CrossellingManagementViewComponent implements OnInit {
  loading = false;
  showSuccess = false;
  showError = false;
  errorMsg = '';
  private readonly errorDefault = 'COMMON_ERRORS.SAVE_ERROR';

  constructor(private apiService: CrossellingQueryService) {
    this.errorMsg = this.errorDefault;
  }

  ngOnInit(): void {}

  submit(value: any) {
    this.loading = true;
    this.showSuccess = false;
    this.showError = false;
    this.errorMsg = this.errorDefault;

    this.apiService.uploadFile(value).subscribe(
      (d) => {
        this.showSuccess = true;
        this.loading = false;
      },
      (e) => {
        if (e && e.error && e.error.error.detail) {
          const search = e.error.error.detail.validations?.find((data: any) => data.code === 'ERROR_WRONG_FILE_FORMAT_CODE');
          if (search) {
            this.errorMsg = 'CROSSELLING.MANAGEMENT.ERROR_FORMAT';
          }
        }

        this.showError = true;
        this.loading = false;
      }
    );
  }
}
