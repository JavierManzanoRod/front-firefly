import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SimplebarAngularModule } from 'simplebar-angular';
import { FileUploadModule } from 'src/app/file-upload/components/file-upload.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ToastModule } from '../../../modules/toast/toast.module';
import { CrossellingManagementConfigComponent } from './components/crosselling-management-config/crosselling-management-config.component';
import { CrossellingManagementViewComponent } from './containers/crosselling-management-view/crosselling-management-view.component';
import { CrossellingManagementRoutingModule } from './crosselling-management-routing.module';

@NgModule({
  declarations: [CrossellingManagementViewComponent, CrossellingManagementConfigComponent],
  imports: [
    CommonModule,
    CrossellingManagementRoutingModule,
    SharedModule,
    FormErrorModule,
    FormsModule,
    ReactiveFormsModule,
    ToastModule,
    SimplebarAngularModule,
    FileUploadModule,
    NgSelectModule,
  ],
})
export class CrossellingManagementModule {}
