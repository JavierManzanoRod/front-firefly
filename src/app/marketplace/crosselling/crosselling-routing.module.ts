import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'query',
    loadChildren: () => import('./crosselling-query/crosselling-query.module').then((m) => m.CrossellingQueryModule),
  },
  {
    path: 'management',
    loadChildren: () => import('./crosselling-management/crosselling-management.module').then((m) => m.CrossellingManagementModule),
  },
  {
    path: 'historic',
    loadChildren: () => import('./crosselling-historic/crosselling-historic.module').then((m) => m.CrossellingHistoricModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrossellingRoutingModule {}
