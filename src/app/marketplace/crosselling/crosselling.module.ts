import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CrossellingRoutingModule } from './crosselling-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CrossellingRoutingModule],
})
export class CrossellingModule {}
