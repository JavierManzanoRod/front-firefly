import { HttpHeaders } from '@angular/common/http';

export function getFileName(headers: HttpHeaders): string {
  const contentdisposition = headers.get('content-disposition');
  const splitContent = contentdisposition?.split('="');
  let splitName = '';

  if (splitContent && splitContent.length >= 1) {
    splitName = splitContent[1];
  }

  if (splitName.length > 0) {
    return splitName.split('"')[0];
  }

  return '';
}
