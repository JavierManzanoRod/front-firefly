import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrossellingHistoricContainerComponent } from './containers/crosselling-historic-list-container';

const routes: Routes = [
  {
    path: '',
    component: CrossellingHistoricContainerComponent,
    data: {
      header_title: 'MENU_LEFT.MKP_ADMINISTRATOR',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrossellingHistoricRoutingModule {}
