import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CrossellingQueryService } from '../../../crosselling-query/services/crosselling-query.service';
import { CrossellingHistoricListComponent } from './crosselling-historic-list.component';

describe('CrossellingHistoricListComponent', () => {
  let component: CrossellingHistoricListComponent;
  let fixture: ComponentFixture<CrossellingHistoricListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CrossellingHistoricListComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: CrossellingQueryService,
          useFactory: () => ({
            getFile: () => {
              of({
                headers: {},
              });
            },
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrossellingHistoricListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
