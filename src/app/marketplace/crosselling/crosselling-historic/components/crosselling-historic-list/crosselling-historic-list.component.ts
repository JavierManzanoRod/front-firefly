import { Component } from '@angular/core';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { finalize } from 'rxjs/operators';
import { BaseListComponent } from 'src/app/shared/components/base-component-list.component';
import { downloadFile } from 'src/app/shared/utils/utilsCsv';
import { getFileName } from '../../../utils/file-name';
import { CrossellingHistoric } from '../../models/crosselling-historic.model';
import { CrossellingQueryService } from './../../../crosselling-query/services/crosselling-query.service';

@Component({
  selector: 'ff-crosselling-historic-list',
  templateUrl: './crosselling-historic-list.component.html',
  styleUrls: ['./crosselling-historic-list.component.scss'],
})
export class CrossellingHistoricListComponent extends BaseListComponent<CrossellingHistoric> {
  loadingIcon: { [key: string]: boolean } = {};

  constructor(private apiService: CrossellingQueryService) {
    super();
  }

  download(id: string) {
    this.loadingIcon[id] = true;
    this.apiService
      .getFile({ id }, true)
      .pipe(finalize(() => (this.loadingIcon[id] = false)))
      .subscribe((value) => {
        downloadFile([value.body], getFileName(value.headers));
      });
  }

  getTypeResult(item: any): string {
    return item.validations && item.validations.length > 0 ? item.validations[0].level : 'OK';
  }

  handlePageChanged(event: PageChangedEvent) {
    const { page, itemsPerPage } = event;
    if (page !== this.currentPage) {
      this.currentPage = page;
      const data = {
        ...this.currentData,
        page,
        size: itemsPerPage,
      } as any;
      this.pagination.emit(data);
    }
  }
}
