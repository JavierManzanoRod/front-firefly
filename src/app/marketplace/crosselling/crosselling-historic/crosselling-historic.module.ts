import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/core.module';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { Pagination2Module } from 'src/app/modules/pagination-2/pagination-2.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ToastModule } from '../../../modules/toast/toast.module';
import { SearchSimpleModule } from './../../../modules/search-simple/search-simple.module';
import { CrossellingHistoricListComponent } from './components/crosselling-historic-list/crosselling-historic-list.component';
import { CrossellingHistoricContainerComponent } from './containers/crosselling-historic-list-container';
import { CrossellingHistoricRoutingModule } from './crosselling-historic-routing.module';

@NgModule({
  declarations: [CrossellingHistoricListComponent, CrossellingHistoricContainerComponent],
  imports: [
    CommonModule,
    Pagination2Module,
    SharedModule,
    ToastModule,
    SearchSimpleModule,
    CrossellingHistoricRoutingModule,
    SimplebarAngularModule,
    CoreModule,
    CommonModalModule,
  ],
})
export class CrossellingHistoricModule {}
