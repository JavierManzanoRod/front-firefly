export interface CrossellingHistoric {
  assignment_mode: string;
  filename: string;
  import_date: string;
  result: string;
  site: string;
  validations: {
    code: string;
    level: string;
    message: string;
  }[];
  id: string;
}

export interface CrossellingHistoricDTO {
  assignment_mode: string;
  filename: string;
  import_date: string;
  result: string;
  site: SiteDTO;
  validations: {
    code: string;
    level: string;
    message: string;
  }[];
  identifier: string;
}

export interface SiteDTO {
  identifier: string;
}
