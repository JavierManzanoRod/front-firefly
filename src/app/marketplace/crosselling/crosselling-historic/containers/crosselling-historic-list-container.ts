import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { CrossellingHistoric } from '../models/crosselling-historic.model';
import { CrossellingHistoricService } from '../services/crosselling-historic.service';

@Component({
  selector: 'ff-crosselling-historic-list-container',
  template: `<ff-page-header [pageTitle]="'CROSSELLING.HISTORIC.TITLE' | translate"></ff-page-header>
    <ngx-simplebar class="page-container">
      <div class="page-container-padding">
        <ff-search-simple
          (search)="search($event)"
          [numberItems]="page?.total_elements"
          [key]="'filename'"
          [loading]="loading"
          [title]="'COMMON.SEARCH.TITLE'"
          [placeHolder]="'CROSSELLING.HISTORIC.SEARCH_PLACEHOLDER'"
        ></ff-search-simple>
      </div>

      <div class="page-scroll-wrapper">
        <ngx-simplebar class="page-scroll">
          <div class="page-container-padding">
            <ff-crosselling-historic-list
              [list]="list$ | async"
              [loading]="loading"
              [page]="page"
              [currentData]="currentData"
              (pagination)="handlePagination($event)"
            ></ff-crosselling-historic-list>
          </div>
        </ngx-simplebar>
      </div>
    </ngx-simplebar> `,
})
export class CrossellingHistoricContainerComponent extends BaseListContainerComponent<CrossellingHistoric> implements OnInit {
  constructor(public apiService: CrossellingHistoricService, private toast: ToastService, public utils: CrudOperationsService) {
    super(utils, apiService);
  }

  ngOnInit() {
    this.getDataList(null);
  }

  public getDataList(filter: any) {
    this.loading = true;
    this.filter = filter;
    this.list$ = this.apiService.list(filter).pipe(
      tap(({ page }) => (this.page = page)),
      tap(({ content }) => {
        if (content.length === 0) {
          this.toast.error('CROSSELLING.QUERY.EMPTY');
        }
        this.tmpList = content;
      }),
      map(({ content }) => content),
      catchError(() => {
        this.page = undefined;
        this.tmpList = [];
        this.toast.error('COMMON_ERRORS.SEARCH_ERROR');
        return of(null);
      }),
      tap(() => (this.loading = false))
    );
  }
}
