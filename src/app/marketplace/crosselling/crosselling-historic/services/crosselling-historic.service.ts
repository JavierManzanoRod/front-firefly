import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BusinessApiService } from '@core/base/api-business.service';
import { BusinessApiResponse, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_MARKETPLACE_PRODUCT_RELATIONS } from 'src/app/configuration/tokens/api-versions.token';
import { CrossellingHistoric, CrossellingHistoricDTO } from '../models/crosselling-historic.model';
import { CrossellingHistoricOutMapper } from './dto/crosselling-historic-out-mapper-class';

@Injectable({
  providedIn: 'root',
})
export class CrossellingHistoricService extends BusinessApiService<CrossellingHistoric> {
  endPoint = `products/marketplace-product-relations/${this.version}`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_MARKETPLACE_PRODUCT_RELATIONS) private version: string) {
    super();
  }

  list(filter: any): Observable<GenericApiResponse<CrossellingHistoric>> {
    const paramsSize = super.getFilter(filter);
    return this.http
      .get<BusinessApiResponse<CrossellingHistoric>>(this.urlBase + 'file-uploads', {
        params: this.getParams(paramsSize),
      })
      .pipe(
        map((data) => this.toGeneric(data)),
        map(({ content, page }) => ({
          content: content.map((seller) => {
            const data = new CrossellingHistoricOutMapper(seller as unknown as CrossellingHistoricDTO).data;
            return data;
          }),
          page,
        })),
        map((value) => {
          value.content = value.content.map((v) => {
            switch (v.assignment_mode) {
              case 'CREATE':
                v.assignment_mode = 'CREAR';
                break;
              case 'UPDATE':
                v.assignment_mode = 'ACTUALIZAR';
                break;
              case 'DELETE':
                v.assignment_mode = 'ELIMINAR';
                break;
              case 'PROCESSING':
                v.assignment_mode = 'PROCESAR';
                break;
            }

            return v;
          });
          return value;
        })
      );
  }
}
