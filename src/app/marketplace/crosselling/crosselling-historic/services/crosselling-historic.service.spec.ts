import { HttpClient } from '@angular/common/http';
import { BusinessApiResponse, GenericApiRequest, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { CrossellingHistoricDTO } from '../models/crosselling-historic.model';
import { CrossellingHistoricService } from './crosselling-historic.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: CrossellingHistoricService;
const version = 'v1/';
const expectedData: CrossellingHistoricDTO = {
  assignment_mode: 'ahh',
  filename: `file`,
  import_date: '2020-08-23T10:20Z',
  result: 'a',
  site: {
    identifier: 'ada',
  },
  validations: [{ code: 's', level: 'ad', message: 'sad' }],
  identifier: 'el',
};

const expectedList: BusinessApiResponse<CrossellingHistoricDTO> = {
  business_object: [expectedData],
  pagination_object: null as unknown as Page,
};

describe('CrossellingHistoricService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`CrossellingHistoricService points to products/marketplace-product-relations/${version}`, () => {
    const service = new CrossellingHistoricService(null as any, version);
    expect(service.endPoint).toEqual(`products/marketplace-product-relations/${version}`);
  });

  it('CrossellingHistoricService.list calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new CrossellingHistoricService(httpClientSpy as any, version);
    myService.list(null as unknown as GenericApiRequest).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });

  it('CrossellingHistoricService.emptyRecord record gives me some data', () => {
    myService = new CrossellingHistoricService(null as unknown as HttpClient, version);
    const record = myService.emptyRecord();
    expect(record).toBeDefined();
  });
});
