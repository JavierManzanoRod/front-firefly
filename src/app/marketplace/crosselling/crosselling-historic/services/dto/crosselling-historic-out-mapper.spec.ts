import { CrossellingHistoricDTO } from '../../models/crosselling-historic.model';
import { CrossellingHistoricOutMapper } from './crosselling-historic-out-mapper-class';

describe('historic Crosselling mapper out', () => {
  it('parse a outgoing request as expected', () => {
    const remoteItem: CrossellingHistoricDTO = {
      assignment_mode: 'ahh',
      filename: `file`,
      import_date: '2020-08-23T10:20Z',
      result: 'a',
      site: {
        identifier: 'ada',
      },
      validations: [{ code: 's', level: 'ad', message: 'sad' }],
      identifier: 'el',
    } as unknown as CrossellingHistoricDTO;

    const mapper = new CrossellingHistoricOutMapper(remoteItem);
    const historic = mapper.data;
    expect(historic.assignment_mode).toEqual('ahh');
    expect(historic.filename).toEqual('file');
    expect(historic.import_date).toEqual('2020-08-23T10:20Z');
    expect(historic.result).toEqual('a');
    expect(historic.site).toEqual('ada');
    expect(historic.result).toEqual('a');
    expect(historic.validations).toEqual([{ code: 's', level: 'ad', message: 'sad' }]);
    expect(historic.id).toEqual('el');
  });

  it('parse an incoming crosselling with some fields null or undefined does not break anything', () => {
    const remoteItem: CrossellingHistoricDTO = {
      assignment_mode: null,
      filename: null,
      import_date: null,
      result: null,
      site: {
        identifier: undefined,
      },
      validations: [{ code: undefined, level: undefined, message: undefined }],
      identifier: null,
    } as unknown as CrossellingHistoricDTO;

    const mapper = new CrossellingHistoricOutMapper(remoteItem);
    const historic = mapper.data;
    expect(historic.assignment_mode).toEqual(null as any);
    expect(historic.filename).toEqual(null as any);
    expect(historic.import_date).toEqual(null as any);
    expect(historic.site).toEqual(undefined as any);
  });
});
