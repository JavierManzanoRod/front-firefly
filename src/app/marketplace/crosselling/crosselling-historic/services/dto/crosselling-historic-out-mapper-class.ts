import { CrossellingHistoric, CrossellingHistoricDTO } from '../../models/crosselling-historic.model';

export class CrossellingHistoricOutMapper {
  data = {} as CrossellingHistoric;

  constructor(remoteData: CrossellingHistoricDTO) {
    this.data = {
      assignment_mode: remoteData.assignment_mode,
      filename: remoteData.filename,
      import_date: remoteData.import_date,
      result: remoteData.result,
      site: remoteData.site.identifier,
      validations: remoteData.validations,
      id: remoteData.identifier,
    } as unknown as CrossellingHistoric;
  }
}
