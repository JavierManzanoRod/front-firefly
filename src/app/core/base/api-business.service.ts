import { BusinessApiResponse, GenericApiResponse, MayHaveIdName } from '@model/base-api.model';
import { ApiService } from './api.service';

export class BusinessApiService<U extends MayHaveIdName> extends ApiService<U> {
  toGeneric({ business_object, pagination_object }: BusinessApiResponse<U>): GenericApiResponse<U> {
    return {
      content: business_object,
      page: pagination_object,
    };
  }

  toBusiness({ content, page }: GenericApiResponse<U>): BusinessApiResponse<U> {
    return {
      business_object: content,
      pagination_object: page,
    };
  }
}
