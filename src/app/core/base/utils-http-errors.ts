export interface CommonHttpErrors {
  urlSubstr?: string;
  message?: string;
  codeStatus?: number;
  requestMethod?: 'POST' | 'DELETE' | 'PUT';
}

export const httpMessageErrors: CommonHttpErrors[] = [
  {
    urlSubstr: 'COMMON_ERRORS.DELETE_ERROR',
    requestMethod: 'POST',
  },
  {
    urlSubstr: 'geo-zones/scopes',
    message: 'SCOPE.NAME_ALREADY_EXISTS',
    codeStatus: 409,
    requestMethod: 'POST',
  },
  {
    urlSubstr: 'geo-zones/scopes',
    message: 'SCOPE.NAME_ALREADY_EXISTS',
    codeStatus: 409,
    requestMethod: 'PUT',
  },
  {
    urlSubstr: 'geo-zones/scopes',
    message: 'SCOPE.DELETE_SCOPE_ERROR',
    codeStatus: 409,
    requestMethod: 'DELETE',
  },
  {
    urlSubstr: 'sites/v1/sites',
    message: 'SITES.ERROR_ID_EXISTS',
    codeStatus: 409,
    requestMethod: 'POST',
  },
  {
    urlSubstr: 'subsites/v1/sites',
    message: 'SUBSITES.ERROR_ID_EXISTS',
    codeStatus: 409,
    requestMethod: 'POST',
  },
  {
    message: 'Error, el registro está siendo usado en otra entidad',
    codeStatus: 409,
    requestMethod: 'POST',
  },
  {
    urlSubstr: 'geo-zones/targets',
    message: 'TARGET.NAME_ALREADY_EXISTS',
    codeStatus: 409,
    requestMethod: 'POST',
  },
  {
    urlSubstr: 'geo-zones/targets',
    message: 'TARGET.NAME_ALREADY_EXISTS',
    codeStatus: 409,
    requestMethod: 'PUT',
  },
  {
    urlSubstr: 'entity-type/v1/entity-types',
    message: 'ENTITY_TYPES.ERROR_DELETE_409',
    codeStatus: 400,
    requestMethod: 'DELETE',
  },
  {
    urlSubstr: 'entity-type/v1/entity-types',
    message: 'ENTITY_TYPES.ERROR_POST_409',
    codeStatus: 409,
    requestMethod: 'POST',
  },
  {
    message: 'COMMON_ERRORS.TIMEOUT',
    codeStatus: 1,
  },
];
