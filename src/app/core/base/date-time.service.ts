/* eslint-disable @typescript-eslint/restrict-plus-operands */
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateTimeService {
  public setDateTime(date: Date, time: Date) {
    const newDate = date;

    if (time) {
      newDate.setHours(time.getHours());
      newDate.setMinutes(time.getMinutes());
    }

    return newDate;
  }

  public convertUTCDateToSend(date: Date): string {
    const localDate = new Date(date);

    const hour = (localDate.getUTCHours() + '').length < 2 ? '0' + localDate.getUTCHours() : localDate.getUTCHours();
    const minute = (localDate.getUTCMinutes() + '').length < 2 ? '0' + localDate.getUTCMinutes() : localDate.getUTCMinutes();
    const day = (localDate.getUTCDate() + '').length < 2 ? '0' + localDate.getUTCDate() : localDate.getUTCDate();
    const month = (localDate.getUTCMonth() + 1 + '').length < 2 ? '0' + (localDate.getUTCMonth() + 1) : localDate.getUTCMonth() + 1;
    const year = localDate.getUTCFullYear();

    const fecha = `${year}-${month}-${day}T${hour}:${minute}Z`;
    return fecha;
  }

  public convertDateToSend(date: Date | string): string {
    const localDate = new Date(date);

    const hour = (localDate.getHours() + '').length < 2 ? '0' + localDate.getHours() : localDate.getHours();
    const minute = (localDate.getMinutes() + '').length < 2 ? '0' + localDate.getMinutes() : localDate.getMinutes();
    const day = (localDate.getDate() + '').length < 2 ? '0' + localDate.getDate() : localDate.getDate();
    const month = (localDate.getMonth() + 1 + '').length < 2 ? '0' + (localDate.getMonth() + 1) : localDate.getMonth() + 1;
    const year = localDate.getFullYear();

    const fecha = `${year}-${month}-${day}T${hour}:${minute}Z`;
    return fecha;
  }

  public startOfDay(date: Date): Date {
    const temp = this.cloneDate(date);
    temp.setHours(0, 0, 0, 0);
    return temp;
  }

  public endOfDay(date: Date): Date {
    const temp = this.cloneDate(date);
    temp.setHours(23, 59, 59, 999);
    return temp;
  }

  public subDay(date: Date, days: number): Date {
    return this.cloneDate(date.getTime() - 1000 * 60 * 60 * 24 * days);
  }

  public addDay(date: Date, days: number): Date {
    const temp = this.cloneDate(date);
    temp.setDate(date.getDate() + days);
    return temp;
  }

  private cloneDate(date: Date | number) {
    return new Date(typeof date === 'number' ? date : date.getTime());
  }
}
