import { HttpClient } from '@angular/common/http';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { of } from 'rxjs';
import { ApiService, BaseApiService } from './api.service';

let httpClientSpy: { get: jasmine.Spy; delete: jasmine.Spy; request: jasmine.Spy };
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface MyModel {
  id: string;
  name?: string;
}
class MyService extends ApiService<MyModel> {
  endPoint = 'endpoint';
  constructor(protected http: HttpClient) {
    super();
  }
}
let myService: MyService;
const expectedData: MyModel = { id: '1', name: 'name' };
const expectedList: GenericApiResponse<MyModel> = {
  content: [expectedData],
  page: null as any,
};

describe('base api.service', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'delete', 'request']);
  });

  it('api.service.list handle gets and returns lists', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new MyService(httpClientSpy as any);
    myService.list(null as any).subscribe((response) => {
      const content = response && response.content && response.content[0];
      expect(content).toEqual(expectedData, 'expected data');
    });
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('api.service.get handle detail and return one element', () => {
    httpClientSpy.get.and.returnValue(of(expectedData));
    myService = new MyService(httpClientSpy as any);
    myService.detail(null as any).subscribe((response) => {
      expect(response).toEqual(expectedData, 'expected data');
    });
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('api.service.update handle detail and return one element', () => {
    const rawHttpResponse = { status: 200, body: { name: 'api-response-name' } };
    httpClientSpy.request.and.returnValue(of(rawHttpResponse));
    myService = new MyService(httpClientSpy as any);
    myService.update(expectedData).subscribe((response) => {
      expect(response.status).toEqual(rawHttpResponse.status, 'expected status');
      expect(response.data.name).toEqual(rawHttpResponse.body.name, 'expected status');
    });
    expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
  });

  it('api.service.post handle detail and return one element', () => {
    const rawHttpResponse = { status: 200, body: { name: 'api-response-name' } };
    httpClientSpy.request.and.returnValue(of(rawHttpResponse));
    myService = new MyService(httpClientSpy as any);
    myService.post(expectedData).subscribe((response) => {
      expect(response.status).toEqual(rawHttpResponse.status, 'expected status');
      expect(response.data.name).toEqual(rawHttpResponse.body.name, 'expected status');
    });
    expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
  });

  it('api.service.delete handle detail and return one element', () => {
    httpClientSpy.delete.and.returnValue(of(expectedData));
    myService = new MyService(httpClientSpy as any);
    myService.delete(expectedData).subscribe((response) => {
      expect(response).toEqual(expectedData, 'expected data');
    });
    expect(httpClientSpy.delete.calls.count()).toBe(1, 'one call');
  });

  it('BaseApiService handle params ', () => {
    const baseApiService = new BaseApiService();
    // eslint-disable-next-line one-var
    const page = 1,
      name = 'name',
      // eslint-disable-next-line @typescript-eslint/naming-convention,no-underscore-dangle,id-blacklist,id-match
      page_number = 2,
      // eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
      page_size = 3,
      params: GenericApiRequest = {
        name,
        page,
        page_number,
        page_size,
      };
    const paramsToSend = baseApiService.getParams(params);
    expect(paramsToSend.get('page')).toBe(String(page));
    expect(paramsToSend.get('page_number')).toBe(String(page_number));
    expect(paramsToSend.get('page_size')).toBe(String(page_size));
  });
});
