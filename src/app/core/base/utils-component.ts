import { HttpErrorResponse } from '@angular/common/http';
import { MayHaveIdName, ModalEngineService } from '@model/base-api.model';
import { Page } from '@model/metadata.model';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { ModalCloneComponent } from 'src/app/modules/common-modal/components/modal-clone/modal-clone.component';
import { ModalCreateComponent } from 'src/app/modules/common-modal/components/modal-create/modal-create.component';
import { ModalDeleteComponent } from 'src/app/modules/common-modal/components/modal-delete/modal-delete.component';
import { ModalSaveComponent } from 'src/app/modules/common-modal/components/modal-save/modal-save.component';
import { ToastService } from '../../modules/toast/toast.service';
import { IApiService2 } from './api.service';
import { httpMessageErrors } from './utils-http-errors';

interface Config {
  modalService: ModalEngineService;
  apiService: IApiService2;
  toast?: ToastService;
  tmpList?: any[];
  page?: Page;
  list$?: Observable<any[]>;
  methodToApply?: 'PUT' | 'POST';
}

export class UtilsComponent {
  deleteActionModal(
    { modalService, apiService, toast }: Config,
    itemToDelete: MayHaveIdName,
    next?: any,
    errorMessage: ((error: HttpErrorResponse, modal?: ModalEngineService, method?: any) => string) | string = 'COMMON_ERRORS.DELETE_ERROR'
  ) {
    const next2 = () => {
      if (toast) {
        toast.success('El registro se ha eliminado correctamente');
        if (next) {
          next();
        }
      }
    };
    this.modal(modalService, apiService.delete.bind(apiService), ModalDeleteComponent, itemToDelete, errorMessage, next2, 'DELETE');
  }

  updateOrSaveModal(
    { modalService, apiService, toast, methodToApply }: Config,
    itemToUpdate: MayHaveIdName,
    next?: any,
    errorMessage: ((error: HttpErrorResponse, modal?: ModalEngineService, method?: any) => string) | string = 'COMMON_ERRORS.SAVE_ERROR',
    isPatch: boolean = false
  ) {
    let componentToShow: any;
    componentToShow = ModalSaveComponent;
    let method: any = itemToUpdate.id ? apiService.update : apiService.post;
    if (methodToApply) {
      method = methodToApply === 'PUT' ? apiService.update : apiService.post;
      if (methodToApply === 'POST') {
        componentToShow = ModalCreateComponent;
      }
    }

    if (isPatch) {
      method = apiService.patch;
      componentToShow = ModalSaveComponent;
    }

    const next2 = (resultOperation: any) => {
      if (toast) {
        let textToShowToast;

        // Se esta comprobando si viene algo en el objeto no si el status o el data es el se exito
        if (resultOperation && !resultOperation.error) {
          textToShowToast =
            resultOperation.status === 200 ? 'El registro se ha actualizado correctamente' : 'Acción registrada correctamente';

          toast.success(textToShowToast);
        }
      }
      if (next) {
        next(resultOperation);
      }
    };
    this.modal(modalService, method.bind(apiService), componentToShow, itemToUpdate, errorMessage, next2, methodToApply);
  }

  cloneActionModal(
    { modalService, apiService, toast }: Config,
    itemToClone: MayHaveIdName,
    next?: any,
    errorMessage: ((error: HttpErrorResponse, modal?: ModalEngineService, method?: any) => string) | string = 'COMMON_ERRORS.CLONE_ERROR'
  ) {
    const componentToShow = ModalCloneComponent;
    const method: any = apiService.post;

    const next2 = (resultOperation: any) => {
      if (toast) {
        let textToShowToast;

        if (!resultOperation) {
          textToShowToast = 'Se ha producido un error';
        } else {
          textToShowToast =
            resultOperation.status === 200 ? 'El registro se ha actualizado correctamente' : 'El registro se ha clonado correctamente';
        }

        toast.success(textToShowToast);
        if (next) {
          next(resultOperation);
        }
      }
    };
    this.modal(modalService, method.bind(apiService), componentToShow, itemToClone, errorMessage, next2);
  }

  modal(
    modalService: Config['modalService'],
    method: any,
    componentToShow: any,
    item: MayHaveIdName,
    errorMessage: ((error: HttpErrorResponse, modal?: ModalEngineService, method?: any) => string) | string,
    next?: any,
    requestMethod?: string
  ) {
    const newItem = { ...item };
    const initialState = { item: newItem };
    const modal = modalService.show(componentToShow, { initialState });
    const content = modal.content;
    content.confirm
      .pipe(
        tap(() => (content.loading = true)),
        switchMap(() => method(newItem)),
        tap(() => (content.loading = false))
      )
      .subscribe(
        (result: any) => {
          modal.hide();
          if (next) {
            next(result);
          }
        },
        (e: HttpErrorResponse) => {
          const text = httpMessageErrors.find(
            (error) =>
              error.codeStatus === e.status && e.url && e.url.includes(error.urlSubstr || '') && requestMethod === error.requestMethod
          );
          content.loading = false;
          modal.content.errorMessage = text
            ? text.message
            : errorMessage instanceof Function
            ? errorMessage(e, modal, method)
            : errorMessage;
        }
      );
  }
}
