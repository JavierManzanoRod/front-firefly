import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Attribute } from '@core/models/attribute.model';
import { Audit, AuditQuery } from '@core/models/audit.model';
import { EntityType } from '@core/models/entity-type.model';
import { environment } from '@env/environment';
import { GenericApiRequest, GenericApiResponse, MayHaveIdName } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface IApiAttributes {
  attributes(x: any): Observable<Attribute[]>;
}

export interface IApiSearchable<T extends MayHaveIdName> {
  search(str: string): Observable<MayHaveIdName[]>;

  detail(x: any, expanded?: boolean): Observable<T>;
}

export interface IApiService2<T extends MayHaveIdName = MayHaveIdName> {
  endPoint: string;

  list(x: any, expanded?: boolean): Observable<GenericApiResponse<T>>;

  detail(x: T | string, expanded?: boolean): Observable<T>;

  post(x: T): Observable<UpdateResponse<T>>;

  publish?(x: T): Observable<UpdateResponse<T>>;

  update(y: T): Observable<UpdateResponse<T>>;

  delete(x: T): Observable<T>;

  patch?(y: T): Observable<UpdateResponse<T>>;

  emptyRecord?(): T;

  buildDetail?(id: string): Observable<T>;
}

export interface UpdateResponse<U> {
  status: number;
  data: U;
  error?: any;
}

export interface IApiGetAllService {
  getAll(x: any): Observable<any>;
}

export interface IApiListEntityTypes {
  entityTypes(x: any): Observable<EntityType[]>;
}

export class BaseApiService {
  getParams(filter: any): HttpParams {
    let params: HttpParams = new HttpParams();
    const keys = Object.keys(filter || {});
    keys.forEach((value) => {
      params = filter[value] !== null && filter[value] !== '' ? params.set(value, filter[value]?.toString()) : params;
    });

    return params;
  }
}

const urlBase = `${environment.API_URL_BACKOFFICE}`;

export class ApiService<U extends MayHaveIdName> extends BaseApiService implements IApiService2<U> {
  public endPoint!: string;
  public auditPath = 'audits';
  protected http!: HttpClient;

  constructor() {
    super();
  }

  get urlBase() {
    return urlBase + '/' + this.endPoint;
  }

  getFilter(filter: GenericApiRequest) {
    if (!filter) {
      filter = {
        size: 10,
      };
    } else if (!filter.size) {
      filter.size = 10;
    }
    return filter;
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<U>> {
    return this.http.get<GenericApiResponse<U>>(this.urlBase, {
      params: this.getParams(this.getFilter(filter)),
    });
  }

  detail(id: string, withoutSlash?: boolean): Observable<U> {
    const options = {
      headers: {
        'Accept-Language': 'es-ES,es;q=0.9,en;q=0.8',
      },
    };
    const urlDetail = `${this.urlBase}${!withoutSlash ? '/' : ''}${id}`;
    return this.http.get<U>(urlDetail, options);
  }

  post(data: U): Observable<UpdateResponse<U>> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .request('post', this.urlBase, {
        body: data,
        headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            status: res.status,
            data: res.body,
          };
        })
      );
  }

  publish(data: U): Observable<UpdateResponse<U>> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const url = `${this.urlBase}/${data.identifier || data.id}/publications`;
    return this.http
      .request('post', url, {
        //body: data,
        headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            status: res.status,
            data: res.body,
          };
        })
      );
  }

  patch(data: U): Observable<UpdateResponse<U>> {
    const url = `${this.urlBase}/${data.identifier || data.id}`;
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.request('patch', url, { body: data, headers, observe: 'response', responseType: 'json' }).pipe(
      map((res: HttpResponse<any>) => {
        return {
          status: res.status,
          data: res.body,
        };
      })
    );
  }

  update(data: U): Observable<UpdateResponse<U>> {
    const url = `${this.urlBase}/${data.identifier || data.id}`;
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.request('put', url, { body: data, headers, observe: 'response', responseType: 'json' }).pipe(
      map((res: HttpResponse<any>) => {
        return {
          status: res.status,
          data: res.body,
        };
      })
    );
  }

  delete(data: U): Observable<U> {
    const id = data.id || data.priority || data.department_id;
    const url = `${this.urlBase}/${id}`;
    return this.http.delete<any>(url);
  }

  /**
   * convenience method to retrieve an empty object when we create a new record.
   */
  emptyRecord(): U {
    return {} as U;
  }

  auditParseEndpoint(endpoint?: string) {
    if (!endpoint) {
      endpoint = `${this.urlBase}-audits/search`;
    }

    return endpoint;
  }

  audit({ page = 0, size = 10, ...query }: Partial<AuditQuery>, endpoint?: string): Observable<GenericApiResponse<Audit>> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http
      .request('post', this.auditParseEndpoint(endpoint), {
        params: { page, size },
        body: query,
        headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            content: res.body?.content,
            page: res.body?.page,
          };
        })
      );
  }
}
