import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { UiModalWarningUnChangesService } from 'src/app/modules/common-modal/services/ui-modal-warning-unchanges.service';

export interface CanComponentDeactivate {
  canDeactivate: () => UICommonModalConfig | false;
}

@Injectable({
  providedIn: 'root',
})
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
  constructor(private uiModalWarningUnChangesService: UiModalWarningUnChangesService) {}

  canDeactivate(component: CanComponentDeactivate) {
    const canDeactivate = component.canDeactivate();
    return !canDeactivate || this.uiModalWarningUnChangesService.show(canDeactivate);
  }
}
