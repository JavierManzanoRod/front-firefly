/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import { LocationStrategy } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IApiService2, UpdateResponse } from '@core/base/api.service';
import { httpMessageErrors } from '@core/base/utils-http-errors';
import { MayHaveIdName } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable, of, race, throwError } from 'rxjs';
import { catchError, first, map, switchMap, tap } from 'rxjs/operators';
import { ModalCloneComponent } from 'src/app/modules/common-modal/components/modal-clone/modal-clone.component';
import { ModalPublishComponent } from 'src/app/modules/common-modal/components/modal-publish/modal-publish.component';
import { clone } from 'src/app/shared/utils/utils';
import { ModalCreateComponent } from '../../modules/common-modal/components/modal-create/modal-create.component';
import { ModalDeleteComponent } from '../../modules/common-modal/components/modal-delete/modal-delete.component';
import { ModalSaveComponent } from '../../modules/common-modal/components/modal-save/modal-save.component';
import { ModalStopComponent } from '../../modules/common-modal/components/modal-stop/modal-stop.component';
import { ToastService } from '../../modules/toast/toast.service';

interface Config<T extends MayHaveIdName> {
  apiService: IApiService2;
  methodToApply?: 'PUT' | 'POST';
  idKey?: keyof T;
  captureCancel?: boolean;
}

export enum groupType {
  ADMIN_GROUP = 'admin-group',
  ADMIN_GROUP_TYPE = 'admin-group-type',
  SITE_SUBSITE = 'site-subsite',
  EXPERT = 'expert',
  RULE = 'rule',
  ENTITY_TYPE = 'entity-type',
}

@Injectable()
export class CrudOperationsService<T extends MayHaveIdName = MayHaveIdName> {
  urlList: string | undefined;
  constructor(
    public toast: ToastService,
    private modalService: BsModalService,
    private translate: TranslateService,
    private path: LocationStrategy
  ) {}

  deleteActionModal(
    apiService: IApiService2,
    itemToDelete: T,
    errorMessage: string | ((response: HttpErrorResponse, defaultMessage: string) => string) = 'COMMON_ERRORS.DELETE_ERROR'
  ): Observable<T | HttpErrorResponse> {
    return this.modal(apiService.delete.bind(apiService), ModalDeleteComponent, itemToDelete, errorMessage, 'DELETE').pipe(
      tap((result) => {
        if (result == null || (result && !result.error)) {
          this.toast.success('COMMON_MODALS.DELETE_SUCCESS', undefined, { timeOut: 2500 });
        }
      })
    );
  }

  publishModal(
    apiService: IApiService2,
    item: T,
    errorMessage: string | ((response: HttpErrorResponse, defaultMessage: string) => string) = 'COMMON_ERRORS.DELETE_ERROR'
  ): Observable<T | HttpErrorResponse> {
    if (apiService.publish) {
      return this.modal(apiService.publish.bind(apiService), ModalPublishComponent, item, errorMessage, 'PUBLISH').pipe(
        tap((result) => {
          if (result == null || (result && !result.error)) {
            this.toast.success('COMMON_MODALS.PUBLISH_SUCCESS', undefined, { timeOut: 2500 });
          }
        })
      );
    }
    return of();
  }

  updateOrSaveModal(
    { methodToApply, apiService, idKey = 'id', captureCancel = false }: Config<T>,
    itemToUpdate: T,
    typeOf?:
      | groupType.ADMIN_GROUP
      | groupType.ADMIN_GROUP_TYPE
      | groupType.SITE_SUBSITE
      | groupType.EXPERT
      | groupType.RULE
      | groupType.ENTITY_TYPE,
    errorMessage: string | ((response: HttpErrorResponse, defaultMessage: string) => string) = 'COMMON_ERRORS.SAVE_ERROR',
    isStopModal = false
  ): Observable<UpdateResponse<T>> {
    // eslint-disable-next-line @typescript-eslint/unbound-method
    let method: any = itemToUpdate.id ? apiService.update : apiService.post;
    if (methodToApply) {
      // eslint-disable-next-line @typescript-eslint/unbound-method
      method = methodToApply === 'PUT' ? apiService.update : apiService.post;
    } else if (idKey) {
      // eslint-disable-next-line @typescript-eslint/unbound-method
      method = itemToUpdate[idKey] ? apiService.update : apiService.post;
    }

    function typeOfError409(): string {
      switch (typeOf) {
        case groupType.ADMIN_GROUP:
          return 'COMMON_MODALS.409_ERROR_ADMIN_GROUP';
        case groupType.ADMIN_GROUP_TYPE:
          return 'COMMON_MODALS.409_ERROR_ADMIN_GROUP_TYPE';
        case groupType.SITE_SUBSITE:
          return 'COMMON_MODALS.409_ERROR_SITE';
        case groupType.EXPERT:
          return 'COMMON_MODALS.409_EXPERT';
        case groupType.RULE:
          return 'COMMON_MODALS.409_ERROR';
        case groupType.ENTITY_TYPE:
          return 'COMMON_MODALS.409_ERROR_ENTITY_TYPE';
        default:
          return 'COMMON_ERRORS.SAVE_ERROR';
      }
    }

    const error409 = {
      error: false,
      message: typeOfError409(),
    };

    let modalToShow: any;

    if (isStopModal) {
      modalToShow = ModalStopComponent;
    } else {
      modalToShow = methodToApply === 'PUT' ? ModalSaveComponent : ModalCreateComponent;
    }

    return this.modal(method.bind(apiService), modalToShow, itemToUpdate, errorMessage, methodToApply, error409, captureCancel).pipe(
      tap((resultOperation) => {
        let textToShowToast;
        // Se esta comprobando si viene algo en el objeto no si el status o el data es el se exito
        if (resultOperation && !resultOperation.error) {
          textToShowToast = resultOperation.status === 200 ? 'COMMON_MODALS.PUT_SUCCESS' : 'COMMON_MODALS.POST_SUCCESS';
          this.toast.success(textToShowToast, undefined, { timeOut: 2500 });
        }
      })
    );
  }

  cloneActionModal(
    apiService: IApiService2,
    itemToClone: T,
    errorMessage: string | ((response: HttpErrorResponse, defaultMessage: string) => string) = 'COMMON_ERRORS.CLONE_ERROR',
    urlList?: string
  ): Observable<T | HttpErrorResponse> {
    this.urlList = urlList;
    const cloneItem = clone(itemToClone);
    delete cloneItem.id;
    return this.modal(apiService.post.bind(apiService), ModalCloneComponent, cloneItem, errorMessage).pipe(
      tap((resp: any) => {
        if (resp.status === 201) {
          const routeToCloned = `${this.path.getBaseHref()}/${urlList}/${resp.data.id || resp.data.identifier}`;
          this.toast.success(this.translate.instant('COMMON_MODALS.CLONE_SUCCESS', { item: routeToCloned }), undefined, {
            timeOut: 2500,
          });
        }
      })
    );
  }

  modal(
    method: IApiService2['delete'] | IApiService2['post'] | IApiService2['update'],
    componentToShow:
      | typeof ModalDeleteComponent
      | typeof ModalSaveComponent
      | typeof ModalCreateComponent
      | typeof ModalCloneComponent
      | typeof ModalPublishComponent,
    item: T,
    errorMessage: string | ((response: HttpErrorResponse, defaultMessage: string) => string),
    requestMethod?: string,
    error409?: { error: boolean; message: string },
    captureCancel?: boolean
  ): Observable<any> {
    const newItem = { ...item };
    const initialState = { item: newItem, error409 };
    const modal = this.modalService.show(componentToShow as any, { initialState });
    const content = modal.content as unknown as
      | ModalSaveComponent
      | ModalCreateComponent
      | ModalDeleteComponent
      | ModalStopComponent
      | ModalPublishComponent;
    const confirm = content.confirm.pipe(
      tap(() => (content.loading = true)),
      switchMap(() => method(newItem)),
      tap(() => (content.loading = false)),
      tap(() => modal.hide()),
      catchError((e: HttpErrorResponse) => {
        content.loading = false;
        if (!e.status) {
          const text = httpMessageErrors.find((error) => error.codeStatus === 1);
          if (typeof content !== 'undefined') {
            content.errorMessage = text && text.message ? text.message : 'Undefined error';
          }
        } else if (e.error?.error?.detail?.error_code === 'BUS-404' && typeof errorMessage === 'string') {
          content.errorMessage = errorMessage;
        } else if (e.error?.error?.detail?.error_code === 'BUS-466' && typeof errorMessage === 'string') {
          content.errorMessage = 'COMMON_ERRORS.DELETE_MERCHANDISE';
        } else if (e.error?.error?.detail?.validations?.find((err: any) => err?.code === 'BUS-469') && typeof errorMessage === 'string') {
          content.errorMessage = 'COMMON_ERRORS.BUS-469';
        } else {
          if (errorMessage === 'COMMON_ERRORS.ERROR_CONTENT' || errorMessage === 'COMMON_ERRORS.ERROR_CATEGORY') {
            errorMessage = 'COMMON_ERRORS.DELETE_ERROR';
          }
          const text = httpMessageErrors.find(
            (error) =>
              error.codeStatus === e.status &&
              e.url &&
              e.url.includes(error && error.urlSubstr ? error.urlSubstr : '') &&
              requestMethod === error.requestMethod
          );
          if (e.status === 409) {
            content.error409 = { error: true, message: error409?.message || 'COMMON_ERRORS.SAVE_ERROR' };
            content.errorMessage = 'COMMON_ERRORS.ERROR_409';
          } else if (typeof errorMessage === 'string') {
            content.errorMessage = text && text.message ? text.message : errorMessage;
          }
        }
        if (typeof errorMessage === 'function' && !e.error.description) {
          content.errorMessage = errorMessage(e, content.errorMessage);
        }
        if (e.status === 409 && componentToShow === ModalCloneComponent) {
          confirm
            .pipe(
              first(),
              tap((response) => {
                if (response.status === 201) {
                  const routeToCloned = `${this.path.getBaseHref()}/${this.urlList}/${response.data.id || response.data.identifier}`;
                  this.toast.success(this.translate.instant('COMMON_MODALS.CLONE_SUCCESS', { item: routeToCloned }), undefined, {
                    timeOut: 2500,
                  });
                }
              })
            )
            .subscribe();
        }
        if (e.status !== 409) {
          confirm
            .pipe(
              first(),
              tap((response) => {
                if (response.status === 200) {
                  if (componentToShow === ModalDeleteComponent) {
                    this.toast.success('COMMON_MODALS.DELETE_SUCCESS', undefined, { timeOut: 2500 });
                  } else if (componentToShow === ModalPublishComponent) {
                    this.toast.success('COMMON_MODALS.PUBLISH_SUCCESS', undefined, { timeOut: 2500 });
                  } else if (componentToShow === ModalSaveComponent) {
                    this.toast.success('COMMON_MODALS.PUT_SUCCESS', undefined, { timeOut: 2500 });
                  } else if (componentToShow === ModalCreateComponent) {
                    this.toast.success('COMMON_MODALS.POST_SUCCESS', undefined, { timeOut: 2500 });
                  }
                }
              })
            )
            .subscribe();
        }

        return throwError({ ...e, noShow: true });
      })
    );

    return captureCancel ? race(confirm, content.cancel.pipe(map(() => false))) : confirm;
  }
}
