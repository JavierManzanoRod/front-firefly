import { HttpBackend, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { OAuthService } from 'angular-oauth2-oidc';
import { INGXLoggerMetadata, NgxLoggerLevel, NGXLoggerServerService } from 'ngx-logger';
import { catchError, Observable, of } from 'rxjs';
import { UserService } from '../../shared/services/user.service';
import { FireflyLogInterface, LoggerLevel, LoggerLevelType } from '../models/logger.model';
import packageInfo from '../../../../package.json';

interface FootPrint {
  line_number: number;
  message: string;
  filename: string;
}

@Injectable()
export class LoggerHttpService extends NGXLoggerServerService {
  private currentError!: FootPrint;
  get user() {
    return this.userService.getUser();
  }

  public constructor(private readonly http: HttpBackend, private userService: UserService, private oauthService: OAuthService) {
    super(http);
  }

  public logOnServer(
    url: string,
    log: any,
    options: {
      headers?: HttpHeaders;
      reportProgress?: boolean;
      params?: HttpParams;
      responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
      withCredentials?: boolean;
    }
  ): Observable<any> {
    const { filename, message, line_number } = log;
    // Avoid loop if we are receiving the same error again and again
    if (NgxLoggerLevel[environment.LOG_LEVEL] > log.level || this.isRepeatedError({ filename, message, line_number })) {
      return of();
    }
    this.currentError = { filename, message, line_number };
    return super
      .logOnServer(url, log, {
        ...options,
        headers: new HttpHeaders({ Authorization: `Bearer ${this.oauthService.getAccessToken()}` }),
      })
      .pipe(catchError(() => of()));
  }

  customiseRequestBody(metadata: INGXLoggerMetadata): FireflyLogInterface {
    const [info, ...additionalData] = metadata.additional || [];
    const level = `LEVEL_${metadata.level}` as unknown as LoggerLevelType;
    additionalData.unshift(packageInfo.version);
    return {
      log_level: LoggerLevel[level],
      timestamp: new Date(metadata.timestamp || 0).getTime(),
      filename: metadata.fileName,
      line_number: metadata.lineNumber,
      message: metadata.message,
      http_method: info?.method,
      http_error: info?.status,
      http_url: info?.url,
      http_body: JSON.stringify(info?.body),
      user: (this.user && this.user.preferred_username) || this.user.sub || '',
      additional: JSON.stringify(additionalData),
      // enviroment:
      http_params: info?.params.toString(),
      error_response: JSON.stringify(info?.error),
    } as FireflyLogInterface;
  }

  private isRepeatedError(error: FootPrint) {
    return (
      error?.filename === this.currentError?.filename &&
      error?.line_number === this.currentError?.line_number &&
      error?.message === this.currentError?.message
    );
  }
}
