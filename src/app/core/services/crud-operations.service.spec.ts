import { LocationStrategy } from '@angular/common';
import { IApiService2, UpdateResponse } from '@core/base/api.service';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of, throwError } from 'rxjs';
import { ToastService } from '../../modules/toast/toast.service';

describe('Utils3Service', () => {
  let util: CrudOperationsService;
  const toast = {
    success: () => {},
  };
  let translate: TranslateService;

  beforeEach(() => {
    util = new CrudOperationsService(
      toast as unknown as ToastService,
      {
        show: () => {
          return {
            content: {
              confirm: of({}),
            },
            hide: () => {},
          };
        },
      } as unknown as BsModalService,
      translate as TranslateService,
      null as unknown as LocationStrategy
    );
  });

  describe('DeleteActionModal', () => {
    it('delete action modal', () => {
      const obs = util.deleteActionModal(
        {
          delete: () => {
            return of({});
          },
        } as unknown as IApiService2,
        null as any,
        null as any
      );

      const spy = spyOn(toast, 'success');

      obs.subscribe((a) => {
        expect(spy).toHaveBeenCalled();
      });
    });

    it('delete action modal error', () => {
      const obs = util.deleteActionModal(
        {
          delete: () => {
            return of({});
          },
        } as unknown as IApiService2,
        null as any,
        null as any
      );

      obs.subscribe(
        (a) => {},
        (error) => {
          expect(error).toBeTruthy();
        }
      );
    });

    it('delete action modal error', () => {
      const obs = util.deleteActionModal(
        {
          delete: () => {
            return throwError({
              status: 409,
              message: 'test',
              url: 'geo-zones/scopes',
            });
          },
        } as unknown as IApiService2,
        null as any,
        null as any
      );

      obs.subscribe(
        (a) => {},
        (error) => {
          expect(error).toBeTruthy();
        }
      );
    });
  });

  it('update or save modal update put', () => {
    const obs = util.updateOrSaveModal(
      {
        apiService: {
          update: () => {
            return of({} as unknown as UpdateResponse<any>);
          },
        } as unknown as IApiService2,
        methodToApply: 'PUT',
      },
      {},
      null as any
    );

    const spy = spyOn(toast, 'success');

    obs.subscribe((a) => {
      expect(spy).toHaveBeenCalled();
    });
  });

  it('update or modal save update post', () => {
    const obs = util.updateOrSaveModal(
      {
        apiService: {
          post: () => {
            return of({
              status: 200,
            } as unknown as UpdateResponse<any>);
          },
        } as unknown as IApiService2,
        methodToApply: 'POST',
      },
      {
        id: 1,
      } as unknown as any,
      null as any
    );

    const spy = spyOn(toast, 'success');

    obs.subscribe((a) => {
      expect(spy).toHaveBeenCalled();
    });
  });
});
