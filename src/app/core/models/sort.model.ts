export interface SortApi {
  name: string;
  type: SortApiType;
}

export enum SortApiType {
  desc = 'desc',
  asc = 'asc',
}
