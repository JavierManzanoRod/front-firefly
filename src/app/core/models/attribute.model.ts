export interface Attribute {
  id?: string;
  label: string;
  name: string;
  weight?: number;
  path?: string;
  attribute_label?: string;
  data_type: 'STRING' | 'DOUBLE' | 'BOOLEAN' | 'ENTITY' | 'EMBEDDED' | 'DATE_TIME' | 'VIRTUAL';
  entity_type_id?: string;
  attribute_id?: string;
  label_attribute?: boolean;
  entity_reference?: string;
  entity_type_reference_id?: string;
  entity_reference_name?: string;
  multiple_cardinality?: boolean;
  is_i18n?: boolean;
  tooltip_label?: string;
  attribute_data_type?: string;
  is_sub_entity_type?: boolean;
  attribute?: any;
  is_tree_attribute?: boolean;
  /**
   * virtual field for conditions component
   */
  source?: string;
  value?: any;
}

export interface EntityList {
  id?: string;
  is_define_rules: boolean;
  name: string;
  label?: string;
  attributes?: Attribute[];
}
