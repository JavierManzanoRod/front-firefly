export enum AuditType {
  ALL = '',
  INSERT = 'INSERT',
  UPDATE = 'UPDATE',
  DELETE = 'DELETE',
}

export enum AuditOperator {
  EQUALS = 'EQUALS',
  GREATER_THAN = 'GREATER_THAN',
  GREATER_THAN_OR_EQUALS = 'GREATER_THAN_OR_EQUALS',
  LESS_THAN = 'LESS_THAN',
  LESS_THAN_OR_EQUALS = 'LESS_THAN_OR_EQUALS',
  LIKE = 'LIKE',
  DISTINCT = 'DISTINCT',
}

export interface AuditQueryFilter {
  field: string;
  operator: AuditOperator;
  field_value: string;
}

export interface AuditQuery {
  user: string;
  action: AuditOperator;
  changes_since: string | Date;
  changes_to: string | Date;
  custom_filters: AuditQueryFilter[];
  page: number;
  size: number;
}

export type AuditQueryKeys = keyof AuditQuery;

export interface AuditRevision {
  application_name?: string;
  comment?: string;
  date?: string | Date;
  identifier?: number;
  user?: string;
  site?: string;
}

export interface AuditEntity {
  name?: string;
  identifier?: string;
  id?: string;
}

export interface Audit {
  entity?: AuditEntity;
  operation?: AuditType;
  revision?: AuditRevision;
}
