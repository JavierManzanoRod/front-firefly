export enum LoggerLevel {
  LEVEL_0 = 'TRACE',
  LEVEL_1 = 'DEBUG',
  LEVEL_2 = 'INFO',
  LEVEL_3 = 'LOG',
  LEVEL_4 = 'WARN',
  LEVEL_5 = 'ERROR',
  LEVEL_6 = 'FATAL',
  LEVEL_7 = 'OFF',
}

export type LoggerLevelType = keyof typeof LoggerLevel;

export interface FireflyLogInterface {
  log_level: string;
  timestamp: number;
  filename: string;
  line_number: number;
  message: string;
  http_method: string;
  http_error: number;
  http_url: string;
  http_body: string;
  user: string;
  additional: string;
  enviroment: string;
  http_params: string;
  error_response: string;
}
