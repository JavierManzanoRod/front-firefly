import { GenericApiResponse } from '@model/base-api.model';

export interface AttributeEntityTypeDTO extends AttributeDTO {
  entity_view?: {
    identifier: string;
    name?: string;
    label?: string;
  };
  evaluation_info?: {
    path: string;
  };
  node_type?: string;
}

export interface AttributeDTO {
  identifier?: string;
  label: string;
  name: string;
  weight?: number;
  attribute_label?: string;
  data_type: 'STRING' | 'DOUBLE' | 'BOOLEAN' | 'ENTITY' | 'EMBEDDED' | 'DATE_TIME' | 'VIRTUAL';
  entity_type_id?: string;
  attribute_id?: string;
  is_label_attribute?: boolean;
  entity_type_reference_id?: string;
  entity_type_reference_name?: string;
  is_multiple_cardinality?: boolean;
  is_i18n?: boolean;
  tooltip_label?: string;
  attribute_data_type?: string;
  is_sub_entity_type: boolean | undefined;
  is_tree_attribute: boolean | undefined;
  /**
   * virtual field for conditions component
   */
  source?: string;
  value?: any;
}

export interface EntityTypeDTO {
  identifier?: string;
  name: string;
  label?: string;
  is_master: boolean;
  is_tree_attribute?: boolean;
  is_define_rules?: boolean;
  attributes: AttributeEntityTypeDTO[];
}

export type EntityTypeResponseDTO = GenericApiResponse<EntityTypeDTO>;
