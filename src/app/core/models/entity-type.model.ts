import { GenericApiResponse } from '@model/base-api.model';
import { Attribute } from './attribute.model';

export interface AttributeEntityType extends Attribute {
  entity_view?: {
    id: string;
    name: string;
    label: string;
  };
  evaluation_info?: {
    path: string;
  };
  node_type?: string;
}

export interface EntityType {
  id?: string;
  name: string;
  label?: string;
  is_tree_attribute?: boolean;
  define_rules?: boolean;
  is_master: boolean;
  attributes: AttributeEntityType[];
}

export type EntityTypeResponse = GenericApiResponse<EntityType>;
