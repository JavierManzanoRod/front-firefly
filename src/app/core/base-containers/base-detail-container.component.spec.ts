import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { MenuLeftService } from '../../components/menu-left/services/menu-left.service';
import { ToastModule } from '../../modules/toast/toast.module';
import { CrudOperationsService } from '../services/crud-operations.service';

describe('BaseDetailContainer3', () => {
  let fixture: ComponentFixture<BaseDetailContainerComponent<any>>;
  let component: BaseDetailContainerComponent<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, ToastModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: CrudOperationsService,
          useFactory: () => ({
            deleteActionModal: () => {
              return of({});
            },
            updateOrSaveModal: () => {
              return of({});
            },
          }),
        },
        {
          provide: 'apiService',
          useFactory: () => ({
            list: () => {
              of({
                page: {},
                content: {},
              });
            },
            detail: () => of({}),
          }),
        },
        { provide: MenuLeftService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(BaseDetailContainerComponent as any);
    component = fixture.componentInstance;

    component.router = {
      navigate: () => {},
    } as unknown as Router;

    component.activeRoute = {
      params: of({
        id: 1,
      }),
    } as unknown as ActivatedRoute;
  });

  it('init', () => {
    expect(component).toBeTruthy();
  });

  it('delete action modal', () => {
    const spy = spyOn(component.router, 'navigate');
    component.delete('1');
    expect(spy).toHaveBeenCalled();
  });

  it('handleSubmitForm', () => {
    const spy = spyOn(component.router, 'navigate');
    component.handleSubmitForm({
      priority: 2,
      id: 2,
      department_id: 2,
    });
    component.handleSubmitForm({});
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('load details', () => {
    const spy = spyOn(component.apiService, 'detail').and.returnValue(of({}));
    component.detail$ = of({});
    component.ngOnInit();
    component.detail$.subscribe(() => {});
    expect(spy).toHaveBeenCalled();
  });
});
