import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Page } from '@model/metadata.model';
import { of, throwError } from 'rxjs';
import { CrudOperationsService } from '../services/crud-operations.service';
import { BaseListContainerComponent } from './base-list-container.component';

describe('BaseListaContainer3', () => {
  let fixture: ComponentFixture<BaseListContainerComponent<any>>;
  let component: BaseListContainerComponent<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        {
          provide: CrudOperationsService,
          useFactory: () => ({
            deleteActionModal: () => {
              return of({});
            },
            updateOrSaveModal: () => {
              return of({});
            },
          }),
        },
        {
          provide: 'apiService',
          useFactory: () => ({
            list: () => {
              of({
                page: {},
                content: {},
              });
            },
          }),
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(BaseListContainerComponent as any);
    component = fixture.componentInstance;

    /*
    component.apiService = ({
      list: () => {
        of({
          page: {},
          content: {},
        });
      },
    } as unknown) as IApiService2;

     */
  });

  it('init', () => {
    expect(component).toBeTruthy();
  });

  it('get list with search', () => {
    const spy = spyOn(component.apiService, 'list').and.returnValue(
      of({
        page: null,
        content: null,
      })
    );
    component.search({
      name: 'test',
    });
    component.list$.subscribe(() => {});
    expect(spy).toHaveBeenCalled();
    expect(component.searchName).toEqual('test');
  });

  it('delete', () => {
    const spy = spyOn(component.apiService, 'list').and.returnValue(
      of({
        page: null,
        content: null,
      })
    );
    component.page = {} as unknown as Page;
    component.delete(null);
    expect(spy).toHaveBeenCalled();
  });

  it('pagination', () => {
    const spy = spyOn(component.apiService, 'list').and.returnValue(
      of({
        page: null,
        content: null,
      })
    );
    component.handlePagination({
      size: 12,
      page: 10,
    });
    expect(spy).toHaveBeenCalled();
  });

  it('init with error api', () => {
    const spy = spyOn(component.apiService, 'list').and.returnValue(throwError({}));
    component.ngOnInit();
    component.list$.subscribe();
    expect(spy).toHaveBeenCalled();
    expect(component.page).toBeUndefined();
  });
});
