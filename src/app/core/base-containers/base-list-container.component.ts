import { Component, Inject, OnInit } from '@angular/core';
import { IApiService2 } from '@core/base/api.service';
import { SortApi } from '@core/models/sort.model';
import { MayHaveIdName, Page } from '@model/base-api.model';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { checkDateActive } from 'src/app/shared/utils/utils';
import { CrudOperationsService } from '../services/crud-operations.service';

@Component({
  template: '',
})
export abstract class BaseListContainerComponent<T extends MayHaveIdName> implements OnInit {
  public loading!: boolean;
  public list$!: Observable<T[] | null>;
  public tmpList!: T[] | undefined;
  public searchName!: string;
  public page: Page | undefined;
  public currentData: any;
  public filter: any;

  constructor(public utils: CrudOperationsService, @Inject('apiService') public apiService: IApiService2<T>) {}

  ngOnInit() {
    this.getDataList(null);
  }

  getDataList(filter: any) {
    this.loading = true;
    this.filter = filter;

    this.list$ = this.apiService.list(filter).pipe(
      tap(({ page }) => (this.page = page)),
      tap(({ content }) => (this.tmpList = content)),
      tap(({ content }) => {
        if (content.length === 0) {
          this.utils.toast.warning('COMMON_ERRORS.SEARCH', undefined, { timeOut: 2500 });
        }
      }),
      map(({ content }) => content),
      catchError((err) => {
        if (err.status === 404) {
          this.utils.toast.error('COMMON_ERRORS.SEARCH_ERROR', undefined, { timeOut: 2500 });
        } else if (err.status === 400 || err.status === 403 || err.status === 500 || err.status === 503) {
          this.utils.toast.error('COMMON_ERRORS.INTERNAL_ERROR', undefined, { timeOut: 2500 });
        }
        this.page = undefined;
        return of(null);
      }),
      tap(() => (this.loading = false))
    );
  }

  search(criteria: any) {
    this.searchName = criteria.name;
    this.page = undefined;
    this.filter = {
      ...this.filter,
      ...criteria,
      page: 0,
    };
    this.getDataList(this.filter);
  }

  checkDateActive(item: T) {
    return checkDateActive(item.start_date, item.end_date, item.active);
  }

  delete(item: T) {
    this.utils
      .deleteActionModal(this.apiService, item)
      .pipe(
        tap(() => {
          const dataPage = {
            ...this.filter,
            page: this.page?.page_number,
            size: this.page?.page_size,
          };
          this.getDataList(dataPage);
        })
      )
      .subscribe();
  }

  handlePagination($event: any) {
    if ($event.size) {
      const dataToSearch = {
        ...this.filter,
        page: $event.page - 1,
        size: $event.size,
      };
      this.getDataList(dataToSearch);
    }
  }

  sort(sortItem: SortApi) {
    this.search({
      sort: sortItem.name + ',' + sortItem.type,
    });
  }
}
