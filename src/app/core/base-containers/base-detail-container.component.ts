import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { IApiService2 } from '@core/base/api.service';
import { MayHaveIdName } from '@model/base-api.model';
import { Observable, of, throwError } from 'rxjs';
import { catchError, delay, switchMap, tap } from 'rxjs/operators';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { ToastService } from '../../modules/toast/toast.service';
import { CrudOperationsService } from '../services/crud-operations.service';

type DetailElement = {
  haveChanges?: boolean;
};
@Component({
  template: '',
})
export abstract class BaseDetailContainerComponent<T extends MayHaveIdName> implements OnInit {
  @Output() changed = new EventEmitter();
  @Output() deleted = new EventEmitter();
  @Output() cloned = new EventEmitter();
  public detailElement?: DetailElement;
  public urlListado!: string;
  public id!: string;
  public name!: string | undefined;
  public detail$!: Observable<T>;
  public detail!: T | undefined;
  public loading = true;
  protected trackDetail!: boolean;

  constructor(
    public crudSrv: CrudOperationsService,
    public menuLeftSrv: MenuLeftService,
    public activeRoute: ActivatedRoute,
    public router: Router,
    @Inject('apiService') public apiService: IApiService2<T>,
    public toastService: ToastService
  ) {}

  ngOnInit() {
    this.activeRoute.params.subscribe(({ id }) => {
      this.id = id;
    });
    this.detail$ = this.activeRoute.params.pipe(
      tap(() => (this.loading = true)),
      switchMap(({ id }: Params) => {
        if (id) {
          return this.apiService.detail(id);
        } else {
          return of(this.apiService.emptyRecord ? this.apiService.emptyRecord() : undefined).pipe(delay(250));
        }
      }),
      tap(() => (this.loading = false)),
      tap((detail) => {
        this.detail = detail;
        this.name = detail && detail.name;
      }),
      tap((detail: any) => {
        if (this.trackDetail) {
          this.detail = detail;
        }
      }),
      catchError((err) => {
        this.router.navigateByUrl(this.urlListado).then(() => {
          this.toastService.error('COMMON_ERRORS.NOT_FOUND_DESCRIPTION', 'COMMON_ERRORS.NOT_FOUND_TITLE');
        });
        return throwError(err);
      })
    );
  }

  delete(itemToDelete: T | undefined) {
    if (itemToDelete) {
      this.crudSrv
        .deleteActionModal(this.apiService, itemToDelete)
        .pipe(
          tap(() => {
            this.deleted.emit(itemToDelete);
            if (this.detailElement) {
              this.detailElement.haveChanges = false;
              this.changed.emit(itemToDelete);
            }
          }),
          tap(() => {
            this.router.navigate([this.urlListado]);
          })
        )
        .subscribe();
    }
  }

  handleSubmitForm(itemToSave: T) {
    this.crudSrv
      .updateOrSaveModal(this, itemToSave)
      .pipe(
        tap(() => {
          if (!itemToSave.id && !itemToSave.department_id && !itemToSave.priority) {
            this.changed.emit(itemToSave);
            this.router.navigate([this.urlListado]);
          }
        })
      )
      .subscribe();
  }

  clone(itemToClone: T | undefined, urlListado: string) {
    if (itemToClone) {
      this.crudSrv.cloneActionModal(this.apiService, itemToClone, undefined, urlListado).subscribe((response: any) => {
        this.cloned.next(response.data);
      });
    }
  }

  navigateBack() {
    if (this.menuLeftSrv.canBack) {
      window.history.back();
    } else {
      this.router.navigate([this.urlListado]);
    }
  }
}
