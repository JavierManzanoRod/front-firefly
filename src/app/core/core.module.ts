import { NgModule } from '@angular/core';
import { ToastModule } from '../modules/toast/toast.module';
import { CrudOperationsService } from './services/crud-operations.service';

@NgModule({
  imports: [ToastModule],
  declarations: [],
  providers: [CrudOperationsService],
  exports: [],
})
export class CoreModule {}
