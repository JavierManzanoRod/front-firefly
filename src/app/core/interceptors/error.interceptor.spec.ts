import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { NGXLogger } from 'ngx-logger';
import { throwError } from 'rxjs';
import { ToastService } from '../../modules/toast/toast.service';
import { ErrorInterceptor } from './error.interceptor';

describe('ErrorInterceptor', () => {
  let httpRequestSpy;
  let httpHandlerSpy;
  let auth: any;
  let route: any;
  let toast: any;
  let translate: any;
  let logger: any;
  let errorInterceptor: ErrorInterceptor;
  const errorValue = new HttpErrorResponse({ error: 'someerror', status: 499 });

  beforeEach(() => {
    errorInterceptor = new ErrorInterceptor(
      auth as OAuthService,
      route as Router,
      toast as ToastService,
      translate as TranslateService,
      logger as NGXLogger
    );
  });

  it('should auto logout if 499 response returned from api', () => {
    // arrange
    httpRequestSpy = jasmine.createSpyObj('HttpRequest', ['doesNotMatter']);
    httpHandlerSpy = jasmine.createSpyObj('HttpHandler', ['handle']);
    httpHandlerSpy.handle.and.returnValue(throwError(errorValue));
    // act
    errorInterceptor.intercept(httpRequestSpy, httpHandlerSpy).subscribe(
      (result: any) => console.log('good', result),
      (err: any) => {
        expect(true).toBeTruthy();
      }
    );
  });
});
