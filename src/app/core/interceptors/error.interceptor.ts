import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ToastService } from '../../modules/toast/toast.service';
import { TranslateService } from '@ngx-translate/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private oauthService: OAuthService,
    private router: Router,
    private toastService: ToastService,
    private translate: TranslateService,
    private logger: NGXLogger
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    return next.handle(req).pipe(
      catchError((err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.error.content) {
            this.logger.debug(err.message, { ...err, ...req });
          } else {
            this.logger.error(err.message, { ...err, ...req });
          }

          if (
            // err.status 0 only applies for mocks
            err.status === 0 ||
            err.status === 400 ||
            err.status === 403 ||
            err.status === 500 ||
            err.status === 404 ||
            err.status === 503 ||
            err.status === 409
          ) {
            return next.handle(req);
          }
          return this.handleError(err);
        }
        return throwError(err);
      }),
      tap((response) => {
        if (response instanceof HttpResponse) {
          const { body, ...res } = response;
          const url = (response.url && response.url.split('/').pop()) || '';
          if (url.indexOf('.') === -1) {
            this.logger.debug(response.statusText, { ...req, ...res });
          }
        }
      })
    );
  }

  private handleError(err: HttpErrorResponse) {
    const msg = `GENERIC_ERROR.${err.status}`;
    if (err.status === 401) {
      this.toastService.info(this.translate.instant('AUTH.REDIRECT'), '', { disableTimeOut: true });
      setTimeout(() => {
        // wait one second to show properly the toast msg.
        this.oauthService.initCodeFlow(this.router.url);
      }, 1000);
    } else this.showToast(msg);
    return throwError(err);
  }

  private showToast(msg: string) {
    if (!msg) {
      return;
    }

    this.toastService.error(this.translate.instant(msg), '', {
      timeOut: 2500,
    });
  }
}
