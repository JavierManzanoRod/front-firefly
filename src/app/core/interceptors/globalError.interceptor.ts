import { HttpHandler, HttpRequest } from '@angular/common/http';
import { Inject, Injectable, Injector } from '@angular/core';
import { environment } from '@env/environment';
import { NGXLogger } from 'ngx-logger';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ToastService } from 'src/app/modules/toast/toast.service';

@Injectable()
export class GlobalErrorHandler {
  //Avoid circular dependency, cannnot inject service directly
  private get toastService() {
    return this.injector.get(ToastService);
  }
  constructor(@Inject(Injector) private readonly injector: Injector, private logger: NGXLogger) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    return next.handle(req).pipe(
      catchError((err: any) => {
        this.handleError(err);
        return throwError(err);
      })
    );
  }

  handleError(error: any): void {
    const chunkFailedMessage = 'ChunkLoadError';
    if (error?.message?.indexOf(chunkFailedMessage) !== -1) {
      window.location.reload();
    }
    if (!environment.production && !error.noShow) {
      this.toastService.error('COMMON.ERROR_APP_DESC', 'COMMON.ERROR_APP');
    }
    this.logger.error(error?.stack || error.message);
  }
}
