import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { OAuthService } from 'angular-oauth2-oidc';
import { CategoryRule } from '../../rule-engine/category-rule/models/category-rule.model';
import { CategoryRuleService } from '../../rule-engine/category-rule/services/category-rule.service';
import { TokenInterceptor } from './token.interceptor';

describe(`TokenInterceptor`, () => {
  let service: CategoryRuleService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CategoryRuleService,
        {
          provide: OAuthService,
          useValue: {
            getAccessToken: () => '',
          },
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true,
        },
      ],
    });

    service = TestBed.inject(CategoryRuleService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should add an Authorization header', () => {
    service.delete({ id: 'someId' } as CategoryRule).subscribe((response) => {
      expect(response).toBeTruthy();
    });

    const httpRequest = httpMock.expectOne((s) => true);
    expect(httpRequest.request.headers.has('Authorization')).toEqual(true);
  });
});
