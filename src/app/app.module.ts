import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorInterceptor } from '@core/interceptors/error.interceptor';
import { GlobalErrorHandler } from '@core/interceptors/globalError.interceptor';
import { TokenInterceptor } from '@core/interceptors/token.interceptor';
import { LoggerHttpService } from '@core/services/logger-http.service';
import { PATH_ASSETS } from '@env/custom-variables';
import { environment } from '@env/environment';
// import ngx-translate and the http loader
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { OAuthModule } from 'angular-oauth2-oidc';
// Bootstrap
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
import { LoggerModule, NgxLoggerLevel, TOKEN_LOGGER_SERVER_SERVICE } from 'ngx-logger';
import { DefaultLayoutComponent } from './app-layouts/default-layout/default-layout.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuLeftModule } from './components/menu-left/menu-left.module';
import { MenuTopModule } from './components/menu-top/menu-top.module';
import { EvaluateRulesModule } from './rule-query/evaluate-rules/evaluate-rules.module';
import { SatisfiedRulesModule } from './rule-query/satisfied-rules/satisfied-rules.module';
@NgModule({
  declarations: [DefaultLayoutComponent, AppComponent],
  exports: [CollapseModule],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    // ngx-translate and the loader module
    HttpClientModule,
    MenuTopModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactoryWithPath,
        deps: [HttpClient],
      },
    }),
    OAuthModule.forRoot(),
    AppRoutingModule,
    MenuLeftModule,
    // Bootstrap
    CollapseModule,
    SatisfiedRulesModule,
    EvaluateRulesModule,
    LoggerModule.forRoot(
      {
        serverLoggingUrl: environment.production
          ? `${environment.API_URL_BACKOFFICE}/systems/backoffice-els-logger/${environment.API_PATH_LOGGER_VERSION}logs`
          : undefined,
        level: NgxLoggerLevel.DEBUG,
        serverLogLevel: NgxLoggerLevel[environment.LOG_LEVEL],
        // enableSourceMaps: true,
      },
      {
        serverProvider: {
          provide: TOKEN_LOGGER_SERVER_SERVICE,
          useClass: LoggerHttpService,
        },
      }
    ),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    },
    {
      provide: LOCALE_ID,
      useValue: 'es',
    },
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        coreLibraryLoader: () => import('highlight.js/lib/core' as any),
        lineNumbersLoader: () => import('highlightjs-line-numbers.js' as any), // Optional, only if you want the line numbers
        languages: {
          typescript: () => import('highlight.js/lib/languages/typescript' as any),
          scss: () => import('highlight.js/lib/languages/scss' as any),
          xml: () => import('highlight.js/lib/languages/xml' as any),
        },
      },
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

// required for AOT compilationn
export function HttpLoaderFactoryWithPath(http: HttpClient) {
  return new TranslateHttpLoader(http, PATH_ASSETS + 'assets/i18n/');
}
