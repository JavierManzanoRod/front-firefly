import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { Pagination2Module } from '../pagination-2/pagination-2.module';
import { SearchNoneModule } from '../search-none/search-none.module';
import { SearchMultipleModule } from './../search-multiple/search-multiple.module';
import { SearchSimpleModule } from './../search-simple/search-simple.module';
import { GenericListComponent } from './generic-list.component';

@NgModule({
  declarations: [GenericListComponent],
  imports: [CommonModule, TranslateModule, RouterModule, Pagination2Module, SearchSimpleModule, SearchMultipleModule, SearchNoneModule],
  exports: [GenericListComponent],
})
export class GenericListComponentModule {}
