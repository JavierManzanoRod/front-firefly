/* eslint-disable @typescript-eslint/member-ordering */
import { ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { SortApi, SortApiType } from '@core/models/sort.model';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { AdminGroupTypeMaster } from 'src/app/configuration/models/configuration.model';
import { FF_ALL_ADMIN_GROUP_TYPE } from 'src/app/configuration/tokens/admin-group-type.token';
import { BaseListComponent } from 'src/app/shared/components/base-component-list.component';
import { ConfigMultiple } from '../search-multiple/models/config.models';
import { GenericListConfig, TypeSearch } from './models/generic-list.model';
import { SearchProvider } from 'src/app/shared/components/list-search/providers/search-provider';

@Component({
  selector: 'ff-generic-list',
  templateUrl: './generic-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./generic-list.component.scss'],
})
export class GenericListComponent<T = any> extends BaseListComponent<any> {
  // Configuracion para la tabla de contenido
  @Input() arrayKeys!: GenericListConfig[];
  // Configuracion para las rutas de detail
  @Input() route!: string;
  // Configuracion para formar las claves de traduccion prefix + key (se puede usar headerName)
  @Input() prefix!: string;
  // Flag para mostrar o no la paginacion
  @Input() set showPagination(show) {
    this._showPagination = show;
  }
  get showPagination() {
    return this._showPagination && this.page && this.page.total_pages > 1;
  }
  // Flag para mostrar o no icono de borrar
  @Input() canDelete = true;
  // Flag para mostrar o no icono de detalle
  @Input() canView = true;
  @Input() canHistory = false;
  // Configuracion para añadir buscador (simple , multiple o ninguno)
  // Teneis ejemplos en seller-list-container (buscador multiple) y target-list-container (buscador simple)
  @Input() type: TypeSearch | null = null;
  // Configuracion para el search multiple , donde se le pasa key , tipo y class entre otros
  @Input() configMultiple: ConfigMultiple[] = [];
  // Configuracion para la clave de traduccion del titulo de busqueda simple
  @Input() title = 'name';
  // Configuracion opcional para la key del campo de busqueda simple
  @Input() key = 'name';
  // Configuracion opcional para la key del placeholder del campo de busqueda simple
  @Input() placeHolder = 'COMMON.PLACEHOLDER_NAME';
  // Configuracion de columna activa
  @Input() sortActive = 'name';
  // flag para mostrar el ícono de view o el de pencil
  @Input() isViewOnly = false;

  @Input() viewFilter: ((item: T) => boolean) | undefined;
  @Input() routerFilter: ((item: T, index?: number) => void) | undefined;
  @Input() historyFilter: ((item: T, index?: number) => void) | undefined;
  @Input() checkDateActive: ((item: T) => boolean) | undefined;

  @Input() stickyHeader = false;
  @Input() valueSearch: any = '';
  @Input() customGoDetail = false;
  // Evento search enviado desde el search simple o multiple
  @Output() search = new EventEmitter<any>();
  // Evento search enviado desde el encabezado para ordenar
  @Output() sortEvent = new EventEmitter<SortApi>();
  // Make a variable reference to our Enum
  enumType = TypeSearch;
  _showPagination = true;

  // Funcion para crear la ruta de detalle para cada fila por defecto ruta/id
  @Input() routerData = (e: any) => this.defaultRoute(e);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  keysOrder: any = { [this.sortActive]: true };
  // eslint-disable-next-line @typescript-eslint/member-ordering

  // eslint-disable-next-line @typescript-eslint/member-ordering
  constructor(
    private router: Router,
    @Inject(FF_ALL_ADMIN_GROUP_TYPE) public adminGroupsType: { [key in AdminGroupTypeMaster]: string },
    private providerSearch: SearchProvider
  ) {
    super();
  }

  searchEvent(event: any) {
    this.currentPage = 0;
    this.search.emit(event);
  }
  canViewWithFilter(item: T) {
    if (this.viewFilter) {
      return this.viewFilter(item);
    }
    return true;
  }

  handlerRouter(item: any, index: number) {
    if (this.routerFilter) {
      return this.routerFilter(item, index);
    }
    if (this.customGoDetail) {
      this.go(item);
    } else this.router.navigateByUrl(this.routerData(item));
  }

  go(rule: any) {
    const { admin_group_type_id, id } = rule;

    let urlBase;

    switch (admin_group_type_id) {
      case this.adminGroupsType.badge:
        urlBase = '/control-panel/badge/view/';
        break;
      case this.adminGroupsType.contentGroup:
        urlBase = '/catalog/content-group/rule/';
        break;
      case this.adminGroupsType.excludeSearch:
        urlBase = '/stock/exclude-search/view';
        break;
      case this.adminGroupsType.expert:
        urlBase = '/control-panel/expert-admin/view/';
        break;
      case this.adminGroupsType.fluorGases:
        urlBase = '/control-panel/fluorinated-gas/view/';
        break;
      case this.adminGroupsType.limitSale:
        urlBase = '/stock/limit-sale/view';
        break;
      case this.adminGroupsType.loyalty:
        urlBase = '/control-panel/loyalty/view/';
        break;
      case this.adminGroupsType.priceInheritance:
        urlBase = '/rule-engine/price-inheritance/view/';
        break;
      case this.adminGroupsType.sizeGuide:
        urlBase = '/control-panel/size-guide/view/';
        break;
      case this.adminGroupsType.specialProducts:
        urlBase = '/control-panel/special-product/view/';
        break;
      default:
        urlBase = '/rule-engine/admin-group/view';
        break;
    }

    this.providerSearch.data = this.valueSearch;
    // excepcion para content group
    if (admin_group_type_id === this.adminGroupsType.contentGroup) {
      this.router.navigate([`${urlBase}/${rule.folder_id}/${id}`], {
        state: { goBack: true, backMessage: 'SPENTER.BREAD_CRUMB_TITLE' },
      });
    } else {
      this.router.navigate([`${urlBase}/${id}`], { state: { goBack: true, backMessage: 'SPENTER.BREAD_CRUMB_TITLE' } });
    }
  }

  handleHistory(item: any, index: number) {
    if (this.historyFilter) {
      return this.historyFilter(item, index);
    }
  }

  dataByString(itemdata: any, itemKey: GenericListConfig) {
    let result = itemdata;
    let key = itemKey.key;

    key = key.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    key = key.replace(/^\./, ''); // strip a leading dot
    const a = key.split('.');

    for (let i = 0, n = a.length; i < n; ++i) {
      const k = a[i];
      if (k in result) {
        result = result[k];
      } else {
        return;
      }
    }

    if (itemKey.customFormat) {
      return itemKey.customFormat(itemdata);
    }

    return result as string;
  }

  handlePageChanged(event: PageChangedEvent) {
    const { page, itemsPerPage } = event;
    if (page !== this.currentPage) {
      this.currentPage = page;
      const data = {
        ...this.currentData,
        page,
        size: itemsPerPage,
      };
      this.pagination.emit(data);
    }
  }

  defaultRoute(item: any): string {
    // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
    return `${this.route}/${item.id}`.replace(/\/\//g, '/');
  }

  sort(name: string) {
    const temp = !this.keysOrder[name];

    this.sortActive = name;
    this.keysOrder = {
      [name]: temp,
    };
    if (name) {
      this.currentPage = 1;
    }
    this.sortEvent.emit({
      name,
      type: this.keysOrder[name] ? SortApiType.asc : SortApiType.desc,
    });
  }
}
