export interface GenericListConfig {
  key: string;
  headerName?: string; // Añadir la clave de la traduccion para el campo
  textCenter?: boolean;
  showOrder?: boolean;
  width?: string;
  language?: boolean; // Marcar si pones el language directo o buscas las traducion en LANGUAGES.
  canActiveClass?: boolean; // Marcar si se quiere que la key este en verde o rojo segun el valor booleano del dato
  canFormatDate?: boolean; // Marcar si se quiere formatear la fecha con el pipe date
  formatDate?: string; // Formato de la fecha por defecto esta dd-MM-yyyy
  customContent?: boolean; // Marcar deseas mostrar iconos de CRUD (creado, modificado, eliminado)
  customFormat?: (itemData: any) => string;
}

export enum TypeSearch {
  'multiple' = 'multiple',
  'simple' = 'simple',
  'none' = 'none',
}
