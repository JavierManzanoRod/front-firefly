import { Injectable } from '@angular/core';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { UiTreeClickEvent } from '../ui-tree-list/models/ui-tree-list.model';
import { UiEntityTypesSelectorsMultipleReadOnlyComponent } from './components/ui-entity-types-selectors-multiple-readonly/ui-entity-types-selectors-multiple-readonly.component';
import { UiEntityTypesSelectorsMultipleComponent } from './components/ui-entity-types-selectors-multiple/ui-entity-types-selectors-multiple.component';
import { UiEntityTypesSelectorsMultiple } from './components/ui-entity-types-selectors-multiple/ui-entity-types-selectors-multiple.model';
import { UiEntityTypesSelectorsSingleComponent } from './components/ui-entity-types-selectors-single/ui-entity-types-selectors-single.component';
import { UiEntityTypesSelectorsSingle } from './components/ui-entity-types-selectors-single/ui-entity-types-selectors-single.model';

@Injectable()
export class UiEntityTypesSelectorsService {
  constructor(public modalService: BsModalService, public entityTypeService: EntityTypeService) {}

  showSelectSigleAttributeByEntityTypeId(config: UiEntityTypesSelectorsSingle): Observable<UiTreeClickEvent<AttributeEntityType>> {
    const modal = this.modalService.show(UiEntityTypesSelectorsSingleComponent as any, {
      initialState: { config },
    });

    const content = modal.content as UiEntityTypesSelectorsSingleComponent;
    return content.selected.asObservable();
  }

  showSelectMultipleAttributeReadOnly(config: UiEntityTypesSelectorsMultiple) {
    const modal = this.modalService.show(UiEntityTypesSelectorsMultipleReadOnlyComponent as any, {
      initialState: { config },
    });

    const content = modal.content as UiEntityTypesSelectorsMultipleReadOnlyComponent;
    return content.selected.asObservable() as Observable<AttributeEntityType[]>;
  }

  showSelectMultipleAttribute(config: UiEntityTypesSelectorsMultiple) {
    const modal = this.modalService.show(UiEntityTypesSelectorsMultipleComponent as any, {
      initialState: { config },
      class: 'modal-center modal-lg modal-w-100',
    });

    const content = modal.content as UiEntityTypesSelectorsMultipleComponent;
    return content.selected.asObservable() as Observable<AttributeEntityType[]>;
  }
}
