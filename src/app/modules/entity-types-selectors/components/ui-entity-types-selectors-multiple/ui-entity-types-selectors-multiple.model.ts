import { AttributeEntityType } from '@core/models/entity-type.model';

export interface UiEntityTypesSelectorsMultiple {
  // Items ya seleccionados
  selectedItems?: UiEntityTypesSelectorsMultipleItem[];
  // Texto boton cancelar
  cancelBtnText?: string;
  // Texto boton ok
  okBtnText?: string;
  // Titulo de la modal
  titleText?: string;
}

export interface UiEntityTypesSelectorsMultipleItem extends AttributeEntityType {
  parents: AttributeEntityType[];
}
