import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { UiTreeClickEvent, UITreeListItem } from '../../../ui-tree-list/models/ui-tree-list.model';
import { UiEntityTypesSelectorsMultiple, UiEntityTypesSelectorsMultipleItem } from './ui-entity-types-selectors-multiple.model';
import { UiEntityTypesSelectorsMultipleService } from './ui-entity-types-selectors-multiple.service';

@Component({
  templateUrl: './ui-entity-types-selectors-multiple.component.html',
  styleUrls: ['./ui-entity-types-selectors-multiple.component.scss'],
  providers: [UiEntityTypesSelectorsMultipleService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UiEntityTypesSelectorsMultipleComponent implements OnInit {
  @Output() selected: EventEmitter<UiEntityTypesSelectorsMultipleItem[]> = new EventEmitter<UiEntityTypesSelectorsMultipleItem[]>();

  config: UiEntityTypesSelectorsMultiple = {};

  loading = false;
  filter = '';
  items: UITreeListItem[] = [];

  selectedItems: UiEntityTypesSelectorsMultipleItem[] = [];
  selectedItemsTree: UITreeListItem[] = [];

  get selectedItemsIds() {
    return this.selectedItems.map((item) => item.id);
  }

  get treeState(): [string, boolean][] {
    return this.selectedItemsTree.map((item) => [item.value.id, item.isOpen ?? true]);
  }

  get validSearchParam() {
    return this.filter.length > 2;
  }

  constructor(
    public changeDetectorRef: ChangeDetectorRef,
    public modalRef: BsModalRef,
    public modalService: BsModalService,
    public entityTypeService: EntityTypeService,
    private uiModalSelectMultipleEntityAttributesService: UiEntityTypesSelectorsMultipleService
  ) {}

  ngOnInit() {
    this.selectedItems = Array.isArray(this.config.selectedItems) ? Array.from(this.config.selectedItems) : [];
    this.selectedItemsTree = this.transformSelectedItemsToTree();
  }

  transformSelectedItemsToTree() {
    const tree: UITreeListItem[] = [];
    let parent: any;
    const treeState = this.treeState;
    this.selectedItems.forEach((item) => {
      parent = this.findInTree(tree, item.parents[item.parents.length - 1]);
      if (!parent) {
        const parentId = item.parents[0].id;
        const state = treeState.find((t) => t[0] === parentId);
        tree.push({
          value: item.parents[item.parents.length - 1],
          icon: 'icon-icon-atributos-1',
          isOpen: state ? state[1] : true,
          childrens: [
            {
              value: item,
              icon: 'icon-icon-agrupaciones',
            },
          ],
        });
      } else {
        if (!parent.childrens) {
          parent.childrens = [];
        }
        parent.childrens.push({
          value: item,
          icon: 'icon-icon-agrupaciones',
        });
      }
    });
    return tree;
  }

  findInTree(tree: UITreeListItem[], toFind: AttributeEntityType): UITreeListItem | null {
    let treeItem: UITreeListItem | null = null;
    let item;
    for (let i = 0, l = tree.length; i < l; i++) {
      item = tree[i];
      if (item.value.id === toFind.id) {
        treeItem = item;
        break;
      }
    }
    return treeItem;
  }

  filterItems() {
    if (!this.validSearchParam) {
      return;
    }
    this.loading = true;
    this.items = [];
    this.changeDetectorRef.detectChanges();
    this.uiModalSelectMultipleEntityAttributesService.search(this.filter).subscribe(
      (items: UITreeListItem[]) => {
        this.items = items;
        this.loading = false;
        this.changeDetectorRef.detectChanges();
        this.changeDetectorRef.markForCheck();
      },
      () => (this.loading = false)
    );
  }

  clickedItem(event: UiTreeClickEvent<AttributeEntityType>) {
    if (Object.prototype.hasOwnProperty.call(event.selectedItem, 'data_type')) {
      const index = this.selectedItemsIds.indexOf(event.selectedItem.id);
      if (index < 0) {
        this.selectedItems.push(this.parseEventToItem(event));
      } else {
        this.selectedItems.splice(index, 1);
      }
    }
    this.selectedItemsTree = this.transformSelectedItemsToTree();
  }

  removeItem(selectedItem: any) {
    this.removeItemAndHisChildrens(selectedItem);
    this.selectedItemsTree = this.transformSelectedItemsToTree();
    this.changeDetectorRef.detectChanges();
  }

  removeItemAndHisChildrens(selectedItem: any) {
    if (selectedItem.childrens) {
      selectedItem.childrens.forEach((item: any) => {
        this.removeItemAndHisChildrens(item);
      });
    }
    const index = this.selectedItemsIds.indexOf(selectedItem.value.id);
    if (index !== -1) {
      this.selectedItems.splice(index, 1);
    }
  }

  parseEventToItem(event: UiTreeClickEvent<AttributeEntityType>): UiEntityTypesSelectorsMultipleItem {
    const item: UiEntityTypesSelectorsMultipleItem = event.selectedItem as UiEntityTypesSelectorsMultipleItem;
    event.path.pop();
    item.parents = event.path.map((parent: any) => {
      return parent.item;
    });
    return item;
  }

  selectAll() {
    if (!this.items.length) {
      return;
    }
    const allChildrens: any[] = [];
    this.items.forEach((item) => {
      const childrens = item.childrens?.map((i) => ({ ...i.value, parents: [item.value] }));
      allChildrens.push(...(childrens as []));
    });
    allChildrens.forEach((item) => {
      if (!this.selectedItemsIds.includes(item.id)) {
        this.selectedItems.push(item);
      }
    });
    this.selectedItems.sort((a, b) => (a.label < b.label ? -1 : 1));
    this.selectedItemsTree = this.transformSelectedItemsToTree();
  }

  selectNone() {
    this.selectedItems = [];
    this.selectedItemsTree = this.transformSelectedItemsToTree();
  }

  handleSelect() {
    this.selected.emit(this.selectedItems);
    this.closeModal();
  }

  handleCancel() {
    this.selected.emit(this.config.selectedItems);
    this.closeModal();
  }

  private closeModal() {
    this.modalRef.hide();
  }
}
