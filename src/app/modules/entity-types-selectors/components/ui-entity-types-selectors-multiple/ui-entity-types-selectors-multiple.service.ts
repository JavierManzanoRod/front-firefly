import { Injectable } from '@angular/core';
import { AttributeEntityType, EntityType } from '@core/models/entity-type.model';
import { combineLatest, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { UITreeListItem } from '../../../ui-tree-list/models/ui-tree-list.model';

@Injectable()
export class UiEntityTypesSelectorsMultipleService {
  constructor(private entityTypeService: EntityTypeService) {}

  search(name: string): Observable<UITreeListItem[]> {
    return this.entityTypeService.search(name, { size: 999 }).pipe(
      switchMap((entityTypes) => {
        return entityTypes.length ? combineLatest(entityTypes.map((entityType) => this.getTreeListItemFromEntityType(entityType))) : of([]);
      })
    );
  }

  private getTreeListItemFromEntityType(entityType: string | EntityType): Observable<UITreeListItem> {
    if (typeof entityType === 'string') {
      return this.entityTypeService.detail(entityType).pipe(
        map((data) => {
          return this.parseEntityTypeToTreeListItem(data);
        })
      );
    } else {
      return of(this.parseEntityTypeToTreeListItem(entityType));
    }
  }

  private parseEntityTypeToTreeListItem(entityType: EntityType): UITreeListItem {
    return {
      value: entityType,
      icon: 'icon-icon-atributos-1',
      childrens: entityType.attributes.map((attribute) => {
        return this.parseAttributeToTreeListItem(attribute);
      }),
    };
  }

  private parseAttributeToTreeListItem(attribute: AttributeEntityType): UITreeListItem {
    const result: UITreeListItem = {
      value: attribute,
      icon: 'icon-icon-agrupaciones',
    };
    return result;
  }
}
