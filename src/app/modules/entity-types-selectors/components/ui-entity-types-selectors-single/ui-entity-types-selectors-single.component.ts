import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { UiTreeClickEvent, UITreeListItem } from '../../../ui-tree-list/models/ui-tree-list.model';

@Component({
  templateUrl: './ui-entity-types-selectors-single.component.html',
  styleUrls: ['./ui-entity-types-selectors-single.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UiEntityTypesSelectorsSingleComponent implements OnInit {
  @Input() config: any = {};
  @Output() selected: EventEmitter<UiTreeClickEvent<AttributeEntityType>> = new EventEmitter<UiTreeClickEvent<AttributeEntityType>>();

  loading = false;
  items: UITreeListItem[] = [];

  selectedItem!: AttributeEntityType | null;
  selectedEvent!: UiTreeClickEvent<AttributeEntityType | null>;

  constructor(
    public changeDetectorRef: ChangeDetectorRef,
    public modalRef: BsModalRef,
    public modalService: BsModalService,
    public entityTypeService: EntityTypeService
  ) {}

  ngOnInit() {
    this.loading = true;
    this.getTreeItems(this.config.id || '').subscribe((items) => {
      this.items = items;
      this.loading = false;
      this.changeDetectorRef.detectChanges();
    });
  }

  getTreeItems(id: string): Observable<UITreeListItem[]> {
    return this.entityTypeService.detail(id).pipe(
      map((entityType) => {
        return [
          {
            value: entityType,
            icon: 'icon-icon-folder-closed',
            isOpen: true,
            childrens: entityType.attributes.map((item) => {
              return this.getAttributeForTree(item);
            }),
          },
        ];
      })
    );
  }

  getAttributeForTree(attribute: AttributeEntityType): UITreeListItem {
    const result: UITreeListItem = {
      value: attribute,
      icon: 'icon-icon-atributos',
    };
    if (attribute.data_type === 'EMBEDDED' || attribute.data_type === 'ENTITY') {
      result.icon = 'icon-icon-folder-closed';
      result.loadChildrens = (value) => {
        return this.getTreeItemsAttibute(value.entity_type_id || value.entity_reference || value.entity_view.id);
      };
    }
    return result;
  }

  getTreeItemsAttibute(id: string): Observable<UITreeListItem[]> {
    return this.entityTypeService.detail(id).pipe(
      map((entityType) => {
        return entityType.attributes.map((item) => {
          return this.getAttributeForTree(item);
        });
      })
    );
  }

  filterItems(event: any) {
    let name;
    this.items[0].childrens?.forEach((item) => {
      // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
      name = item.value.name.toLowerCase().trim() + item.value.label.toLowerCase().trim();
      item.hide = name.indexOf(event.toLowerCase().trim()) < 0;
      if (!event) {
        item.hide = false;
      }
    });
  }

  clickedItem(event: UiTreeClickEvent<AttributeEntityType>) {
    this.selectedItem = null;
    if (event.selectedItem.data_type !== 'ENTITY' && event.selectedItem.data_type !== 'EMBEDDED') {
      this.selectedItem = event.selectedItem;
      this.selectedEvent = event;
    }
  }

  handleSelect() {
    this.selected.emit(this.selectedEvent as any);
    this.closeModal();
  }

  handleCancel() {
    this.selected.emit(undefined);
    this.closeModal();
  }

  private closeModal() {
    this.modalRef.hide();
  }
}
