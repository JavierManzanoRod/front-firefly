interface UiEntityTypesSelectorsSingle1 {
  id: string;
}

export type UiEntityTypesSelectorsSingle = Readonly<Partial<UiEntityTypesSelectorsSingle1>>;
