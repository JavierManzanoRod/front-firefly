import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { UITreeListItem } from '../../../ui-tree-list/models/ui-tree-list.model';
import {
  UiEntityTypesSelectorsMultiple,
  UiEntityTypesSelectorsMultipleItem,
} from '../ui-entity-types-selectors-multiple/ui-entity-types-selectors-multiple.model';

@Component({
  templateUrl: './ui-entity-types-selectors-multiple-readonly.component.html',
  styleUrls: ['./ui-entity-types-selectors-multiple-readonly.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UiEntityTypesSelectorsMultipleReadOnlyComponent implements OnInit {
  @Output() selected: EventEmitter<UiEntityTypesSelectorsMultipleItem[]> = new EventEmitter<UiEntityTypesSelectorsMultipleItem[]>();

  config: UiEntityTypesSelectorsMultiple = {};

  loading = false;
  filter = '';
  items: UITreeListItem[] = [];

  selectedItems: UiEntityTypesSelectorsMultipleItem[] = [];
  selectedItemsTree: UITreeListItem[] = [];

  constructor(
    public changeDetectorRef: ChangeDetectorRef,
    public modalRef: BsModalRef,
    public modalService: BsModalService,
    public entityTypeService: EntityTypeService
  ) {}

  get selectedItemsIds() {
    return this.selectedItems.map((item) => item.id);
  }

  get treeState(): [string, boolean][] {
    return this.selectedItemsTree.map((item) => [item.value.id, item.isOpen ?? true]);
  }

  get validSearchParam() {
    return this.filter.length > 2;
  }

  ngOnInit() {
    this.selectedItems = Array.isArray(this.config.selectedItems) ? Array.from(this.config.selectedItems) : [];
    this.selectedItemsTree = this.transformSelectedItemsToTree();
  }

  transformSelectedItemsToTree() {
    const tree: UITreeListItem[] = [];
    let parent: any;
    const treeState = this.treeState;
    this.selectedItems.forEach((item) => {
      parent = this.findInTree(tree, item.parents[item.parents.length - 1]);
      if (!parent) {
        const parentId = item.parents[0].id;
        const state = treeState.find((t) => t[0] === parentId);
        tree.push({
          value: item.parents[item.parents.length - 1],
          icon: 'icon-icon-atributos-1',
          isOpen: state ? state[1] : true,
          childrens: [
            {
              value: item,
              icon: 'icon-icon-agrupaciones',
            },
          ],
        });
      } else {
        if (!parent.childrens) {
          parent.childrens = [];
        }
        parent.childrens.push({
          value: item,
          icon: 'icon-icon-agrupaciones',
        });
      }
    });
    return tree;
  }

  findInTree(tree: UITreeListItem[], toFind: AttributeEntityType): UITreeListItem | null {
    let treeItem: UITreeListItem | null = null;
    let item;
    for (let i = 0, l = tree.length; i < l; i++) {
      item = tree[i];
      if (item.value.id === toFind.id) {
        treeItem = item;
        break;
      }
    }
    return treeItem;
  }

  handleCancel() {
    this.selected.emit(this.config.selectedItems);
    this.closeModal();
  }

  private closeModal() {
    this.modalRef.hide();
  }
}
