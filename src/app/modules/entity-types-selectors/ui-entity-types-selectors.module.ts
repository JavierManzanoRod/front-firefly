import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from '../common-modal/common-modal.module';
import { UiTreeListModule } from '../ui-tree-list/ui-tree-list.module';
import { UiEntityTypesSelectorsMultipleComponent } from './components/ui-entity-types-selectors-multiple/ui-entity-types-selectors-multiple.component';
import { UiEntityTypesSelectorsMultipleReadOnlyComponent } from './components/ui-entity-types-selectors-multiple-readonly/ui-entity-types-selectors-multiple-readonly.component';
import { UiEntityTypesSelectorsSingleComponent } from './components/ui-entity-types-selectors-single/ui-entity-types-selectors-single.component';
import { UiEntityTypesSelectorsService } from './ui-entity-types-selectors.service';

@NgModule({
  declarations: [
    UiEntityTypesSelectorsSingleComponent,
    UiEntityTypesSelectorsMultipleComponent,
    UiEntityTypesSelectorsMultipleReadOnlyComponent,
  ],
  providers: [UiEntityTypesSelectorsService],
  imports: [CommonModule, UiTreeListModule, CommonModalModule, TranslateModule, FormsModule, SimplebarAngularModule],
  exports: [],
})
export class UiEntityTypesSelectorsModule {}
