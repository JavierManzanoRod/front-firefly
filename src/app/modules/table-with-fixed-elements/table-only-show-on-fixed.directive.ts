import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[ffTableOnlyShowOnFixed]',
})
export class TableOnlyShowOnFixedDirective implements OnInit {
  @Input() ffTableOnlyShowOnFixed!: boolean;

  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    if (this.ffTableOnlyShowOnFixed) {
      this.elementRef.nativeElement.classList.add('only-show-on-fixed');
    }
  }
}
