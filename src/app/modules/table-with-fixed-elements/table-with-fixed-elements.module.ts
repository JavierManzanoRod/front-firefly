import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { TableFixedElementDirective } from './table-fixed-element.directive';
import { TableOnlyShowOnFixedDirective } from './table-only-show-on-fixed.directive';
import { TableWithFixedElementsComponent } from './table-with-fixed-elements.component';
import { TableWithFixedElementsDirective } from './table-with-fixed-elements.directive';

@NgModule({
  imports: [CommonModule, SimplebarAngularModule],
  declarations: [
    TableWithFixedElementsComponent,
    TableWithFixedElementsDirective,
    TableOnlyShowOnFixedDirective,
    TableFixedElementDirective,
  ],
  providers: [],
  exports: [TableWithFixedElementsComponent, TableWithFixedElementsDirective, TableOnlyShowOnFixedDirective, TableFixedElementDirective],
})
export class TableWithFixedElementsModule {}
