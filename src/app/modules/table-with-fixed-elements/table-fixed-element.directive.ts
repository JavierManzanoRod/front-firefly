import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[ffTableFixedElement]',
})
export class TableFixedElementDirective implements OnInit {
  @Input() ffTableFixedElement!: boolean;

  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    if (this.ffTableFixedElement) {
      this.elementRef.nativeElement.classList.add('fixed-side');
    }
  }
}
