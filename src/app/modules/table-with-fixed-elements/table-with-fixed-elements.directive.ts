import { Directive } from '@angular/core';

@Directive({
  selector: '[ffTableWithFixedElements]',
})
export class TableWithFixedElementsDirective {}
