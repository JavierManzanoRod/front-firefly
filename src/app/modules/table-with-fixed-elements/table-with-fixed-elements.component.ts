import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  HostListener,
  OnDestroy,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { SimplebarAngularComponent } from 'simplebar-angular';
import { TableWithFixedElementsDirective } from './table-with-fixed-elements.directive';

@Component({
  selector: 'ff-table-with-fixed-elements',
  templateUrl: './table-with-fixed-elements.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./table-with-fixed-elements.component.scss'],
})
export class TableWithFixedElementsComponent implements AfterViewInit, OnDestroy {
  @ViewChild(SimplebarAngularComponent, { static: true }) simplebar: SimplebarAngularComponent | undefined;
  @ContentChild(TableWithFixedElementsDirective, { read: TemplateRef }) table: any;
  disabled = false;
  isFixed = false;
  interval!: any;

  constructor(private ref: ChangeDetectorRef) {}

  @HostListener('window:resize', ['$event'])
  windowResize() {
    this.onResize();
  }

  ngAfterViewInit() {
    this.onResize();
    this.interval = setInterval(() => {
      this.onResize();
    }, 1000 / 15);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  onResize() {
    this.isFixed = this.simplebar?.SimpleBar.axis.x.isOverflowing;
    this.ref.detectChanges();
  }
}
