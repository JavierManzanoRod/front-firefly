import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ff-search-simple',
  templateUrl: './search-simple.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./search-simple.component.scss'],
})
export class SearchSimpleComponent implements OnInit {
  @Input() key = 'name';
  @Input() title = 'name';
  @Input() placeHolder = 'COMMON.PLACEHOLDER_NAME';
  @Input() numberItems = 0;
  @Input() value = '';
  @Input() loading = false;

  @Input()
  @HostBinding('class.hideResults')
  hideResults = false;

  @Output() search = new EventEmitter<any>();

  form!: FormGroup;
  addSearchStr = this.translate.instant('COMMON.RESULTS_SIMPLE');

  valueSearch = null;
  clear = false;

  constructor(private translate: TranslateService) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      [this.key]: new FormControl(this.value || ''),
    });
    this.clear = !!this.value;
  }

  submit() {
    if (this.clear) {
      this.form.reset();
    }

    this.search.emit(this.form.value);
    this.valueSearch = this.form.value[this.key];
    this.clear = !this.clear;
  }

  valueChange() {
    this.clear = false;
  }
}
