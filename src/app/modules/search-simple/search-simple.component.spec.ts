import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { SearchSimpleComponent } from './search-simple.component';

describe('SearchSimpleComponent', () => {
  let component: SearchSimpleComponent;
  let fixture: ComponentFixture<SearchSimpleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchSimpleComponent],
      imports: [TranslateModule.forRoot()],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
