import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { SearchSimpleComponent } from './search-simple.component';

@NgModule({
  declarations: [SearchSimpleComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  exports: [SearchSimpleComponent],
})
export class SearchSimpleModule {}
