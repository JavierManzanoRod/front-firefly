import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { DateTimeService } from '@core/base/date-time.service';
import { TranslateModule } from '@ngx-translate/core';
import { Conditions2BasicService } from 'src/app/modules/conditions2/services/conditions2-basic.service';
import { ConditionsBasicUtilsService } from '../conditions-basic/services/conditions-basic.sevice';
import { Conditions2UtilsService } from '../conditions2/services/conditions2-utils.service';
import { ToastService } from '../toast/toast.service';
import { ConditionsBasicAndAdvanceComponent } from './conditions-basic-and-advance.component';
import { HttpClientModule } from '@angular/common/http';
import { ExpertService } from 'src/app/control-panel/expert/services/expert.service';
import { BsModalService } from 'ngx-bootstrap/modal';
describe('ConditionsBasicAndAdvanceComponent', () => {
  let component: ConditionsBasicAndAdvanceComponent;
  let fixture: ComponentFixture<ConditionsBasicAndAdvanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConditionsBasicAndAdvanceComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        FormBuilder,
        {
          provide: DateTimeService,
          useFactory: () => ({}),
        },
        {
          provide: Conditions2UtilsService,
          useFactory: () => ({}),
        },
        {
          provide: BsModalService,
          useFactory: () => ({}),
        },
        {
          provide: ConditionsBasicUtilsService,
          useFactory: () => ({}),
        },
        {
          provide: ToastService,
          useFactory: () => ({}),
        },
        {
          provide: Conditions2BasicService,
          useFactory: () => ({}),
        },
        {
          provide: HttpClientModule,
          useFactory: () => ({}),
        },
        {
          provide: ExpertService,
          useFactory: () => ({}),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditionsBasicAndAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
