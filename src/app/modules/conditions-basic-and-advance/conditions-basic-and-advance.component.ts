/* eslint-disable @typescript-eslint/member-ordering */
import { Attribute, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateTimeService } from '@core/base/date-time.service';
import { EntityType } from '@core/models/entity-type.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import {
  ConditionsBasic,
  ConditionsToShow,
  ControlPanelConditions,
} from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { Conditions2BasicService } from 'src/app/modules/conditions2/services/conditions2-basic.service';
import { clone } from 'src/app/shared/utils/utils';
import { dateRangeValidator } from 'src/app/shared/validations/date-range.validator';
import { ConditionsBasicUtilsService } from '../conditions-basic/services/conditions-basic.sevice';
import { IncludedExcludedTreeComponent } from '../conditions2/components/included-excluded-tree/included-excluded-tree.component';
import { Conditions2 } from '../conditions2/models/conditions2.model';
import { Conditions2UtilsService } from '../conditions2/services/conditions2-utils.service';
import { IRulesHeaderProperties } from '../rules-header-form/models/rules-header.model';
import { RulesHeaderFormService } from '../rules-header-form/services/rules-header-form.service';
import { ToastService } from '../toast/toast.service';
import { MayHaveIdName } from '../../model/base-api.model';
import { UiModalSelectService } from '../ui-modal-select/ui-modal-select.service';
import { ConditionsRules, ConfigPreview, ERROR_RULES, RulesType } from './models/conditions-basic-and-advance.model';

@Component({
  selector: 'ff-conditions-basic-and-advance',
  templateUrl: './conditions-basic-and-advance.component.html',
  styleUrls: ['./conditions-basic-and-advance.component.scss'],
})
export class ConditionsBasicAndAdvanceComponent implements OnInit, OnChanges {
  @Input() loading!: boolean;
  @Input() item!: ConditionsRules;
  @Input() idAdminGroup!: string;
  @Input() entityTypes!: EntityType[];
  @Input() type: RulesType = RulesType.base_conditions;
  @Input() readonly = false;
  @ViewChild(IncludedExcludedTreeComponent) includedExcludedTreeComponent!: IncludedExcludedTreeComponent;

  @Output() formSubmit: EventEmitter<ConditionsRules> = new EventEmitter<ConditionsRules>();
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  form: FormGroup;
  sended = false;
  conditionValue: Conditions2 | ConditionsBasic | null = null;

  basicValue: ConditionsBasic | undefined;
  disabledBasicMode!: boolean;

  basicMode = true;
  advancedMode = false;
  errorEmptyBasicValue = false;
  _haveChanges = false;
  isCollapsedDefinition = false;

  categoriesList: string[] = [];

  tmpAttributes: Attribute[] = [];
  conditionsToShow!: ConditionsToShow;
  isCollapsedConfig: any = {};
  includedValue!: ConditionsBasic;
  excludedValue!: ConditionsBasic;
  skipFirstHourImput = 0;
  isConditionBasic = true;
  headerFields!: IRulesHeaderProperties[];
  visibleAttribute!: string;

  get errorInvalidStartDate() {
    return this.form.controls.start_date?.errors?.invalidDate as boolean;
  }

  get errorInvalidEndDate() {
    return this.form.controls.end_date?.errors?.invalidDate as boolean;
  }

  constructor(
    private fb: FormBuilder,
    public dateTimeService: DateTimeService,
    private utilsConditions: Conditions2UtilsService,
    private utilsBasicsConditions: ConditionsBasicUtilsService,
    private UiModalService: UiModalSelectService,
    private change: ChangeDetectorRef,
    private modalService: BsModalService,
    private toastService: ToastService,
    private controlPanelConditionsService: Conditions2BasicService,
    private rulesHeaderFormSrv: RulesHeaderFormService
  ) {
    this.form = this.fb.group(
      {
        active: [false],
        node: [null, Validators.required],
      },
      {
        validators: [dateRangeValidator()],
      }
    );
    this.form.valueChanges.subscribe(() => {
      this.haveChanges = true;
    });
  }

  ngOnInit() {
    this.conditionsToShow = {
      department: true,
      family: true,
      barra: true,
      sizeCode: false,
      product: true,
      saleReference: true,
      provider: true,
      brand: true,
      categories: true,
      goodTypes: true,
      classifications1: true,
      classifications2: true,
      classifications3: true,
      classifications4: true,
      classifications5: true,
      classifications6: true,
      managementType: true,
      customisedRequest: true,
      centers: true,
      ranges: true,
      gtin: true,
    };

    this.headerFields = this.rulesHeaderFormSrv.getRulesTypeHeader(this.type);

    if (this.readonly) {
      this.form.disable();
    }
    this.change.detectChanges();
  }

  set haveChanges(value: boolean) {
    if (!value) {
      this.includedExcludedTreeComponent?.formExcluded?.markAsPristine();
      this.includedExcludedTreeComponent?.formIncluded?.markAsPristine();
    }
    this._haveChanges = value;
  }

  get haveChanges() {
    return this._haveChanges;
  }

  parseItem(item: ConditionsRules): ConditionsRules {
    const result = JSON.parse(JSON.stringify(item)) as ConditionsRules;
    result.start_date = result.start_date ? new Date(result.start_date) : undefined;
    result.end_date = result.end_date ? new Date(result.end_date) : undefined;
    result.is_basic = result.id ? result.is_basic : true;
    return result;
  }

  ngOnChanges() {
    if (this.item) {
      this.item = this.parseItem(this.item);
      this.isConditionBasic = !!this.item.is_basic;
      this.form.patchValue(
        {
          ...this.item,
        },
        { emitEvent: false }
      );

      if (this.item.node) {
        this.getCurrentTab();
        // this.basicValue = this.basicMode
        //   ? this.utilsBasicsConditions.parseAsBasic(this.item.node)
        //   : undefined;

        if (this.basicMode) {
          const values = this.controlPanelConditionsService.nodeToIncludedExcludedValues(this.item.node);
          this.includedValue = values.included;
          this.excludedValue = values.excluded;
        }
      }

      // if (!this.basicMode) {
      //   this.disabledBasicMode = !this.utilsBasicsConditions.isBasicCondition(this.item.node);
      // }
      if (!this.isConditionBasic) {
        this.disabledBasicMode = true;
        this.changeAdvancedMode();
      }

      this.form.controls.node.valueChanges.subscribe((newVal) => {
        if (this.advancedMode) {
          this.disabledBasicMode = true;
        }
        if (newVal === null) this.disabledBasicMode = false;
        // this.disabledBasicMode = !this.utilsBasicsConditions.isBasicCondition(newVal);
      });
    }
  }

  getCurrentTab() {
    const currentTab = this.isConditionBasic;

    this.basicMode = currentTab;
    this.advancedMode = !currentTab;
  }

  changeBasicMode() {
    if (!this.disabledBasicMode && !this.form.controls.node.errors?.isFilled) {
      this.basicMode = true;
      this.advancedMode = false;
      const values = this.controlPanelConditionsService.nodeToIncludedExcludedValues(this.form.controls.node.value);
      this.includedValue = values.included;
      this.excludedValue = values.excluded;
    }
  }

  changeAdvancedMode() {
    this.basicMode = false;
    this.advancedMode = true;
    this.form.updateValueAndValidity({ emitEvent: false });
  }

  getDataToSend(): ConditionsRules | ERROR_RULES {
    let nodeValue: Conditions2 | undefined;

    const start_date = this.form.value.start_date ? this.dateTimeService.convertUTCDateToSend(this.form.value.start_date) : undefined;

    const end_date = this.form.value.end_date ? this.dateTimeService.convertUTCDateToSend(this.form.value.end_date) : undefined;

    if (this.basicMode) {
      this.categoriesList =
        this.includedValue?.categories?.map((categorie: string | MayHaveIdName) =>
          categorie instanceof Object ? categorie.id || '' : categorie
        ) || [];
      nodeValue = this.controlPanelConditionsService.objectsToConditions(this.includedValue, this.excludedValue);
    } else {
      const tempNodes = this.utilsConditions.valueToSend(this.form.controls.node.value);
      this.categoriesList = this.utilsConditions.categorieList;
      if (tempNodes === ERROR_RULES.ERROR_RANGE || tempNodes === ERROR_RULES.ERROR_CENTER) {
        return tempNodes;
      }
      nodeValue = tempNodes;
    }

    return {
      ...this.form.value,
      id: this.item && this.item.id,
      name: this.form.value.name,
      node: nodeValue,
      active: this.form.value.active,
      start_date,
      is_basic: this.basicMode,
      end_date,
      result: '{}',
    };
  }

  get isEmptyBasicValue() {
    return (
      (!this.includedValue && !this.excludedValue) ||
      (Object.keys(this.includedValue || {}).length === 0 && Object.keys(this.excludedValue || {}).length === 0)
    );
  }

  submit() {
    this.form.markAllAsTouched();
    this.sended = true;
    if (this.form.valid) {
      if (this.rangesWithoutCenters()) {
        this.toastService.error(
          this.visibleCenter ? 'CONDITIONS_BASIC_AND_ADVANCE.RANGES_WITHOUT_CENTERS' : 'CONDITIONS_BASIC_AND_ADVANCE.ERROR_RANGES_HIDDEN'
        );
      } else if (this.centersWithoutRanges()) {
        this.toastService.error(
          this.visibleCenter ? 'CONDITIONS_BASIC_AND_ADVANCE.CENTERS_WITHOUT_RANGES' : 'CONDITIONS_BASIC_AND_ADVANCE.ERROR_RANGES_HIDDEN'
        );
      } else {
        const data = this.getDataToSend();
        if (data === ERROR_RULES.ERROR_RANGE) {
          this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.RANGES_WITHOUT_CENTERS');
        } else if (data === ERROR_RULES.ERROR_CENTER) {
          this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.CENTERS_WITHOUT_RANGES');
        } else {
          this.formSubmit.emit(data);
        }
      }
    } else {
      if (this.form.controls.node.errors?.required) {
        this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.ENTER_CONDITION');
      }
      if (this.form.controls.node.errors?.isFilled) {
        this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.INCOMPLETE_RULES');
      }
      if (this.form.controls.node.errors?.doubleNotLT) {
        this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.DOUBLE_NOT_LT_ERROR');
      }
      if (this.form.controls.node.errors?.errorRange) {
        this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.ERROR_RANGE');
      }
      if (this.form.errors?.plp_pdp_required) {
        this.toastService.error('BADGE.DETAIL.PLP_PDP_REQUIRED', 'COMMON_ERRORS.REQUIRED');
      }
    }
  }

  get isDirty(): boolean {
    return this.haveChanges;
  }

  preview() {
    if (this.rulesPreviewConfig) {
      this.UiModalService.showRulesPreview(this.rulesPreviewConfig, this.modalService).subscribe(() => {});
    }
  }

  get rulesPreviewConfig(): ConfigPreview | null {
    return this.includedExcludedTreeComponent
      ? {
          title: 'CONDITIONS_BASIC_AND_ADVANCE.PREVIEW',
          name: this.form.value.name,
          included: this.includedExcludedTreeComponent.previewInclude,
          excluded: this.includedExcludedTreeComponent.previewExclude,
        }
      : null;
  }
  get visibleCenter(): boolean {
    return this.visibleAttribute === ControlPanelConditions.centers;
  }

  setVisibleAttribute(data: string) {
    this.visibleAttribute = data;
  }

  changeIncludedData(data: ConditionsBasic) {
    this.haveChanges = true;
    this.includedValue = data;
    this.updateValues();
  }

  updateValues() {
    const node: any = this.controlPanelConditionsService.objectsToConditions(this.includedValue, this.excludedValue, true);
    if (!node || (Array.isArray(node?.nodes || node) && (!node.nodes?.length || node.length))) {
      this.form.patchValue({
        node: null,
      });
    } else {
      this.form.patchValue({ node });
    }
  }

  changeExcludedData(data: ConditionsBasic) {
    this.haveChanges = true;
    this.excludedValue = data;
    this.updateValues();
  }

  private rangesWithoutCenters(): boolean {
    if (
      (this.includedValue?.ranges && !this.includedValue.centers?.length) ||
      (this.excludedValue?.ranges && !this.excludedValue.centers?.length)
    ) {
      return true;
    }
    return false;
  }

  private centersWithoutRanges(): boolean {
    if (
      (this.includedValue?.centers?.length && !this.includedValue.ranges) ||
      (this.excludedValue?.centers?.length && !this.excludedValue.ranges)
    ) {
      return true;
    }
    return false;
  }
}
