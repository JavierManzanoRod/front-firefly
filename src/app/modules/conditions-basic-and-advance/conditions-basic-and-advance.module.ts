import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ControlPanelSharedModule } from 'src/app/control-panel/shared/control-panel-shared.module';
import { FireflyDirectivesModule } from 'src/app/shared/directives/firefly-directives.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConditionsBasicModule } from '../conditions-basic/conditions-basic.module';
import { Conditions2Module } from '../conditions2/conditions2.module';
import { DatetimepickerModule } from '../datetimepicker/datetimepicker.module';
import { FormErrorModule } from '../form-error/form-error.module';
import { RulesHeaderFormModule } from '../rules-header-form/rules-header-form.module';
import { UiModalSelectModule } from '../ui-modal-select/ui-modal-select.module';
import { ConditionsBasicAndAdvanceComponent } from './conditions-basic-and-advance.component';

@NgModule({
  declarations: [ConditionsBasicAndAdvanceComponent],
  imports: [
    CommonModule,
    SharedModule,
    SimplebarAngularModule,
    FormsModule,
    Conditions2Module,
    ControlPanelSharedModule,
    FormErrorModule,
    ReactiveFormsModule,
    UiModalSelectModule,
    DatetimepickerModule,
    ConditionsBasicModule,
    RulesHeaderFormModule,
    FireflyDirectivesModule,
  ],
  exports: [ConditionsBasicAndAdvanceComponent],
})
export class ConditionsBasicAndAdvanceModule {}
