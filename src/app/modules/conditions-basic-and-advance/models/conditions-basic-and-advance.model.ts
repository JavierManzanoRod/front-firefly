import { Conditions2 } from '../../conditions2/models/conditions2.model';
import { RulesPreview } from '../../ui-modal-select/components/ui-modal-rules-preview/models/modal-rules-preview.model';

export interface ConditionsRules {
  id?: string;
  name: string;
  start_date?: string | Date;
  start_time?: string | Date;
  end_date?: string | Date;
  end_time?: string | Date;
  active: boolean;
  node?: Conditions2;
  result: string;
  folder_id?: string;
  is_basic?: boolean;
}

export enum ERROR_RULES {
  ERROR_RANGE = 'ERROR_RANGE',
  ERROR_CENTER = 'ERROR_CENTER',
}

export enum RulesType {
  admin_group = 'admin_group',
  content_group = 'content_group',
  base_conditions = 'base_conditions',
  size_guide = 'size_guide',
  special_product = 'special_product',
  fluorinated_gas = 'fluorinated_gas',
  expert_admin = 'expert_admim',
  price_inheritance = 'price_inheritance',
  badge = 'badge',
}

export interface ConfigPreview {
  title: string;
  name: string;
  included: RulesPreview;
  excluded: RulesPreview;
}
