import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { FieldI18nComponent } from 'src/app/modules/field-i18n/components/simple/field-i18.component';

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };
  public get errorMessage() {
    return this.content.errorMessage;
  }
}

describe('DashboardQueryListComponent', () => {
  let component: FieldI18nComponent;
  let fixture: ComponentFixture<FieldI18nComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FieldI18nComponent],
      providers: [{ provide: BsModalService, useClass: ModalServiceMock }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldI18nComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
