import { DatePipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DateTimeService } from '@core/base/date-time.service';
import { MayHaveLabel } from '@model/base-api.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UIModalRangeValue } from 'src/app/modules/ui-modal-select/models/modal-select.model';
import { UiModalSelectService } from 'src/app/modules/ui-modal-select/ui-modal-select.service';
import { UiEntityTypesSelectorsMultipleItem } from '../../../entity-types-selectors/components/ui-entity-types-selectors-multiple/ui-entity-types-selectors-multiple.model';
import { UiEntityTypesSelectorsService } from '../../../entity-types-selectors/ui-entity-types-selectors.service';
import { ChipsControlSelectConfig, ChipsUIModalRange, ModalChipsTypes } from '../chips.control.model';

function isStringArray(x: MayHaveLabel[] | string[] | null | { [key: string]: string }): x is string[] {
  return x !== null && Array.isArray(x) && typeof x[0] === 'string';
}

function isMayHaveLabelArray(x: MayHaveLabel[] | string[] | null | { [key: string]: string }): x is MayHaveLabel[] {
  const data: MayHaveLabel | undefined = x as MayHaveLabel[][0];
  return x !== null && !isStringArray(x) && data && (data.label || data.id) ? true : false;
}

function isMap(x: MayHaveLabel[] | string[] | null | { [key: string]: string }): x is { [key: string]: string } {
  return x !== null && !isStringArray(x) && !isMayHaveLabelArray(x);
}

@Component({
  selector: 'ff-chips-control-select',
  templateUrl: 'chips-control-select.component.html',
  styleUrls: ['./chips-control-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ChipsControlSelectComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChipsControlSelectComponent {
  @Input() required = false;
  @Input() config!: ChipsControlSelectConfig;
  @Input() readonly = false;
  @Input() maxChipsToShow = 10;
  @Input() showDisabled = false;
  value!: UiEntityTypesSelectorsMultipleItem[] | MayHaveLabel[] | string[] | undefined;
  keyValue!: { [key: string]: string } | undefined;
  objectValue!: UIModalRangeValue[];
  objectLabel!: string;
  chipLabelCache: { [key: string]: string } = {};
  disabled!: boolean;

  constructor(
    private readonly modalService: BsModalService,
    private datePipe: DatePipe,
    private dateService: DateTimeService,
    private readonly uiModalSelectService: UiModalSelectService,
    private readonly uiEntityTypesSelectorsService: UiEntityTypesSelectorsService,
    private ref: ChangeDetectorRef
  ) {}

  writeValue(data: MayHaveLabel[] | string[] | undefined | { [key: string]: string } | UIModalRangeValue, update?: boolean): void {
    // console.log('writing....', this.config.modalType, data);
    let value = data;
    if ([ModalChipsTypes.keyvalue, ModalChipsTypes.i18n].includes(this.config.modalType)) {
      const valueAux: any[] = [];
      if (data) {
        this.keyValue = data as any;
        Object.keys(data).forEach((key: string) => {
          const dataValue: string = (data as any)[key];
          valueAux.push({ label: '(' + key + ') ' + dataValue, id: key });
        });
      } else {
        this.keyValue = undefined;
      }
      value = valueAux;
    } else if ([ModalChipsTypes.experts].includes(this.config.modalType)) {
      const valueAux: any[] = [];
      if (data) {
        this.keyValue = {
          ios: '',
          android: '',
          ...(data as any),
        };
        Object.keys(data).forEach((key: string) => {
          const dataValue: string = (data as any)[key];
          if (dataValue) {
            valueAux.push({ label: '(' + key + ') ' + dataValue, id: key });
          }
        });
      } else {
        this.keyValue = undefined;
      }
      value = valueAux;
    } else if (this.config.modalType === ModalChipsTypes.select) {
      if (!this.config.modalConfig.multiple && !Array.isArray(value)) {
        value = value ? [value] : [];
      }
    }

    if ([ModalChipsTypes.range].includes(this.config.modalType)) {
      const config = this.config as ChipsUIModalRange;
      this.objectValue = value as UIModalRangeValue[];

      if (config.modalConfig.selectMultiple) {
        const select = ((value && (value as { [key: string]: any }).select) || []) as string[];
        if (select && select.length) {
          value = select.map((item) => {
            const obj = [{ ...value, select: item }] as UIModalRangeValue[];
            return {
              key: item,
              label: (config.chipFormat && config.chipFormat(obj)) || JSON.stringify(obj),
            };
          });
        }
      } else {
        this.objectValue = value as UIModalRangeValue[];

        this.objectLabel = (config.chipFormat && config.chipFormat(this.objectValue)) || JSON.stringify(this.objectValue);
      }
    }
    this.value = value as MayHaveLabel[] | string[] | undefined;

    let dataToSave = data;
    if (dataToSave !== null) {
      if (Array.isArray(dataToSave)) {
        if (!dataToSave.length) {
          dataToSave = undefined;
        }
      } else if (typeof dataToSave === 'object') {
        if (!Object.keys(dataToSave).length) {
          dataToSave = undefined;
        }
      }
    }

    if (update) {
      if (this.config.modalType === ModalChipsTypes.select && !this.config.modalConfig.multiple) {
        this.onChange(Array.isArray(dataToSave) ? dataToSave[0] : dataToSave);
      } else {
        this.onChange(dataToSave);
      }
    }

    // Añadido aqui por que si el padre hace un setValue no refresca los cambios.
    this.ref.markForCheck();
  }

  remove(index?: number) {
    if (!index && index !== 0) {
      this.writeValue(undefined, true);
      return;
    }

    if (this.keyValue) {
      if (this.value) {
        if ([ModalChipsTypes.experts].includes(this.config.modalType)) {
          const propRemovable = (this.value[index] as MayHaveLabel).id;
          if (propRemovable) {
            this.keyValue[propRemovable] = '';
          }
          this.writeValue(this.keyValue, true);
          return;
        }
        const propToRemove = (this.value[index] as MayHaveLabel).id;
        if (propToRemove) {
          delete this.keyValue[propToRemove];
        }
      }
      const newVal: any = {};
      this.value?.forEach((val: any) => {
        // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
        newVal[val.id] = val.label.substr(val.label.length - (val.label.length - (val.id.length + 3)));
      });
      this.writeValue(this.keyValue, true);
    } else if ([ModalChipsTypes.range].includes(this.config.modalType) && (this.config as ChipsUIModalRange).modalConfig.selectMultiple) {
      (this.objectValue[0].select as string[]).splice(index, 1);

      this.writeValue(this.objectValue);
    } else {
      this.value?.splice(index, 1);
      this.writeValue(this.value, true);
    }
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {}

  onChange(data: any) {
    // don't delete!
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = this.readonly = isDisabled;
    this.ref.markForCheck();
  }

  select() {
    if (!this.config.modalType || this.config.modalType === ModalChipsTypes.select) {
      (this.config.modalConfig as any).selected = (this.config.modalConfig as any).multiple
        ? Array.isArray(this.value)
          ? this.value
          : []
        : Array.isArray(this.value)
        ? this.value[0]
        : null;

      this.uiModalSelectService.showSelect(this.config.modalConfig as any, this.modalService).subscribe(
        (selectedItem: MayHaveLabel | MayHaveLabel[]) => {
          if (selectedItem) {
            this.writeValue(Array.isArray(selectedItem) ? selectedItem : [selectedItem], true);
          }
        },
        (err) => console.log(err)
      );
    } else if (this.config.modalType === ModalChipsTypes.free) {
      (this.config.modalConfig as any).items = this.value;
      this.uiModalSelectService.showSelectTexts(this.config.modalConfig, this.modalService).subscribe(
        (selectedItems: string[]) => {
          this.writeValue(selectedItems, true);
        },
        (err) => console.log(err)
      );
    } else if (this.config.modalType === ModalChipsTypes.i18n) {
      this.config.modalConfig.items = this.keyValue;
      this.uiModalSelectService.showI18N(this.config.modalConfig, this.modalService).subscribe(
        (selectedItems) => {
          this.writeValue(selectedItems, true);
        },
        (err) => console.log(err)
      );
    } else if (this.config.modalType === ModalChipsTypes.keyvalue) {
      this.config.modalConfig.items = this.keyValue;
      this.uiModalSelectService.showKeyValue(this.config.modalConfig, this.modalService).subscribe(
        (selectedItems) => {
          this.writeValue(selectedItems, true);
        },
        (err) => console.log(err)
      );
    } else if (this.config.modalType === ModalChipsTypes.experts) {
      this.config.modalConfig.items = this.keyValue;
      this.uiModalSelectService.showKeyValueExperts(this.config.modalConfig, this.modalService).subscribe(
        (selectedItems) => {
          this.writeValue(selectedItems, true);
        },
        (err) => console.log(err)
      );
    } else if (this.config.modalType === ModalChipsTypes.range) {
      this.config.modalConfig.value = this.objectValue;

      this.uiModalSelectService.showRange(this.config.modalConfig, this.modalService).subscribe(
        (selectedItems) => {
          this.writeValue(selectedItems, true);
        },
        (err) => console.log(err)
      );
    } else if (this.config.modalType === ModalChipsTypes.tree) {
      (this.config.modalConfig as any).selected = (this.config.modalConfig as any).multiple
        ? Array.isArray(this.value)
          ? this.value
          : []
        : Array.isArray(this.value)
        ? this.value[0]
        : null;
      this.uiModalSelectService.showTreeFolder(this.config.modalConfig as any, this.modalService).subscribe(
        (selectedItem: MayHaveLabel | MayHaveLabel[]) => {
          if (selectedItem) {
            this.writeValue(Array.isArray(selectedItem) ? selectedItem : [selectedItem], true);
          }
        },
        (err) => console.log(err)
      );
    } else if (this.config.modalType === ModalChipsTypes.subsites) {
      this.config.modalConfig.selectedSubsites = this.value;
      this.uiModalSelectService.showSelectSubsites(this.config.modalConfig, this.modalService).subscribe(
        (selectedItems: any) => {
          this.writeValue(selectedItems, true);
        },
        (err) => console.log(err)
      );
    } else if (this.config.modalType === ModalChipsTypes.date) {
      (this.config.modalConfig as any).items = this.value;
      this.uiModalSelectService.showSelectDate(this.config.modalConfig, this.modalService).subscribe(
        (selectedItems: string[]) => {
          selectedItems = selectedItems.map((date) => this.dateService.convertUTCDateToSend(new Date(date)));
          this.writeValue(selectedItems, true);
        },
        (err) => console.log(err)
      );
    } else if (this.config.modalType === ModalChipsTypes.entitiesAtributes) {
      this.config.modalConfig.selectedItems = this.value as UiEntityTypesSelectorsMultipleItem[];
      if (this.readonly) {
        //falta lógica para modal que toque
        this.uiEntityTypesSelectorsService.showSelectMultipleAttributeReadOnly(this.config.modalConfig).subscribe(
          (items) => {
            this.writeValue(items, true);
          },
          (err) => console.log(err)
        );
      } else {
        this.uiEntityTypesSelectorsService.showSelectMultipleAttribute(this.config.modalConfig).subscribe(
          (items) => {
            this.writeValue(items, true);
          },
          (err) => console.log(err)
        );
      }
    }
  }

  getItemValue(item: any) {
    if (this.config.modalType === ModalChipsTypes.select) {
      let value;
      this.config.modalConfig.itemLabelKey?.forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(item, key)) {
          value = item[key];
          return false;
        }
      });
      return value;
    }
  }

  getSiteSubsiteValue(item: any) {
    return `${item.site} (${item.subsite.name})`;
  }

  stringEllipsis(text: string) {
    if (text.length > 33) {
      return text.slice(0, 33) + '...';
    } else {
      return text;
    }
  }

  getToolTip(item: any): string | null {
    if (this.config.modalType === ModalChipsTypes.entitiesAtributes) {
      return this.getToolTipEntity(item);
    } else {
      const result = this.getText(item);
      return result?.length > 33 ? result : null;
    }
  }

  getToolTipEntity(val: any): string {
    const result = [];
    const parents = val?.parents;
    if (parents?.length) {
      for (const p of parents) {
        result.push(p?.label || p?.name);
      }
    }
    result.push(val?.label || val?.name);
    return result.join('-> ');
  }

  getText(item: any): string {
    if (this.config.modalType === 'date' && item) {
      return this.datePipe.transform(item, 'dd/MM/yyyy HH:mm') || '';
    }

    return this.config.modalType === 'free'
      ? item
      : this.config.modalType === 'select' && this.config.modalConfig.itemLabelKey
      ? this.getItemValue(item)
      : this.config.modalType === ModalChipsTypes.subsites
      ? this.getSiteSubsiteValue(item)
      : item.label || item.name || item;
  }

  getTextAsync(item: string): Observable<string> {
    if (this.config.chipLabel) {
      if (this.chipLabelCache[item]) {
        return of(this.chipLabelCache[item]);
      }

      return this.config.chipLabel(item).pipe(tap((label) => (this.chipLabelCache[item] = label)));
    }

    return of(item);
  }
}
