import { FormWrapper } from '.storybook/form-wrapper.component';
import { StorybookTranslateModule } from '.storybook/StorybookTranslate.module';
import { StorybookWrappersModule } from '.storybook/StorybookWrappers.module';
import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { componentWrapperDecorator, Meta, moduleMetadata, Story } from '@storybook/angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { UiEntityTypesSelectorsModule } from 'src/app/modules/entity-types-selectors/ui-entity-types-selectors.module';
import { UiModalSelectModule } from 'src/app/modules/ui-modal-select/ui-modal-select.module';
import { ModalChipsTypes } from '../chips.control.model';
import { ChipsControlSelectComponent } from './chips-control-select.component';

export default {
  component: ChipsControlSelectComponent,
  decorators: [
    moduleMetadata({
      declarations: [ChipsControlSelectComponent],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModalModule,
        UiEntityTypesSelectorsModule,
        HttpClientModule,
        StorybookTranslateModule,
        StorybookWrappersModule,
        UiModalSelectModule,
      ],
      providers: [DatePipe],
    }),
    componentWrapperDecorator(FormWrapper),
  ],
  title: 'Modales/Select',
  args: {
    name: 'something',
    ngModel: true,
    config: {
      label: 'Añadir textos de 0',
      modalType: ModalChipsTypes.free,
      modalConfig: {
        title: 'Añade los textos que quieras',
      },
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Al pinchar en el botón se muestra una modal desde la que poder añadir textos libres que una vez fuera de la modal se mostraran como un listado de chips los cuales podrás borrar uno a uno o editar volviendo a abrir la modal.',
      },
    },
  },
} as Meta;

const Template: Story<ChipsControlSelectComponent> = (args) => ({
  props: {
    ...args,
  },
});

export const Default = Template.bind({});
Default.args = {};

export const Required = Template.bind({});
Required.args = {
  required: true,
};

export const Readonly = Template.bind({});
Readonly.args = {
  readonly: true,
};

export const Select = Template.bind({});
Select.args = {
  config: {
    label: 'Seleccionar',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'Selecciona lo que quieras',
      /** Listado de posibles items a seleccionar */
      items: [
        { id: '1', name: 'Gatito 1' },
        { id: '2', name: 'Gatito 2' },
        { id: '3', name: 'Gatito 3' },
        { id: '4', name: 'Gatito 4' },
      ],
    },
  },
};
export const SelectMultiple = Template.bind({});
SelectMultiple.args = {
  config: {
    label: 'Seleccionar',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'Selecciona lo que quieras',
      multiple: true,
      /** Listado de posibles items a seleccionar */
      items: [
        { id: '1', name: 'Gatito 1' },
        { id: '2', name: 'Gatito 2' },
        { id: '3', name: 'Gatito 3' },
        { id: '4', name: 'Gatito 4' },
      ],
    },
  },
};
export const SelectI18N = Template.bind({});
SelectI18N.args = {
  config: {
    label: 'Añadir textos',
    modalType: ModalChipsTypes.i18n,
    modalConfig: {
      title: 'Añade los textos en los idiomas',

      /** Define el texto del boton de cerrar modal (Default: 'Cancelar') */
      cancelBtnText: 'Cerrar modal',

      /** Define el texto del boton de aceptar los cambios (Default: 'Seleccionar') */
      okBtnText: 'Aceptar cambios',
    },
  },
};
export const KeyValue = Template.bind({});
KeyValue.args = {
  config: {
    label: 'Añadir textos',
    modalType: ModalChipsTypes.keyvalue,
    modalConfig: {
      title: 'Añade los textos en los idiomas',

      /** Define el texto del boton de cerrar modal (Default: 'Cancelar') */
      cancelBtnText: 'Cerrar modal',

      /** Define el texto del boton de aceptar los cambios (Default: 'Seleccionar') */
      okBtnText: 'Aceptar cambios',
    },
  },
};
