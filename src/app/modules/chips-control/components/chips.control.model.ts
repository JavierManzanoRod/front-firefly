import { Observable } from 'rxjs';
import {
  UIModalI18N,
  UIModalKeyValue,
  UIModalRange,
  UIModalSelect,
  UIModalSelectSubsites,
  UIModalSelectTextsView,
} from 'src/app/modules/ui-modal-select/models/modal-select.model';
import { UiEntityTypesSelectorsMultiple } from '../../entity-types-selectors/components/ui-entity-types-selectors-multiple/ui-entity-types-selectors-multiple.model';
import { UIModalRangeValue, UIModalTree } from '../../ui-modal-select/models/modal-select.model';

export const enum ModalChipsTypes {
  'keyvalue' = 'keyvalue',
  'i18n' = 'i18n',
  'select' = 'select',
  'free' = 'free',
  'date' = 'date',
  'subsites' = 'subsites',
  'entitiesAtributes' = 'entitiesAtributes',
  'range' = 'range',
  'tree' = 'tree',
  'experts' = 'experts',
}

export interface ChipsUIModalKeyValue {
  modalType: ModalChipsTypes.keyvalue;
  modalConfig: UIModalKeyValue;
  label?: string;
  tooltip?: string;
  chipLabel?: (value: string) => Observable<string>;
}

export interface ChipsUIModalRange {
  modalType: ModalChipsTypes.range;
  modalConfig: UIModalRange;
  label?: string;
  tooltip?: string;
  chipFormat?: (value: UIModalRangeValue[]) => string | any;
  chipLabel?: (value: string) => Observable<string>;
}

export interface ChipsUIModalTree {
  modalType: ModalChipsTypes.tree;
  modalConfig: UIModalTree;
  label?: string;
  tooltip?: string;
  chipLabel?: (value: string) => Observable<string>;
}

export interface ChipsUIModalI18N {
  modalType: ModalChipsTypes.i18n;
  modalConfig: UIModalI18N;
  label?: string;
  tooltip?: string;
  chipLabel?: (value: string) => Observable<string>;
}

export interface ChipsUIModalSelect {
  modalType: ModalChipsTypes.select;
  modalConfig: UIModalSelect;
  label?: string;
  tooltip?: string;
  chipLabel?: (value: string) => Observable<string>;
}

export interface ChipsUIModalSelectTextsView {
  modalType: ModalChipsTypes.free;
  modalConfig: UIModalSelectTextsView;
  label?: string;
  tooltip?: string;
  chipLabel?: (value: string) => Observable<string>;
}
export interface ChipsUIModalSelectDateView {
  modalType: ModalChipsTypes.date;
  modalConfig: UIModalSelectTextsView;
  label?: string;
  tooltip?: string;
  chipLabel?: (value: string) => Observable<string>;
}

export interface ChipsUIModalSelectSubsites {
  modalType: ModalChipsTypes.subsites;
  modalConfig: UIModalSelectSubsites;
  label?: string;
  tooltip?: string;
  chipLabel?: (value: string) => Observable<string>;
}
export interface ChipsUIModalExperts {
  modalType: ModalChipsTypes.experts;
  modalConfig: UIModalKeyValue;
  label?: string;
  tooltip?: string;
  chipLabel?: (value: string) => Observable<string>;
}

export interface ChipsUIModalSelectMultipleEntityAttributes {
  modalType: ModalChipsTypes.entitiesAtributes;
  modalConfig: UiEntityTypesSelectorsMultiple;
  label?: string;
  tooltip?: string;
  chipLabel?: (value: string) => Observable<string>;
}

export type ChipsControlSelectConfig =
  | ChipsUIModalSelect
  | ChipsUIModalSelectTextsView
  | ChipsUIModalSelectDateView
  | ChipsUIModalI18N
  | ChipsUIModalKeyValue
  | ChipsUIModalSelectSubsites
  | ChipsUIModalSelectMultipleEntityAttributes
  | ChipsUIModalRange
  | ChipsUIModalExperts
  | ChipsUIModalTree;
