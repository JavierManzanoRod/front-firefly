import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { UiEntityTypesSelectorsModule } from '../entity-types-selectors/ui-entity-types-selectors.module';
import { UiTreeListModule } from '../ui-tree-list/ui-tree-list.module';
import { ChipsControlSelectComponent } from './components/chips-control-select/chips-control-select.component';

@NgModule({
  declarations: [ChipsControlSelectComponent],
  imports: [
    TranslateModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TooltipModule,
    UiTreeListModule,
    UiEntityTypesSelectorsModule,
    // UiModalSelectModule,
    // CommonModalModule,
    // UiModalSelectModule
  ],
  exports: [ChipsControlSelectComponent],
  providers: [DatePipe],
})
export class ChipsControlModule {}
