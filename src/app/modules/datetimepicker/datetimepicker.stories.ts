import { generateFormAttributes, StorybookWrappersModule } from '.storybook/StorybookWrappers.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { FormErrorModule } from '../form-error/form-error.module';
import { DatetimepickerComponent } from './datetimepicker.component';

export default {
  component: DatetimepickerComponent,
  decorators: [
    moduleMetadata({
      declarations: [DatetimepickerComponent],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        BsDatepickerModule.forRoot(),
        ReactiveFormsModule,
        FormErrorModule,
        TimepickerModule.forRoot(),
        FormsModule,
        StorybookWrappersModule,
      ],
    }),
  ],
  title: 'Datetimepicker',
  parameters: {
    docs: {
      description: {
        component:
          'Selector de fecha y tiempo, el tiempo (horas/minutos) no se puede especificar hasta que se seleccione una fecha y cada vez que se selecciona una fecha se resetea el tiempo especificado',
      },
    },
  },
} as Meta;

const Template: Story<DatetimepickerComponent> = (args) => ({
  props: {
    ...args,
  },
  template: `<ff-form-wrapper>
  <ff-datetimepicker ${generateFormAttributes(args)}></ff-datetimepicker>
  </ff-form-wrapper>`,
});

export const Default = Template.bind({});
Default.args = {
  id: '1',
  defaultHour: 12,
  defaultMinute: 15,
  defaultSeconds: 30,
};
