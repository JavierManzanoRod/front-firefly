import { Inject, LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { FormErrorModule } from '../form-error/form-error.module';
import { TimepickerModule } from '../timepicker/timepicker.module';
import { DatetimepickerComponent } from './datetimepicker.component';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
defineLocale('es', esLocale);

@NgModule({
  imports: [BsDatepickerModule.forRoot(), ReactiveFormsModule, FormErrorModule, FormsModule, TimepickerModule],
  declarations: [DatetimepickerComponent],
  providers: [],
  exports: [DatetimepickerComponent],
})
export class DatetimepickerModule {
  constructor(localeService: BsLocaleService, @Inject(LOCALE_ID) localeID: string) {
    localeService.use(localeID);
  }
}
