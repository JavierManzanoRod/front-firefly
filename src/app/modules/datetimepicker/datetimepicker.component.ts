import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { DestroyService } from 'src/app/shared/services/destroy.service';

@Component({
  selector: 'ff-datetimepicker',
  templateUrl: './datetimepicker.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatetimepickerComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: DatetimepickerComponent,
      multi: true,
    },
    DestroyService,
  ],
  styleUrls: ['./datetimepicker.component.scss'],
})
export class DatetimepickerComponent implements ControlValueAccessor, OnInit, OnDestroy {
  @Input() id: string | null = null;
  @Input() defaultHour: number | null = null;
  @Input() defaultMinute: number | null = null;
  @Input() defaultSeconds: number | null = null;

  form = new FormGroup({
    dateView: new FormControl(),
    timeView: new FormControl(),
  });
  dateTimeModel!: Date | null;
  disabled = false;
  skipFirstTimeValidation = true;
  skipFirstChangeDate = true;
  skipFirstChangeTime = true;
  comesWithSomeValue = false;
  isFirstWrite = false;
  isFirstWriteTime = false;

  get timeView(): FormControl {
    return this.form.get('timeView') as FormControl;
  }

  get timeViewValue(): Date | null {
    return this.timeView.value as Date | null;
  }

  get dateView(): FormControl {
    return this.form.get('dateView') as FormControl;
  }

  get dateViewValue(): Date | null {
    return this.dateView.value as Date | null;
  }

  constructor(private ref: ChangeDetectorRef, private destroy$: DestroyService) {}

  ngOnInit(): void {
    this.form
      .get('dateView')
      ?.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe((val) => this.onDateChange(val));
    this.form
      .get('timeView')
      ?.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe((val) => this.onTimeChange(val));
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  onTouch = () => {};

  updateModel(value: Date | null) {
    this.dateTimeModel = this.cloneDate(value);
    // //Update forms with real value
    this.dateView.setValue(this.cloneDate(value), { emitEvent: false });
    this.timeView.setValue(this.cloneDate(value), { emitEvent: false });
    this.onChange(this.cloneDate(value));
  }

  onDateChange(newDateValue: Date) {
    this.onTouch();
    if (!newDateValue) {
      this.updateModel(null);
      this.timeView?.setValue(null);
      this.timeView.disable();
      return;
    }
    if (!this.disabled && this.timeView.disabled) {
      this.timeView.enable();
    }
    if (this.isValidDate(this.timeViewValue)) {
      newDateValue.setHours(this.timeViewValue?.getHours());
      newDateValue.setMinutes(this.timeViewValue?.getMinutes());
    } else {
      if (this.isNumber(this.defaultHour)) newDateValue.setHours(this.defaultHour);
      if (this.isNumber(this.defaultMinute)) newDateValue.setMinutes(this.defaultMinute);
      if (this.isNumber(this.defaultSeconds)) newDateValue.setSeconds(this.defaultSeconds);
    }
    this.updateModel(newDateValue);
  }

  onTimeChange(newVal: Date | null) {
    this.onTouch();
    if (!newVal) {
      this.updateModel(null);
      return;
    }
    if (this.isValidDate(this.dateViewValue)) {
      newVal?.setFullYear(this.dateViewValue.getFullYear());
      newVal?.setMonth(this.dateViewValue.getMonth());
      newVal?.setDate(this.dateViewValue.getDate());
    }
    this.updateModel(newVal);
  }

  writeValue(date: Date): void {
    this.comesWithSomeValue = !!date;
    this.dateTimeModel = this.cloneDate(date);
    this.isFirstWrite = true;
    this.isFirstWriteTime = true;

    this.dateView.setValue(this.cloneDate(date), { emitEvent: false });
    this.timeView.setValue(this.cloneDate(date), { emitEvent: false });
    // Disable time if date is null
    if (!date) {
      this.timeView.disable({ emitEvent: false });
    } else if (!this.disabled) {
      this.timeView.enable({ emitEvent: false });
    }
    this.ref.detectChanges();
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouch = fn;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange(data: any) {
    // don't delete!
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    if (this.disabled) {
      this.form.disable();
    } else {
      this.form.enable();
      if (!this.dateViewValue) {
        this.timeView.disable();
      }
    }
    this.ref.detectChanges();
  }

  validate({ value }: FormControl) {
    const validDate = this.isValidDate(value) || !value;
    const validTime = this.isValidDate(this.timeViewValue) || !this.timeViewValue;
    const emptyTime = this.isValidDate(this.dateViewValue) && !this.timeViewValue;
    return validDate && validTime && !emptyTime
      ? null
      : {
          invalidDate: true,
        };
  }
  private isValidDate(date: Date | string | null): date is Date {
    return date instanceof Date || !!(date && !isNaN(new Date(date).getTime()));
  }

  private isNumber(x: any): x is number {
    return !Number.isNaN(x) && x !== null;
  }

  private isString(value: any): value is string {
    return typeof value === 'string' || value instanceof String;
  }

  private cloneDate(date: Date | null | string) {
    if (!date) {
      return null;
    } else if (this.isString(date)) {
      return new Date(date);
    }
    return new Date(date.getTime());
  }
}
