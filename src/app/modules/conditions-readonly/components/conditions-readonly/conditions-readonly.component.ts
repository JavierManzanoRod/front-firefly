import { ChangeDetectorRef, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Conditions2, Conditions2Node, Conditions2NodeTypes } from '../../../conditions2/models/conditions2.model';
import { Conditions2UtilsService } from '../../../conditions2/services/conditions2-utils.service';

@Component({
  selector: 'ff-conditions-readonly',
  templateUrl: './conditions-readonly.component.html',
  providers: [],
  styleUrls: ['./conditions-readonly.component.scss'],
})
export class ConditionsReadonlyComponent implements OnChanges {
  @Input() node!: Conditions2;

  condition!: Conditions2Node;

  constructor(private utils: Conditions2UtilsService, private ref: ChangeDetectorRef) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.node) {
      this.init();
    }
  }

  init() {
    if (this.node === null) {
      this.condition = this.getEmptyCondition();
    } else if (this.utils.isConditions2Node(this.node)) {
      this.condition = this.node;
    } else if (this.utils.isCondition2Simple(this.node)) {
      // simple or reference we wrap it (not acepted conditionsreference aloneif)
      this.condition = { node_type: Conditions2NodeTypes.or, nodes: [this.node] };
    } else if (this.utils.isConditions2Reference(this.node)) {
      this.condition = { node_type: Conditions2NodeTypes.or, nodes: [this.node] };
    } else {
      throw new Error('data provided is not recogniez');
    }
    this.ref.markForCheck();
  }

  getEmptyCondition(): Conditions2Node {
    return {
      node_type: Conditions2NodeTypes.or,
      nodes: [
        {
          node_type: Conditions2NodeTypes.and,
          nodes: [],
        },
        {
          node_type: Conditions2NodeTypes.and,
          nodes: [],
        },
      ],
    };
  }
}
