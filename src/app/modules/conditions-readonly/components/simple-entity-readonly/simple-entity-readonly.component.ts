import { Component, Input, OnInit } from '@angular/core';
import { Condition2Simple, Condition2SimpleEntity } from '../../../conditions2/models/conditions2.model';
import { Conditions2OperatorsService } from '../../../conditions2/services/conditions2.operators.service';

@Component({
  selector: 'ff-conditions-entity-readonly',
  templateUrl: './simple-entity-readonly.component.html',
  styleUrls: ['./simple-entity-readonly.component.scss'],
})
export class ConditionsEntityReadonlyComponent implements OnInit {
  @Input() condition!: Condition2SimpleEntity;

  operators: Condition2Simple['node_type'][] = [];

  constructor(private operatorService: Conditions2OperatorsService) {}

  ngOnInit() {
    this.operators = this.operatorService.getAvailableOperators(this.condition.attribute_data_type, this.condition.multiple_cardinality);
  }
}
