import { Component, Input } from '@angular/core';
import { Condition2SimpleEntity } from '../../../../conditions2/models/conditions2.model';

@Component({
  selector: 'ff-conditions-multiple-entity-readonly',
  templateUrl: './multiple-values-readonly.component.html',
  styleUrls: ['./multiple-values-readonly.component.scss'],
})
export class ConditionsMultipleEntityReadonlyComponent {
  @Input() condition!: Condition2SimpleEntity;

  constructor() {}
}
