import { Component, Input } from '@angular/core';
import {
  Condition2Reference,
  Conditions2,
  Conditions2Node,
  Conditions2NodeTypes,
  Conditions2ReferenceNodeType,
} from '../../../conditions2/models/conditions2.model';

@Component({
  selector: 'ff-conditions-node-readonly',
  templateUrl: './node-readonly.component.html',
  styleUrls: ['./node-readonly.component.scss'],
})
export class ConditionsNodeReadonlyComponent {
  @Input() condition!: Conditions2Node;

  constructor() {}

  showSimpleComponent(data: Conditions2): boolean {
    if (this.isConditions2Node(data)) {
      return false;
    } else if (this.isConditions2Reference(data)) {
      return false;
    } else {
      return data.attribute_data_type !== 'ENTITY';
    }
  }

  showSimpleEntityComponent(data: Conditions2): boolean {
    if (this.isConditions2Node(data)) {
      return false;
    } else if (this.isConditions2Reference(data)) {
      return false;
    } else {
      return data.attribute_data_type === 'ENTITY';
    }
  }

  isConditions2Node(condition: Conditions2): condition is Conditions2Node {
    return condition.node_type === Conditions2NodeTypes.and || condition.node_type === Conditions2NodeTypes.or;
  }

  isConditions2Reference(condition: Conditions2): condition is Condition2Reference {
    return condition.node_type === Conditions2ReferenceNodeType.reference;
  }
}
