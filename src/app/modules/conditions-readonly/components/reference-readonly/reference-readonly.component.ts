import { Component, Input } from '@angular/core';
import { Condition2Reference } from '../../../conditions2/models/conditions2.model';

@Component({
  selector: 'ff-conditions-reference-readonly',
  templateUrl: './reference-readonly.component.html',
  styleUrls: ['./reference-readonly.component.scss'],
})
export class ConditionsReferenceReadonlyComponent {
  @Input() condition!: Condition2Reference;

  constructor() {}
}
