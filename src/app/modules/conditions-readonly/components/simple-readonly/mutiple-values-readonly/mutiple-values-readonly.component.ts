import { Component, Input } from '@angular/core';

@Component({
  selector: 'ff-conditions-simple-multiple-values-readonly',
  templateUrl: './mutiple-values-readonly.component.html',
  styleUrls: ['./mutiple-values-readonly.component.scss'],
})
export class ConditionsSimpleMultipleValuesReadonlyComponent {
  @Input() values!: string[] | number[];

  constructor() {}
}
