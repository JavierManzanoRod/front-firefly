import { Component, Input, OnInit } from '@angular/core';
import { Condition2Simple } from '../../../conditions2/models/conditions2.model';
import { Conditions2OperatorsService } from '../../../conditions2/services/conditions2.operators.service';

@Component({
  selector: 'ff-conditions-simple-readonly',
  templateUrl: './simple-readonly.component.html',
  styleUrls: ['./simple-readonly.component.scss'],
  providers: [],
})
export class ConditionsSimpleReadonlyComponent implements OnInit {
  @Input() condition!: Condition2Simple;

  operators: Condition2Simple['node_type'][] = [];
  inputType = 'text';

  constructor(private operatorService: Conditions2OperatorsService) {}

  ngOnInit() {
    this.operators = this.operatorService.getAvailableOperators(this.condition.attribute_data_type, this.condition.multiple_cardinality);
    this.inputType = this.getInputType();
  }

  getInputType(): string {
    if (!this.condition) {
      return 'text';
    } else {
      return {
        STRING: 'text',
        DOUBLE: 'number',
        BOOLEAN: 'checkbox',
        ENTITY: 'entity',
        DATE_TIME: 'date',
        EMBEDDED: 'embedded',
        VIRTUAL: '',
      }[this.condition.attribute_data_type];
    }
  }
}
