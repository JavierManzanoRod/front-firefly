import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { SortablejsModule } from 'ngx-sortablejs';
import { LoadingModule } from 'src/app/modules/loading/loading.module';
import { SelectModule } from '../select/select.module';
import { ConditionsReadonlyComponent } from './components/conditions-readonly/conditions-readonly.component';
import { ConditionsNodeReadonlyComponent } from './components/node-readonly/node-readonly.component';
import { ConditionsReferenceReadonlyComponent } from './components/reference-readonly/reference-readonly.component';
import { ConditionsMultipleEntityReadonlyComponent } from './components/simple-entity-readonly/multiple-values-readonly/multiple-values-readonly.component';
import { ConditionsEntityReadonlyComponent } from './components/simple-entity-readonly/simple-entity-readonly.component';
import { ConditionsSimpleMultipleValuesReadonlyComponent } from './components/simple-readonly/mutiple-values-readonly/mutiple-values-readonly.component';
import { ConditionsSimpleReadonlyComponent } from './components/simple-readonly/simple-readonly.component';

@NgModule({
  declarations: [
    ConditionsReadonlyComponent,
    ConditionsNodeReadonlyComponent,
    ConditionsSimpleReadonlyComponent,
    ConditionsEntityReadonlyComponent,
    ConditionsReferenceReadonlyComponent,
    ConditionsMultipleEntityReadonlyComponent,
    ConditionsSimpleMultipleValuesReadonlyComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LoadingModule,
    AccordionModule.forRoot(),
    TranslateModule,
    SortablejsModule.forRoot({ animation: 150 }),
    NgSelectModule,
    SelectModule,
  ],
  exports: [ConditionsReadonlyComponent],
})
export class ConditionsReadonlyModule {}
