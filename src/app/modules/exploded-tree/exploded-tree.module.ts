import { CommonModule, TitleCasePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { DndModule } from 'ngx-drag-drop';
import { ToastModule } from '../toast/toast.module';
import { FoldersExplodedTreeComponent } from './components/adapters/folders-exploded-tree/folders-exploded-tree.component';
import { OfferExplodedTreeRulesComponent } from './components/adapters/offer-exploded-tree-rules/offer-exploded-tree-rules.component';
import { OfferExplodedTreeComponent } from './components/adapters/offer-exploded-tree/offer-exploded-tree.component';
import { ExplodedTreeNodeComponent } from './components/exploded-tree-node/exploded-tree-node.component';
import { ExplodedTreeComponent } from './exploded-tree.component';
import { HighlightPipe } from './pipes/highlight.pipe';

@NgModule({
  declarations: [
    ExplodedTreeNodeComponent,
    ExplodedTreeComponent,
    OfferExplodedTreeComponent,
    FoldersExplodedTreeComponent,
    HighlightPipe,
    OfferExplodedTreeRulesComponent,
  ],
  imports: [CommonModule, DndModule, TooltipModule, TranslateModule, NgSelectModule, ToastModule],
  exports: [
    ExplodedTreeNodeComponent,
    ExplodedTreeComponent,
    FoldersExplodedTreeComponent,
    OfferExplodedTreeComponent,
    HighlightPipe,
    OfferExplodedTreeRulesComponent,
  ],
  providers: [TitleCasePipe],
})
export class ExplodedTreeModule {}
