/* eslint-disable @typescript-eslint/member-ordering */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import {
  ExplodedActionEvent,
  ExplodedChangeEvent,
  ExplodedClickEvent,
  ExplodedEvent,
  ExplodedTree,
  ExplodedTreeAction,
  ExplodedTreeNode,
} from '../../../models/exploded-tree.model';
import { ExplodedTreeNodeComponent } from '../../exploded-tree-node/exploded-tree-node.component';
import { ExplodedTreeComponent } from 'src/app/modules/exploded-tree/exploded-tree.component';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'ff-folders-exploded-tree',
  templateUrl: './folders-exploded-tree.component.html',
  styleUrls: ['./folders-exploded-tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FoldersExplodedTreeComponent {
  @ViewChild(ExplodedTreeComponent) explodedTree!: ExplodedTreeComponent;

  @Input() loading = true;
  @Input() noIconActions = false;
  @Input() hideSearch = true;
  @Input() searchPlaceholder = 'CONTENTGROUP.SEARCH';
  @Input() searchInfoText = 'CONTENTGROUP.SEARCH_INFO';
  @Input() showFilter: ((node: ExplodedTreeNode) => boolean) | boolean = (node: ExplodedTreeNode) => (node?.value?.isRule ? false : true);
  @Input() classFilter: ((item: ExplodedTreeNodeComponent) => string) | undefined;

  @Input() set tree(tree: ExplodedTree[]) {
    this.treeFilter = tree;
    this._treeOriginal = tree;
    this.loading = false;
    this.setFilterItems(tree);
  }

  get tree(): ExplodedTree[] {
    return this.treeFilter;
  }

  get treeOriginal(): ExplodedTree[] {
    return this._treeOriginal;
  }

  @Output() clickNode = new EventEmitter<ExplodedClickEvent<ExplodedTreeNodeComponent>>();
  @Output() changeNode = new EventEmitter();
  @Output() clickAction = new EventEmitter();
  @Output() searchNodes = new EventEmitter();
  @Output() removeNode = new EventEmitter();

  defaultTree: ExplodedTree[] = [
    {
      label: this.translate.instant('CONTENTGROUP.FOLDER'),
      icon: 'icon-folder-new',
      class: 'create-folder',
      value: { create: true },
      children: [
        {
          placeholder: this.translate.instant('CONTENTGROUP.NEW_FOLDER'),
          icon: 'icon-icon-folder-closed',
          class: 'new-folder-mother',
          value: { create: true },
        },
      ],
    },
  ];

  filterItems: string[] = [];

  protected _treeOriginal: ExplodedTree[] = [];
  treeFilter: ExplodedTree[] = [];

  constructor(public translate: TranslateService) {}

  onClickAction(event: ExplodedActionEvent<ExplodedTreeNodeComponent>) {
    this.clickAction.emit(event);
    if (event.nativeEvent) {
      event.nativeEvent.preventDefault();
      event.nativeEvent.stopPropagation();
    }
  }

  onClickNode(event: ExplodedEvent<ExplodedTreeNodeComponent>) {
    if (event.data.value?.create) {
      setTimeout(() => {
        event.target.nodes?.get(0)?.editMode(true);
      });
    }
    this.clickNode.emit(event);
  }

  onChangeNode(event: ExplodedChangeEvent<ExplodedTreeNodeComponent>) {
    this.changeNode.emit(event);
    this.update();
  }

  onSearchNodes(value: string) {
    if (value) this.searchNodes.emit(value);
    else this.clear();
  }

  onRemoveNode(node: ExplodedTreeNodeComponent) {
    this.removeNode.next(node);
    this.update();
  }

  iconFilter(target: ExplodedTreeNodeComponent) {
    if (target.data.value?.isRule) {
      return 'icon-agrupaciones';
    }
    if (!target.collapsed) {
      return 'icon-folder';
    }

    return 'icon-icon-folder-closed';
  }

  filter(filter: string[]) {
    if (filter.length) {
      this.treeFilter = this._treeOriginal.filter((node) => filter.find((n) => n === node.name));
    } else this.treeFilter = this.treeOriginal;
    this.update();
  }
  clear() {
    this.treeFilter = this.treeOriginal;
    this.searchNodes.emit('');
    this.update();
  }

  setFilterItems(folder?: ExplodedTreeNode[]) {
    let nodes: ExplodedTreeNode[] = [];
    this.filterItems = [];

    if (folder?.length) {
      nodes = folder.map((node) => ({
        node_id: node.id,
        label: node.name,
      }));
    }
    nodes.sort((a, b) => (a.label?.toLowerCase() && b.label?.toLowerCase() && a.label?.toLowerCase() > b.label?.toLowerCase() ? 1 : -1));

    nodes.forEach((node) => {
      if (node.node_id) {
        if (node.label && !this.filterItems.find((item) => item === node.label)) {
          this.filterItems.push(node.label);
        }
      }
    });
  }

  actionsFilter(target: ExplodedTreeNodeComponent): ExplodedTreeAction[] {
    if (this.noIconActions) return [];
    if (target.data.value?.create || target.data.icon?.includes('icon-icon-warning')) {
      return [];
    }
    if (target.data.value?.isRule) {
      return [
        {
          icon: 'icon-pencil',
          label: 'detail',
          type: 'detail',
        },
        {
          icon: 'icon-icon-trash',
          label: 'delete',
          type: 'remove',
        },
      ];
    } else if (target.data) {
      return [
        {
          icon: 'icon-folder-new',
          label: 'folder',
          type: 'folder',
          tooltip: 'EXPLODED_TREE.TOOLTIP.CREATE_FOLDER',
        },
        {
          icon: 'icon-plus',
          label: 'new',
          type: 'new',
          tooltip: 'EXPLODED_TREE.TOOLTIP.CREATE_RULE',
        },
        {
          icon: 'icon-pencil',
          label: 'editable',
          type: 'edit',
          tooltip: 'EXPLODED_TREE.TOOLTIP.EDIT_FOLDER',
        },
        {
          icon: 'icon-icon-trash',
          label: 'delete',
          type: 'remove',
          tooltip: 'EXPLODED_TREE.TOOLTIP.DELETE_FOLDER',
        },
      ];
    } else return [];
  }

  update() {
    this.setFilterItems(this.treeOriginal);
    this.explodedTree.update();
  }
}
