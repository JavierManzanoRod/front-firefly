import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_BUNDLE_OF } from 'src/app/configuration/tokens/admin-group-type.token';
import { FF_API_PATH_VERSION_EXPLODED_TREE } from 'src/app/configuration/tokens/api-versions.token';
import { ExplodedTree, ExplodedTreeNode } from 'src/app/modules/exploded-tree/models/exploded-tree.model';

@Injectable({
  providedIn: 'root',
})
export class FolderExplodedTreeService extends ApiService<ExplodedTree> {
  endPoint = `products/backoffice-exploded-trees/${this.version}exploded-trees`;

  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_EXPLODED_TREE) private version: string,
    @Inject(FF_BUNDLE_OF) private idBundleOf: string
  ) {
    super();
  }

  get(id: string, hiddenNodes = false): Observable<ExplodedTreeNode> {
    return this.http.get<ExplodedTreeNode>(`${this.urlBase}/${id}`).pipe(
      map((value) => {
        value.children = value?.children?.sort((a: any, b: any) => (a.label > b.label ? 1 : -1));
        if (hiddenNodes) {
          value.children = value.children?.filter((att) => att.node_id !== this.idBundleOf);
        }
        return value;
      })
    );
  }

  search(filter: GenericApiRequest): Observable<GenericApiResponse<ExplodedTree>> {
    return this.http.get<GenericApiResponse<ExplodedTree>>(this.urlBase, {
      params: this.getParams({ ...this.getFilter(filter), ...{ size: 999999 } }),
    });
  }
}
