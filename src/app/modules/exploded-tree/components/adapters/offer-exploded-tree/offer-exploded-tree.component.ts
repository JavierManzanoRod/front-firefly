import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
} from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { catchError, finalize, map, take, filter } from 'rxjs/operators';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_ENTITY_TYPE_ID_OFFER } from 'src/app/configuration/tokens/configuracion';
import {
  ExplodedClickEvent,
  ExplodedTree,
  ExplodedTreeAction,
  ExplodedTreeElementType,
  ExplodedTreeNode,
  ExplodedTreeNodeType,
} from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { clone } from 'src/app/shared/utils/utils';
import { ExplodedTreeComponent } from '../../../exploded-tree.component';
import { ExplodedTreeNodeComponent } from '../../exploded-tree-node/exploded-tree-node.component';
import { OfferExplodedTreeService } from './offer-exploded-tree.service';

@Component({
  selector: 'ff-offer-exploded-tree',
  templateUrl: './offer-exploded-tree.component.html',
  styleUrls: ['./offer-exploded-tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OfferExplodedTreeComponent implements OnInit {
  @Input() draggable = false;
  @Input() rootCollapsed = false;
  @Input() rootAutoCollapse = true;
  @Input() hiddenBundleOf = false;
  @Input() filterHidden = false;
  @Input() initialExpandPaths: string[] = [];

  @Input() showFilter: ((item: ExplodedTreeNode) => boolean) | undefined;
  @Input() dragFilter: ((item: ExplodedTreeNodeComponent) => boolean) | undefined;
  @Input() clickFilter: ((item: ExplodedTreeNodeComponent) => boolean) | undefined;
  @Input() iconFilter: ((item: ExplodedTreeNodeComponent) => string) | undefined;
  @Input() badgeFilter: ((item: ExplodedTreeNodeComponent) => string) | undefined;
  @Input() fieldPathFilter: ((item: ExplodedTreeNodeComponent) => string) | undefined;
  @Input() offerPathFilter: ((item: ExplodedTreeNodeComponent) => string) | undefined;
  @Input() classFilter: ((item: ExplodedTreeNodeComponent) => string) | undefined;
  @Input() dataFilter: ((item: ExplodedTreeNodeComponent) => ExplodedTreeNode) | undefined;
  @Input() actionsFilter: ((item: ExplodedTreeNodeComponent) => ExplodedTreeAction[]) | undefined;

  @Input() staticTrees: ExplodedTree[] = [];

  @Output() clickNode = new EventEmitter<ExplodedClickEvent<ExplodedTreeNodeComponent>>();
  @Output() searchNodes = new EventEmitter();
  @Output() clickAction = new EventEmitter();

  @ViewChild(ExplodedTreeComponent) explodedTree!: ExplodedTreeComponent;
  loading = true;
  offerPath = '/Offer';
  tree: Subject<ExplodedTree[]> = new Subject<ExplodedTree[]>();
  searchValue = '';
  filterValue = '';
  noSearchResults = false;
  filterItems: string[] = [];

  private showCache: { [key: string]: boolean } = {};
  private productCache: ExplodedTreeNode[] = [];
  private paths = ['item_offered', 'item_offered/is_variant_of/pack_products/product/product_variants', 'bundle_of/product_variants'];
  private productsPaths: ExplodedTreeNode[] = [];

  constructor(
    private offerExplodedTree: OfferExplodedTreeService,
    private change: ChangeDetectorRef,
    private toast: ToastService,
    @Inject(FF_ENTITY_TYPE_ID_OFFER)
    public offerId: ConfigurationDefinition['entityTypeIdOffer']
  ) {}

  ngOnInit(): void {
    this.offerExplodedTree
      .get(this.offerId || '', this.hiddenBundleOf)
      .pipe(
        catchError((): Observable<ExplodedTreeNode> => {
          this.change.markForCheck();

          return of();
        }),
        map((response) => response || {}),
        finalize(() => (this.loading = false))
      )
      .subscribe((content) => this.tree.next(this.staticTrees.concat([content])));

    this.tree.subscribe(
      (content) => {
        this.loading = false;
        this.setProductPaths(content);
        this.setFilterItems();
      },
      () => (this.loading = false)
    );
  }

  click(event: ExplodedClickEvent<ExplodedTreeNodeComponent>) {
    if (
      event.data.element_type &&
      [ExplodedTreeElementType.ENTITY, ExplodedTreeElementType.EMBEDDED].includes(event.data.element_type) &&
      event.data.children?.length === 1 &&
      event.data.children[0].node_type === ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE
    ) {
      const label = event.data.label;
      let node_id_iss = clone(event.data.node_id);
      let namePath = '';
      event.data = event.target.data = event.data.children[0];
      if (event.target.parent?.data?.value?.node_id_iss) {
        node_id_iss = clone(event.target.parent?.data.value.node_id_iss);
        namePath = clone(event.target.parent?.data?.label || '');
      }

      if (event.target.parent?.data?.value?.namePath) {
        namePath = clone(event.target.parent?.data?.value?.namePath || '');
      }
      event.data.value = {
        ...event.data.value,
        node_id_iss,
        namePath,
      };

      event.target.data.label = event.data.label = label;
      this.click(event);
      return;
    }

    if (
      !event.data.children?.length &&
      event.data.node_type &&
      [
        ExplodedTreeNodeType.ATTRIBUTE_TREE_NODE,
        ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE,
        ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE,
      ].includes(event.data.node_type) &&
      event.data.node_id
    ) {
      event.target.setLoading(true);

      this.offerExplodedTree
        .get(encodeURIComponent(event.data.node_id))
        .pipe(
          catchError((): Observable<ExplodedTreeNode> => {
            event.target.setError('EXPLODED_TREE.ERROR_CHILDREN');
            event.target.setLoading(false);
            this.change.markForCheck();

            return of();
          }),
          map((response) => response || {})
        )
        .subscribe((content) => {
          if (content) {
            event.data.children = content.children;
            event.target.expand();
            event.target.setLoading(false);
            this.change.markForCheck();
          }
        });
    }

    if (event.target.data?.value?.node_id_iss) {
      event.target.data.node_id = event.target.data.value.node_id_iss;
    }

    this.clickNode.next({
      target: event.target,
      data: event.target.data,
    });
  }

  findOfferTree(nodes: ExplodedTree[]): ExplodedTree {
    return nodes.find((tree) => tree.identifier === this.offerId) || {};
  }

  show(node: ExplodedTreeNode) {
    if (!node.node_id && !node.id) {
      return false;
    }

    let show = true;

    if (node.node_id && this.showCache[node.node_id]) {
      return this.showCache[node.node_id];
    }

    if (this.searchValue) {
      show = this.showDepth(node);
    }

    if (this.showFilter) {
      show = this.showFilter(node) && show;
    }

    if (this.filterValue && node.node_type === ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE) {
      show = this.filterValue === node.label;
    }

    if (node.node_id && this.showCache) {
      this.showCache[node.node_id] = show;
    }
    return show;
  }

  showDepth(node: ExplodedTreeNode, fixed = true): boolean {
    let show = false;

    if (
      node.element_type &&
      [ExplodedTreeElementType.ENTITY, ExplodedTreeElementType.EMBEDDED].includes(node.element_type) &&
      node.children?.length === 1 &&
      node.children[0].node_type === ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE
    ) {
      const node_id_iss = node.node_id;
      const label = node.label;
      const child = node.children[0];
      Object.keys(child).forEach((key: string) => {
        (node as any)[key] = (child as any)[key];
      });
      node.children = node.children[0].children;
      node.label = label;
      node.value = { ...node.value, node_id_iss };
      node.fixed = true;
    }

    if (new RegExp(`${this.searchValue}`, 'i').exec(node.label || node.name || '')) {
      show = true;
      node.fixed = fixed;
    }

    if (!show && node.children) {
      show = node.children.filter((n) => this.showDepth(n, fixed)).length > 0;
    }

    return show;
  }

  expandSearchDepth(item: ExplodedTreeComponent | ExplodedTreeNodeComponent) {
    item.nodes?.changes
      .pipe(
        take(1),
        filter((nodes: QueryList<ExplodedTreeNodeComponent>) => nodes.length > 0)
      )
      .subscribe((nodes: QueryList<ExplodedTreeNodeComponent>) => {
        this.expandNodes(nodes);
        if (this.noSearchResults) {
          this.toast.warning('COMMON_ERRORS.SEARCH');
        }
      });
    if (item.nodes?.length) {
      this.expandNodes(item.nodes);
    }
  }

  expandNodes(nodes: QueryList<ExplodedTreeNodeComponent>) {
    nodes.forEach((node: ExplodedTreeNodeComponent) => {
      if (!node.data?.children?.filter((n) => this.showDepth(n, false)).length ? true : false) {
        node.collapsed = true;
        node.expanded = false;
        node.update();
        setTimeout(() => {
          node.update();
        });
      } else {
        this.expandSearchDepth(node);
      }
    });
    if (nodes.find((node) => this.showDepth(node.data, false))) {
      this.noSearchResults = false;
    } else {
      this.noSearchResults = true;
    }
  }

  search(value: string) {
    this.loading = true;

    this.showCache = {};
    this.searchValue = value;

    this.explodedTree.expanded = !!this.searchValue;
    this.explodedTree.collapseDepth(1);

    this.productCache.forEach((product) => (product.children = []));
    this.productCache = [];
    this.setFilterItems();

    if (value) {
      this.offerExplodedTree
        .search({ attribute_name: value, products_name: this.filterValue })
        .pipe(map((response) => response.content || []))
        .subscribe((content) => {
          this.loading = false;
          const finded: ExplodedTreeNode[] = [];
          if (content.length) {
            content.forEach((product) => {
              if (product.identifier && product.identifier !== this.offerId) {
                this.productsPaths.forEach((path) => {
                  const product_node = path?.children?.find((child) => child.node_id === `exploded_tree::${product.identifier}`);
                  if (product_node) {
                    product_node.children = product?.children;
                    this.productCache.push(product_node);
                    finded.push(product);
                  }
                });
              }
            });
            this.setFilterItems(finded);
          } else {
            this.expandSearchDepth(this.explodedTree);
          }
          this.change.markForCheck();
        });
    }

    setTimeout(() => {
      if (!this.searchValue) {
        this.loading = false;
      }

      this.explodedTree.update();
      this.change.markForCheck();
    });
  }

  filter(filterParam: string) {
    this.showCache = {};
    this.filterValue = filterParam;

    if (this.searchValue) {
      this.search(this.searchValue);
    } else if (filterParam) {
      this.filterItems = [filterParam];
    } else {
      this.setFilterItems();
    }

    if (this.filterValue) {
      //this.explodedTree.updateVisualPaths(this.paths.map((path) => this.offerPath + '/' + path));
      this.explodedTree.updateVisualPaths([this.offerPath + '/' + this.paths[0]]);
    }

    this.explodedTree.update();
  }

  clear() {
    this.filterValue = '';
    this.search('');
  }

  setProductPaths(nodes: ExplodedTreeNode[]) {
    this.productsPaths = this.paths.map((path) => {
      const paths = path.split('/');
      return this.findDepth(this.findOfferTree(nodes), paths);
    }) as ExplodedTreeNode[];
  }

  setFilterItems(products?: ExplodedTreeNode[]) {
    let nodes: ExplodedTreeNode[] = [];
    this.filterItems = [];

    if (products?.length) {
      nodes = products.map((node) => ({
        node_id: node.identifier,
        node_type: ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE,
        label: node.label,
      }));
    } else {
      this.productsPaths.forEach((path) => {
        if (path?.children?.length) {
          nodes = nodes.concat(path?.children);
        }
      });
    }

    nodes.sort((a, b) => (a.label && b.label && a.label > b.label ? 1 : -1));

    nodes.forEach((node) => {
      if (node.node_id && node.node_type === ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE && node.node_id !== this.offerId) {
        if (node.label && !this.filterItems.find((item) => item === node.label)) {
          this.filterItems.push(node.label);
        }
      }
    });
  }

  findDepth(node: ExplodedTreeNode, path: string[], index = 0): ExplodedTreeNode | undefined {
    if (node.children) {
      const find = node.children?.find((n) => n.label === path[index]);
      if (find) {
        const subfind = this.findDepth(find, path, index + 1);
        if (subfind) {
          return subfind;
        }
      }

      return find;
    }

    return node;
  }
}
