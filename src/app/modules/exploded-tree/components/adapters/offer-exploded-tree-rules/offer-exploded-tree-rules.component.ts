/* eslint-disable @typescript-eslint/member-ordering */
import { TitleCasePipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of, Subject } from 'rxjs';
import { catchError, finalize, map, take, tap } from 'rxjs/operators';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_ENTITY_TYPE_ID_OFFER } from 'src/app/configuration/tokens/configuracion';
import { Conditions2ReferenceNodeType } from 'src/app/modules/conditions2/models/conditions2.model';
import { Conditions2OperatorsService } from 'src/app/modules/conditions2/services/conditions2.operators.service';
import {
  ExplodedClickEvent,
  ExplodedTree,
  ExplodedTreeAction,
  ExplodedTreeElementType,
  ExplodedTreeNode,
  ExplodedTreeNodeType,
} from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { CategoryRuleService } from 'src/app/rule-engine/category-rule/services/category-rule.service';
import { clone } from 'src/app/shared/utils/utils';
import { ExplodedTreeComponent } from '../../../exploded-tree.component';
import { ExplodedTreeNodeComponent } from '../../exploded-tree-node/exploded-tree-node.component';
import { OfferExplodedTreeRulesService } from './offer-exploded-tree-rules.service';

@Component({
  selector: 'ff-offer-exploded-tree-rules',
  templateUrl: './offer-exploded-tree-rules.component.html',
  styleUrls: ['./offer-exploded-tree-rules.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OfferExplodedTreeRulesComponent implements OnInit {
  @Input() draggable = true;
  @Input() rootCollapsed = false;
  @Input() rootAutoCollapse = true;
  @Input() hiddenBundleOf = true;
  @Input() filterHidden = false;
  _entityTypeId!: string;
  get entityTypeId(): string {
    return this._entityTypeId;
  }

  @Input() set entityTypeId(value: string) {
    this._entityTypeId = value;
    this.filterValue = '';
    if (this.explodedTree) {
      this.explodedTree.filterValue = '';
    }
    this.loadTree();
  }
  @Input() adminGroupTypeId = '';
  @Input() initialExpandPaths: string[] = [];

  @Input() showFilter: ((item: ExplodedTreeNode) => boolean) | undefined;
  @Input() badgeFilter: ((item: ExplodedTreeNodeComponent) => string) | undefined;
  @Input() fieldPathFilter: ((item: ExplodedTreeNodeComponent) => string) | undefined;
  @Input() offerPathFilter: ((item: ExplodedTreeNodeComponent) => string) | undefined;
  @Input() classFilter: ((item: ExplodedTreeNodeComponent) => string) | undefined;
  @Input() actionsFilter: ((item: ExplodedTreeNodeComponent) => ExplodedTreeAction[]) | undefined;

  @Input() staticTrees: ExplodedTree[] = [];

  @Output() clickNode = new EventEmitter<ExplodedClickEvent<ExplodedTreeNodeComponent>>();
  @Output() searchNodes = new EventEmitter();
  @Output() clickAction = new EventEmitter();

  @ViewChild(ExplodedTreeComponent) explodedTree!: ExplodedTreeComponent;
  loading = true;
  offerPath = '/Offer';
  tree: Subject<ExplodedTree[]> = new Subject<ExplodedTree[]>();
  searchValue = '';
  filterValue = '';
  noSearchResults = false;
  filterItems: string[] = [];
  hiddenShowAll = false;
  private showCache: { [key: string]: boolean } = {};
  private productCache: ExplodedTreeNode[] = [];
  private paths = ['item_offered', 'item_offered/is_variant_of/pack_products/product/product_variants', 'bundle_of/product_variants'];
  private productsPaths: ExplodedTreeNode[] = [];
  private readonly KeyCategory = this.translateService.instant('EXPLODED_TREE.REFERENCIAS');
  private readonly KeyAttribute = this.translateService.instant('EXPLODED_TREE.ATRIBUTOS');

  constructor(
    private offerExplodedTree: OfferExplodedTreeRulesService,
    private change: ChangeDetectorRef,
    private operatorsService: Conditions2OperatorsService,
    private toast: ToastService,
    protected apiCategory: CategoryRuleService,
    private captalizePipe: TitleCasePipe,
    private translateService: TranslateService,
    @Inject(FF_ENTITY_TYPE_ID_OFFER)
    public offerId: ConfigurationDefinition['entityTypeIdOffer']
  ) {}

  ngOnInit(): void {
    //this.loadTree();
    this.tree.subscribe(
      (content) => {
        this.loading = false;
        this.setProductPaths(content);
        this.setFilterItems();
      },
      () => (this.loading = false)
    );
  }

  loadTree() {
    this.loading = true;
    this.offerExplodedTree
      .get(this._entityTypeId || this.offerId || '', this.hiddenBundleOf)
      .pipe(
        catchError((): Observable<ExplodedTreeNode> => {
          this.change.markForCheck();

          return of();
        }),
        map((response) => response || {}),
        finalize(() => (this.loading = false))
      )
      .subscribe((content) => this.tree.next(this.staticTrees.concat([content])));
  }

  iconFilter(node: ExplodedTreeNodeComponent) {
    let icon = 'icon-agrupaciones';
    if (node.isRoot) {
      return 'icon-icon-atributos';
    }
    if (
      node.data?.element_type === 'ENTITY' ||
      node.data?.element_type === 'EMBEDDED' ||
      node.data?.node_type === 'INCLUDE_PRODUCT_TYPE' ||
      node.data?.node_type === 'INCLUDE_ENTITY_TYPE' ||
      node.data?.node_type === 'ATTRIBUTE_TREE_NODE'
    ) {
      icon = node.data?.is_multiple_cardinality ? 'icon-icon-multiple' : 'icon-icon-atributos';
    }
    return icon;
  }

  dragFilter(node: ExplodedTreeNodeComponent) {
    return (
      node.data.element_type !== 'EMBEDDED' &&
      !node.isRoot &&
      node.data.node_type !== 'ATTRIBUTE_TREE_NODE' &&
      node.data.node_type !== 'INCLUDE_PRODUCT_TYPE'
    );
  }

  dataFilter(node: ExplodedTreeNodeComponent) {
    //node.data.is_multiple_cardinality = true;
    const availableOperators = this.operatorsService.getAvailableOperators(
      node.data.element_type || 'STRING',
      node.data.is_multiple_cardinality
    );

    let parent: any[] = [];

    if (
      node.data.element_type &&
      [ExplodedTreeElementType.ENTITY, ExplodedTreeElementType.EMBEDDED].includes(node.data.element_type) &&
      !node.data.entity_reference
    ) {
      node.data.entity_reference = node.data.children
        ? node.data.children[0].node_id?.replace('exploded_tree::', '')
        : node.data.node_id?.replace('exploded_tree::', '');
    }

    if (node.parent?.data?.node_type === 'ATTRIBUTE' || node.parent?.data?.node_type === 'COMPOSITE_FIELD') {
      const split = node.data?.field_path ? node.data?.field_path.split('/') : [node.data.node_id];
      node.data.node_id = split[split.length - 1];
    }

    if (node.parent?.data.value?.parent) {
      parent = [...node.parent?.data.value.parent, node.data.node_id];
    } else if (node.parent?.data.node_id) {
      parent = [node.parent?.data.node_id, node.data.node_id];
    } else if (node.data.node_id) {
      parent = [node.data.node_id];
    }

    if (node.data.node_type === 'ATTRIBUTE_TREE_NODE') {
      parent = [...node.parent?.data.value.parent];
    }

    if (node.data.node_type === 'INCLUDE_PRODUCT_TYPE') {
      node.data.iss = node.data.node_id;
    }

    // "ATTRIBUTE_TREE_NODE" son los virtuales
    const data: any = {
      ...node.data,
      parents: node.data.parents,
      value: {
        ...node.data,
        attribute_data_type: node.data.element_type,
        parent_attribute_tree_node_id: node.data.node_type === 'ATTRIBUTE_TREE_NODE',
        attribute_id: node.data.node_id,
        data_type: node.data.element_type as any,
        node_type: availableOperators && (availableOperators[0] as any),
        attribute_label: this.captalizePipe.transform(node.data.label),
        tooltip_label: node.generateVisualPath(true),
        multiple_cardinality: node.data.is_multiple_cardinality,
        iss: node.parent?.data?.value?.iss || node.data.iss,
        // necesito parents con un array de los padres con todos sus datos
        //value: null,
        parent,
        attribute: {
          identifier: node.data.node_id,
        },
      },
      // falta relativo a entidad y id del attributo y padres
    };

    if (data.value.data_type === Conditions2ReferenceNodeType.reference) {
      data.value.node_type = Conditions2ReferenceNodeType.reference;
    }

    if (node.parent?.data.parents) {
      data.parents = [...node.parent?.data.parents, clone(data.value)];
    } else {
      data.parents = [clone(data.value)];
    }

    if (data?.value?.value?.node_id_iss) {
      const temp = data.parents?.find((v: any) => v.node_id === data.node_id);
      if (temp) {
        temp.attribute = {
          identifier: data?.value?.value?.node_id_iss,
        };
        // temp.attribute_id = data?.value?.value?.node_id_iss;
        temp.parent[temp.parent?.length - 1] = data?.value?.value?.node_id_iss;
      }
      data.value.attribute = {
        identifier: data?.value?.value?.node_id_iss,
      };
      delete temp.value;
      data.value.attribute_id = data?.value?.value?.node_id_iss;
      temp.attribute_id = data?.value?.value?.node_id_iss;
    }

    if (data.value?.value) {
      data.value.value = null;
    }

    return data;
  }

  click(event: ExplodedTreeNodeComponent) {
    if (this.isEntity(event) && event.data.children) {
      event.data = {
        ...event.data,
        ...event.data.children[0],
        label: event.data.label,
        is_multiple_cardinality: event.data.is_multiple_cardinality || event.data.children[0].is_multiple_cardinality,
      };
      event.data.children = [];
      /* this.click(event);
      return;*/
    }

    if (
      !event.data.children?.length &&
      event.data.node_type &&
      [
        ExplodedTreeNodeType.ATTRIBUTE_TREE_NODE,
        ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE,
        ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE,
      ].includes(event.data.node_type) &&
      event.data.node_id
    ) {
      event.setLoading(true);
      this.offerExplodedTree
        .get(encodeURIComponent(event.data.node_id))
        .pipe(
          catchError((): Observable<ExplodedTreeNode> => {
            event.setError('EXPLODED_TREE.ERROR_CHILDREN');
            event.setLoading(false);
            this.change.markForCheck();

            return of();
          }),
          map((response) => response || {})
        )
        .subscribe((content) => {
          if (content) {
            event.data.children = content.children;
            event.expand();
            event.setLoading(false);
            this.change.markForCheck();
          }
        });
    }
  }

  isEntity(event: ExplodedTreeNodeComponent): boolean {
    return !!(
      event.data.element_type &&
      [ExplodedTreeElementType.ENTITY, ExplodedTreeElementType.EMBEDDED].includes(event.data.element_type) &&
      event.data.children?.length === 1 &&
      event.data.children[0].node_type === ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE
    );
  }

  findOfferTree(nodes: ExplodedTree[]): ExplodedTree {
    return nodes.find((tree) => tree.identifier === this.offerId) || {};
  }

  show(node: ExplodedTreeNode) {
    if (!node.node_id && !node.id) {
      return false;
    }

    let show = true;

    if (node.node_id && this.showCache[node.node_id]) {
      return this.showCache[node.node_id];
    }

    if (this.searchValue) {
      show = this.showDepth(node);
    }

    if (this.showFilter) {
      show = this.showFilter(node) && show;
    }

    if (this.filterValue && node.node_type === ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE) {
      show = this.filterValue === node.label;
    }

    if (node.node_id && this.showCache) {
      this.showCache[node.node_id] = show;
    }
    return show;
  }

  showDepth(node: ExplodedTreeNode, fixed = true): boolean {
    let show = false;

    if (
      node.element_type &&
      [ExplodedTreeElementType.ENTITY, ExplodedTreeElementType.EMBEDDED].includes(node.element_type) &&
      node.children?.length === 1 &&
      node.children[0].node_type === ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE
    ) {
      const node_id_iss = node.node_id;
      const label = node.label;
      const temp = clone(node);
      const child = node?.children[0];
      if (Object.keys(child).length) {
        Object.keys(child).forEach((key: string) => {
          (node as any)[key] = (child as any)[key];
        });
      }
      node.children = node?.children ? node.children[0]?.children : node.children;
      node.is_multiple_cardinality = temp.is_multiple_cardinality;
      node.is_tree_attribute = temp.is_tree_attribute;
      node.label = label;
      node.value = { ...node.value, node_id_iss };
      node.fixed = true;
    }

    if (new RegExp(`${this.searchValue}`, 'i').exec(node.label || node.name || '')) {
      show = true;
      node.fixed = fixed;
    }

    if (!show && node.children) {
      show = node.children?.filter((n) => this.showDepth(n, fixed)).length ? true : false;
    }

    return show;
  }

  expandSearchDepth(item: ExplodedTreeComponent | ExplodedTreeNodeComponent) {
    item.nodes?.changes.pipe(take(1)).subscribe((nodes: QueryList<ExplodedTreeNodeComponent>) => {
      this.expandNodes(nodes);
      if (this.noSearchResults) {
        this.toast.warning('COMMON_ERRORS.SEARCH');
      }
    });
    if (item.nodes?.length) {
      this.expandNodes(item.nodes);
    }
  }

  expandNodes(nodes: QueryList<ExplodedTreeNodeComponent>) {
    nodes.forEach((node: ExplodedTreeNodeComponent) => {
      if (!node.data?.children?.filter((n) => this.showDepth(n, false)).length ? true : false) {
        node.collapsed = true;
        node.expanded = false;
        node.update();
        setTimeout(() => {
          node.update();
        });
      } else {
        this.expandSearchDepth(node);
      }
    });
    if (nodes.find((node) => this.showDepth(node.data, false))) {
      this.noSearchResults = false;
    } else {
      this.noSearchResults = true;
    }
  }

  get searchPlaceholder() {
    return this.filterValue === this.KeyCategory ? 'EXPLODED_TREE.SEARCH_REFERENCES' : 'EXPLODED_TREE.SEARCH_ATTRIBUTES';
  }

  search(value: string) {
    this.loading = true;

    this.showCache = {};
    this.searchValue = value;

    this.explodedTree.expanded = !!this.searchValue;
    this.explodedTree.collapseDepth(1);

    this.productCache.forEach((product) => (product.children = []));
    this.productCache = [];
    this.setFilterItems();

    if (value) {
      if (this.filterValue === this.KeyCategory) {
        this.loadCategory();
      } else {
        this.offerExplodedTree
          .search({ attribute_name: value, products_name: this.filterValue })
          .pipe(map((response) => response.content || []))
          .subscribe((content) => {
            this.loading = false;
            const finded: ExplodedTreeNode[] = [];
            if (content.length) {
              content.forEach((product) => {
                if (product.identifier && product.identifier !== this.offerId) {
                  this.productsPaths.forEach((path) => {
                    const product_node = path?.children?.find((child) => child.node_id === `exploded_tree::${product.identifier}`);
                    if (product_node) {
                      product_node.children = product?.children;
                      this.productCache.push(product_node);
                      finded.push(product);
                    }
                  });
                }
              });
              this.setFilterItems(finded);
            } else {
              this.expandSearchDepth(this.explodedTree);
            }
            this.change.markForCheck();
          });
      }
    }

    setTimeout(() => {
      if (!this.searchValue) {
        this.loading = false;
      }

      this.explodedTree.update();
      this.change.markForCheck();
    });
  }

  loadFilterCategory() {
    this.filterItems = [this.KeyAttribute];
    this.explodedTree.update();
    this.change.markForCheck();
  }

  loadCategory() {
    this.loading = true;
    // falta lo que busca y el admin group
    this.apiCategory
      .search(this.searchValue, {
        size: 9999,
        page: 0,
        admin_group_type_id: this.adminGroupTypeId,
      })
      .pipe(
        tap(() => (this.loading = true)),
        map((rules: any) => {
          return (rules || []).map((categoryRule: any) => {
            return {
              icon: 'icon-agrupaciones',
              node_id: categoryRule.id || '',
              id: categoryRule.id || '',
              label: categoryRule.name,
              element_type: Conditions2ReferenceNodeType.reference,
              name: categoryRule.name,
              node_type: Conditions2ReferenceNodeType.reference,
              reference_id: categoryRule.id || '',
            };
          });
        }),
        finalize(() => (this.loading = false))
      )
      .subscribe(
        (content) => {
          content = [
            {
              label: this.KeyCategory,
              children: content,
            },
          ];
          this.tree.next(this.staticTrees.concat(content));
          this.loadFilterCategory();
        },
        () => (this.loading = false)
      );
  }

  filter(filter: string) {
    this.showCache = {};
    this.filterValue = filter;

    if (filter === this.KeyCategory) {
      this.loadCategory();
      this.hiddenShowAll = true;
      return;
    }

    if (filter === this.KeyAttribute) {
      this.hiddenShowAll = false;
      this.loadTree();
      this.filterValue = '';
      this.explodedTree.filterValue = '';
      return;
    }

    if (this.searchValue) {
      this.search(this.searchValue);
    } else if (filter) {
      this.filterItems = [filter];
    } else {
      this.setFilterItems();
    }

    if (this.filterValue) {
      //this.explodedTree.updateVisualPaths(this.paths.map((path) => this.offerPath + '/' + path));
      this.explodedTree.updateVisualPaths([this.offerPath + '/' + this.paths[0]]);
    }

    this.explodedTree.update();
  }

  clear() {
    if (this.filterValue === this.KeyCategory) {
      this.searchValue = '';
      this.loadCategory();
    } else {
      this.filterValue = '';
      this.search('');
    }
  }

  setProductPaths(nodes: ExplodedTreeNode[]) {
    this.productsPaths = this.paths.map((path) => {
      const paths = path.split('/');
      return this.findDepth(this.findOfferTree(nodes), paths);
    }) as ExplodedTreeNode[];
  }

  setFilterItems(products?: ExplodedTreeNode[]) {
    let nodes: ExplodedTreeNode[] = [];
    this.filterItems = [this.KeyCategory];

    if (products?.length) {
      nodes = products.map((node) => ({
        node_id: node.identifier,
        node_type: ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE,
        label: node.label,
      }));
    } else {
      this.productsPaths.forEach((path) => {
        if (path?.children?.length) {
          nodes = nodes.concat(path?.children);
        }
      });
    }

    nodes = nodes.sort((a, b) => (a.label && b.label && a.label > b.label ? 1 : -1));

    nodes.forEach((node) => {
      if (node.node_id && node.node_type === ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE && node.node_id !== this.offerId) {
        if (node.label && !this.filterItems.find((item) => item === node.label)) {
          this.filterItems.push(node.label);
        }
      }
    });
  }

  findDepth(node: ExplodedTreeNode, path: string[], index = 0): ExplodedTreeNode | undefined {
    if (node.children) {
      const find = node.children?.find((n) => n.label === path[index]);
      if (find) {
        const subfind = this.findDepth(find, path, index + 1);
        if (subfind) {
          return subfind;
        }
      }

      return find;
    }

    return node;
  }
}
