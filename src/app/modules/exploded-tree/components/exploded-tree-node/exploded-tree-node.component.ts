/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList,
  Renderer2,
  SimpleChanges,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ExplodedTreeComponent } from '../../exploded-tree.component';
import {
  ExplodedClickEvent,
  ExplodedTree,
  ExplodedTreeAction,
  ExplodedTreeNode,
  ExplodedTreeNodeType,
} from '../../models/exploded-tree.model';

@Component({
  selector: 'ff-exploded-tree-node',
  templateUrl: './exploded-tree-node.component.html',
  styleUrls: ['./exploded-tree-node.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExplodedTreeNodeComponent<P = any> implements OnInit, OnChanges {
  @Input() collapsed = true;
  @Input() draggable = false;
  @Input() parent: ExplodedTreeNodeComponent<P> | null = null;
  @Input() data: ExplodedTree<P> & ExplodedTreeNode<P> = {};
  @Input() root: ExplodedTreeComponent<P> | null = null;
  @Input() expanded = false;
  @Input() editable = false;
  @Input() cannotTruncated = false;

  @HostBinding('class')
  get class() {
    if (this.root && this.root?.classFilter) return [this.root?.classFilter(this)];

    if (this.data.class) {
      return [this.data.class];
    }

    return [];
  }

  @Input()
  @HostBinding('class.root')
  isRoot = false;

  @Input()
  @HostBinding('class.loading')
  loading = false;

  @Output() clickNode = new EventEmitter<ExplodedClickEvent<ExplodedTreeNodeComponent>>();
  @ViewChildren(ExplodedTreeNodeComponent) nodes!: QueryList<ExplodedTreeNodeComponent> | null;
  @ViewChild('inputRef') inputRef!: ElementRef<HTMLInputElement>;

  @HostBinding('class.horizontalScroll')
  get horizontalScroll() {
    return this.root?.enableHorizontalScroll;
  }

  focused = false;
  canSave = false;
  actions: ExplodedTreeAction[] = [];

  get actived() {
    return (
      !this.root?.disableActived &&
      this.data === this.root?.active?.data &&
      ((this.id === this.root?.active?.id && !this.root?.ignoreId) || this.root?.ignoreId)
    );
  }

  get error() {
    return this.data.node_type === ExplodedTreeNodeType.ERROR;
  }

  get warning() {
    return this.data.node_type === ExplodedTreeNodeType.WARNING;
  }

  @HostBinding('class.edit-mode')
  get isEditable() {
    return this.root?.nodeEditable || this.editable;
  }

  get rootNode(): ExplodedTreeNode | null {
    if (this.root && this.root.nodes?.length) {
      return this.root.nodes.first.data;
    }

    return null;
  }

  get isDragFilter(): boolean {
    if (this.root?.dragFilter) {
      return this.root?.dragFilter(this);
    }

    return true;
  }

  id = (Math.random() + 1).toString(36).substring(2);
  dragabled = false;
  truncated = false;
  icon = '';
  badge = '';

  constructor(private change: ChangeDetectorRef, private translate: TranslateService, private renderer: Renderer2) {}

  ngOnInit(): void {
    if (!this.data.children) {
      this.dragabled = true;
    }

    if (this.root?.dataFilter) {
      this.data = this.root?.dataFilter(this);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.generateStatics();

    if (changes.expanded.previousValue !== changes.expanded.currentValue && changes.expanded.currentValue) {
      this.collapsed = false;
    }
  }

  isIncludeEntityType() {
    return this.data.node_type === ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE;
  }

  getVisibleChildren(closest = false) {
    if (closest && this.closestParam('fixed', true)) {
      return [];
    }

    return this.data.children ? this.data.children?.filter((child) => this.isVisible(child)) : [];
  }

  generateStatics() {
    this.icon = this.generateIcon();
    this.badge = this.generateBadge();
    this.actions = this.generateActions();
  }

  generateBadge(): string {
    if (this.root?.badgeFilter) {
      return this.root.badgeFilter(this);
    }

    if (this.data.badge) {
      return this.data.badge;
    }

    return '';
  }

  generateIcon(): string {
    const children = this.getVisibleChildren();

    if (this.data.icon) {
      return this.data.icon;
    }

    if (this.root?.iconFilter) {
      const icon = this.root.iconFilter(this);
      if (icon) {
        return icon;
      }
    }

    if (this.isRoot || ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE === this.data.node_type) {
      return 'icon-folder';
    }

    if (
      children?.length ||
      this.data.node_type === ExplodedTreeNodeType.INCLUDE_ENTITY_TYPE ||
      (children?.length && this.data.node_type && ExplodedTreeNodeType.ATTRIBUTE_TREE_NODE === this.data.node_type)
    ) {
      return 'icon-atributos';
    }

    return 'icon-agrupaciones';
  }

  generateActions() {
    if (this.root?.actionsFilter) {
      return this.root?.actionsFilter(this);
    }

    return [];
  }

  generateFieldPath(): string | undefined {
    const path = [];

    path.push(this.root?.fieldPathFilter ? this.root?.fieldPathFilter(this) : this.data.field_path);

    return path.join('');
  }

  generateOfferPath(root?: boolean): string {
    const path = [];

    if (!root) {
      path.push(this.root?.offerPath);
    }

    path.push(this.root?.offerPathFilter ? this.root?.offerPathFilter(this) : this.getOfferPath());

    return path.join('');
  }

  generateVisualPath(includeRoot = false) {
    let path = '';

    if (this.parent && (!this.parent.isRoot || includeRoot)) {
      path += this.parent.generateVisualPath(includeRoot);
    }
    path += `/${this.data.label || this.data.name}`;

    return path;
  }

  getVisualPath(includeRoot = false) {
    return this.generateVisualPath(includeRoot);
  }

  getOfferPath(parent?: boolean): string {
    if (!parent && this.data.offer_path && this.data.offer_path !== '/') {
      return this.data.offer_path || '';
    }

    if (parent && this.data.node_type === ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE) {
      return this.data.offer_path || '';
    }

    if (this.parent && this.parent.getOfferPath(true)) {
      return this.parent.getOfferPath(true);
    }

    return this.data.offer_path || '';
  }

  activate() {
    if (this.root) {
      this.root.active = this;
    }
  }

  click() {
    if (this.isEditable) {
      return;
    }

    if (this.data.children?.length) {
      this.collapsed = !this.collapsed;
    }

    this.clickNode.next({
      target: this,
      data: this.data,
    });
  }

  isClickable() {
    /*if (this.root?.clickFilter) {
      return this.root?.clickFilter(this);
    }*/
    return true;
  }

  collapseOthers(current: ExplodedTreeNodeComponent) {
    this.nodes?.forEach((node) => node.data !== current.data && (node.collapsed = true));
  }

  expand() {
    this.collapsed = false;
    this.change.markForCheck();
  }

  collapse() {
    this.collapsed = true;
    this.change.markForCheck();
  }

  collapseParent() {
    this.parent?.collapse();
  }

  setError(message: string) {
    this.data.children = [
      {
        icon: 'icon-icon-warning',
        node_type: ExplodedTreeNodeType.ERROR,
        label: this.translate.instant(message),
      },
    ];

    this.expand();
  }

  closestExpand() {
    this.expand();
    this.parent?.closestExpand();
  }

  setWarning(message: string) {
    this.data.children = [
      {
        icon: 'icon-icon-warning',
        node_type: ExplodedTreeNodeType.WARNING,
        label: this.translate.instant(message),
      },
    ];

    this.expand();
  }

  clearAllMessages() {
    this.clearMessages(ExplodedTreeNodeType.ERROR);
    this.clearMessages(ExplodedTreeNodeType.WARNING);
  }

  clearMessages(type: ExplodedTreeNodeType.ERROR | ExplodedTreeNodeType.WARNING) {
    if (this.data.children) {
      const index = this.data.children?.findIndex((item) => item.node_type === type);
      if (index > -1) {
        this.data.children?.splice(index, 1);
      }
    }
  }

  setLoading(loading: boolean) {
    this.loading = loading;
    this.change.markForCheck();
  }

  isVisible(child: ExplodedTreeNode<P>) {
    if (child.node_type === ExplodedTreeNodeType.ERROR) {
      return true;
    }

    if (this.closestParam('fixed', true)) {
      return true;
    }

    return (this.root?.showFilter && this.root?.showFilter(child)) || !this.root?.showFilter;
  }

  isStatic() {
    return false;
  }

  closestParam(attr: string, value?: any): boolean {
    let find = null;

    const data = this.data as unknown as { [key: string]: any };
    find = data[attr];

    if (find && value) {
      find = find === value;
    }

    if (!find && this.parent) {
      find = this.parent.closestParam(attr, value);
    }

    return !!find;
  }

  checkTruncated(label: HTMLElement) {
    const range = document.createRange();
    range.selectNodeContents(label.firstChild as HTMLElement);
    if (!this.truncated && range.getBoundingClientRect().width > label.clientWidth) {
      this.truncated = true;
      this.change.markForCheck();
    }
  }

  update() {
    this.generateStatics();

    this.change.markForCheck();
    this.nodes?.forEach((node) => node.update());
  }

  isCollapsed() {
    if (this.root?.visualPaths.length) {
      for (const path of this.root.visualPaths) {
        if (path.startsWith(this.getVisualPath(true))) {
          this.collapsed = false;
          return false;
        }
      }
    }

    return this.collapsed && !this.expanded;
  }

  editMode(status: boolean) {
    this.editable = status;
    this.change.markForCheck();
    setTimeout(() => {
      if (this.inputRef) {
        this.renderer.selectRootElement(this.inputRef.nativeElement).focus();
      }
    });
  }

  handleCancelIcon() {
    this.canSave = true;
    if (this.inputRef.nativeElement && this.data.value) {
      this.parent?.collapse();
      this.inputRef.nativeElement.value = '';
    } else {
      this.collapse();
      this.editMode(false);
    }
  }

  input(event: KeyboardEvent) {
    this.canSave = true;

    if (event.key === 'Enter') {
      this.save();
    }
    if (event.key === 'Escape') {
      this.handleCancelIcon();
    }
  }

  save(event?: MouseEvent) {
    if (!this.isEditable) {
      return false;
    }

    if (event) {
      event.stopPropagation();
    }

    this.editMode(false);
    this.canSave = false;
    this.root?.changeNode.next({
      target: this,
      data: this.data,
      value: this.inputRef.nativeElement.value || this.data.placeholder,
      type: 'editor',
    });
    this.renderer.selectRootElement(this.inputRef.nativeElement).blur();
  }

  clickAction(action: ExplodedTreeAction, event?: MouseEvent) {
    this.root?.clickAction.next({
      target: this,
      data: this.data,
      action,
      nativeEvent: event,
    });
  }

  getExpandedNodes() {
    let nodes: ExplodedTreeNodeComponent[] = [];

    this.nodes?.forEach((node) => {
      nodes = nodes.concat(node.getExpandedNodes());
    });

    if (!this.collapsed) {
      nodes.push(this);
    }

    return nodes;
  }

  getActiveNode() {
    let node!: ExplodedTreeNodeComponent;

    if (this.actived) {
      node = this;
    }

    this.nodes?.forEach((n) => {
      if (!node) {
        node = n.getActiveNode();
      }
    });

    return node;
  }

  blur() {
    if (this.parent?.collapsed) {
      return false;
    }
    this.focused = false;
    if (!this.isEditable) {
      return false;
    }
    setTimeout(() => {
      this.save();
    }, 30);
  }

  remove() {
    const children = this.parent?.data.children || this.root?.tree || [];
    const index = children.findIndex((child) => (child instanceof ExplodedTreeNodeComponent ? child.data : child) === this.data);
    if (index > -1) {
      children?.splice(index, 1);
      this.root?.removeNode.next(this);
      this.update();
    }
  }
}
