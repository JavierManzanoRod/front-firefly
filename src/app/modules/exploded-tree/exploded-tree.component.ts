import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Inject,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { ExplodedTreeNodeComponent } from './components/exploded-tree-node/exploded-tree-node.component';
import { ExplodedClickEvent, ExplodedTree, ExplodedTreeAction, ExplodedTreeNode } from './models/exploded-tree.model';

@Component({
  selector: 'ff-exploded-tree',
  templateUrl: './exploded-tree.component.html',
  styleUrls: ['./exploded-tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExplodedTreeComponent<P = any> implements OnInit, AfterViewInit {
  @Input() height = 39;
  @Input() loading = false;
  @Input() tree: ExplodedTree<P>[] = [];
  @Input() hideSearch = false;
  @Input() searchPlaceholder = 'EXPLODED_TREE.SEARCH_ATTRIBUTES';
  @Input() searchInfoText = 'EXPLODED_TREE.SEARCH_INFO';
  @Input() autoCollapse = false;
  @Input() rootAutoCollapse = true;
  @Input() draggable = false;
  @Input() rootCollaspable = false;
  @Input() offerPath = '';
  @Input() expanded = false;
  @Input() highlight = '';
  @Input() filterItems: string[] = [];
  @Input() rootCollapsed = false;
  @Input() filterHidden = false;
  @Input() initialExpandPaths = [];
  @Input() disableActived = false;
  @Input() nodeEditable = false;
  @Input() hiddenMode = false;
  @Input() ignoreId = false;
  @Input() filterMultiple = false;
  @Input() hiddenShowAll = false;
  @Input() cannotTruncated = false;

  @Input()
  @HostBinding('class.horizontalScroll')
  enableHorizontalScroll = true;

  @Input() showFilter: ((item: ExplodedTreeNode<P>) => boolean) | undefined;
  @Input() dragFilter: ((item: ExplodedTreeNodeComponent<P>) => boolean) | undefined;
  @Input() clickFilter: ((item: ExplodedTreeNodeComponent<P>) => boolean) | undefined;
  @Input() iconFilter: ((item: ExplodedTreeNodeComponent<P>) => string) | undefined;
  @Input() fieldPathFilter: ((item: ExplodedTreeNodeComponent<P>) => string) | undefined;
  @Input() offerPathFilter: ((item: ExplodedTreeNodeComponent<P>) => string) | undefined;
  @Input() badgeFilter: ((item: ExplodedTreeNodeComponent<P>) => string) | undefined;
  @Input() classFilter: ((item: ExplodedTreeNodeComponent<P>) => string) | undefined;
  @Input() dataFilter: ((item: ExplodedTreeNodeComponent<P>) => ExplodedTreeNode) | undefined;
  @Input() editableFilter: ((item: ExplodedTreeNodeComponent<P>) => ExplodedTreeNode) | undefined;
  @Input() actionsFilter: ((item: ExplodedTreeNodeComponent<P>) => ExplodedTreeAction[]) | undefined;

  @Output() clickNode = new EventEmitter<ExplodedClickEvent<ExplodedTreeNodeComponent<P>, P>>();
  @Output() searchNodes = new EventEmitter();
  @Output() filterNodes = new EventEmitter<string | string[]>();
  @Output() clearFilter = new EventEmitter();
  @Output() changeNode = new EventEmitter();
  @Output() clickAction = new EventEmitter();
  @Output() removeNode = new EventEmitter();

  @ViewChild('searcher') searcher: ElementRef<HTMLInputElement> | null = null;
  @ViewChild('searcherProduct') searcherProduct: ElementRef<HTMLInputElement> | null = null;
  @ViewChild('scroll') scroll: ElementRef<HTMLDivElement> | null = null;

  @ViewChildren(ExplodedTreeNodeComponent) nodes: QueryList<ExplodedTreeNodeComponent<P>> | null = null;

  active: ExplodedTreeNodeComponent<P> | null = null;
  prevActive: ExplodedTreeNodeComponent<P> | null = null;
  index = 0;
  filterOpen = false;
  filterValue = '';
  filterSelect = '';
  filterSeletMultiple: string[] = [];
  enter = false;

  visualPaths: string[] = [];

  constructor(@Inject(DOCUMENT) private document: Document, private change: ChangeDetectorRef) {}

  ngOnInit() {
    this.updateVisualPaths(this.initialExpandPaths);
  }

  ngAfterViewInit(): void {
    const searcher = this.searcher?.nativeElement;
    const searcherProduct = this.searcherProduct?.nativeElement;

    if (searcher) {
      fromEvent(searcher, 'keyup')
        .pipe(
          filter((e: any) => e.keyCode !== 13 && !this.enter),
          debounceTime(1000),
          distinctUntilChanged()
        )
        .subscribe(() => {
          if (!this.enter) {
            this.search();
          }
        });
    }

    if (searcherProduct) {
      fromEvent<KeyboardEvent>(searcherProduct, 'keyup')
        .pipe(distinctUntilChanged())
        .subscribe((e: KeyboardEvent) => {
          if (e.key === 'Enter') {
            this.filter(this.filterSelect || this.getNavigationFilterValue());
          }
          this.change.markForCheck();
        });
    }

    fromEvent<KeyboardEvent>(this.document.body, 'keydown')
      .pipe(distinctUntilChanged())
      .subscribe((e: KeyboardEvent) => {
        if (this.filterOpen) {
          if (['ArrowDown', 'ArrowUp'].includes(e.key)) {
            this.filterSelect = this.getNavigationFilterValue(e.key === 'ArrowDown' ? 1 : -1);
            e.preventDefault();
            this.change.markForCheck();
          }
        }
      });

    fromEvent<MouseEvent>(this.document.body, 'mousedown').subscribe((e: MouseEvent) => {
      const target = e.target as HTMLElement;

      if (!target.closest('.dropdown.filter')) {
        this.filterOpen = false;
        this.change.markForCheck();
      }
    });
  }

  getNavigationFilterValue(direction = 1) {
    const visibles = this.filterItems.filter((item) => this.showFilterItem(item));
    let index = visibles.findIndex((item) => item === this.filterSelect) + direction;

    index = Math.min(Math.max(index, 0), visibles.length - 1);

    let find = false;
    while (find === false && index < visibles.length) {
      if (visibles[index]) {
        find = true;
      } else {
        index++;
      }
    }

    if (this.scroll) {
      const scroll = this.scroll.nativeElement;
      const height = 33;
      const scrollTop = (index + (this.searcherProduct?.nativeElement.value ? 0 : 1)) * 33;

      if (scrollTop < scroll.scrollTop) {
        scroll.scrollTop = scrollTop;
      }
      if (scrollTop + height > scroll.clientHeight + scroll.scrollTop) {
        scroll.scrollTop = scrollTop + height - scroll.clientHeight;
      }
    }

    return visibles[index];
  }

  click(event: ExplodedClickEvent<ExplodedTreeNodeComponent, P>) {
    this.clearVisualPaths();
    this.collapseDepth();

    this.prevActive = this.active;
    event.target.activate();

    if (this.autoCollapse) {
      if (event.target?.parent) {
        event.target?.parent?.collapseOthers(event.target);
      } else if (this.rootAutoCollapse) {
        this.nodes?.forEach((node) => node.data !== event.data && (node.collapsed = true));
      }
    }

    if ((this.clickFilter && this.clickFilter(event.target)) || !this.clickFilter) {
      this.clickNode.emit(event);
    }

    this.update();
  }

  search() {
    const value = this.searcher?.nativeElement?.value.trim();
    this.filterSeletMultiple = [];
    if ((value && value?.length > 2) || !value) {
      if (this.filterMultiple && !value) {
        this.filterSeletMultiple = this.filterItems.filter((v) => v.toLocaleLowerCase().includes(value?.toLocaleLowerCase() || ''));
        this.filterValue = this.filterSeletMultiple.join(', ');
      }
      if (!value) {
        this.clear();
      }
      if (value && this.filterMultiple && this.filterValue.length) {
        this.filterValue = '';
      }
      this.searchNodes.next(value);
    }

    setTimeout(() => {
      this.enter = false;
    }, 1500);
  }

  enterSearch() {
    this.enter = true;
    this.search();
  }

  filter(key: string) {
    if (!this.filterMultiple) {
      this.filterNodes.next((this.filterSelect = this.filterValue = key));
      this.filterOpen = false;
    } else {
      const indexKey = this.filterSeletMultiple.findIndex((v) => v === key);
      if (indexKey >= 0) {
        this.filterSeletMultiple.splice(indexKey, 1);
      } else {
        this.filterSeletMultiple.push(key);
      }
      if (this.searcher) {
        this.searcher.nativeElement.value = '';
      }
      this.filterNodes.next(this.filterSeletMultiple);
      this.filterValue = this.filterSeletMultiple.join(', ');
    }
  }

  isVisibleInViewport(top: number): boolean {
    return true;
  }

  expandDepth() {
    this.expanded = true;
  }

  collapseDepth(index?: number) {
    if (!index) {
      this.expanded = false;
    }

    if (index === 1 && this.nodes && this.nodes.first) {
      this.nodes.first.nodes?.forEach((n) => (n.collapsed = true));
    }
  }

  toggleFilter() {
    this.filterOpen = !this.filterOpen;
    this.filterSelect = '';

    if (this.searcherProduct) {
      if (this.filterOpen) {
        this.searcherProduct?.nativeElement.focus();
      }
      this.searcherProduct.nativeElement.value = '';
    }
  }

  showFilterItem(item: string) {
    return !!new RegExp(`${this.searcherProduct?.nativeElement.value}`, 'i').exec(item || '');
  }

  clear() {
    this.filterOpen = false;

    if (this.loading) {
      return;
    }

    if (this.searcher) {
      this.searcher.nativeElement.value = '';
    }

    if (this.clearFilter.observers.length) {
      this.filterSelect = this.filterValue = '';
      this.filterSeletMultiple = [];

      this.clearFilter.next(null);
    } else {
      this.filter('');
      this.searchNodes.next(null);
    }
  }

  update() {
    this.nodes?.forEach((node) => node.update());
  }

  updateVisualPaths(visualPaths: string[]) {
    this.visualPaths = visualPaths;
  }

  getExpandedNodes() {
    let nodes: ExplodedTreeNodeComponent[] = [];

    this.nodes?.forEach((node) => {
      nodes = nodes.concat(node.getExpandedNodes());
    });

    return nodes;
  }

  getActiveNode() {
    let node!: ExplodedTreeNodeComponent;

    this.nodes?.forEach((n) => {
      if (!node) {
        node = n.getActiveNode();
      }
    });

    return node;
  }

  clearVisualPaths() {
    this.visualPaths = [];
    this.change.markForCheck();
  }

  input() {
    this.change.markForCheck();
  }
}
