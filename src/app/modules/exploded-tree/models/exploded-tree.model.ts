import { GenericApiResponse } from '@model/base-api.model';
import { DndDropEvent } from 'ngx-drag-drop';

export enum ExplodedTreeType {
  OFFER = 'OFFER',
  PRODUCT_TYPE = 'PRODUCT_TYPE',
  ENTITY_TYPE = 'ENTITY_TYPE',
}

export enum ExplodedTreeNodeType {
  ATTRIBUTE_TREE_NODE = 'ATTRIBUTE_TREE_NODE',
  ATTRIBUTE = 'ATTRIBUTE',
  COMPOSITE_FIELD = 'COMPOSITE_FIELD',
  INCLUDE_PRODUCT_TYPE = 'INCLUDE_PRODUCT_TYPE',
  INCLUDE_ENTITY_TYPE = 'INCLUDE_ENTITY_TYPE',
  ERROR = 'ERROR',
  WARNING = 'WARNING',
}

export enum ExplodedTreeElementType {
  EMBEDDED = 'EMBEDDED',
  ENTITY = 'ENTITY',
  STRING = 'STRING',
  DOUBLE = 'DOUBLE',
  BOOLEAN = 'BOOLEAN',
  VIRTUAL = 'VIRTUAL',
  DATE_TIME = 'DATE_TIME',
}

export type ExplodedTreeResponse = GenericApiResponse<ExplodedTree | ExplodedTreeNode>;

export type ExplodedTree<V = any> = {
  icon?: string;
  class?: string;
  badge?: string;
  name?: string;
  id?: string;
  tooltip?: string;
  identifier?: string;
  id_product?: string;
  label?: string;
  tree_type?: ExplodedTreeType;
  children?: ExplodedTreeNode<V>[];
  node_type?: ExplodedTreeNodeType;
  fixed?: boolean;
  value?: V;
};

export type ExplodedTreeNode<V = any> = {
  node_id?: string;
  identifier?: string;
  offer_path?: string;
  field_path?: string;
  placeholder?: string;
  node_type?: ExplodedTreeNodeType;
  element_type?: ExplodedTreeElementType;
  i18n?: boolean;
  data?: { [key: string]: any };
} & ExplodedTree<V> & { [key: string]: any };

export type ExplodedTreeAction = {
  type: string;
  icon?: string;
  label?: string;
  visible?: boolean;
  tooltip?: string;
};

export interface ExplodedEvent<T, V = any> {
  data: ExplodedTreeNode<V>;
  target: T;
}
export interface ExplodedChangeEvent<T, V = any> {
  value: any;
  type: string;
  data: ExplodedTreeNode<V>;
  target: T;
}
export interface ExplodedActionEvent<T, V = any> {
  target: T;
  data: ExplodedTreeNode<V>;
  action: ExplodedTreeAction;
  nativeEvent: MouseEvent;
}

export type ExplodedClickEvent<T, V = any> = ExplodedEvent<T, V>;

export interface ExplodedDropEvent<T> extends DndDropEvent {
  data?: T;
}
