import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { InputSearchComponent } from './input-search.component';

@NgModule({
  declarations: [InputSearchComponent],
  imports: [CommonModule, TranslateModule, ReactiveFormsModule],
  exports: [InputSearchComponent],
})
export class InputSearchModule {}
