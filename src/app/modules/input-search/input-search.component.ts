import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'ff-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputSearchComponent),
      multi: true,
    },
  ],
})
export class InputSearchComponent<T> implements OnInit, ControlValueAccessor {
  @Input() set searcherFn(fn: (val: string) => Observable<T[]>) {
    if (fn) {
      this._searcherFn = fn;
    }
  }
  get searcherFn() {
    return this._searcherFn;
  }
  _searcherFn!: (val: string) => Observable<T[]>;
  found: T[] | undefined;
  disabled = false;
  fetching = false;
  public searchInput = new FormControl('');
  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {
    this.searchInput.valueChanges
      .pipe(
        filter(() => !this.disabled),
        debounceTime(400),
        distinctUntilChanged(),
        tap((val: string) => {
          this.fetching = true;
          this.changeDetector.markForCheck();
        }),
        switchMap((input) =>
          input
            ? this.searcherFn(input).pipe(
                catchError((err) => {
                  this.searchInput.setErrors({ failedRequest: 'Error recuperando datos de producto' });
                  return of(undefined);
                })
              )
            : of(undefined)
        )
      )
      .subscribe(
        (val) => {
          this.found = val;
          this.fetching = false;
          const resp = this.found?.length
            ? {
                searchParam: this.searchInput.value,
                searchResult: this.found,
              }
            : null;
          this.onChange(resp);
          this.changeDetector.markForCheck();
        },
        (err) => {
          this.fetching = false;
          this.searchInput.setErrors({ failedRequest: 'Error recuperando datos de producto' });
          this.changeDetector.markForCheck();
        },
        () => {
          this.fetching = false;
          this.changeDetector.markForCheck();
        }
      );
  }

  get failedRequest() {
    return this.searchInput.errors?.failedRequest !== undefined;
  }

  get productErrors() {
    return Object.keys(this.searchInput.errors || {}).length > 0;
  }
  get error() {
    return (this.productErrors || this.emptySearch) && !this.fetching && this.searchInput.dirty;
  }
  get emptySearch() {
    return this.found && !this.found.length;
  }
  get success() {
    return !this.productErrors && !this.emptyResponse && !this.emptySearch && !this.fetching && this.searchInput.dirty;
  }
  get emptyResponse() {
    return this.searchInput.value === '';
  }
  onTouch = () => {};
  onChange = (val: any) => {};
  writeValue(items: { searchParam: string; searchResult: T[] }): void {
    if (typeof items === 'string') {
      this.searchInput.setValue(items);
    } else if (items === null) {
      this.searchInput.reset();
    } else {
      this.found = items?.searchResult;
      //Avoid search if we have all data;
      const options = { emitEvent: Array.isArray(items?.searchResult) ? false : undefined };
      this.searchInput.setValue(items?.searchParam, options);
    }
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    if (this.disabled) {
      this.searchInput.disable();
    } else {
      this.searchInput.enable();
    }
  }
}
