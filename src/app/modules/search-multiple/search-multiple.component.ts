import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ConfigMultiple } from './models/config.models';

@Component({
  selector: 'ff-search-multiple',
  templateUrl: './search-multiple.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./search-multiple.component.scss'],
})
export class SearchMultipleComponent implements OnInit {
  @Input() config: ConfigMultiple[] = [];
  @Input() numberItems = 0;
  @Input() loading = false;
  @Input() value: any = {};
  @Output() search = new EventEmitter<any>();

  form = new FormGroup({});
  clear = false;

  constructor() {}

  ngOnInit(): void {
    this.config.forEach((conf) => {
      this.form.addControl(conf.key, new FormControl(this.value ? this.value[conf.key] : null));
    });
  }

  submit() {
    if (this.clear) {
      this.form.reset();
    }

    this.search.emit(this.form.value);
    this.clear = !this.clear;
  }

  valueChange() {
    this.clear = false;
  }
}
