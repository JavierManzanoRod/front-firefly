import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SharedModule } from 'src/app/shared/shared.module';
import { SearchMultipleComponent } from './search-multiple.component';

@NgModule({
  declarations: [SearchMultipleComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule, BsDatepickerModule.forRoot(), NgSelectModule],
  exports: [SearchMultipleComponent],
})
export class SearchMultipleModule {}
