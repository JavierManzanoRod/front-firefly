import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { SearchMultipleComponent } from './search-multiple.component';

describe('SearchMultipleComponent', () => {
  let component: SearchMultipleComponent;
  let fixture: ComponentFixture<SearchMultipleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchMultipleComponent],
      imports: [TranslateModule.forRoot()],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
