export interface ConfigMultiple {
  key: string;
  type: 'TEXT' | 'SELECT' | 'DATE';
  class?: string;
  placeHolder?: string;
  label?: string;
  selectOptions?: {
    label: string;
    value: any;
  }[];
}
