import { ChangeDetectionStrategy, Component, ContentChild, Input, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { appearOppacity } from './animations/appear-oppacity';
import { AccordionErrorDirective } from './directives/accordion-error.directive';
import { AccordionLoadingDirective } from './directives/accordion-loading.directive';
import { AccordionRowDirective } from './directives/accordion-row.directive';
import { AccordionTitleDirective } from './directives/accordion-title.directive';

export interface AccordionItem {
  expanded: boolean;
  title: unknown;
  row: Observable<unknown>;
}

@Component({
  selector: 'ff-accordion-list',
  templateUrl: './accordion-list.component.html',
  styleUrls: ['./accordion-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [appearOppacity()],
})
export class AccordionListComponent<T> implements OnInit, OnDestroy {
  @Input() set items(items: [T, Observable<any>][]) {
    if (items) {
      this.data = [...items];
    }
  }
  @Input() multiple = false;
  @Input() cache = false;
  @ContentChild(AccordionTitleDirective, { read: TemplateRef }) titleTemplate: any;
  @ContentChild(AccordionRowDirective, { read: TemplateRef }) rowTemplate: any;
  @ContentChild(AccordionLoadingDirective, { read: TemplateRef }) loadingTemplate: any;
  @ContentChild(AccordionErrorDirective, { read: TemplateRef }) errorTemplate: any;
  public accordionItems!: AccordionItem[];
  public data!: [T, Observable<any>][];
  private destroy$ = new Subject();
  constructor() {}

  ngOnInit(): void {
    this.accordionItems = this.data?.map((item, index) => ({
      expanded: false,
      title: item[0],
      row: item[1].pipe(
        takeUntil(this.destroy$),
        this.cache ? shareReplay(1) : tap(),
        map((v) => {
          item[0] = {
            ...item[0],
            rows: v?.length,
          };
          this.accordionItems[index].title = item[0];
          return v;
        })
      ),
    }));
  }
  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
  public toggle(i: number) {
    const selected = this.accordionItems[i];
    if (!selected.expanded && !this.multiple) {
      this.accordionItems.forEach((item) => (item.expanded = false));
    }
    selected.expanded = !selected.expanded;
  }
}
