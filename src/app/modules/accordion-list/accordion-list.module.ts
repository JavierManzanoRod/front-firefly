import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { AccordionListComponent } from './accordion-list.component';
import { AccordionErrorDirective } from './directives/accordion-error.directive';
import { AccordionLoadingDirective } from './directives/accordion-loading.directive';
import { AccordionRowDirective } from './directives/accordion-row.directive';
import { AccordionTitleDirective } from './directives/accordion-title.directive';
import { WithLoadingPipe } from './pipe/with-loading.pipe';

@NgModule({
  declarations: [
    WithLoadingPipe,
    AccordionListComponent,
    AccordionErrorDirective,
    AccordionLoadingDirective,
    AccordionRowDirective,
    AccordionTitleDirective,
  ],
  imports: [CommonModule, TranslateModule],
  exports: [
    WithLoadingPipe,
    AccordionListComponent,
    AccordionErrorDirective,
    AccordionLoadingDirective,
    AccordionRowDirective,
    AccordionTitleDirective,
  ],
})
export class AccordionListModule {}
