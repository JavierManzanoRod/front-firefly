import { trigger, transition, query, style, stagger, animate } from '@angular/animations';

export function appearOppacity(staggerDuration = 50, animationDuration = 100) {
  return trigger('appearOppacity', [
    transition('* => *', [
      query(':enter', [style({ opacity: 0 }), stagger(staggerDuration, [animate(animationDuration, style({ opacity: 1 }))])], {
        optional: true,
      }),
    ]),
  ]);
}
