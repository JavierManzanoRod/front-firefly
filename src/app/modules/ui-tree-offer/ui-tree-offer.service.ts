import { Injectable } from '@angular/core';
import { AttributeEntityType, EntityType } from '@core/models/entity-type.model';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { Conditions2OperatorsService } from '../conditions2/services/conditions2.operators.service';
import { UITreeListOffer } from './ui-tree-offer.model';

@Injectable({
  providedIn: 'root',
})
export class UITreeOfferService {
  constructor(private entityTypeApiService: EntityTypeService, private operatorsService: Conditions2OperatorsService) {}

  entityTypeAsTree(entityType: EntityType, draggable: boolean, parentsDragabble = false): UITreeListOffer | null {
    return {
      value: entityType as any,
      icon: 'icon-folder',
      childrens: entityType.attributes.map((att) => this.asTree(att, draggable, parentsDragabble)),
    };
  }

  public asTree(attribute: AttributeEntityType, draggable: boolean, parentsDragabble = false) {
    const availableOperators = this.operatorsService.getAvailableOperators(attribute.data_type, attribute.multiple_cardinality);
    const result: UITreeListOffer = {
      value: {
        ...attribute,
        node_type: availableOperators && availableOperators[0],
        attribute_id: attribute.id,
        attribute_data_type: attribute.data_type,
        attribute_label: attribute.label,
        tooltip_label: attribute.tooltip_label ? attribute.tooltip_label + '/' + attribute.label : attribute.label,
      },
      icon: 'icon-agrupaciones',
      draggable: attribute.data_type !== 'EMBEDDED',
    };
    if (attribute?.data_type === 'EMBEDDED' || attribute?.data_type === 'ENTITY') {
      result.loadChildrens = (el: AttributeEntityType) => this.loadEntityTypeAttributeChildrens(el, draggable, parentsDragabble);
      result.icon = attribute.multiple_cardinality ? 'icon-icon-multiple' : 'icon-icon-atributos';
    }
    return result;
  }

  private loadEntityTypeAttributeChildrens(attributeToExpand: AttributeEntityType, draggable: boolean, parentsDragabble: boolean) {
    if (attributeToExpand.entity_reference) {
      return this.entityTypeApiService.detail(attributeToExpand.entity_reference).pipe(
        map((entityType) => {
          return entityType.attributes.map((att) => this.asTree(att, draggable, parentsDragabble));
        })
      );
    }
    return of([]);
  }
}
