import { AttributeEntityType } from '@core/models/entity-type.model';
import { Observable } from 'rxjs';

export interface UITreeListOffer {
  value: AttributeEntityType;
  icon?: string;
  hide?: boolean;
  isOpen?: boolean;
  draggable?: boolean;
  childrens?: UITreeListOffer[];
  loadChildrens?: (value: AttributeEntityType) => Observable<UITreeListOffer[]>;
}
