import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';

import { UiTreeOfferComponent } from './ui-tree-offer.component';

xdescribe('UiTreeOfferComponent', () => {
  let component: UiTreeOfferComponent;
  let fixture: ComponentFixture<UiTreeOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UiTreeOfferComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        { provide: EntityTypeService },
        {
          provide: HttpClient,
          useFactory: () => ({
            error: () => {},
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiTreeOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
