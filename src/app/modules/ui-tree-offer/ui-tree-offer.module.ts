import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { UiTreeListModule } from '../ui-tree-list/ui-tree-list.module';
import { UiTreeOfferComponent } from './ui-tree-offer.component';

@NgModule({
  declarations: [UiTreeOfferComponent],
  imports: [CommonModule, UiTreeListModule, TranslateModule],
  exports: [UiTreeOfferComponent],
})
export class UiTreeOfferModule {}
