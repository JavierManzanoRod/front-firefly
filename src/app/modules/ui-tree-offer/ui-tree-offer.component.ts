import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProductTypeList2StateService } from 'src/app/catalog/product-type/components/product-type-list-2/product-type-list2.state.service';
import { configuration } from 'src/app/configuration/configuration';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { UITreeListOffer } from './ui-tree-offer.model';
import { UITreeOfferService } from './ui-tree-offer.service';

@Component({
  selector: 'ff-ui-tree-offer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './ui-tree-offer.component.html',
  styleUrls: ['./ui-tree-offer.component.scss'],
  providers: [ProductTypeList2StateService],
})
export class UiTreeOfferComponent implements OnInit {
  @Output() clicked: EventEmitter<UITreeListOffer> = new EventEmitter<UITreeListOffer>();
  @Output() collapsed: EventEmitter<UITreeListOffer> = new EventEmitter<UITreeListOffer>();
  @Output() expanded: EventEmitter<UITreeListOffer> = new EventEmitter<UITreeListOffer>();
  @Output() loadError: EventEmitter<any> = new EventEmitter<any>();
  @Input() draggable = false;
  @Input() parentsDragabble = false;
  @Input() calculeItemsPath = false;

  item: UITreeListOffer | null = null;

  // TODO: Añadir outputs del ref-list
  constructor(
    private entityTypeApiService: EntityTypeService,
    private uiTreeOfferService: UITreeOfferService,
    private ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.entityTypeApiService.detail(configuration.offerEntityType || '').subscribe((v) => {
      this.item = this.uiTreeOfferService.entityTypeAsTree(v, this.draggable, this.parentsDragabble);
      this.ref.markForCheck();
    });
  }
}
