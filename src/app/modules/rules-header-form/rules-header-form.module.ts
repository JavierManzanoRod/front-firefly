import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ControlPanelSharedModule } from 'src/app/control-panel/shared/control-panel-shared.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConditionsBasicModule } from '../conditions-basic/conditions-basic.module';
import { Conditions2Module } from '../conditions2/conditions2.module';
import { DatetimepickerModule } from '../datetimepicker/datetimepicker.module';
import { FormErrorModule } from '../form-error/form-error.module';
import { SwitchFieldTypesModule } from './components/switch-field-types.module';
import { RulesHeaderFormComponent } from './rules-header-form.component';

@NgModule({
  declarations: [RulesHeaderFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    SimplebarAngularModule,
    FormsModule,
    Conditions2Module,
    ControlPanelSharedModule,
    FormErrorModule,
    ReactiveFormsModule,
    DatetimepickerModule,
    ConditionsBasicModule,
    SwitchFieldTypesModule,
  ],
  exports: [RulesHeaderFormComponent],
})
export class RulesHeaderFormModule {}
