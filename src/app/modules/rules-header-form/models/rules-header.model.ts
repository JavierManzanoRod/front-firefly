import { ValidatorFn } from '@angular/forms';
import { ChipsControlSelectConfig } from '../../chips-control/components/chips.control.model';

export interface IRulesHeaderProperties {
  label: string;
  name: string;
  type: IRulesHeaderPropertyTypes;
  class?: string;
  order?: number;
  placeholder?: string;
  validators?: ValidatorFn[];
  errorMessages?: IErrorMessages[];
  required?: boolean;
  maxLength?: number;
  value?: any;
  isResult?: boolean;
  result?: string;
  modalConfig?: ChipsControlSelectConfig;
}

export enum IRulesHeaderPropertyTypes {
  input = 'input',
  numberInput = 'numberInput',
  datepicker = 'datepicker',
  modalSelect = 'modalSelect',
}

export interface IErrorMessages {
  type: string;
  message: string;
}
