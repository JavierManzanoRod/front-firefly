import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { IRulesHeaderProperties } from '../models/rules-header.model';

@Component({
  selector: 'ff-switch-field-types',
  templateUrl: './switch-field-types.component.html',
})
export class SwitchFieldTypesComponent implements OnInit {
  @Input() fieldProperties!: IRulesHeaderProperties;
  @Input() field!: FormControl;
  @Input() form!: FormGroup;

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    this.fieldProperties.placeholder = this.fieldProperties.placeholder ? this.translate.instant(this.fieldProperties.placeholder) : '';
  }
}
