import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ChipsControlModule } from '../../chips-control/chips-control.module';
import { DatetimepickerModule } from '../../datetimepicker/datetimepicker.module';
import { FormErrorModule } from '../../form-error/form-error.module';
import { SwitchFieldTypesComponent } from './switch-field-types.component';

@NgModule({
  declarations: [SwitchFieldTypesComponent],
  imports: [ReactiveFormsModule, CommonModule, DatetimepickerModule, TranslateModule, FormErrorModule, ChipsControlModule],
  exports: [SwitchFieldTypesComponent],
})
export class SwitchFieldTypesModule {}
