/* eslint-disable @typescript-eslint/dot-notation */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { Injectable } from '@angular/core';
import { RulesType } from '../../conditions-basic-and-advance/models/conditions-basic-and-advance.model';

@Injectable({
  providedIn: 'root',
})
export class DeserializeResultService {
  constructor() {}

  getResultObject(type: RulesType, serializedResult: string) {
    switch (type) {
      case RulesType.badge:
        return this.getBadgeResult(serializedResult);
      case RulesType.size_guide:
        return this.getSizeGuideResult(serializedResult);
      case RulesType.special_product:
        return this.getDoubleSerializable(type, serializedResult);
      case RulesType.fluorinated_gas:
        return this.getDoubleSerializable(type, serializedResult);
      default:
        return {};
        break;
    }
  }

  private getSizeGuideResult(serializedResult: string) {
    return JSON.parse(serializedResult);
  }

  private getBadgeResult(serializedResult: string) {
    return JSON.parse(serializedResult);
  }

  private getDoubleSerializable(type: RulesType, serializedResult: string) {
    const parse = JSON.parse(serializedResult);
    switch (type) {
      case RulesType.fluorinated_gas:
        return JSON.parse(parse['eci_fluor_gases']);
      case RulesType.special_product:
        return JSON.parse(parse['related_services']);
      default:
        return {};
    }
  }
}
