import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { dateTimeValidator } from 'src/app/shared/validations/date-time.validator';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../chips-control/components/chips.control.model';
import { RulesType } from '../../conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { IRulesHeaderProperties, IRulesHeaderPropertyTypes } from '../models/rules-header.model';
import { ExpertService } from 'src/app/control-panel/expert/services/expert.service';
@Injectable({
  providedIn: 'root',
})
export class RulesHeaderFormService {
  baseHeader: IRulesHeaderProperties[] = [
    {
      label: 'NAME',
      name: 'name',
      placeholder: 'CONDITIONS_BASIC_AND_ADVANCE.ENTER_RULE_NAME',
      class: 'form-control',
      validators: [Validators.required],
      type: IRulesHeaderPropertyTypes.input,
      required: true,
      errorMessages: [
        {
          type: 'required',
          message: 'CONDITIONS_BASIC_AND_ADVANCE.NAME_REQUIRED',
        },
        {
          type: '409',
          message: 'CONDITIONS_BASIC_AND_ADVANCE.NAME_DUPLICATED',
        },
      ],
    },
    {
      label: 'CONDITIONS_BASIC_AND_ADVANCE.START_DATE',
      name: 'start_date',
      type: IRulesHeaderPropertyTypes.datepicker,
      validators: [dateTimeValidator()],
      errorMessages: [
        {
          type: 'invalidDate',
          message: 'COMMON.ERROR_DATE_TIME_INVALID',
        },
      ],
    },
    {
      label: 'CONDITIONS_BASIC_AND_ADVANCE.END_DATE',
      name: 'end_date',
      type: IRulesHeaderPropertyTypes.datepicker,
      validators: [dateTimeValidator()],
      errorMessages: [
        {
          type: 'invalidDate',
          message: 'COMMON.ERROR_DATE_TIME_INVALID',
        },
      ],
    },
  ];

  constructor(private expertService: ExpertService) {}

  getRulesTypeHeader(type: RulesType): IRulesHeaderProperties[] {
    switch (type) {
      case RulesType.base_conditions:
        return this.getBaseRulesHeader();
      case RulesType.size_guide:
        return this.getSizeGuideRulesHeader();
      case RulesType.price_inheritance:
        return this.getBaseRulesHeader();
      case RulesType.content_group:
        return this.getBaseRulesHeader();
      case RulesType.admin_group:
        return this.getBaseRulesHeader();
      case RulesType.expert_admin:
        return this.getExpertAdminRulesHeader();
      case RulesType.special_product:
        return this.getSpecialProductRulesHeader();
      case RulesType.fluorinated_gas:
        return this.getFluorinatedGasRulesHeader();
      case RulesType.badge:
        return this.getBadgeRulesHeader();
      default:
        return this.baseHeader;
    }
  }

  private getBaseRulesHeader(): IRulesHeaderProperties[] {
    return this.baseHeader;
  }

  private getSizeGuideRulesHeader(): IRulesHeaderProperties[] {
    const sizeGuideRules = [...this.baseHeader];
    sizeGuideRules.push({
      label: 'SIZE_GUIDE.DETAIL.PRODUCT_URL',
      name: 'product_url',
      type: IRulesHeaderPropertyTypes.input,
      placeholder: 'SIZE_GUIDE.DETAIL.PRODUCT_URL_PLACEHOLDER',
      class: 'col-4',
      required: true,
      validators: [Validators.required],
      errorMessages: [
        {
          type: 'required',
          message: 'COMMON_ERRORS.REQUIRED',
        },
      ],
      isResult: true,
      result: 'size_guide_url',
    });
    return sizeGuideRules;
  }

  private getSpecialProductRulesHeader(): IRulesHeaderProperties[] {
    const specialProductRules = [...this.baseHeader];
    specialProductRules.push(
      {
        label: 'SPECIAL_PRODUCT.DETAIL.PRODUCT_NAME',
        name: 'product_name',
        type: IRulesHeaderPropertyTypes.input,
        placeholder: 'SPECIAL_PRODUCT.DETAIL.PLACEHODER_NAME_SPECIAL',
        class: 'col-4',
        required: true,
        validators: [Validators.required],
        errorMessages: [
          {
            type: 'required',
            message: 'COMMON_ERRORS.REQUIRED',
          },
        ],
        isResult: true,
        result: 'name',
      },
      {
        label: 'SPECIAL_PRODUCT.DETAIL.PRODUCT_TYPE',
        name: 'product_type',
        type: IRulesHeaderPropertyTypes.input,
        placeholder: 'SPECIAL_PRODUCT.DETAIL.PLACEHOLDER_PRODUCT_TYPE',
        class: 'col-4',
        required: true,
        validators: [Validators.required],
        errorMessages: [
          {
            type: 'required',
            message: 'COMMON_ERRORS.REQUIRED',
          },
        ],
        isResult: true,
        result: 'type',
      },
      {
        label: 'SPECIAL_PRODUCT.DETAIL.PRODUCT_URL',
        name: 'product_url',
        type: IRulesHeaderPropertyTypes.input,
        placeholder: 'SPECIAL_PRODUCT.DETAIL.PLACEHOLDER_PRODUCT_URL',
        class: 'col-4',
        required: true,
        validators: [Validators.required],
        errorMessages: [
          {
            type: 'required',
            message: 'COMMON_ERRORS.REQUIRED',
          },
        ],
        isResult: true,
        result: 'url',
      }
    );
    return specialProductRules;
  }

  private getFluorinatedGasRulesHeader(): IRulesHeaderProperties[] {
    const fluorGasRules = [...this.baseHeader];
    fluorGasRules.push(
      {
        label: 'FLUORINATED_GAS.DETAIL.PRODUCT_NAME',
        name: 'fluor_name',
        type: IRulesHeaderPropertyTypes.input,
        placeholder: 'FLUORINATED_GAS.DETAIL.PLACEHOLDER_PRODUCT_NAME',
        class: 'col-4',
        required: true,
        validators: [Validators.required],
        errorMessages: [
          {
            type: 'required',
            message: 'COMMON_ERRORS.REQUIRED',
          },
        ],
        isResult: true,
        result: 'name',
      },
      {
        label: 'FLUORINATED_GAS.DETAIL.PRODUCT_TYPE',
        name: 'fluor_type',
        type: IRulesHeaderPropertyTypes.input,
        placeholder: 'FLUORINATED_GAS.DETAIL.PLACEHOLDER_PRODUCT_TYPE',
        class: 'col-4',
        required: true,
        validators: [Validators.required],
        errorMessages: [
          {
            type: 'required',
            message: 'COMMON_ERRORS.REQUIRED',
          },
        ],
        isResult: true,
        result: 'type',
      },
      {
        label: 'FLUORINATED_GAS.DETAIL.PRODUCT_URL',
        name: 'fluor_url',
        type: IRulesHeaderPropertyTypes.input,
        placeholder: 'FLUORINATED_GAS.DETAIL.PLACEHOLDER_PRODUCT_URL',
        class: 'col-4',
        required: true,
        validators: [Validators.required],
        errorMessages: [
          {
            type: 'required',
            message: 'COMMON_ERRORS.REQUIRED',
          },
        ],
        isResult: true,
        result: 'url',
      }
    );
    return fluorGasRules;
  }

  private getExpertAdminRulesHeader() {
    const configExpertAdmin: ChipsControlSelectConfig = {
      label: 'EXPERT_ADMIN.DETAIL.EXPERT',
      modalType: ModalChipsTypes.select,
      modalConfig: {
        title: 'EXPERT_ADMIN.DETAIL.ENTER_EXPERT',
        multiple: false,
        searchFnOnInit: true,
        itemValueKey: 'name',
        searchFn: (criteria: any) => {
          return this.expertService.list(criteria);
        },
      },
    };
    const expertAdmin = [...this.baseHeader];
    expertAdmin.push({
      label: '',
      name: 'expert',
      required: true,
      type: IRulesHeaderPropertyTypes.modalSelect,
      modalConfig: configExpertAdmin,
    });
    return expertAdmin;
  }

  private getBadgeRulesHeader() {
    const badgeRules = [...this.baseHeader];
    badgeRules.push(
      {
        label: 'BADGE.DETAIL.PLP',
        name: 'plp',
        type: IRulesHeaderPropertyTypes.numberInput,
        placeholder: 'BADGE.DETAIL.PLACEHOLDER_PLP',
        class: 'col-4',
        required: false,
        validators: [],
        isResult: true,
        result: 'PLP',
      },
      {
        label: 'BADGE.DETAIL.PDP',
        name: 'pdp',
        type: IRulesHeaderPropertyTypes.numberInput,
        placeholder: 'BADGE.DETAIL.PLACEHOLDER_PDP',
        class: 'col-4',
        required: false,
        validators: [],
        isResult: true,
        result: 'PDP',
      }
    );
    return badgeRules;
  }
}
