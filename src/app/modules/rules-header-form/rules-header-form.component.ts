import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { RulesType } from '../conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { IRulesHeaderProperties } from './models/rules-header.model';
import { DeserializeResultService } from './services/deserialize-result.service';

@Component({
  selector: 'ff-rules-header-form',
  templateUrl: './rules-header-form.component.html',
  styleUrls: ['./rules-header-form.component.scss'],
})
export class RulesHeaderFormComponent implements OnInit {
  @Input() headerFormFields!: IRulesHeaderProperties[];
  @Input() form!: FormGroup;
  @Input() item: { [key: string]: any } = {};
  @Input() rulesType!: RulesType;
  @Input() readonly = false;

  deserializedResult: { [key: string]: any } = {};

  constructor(private deserializeSrv: DeserializeResultService) {}

  ngOnInit() {
    if (this.item.result) {
      this.deserializedResult = this.deserializeSrv.getResultObject(this.rulesType, this.item.result);
    }
    this.headerFormFields.forEach((field) => {
      this.form.addControl(field.name, new FormControl(this.getValue(field.name, field), field.validators ? field.validators : []), {
        emitEvent: false,
      });
    });

    if (this.rulesType === RulesType.badge) {
      this.form.addValidators([this.checkPlpAndPdp]);
    }
    if (this.readonly) {
      this.form.disable();
    }
  }

  private getValue(key: string, field: IRulesHeaderProperties) {
    if (field.isResult) {
      return this.deserializedResult[field.result as string];
    }
    return this.item[key] || null;
  }

  private checkPlpAndPdp: ValidatorFn = (control: AbstractControl): null | ValidationErrors => {
    const plp = control.get('plp')?.value;
    const pdp = control.get('pdp')?.value;
    return plp || pdp ? null : { plp_pdp_required: true };
  };
}
