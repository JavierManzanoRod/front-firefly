import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { IndividualConfig } from 'ngx-toastr/toastr/toastr-config';

@Injectable()
export class ToastService {
  constructor(private translate: TranslateService, private toastr: ToastrService) {}

  commonFormError(override?: Partial<IndividualConfig>) {
    this.showToast('error', 'COMMON_ERRORS.FORM_ERROR.MESSAGE', 'COMMON_ERRORS.FORM_ERROR.TITLE', override);
  }

  error(message?: string, title?: string, override?: Partial<IndividualConfig>) {
    this.showToast('error', message, title, override);
  }

  warning(message?: string, title?: string, override?: Partial<IndividualConfig>) {
    this.showToast('warning', message, title, override);
  }

  success(message?: string, title?: string, override?: Partial<IndividualConfig>) {
    this.showToast('success', message, title, override);
  }

  info(message?: string, title?: string, override?: Partial<IndividualConfig>) {
    this.showToast('info', message, title, override);
  }

  show(message?: string, title?: string, override?: Partial<IndividualConfig>) {
    this.showToast('show', message, title, override);
  }

  private getConfig(override?: Partial<IndividualConfig>) {
    const config = {
      autoDismiss: false,
      timeOut: 5000,
      closeButton: true,
      positionClass: 'toast-top-full-width',
      enableHtml: true,
    };
    if (override && typeof override === 'object') {
      Object.entries(override).forEach(([key, value]) => {
        if (value) {
          const k = key as 'autoDismiss' | 'timeOut' | 'closeButton' | 'positionClass' | 'enableHtml';
          config[k] = value as never;
        }
      });
    }
    return config;
  }

  private showToast(type: string, message?: string, title?: string, override?: Partial<IndividualConfig>) {
    const toTranslate = [];
    if (message) {
      toTranslate.push(message);
    }
    if (title) {
      toTranslate.push(title);
    }
    this.translate.get(toTranslate).subscribe((response: { [key: string]: string }) => {
      const method = type as 'error' | 'warning' | 'success' | 'info' | 'show';
      this.toastr[method](response[message || ''], response[title || ''], this.getConfig(override));
    });
  }
}
