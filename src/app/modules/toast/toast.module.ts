import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { ToastService } from './toast.service';

@NgModule({
  imports: [ToastrModule.forRoot()],
  declarations: [],
  providers: [ToastService],
  exports: [],
})
export class ToastModule {}
