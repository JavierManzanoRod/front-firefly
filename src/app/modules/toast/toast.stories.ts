import { StorybookTranslateModule } from '.storybook/StorybookTranslate.module';
import { Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { ToastrModule } from 'ngx-toastr';
import { ToastService } from './toast.service';

@Component({
  selector: 'ff-toast-story',
  template: ` <div class="d-flex " style="bottom:1rem;position:absolute">
    <button class="btn btn-primary mr-4" (click)="error()">Error</button>

    <button class="btn btn-primary mr-4" (click)="warning()">Warning</button>

    <button class="btn btn-primary mr-4" (click)="success()">Success</button>

    <button class="btn btn-primary mr-4" (click)="info()">Info</button>
  </div>`,
})
class ToastStoryComponent {
  constructor(private toastService: ToastService) {}

  error() {
    this.toastService.error('Mensaje de del toast', 'Esto es el título');
  }

  warning() {
    this.toastService.warning('Mensaje de del toast', 'Esto es el título');
  }

  success() {
    this.toastService.success('Mensaje de del toast', 'Esto es el título');
  }

  info() {
    this.toastService.info('Mensaje de del toast', 'Esto es el título');
  }
}

export default {
  component: ToastStoryComponent,
  decorators: [
    moduleMetadata({
      declarations: [ToastStoryComponent],
      providers: [ToastService],
      imports: [ToastrModule.forRoot(), StorybookTranslateModule, BrowserAnimationsModule],
    }),
  ],
  title: 'Toasts',
  parameters: {
    docs: {
      description: {
        component: 'Usa el servicio de toast para mostrar alertas flotantes para informar al usuario de algún cambio/error',
      },
    },
  },
} as Meta;

const Template: Story<ToastStoryComponent> = (args) => ({
  props: {
    ...args,
  },
});

export const Default = Template.bind({});
Default.args = {};
