import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { clone } from 'src/app/shared/utils/utils';
import { BreadCrumb } from './models/breadcrumb.model';

@Component({
  selector: 'ff-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent {
  @Input() pageTitle!: string;
  @Input() haveDetails = false;

  breadCrumbs = this.activatedRoute.data.pipe(
    map((e) => {
      const tempBreadCrumb = clone(e.breadcrumb);
      if (this.haveDetails) {
        const id = this.activatedRoute.snapshot.paramMap.get('id');
        const name = this.activatedRoute.snapshot.paramMap.get('name');
        tempBreadCrumb?.forEach((element: BreadCrumb) => {
          if (id) {
            element.url = element.url.replace(':id', id);
          }
          if (name) {
            element.label = element.label.replace(':id', name);
          }
        });
      }
      return tempBreadCrumb?.filter((v: any) => v.label || v.url);
    })
  );

  constructor(protected router: Router, public activatedRoute: ActivatedRoute) {}

  breadCrumbClick(url: string) {
    if (!url) {
      return false;
    }
    this.router.navigateByUrl(url);
  }
}
