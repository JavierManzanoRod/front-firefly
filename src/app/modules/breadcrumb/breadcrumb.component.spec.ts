import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { BreadcrumbComponent } from './breadcrumb.component';

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}
  public get(key: any): any {
    of(key);
  }
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

describe('BreadcrumbComponent', () => {
  let component: BreadcrumbComponent;
  let fixture: ComponentFixture<BreadcrumbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BreadcrumbComponent, TranslatePipeMock],
      imports: [RouterTestingModule],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceStub },
        {
          provide: ActivatedRoute,
          useFactory: () => ({
            data: of({
              breadcrumb: [
                {
                  label: 'test',
                  url: 'url',
                },
                {
                  label: '',
                  url: '',
                },
              ],
            }),
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('navigate to url', () => {
    const s = spyOn(TestBed.inject(Router), 'navigateByUrl');
    expect(component.breadCrumbClick(null as any)).toBeFalsy();
    component.breadCrumbClick('url');
    expect(s).toHaveBeenCalled();
  });

  it('load breadcrum', () => {
    component.breadCrumbs.subscribe((d) => {
      expect(d.length).toEqual(1);
    });
  });
});
