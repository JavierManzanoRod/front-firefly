import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { DndModule } from 'ngx-drag-drop';
import { SortablejsModule } from 'ngx-sortablejs';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { LoadingModule } from 'src/app/modules/loading/loading.module';
import { CommonModalModule } from '../common-modal/common-modal.module';
import { ExplodedTreeModule } from '../exploded-tree/exploded-tree.module';
import { SelectModule } from '../select/select.module';
import { UiTreeListModule } from '../ui-tree-list/ui-tree-list.module';
import { DatetimepickerModule } from './../datetimepicker/datetimepicker.module';
import { FormErrorModule } from './../form-error/form-error.module';
import { ControlPanel2CategoryRuylesComponent } from './components/category-rules/category-rules.component';
import { Conditions2ChildsComponent } from './components/childs/childs.component';
import { ConditionsTreeAttributeComponent } from './components/conditions-tree-attribute/conditions-tree-attribute.component';
import { Conditions2Component } from './components/conditions2/conditions2.component';
import { IncludedExcludedFormComponent } from './components/included-excluded-form/included-excluded-form.component';
import { IncludedExcludedTreeComponent } from './components/included-excluded-tree/included-excluded-tree.component';
import { Conditions2NodeComponent } from './components/node/node.component';
import { Conditions2ReferenceComponent } from './components/reference/reference.component';
import { Conditions2MultipleEntityComponent } from './components/simple-entity/multiple-values/mutiple-values.component';
import { Conditions2EntityComponent } from './components/simple-entity/simple-entity.component';
import { Conditions2SimpleMultipleValuesComponent } from './components/simple/mutiple-vales/mutiple-values.component';
import { Conditions2SimpleComponent } from './components/simple/simple.component';
import { Conditions2DoubleValidatorDirective } from './validators/conditions2-double.validator';
import { Conditions2FilledValidatorDirective } from './validators/conditions2-filled.validator';

@NgModule({
  declarations: [
    Conditions2Component,
    Conditions2NodeComponent,
    Conditions2SimpleComponent,
    Conditions2SimpleMultipleValuesComponent,
    Conditions2MultipleEntityComponent,
    Conditions2EntityComponent,
    ControlPanel2CategoryRuylesComponent,
    ConditionsTreeAttributeComponent,
    Conditions2ReferenceComponent,
    Conditions2FilledValidatorDirective,
    Conditions2DoubleValidatorDirective,
    Conditions2ChildsComponent,
    IncludedExcludedFormComponent,
    IncludedExcludedTreeComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LoadingModule,
    AccordionModule.forRoot(),
    TranslateModule,
    SortablejsModule.forRoot({ animation: 150 }),
    NgSelectModule,
    ChipsControlModule,
    SelectModule,
    DatetimepickerModule,
    FormErrorModule,
    DndModule,
    CommonModalModule,
    UiTreeListModule,
    TooltipModule,
    ExplodedTreeModule,
    SimplebarAngularModule,
  ],
  exports: [
    Conditions2Component,
    ConditionsTreeAttributeComponent,
    Conditions2FilledValidatorDirective,
    Conditions2DoubleValidatorDirective,
    IncludedExcludedFormComponent,
    IncludedExcludedTreeComponent,
  ],
})
export class Conditions2Module {}
