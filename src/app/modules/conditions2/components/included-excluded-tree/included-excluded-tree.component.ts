/* eslint-disable @typescript-eslint/member-ordering */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import {
  ConditionsBasic,
  ConditionsToShow,
  ControlPanelConditions,
} from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { ExplodedTreeComponent } from 'src/app/modules/exploded-tree/exploded-tree.component';
import {
  ExplodedDropEvent,
  ExplodedTree,
  ExplodedTreeElementType,
  ExplodedTreeNode,
  ExplodedTreeNodeType,
} from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import {
  RulesPreview,
  StatusRulesPreview,
} from 'src/app/modules/ui-modal-select/components/ui-modal-rules-preview/models/modal-rules-preview.model';
import { clone } from 'src/app/shared/utils/utils';
import { IActivatedAttribute, INCLUDED_ATTRIBUTES } from './models/included-excluded-attributes.model';
import { IncludedExcludedTreeService } from './services/included-excluded-tree.service';

type Props = {
  control: {
    pristine: boolean;
  };
  identifier: ControlPanelConditions;
};

@Component({
  selector: 'ff-included-excluded-tree',
  templateUrl: 'included-excluded-tree.component.html',
  styleUrls: ['included-excluded-tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IncludedExcludedTreeComponent implements OnInit {
  @ViewChild(ExplodedTreeComponent) explodedTree!: ExplodedTreeNodeComponent;
  @Output()
  changedIncluded: EventEmitter<ControlPanelConditions | null> = new EventEmitter<ControlPanelConditions | null>();
  @Output()
  changedExcluded: EventEmitter<ControlPanelConditions | null> = new EventEmitter<ControlPanelConditions | null>();
  @Output()
  visibleAttribute: EventEmitter<string> = new EventEmitter<string>();

  @Input() dataIncluded!: ConditionsBasic;
  @Input() dataExcluded!: ConditionsBasic;
  @Input() idAdminGroup!: string;
  @Input() readonly!: boolean;
  @Input() collapseTree!: boolean;
  @Input() conditionsToShow!: ConditionsToShow;
  _haveChanges!: boolean;
  get haveChanges(): boolean {
    return this._haveChanges;
  }
  @Input() set haveChanges(value: boolean) {
    this._haveChanges = value;
    this.explodedTree?.update();
  }

  tree: ExplodedTree[] = [];
  originalTree: ExplodedTree[] = [];
  clickData: ExplodedTreeNode | null = null;
  previewInclude: RulesPreview | any = {};
  previewExclude: RulesPreview | any = {};
  dropData: ExplodedTreeNode | null = null;
  target: ExplodedTreeNodeComponent | null = null;
  path: string | undefined;
  loading = false;
  activatedAttribute: IActivatedAttribute | null = null;
  formIncluded!: FormGroup;
  filterItems = INCLUDED_ATTRIBUTES.children?.map((v) => this.translateSrv.instant(v.label || ''));
  formExcluded!: FormGroup;

  constructor(private includedExcludedServ: IncludedExcludedTreeService, private translateSrv: TranslateService) {}

  ngOnInit() {
    this.originalTree = this.includedExcludedServ.getTreeAttributes();
    this.tree = clone(this.originalTree);
  }

  click(event: any) {
    this.activatedAttribute = {
      isIncluded: event.data.id === 'included',
      attribute: event.data.identifier,
    };
    this.visibleAttribute.emit(event.data.identifier);
  }

  clear() {
    this.tree = clone(this.originalTree);
  }

  loadForm(forms: { included: FormGroup; excluded: FormGroup }) {
    this.formExcluded = forms.excluded;
    this.formIncluded = forms.included;
  }

  search(event: string) {
    this.tree = clone(this.originalTree);
    const translatedTree = this.tree;
    translatedTree.forEach((node) => {
      node.children?.map((item) => (item.label = this.translateSrv.instant(item.label as string)) as string);
    });

    if (Array.isArray(event)) {
      translatedTree.forEach((node) => {
        node.children = node.children?.filter((item) => (item.label ? event.includes(item.label) : false));
      });
    } else {
      translatedTree.forEach((node) => {
        node.children = node.children?.filter((item) => item.label?.toLocaleLowerCase().includes(event.toLowerCase()));
      });
    }

    if (!translatedTree[0].children?.length && translatedTree[1].children?.length) this.tree = [this.tree[1]];
    else if (!translatedTree[1].children?.length && translatedTree[0].children?.length) this.tree = [this.tree[0]];
    else this.tree = translatedTree;

    this.explodedTree.update();
  }

  clickFilter(node: ExplodedTreeNodeComponent) {
    return node.data.element_type !== ExplodedTreeElementType.EMBEDDED;
  }

  badgeFilter(node: ExplodedTreeNodeComponent<Props>) {
    let key = node.data.identifier || '';
    key = key === 'centers' ? 'ranges' : key;
    const form = node.data.id === 'excluded' ? this.formExcluded : this.formIncluded;
    const preview = node.data.id === 'excluded' ? this.previewExclude : this.previewInclude;
    const data = key ? form.controls[key] : null;

    // if (
    //   key === ControlPanelConditions.centers &&
    //   !form.value.ranges?.length &&
    //   form.value.ranges &&
    //   (!data?.pristine || !form.controls.ranges.pristine)
    // ) {
    //   // preview[ControlPanelConditions.centers] = {
    //   //   values: form.value.centers,
    //   //   status: StatusRulesPreview.deleted,
    //   // };

    //   preview[ControlPanelConditions.ranges] = {
    //     values: form.value.ranges ? [form.value.ranges[0]] : [],
    //     status: StatusRulesPreview.deleted,
    //   };

    //   return 'icon-icon-multiply';
    // }

    if (data?.value && data?.value.length && (data.pristine || !this._haveChanges)) {
      preview[key] = {
        values: data.value,
        status: StatusRulesPreview.saved,
      };
      if (key === ControlPanelConditions.centers) {
        preview[ControlPanelConditions.ranges] = {
          values: form.value.ranges ? [form.value.ranges[0]] : [],
          status: StatusRulesPreview.saved,
        };
      }
      return 'icon-icon-check';
    }

    if (data && !data?.pristine && this._haveChanges) {
      preview[key] = {
        values: data.value,
        status: StatusRulesPreview.edited,
      };

      if (key === ControlPanelConditions.centers) {
        preview[ControlPanelConditions.ranges] = {
          values: form.value.ranges ? [form.value.ranges[0]] : [],
          status: StatusRulesPreview.edited,
        };
      }
      return 'icon-pencil';
    }

    if (node.isRoot) {
      return '';
    }

    if (node.data.element_type === ExplodedTreeElementType.STRING) {
      return 'info-light';
    }

    return 'left';
  }

  iconFilter(node: ExplodedTreeNodeComponent) {
    if (node.isRoot) {
      return node.data?.value?.included ? 'icon-icon-included' : 'icon-icon-excluded';
    }

    if (
      node.data.children?.length ||
      (node.data.node_type &&
        [ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE, ExplodedTreeNodeType.ATTRIBUTE_TREE_NODE].includes(node.data.node_type))
    ) {
      return 'icon-atributos';
    }

    return 'icon-agrupaciones';
  }

  calculatePath() {
    if (!this.target) {
      return (this.path = 'Select a node');
    }

    this.path = this.target.generateFieldPath();
  }

  drop({ data }: ExplodedDropEvent<ExplodedTreeNode>) {
    this.dropData = data as ExplodedTreeNode;
  }

  onChangeIncluded(data: any) {
    this.changedIncluded.emit(data);
    this.explodedTree?.update();
  }

  onChangeExcluded(data: any) {
    this.changedExcluded.emit(data);
    this.explodedTree?.update();
  }
}
