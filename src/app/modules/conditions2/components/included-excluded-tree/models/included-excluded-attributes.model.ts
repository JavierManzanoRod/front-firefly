import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { ExplodedTree } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { RouteTooltipLabels } from '../../../models/conditions2.model';

export interface IActivatedAttribute {
  isIncluded: boolean;
  attribute: string;
}

export const INCLUDED_ATTRIBUTES: ExplodedTree = {
  label: 'SPENTER.INCLUDED_FORM.INCLUDED',
  tooltip: 'Incluidos',
  value: { included: true },
  children: [
    {
      label: 'SPENTER.INCLUDED_FORM.SITES_TITLE',
      tooltip: RouteTooltipLabels.SITE,
      identifier: ControlPanelConditions.sites,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CONTENT_GROUP_TITLE',
      tooltip: 'Referencias',
      identifier: ControlPanelConditions.categoryRules,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.DEPARTAMENT_TITLE',
      tooltip: RouteTooltipLabels.DEPARTMENT,
      identifier: ControlPanelConditions.department,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.FAMILY_TITLE',
      tooltip: RouteTooltipLabels.FAMILY,
      identifier: ControlPanelConditions.family,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.BARRA',
      tooltip: RouteTooltipLabels.BARRA,
      identifier: ControlPanelConditions.barra,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.PRODUCT',
      tooltip: RouteTooltipLabels.PRODUCT,
      identifier: ControlPanelConditions.product,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.REFERENCE_TITLE',
      tooltip: RouteTooltipLabels.SALE_REFERENCE,
      identifier: ControlPanelConditions.saleReference,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.GTIN',
      identifier: ControlPanelConditions.gtin,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.SUPLIER',
      tooltip: RouteTooltipLabels.PROVIDER,
      identifier: ControlPanelConditions.provider,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.BRAND',
      tooltip: RouteTooltipLabels.BRAND,
      identifier: ControlPanelConditions.brand,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CATEGORIES_TITLE',
      tooltip: RouteTooltipLabels.CATEGORIES,
      identifier: ControlPanelConditions.categories,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.MERCHANDISE_TYPE',
      tooltip: RouteTooltipLabels.GOODTYPES,
      identifier: ControlPanelConditions.goodTypes,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION1',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications1,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION2',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications2,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION3',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications3,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION4',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications4,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION5',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications5,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION6',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications6,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.GESTION_TYPE_TITLE',
      tooltip: RouteTooltipLabels.MANAGEMENT_TYPE,
      identifier: ControlPanelConditions.managementType,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CUSTOM_ORDER',
      tooltip: RouteTooltipLabels.CUSTOMISED_REQUEST,
      identifier: ControlPanelConditions.customisedRequest,
      id: 'included',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CENTERS_PRICES_DISCOUNTS',
      tooltip: RouteTooltipLabels.PRICE_SPECIFICATION,
      identifier: ControlPanelConditions.centers,
      id: 'included',
    },
    // {
    //   label: 'SPENTER.INCLUDED_FORM.RANGES.LABEL',
    //   identifier: ControlPanelConditions.ranges,
    //   id: 'excluded',
    // },
  ],
};

export const EXCLUDED_ATTRIBUTES: ExplodedTree = {
  label: 'SPENTER.INCLUDED_FORM.EXCLUDED',
  tooltip: 'Excluidos',
  value: { included: false },
  children: [
    {
      label: 'SPENTER.INCLUDED_FORM.DEPARTAMENT_TITLE',
      tooltip: RouteTooltipLabels.DEPARTMENT,
      identifier: ControlPanelConditions.department,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.FAMILY_TITLE',
      tooltip: RouteTooltipLabels.FAMILY,
      identifier: ControlPanelConditions.family,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.BARRA',
      tooltip: RouteTooltipLabels.BARRA,
      identifier: ControlPanelConditions.barra,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.PRODUCT',
      tooltip: RouteTooltipLabels.PRODUCT,
      identifier: ControlPanelConditions.product,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.REFERENCE_TITLE',
      tooltip: RouteTooltipLabels.SALE_REFERENCE,
      identifier: ControlPanelConditions.saleReference,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.GTIN',
      identifier: ControlPanelConditions.gtin,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.SUPLIER',
      tooltip: RouteTooltipLabels.PROVIDER,
      identifier: ControlPanelConditions.provider,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.BRAND',
      tooltip: RouteTooltipLabels.BRAND,
      identifier: ControlPanelConditions.brand,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CATEGORIES_TITLE',
      tooltip: RouteTooltipLabels.CATEGORIES,
      identifier: ControlPanelConditions.categories,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.MERCHANDISE_TYPE',
      tooltip: RouteTooltipLabels.GOODTYPES,
      identifier: ControlPanelConditions.goodTypes,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION1',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications1,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION2',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications2,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION3',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications3,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION4',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications4,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION5',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications5,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CALIFICATION6',
      tooltip: RouteTooltipLabels.CLASSIFICATIONS,
      identifier: ControlPanelConditions.classifications6,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.GESTION_TYPE_TITLE',
      tooltip: RouteTooltipLabels.MANAGEMENT_TYPE,
      identifier: ControlPanelConditions.managementType,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CUSTOM_ORDER',
      tooltip: RouteTooltipLabels.CUSTOMISED_REQUEST,
      identifier: ControlPanelConditions.customisedRequest,
      id: 'excluded',
    },
    {
      label: 'SPENTER.INCLUDED_FORM.CENTERS_PRICES_DISCOUNTS',
      tooltip: RouteTooltipLabels.PRICE_SPECIFICATION,
      identifier: ControlPanelConditions.centers,
      id: 'excluded',
    },
    // {
    //   label: 'SPENTER.INCLUDED_FORM.RANGES.LABEL',
    //   identifier: ControlPanelConditions.ranges,
    //   id: 'excluded',
    // },
  ],
};
