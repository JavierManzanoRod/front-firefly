import { Injectable } from '@angular/core';
import { ExplodedTree } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { EXCLUDED_ATTRIBUTES, INCLUDED_ATTRIBUTES } from '../models/included-excluded-attributes.model';

@Injectable({
  providedIn: 'root',
})
export class IncludedExcludedTreeService {
  constructor() {}

  getTreeAttributes(): ExplodedTree[] {
    const attributeList: ExplodedTree[] = [];
    attributeList.push(INCLUDED_ATTRIBUTES);
    attributeList.push(EXCLUDED_ATTRIBUTES);
    return attributeList;
  }
}
