import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Conditions2 } from 'src/app/modules/conditions2/models/conditions2.model';
import { ConditionsToShow, ControlPanelConditions } from '../../../../control-panel/shared/models/control-panel-conditions.model';
import { Conditions2RulesChipsConfig } from '../../services/conditions2-rules-chips-config.service';
import { IActivatedAttribute } from '../included-excluded-tree/models/included-excluded-attributes.model';

const CENTER = {
  attributes: {
    '1a541a68-e08b-4a3c-b41a-c328efe713a6': 'Center-Code',
    'fcb46a6a-9cfe-45aa-bb0a-ae170958a06e': 'Center-Identifier',
  },
  entity_type_id: 'd76fa2ef-50f3-4a0c-ac1f-3754d030d993',
  id: '96e33748-1feb-41ff-bbc1-ee115daf8d05',
  label: 'VENTA A DISTANCIA',
  name: 'VENTA A DISTANCIA',
  type: undefined,
};
@Component({
  selector: 'ff-included-excluded-form',
  templateUrl: 'included-excluded-form.component.html',
  styleUrls: ['./included-excluded-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IncludedExcludedFormComponent implements OnInit, OnChanges {
  @Output()
  changedIncluded: EventEmitter<ControlPanelConditions | null> = new EventEmitter<ControlPanelConditions | null>();
  @Output()
  changedExcluded: EventEmitter<ControlPanelConditions | null> = new EventEmitter<ControlPanelConditions | null>();
  @Output()
  loadForm: EventEmitter<{ included: FormGroup; excluded: FormGroup }> = new EventEmitter<{ included: FormGroup; excluded: FormGroup }>();
  @Input() includedData!: Conditions2;
  @Input() excludedData!: Conditions2;
  @Input() conditionsToShow!: ConditionsToShow | any;
  @Input() isExcluded = false;
  @Input() idAdminGroup = '';
  @Input() activatedAttribute!: IActivatedAttribute;
  @Input() readonly = false;

  activeCondition!: ControlPanelConditions;
  excludedForm!: FormGroup;
  includedForm!: FormGroup;
  configSelectFieldsForTemplate: any;
  canSetFormOnChanges = true;
  conditions: string[] = [];

  constructor(private fb: FormBuilder, private rulesChipsConfigService: Conditions2RulesChipsConfig) {
    const formConfig: Partial<Record<ControlPanelConditions, any>> = {
      atue: [{ value: [], disabled: false }],
      barra: [{ value: [], disabled: false }],
      brand: [{ value: [], disabled: false }],
      categories: [{ value: [], disabled: false }],
      categoryRules: [{ value: [], disabled: false }],
      centers: [{ value: [], disabled: false }],
      classifications1: [{ value: [], disabled: false }],
      classifications2: [{ value: [], disabled: false }],
      classifications3: [{ value: [], disabled: false }],
      classifications4: [{ value: [], disabled: false }],
      classifications5: [{ value: [], disabled: false }],
      classifications6: [{ value: [], disabled: false }],
      customisedRequest: [{ value: [], disabled: false }],
      gtin: [{ value: [], disabled: false }],
      department: [{ value: [], disabled: false }],
      family: [{ value: [], disabled: false }],
      goodTypes: [{ value: [], disabled: false }],
      infiniteStock: [{ value: [], disabled: false }],
      internetSignal: [{ value: [], disabled: false }],
      luxuryReference: [{ value: [], disabled: false }],
      maker: [{ value: [], disabled: false }],
      managementType: [{ value: [], disabled: false }],
      margin: [{ value: [], disabled: false }],
      product: [{ value: [], disabled: false }],
      provider: [{ value: [], disabled: false }],
      ranges: [{ value: null, disabled: false }],
      referenceType: [{ value: [], disabled: false }],
      saleReference: [{ value: [], disabled: false }],
      serie: [{ value: [], disabled: false }],
      sites: [{ value: [], disabled: false }],
      sizeCode: [{ value: [], disabled: false }],
      volume: [{ value: [], disabled: false }],
    };

    this.includedForm = this.fb.group(formConfig);
    this.excludedForm = this.fb.group(formConfig);

    this.configSelectFieldsForTemplate = this.rulesChipsConfigService.getData(this.idAdminGroup);
  }

  get includedIsEmpty() {
    return Object.values(this.includedForm.controls).every((val) => {
      const valueField = val.value;
      return valueField == null || valueField.length === 0;
    });
  }

  get excludedIsEmpty() {
    return Object.values(this.excludedForm.controls).every((val) => {
      const valueField = val.value;
      return valueField == null || valueField.length === 0;
    });
  }

  ngOnInit() {
    this.conditions = Object.keys(this.conditionsToShow).filter((k) => this.conditionsToShow[k]);
    if (this.includedData) {
      this.includedForm.patchValue(this.includedData, { emitEvent: false });
    }
    if (this.excludedData) {
      this.excludedForm.patchValue(this.excludedData, { emitEvent: false });
    }

    this.includedForm.controls.ranges.valueChanges.subscribe((ranges) => {
      if (ranges) {
        this.includedForm.controls.centers.setValue([CENTER]);
      } else {
        this.includedForm.controls.centers.setValue(undefined);
      }
    });

    this.excludedForm.controls.ranges.valueChanges.subscribe((ranges) => {
      if (ranges) {
        this.excludedForm.controls.centers.setValue([CENTER]);
      } else {
        this.excludedForm.controls.centers.setValue(undefined);
      }
    });

    this.includedForm.valueChanges.subscribe((data) => {
      console.log('la data', data);
      this.changedIncluded.emit(this.includedIsEmpty ? null : JSON.parse(JSON.stringify(data)));
    });

    this.excludedForm.valueChanges.subscribe((data) => {
      this.changedExcluded.emit(this.excludedIsEmpty ? null : JSON.parse(JSON.stringify(data)));
    });

    if (this.readonly) {
      this.includedForm.disable();
      this.excludedForm.disable();
    }
    this.loadForm.emit({ included: this.includedForm, excluded: this.excludedForm });
  }

  ngOnChanges() {
    if (this.activatedAttribute) {
      this.activeCondition = this.activatedAttribute.attribute as ControlPanelConditions;
    }
  }
}
