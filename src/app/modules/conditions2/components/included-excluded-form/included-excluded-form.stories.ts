import { StorybookTranslateModule } from '.storybook/StorybookTranslate.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { Condition2SimpleDataTypes, Conditions2LeafTypes } from 'src/app/modules/conditions2/models/conditions2.model';
import { LoadingModule } from 'src/app/modules/loading/loading.module';
import { IncludedExcludedFormComponent } from './included-excluded-form.component';

export default {
  component: IncludedExcludedFormComponent,
  decorators: [
    moduleMetadata({
      declarations: [IncludedExcludedFormComponent],
      imports: [
        CommonModule,
        CommonModalModule,
        ReactiveFormsModule,
        FormsModule,
        LoadingModule,
        StorybookTranslateModule,
        ChipsControlModule,
      ],
    }),
  ],
  title: 'Condition/Include exclude form',
  parameters: {
    docs: {
      description: {
        component: 'Selector de condiciones',
      },
    },
  },
} as Meta;

const Template: Story<IncludedExcludedFormComponent> = (args) => ({
  props: {
    ...args,
  },
});

export const Default = Template.bind({});
Default.args = {
  conditionsToShow: {
    department: true,
    family: true,
    barra: true,
    sizeCode: false,
    product: true,
    saleReference: true,
    provider: true,
    brand: true,
    categories: true,
    goodTypes: true,
    classifications1: true,
    classifications2: true,
    classifications3: true,
    classifications4: true,
    classifications5: true,
    classifications6: true,
    managementType: true,
    customisedRequest: true,
    centers: true,
    ranges: true,
  },
  includedData: {
    attribute_id: '12',
    attribute_label: 'Label 1',
    entity_type_id: '1245',
    is_tree_attribute: true,
    entity_type_name: 'empty',
    tooltip_label: 'Lable one tooltip',
    multiple_cardinality: false,
    attribute: { identifier: '123' },
    node_type: Conditions2LeafTypes.eq,
    attribute_data_type: Condition2SimpleDataTypes.STRING,
  },
};
