import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, OnDestroy } from '@angular/core';
import { FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Conditions2Node, Conditions2NodeTypes } from '../../models/conditions2.model';
import { Conditions2UtilsService } from '../../services/conditions2-utils.service';

@Component({
  selector: 'ff-conditions2',
  templateUrl: './conditions2.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => Conditions2Component),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => Conditions2Component),
      multi: true,
    },
  ],
  styleUrls: ['./conditions2.component.scss'],
})
export class Conditions2Component implements OnDestroy {
  dataView!: Conditions2Node;
  isDisabled = false;

  constructor(private utils: Conditions2UtilsService, private ref: ChangeDetectorRef) {}

  init(data: any) {
    if (!data) {
      this.dataView = this.getEmptyCondition();
    } else if (this.utils.isConditions2Node(data)) {
      this.dataView = data;
    } else if (this.utils.isCondition2Simple(data)) {
      // simple or reference we wrap it (not acepted conditionsreference aloneif)
      this.dataView = { node_type: Conditions2NodeTypes.or, nodes: [data] };
    } else if (this.utils.isConditions2Reference(data)) {
      this.dataView = { node_type: Conditions2NodeTypes.or, nodes: [data] };
    } else {
      // throw new Error('data provided is not recogniez');
      this.dataView = { node_type: Conditions2NodeTypes.or, nodes: [data] };
    }
    this.ref.markForCheck();
  }

  changes(data: any) {
    this.writeValue(data, true);
  }

  writeValue(data: any, innerUpdate = false): void {
    let trimmedValue;
    if (!innerUpdate) {
      // this.state.reset();
      trimmedValue = data;
      this.init(trimmedValue);
    } else {
      trimmedValue = this.utils.trim(data);
    }
    this.onChange(trimmedValue);
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  // Allows Angular to register a function to call when the input has been touched.
  // Save the function as a property to call later here.
  registerOnTouched(fn: () => void): void {}

  onChange(data: any) {
    // don't delete!
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }

  getEmptyCondition(): Conditions2Node {
    return {
      node_type: Conditions2NodeTypes.or,
      nodes: [
        {
          node_type: Conditions2NodeTypes.and,
          nodes: [],
        },
        {
          node_type: Conditions2NodeTypes.and,
          nodes: [],
        },
      ],
    };
  }

  // getEmptyCondition(): Conditions2Node {
  //   return { node_type: Conditions2NodeTypes.or, nodes: [] };
  // }

  public validate(c: FormControl) {
    // this.state.setState(c);
    return null;
  }

  ngOnDestroy() {}
}
