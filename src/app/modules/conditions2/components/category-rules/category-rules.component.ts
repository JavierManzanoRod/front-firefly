import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { SortablejsOptions } from 'ngx-sortablejs';
import { Subject } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { Condition2Reference } from 'src/app/modules/conditions2/models/conditions2.model';
import { CategoryRuleService } from 'src/app/rule-engine/category-rule/services/category-rule.service';
import { Attribute } from '../../../../core/models/attribute.model';

@Component({
  selector: 'ff-control-panel2-category-rules',
  templateUrl: './category-rules.component.html',
  styleUrls: ['./category-rules.component.scss'],
})
export class ControlPanel2CategoryRuylesComponent implements OnInit, OnChanges {
  @Input() attributes?: Attribute[] = [];

  // eslint-disable-next-line import/no-deprecated
  sortablejsOptionsReference: SortablejsOptions = {
    handle: '.sortable-handle',
    group: {
      name: 'reference',
      put: false,
      pull: 'clone',
      revertClone: true,
    },
  };

  referenceSearchChanged = new Subject<string>();
  referenceSearch = '';
  referenceSearchDirty = false;
  itemsToDrag: Condition2Reference[] = [];

  loadingReferences = false;

  constructor(private categoryRuleService: CategoryRuleService) {}

  ngOnInit() {
    this.referenceSearchChanged
      .pipe(
        map(() => (this.loadingReferences = true)),
        debounceTime(300)
      )
      .subscribe(() => {
        /* this.categoryRuleService.searchAndFilter(this.referenceSearch, this.attributes || [])
        .subscribe(categoryRulesList => {
          this.loadingReferences = false;
          this.itemsToDrag = (categoryRulesList || []).map(
            (categoryRule) => {
             return {
                name: categoryRule.name,
                node_type: Conditions2ReferenceNodeType.reference,
                reference_id: categoryRule.id || ''
             };
            }
          );
        });*/
      });
  }

  ngOnChanges() {
    this.itemsToDrag = [];
    this.referenceSearch = '';
  }

  searchReferences() {
    this.itemsToDrag = [];
    this.referenceSearchDirty = true;
    if (this.referenceSearch.length >= 3) {
      this.referenceSearchChanged.next('');
    }
  }
}
