import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Attribute } from '@core/models/attribute.model';
import { SortablejsOptions } from 'ngx-sortablejs';
import { FF_ATTRIBUTE_PATHS } from 'src/app/configuration/tokens/configuracion';
import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { Condition2Simple, Conditions2 } from '../../models/conditions2.model';
import { Conditions2RulesChipsConfig } from '../../services/conditions2-rules-chips-config.service';
import { Conditions2OperatorsService } from '../../services/conditions2.operators.service';
import { Conditions2LeafTypes } from './../../models/conditions2.model';

enum TypesData {
  checkbox = 'checkbox',
  date = 'date',
  embedded = 'embedded',
  entity = 'entity',
  numberData = 'number',
  text = 'text',
}
@Component({
  selector: 'ff-conditions2-simple',
  templateUrl: './simple.component.html',
  styleUrls: ['./simple.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [],
})
export class Conditions2SimpleComponent implements OnInit {
  @Input() index!: number;
  @Input() _data!: Conditions2;
  @Input() readonly!: boolean;
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  @Output() changes: EventEmitter<any> = new EventEmitter();
  data!: Condition2Simple;

  operators: Condition2Simple['node_type'][] = [];
  attSelected!: Attribute;
  inputType = 'text';
  lastSelector: Condition2Simple['node_type'] | null = null;

  // eslint-disable-next-line import/no-deprecated
  sortablejsOptions: SortablejsOptions = {
    handle: '.sortable-handle',
    ghostClass: 'condition-simple',
    group: {
      name: 'condition-simple',
      put: ['operators'],
    },
  };
  types = Conditions2LeafTypes;
  typesData = TypesData;
  validators: any;
  form!: FormGroup;
  formErrors: { type: string; message: string }[] = [];

  constructor(
    public operatorService: Conditions2OperatorsService,
    public ref: ChangeDetectorRef,
    @Inject(FF_ATTRIBUTE_PATHS) private attributePath: { [key in ControlPanelConditions]: string },
    private ruleChipsConfig: Conditions2RulesChipsConfig
  ) {}

  ngOnInit() {
    this.data = this._data as Condition2Simple;
    this.attSelected = {
      data_type: this.data.attribute_data_type,
      label: this.data.attribute_label,
      name: '',
      id: this.data.attribute_id,
      entity_type_id: this.data.entity_type_id,
      multiple_cardinality: this.data.multiple_cardinality,
    };
    this.operators = this.operatorService.getAvailableOperators(this.data.attribute_data_type, this.data.multiple_cardinality);
    if (this.dataTypeLT) {
      this.data.node_type = Conditions2LeafTypes.lt;
      this.operators = [Conditions2LeafTypes.lt];
    }
    this.inputType = this.getInputType(this.data);
    this.lastSelector = this.data.node_type;

    this.form = new FormGroup({
      field: new FormControl({
        value: this.data?.value,
        disabled: this.readonly,
      }),
    });

    this.addValidators();
    this.field.valueChanges.subscribe((value) => {
      this.onChangeData(value);
    });
  }

  addValidators() {
    let tempRule: any = '';
    if (this.data?.attribute?.identifier === this.attributePath.barra) {
      tempRule = this.ruleChipsConfig.getData('').barra?.modalConfig;
    }

    if (this.data?.attribute?.identifier === this.attributePath.department) {
      tempRule = this.ruleChipsConfig.getData('').department?.modalConfig;
    }

    if (this.data?.attribute?.identifier === this.attributePath.family) {
      tempRule = this.ruleChipsConfig.getData('').family?.modalConfig;
    }

    if (this.data?.attribute?.identifier === this.attributePath.saleReference) {
      tempRule = this.ruleChipsConfig.getData('').saleReference?.modalConfig;
    }

    if (tempRule) {
      this.formErrors = tempRule.validatorErrors;
      this.field.setValidators([...tempRule.inputValidators, Validators.required]);
    } else {
      this.formErrors = [
        {
          type: 'required',
          message: 'COMMON.REQUIRED_FIELD',
        },
      ];
      this.field.setValidators(Validators.required);
    }
  }

  onChangeData(value?: string) {
    if (this.field.valid) {
      this.lastSelector = this.data.node_type;
      this.data.value = value;
      this.changes.emit();
    } else {
      this.data.value = null;
      this.changes.emit();
    }
  }

  onChangeDataSelect() {
    if (this.lastSelector && this.lastSelector !== this.data.node_type) {
      this.data.value = null;
      this.field.setValue(null);
      this.ref.detectChanges();
    }

    this.lastSelector = this.data.node_type;
    this.changes.emit();
  }

  deleteMySelf() {
    this.delete.emit(this.index);
  }

  updateMultipleValue(newVal: string[]) {
    this.data.value = newVal;
    this.changes.emit();
  }

  getInputType(data: Condition2Simple | null): string {
    if (!data) {
      return TypesData.text;
    } else {
      return {
        STRING: TypesData.text,
        DOUBLE: TypesData.numberData,
        DATE_TIME: TypesData.date,
        BOOLEAN: TypesData.checkbox,
        ENTITY: TypesData.entity,
        EMBEDDED: TypesData.embedded,
        VIRTUAL: '',
      }[data.attribute_data_type];
    }
  }

  updateCheckBoxValue($event: any) {
    this.data.value = Boolean($event.target.checked);
    this.changes.emit();
  }

  get dataTypeLT(): boolean {
    return this.data.node_type === Conditions2LeafTypes.lt && !!this.data.range;
  }

  get field(): FormControl {
    return this.form.controls.field as FormControl;
  }
}
