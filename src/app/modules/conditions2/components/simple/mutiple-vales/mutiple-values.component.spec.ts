import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FF_ATTRIBUTE_PATHS } from 'src/app/configuration/tokens/configuracion';
import { Conditions2RulesChipsConfig } from '../../../services/conditions2-rules-chips-config.service';
import { Conditions2UtilsService } from '../../../services/conditions2-utils.service';
import { Conditions2SimpleMultipleValuesComponent } from './mutiple-values.component';

describe('Conditions2SimpleMultipleValuesComponent', () => {
  let component: Conditions2SimpleMultipleValuesComponent;
  let fixture: ComponentFixture<Conditions2SimpleMultipleValuesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule],
      providers: [
        {
          provide: Conditions2RulesChipsConfig,
          useFactory: () => ({
            getData: () => {},
          }),
        },
        {
          provide: Conditions2UtilsService,
          useFactory: () => ({
            getFullIdAsString: () => {},
          }),
        },
        {
          provide: FF_ATTRIBUTE_PATHS,
          useFactory: () => ({
            barra: {},
            family: {},
          }),
        },
      ],
      // declarations: [ BannerComponent ]
    });
    fixture = TestBed.createComponent(Conditions2SimpleMultipleValuesComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should have as many inputs as array elements ', () => {
    component.values = ['a', 'b'];
    fixture.detectChanges();
    expect(true).toBeDefined();
  });
});
