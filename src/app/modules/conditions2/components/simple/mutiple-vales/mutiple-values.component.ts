import { Conditions2UtilsService } from './../../../services/conditions2-utils.service';
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FF_ATTRIBUTE_PATHS } from 'src/app/configuration/tokens/configuracion';
import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import {
  ChipsUIModalSelectDateView,
  ChipsUIModalSelectTextsView,
  ModalChipsTypes,
} from 'src/app/modules/chips-control/components/chips.control.model';
import { numberValidator } from 'src/app/shared/validations/number-validator';
import { Condition2Simple } from '../../../models/conditions2.model';
import { Conditions2RulesChipsConfig } from './../../../services/conditions2-rules-chips-config.service';

@Component({
  selector: 'ff-conditions2-simple-multiple-values',
  templateUrl: './mutiple-values.component.html',
  styleUrls: ['./mutiple-values.component.scss'],
})
export class Conditions2SimpleMultipleValuesComponent implements OnInit {
  @Input() values!: string[] | number[];
  @Input() data: Condition2Simple | null = null;
  @Input() type!: 'text' | 'number';
  @Input() readonly!: boolean;
  @Output() update: EventEmitter<string[] | number[]> = new EventEmitter<string[] | number[]>();

  chipsConfig: ChipsUIModalSelectTextsView | ChipsUIModalSelectDateView = {
    label: 'MULTIPLE.NAME',
    modalType: ModalChipsTypes.free,
    modalConfig: {
      title: 'MULTIPLE.NAME',
    },
  };

  constructor(
    @Inject(FF_ATTRIBUTE_PATHS) private attributePath: { [key in ControlPanelConditions]: string },
    private ruleChipsConfig: Conditions2RulesChipsConfig,
    private conditionsUtilService: Conditions2UtilsService
  ) {}

  ngOnInit() {
    let chips: any = '';
    const keysId = Object.keys(this.attributePath);
    keysId.forEach((key) => {
      const id = this.attributePath[key as ControlPanelConditions];
      if (this.data?.attribute && this.conditionsUtilService.getFullIdAsString(this.data?.attribute) === id) {
        chips = this.ruleChipsConfig.getData('')[key as ControlPanelConditions];
      }
    });

    if (chips) {
      this.chipsConfig.modalConfig = {
        ...this.chipsConfig.modalConfig,
        ...chips.modalConfig,
      };
    }

    if (this.type !== 'text') {
      this.chipsConfig.modalConfig = {
        title: 'MULTIPLE.NAME',
        inputValidators: [numberValidator()],
        validatorErrors: [{ type: 'number', message: 'COMMON_ERRORS.NUMBER' }],
      };
    }

    if (this.data?.attribute_data_type === 'DATE_TIME') {
      this.chipsConfig = {
        label: 'MULTIPLE.NAME',
        modalType: ModalChipsTypes.date,
        modalConfig: {
          title: 'MULTIPLE.NAME',
        },
      };
    }
  }

  parseData(values: any[]) {
    if (this.type !== 'text') {
      this.update.emit(values?.map((v) => +v));
    } else {
      this.update.emit(values);
    }
  }
}
