import { Condition2Simple } from '../../models/conditions2.model';
import { Conditions2OperatorsService } from '../../services/conditions2.operators.service';
import { Conditions2SimpleComponent } from './simple.component';

describe('Conditions2SimpleComponent', () => {
  it('it should create ', () => {
    const component = new Conditions2SimpleComponent(null as any, null as any, {} as any, null as any);
    expect(component).toBeDefined();
  });

  it('should call changes updating checkboxes', (done) => {
    const component = new Conditions2SimpleComponent(null as any, null as any, {} as any, null as any);
    component.data = {} as Condition2Simple;
    component.changes.subscribe(() => {
      expect(true).toBe(true);
      done();
    });
    const $event: any = { target: { checked: null as any } };
    component.updateCheckBoxValue($event);
  });

  it('should call changes updating checkboxes', (done) => {
    const component = new Conditions2SimpleComponent(null as any, null as any, {} as any, null as any);
    component.data = {} as Condition2Simple;
    component.changes.subscribe(() => {
      expect(true).toBe(true);
      done();
    });
    component.updateMultipleValue([]);
  });

  xit('all operators when data type is STRING MULTIPLE', () => {
    const service = new Conditions2OperatorsService();
    const component = new Conditions2SimpleComponent(service, null as any, {} as any, null as any);
    const expectOperators = ['INCLUDE_ALL', 'NOT_INCLUDE_ALL', 'INCLUDE_ANY', 'NOT_INCLUDE_ANY', 'IS_DEFINED', 'IS_NOT_DEFINED'];

    component._data = {
      attribute_id: 'id_attr',
      attribute_data_type: 'STRING',
      multiple_cardinality: true,
      node_type: 'eq',
      value: 'aaaa',
    } as Condition2Simple;
    component.ngOnInit();
    expect(component.operators).toEqual(jasmine.arrayWithExactContents(expectOperators));
  });
});
