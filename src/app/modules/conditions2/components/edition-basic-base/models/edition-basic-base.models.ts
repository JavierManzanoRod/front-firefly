import { FormItemConfig } from 'src/app/shared/services/form-utils.service';
import { Conditions2 } from '../../../models/conditions2.model';

export interface RulesBase {
  start_date?: string | Date;
  end_date?: string | Date;
  node?: Conditions2;
}

export interface EditionBasicFormHeader {
  [key: string]: FormItemConfig;
}
