import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTimeService } from '@core/base/date-time.service';
import { EntityType } from '@core/models/entity-type.model';
import { ConditionsBasic, ConditionsToShow } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { Conditions2BasicService } from '../../services/conditions2-basic.service';
import { RulesBase } from './models/edition-basic-base.models';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '',
})
export class EditionBasicBaseComponent<T extends RulesBase> implements OnInit, OnChanges {
  @Output() formSubmit: EventEmitter<T> = new EventEmitter<T>();
  @Input() entityType!: EntityType;
  @Input() loading!: boolean;
  @Input() item!: T;

  form!: FormGroup;
  formErrors: any = {};

  isCollapsedConfig: any = {};
  includedValue!: ConditionsBasic;
  excludedValue!: ConditionsBasic;
  conditionsToShow!: ConditionsToShow;
  haveChanges = false;
  skipFirstHourImput = 0;

  get isDirty(): boolean {
    return this.form.dirty || this.haveChanges;
  }

  constructor(
    public dateTimeService: DateTimeService,
    public controlPanelConditionsService: Conditions2BasicService,
    public toastService: ToastService,
    public router: Router,
    public activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.conditionsToShow = {
      department: true,
      family: true,
      barra: true,
      sizeCode: false,
      product: true,
      saleReference: true,
      provider: true,
      brand: true,
      categories: true,
      goodTypes: true,
      classifications1: true,
      classifications2: true,
      classifications3: true,
      classifications4: true,
      classifications5: true,
      classifications6: true,
      managementType: true,
      customisedRequest: true,
      centers: true,
      ranges: true,
    };
  }

  ngOnChanges() {
    this.item = this.item ? JSON.parse(JSON.stringify(this.item)) : this.item;
    console.log('item que llega, parseado', this.item);
    if (this.item) {
      this.item.start_date = this.item.start_date ? new Date(this.item.start_date) : new Date();
      if (this.item.end_date) {
        this.item.end_date = new Date(this.item.end_date);
      } else {
        this.item.end_date = new Date();
        this.item.end_date.setDate(this.item.end_date.getDate() + 1);
      }

      this.form.patchValue({
        ...this.item,
      });
    }

    if (this.item?.node) {
      const values = this.controlPanelConditionsService.nodeToIncludedExcludedValues(this.item.node);
      this.includedValue = values.included;
      this.excludedValue = values.excluded;
    }
  }

  changeIncludedData(data: ConditionsBasic) {
    this.haveChanges = true;
    this.includedValue = data;
  }

  changeExcludedData(data: ConditionsBasic) {
    this.haveChanges = true;
    this.excludedValue = data;
  }

  emptyRules(): boolean {
    if (
      (this.includedValue === null || this.includedValue === undefined) &&
      (this.excludedValue === null || this.excludedValue === undefined)
    ) {
      return true;
    } else {
      return false;
    }
  }

  rangesWithoutCenters(): boolean {
    if (
      (this.includedValue?.ranges && !this.includedValue.centers?.length) ||
      (this.excludedValue?.ranges && !this.excludedValue.centers?.length)
    ) {
      return true;
    }
    return false;
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.activeRoute.parent });
  }
}
