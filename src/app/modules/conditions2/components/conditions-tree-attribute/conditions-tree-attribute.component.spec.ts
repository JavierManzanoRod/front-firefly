import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ConditionsTreeAttributeComponent } from './conditions-tree-attribute.component';

xdescribe('ConditionsTreeAttributeComponent', () => {
  let component: ConditionsTreeAttributeComponent;
  let fixture: ComponentFixture<ConditionsTreeAttributeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConditionsTreeAttributeComponent],
      providers: [
        FormBuilder,
        {
          provide: HttpClient,
          useFactory: () => ({
            error: () => {},
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditionsTreeAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
