import { ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EntityType } from '@core/models/entity-type.model';
import { MayHaveIdName } from '@model/base-api.model';
import { SortablejsOptions } from 'ngx-sortablejs';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { FF_BUNDLE_OF } from 'src/app/configuration/tokens/admin-group-type.token';
import { AdminGroupType, AdminGroupTypePeeked } from 'src/app/rule-engine/admin-group-type/models/admin-group-type.model';
import { AdminGroupTypeService } from 'src/app/rule-engine/admin-group-type/services/admin-group-type.service';
import { CategoryRuleService } from 'src/app/rule-engine/category-rule/services/category-rule.service';
import { UITreeListItem } from '../../../ui-tree-list/models/ui-tree-list.model';
import { ConditionsTypeSearch } from '../../models/conditions2.model';

@Component({
  selector: 'ff-conditions-tree-attribute',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './conditions-tree-attribute.component.html',
  styleUrls: ['./conditions-tree-attribute.component.scss'],
})
export class ConditionsTreeAttributeComponent implements OnInit, OnChanges {
  @Output() selectedAdminGroupType: EventEmitter<AdminGroupType> = new EventEmitter<AdminGroupType>();
  @Output() selectedEntityType: EventEmitter<EntityType> = new EventEmitter<EntityType>();
  @Input() idAdminGroup = '';
  @Input() collapseTree!: boolean;

  _adminGroupTypeData: AdminGroupType | null = null;
  entityTypeId: any;
  adminGroupTypeId: string | undefined;
  get adminGroupTypeData(): AdminGroupType | null {
    return this._adminGroupTypeData;
  }
  @Input() set adminGroupTypeData(value: AdminGroupType | null) {
    this._adminGroupTypeData = value;
    this.changeAdminGroupType(value);
  }

  _entityTypes: EntityType[] | null = null;
  get entityTypes(): EntityType[] | null {
    return this._entityTypes;
  }
  @Input() set entityTypes(value: EntityType[] | null) {
    this._entityTypes = value;
    if (value instanceof Array) {
      value.forEach((att) => {
        att.attributes?.sort((a, b) => (a.name > b.name ? 1 : -1));
        att.attributes = att.attributes.filter((at) => at.id !== this.idBundleOf);
      });
      this.formSelector.controls.entityType.enable();
      this.formSelector.controls.entityType.setValue(value[0]);
      this.entityTypeId = value[0].id;
    }
  }

  tmpAdminGroupTypeSelected!: AdminGroupType | null;
  item$!: Observable<AdminGroupType[] | null>;

  formSelector: FormGroup;
  itemLoading = true;
  loading = false;
  searchModel = '';
  enumSearchType = ConditionsTypeSearch;
  searchType = this.enumSearchType.searchAttributes;

  attsToDrag!: UITreeListItem[];
  // eslint-disable-next-line import/no-deprecated
  sortablejsOptionsEntities: SortablejsOptions = {
    handle: '.sortable-handle',
    group: {
      name: 'entities',
      put: false,
      pull: 'clone',
      revertClone: true,
    },
  };
  subscription: Subscription | null = null;

  constructor(
    protected apiServiceadminGroupType: AdminGroupTypeService,
    protected apiCategory: CategoryRuleService,
    private fb: FormBuilder,
    @Inject(FF_BUNDLE_OF) private idBundleOf: string
  ) {
    this.formSelector = this.fb.group({
      adminGroupType: [null],
      entityType: [{ value: null, disabled: !this._entityTypes }],
    });
  }

  ngOnInit(): void {
    this.item$ = this.apiServiceadminGroupType
      .listWithOutListingAdmin({
        size: 9999,
        page: 0,
      })
      .pipe(
        map(({ content }) => content),
        catchError(() => {
          return of(null);
        }),
        tap(() => (this.itemLoading = false))
      );

    this.formSelector.controls.adminGroupType.valueChanges.subscribe((newVal: AdminGroupTypePeeked | AdminGroupType) => {
      this.adminGroupTypeId = newVal?.id;
      this.changeAdminGroupType(newVal);
    });

    this.formSelector.controls.entityType.valueChanges.subscribe((newVal) => {
      this.entityTypeId = newVal?.id;
    });
  }

  clearFilter() {
    this.searchModel = '';
  }

  isAdminGroup(x: AdminGroupTypePeeked | AdminGroupType): boolean {
    if (x) {
      return Array.isArray((x as AdminGroupType).entity_types);
    } else {
      return false;
    }
  }

  changeAdminGroupType(newVal: any) {
    if (!newVal || !Object.keys(newVal).length) {
      this.tmpAdminGroupTypeSelected = null;
      this.formSelector.controls.entityType.disable();
      this.formSelector.controls.entityType.setValue(null);
      this.attsToDrag = [];
    } else if (this.isAdminGroup(newVal)) {
      this.tmpAdminGroupTypeSelected = newVal as AdminGroupType;
      this.formSelector.controls.entityType.enable();
      this.entityTypes = newVal.entity_types;
      this.clearFilter();
    } else {
      this.clearFilter();
      this.apiServiceadminGroupType.detail(newVal.id || '', true).subscribe((adminGroupType) => {
        this.tmpAdminGroupTypeSelected = adminGroupType;
        this.selectedAdminGroupType.emit(this.tmpAdminGroupTypeSelected);
        adminGroupType?.entity_types?.forEach((entity) => {
          entity.attributes?.sort((a, b) => (a.name > b.name ? 1 : -1));
        });
        this.formSelector.patchValue({
          entityType: adminGroupType.entity_types ? adminGroupType.entity_types[0] : undefined,
        });
        this.formSelector.controls.entityType.enable();
      });
    }
    this.searchType = this.enumSearchType.searchAttributes;
  }

  ngOnChanges() {
    if (this.adminGroupTypeData && this.adminGroupTypeData.entity_types) {
      this.formSelector.patchValue({
        adminGroupType: this.adminGroupTypeData,
        entityType: this.adminGroupTypeData.entity_types[0],
      });
    }
  }

  trackByFn(item: MayHaveIdName) {
    return item.id;
  }

  get entityType() {
    return this.formSelector.controls.entityType;
  }

  get allAttributes() {
    return this.entityType.value && this.entityType.value.attributes;
  }
}
