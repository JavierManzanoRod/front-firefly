import { ToastService } from 'src/app/modules/toast/toast.service';
import { Condition2Simple, Conditions2, Conditions2LeafTypes, Conditions2NodeTypes } from '../../models/conditions2.model';
import { Conditions2UtilsService } from '../../services/conditions2-utils.service';
import { Conditions2NodeComponent } from './node.component';

describe('Conditions2NodeComponent', () => {
  it('it should create ', () => {
    const component = new Conditions2NodeComponent((null as unknown) as Conditions2UtilsService, (null as unknown) as ToastService);
    expect(component).toBeDefined();
  });

  it('addComplex add a child complex node', () => {
    const component = new Conditions2NodeComponent((null as unknown) as Conditions2UtilsService, (null as unknown) as ToastService);
    component.data = { node_type: Conditions2NodeTypes.and, nodes: [] };
    component.addComplex();
    expect(component.data.nodes[0].node_type === Conditions2NodeTypes.and);
  });

  it('delete remove child by index', () => {
    const component = new Conditions2NodeComponent(
      ({
        isCondition2Simple: () => false,
        isConditions2Childs: () => false,
      } as unknown) as Conditions2UtilsService,
      (null as unknown) as ToastService
    );
    component.data = {
      node_type: Conditions2NodeTypes.and,
      nodes: [
        {
          node_type: Conditions2LeafTypes.eq,
          attribute_id: 'id1',
        } as Condition2Simple,
        {
          node_type: Conditions2LeafTypes.eq,
          attribute_id: 'id2',
        } as Condition2Simple,
      ],
    };
    component.delete(0);
    const remaining = component.data.nodes[0] as Condition2Simple;
    expect(remaining.attribute_id).toBe('id2');
  });

  it('showSimpleComponent returns true if the component is simple and no entity', () => {
    const component = new Conditions2NodeComponent((null as unknown) as Conditions2UtilsService, (null as unknown) as ToastService);
    const conditions1: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1',
      attribute_data_type: 'STRING',
    } as Condition2Simple;
    expect(component.showSimpleComponent(conditions1)).toBeTruthy();

    const conditions2: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1',
      attribute_data_type: 'ENTITY',
    } as Condition2Simple;
    expect(component.showSimpleComponent(conditions2)).toBeFalsy();
  });

  it('showSimpleEntityComponent returns true if the component is simple and no entity', () => {
    const component = new Conditions2NodeComponent((null as unknown) as Conditions2UtilsService, (null as unknown) as ToastService);
    const conditions1: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1',
      attribute_data_type: 'STRING',
    } as Condition2Simple;
    expect(component.showSimpleEntityComponent(conditions1)).toBeFalsy();

    const conditions2: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1',
      attribute_data_type: 'ENTITY',
    } as Condition2Simple;
    expect(component.showSimpleEntityComponent(conditions2)).toBeTruthy();
  });

  it('deleteMySelf emits event to delete the component with the current index', (done) => {
    const component = new Conditions2NodeComponent((null as unknown) as Conditions2UtilsService, (null as unknown) as ToastService);
    component.index = 1;
    component.deleteMe.subscribe((data: any) => {
      expect(data).toBe(1);
      done();
    });
    component.deleteMySelf();
  });

  it('toggleNodeType toggle node_type', () => {
    const component = new Conditions2NodeComponent((null as unknown) as Conditions2UtilsService, (null as unknown) as ToastService);
    component.data = {
      node_type: Conditions2NodeTypes.and,
      nodes: [],
    };
    component.toggleNodeType();
    expect(component.data.node_type).toBe(Conditions2NodeTypes.or);

    component.toggleNodeType();
    expect(component.data.node_type).toBe(Conditions2NodeTypes.and);
  });
});
