/* eslint-disable @typescript-eslint/no-unsafe-call */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { SortableOptions } from 'sortablejs';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { clone } from 'src/app/shared/utils/utils';
import {
  Condition2Reference,
  Condition2Simple,
  Condition2SimpleChilds,
  Condition2SimpleDataTypes,
  Conditions2,
  Conditions2LeafTypes,
  Conditions2Node,
  Conditions2NodeTypes,
  Conditions2ReferenceNodeType,
} from '../../models/conditions2.model';
import { Conditions2UtilsService } from '../../services/conditions2-utils.service';

@Component({
  selector: 'ff-conditions-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Conditions2NodeComponent implements OnInit, OnChanges {
  @Input() _data!: Conditions2;
  @Input() index!: number;
  @Input() childParent?: Condition2SimpleChilds;

  @Output() changes: EventEmitter<Conditions2> = new EventEmitter<Conditions2>();
  @Output() deleteMe: EventEmitter<number> = new EventEmitter<number>();
  @Input() readonly!: boolean;
  data!: Conditions2Node;
  condition2SimpleDataTypes = Condition2SimpleDataTypes;

  sortablejsOptions: SortableOptions = {
    handle: '.sortable-handle',
    group: {
      name: 'condition-logical',
      put: ['entities', 'reference', 'condition-simple', 'condition-logical'],
    },
    onMove: (e: any) => {
      return e.dragged.className.indexOf('range') === -1;
    },
    onSort: () => {
      this.nodeTypeChanges();
      this.changes.emit(this.data);
    },
    // filter: '.range'
  };

  constructor(private conditions2UtilsService: Conditions2UtilsService, private toast: ToastService) {}

  ngOnInit() {
    this.data = this._data as Conditions2Node;
    // Cuando el nodo esta pintado dentro de un child los hijos solo se deben de poder ordenar dentro de si mismo sin poder sacarse por eso
    // modificamos las opciones del sortablejs
    if (this.childParent) {
      this.sortablejsOptions.group = {
        name: 'childs' + this.childParent.attribute_id,
        put: ['childs' + this.childParent.attribute_id],
      };
    }
    this.nodeTypeChanges();
  }

  addTemp(event: any) {
    // Eliminamos el primer elemento siempre por que es offer y offer no es un attributo
    // event.data.parents?.splice(0, 1);
    event.data.value.notParentVirtual = true;
    if (event.data.parents && event.data.parents.length) {
      const tempParent = this.conditions2UtilsService.reduceParentAttribute(event.data.parents || [], event.data.value?.iss);
      event.data.value = tempParent?.find((v) => v.attribute_id === event.data.value.attribute_id);
      if (event.data?.is_tree_attribute && event.data?.value) {
        event.data.value.recursive_filter = true;
      }
      event.data.parents = tempParent;
    }
    // Viene childParent cuando el nodo esta dentro de un child
    if (this.childParent) {
      const condition: Conditions2 | undefined = this.conditions2UtilsService.checkIfCanAddThisChildAndGetCondition(
        this.childParent,
        event.data
      );
      if (condition) {
        this.data.nodes.splice(event.index, 0, condition);
      } else {
        this.toast.error('CONDITIONS.ERROR_CHILDS');
      }
    } else if (this.conditions2UtilsService.greaterThanNode(this.data.nodes)) {
      this.toast.error('CONDITIONS.ERROR_RANGE');
    } else {
      const newItem = this.conditions2UtilsService.transformTreeListItemToCondition(event.data, event.data.parents);
      if (newItem) {
        this.data.nodes.push(newItem);
      }
    }
    this.changes.emit(this.data);
  }

  ngOnChanges(changes: SimpleChanges) {
    const newData = changes._data?.currentValue;
    if (newData) {
      this.data = newData;
    }
    // TODO: React to changes in the view
  }

  addComplex() {
    const newNode: Conditions2Node = {
      node_type: Conditions2NodeTypes.and,
      nodes: [],
    };
    this.data.nodes.push(newNode);
  }

  delete(index: number) {
    this.data.nodes.splice(index, 1);
    this.nodeTypeChanges();
    this.changes.emit(this.data);
  }

  showSimpleComponent(data: Conditions2): boolean {
    if (this.isConditions2Node(data)) {
      return false;
    } else if (this.isConditions2Reference(data)) {
      return false;
    } else {
      return (
        data.attribute_data_type !== Condition2SimpleDataTypes.ENTITY && data.attribute_data_type !== Condition2SimpleDataTypes.EMBEDDED
      );
    }
  }

  showSimpleEntityComponent(data: Conditions2): boolean {
    if (this.isConditions2Node(data)) {
      return false;
    } else if (this.isConditions2Reference(data)) {
      return false;
    } else {
      return data.attribute_data_type === Condition2SimpleDataTypes.ENTITY;
    }
  }

  showSimpleEmbeddedComponent(data: Conditions2): boolean {
    if (this.isConditions2Node(data)) {
      return false;
    } else if (this.isConditions2Reference(data)) {
      return false;
    } else {
      return data.attribute_data_type === Condition2SimpleDataTypes.EMBEDDED;
    }
  }

  isConditions2Node(condition: Conditions2): condition is Conditions2Node {
    return condition.node_type === Conditions2NodeTypes.and || condition.node_type === Conditions2NodeTypes.or;
  }

  isConditions2Reference(condition: Conditions2): condition is Condition2Reference {
    return condition.node_type === Conditions2ReferenceNodeType.reference;
  }

  deleteMySelf() {
    this.deleteMe.emit(this.index);
  }

  deleteOnParent(sonIndex: number) {
    this.data.nodes.splice(sonIndex, 1);
    this.nodeTypeChanges();
    this.changes.emit(this.data);
  }

  toggleNodeType() {
    const rangeNode = this.data.nodes.some((node) => node.node_type === Conditions2LeafTypes.gt);
    if (rangeNode) {
      this.toast.error('CONDITIONS.ERROR_CHANGE_NODE_RANGE');
      return;
    }
    if (this.data.node_type === Conditions2NodeTypes.and) {
      this.data.node_type = Conditions2NodeTypes.or;
    } else {
      this.data.node_type = Conditions2NodeTypes.and;
    }
    this.nodeTypeChanges();
    this.changes.emit(this.data);
  }

  changesOnChildrenNode($event: any) {
    this.nodeTypeChanges();
    this.changes.emit(this.data);
  }

  changesOnSimple() {
    this.nodeTypeChanges();
    this.changes.emit(this.data);
  }

  nodeTypeChanges() {
    const conditions: Condition2Simple[] = this.data.nodes.filter((condition) => {
      return (
        this.conditions2UtilsService.isCondition2Simple(condition) &&
        this.canHaveGt(condition) &&
        condition.node_type === Conditions2LeafTypes.gt
      );
    }) as Condition2Simple[];

    conditions.forEach((condition) => {
      this.nodeTypeChangesOnSimple(condition);
    });

    if (conditions.length === 0) {
      const temp = clone(this.data.nodes);
      const nodeLt = temp.findIndex((v) => v.node_type === Conditions2LeafTypes.lt);
      if (nodeLt !== -1 && temp[nodeLt].range) {
        temp.splice(nodeLt, 1);
        this.data.nodes = temp;
      }
    }

    if (this.data.node_type === Conditions2NodeTypes.or || this.data.node_type === Conditions2NodeTypes.and) {
      this.data.nodes.forEach((v: any) => {
        if (this.conditions2UtilsService.isConditions2Childs(v)) {
          const t = v.nodes.filter((condition) => {
            return (
              this.conditions2UtilsService.isCondition2Simple(condition) &&
              this.canHaveGt(condition) &&
              condition.node_type === Conditions2LeafTypes.gt
            );
          }) as any[];
          t.forEach((v2) => {
            this.nodeTypeChangesOnSimple(v2, v.nodes);
            const newNode: Conditions2Node = {
              node_type: Conditions2NodeTypes.and,
              nodes: [...v.nodes],
            };
            v.nodes = [newNode];
          });
        } else {
          const arrayGt = v?.nodes?.filter((condition: any) => {
            return (
              this.conditions2UtilsService.isCondition2Simple(condition) &&
              this.canHaveGt(condition) &&
              condition.node_type === Conditions2LeafTypes.gt
            );
          });

          const indexLt = v?.nodes?.findIndex((condition: any) => {
            return (
              this.conditions2UtilsService.isCondition2Simple(condition) &&
              this.canHaveGt(condition) &&
              condition.node_type === Conditions2LeafTypes.lt &&
              !arrayGt.some((gt: any) => gt.attribute_id === condition.attribute_id) &&
              condition.range
            );
          });

          if (indexLt >= 0) {
            v.nodes.splice(indexLt, 1);
          }
        }
      });
    }
  }

  nodeTypeChangesOnSimple(condition: Condition2Simple, data = this.data.nodes) {
    // Si la condicion es de tipo numero y es de "mayor que" comprobamos si tiene algun hermano que sea del mismo atributo y "menor que"
    // si no tiene ningun lo creamos
    if (this.canHaveGt(condition) && condition.node_type === Conditions2LeafTypes.gt) {
      const cond = data.find((item) => {
        return (
          this.conditions2UtilsService.isCondition2Simple(item) &&
          item.attribute.identifier === condition.attribute.identifier &&
          item.node_type === Conditions2LeafTypes.lt
        );
      });
      if (!cond) {
        if (data.length === 1) {
          const newCond = JSON.parse(JSON.stringify(condition));
          newCond.node_type = Conditions2LeafTypes.lt;
          newCond.range = true;
          newCond.value = undefined;
          const index = data.indexOf(condition);
          data.splice(index + 1, 0, newCond);
        } else {
          const newCond = JSON.parse(JSON.stringify(condition));
          newCond.node_type = Conditions2LeafTypes.lt;
          newCond.range = true;
          newCond.value = undefined;
          const index = data.indexOf(condition);
          const newRange: Conditions2Node = {
            node_type: Conditions2NodeTypes.and,
            nodes: [condition, newCond],
          };
          data.splice(index, 1, newRange);
        }
      } else {
        cond.range = true;
      }
    }
  }

  private canHaveGt(condition: Condition2Simple): boolean {
    return (
      condition.attribute_data_type === Condition2SimpleDataTypes.DOUBLE ||
      condition.attribute_data_type === Condition2SimpleDataTypes.DATE_TIME
    );
  }
}
