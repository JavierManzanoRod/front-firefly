import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Condition2Reference, Conditions2 } from '../../models/conditions2.model';

@Component({
  selector: 'ff-conditions2-reference',
  templateUrl: './reference.component.html',
  styleUrls: ['./reference.component.scss'],
})
export class Conditions2ReferenceComponent implements OnInit {
  @Input() index!: number;
  @Input() _data!: Conditions2;
  @Input() readonly!: boolean;
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  data!: Condition2Reference;

  constructor() {}

  ngOnInit() {
    this.data = this._data as Condition2Reference;
  }

  deleteMySelf() {
    this.delete.emit(this.index);
  }
}
