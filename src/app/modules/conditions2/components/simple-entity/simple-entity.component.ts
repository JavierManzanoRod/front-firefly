import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Attribute } from '@core/models/attribute.model';
import { MayHaveIdName } from '@model/base-api.model';
import { SortablejsOptions } from 'ngx-sortablejs';
import { ChipsUIModalSelect, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { EntityService } from 'src/app/rule-engine/entity/services/entity.service';
import {
  Condition2Attribute,
  Condition2Simple,
  Condition2SimpleEntity,
  Conditions2,
  Conditions2LeafTypes,
} from '../../models/conditions2.model';
import { Conditions2OperatorsService } from '../../services/conditions2.operators.service';

@Component({
  selector: 'ff-conditions2-entity',
  templateUrl: './simple-entity.component.html',
  styleUrls: ['./simple-entity.component.scss'],
})
export class Conditions2EntityComponent implements OnInit {
  @Input() index!: number;
  @Input() _data!: Conditions2;
  @Input() readonly!: boolean;
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  @Output() changes: EventEmitter<any> = new EventEmitter();

  data!: Condition2SimpleEntity;
  operators: Condition2Simple['node_type'][] = [];
  attSelected!: Attribute;
  lastSonAttribute!: Condition2Attribute;

  // eslint-disable-next-line import/no-deprecated
  sortablejsOptions: SortablejsOptions = {
    handle: '.sortable-handle',
    ghostClass: 'condition-simple',
    group: {
      name: 'condition-simple',
      put: ['operators'],
    },
    onAdd: () => {},
    onSort: () => {},
  };

  idCheckbox = new Date().getUTCMilliseconds();
  types = Conditions2LeafTypes;

  chipsConfig: ChipsUIModalSelect = {
    label: 'SIMPLE_ENTITY.NAME',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'SIMPLE_ENTITY.NAME',
      multiple: false,
      searchFnOnInit: true,
      itemLabelKey: ['name', 'label'],
      searchParam: 'label_attribute',
      searchFn: (x) => {
        return this.apiEntityService.listWithFilter(this.data.entity_reference, x);
      },
    },
  };

  constructor(public operatorService: Conditions2OperatorsService, public apiEntityService: EntityService, public ref: ChangeDetectorRef) {}

  ngOnInit() {
    this.data = this._data as Condition2SimpleEntity;
    this.lastSonAttribute = this.getAttribute(this.data.attribute);
    if (this.data.entity_info && this.data.value) {
      if (Array.isArray(this.data.value)) {
        this.data.value = this.data.value as string[];
        this.data.value = this.data.value.map((v: any) => {
          if (typeof v === 'string') {
            return {
              id: v,
              name: this.data.entity_info[v]?.entity_name || '',
            };
          }
          return v;
        });
      } else {
        if (typeof this.data.value === 'string') {
          this.data.value = {
            id: this.data.value || '',
            name: this.data.entity_info[this.data.value]?.entity_name || '',
          } as any;
        }
      }
    }

    this.attSelected = {
      data_type: this.data.attribute_data_type,
      label: this.data.attribute_label,
      name: '',
      id: this.data.attribute_id,
      entity_type_id: this.data.entity_type_id,
      multiple_cardinality: this.data.multiple_cardinality,
      entity_reference: this.data.entity_reference,
      entity_reference_name: this.data.entity_reference_name,
      value: this.data.value,
    };

    this.operators = this.operatorService.getAvailableOperators(this.data.attribute_data_type, this.data.multiple_cardinality);
  }

  onChangeData() {
    this.data.value = null;
    this.changes.emit();
  }

  onChangeDataRecursive() {
    this.changes.emit();
  }

  trackByFn(item: MayHaveIdName) {
    return item.id;
  }

  selectedEntityOnChange(event?: any) {
    this.data.value = event;
    this.changes.emit();
  }

  deleteMySelf() {
    this.delete.emit(this.index);
  }

  updateMultipleValue(newVal: string[]) {
    this.data.value = newVal.length > 0 ? newVal : null;
    this.changes.emit();
  }

  private getAttribute(attribute: Condition2Attribute): Condition2Attribute {
    let attr = attribute;
    if (attribute.reference) {
      attr = this.getAttribute(attribute.reference);
    }

    return attr;
  }
}
