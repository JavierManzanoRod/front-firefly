import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_ATTRIBUTE_PATHS, FF_ENTITY_TYPE_GOOD_TYPE } from 'src/app/configuration/tokens/configuracion';
import { ChipsUIModalSelect, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { Entity } from 'src/app/rule-engine/entity/models/entity.model';
import { EntityService } from 'src/app/rule-engine/entity/services/entity.service';

@Component({
  selector: 'ff-conditions2-multiple-entity',
  templateUrl: './mutiple-values.component.html',
  styleUrls: ['./mutiple-values.component.scss'],
})
export class Conditions2MultipleEntityComponent implements OnInit {
  @Input() readonly!: boolean;
  @Output() update: EventEmitter<Entity[]> = new EventEmitter<Entity[]>();
  @Input() data!: any;
  _value: any;
  get value(): any {
    return this._value;
  }
  @Input() set value(value: any) {
    this._value = value;
    if (!value) {
      this.form.controls.entities.setValue(null);
    }
  }

  form = this.fb.group({
    entities: new FormControl({ value: null, disabled: this.readonly }),
  });

  chipsConfig: ChipsUIModalSelect = {
    label: 'MULTIPLE_ENTITY.NAME',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'MULTIPLE_ENTITY.NAME',
      multiple: true,
      searchFnOnInit: true,
      itemLabelKey: ['name', 'label'],
      searchParam: 'label_attribute',
      searchFn: (x) => {
        return this.apiEntityService.listWithFilter(this.data.entity_reference, x);
      },
    },
  };

  constructor(
    private fb: FormBuilder,
    private apiEntityService: EntityService,
    @Inject(FF_ATTRIBUTE_PATHS) private attributeIds: ConfigurationDefinition['attributePath'],
    @Inject(FF_ENTITY_TYPE_GOOD_TYPE) private entityTypeIdGoodType: ConfigurationDefinition['entityTypeIdGoodType']
  ) {}

  ngOnInit() {
    if (this.data.attribute_id === this.attributeIds?.goodTypes || this.data.entity_reference === 'goodTypes') {
      this.chipsConfig.modalConfig = {
        title: 'MULTIPLE_ENTITY.NAME',
        multiple: true,
        searchFnOnInit: true,
        itemLabelKey: ['name', 'label'],
        searchParam: 'label_attribute',
        // items: this.goodTypeService.list(),
        searchFn: (x) => {
          return this.apiEntityService.listWithFilter(this.entityTypeIdGoodType ? this.entityTypeIdGoodType : '', x);
        },
      };
    }
    this.form.controls.entities.setValue(this.value);
    this.form.controls.entities.valueChanges.subscribe((value: Entity[]) => {
      this.update.emit(value || []);
    });
  }
}
