import { ToastService } from 'src/app/modules/toast/toast.service';
import { Conditions2UtilsService } from '../../services/conditions2-utils.service';
import { Conditions2OperatorsService } from '../../services/conditions2.operators.service';
import { Conditions2ChildsComponent } from './childs.component';
describe('Conditions2ChildsComponent', () => {
  it('it should create ', () => {
    const component = new Conditions2ChildsComponent(
      (null as unknown) as Conditions2OperatorsService,
      (null as unknown) as Conditions2UtilsService,
      (null as unknown) as ToastService
    );
    expect(component).toBeDefined();
  });
});
