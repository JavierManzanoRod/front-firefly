import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SortablejsOptions } from 'ngx-sortablejs';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { clone } from 'src/app/shared/utils/utils';
import {
  Condition2Simple,
  Condition2SimpleChilds,
  Conditions2,
  Conditions2LeafTypes,
  Conditions2NodeTypes,
} from '../../models/conditions2.model';
import { Conditions2UtilsService } from '../../services/conditions2-utils.service';
import { Conditions2OperatorsService } from '../../services/conditions2.operators.service';

@Component({
  selector: 'ff-conditions2-childs',
  templateUrl: './childs.component.html',
  styleUrls: ['./childs.component.scss'],
})
export class Conditions2ChildsComponent implements OnInit {
  @Input() index!: number;
  @Input() data!: Condition2SimpleChilds;
  @Input() readonly!: boolean;
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  @Output() changes: EventEmitter<any> = new EventEmitter();

  and = Conditions2NodeTypes.and;
  or = Conditions2NodeTypes.or;

  operators: Condition2Simple['node_type'][] = [];

  sortablejsOptions: SortablejsOptions = {
    handle: '.sortable-handle',
    group: {
      name: 'childs',
      put: [],
    },
    onSort: () => {
      this.changes.emit(this.data);
    },
  };

  constructor(
    private conditions2OperatorsService: Conditions2OperatorsService,
    private conditions2UtilsService: Conditions2UtilsService,
    private toast: ToastService
  ) {}

  ngOnInit() {
    this.operators = this.conditions2OperatorsService.getAvailableOperators(
      this.data.attribute_data_type,
      this.data.multiple_cardinality,
      this.data.nodes
    );

    if (
      (this.data.attribute_data_type === 'ENTITY' || this.data.attribute_data_type === 'EMBEDDED') &&
      this.data.multiple_cardinality &&
      this.data.nodes
    ) {
      if (this.data.node_type === Conditions2LeafTypes.INCLUDE_ALL) {
        this.data.node_type = Conditions2LeafTypes.MATCH_ALL;
      }

      if (this.data.node_type === Conditions2LeafTypes.INCLUDE_ANY) {
        this.data.node_type = Conditions2LeafTypes.MATCH_ANY;
      }
    }

    // si attribute primero no es multiple y el data si es multiple quiere decir q es un nodo virtual
    /*if (!this.data.notParentVirtual && this.data.attribute.reference) {
      this.data.notParentVirtual = true;
      const cloneData = clone(this.data);
      this.data.attribute_id = this.data.attribute.identifier;
      this.data.node_type = Conditions2LeafTypes.MATCH_ALL;
      this.data.attribute_label = this.data.attribute.label || '';
      this.data.tooltip_label = `/${this.data.attribute.label}`;
      this.data.multiple_cardinality = !!this.data.attribute.multiple_cardinality;
      delete this.data.attribute.reference;
      this.data.nodes = [cloneData];
    }*/
  }

  deleteMySelf() {
    this.delete.emit(this.index);
  }

  deleteChild(index: number) {
    this.data.nodes.splice(index, 1);
    // Si no me quedan hijos mando borrarme, por que no tiene sentido que se quede una condicion de hijos sin hijos
    if (!this.data.nodes.length) {
      this.deleteMySelf();
    }
  }

  onChange() {
    this.changes.emit();
  }

  hiddenOperators() {
    // si tiene node y no es multiple quiere decir que un nodo virtual que solo se usara en la vista
    return this.data.nodes && !!this.data.multiple_cardinality;
  }

  onDrop(event: any) {
    if (event.data.parents && event.data.parents.length) {
      const tempParent = this.conditions2UtilsService.reduceParentAttribute(event.data.parents || [], event.data.value?.iss);
      event.data.value = tempParent?.find((v) => v.attribute_id === event.data.value.attribute_id);
      if (event.data?.is_tree_attribute && event.data?.value) {
        event.data.value.recursive_filter = true;
      }
      event.data.parents = tempParent;
    }
    const condition: Conditions2 | undefined = this.conditions2UtilsService.checkIfCanAddThisChildAndGetCondition(this.data, event.data);
    if (condition) {
      this.data.nodes.splice(event.index, 0, condition);
      if (!this.oneOfNodesIsTypeNode()) {
        this.data.nodes = [
          {
            node_type: Conditions2NodeTypes.or,
            nodes: this.data.nodes,
          },
        ];
      }
    } else {
      this.toast.error('CONDITIONS.ERROR_CHILDS');
    }
  }

  oneOfNodesIsTypeNode() {
    return !!this.data.nodes.find((item) => {
      return item.node_type === Conditions2NodeTypes.and || item.node_type === Conditions2NodeTypes.or;
    });
  }
}
