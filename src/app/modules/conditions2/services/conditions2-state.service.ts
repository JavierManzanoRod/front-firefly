import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

interface StatusConditionsView {
  dirty: boolean;
  pristine: boolean;
}

const initialStatus: StatusConditionsView = {
  dirty: false,
  pristine: true,
};

@Injectable()
export class Conditions2StateService {
  status: StatusConditionsView;

  constructor() {
    this.status = initialStatus;
  }

  public setState(c: FormControl) {
    this.status.dirty = c.dirty;
    this.status.pristine = c.pristine;
    // console.log('setting status to', this.status);
  }

  public reset() {
    this.status = initialStatus;
  }
}
