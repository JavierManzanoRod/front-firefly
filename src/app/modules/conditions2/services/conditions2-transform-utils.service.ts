import { Inject, Injectable } from '@angular/core';
import { MayHaveIdName } from '@model/base-api.model';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_ATTRIBUTE_PATHS, FF_GROUPED_ATTRIBUTES } from 'src/app/configuration/tokens/configuracion';
import {
  IncludedExcludedConditionsBasics,
  ControlPanelConditions,
  IPricesAndDiscounts,
  ConditionsBasic,
} from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import {
  Condition2Attribute,
  Condition2Multiple,
  Condition2SimpleEntity,
  Conditions2,
  Conditions2LeafTypes,
  Conditions2NodeTypes,
  Conditions2ReferenceNodeType,
} from '../models/conditions2.model';
import { Conditions2UtilsService } from './conditions2-utils.service';

@Injectable({
  providedIn: 'root',
})
export class ConditionsTransformUtilsService {
  constructor(
    private conditionsUtils: Conditions2UtilsService,
    @Inject(FF_GROUPED_ATTRIBUTES) private groupedAttributes: ConfigurationDefinition['groupedAttributes'],
    @Inject(FF_ATTRIBUTE_PATHS) private attributeIds: ConfigurationDefinition['attributePath']
  ) {}

  public mergeResults(
    result: IncludedExcludedConditionsBasics,
    newResult: IncludedExcludedConditionsBasics
  ): IncludedExcludedConditionsBasics {
    const includedKeys = Object.keys(result.included).concat(Object.keys(newResult.included));
    const excludedKeys = Object.keys(result.excluded).concat(Object.keys(newResult.excluded));

    includedKeys?.forEach((key) => {
      const attribute = key as ControlPanelConditions;
      let resultProperty: string[] = [];
      let newResultProperty: string[] = [];

      if (result.included[attribute]) {
        resultProperty = result.included[attribute] as string[];
      }

      if (newResult.included[attribute]) {
        newResultProperty = newResult.included[attribute] as string[];
      }

      result.included[attribute] = [...resultProperty, ...newResultProperty];
    });

    excludedKeys?.forEach((key) => {
      const attribute = key as ControlPanelConditions;
      let resultProperty: string[] = [];
      let newResultProperty: string[] = [];

      if (result.excluded[attribute]) {
        resultProperty = result.excluded[attribute] as string[];
      }

      if (newResult.excluded[attribute]) {
        newResultProperty = newResult.excluded[attribute] as string[];
      }

      result.excluded[attribute] = [...resultProperty, ...newResultProperty];
    });

    return result;
  }

  public removeRangesEmpty(ranges: IPricesAndDiscounts[]): boolean {
    return ranges.length === 1 && Object.keys(ranges[0].discount || {}).length === 0 && Object.keys(ranges[0].price || {}).length === 0;
  }

  public getConditionType(attribute: Condition2Attribute): ControlPanelConditions {
    const attributeId = this.conditionsUtils.getFullIdAsString(attribute);
    const attributeKeys = Object.keys(this.attributeIds || {});

    const x = attributeKeys.find((environmentKey) => {
      if (!this.attributeIds) {
        return false;
      } else {
        return attributeId === this.attributeIds[environmentKey as ControlPanelConditions];
      }
    });

    return ControlPanelConditions[x as ControlPanelConditions];
  }

  public nodeOrToCondition(nodes: Conditions2[]): IncludedExcludedConditionsBasics {
    const included: ConditionsBasic = {};
    const excluded: ConditionsBasic = {};

    const result: IncludedExcludedConditionsBasics = { included, excluded };

    const contentGroups: MayHaveIdName[] = [];

    nodes.forEach((itemNode) => {
      if (itemNode.node_type === Conditions2LeafTypes.eq) {
        const attribute = this.getConditionType(itemNode.attribute);
        if (result.included[attribute] && Array.isArray(result.included[attribute])) {
          result.included[attribute] = [...(result.included[attribute] as any), itemNode.value];
        } else {
          result.included[attribute] = [itemNode.value];
        }
      } else if (itemNode.node_type === Conditions2ReferenceNodeType.reference) {
        contentGroups.push({ id: itemNode.reference_id, name: itemNode.reference_name || itemNode.name });
        result.included[ControlPanelConditions.categoryRules] = [...contentGroups];
      } else if (itemNode.node_type === Conditions2LeafTypes.notin) {
        const attribute = this.getConditionType(itemNode.attribute);
        excluded[attribute] = itemNode.value as string[];
      } else if (itemNode.node_type === Conditions2NodeTypes.or) {
        return this.mergeResults(result, this.nodeOrToCondition(itemNode.nodes));
      }
    });

    return result;
  }

  public conditionToBasic(node: Conditions2 | Condition2Multiple): IncludedExcludedConditionsBasics {
    const included: ConditionsBasic = {};
    const excluded: ConditionsBasic = {};
    const result: IncludedExcludedConditionsBasics = { included, excluded };

    if (node.node_type === Conditions2LeafTypes.eq) {
      const attribute = this.getConditionType(node.attribute);
      included[attribute] = [node.value as string];
    } else if (node.node_type === Conditions2ReferenceNodeType.reference) {
      const contentGroup: MayHaveIdName = { id: node.reference_id, name: node.reference_name || node.name };
      included[ControlPanelConditions.categoryRules] = [contentGroup];
    } else if (node.node_type === Conditions2LeafTypes.notin) {
      const attribute = this.getConditionType(node.attribute);
      excluded[attribute] = node.value as string[];
    }

    if (
      node.node_type === Conditions2LeafTypes.INCLUDE_ANY ||
      node.node_type === Conditions2LeafTypes.NOT_INCLUDE_ANY ||
      node.node_type === Conditions2LeafTypes.in ||
      node.node_type === Conditions2LeafTypes.notin
    ) {
      const nodeMultiple = node as Condition2Multiple;
      const attribute = this.getConditionType(nodeMultiple.attribute);
      if (node.value) {
        const entitiesValues: MayHaveIdName[] = [];
        node.value.forEach((element: any) => {
          let temp = element;
          if (typeof element === 'string' && node.attribute_data_type === 'ENTITY') {
            const nodeTemp = node as Condition2SimpleEntity;
            temp = { id: element, name: nodeTemp.entity_info[element].entity_name };
          }
          entitiesValues.push(temp);
        });
        if (node.node_type === Conditions2LeafTypes.INCLUDE_ANY || node.node_type === Conditions2LeafTypes.in) {
          included[attribute] = entitiesValues;
        } else {
          excluded[attribute] = entitiesValues;
        }
      }
    }

    return result;
  }

  public countConditions(conditions: ConditionsBasic): number {
    const conditionKeys = Object.keys(conditions) as ControlPanelConditions[];
    let n = 0;

    if (this.onlyClassifications(conditions)) {
      return (n = 1);
    }

    conditionKeys.forEach((item: ControlPanelConditions) => {
      if (!(item === ControlPanelConditions.ranges && conditions[ControlPanelConditions.centers]?.length)) {
        const count = conditions[item] ? conditions[item]?.length : undefined;
        if (count && count > 0) {
          n += 1;
        }
      }
    });

    return n;
  }

  public areOnlyGroupedConditions(conditions: ConditionsBasic): boolean {
    if (conditions && this.groupedAttributes) {
      const keys = Object.keys(this.conditionsUtils.mapConditionsBasicToPathIds(conditions));
      return keys.every((element) => (this.groupedAttributes ? Object.values(this.groupedAttributes).indexOf(element) > -1 : true));
    }

    return true;
  }

  public cleanDeep(object: any) {
    for (const key in object) {
      if (Object.prototype.hasOwnProperty.call(object, key)) {
        if (object[key] !== null && object[key] !== undefined) {
          if (Array.isArray(object[key])) {
            object[key] = object[key].filter((val: any) => val != null);
            object[key].forEach((val: Record<string, unknown>, xkey: string) => {
              object[key][xkey] = this.cleanDeep(val);
            });
          } else if (typeof object[key] === 'object') {
            object[key] = this.cleanDeep(object[key]);
          }
        } else {
          delete object[key];
        }
      }
    }
    return object;
  }

  private onlyClassifications(conditions: ConditionsBasic): boolean {
    const conditionKeys = Object.keys(conditions) as ControlPanelConditions[];

    let areOnlyClassifications = true;
    let anyElement = false;

    conditionKeys.forEach((element: ControlPanelConditions) => {
      if (conditions[element]?.length) {
        anyElement = true;
      }

      if (!element.includes('classifications') && conditions[element]?.length) {
        areOnlyClassifications = false;
      }
    });

    if (!Object.keys(conditions).length || !anyElement) {
      areOnlyClassifications = false;
    }

    return areOnlyClassifications;
  }
}
