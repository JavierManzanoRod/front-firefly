import { Inject, Injectable } from '@angular/core';
import { MayHaveIdName } from '@model/base-api.model';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import {
  FF_ATTRIBUTE_PATHS,
  FF_ENTITY_TYPE_ID_CENTERS,
  FF_ENTITY_TYPE_PRICE_SPECIFICATIONS,
  FF_MULTIVALUED_IDS,
} from 'src/app/configuration/tokens/configuracion';
import {
  ConditionsBasic,
  ControlPanelConditions,
  IClassification,
  IncludedExcludedConditionsBasics,
  IPricesAndDiscounts,
  IRange,
  Ranges,
} from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import {
  Condition2Attribute,
  Condition2MultipleRanges,
  Condition2Simple,
  Condition2SimpleDataTypes,
  Conditions2,
  Conditions2LeafTypes,
  Conditions2NodeTypes,
  Conditions2PriceSpecificationNode,
  Conditions2PriceSpecifications,
  RouteTooltipLabels,
} from '../models/conditions2.model';
import { CLASSIFICATIONS_KEYS } from './conditions2-const';
import { ConditionsTransformUtilsService } from './conditions2-transform-utils.service';
import { Conditions2UtilsService } from './conditions2-utils.service';

@Injectable({
  providedIn: 'root',
})
export class ConditionsTransformMultiplesService {
  constructor(
    private utilsTransform: ConditionsTransformUtilsService,
    private conditionsUtils: Conditions2UtilsService,
    @Inject(FF_ATTRIBUTE_PATHS) private attributeIds: ConfigurationDefinition['attributePath'],
    @Inject(FF_ENTITY_TYPE_PRICE_SPECIFICATIONS)
    private entityTypeIdPriceSpecifications: ConfigurationDefinition['entityTypeIdPriceSpecifications'],
    @Inject(FF_ENTITY_TYPE_ID_CENTERS) private entityTypeCenters: ConfigurationDefinition['entityTypeIdCenters'],
    @Inject(FF_MULTIVALUED_IDS) private multivaluedIds: ConfigurationDefinition['multivaluedIds']
  ) {}

  public transFormMultiples(result: IncludedExcludedConditionsBasics, item: Condition2Simple) {
    result = this.utilsTransform.mergeResults(result, this.nodePriceSpecificationsToCondition(item));
    result = this.utilsTransform.mergeResults(result, this.transformCalificacionToBasic(item));
    if (result.included.ranges) {
      if (this.utilsTransform.removeRangesEmpty(result.included.ranges as IPricesAndDiscounts[])) {
        delete result.included.ranges;
      }
    }

    if (result.excluded.ranges) {
      if (this.utilsTransform.removeRangesEmpty(result.excluded.ranges as IPricesAndDiscounts[])) {
        delete result.excluded.ranges;
      }
    }
  }

  public transformPriceSpecificationsToCondition(
    rangeValues: IPricesAndDiscounts[],
    centers: MayHaveIdName[],
    node_type: Conditions2LeafTypes,
    goToAdvanced: boolean
  ): Conditions2PriceSpecifications | null {
    const centersArray: string[] = [];
    const nodes: Conditions2PriceSpecificationNode[] = [];
    const values = rangeValues ? rangeValues[0] : null;

    if (!centers?.length && !values) {
      return null;
    } else {
      centers?.forEach((item) => centersArray.push(item.id || ''));
      nodes.push({
        node_type: node_type === 'eq' ? Conditions2LeafTypes.INCLUDE_ANY : Conditions2LeafTypes.NOT_INCLUDE_ANY,
        attribute_data_type: Condition2SimpleDataTypes.ENTITY,
        multiple_cardinality: true,
        attribute_label: 'Centers',
        entity_reference: this.entityTypeCenters,
        attribute: {
          identifier: this.attributeIds?.centers || '',
        },
        value: goToAdvanced ? centers : centersArray,
        tooltip_label: RouteTooltipLabels.CENTERS,
      });
    }

    if (centers?.length && !values) {
      return {
        node_type: Conditions2LeafTypes.MATCH_ANY,
        attribute_data_type: Condition2SimpleDataTypes.ENTITY,
        attribute_label: 'Price Specifications',
        entity_reference: this.entityTypeCenters,
        multiple_cardinality: true,
        attribute: {
          identifier: this.entityTypeIdPriceSpecifications || '',
        },
        nodes,
        tooltip_label: RouteTooltipLabels.PRICE_SPECIFICATION,
      };
    } else {
      if (values?.discount?.from || values?.discount?.from === 0 || values?.discount?.to !== null) {
        nodes.push(
          ...this.rangeToNode(
            values?.discount || { from: 0, to: 0 },
            this.conditionsUtils.getAttributeIdentifierPath(this.attributeIds?.discount || ''),
            false
          )
        );
      }

      if (values?.price?.from || values?.price?.from === 0 || values?.price?.to !== null) {
        nodes.push(
          ...this.rangeToNode(
            values?.price || { from: 0, to: 0 },
            this.conditionsUtils.getAttributeIdentifierPath(this.attributeIds?.price || ''),
            true
          )
        );
      }

      return {
        node_type: Conditions2LeafTypes.MATCH_ANY,
        attribute_data_type: Condition2SimpleDataTypes.ENTITY,
        attribute_label: 'Price Specification',
        tooltip_label: RouteTooltipLabels.PRICE_SPECIFICATION,
        multiple_cardinality: true,
        attribute: {
          identifier: this.entityTypeIdPriceSpecifications || '',
        },
        nodes: [
          {
            node_type: Conditions2NodeTypes.and,
            nodes,
          },
        ],
      };
    }
  }

  public classificationToCondition(allClassifications: IClassification[], node_type: Conditions2LeafTypes): any {
    const nodes: any[] = [];
    let attr: Condition2Attribute = { identifier: '' };
    if (allClassifications.every((classification) => classification.values?.length === 0 || typeof classification.values === 'undefined')) {
      return null;
    } else {
      allClassifications.forEach((classification) => {
        let identifierId: string | undefined;
        let valueId: string | undefined;

        if (classification.values?.length === 0 || typeof classification.values === 'undefined') {
          nodes.push(null);
        } else if (classification.key.includes('calificacion')) {
          identifierId = typeof this.multivaluedIds !== 'undefined' ? this.multivaluedIds.classifications.identifier : '';
          valueId = typeof this.multivaluedIds !== 'undefined' ? this.multivaluedIds.classifications.value : '';

          nodes.push({
            node_type: Conditions2NodeTypes.and,
            nodes: [
              {
                attribute_label: 'Identifier',
                attribute_data_type: 'STRING',
                node_type: Conditions2LeafTypes.eq,
                attribute: {
                  identifier: identifierId,
                },
                value: classification.key,
                tooltip_label: RouteTooltipLabels.CLASSIFICATIONS_ID,
              },
              {
                attribute_label: 'Values',
                node_type: node_type === Conditions2LeafTypes.eq ? Conditions2LeafTypes.in : Conditions2LeafTypes.notin,
                attribute: {
                  identifier: valueId,
                },
                value: classification.values,
                tooltip_label: RouteTooltipLabels.CLASSIFICATIONS_VALUE,
              },
            ],
          });
        }
      });
    }

    if (typeof this.attributeIds !== 'undefined') {
      const path = this.attributeIds['classifications1' as ControlPanelConditions];
      attr = path ? this.conditionsUtils.getAttributeIdentifierPath(path) : { identifier: '' };
    }

    return {
      node_type: Conditions2LeafTypes.MATCH_ANY,
      attribute_label: 'Classifications',
      attribute: attr,
      attribute_data_type: 'EMBEDDED',
      nodes: this.utilsTransform.cleanDeep(nodes),
      multiple_cardinality: true,
      tooltip_label: RouteTooltipLabels.CLASSIFICATIONS,
    };
  }

  private nodePriceSpecificationsToCondition(nodes: any): IncludedExcludedConditionsBasics {
    const priceNodes = nodes as Conditions2PriceSpecifications;
    const included: ConditionsBasic = {};
    const excluded: ConditionsBasic = {};

    const result: IncludedExcludedConditionsBasics = { included, excluded };

    priceNodes.nodes.forEach((itemNode: Condition2MultipleRanges | Conditions2PriceSpecificationNode) => {
      if (itemNode.node_type === Conditions2NodeTypes.and) {
        let isIncluded = true;
        const priceCondition: IRange = {};
        const discountCondition: IRange = {};
        itemNode.nodes.forEach((item) => {
          if (item.node_type === Conditions2LeafTypes.INCLUDE_ANY) {
            isIncluded = true;
            const centers: MayHaveIdName[] = this.parseArrToMayHaveLabelArr(item.value as any[], item);
            result.included.centers = centers;
          } else if (item.node_type === Conditions2LeafTypes.NOT_INCLUDE_ANY) {
            isIncluded = false;
            const centers: MayHaveIdName[] = this.parseArrToMayHaveLabelArr(item.value as any[], item);
            result.excluded.centers = centers;
          } else if (item.node_type === Conditions2LeafTypes.GREATER_THAN_OR_EQUALS) {
            const attribute = this.utilsTransform.getConditionType(item.attribute) as unknown as Ranges | Ranges.price;
            if (attribute === Ranges.price) {
              priceCondition.from = item.value as number;
            } else {
              discountCondition.from = item.value as number;
            }
          } else if (item.node_type === Conditions2LeafTypes.LESS_THAN_OR_EQUALS) {
            const attribute = this.utilsTransform.getConditionType(item.attribute) as unknown as Ranges | Ranges.price;
            if (attribute === Ranges.price) {
              priceCondition.to = item.value as number;
            } else {
              discountCondition.to = item.value as number;
            }
          }
        });
        if (isIncluded) {
          included.ranges = [{ price: priceCondition, discount: discountCondition }];
        } else {
          excluded.ranges = [{ price: priceCondition, discount: discountCondition }];
        }
      } else {
        const centers: MayHaveIdName[] = this.parseArrToMayHaveLabelArr(itemNode.value as any[], itemNode);
        if (itemNode.node_type === Conditions2LeafTypes.INCLUDE_ANY) {
          result.included.centers = centers;
        } else {
          result.excluded.centers = centers;
        }
      }
    });

    return result;
  }

  private parseArrToMayHaveLabelArr(arr: any[], item: any): MayHaveIdName[] {
    const arrTransform: MayHaveIdName[] = [];
    if (arr.some((v) => typeof v === 'string')) {
      arr.forEach((cent: string) =>
        arrTransform.push({
          id: cent,
          name: item.entity_info ? item.entity_info[cent].entity_name : '',
        })
      );
    } else {
      arr.forEach((cent: MayHaveIdName) => arrTransform.push({ id: cent.id, name: cent.name }));
    }

    return arrTransform;
  }

  private transformCalificacionToBasic(node: any): IncludedExcludedConditionsBasics {
    const included: any = {};
    const excluded: any = {};
    node?.nodes?.forEach((element: any) => {
      if (element.nodes && element.nodes.length === 2) {
        const arrKeys = Object.keys(CLASSIFICATIONS_KEYS);
        const tempClassification = CLASSIFICATIONS_KEYS as any;
        const findKey = arrKeys.find(
          (k: string) => tempClassification[k] === element.nodes[1].value || tempClassification[k] === element.nodes[0].value
        );
        if (findKey) {
          const value = Array.isArray(element.nodes[0].value) ? element.nodes[0].value : element.nodes[1].value;
          if (element?.nodes?.some((e: Conditions2) => e.node_type === 'in')) {
            included[findKey] = value;
          } else {
            excluded[findKey] = value;
          }
        }
      }
    });
    return { included, excluded };
  }

  private rangeToNode(range: IRange, attribute: Condition2Attribute, isPrice: boolean): Conditions2PriceSpecificationNode[] {
    const nodes: Conditions2PriceSpecificationNode[] = [];
    nodes.push({
      node_type: Conditions2LeafTypes.GREATER_THAN_OR_EQUALS,
      attribute_data_type: Condition2SimpleDataTypes.DOUBLE,
      attribute_label: isPrice ? 'Sale Price' : 'Discount Percentage',
      attribute,
      value: range.from || 0,
      tooltip_label: isPrice ? RouteTooltipLabels.PRICE : RouteTooltipLabels.DISCOUNT,
    });

    if (range.to) {
      nodes.push({
        node_type: Conditions2LeafTypes.LESS_THAN_OR_EQUALS,
        attribute_data_type: Condition2SimpleDataTypes.DOUBLE,
        attribute_label: isPrice ? 'Sale Price' : 'Discount Percentage',
        attribute,
        value: range.to,
        tooltip_label: isPrice ? RouteTooltipLabels.PRICE : RouteTooltipLabels.DISCOUNT,
      });
    }

    return nodes;
  }
}
