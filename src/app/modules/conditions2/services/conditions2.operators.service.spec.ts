import { Conditions2LeafTypes } from '../models/conditions2.model';
import { Conditions2OperatorsService } from './conditions2.operators.service';

describe('Conditions2OperatorsService', () => {
  it('stringOperators is an array containing eq, distinct', () => {
    const service = new Conditions2OperatorsService();
    const stringOperators = service.stringOperators;
    expect(stringOperators.indexOf(Conditions2LeafTypes.eq)).toBeGreaterThan(-1);
    expect(stringOperators.indexOf(Conditions2LeafTypes.distinct)).toBeGreaterThan(-1);
  });

  it('stringMultipleOperators is an array not containing in, notin', () => {
    const service = new Conditions2OperatorsService();
    const stringMultipleOperators = service.stringMultipleOperators;
    expect(stringMultipleOperators.indexOf(Conditions2LeafTypes.in)).toEqual(-1);
    expect(stringMultipleOperators.indexOf(Conditions2LeafTypes.notin)).toEqual(-1);
  });

  it('booleanOperators is defined', () => {
    const service = new Conditions2OperatorsService();
    expect(service.booleanOperators).toBeDefined();
  });

  it('numberOperators is defined', () => {
    const service = new Conditions2OperatorsService();
    expect(service.numberOperators).toBeDefined();
  });

  it('numberMultipleOperators is defined', () => {
    const service = new Conditions2OperatorsService();
    expect(service.numberMultipleOperators).toBeDefined();
  });

  it('entityOperators is defined', () => {
    const service = new Conditions2OperatorsService();
    expect(service.entityOperators).toBeDefined();
  });

  it('entityMultipleOperators is defined', () => {
    const service = new Conditions2OperatorsService();
    expect(service.entityMultipleOperators).toBeDefined();
  });
});
