import { Conditions2LeafTypes, Conditions2NodeTypes } from '../models/conditions2.model';
import { ConditionsTransformMultiplesService } from './conditions2-transform-multiples.service';
import { ConditionsTransformUtilsService } from './conditions2-transform-utils.service';
import { Conditions2UtilsService } from './conditions2-utils.service';

describe('conditions2-transform-multiples.service', () => {
  it('Transform Price Specificatoin in conditions', () => {
    const serviceUtil = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const service = new ConditionsTransformMultiplesService(
      new ConditionsTransformUtilsService(
        serviceUtil,
        {
          barra: 'barra',
          department: 'departament',
          family: 'family',
          sizeCode: 'sizeCode',
        },
        {}
      ),
      serviceUtil,
      {},
      'entityPriceSpecification',
      'entityTypeCenters',
      { classifications: { identifier: 'id', value: 'valor' } }
    );
    const node = service.transformPriceSpecificationsToCondition(
      [
        {
          price: {
            from: 10,
            to: 15,
          },
          discount: {
            from: 6,
            to: 10,
          },
        },
      ],
      [
        {
          id: 'idCenter',
          name: 'nameCenter',
        },
      ],
      Conditions2LeafTypes.eq,
      false
    );

    console.log(node?.nodes[0], 'node');
    expect(node?.node_type).toEqual(Conditions2LeafTypes.MATCH_ANY);
    expect(node?.attribute_data_type).toEqual('ENTITY');
    expect(node?.multiple_cardinality).toBeTruthy();
    expect(node?.attribute?.identifier).toEqual('entityPriceSpecification');
    expect(node?.nodes.length).toEqual(1);
    expect(node?.nodes[0].node_type).toEqual(Conditions2NodeTypes.and);
  });

  it('classificacion to condition', () => {
    const serviceUtil = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const service = new ConditionsTransformMultiplesService(
      new ConditionsTransformUtilsService(
        serviceUtil,
        {
          barra: 'barra',
          department: 'departament',
          family: 'family',
          sizeCode: 'sizeCode',
        },
        {}
      ),
      serviceUtil,
      {},
      'entityPriceSpecification',
      'entityTypeCenters',
      { classifications: { identifier: 'id', value: 'valor' } }
    );
    const node = service.classificationToCondition(
      [
        {
          key: 'calificacion1',
          values: ['1', '2'],
        },
        {
          key: 'calificacion2',
          values: ['3', '4'],
        },
        {
          key: 'test',
          values: ['1', '2'],
        },
      ],
      Conditions2LeafTypes.eq
    );
    console.log(node.nodes, 'node');
    expect(node.node_type).toEqual(Conditions2LeafTypes.MATCH_ANY);
    expect(node.multiple_cardinality).toBeTruthy();
    expect(node.attribute_data_type).toEqual('EMBEDDED');
    expect(node.nodes.length).toEqual(2);
  });
});
