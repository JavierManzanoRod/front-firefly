import { Injectable, Inject } from '@angular/core';
import { MayHaveIdName } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_ATTRIBUTE_PATHS } from 'src/app/configuration/tokens/configuracion';
import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { Conditions2LeafTypes, Condition2Multiple, Condition2EntityNoMultivalued, Condition2Attribute } from '../models/conditions2.model';
import { Conditions2UtilsService } from './conditions2-utils.service';

@Injectable({
  providedIn: 'root',
})
export class Conditions2TransformEntityService {
  constructor(
    private conditionsUtils: Conditions2UtilsService,
    private translate: TranslateService,
    @Inject(FF_ATTRIBUTE_PATHS) private attributeIds: ConfigurationDefinition['attributePath']
  ) {}

  public entitiesToCondition(
    entities: MayHaveIdName[],
    node_type: Conditions2LeafTypes,
    path: string | undefined,
    key: ControlPanelConditions,
    reference: string,
    isMultiple: boolean,
    goToAvanced = false,
    isRecursiveFilter: boolean,
    tooltipLabel?: string
  ): Condition2Multiple | Condition2EntityNoMultivalued | null {
    let attr: Condition2Attribute = { identifier: '' };

    if (entities?.length === 0 || typeof entities === 'undefined') {
      return null;
    } else {
      const entitiesValues: any[] = [];

      entities.forEach((entity) => {
        if (entity.id) {
          // eslint-disable-next-line @typescript-eslint/no-unused-expressions
          goToAvanced ? entitiesValues.push(entity) : entitiesValues.push(entity.id);
        }
      });

      if (typeof this.attributeIds !== 'undefined') {
        // const path = path;
        attr = path ? this.conditionsUtils.getAttributeIdentifierPath(path) : { identifier: '' };
      }

      if (isMultiple) {
        return {
          multiple_cardinality: true,
          attribute_label: this.keyToLabel(key),
          entity_reference: reference,
          attribute_data_type: 'ENTITY',
          node_type: node_type === Conditions2LeafTypes.eq ? Conditions2LeafTypes.INCLUDE_ANY : Conditions2LeafTypes.NOT_INCLUDE_ANY,
          attribute: attr,
          recursive_filter: isRecursiveFilter,
          is_tree_attribute: isRecursiveFilter,
          value: entitiesValues,
          tooltip_label: tooltipLabel,
        } as Condition2Multiple;
      } else {
        return {
          multiple_cardinality: false,
          attribute_label: this.keyToLabel(key),
          entity_reference: reference,
          attribute_data_type: 'ENTITY',
          node_type: node_type === Conditions2LeafTypes.eq ? Conditions2LeafTypes.in : Conditions2LeafTypes.notin,
          attribute: attr,
          recursive_filter: isRecursiveFilter,
          is_tree_attribute: isRecursiveFilter,
          value: entitiesValues,
          tooltip_label: tooltipLabel,
        } as Condition2EntityNoMultivalued;
      }
    }
  }

  private keyToLabel(key: ControlPanelConditions): string {
    if (key.includes('site')) {
      return this.translate.instant('CONDITIONS_BASIC_LABEL.SITE');
    } else if (key.includes('goodTypes')) {
      return this.translate.instant('CONDITIONS_BASIC_LABEL.GOODTYPES');
    }

    return key;
  }
}
