import { Inject, Injectable } from '@angular/core';
import { MayHaveIdName } from '@model/base-api.model';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import {
  FF_ATTRIBUTE_PATHS,
  FF_ENTITY_TYPE_GOOD_TYPE,
  FF_ENTITY_TYPE_ID_CATEGORIES,
  FF_ENTITY_TYPE_ID_SITES,
} from 'src/app/configuration/tokens/configuracion';
import {
  ConditionsBasic,
  ContentGroupCondition,
  ControlPanelConditions,
  IncludedExcludedConditionsBasics,
  IPricesAndDiscounts,
} from '../../../control-panel/shared/models/control-panel-conditions.model';
import { Conditions2, Conditions2LeafTypes, Conditions2NodeTypes, RouteTooltipLabels } from '../models/conditions2.model';
import { CLASSIFICATIONS_KEYS } from './conditions2-const';
import { Conditions2TransformEntityService } from './conditions2-transform-entity.service';
import { ConditionsTransformMultiplesService } from './conditions2-transform-multiples.service';
import { Conditions2TransformReferenceService } from './conditions2-transform-reference.service';
import { Conditions2TransformSimpleService } from './conditions2-transform-simple.service';
import { ConditionsTransformUtilsService } from './conditions2-transform-utils.service';
import { FF_ENTITY_TYPE_ID_BRAND, FF_ENTITY_TYPE_ID_PROVIDER } from '../../../configuration/tokens/configuracion';

@Injectable({
  providedIn: 'root',
})
export class Conditions2BasicService {
  constructor(
    private conditionsTransformSimpleService: Conditions2TransformSimpleService,
    private conditionsTransformEntityService: Conditions2TransformEntityService,
    private conditionsTransformReferenceService: Conditions2TransformReferenceService,
    @Inject(FF_ATTRIBUTE_PATHS) private attributeIds: ConfigurationDefinition['attributePath'],
    @Inject(FF_ENTITY_TYPE_ID_CATEGORIES) private entityTypeCategoriesId: ConfigurationDefinition['entityTypeIdCategories'],
    @Inject(FF_ENTITY_TYPE_ID_SITES) private entityTypeIdSites: ConfigurationDefinition['entityTypeIdSites'],
    @Inject(FF_ENTITY_TYPE_GOOD_TYPE) private entityTypeIdGoodTypes: ConfigurationDefinition['entityTypeIdGoodType'],
    @Inject(FF_ENTITY_TYPE_ID_BRAND) private entityTypeIdBrand: ConfigurationDefinition['entityTypeIdBrand'],
    @Inject(FF_ENTITY_TYPE_ID_PROVIDER) private entityTypeIdProvider: ConfigurationDefinition['entityTypeIdProvider'],
    private conditionsTransformMultiplesService: ConditionsTransformMultiplesService,
    private utilsTransformService: ConditionsTransformUtilsService
  ) {}

  nodeToIncludedExcludedValues(node: Conditions2): IncludedExcludedConditionsBasics {
    const included: ConditionsBasic = {};
    const excluded: ConditionsBasic = {};
    let result: IncludedExcludedConditionsBasics = { included, excluded };

    if (node) {
      if (node.node_type === Conditions2NodeTypes.and) {
        node.nodes.forEach((item) => {
          if (item.node_type === Conditions2NodeTypes.or) {
            result = this.utilsTransformService.mergeResults(result, this.utilsTransformService.nodeOrToCondition(item.nodes));
          } else if (item.node_type === Conditions2LeafTypes.MATCH_ANY) {
            this.conditionsTransformMultiplesService.transFormMultiples(result, item);
          } else {
            result = this.utilsTransformService.mergeResults(result, this.utilsTransformService.conditionToBasic(item));
          }
        });
      } else if (node.node_type === Conditions2NodeTypes.or) {
        result = this.utilsTransformService.mergeResults(result, this.utilsTransformService.nodeOrToCondition(node.nodes));
      } else if (node.node_type === Conditions2LeafTypes.MATCH_ANY) {
        this.conditionsTransformMultiplesService.transFormMultiples(result, node);
      } else {
        result = this.utilsTransformService.mergeResults(result, this.utilsTransformService.conditionToBasic(node));
      }
    }

    return result;
  }

  objectsToConditions(includes: ConditionsBasic, excludes: ConditionsBasic, goToAvanced = false): Conditions2 {
    const nIncludes = includes ? this.utilsTransformService.countConditions(includes) : undefined;
    const nExcludes = excludes ? this.utilsTransformService.countConditions(excludes) : undefined;

    if (
      ((nIncludes && nIncludes === 1) || this.utilsTransformService.areOnlyGroupedConditions(includes)) &&
      (nExcludes === undefined || nExcludes === 0)
    ) {
      const oneInclude = this.utilsTransformService.cleanDeep({ ...this.parse(includes, Conditions2LeafTypes.eq, goToAvanced) });
      return oneInclude[Object.keys(oneInclude)[0]] || [];
    }

    if (
      ((nExcludes && nExcludes === 1) || this.utilsTransformService.areOnlyGroupedConditions(excludes)) &&
      (nIncludes === undefined || nIncludes === 0)
    ) {
      const oneExclude = this.utilsTransformService.cleanDeep({ ...this.parse(excludes, Conditions2LeafTypes.notin, goToAvanced) });
      return oneExclude[Object.keys(oneExclude)[0]] || [];
    }

    return this.utilsTransformService.cleanDeep({
      node_type: Conditions2NodeTypes.and,
      nodes: [
        ...this.parse(includes, Conditions2LeafTypes.eq, goToAvanced),
        ...this.parse(excludes, Conditions2LeafTypes.notin, goToAvanced),
      ],
    });
  }

  private parse(data: ConditionsBasic, node_type: Conditions2LeafTypes, goToAvanced = false) {
    if (!data || Object.keys(data).length === 0) {
      const res = [];
      res[0] = null;
      return res;
    }
    const result = [
      this.utilsTransformService.cleanDeep({
        node_type: Conditions2NodeTypes.or,
        nodes: [
          this.conditionsTransformSimpleService.transformValuesToCondition(
            data.department as string[],
            ControlPanelConditions.department,
            node_type,
            RouteTooltipLabels.DEPARTMENT
          ),
          this.conditionsTransformSimpleService.transformValuesToCondition(
            data.family as string[],
            ControlPanelConditions.family,
            node_type,
            RouteTooltipLabels.FAMILY
          ),
          this.conditionsTransformSimpleService.transformValuesToCondition(
            data.barra as string[],
            ControlPanelConditions.barra,
            node_type,
            RouteTooltipLabels.BARRA
          ),
          this.conditionsTransformSimpleService.transformValuesToCondition(
            data.sizeCode as string[],
            ControlPanelConditions.sizeCode,
            node_type,
            RouteTooltipLabels.SIZECODE
          ),
          this.conditionsTransformSimpleService.transformValuesToCondition(
            data.gtin as string[],
            ControlPanelConditions.gtin,
            node_type,
            RouteTooltipLabels.GTIN
          ),
          this.conditionsTransformSimpleService.transformValuesToCondition(
            data.product as string[],
            ControlPanelConditions.product,
            node_type,
            RouteTooltipLabels.PRODUCT
          ),
          this.conditionsTransformSimpleService.transformValuesToCondition(
            data.saleReference as string[],
            ControlPanelConditions.saleReference,
            node_type,
            RouteTooltipLabels.SALE_REFERENCE
          ),
          // Division
          // Codigo unico
          // Ean
          // Grupo familia
          // Grupo articulo
        ],
      }),
      this.conditionsTransformEntityService.entitiesToCondition(
        data.brand as MayHaveIdName[],
        node_type,
        this.attributeIds?.brand,
        ControlPanelConditions.brand,
        this.entityTypeIdBrand || '',
        false,
        goToAvanced,
        false,
        RouteTooltipLabels.BRAND
      ),
      this.conditionsTransformEntityService.entitiesToCondition(
        data.categories as MayHaveIdName[],
        node_type,
        this.attributeIds?.categories,
        ControlPanelConditions.categories,
        this.entityTypeCategoriesId || '',
        true,
        goToAvanced,
        true,
        RouteTooltipLabels.CATEGORIES
      ),
      this.conditionsTransformEntityService.entitiesToCondition(
        data.sites as MayHaveIdName[],
        node_type,
        this.attributeIds?.sites,
        ControlPanelConditions.sites,
        this.entityTypeIdSites || '',
        false,
        goToAvanced,
        false,
        RouteTooltipLabels.SITE
      ),
      this.conditionsTransformReferenceService.categoryRuleToCondition(data.categoryRules as ContentGroupCondition[]),
      this.conditionsTransformSimpleService.transformValuesToCondition(data.atue as string[], ControlPanelConditions.atue, node_type, 'a'),
      // this.multivalueconditionsTransformEntityService.entitiesToCondition(data.centers as MayHaveIdName[],
      //   node_type, this.attributeIds?.centers, ControlPanelConditions.centers, this.entityTypeCenters || ''),
      // Tamaño
      // Excluidos
      // F alta
      // F incio surtido
      // F disponibilidad
      // F lanzamiento
      this.conditionsTransformSimpleService.transformValuesToCondition(
        data.maker as string[],
        ControlPanelConditions.maker,
        node_type,
        RouteTooltipLabels.MAKER
      ),
      this.conditionsTransformEntityService.entitiesToCondition(
        data.provider as MayHaveIdName[],
        node_type,
        this.attributeIds?.provider,
        ControlPanelConditions.provider,
        this.entityTypeIdProvider || '',
        false,
        goToAvanced,
        false,
        RouteTooltipLabels.PROVIDER
      ),
      // Dsitribuidor
      this.conditionsTransformSimpleService.transformValuesToCondition(
        data.serie as string[],
        ControlPanelConditions.serie,
        node_type,
        RouteTooltipLabels.SERIE
      ),
      this.conditionsTransformEntityService.entitiesToCondition(
        data.goodTypes as MayHaveIdName[],
        node_type,
        this.attributeIds?.goodTypes,
        ControlPanelConditions.goodTypes,
        this.entityTypeIdGoodTypes || '',
        true,
        goToAvanced,
        false,
        RouteTooltipLabels.GOODTYPES
      ),
      this.conditionsTransformMultiplesService.classificationToCondition(
        [
          {
            values: data.classifications1 as string[],
            key: CLASSIFICATIONS_KEYS.classifications1,
          },
          {
            values: data.classifications2 as string[],
            key: CLASSIFICATIONS_KEYS.classifications2,
          },
          {
            values: data.classifications3 as string[],
            key: CLASSIFICATIONS_KEYS.classifications3,
          },
          {
            values: data.classifications4 as string[],
            key: CLASSIFICATIONS_KEYS.classifications4,
          },
          {
            values: data.classifications5 as string[],
            key: CLASSIFICATIONS_KEYS.classifications5,
          },
          {
            values: data.classifications6 as string[],
            key: CLASSIFICATIONS_KEYS.classifications6,
          },
        ],
        node_type
      ),
      this.conditionsTransformSimpleService.transformValuesToCondition(
        data.managementType as string[],
        ControlPanelConditions.managementType,
        node_type,
        RouteTooltipLabels.MANAGEMENT_TYPE
      ),
      this.conditionsTransformSimpleService.transformValuesToCondition(
        data.customisedRequest as string[],
        ControlPanelConditions.customisedRequest,
        node_type,
        RouteTooltipLabels.CUSTOMISED_REQUEST
      ),
      // Pedido suma
      // Reserva sin stock
      this.conditionsTransformSimpleService.transformValuesToCondition(
        data.luxuryReference as string[],
        ControlPanelConditions.luxuryReference,
        node_type,
        RouteTooltipLabels.LUXURY_REFERENCE
      ),
      this.conditionsTransformSimpleService.transformValuesToCondition(
        data.referenceType as string[],
        ControlPanelConditions.referenceType,
        node_type,
        RouteTooltipLabels.REFERENCE_TYPE
      ),
      this.conditionsTransformSimpleService.transformValuesToCondition(
        data.infiniteStock as string[],
        ControlPanelConditions.infiniteStock,
        node_type,
        RouteTooltipLabels.INFINITE_STOCK
      ),
      // PVP
      // Coste
      this.conditionsTransformSimpleService.transformValuesToCondition(
        data.margin as string[],
        ControlPanelConditions.margin,
        node_type,
        RouteTooltipLabels.MARGIN
      ),
      this.conditionsTransformMultiplesService.transformPriceSpecificationsToCondition(
        data.ranges as IPricesAndDiscounts[],
        data.centers as MayHaveIdName[],
        node_type,
        goToAvanced
      ),

      // tslint:disable-next-line:max-line-length
      // No en mail - this.conditionsTransformSimpleService.transformValuesToCondition(data.volume as string[], ControlPanelConditions.volume, node_type),
      // tslint:disable-next-line:max-line-length
      // No en mail - this.conditionsTransformSimpleService.transformValuesToCondition(data.internetSignal as string[], ControlPanelConditions.internetSignal, node_type),
    ];

    if (result[0].node_type === Conditions2NodeTypes.or && !result[0].nodes.length) {
      result[0] = null;
    } else if (result[0].nodes.length === 1) {
      result[0] = result[0].nodes[0];
    }

    return result;
  }
}
