/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { Inject, Injectable } from '@angular/core';
import { DateTimeService } from '@core/base/date-time.service';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { ProductTypeTreeService } from 'src/app/catalog/product-type/services/product-type-tree.service';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_ITEM_OFFERED } from 'src/app/configuration/tokens/admin-group-type.token';
import { FF_ATTRIBUTE_PATHS, FF_ENTITY_TYPE_ID_OFFER } from 'src/app/configuration/tokens/configuracion';
import { ConditionsBasic, ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { Condition2SimpleDataTypes } from 'src/app/modules/conditions2/models/conditions2.model';
import { clone } from 'src/app/shared/utils/utils';
import { ERROR_RULES } from '../../conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { UITreeListItem } from '../../ui-tree-list/models/ui-tree-list.model';
import {
  Condition2Attribute,
  Condition2Reference,
  Condition2Simple,
  Condition2SimpleChilds,
  Conditions2,
  Conditions2LeafTypes,
  Conditions2Node,
  Conditions2NodeTypes,
  Conditions2ReferenceNodeType,
} from '../models/conditions2.model';

@Injectable({
  providedIn: 'root',
})
export class Conditions2UtilsService {
  categorieList: string[] = [];

  constructor(
    public dateTimeService: DateTimeService,
    @Inject(FF_ITEM_OFFERED) public itemOffered: string,
    @Inject(FF_ENTITY_TYPE_ID_OFFER) public entityTypeIdOffer: string,
    @Inject(FF_ATTRIBUTE_PATHS) private attributeIds: ConfigurationDefinition['attributePath'],
    public productTypeTree: ProductTypeTreeService
  ) {}

  mapConditionsBasicToPathIds(attributes: ConditionsBasic) {
    const obj: any = {};
    const conditionsKeys = Object.keys(attributes);

    conditionsKeys.forEach((key) => {
      if (attributes[key as ControlPanelConditions]?.length) {
        const path = this.attributeIds ? this.attributeIds[key as ControlPanelConditions] : 'error';
        obj[path as string] = attributes[key as ControlPanelConditions];
      }
    });

    return obj;
  }

  isConditions2Childs(condition: Conditions2): condition is Conditions2Node {
    return (
      !this.isConditions2Node(condition) &&
      !this.isConditions2Reference(condition) &&
      (condition.attribute_data_type === 'EMBEDDED' || condition.attribute_data_type === 'ENTITY') &&
      Object.prototype.hasOwnProperty.call(condition, 'nodes')
    );
  }

  isConditions2Node(condition: Conditions2): condition is Conditions2Node {
    return condition.node_type === Conditions2NodeTypes.and || condition.node_type === Conditions2NodeTypes.or;
  }

  isConditions2Reference(condition: Conditions2): condition is Condition2Reference {
    return condition.node_type === Conditions2ReferenceNodeType.reference;
  }

  isCondition2Simple(condition: Conditions2): condition is Condition2Simple {
    return !this.isConditions2Node(condition) && !this.isConditions2Reference(condition) && !this.isConditions2Childs(condition);
  }

  flatMap(data: Conditions2): Conditions2[] {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const _this = this;
    const tree = (condition: Conditions2) => {
      return {
        *[Symbol.iterator]() {
          if (_this.isCondition2Simple(condition) || _this.isConditions2Reference(condition)) {
            yield condition;
          } else {
            for (const data2 of condition.nodes) {
              yield* Array.from(tree(data2));
            }
          }
        },
      } as any;
    };
    return Array.from(tree(data));
  }

  filterSimples(data: Conditions2): Condition2Simple[] {
    const result: Condition2Simple[] = [];
    const flatten = this.flatMap(data);
    for (const condition of flatten) {
      if (this.isCondition2Simple(condition)) {
        result.push(condition);
      }
    }
    return result;
  }

  filterReferences(data: Conditions2): Condition2Reference[] {
    const result: Condition2Reference[] = [];
    const flatten = this.flatMap(data);
    for (const condition of flatten) {
      if (this.isConditions2Reference(condition)) {
        result.push(condition);
      }
    }
    return result;
  }

  trim(data: Conditions2Node | null): Conditions2 | null {
    if (data == null) {
      return null;
    }

    const trimNode = (dataToTrim: Conditions2Node): Conditions2Node => {
      const resultTrim: Conditions2Node = {
        node_type: dataToTrim.node_type,
        nodes: [],
      };

      for (const leaf of dataToTrim.nodes) {
        if (this.isConditions2Node(leaf)) {
          const result2 = trimNode(leaf);
          if (result2.nodes.length > 1) {
            resultTrim.nodes.push(result2);
          } else if (result2.nodes.length === 1) {
            resultTrim.nodes.push(result2.nodes[0]);
          }
        } else {
          resultTrim.nodes.push(leaf);
        }
      }
      return resultTrim;
    };

    const data2 = clone(data);
    const result = trimNode(data2);
    if (result.nodes.length === 0) {
      return null;
    } else if (result.nodes.length === 1) {
      return result.nodes[0];
    } else {
      return result;
    }
  }

  getAttributeIdentifierPath(path: string): Condition2Attribute {
    const ids = path.split('/');
    let attributePath: Condition2Attribute;
    if (ids.length === 1) {
      attributePath = {
        identifier: ids[0],
      };
    } else {
      attributePath = this.getReferencePath(ids);
    }
    return attributePath;
  }

  getFullIdAsString(attribute: Condition2Attribute): string {
    if (attribute?.reference) {
      return `${attribute?.identifier}/${this.getFullIdAsString(attribute?.reference)}`;
    } else {
      return attribute?.identifier;
    }
  }

  getLastSonAttributeId(path: string): string {
    const ids = path.split('/');

    return ids[ids.length - 1];
  }

  valueToSend(data: Conditions2 | null): Conditions2 | undefined | ERROR_RULES {
    this.categorieList = [];
    if (!data) {
      return undefined;
    } else if (this.isCondition2Simple(data)) {
      return this.simpleValue(data);
    } else if (this.isConditions2Reference(data)) {
      return {
        name: data.reference_name || '',
        reference_id: data.reference_id,
        node_type: data.node_type,
      };
    } else {
      const data2 = clone(data);
      const result = this.walk(data2, true);
      if (result === ERROR_RULES.ERROR_RANGE || result === ERROR_RULES.ERROR_CENTER) {
        return result;
      }
      if (result.nodes.length === 0) {
        return undefined;
      } else {
        return result;
      }
    }
  }

  parseAttribute(x: any, attributeEntity: AttributeEntityType, parent: any, idSubEntity: string = '') {
    if (x.multiple_cardinality) {
      return {
        identifier: attributeEntity.id,
      };
    } else {
      // La ruta de los padres
      return this.getReferencePath([...parent.parent, attributeEntity.id], idSubEntity);
    }
  }

  getAttributeIdentifier(path: AttributeEntityType[]) {
    if (path && Array.isArray(path)) {
      let attrResult: any = null;
      let attr: any = null;
      path.forEach((pa) => {
        if (!attrResult) {
          attrResult = {
            identifier: pa.id,
          };
          attr = attrResult;
        } else {
          attr.reference = {
            identifier: pa.id,
          };
          attr = attr.reference;
        }
      });
      return attrResult;
    }
  }

  /**
   * Transforma un UITreeListItem en una condición, comprobando todos los padres (item.parents) para ver si hay que generar las condiciones
   * de include_all, include_any, not_include_all, not_include_any en función de si los padres son entities o embedded muliples
   */
  transformTreeListItemToCondition(item: any, parent: any[]): Condition2Simple | undefined {
    // Comprobamos si alguno de los items hacia arriba cumple las condiciones de ser de tipo includes con hijos
    let indexIncludes = -1;
    if (item.parents) {
      // Buscamos el primer elemento multivaluado y excluimos los anteriores
      const findIndex: number = parent?.findIndex((v: Condition2Simple) => v.multiple_cardinality);
      if (findIndex > 0) {
        const el = item.parents[findIndex] as Condition2Simple;
        if (findIndex + 1 === item.parents.length && !el.multiple_cardinality) {
          indexIncludes = item.parents.length;
        } else {
          const limit = el.multiple_cardinality ? 1 : 2;
          indexIncludes = item.parents.length - findIndex - limit;
        }
      }

      if (findIndex === 0) {
        indexIncludes = item.parents.length;
      }

      item.parents.reverse();
    }

    let result: Condition2Simple | undefined;

    // Si el indice es mayor de 0 significa que montar un hijos con padres
    if (indexIncludes > 0) {
      result = item.parents?.slice(0, indexIncludes + 1).reduce((acc: any, value: any) => {
        //if (value.data_type === 'ENTITY') {
        // value.recursive_filter = true;
        //}

        if (value.data_type === 'EMBEDDED' || value.data_type === 'ENTITY') {
          value.node_type = 'INCLUDE_ALL';
          value.nodes = [acc];
          return value;
        } else {
          return value;
        }
      }) as Condition2Simple;
    } else {
      // Da igual que sea entity multiple solo se añadira la condicion normal de entitie
      result = item.value;
    }

    return result;
  }

  checkIfCanAddThisChildAndGetCondition(parent: Condition2SimpleChilds, item: UITreeListItem): Condition2Simple | undefined {
    // Le damos la vuelta para empezar por el hijo mas abajo en el arbol e ir subiendo
    let tempNewAtt = clone(item.parents || []);
    const oldAtt: string[] = clone(parent?.value?.parent || parent.parent || []).map((v: string) => v?.replace('exploded_tree::', ''));
    tempNewAtt.pop();
    const temp = clone(tempNewAtt);
    tempNewAtt = tempNewAtt.map((v) => v.attribute_id?.replace('exploded_tree::', ''));
    const canDoBrother = tempNewAtt.join(',').indexOf(oldAtt.join(','));
    // Si el indice es mayor que 1 significa que han arrastrado un hijo de un hijo (o mas hacia abajo) de este padre
    // y entonces tenemos que generar la condicion con los hijos hacia abajo
    if (canDoBrother >= 0 && tempNewAtt.length > 1) {
      // transformTreeListItemToCondition me devolvera todas las condiciones hasta llegar al hijo final, pero esta mal por que solo tenemos
      // que devolver lo que este debajo de parent
      const coditions = this.transformTreeListItemToCondition(item, temp);
      if (coditions) {
        return this.reduceChildsConditionsToFindParent(parent, coditions as Condition2SimpleChilds);
      }
    } else if (tempNewAtt.length === 1 && canDoBrother >= 0) {
      const tempParent = this.reduceParentAttribute(item.parents || []);
      item.value = tempParent?.find((v) => v.attribute_id === item.value.attribute_id);
      return item.value;
    }
  }

  reduceChildsConditionsToFindParent(
    parentToFind: Condition2SimpleChilds,
    condition: Condition2SimpleChilds
  ): Condition2SimpleChilds | undefined {
    if (condition.nodes) {
      if (
        condition.attribute_id === parentToFind.attribute_id ||
        condition.attribute_id === this.getLastSonAttributeId(this.getFullIdAsString(parentToFind.attribute))
      ) {
        return condition.nodes[0] as Condition2SimpleChilds;
      } else {
        return this.reduceChildsConditionsToFindParent(parentToFind, condition.nodes[0] as Condition2SimpleChilds);
      }
    }
  }

  greaterThanNode(nodes: Conditions2[]): boolean {
    return nodes.some((node) => node.node_type === Conditions2LeafTypes.gt);
  }

  public getReferencePath(ids: string[], idSubEntity: string = ''): Condition2Attribute {
    let attrResult: any = null;
    let attr: any = null;
    ids.forEach((id) => {
      if (!attrResult) {
        attrResult = {
          identifier: id.replace('exploded_tree::', ''),
          is_sub_entity_type: id === idSubEntity,
        };
        attr = attrResult;
      } else {
        attr.reference = {
          identifier: id.replace('exploded_tree::', ''),
          is_sub_entity_type: id === idSubEntity,
        };
        attr = attr.reference;
      }
    });
    return attrResult;
  }

  public reduceParentAttribute(data?: any[], idSubEntityId?: string) {
    data = data?.filter((v) => !v.parent_attribute_tree_node_id); // Eliminar esto para que aparezcan los nodos virtuales del arbol
    if (data && (data[0].id === this.entityTypeIdOffer || data[0].identifier === this.entityTypeIdOffer)) {
      data.shift();
    }
    const tempParent = data;
    // Creo la ruta desde el ultimo mulvaluado hasta tu propio valor
    tempParent?.reduce((acc: any, value: any) => {
      value.notParentVirtual = true;

      if (!Array.isArray(acc)) {
        acc.notParentVirtual = true;
      }

      // buscar los nodos con parent_attribute_tree_node_id para que no se tengan en cuenta
      // y darles las propiedas necesarias para parecer un nodo
      // quizas poner el attribute_data_type a virtual
      // luego en el walk donde se carga el node tendremos que omitir este nodo
      // si es parent_attribute_tree_node_id añadir propiedades para node y return acc;
      /* if (value.parent_attribute_tree_node_id) {
        console.log(value, 'value');
        // Formatear objeto para que parezca un nodo
        return acc;
      }*/

      if (value.multiple_cardinality) {
        if (Array.isArray(acc)) {
          value.attribute = this.getReferencePath([...acc, value.attribute_id], idSubEntityId);
        } else if (acc.attribute_id) {
          if (acc.multiple_cardinality) {
            value.attribute = this.getReferencePath([value.attribute_id], idSubEntityId);
          } else {
            value.attribute = this.getReferencePath([acc.attribute_id, value.attribute_id], idSubEntityId);
          }
        }
        return [];
      } else {
        if (!acc.multiple_cardinality) {
          if (Array.isArray(acc)) {
            value.attribute = this.getReferencePath([...acc, value.attribute_id], idSubEntityId);
          } else if (acc.attribute_id) {
            value.attribute = this.getReferencePath([acc.attribute_id, value.attribute_id], idSubEntityId);
          }
        } else {
          if (Array.isArray(acc)) {
            value.attribute = this.getReferencePath([...acc, value.attribute_id], idSubEntityId);
          }
        }
      }

      let tempAcc: any = '';
      if (Array.isArray(acc)) {
        tempAcc = acc;
      } else {
        acc.notParentVirtual = true;
        if (acc.multiple_cardinality) {
          return [value.attribute_id];
        }
        tempAcc = [acc.attribute_id];
      }
      return [...tempAcc, value.attribute_id];
    });

    return tempParent;
  }

  private simpleValue(simple: Condition2Simple): Condition2Simple {
    if (simple.attribute_data_type === 'ENTITY') {
      const leaf2: any = {
        ...simple,
      };
      if (!Object.prototype.hasOwnProperty.call(leaf2, 'recursive_filter')) {
        leaf2.recursive_filter = false;
      }

      if (!leaf2.multiple_cardinality && simple.node_type !== 'in' && simple.node_type !== 'notin') {
        leaf2.value = leaf2.value?.id;
      } else {
        leaf2.value = leaf2.value?.map((v: any) => (v.id ? v.id : v));
      }
      this.checkCategorieValue(leaf2);
      return leaf2;
    } else if (simple.attribute_data_type === 'BOOLEAN') {
      return {
        ...simple,
        value: !!simple.value,
      };
    } else if (simple.attribute_data_type === 'STRING') {
      return {
        ...simple,
        value: simple.value,
      };
    } else if (simple.attribute_data_type === 'DATE_TIME') {
      if (simple.value && !Array.isArray(simple.value)) {
        simple.value = this.dateTimeService.convertUTCDateToSend(new Date(simple.value));
      }
      return simple;
    } else {
      return simple;
    }
  }

  private walk(conditionToSend: any, firtsTime = false, parent: any = null): Conditions2Node | ERROR_RULES {
    let resultTrim: Conditions2Node = {
      node_type: conditionToSend.node_type,
      nodes: [],
    };

    // Si es un childs añadimos lo que viene ya que hay que mandar el campo attribute etc
    if (this.isConditions2Childs(conditionToSend)) {
      if (
        conditionToSend.nodes &&
        conditionToSend.nodes.length === 1 &&
        conditionToSend.nodes[0].node_type === Conditions2NodeTypes.or &&
        conditionToSend.nodes[0].nodes.length === 1
      ) {
        conditionToSend.nodes = conditionToSend.nodes[0].nodes;
      }
      resultTrim = clone(conditionToSend);
      if (firtsTime) {
        resultTrim.nodes = [];
      }
    }
    const tempNodes: any[] = [];
    for (const leaf1 of conditionToSend?.nodes) {
      const tempLeaf: any = leaf1;

      if (
        (tempLeaf.nodes && !tempLeaf.multiple_cardinality && !conditionToSend.multiple_cardinality && leaf1.attribute?.reference) ||
        tempLeaf.attribute_data_type === Condition2SimpleDataTypes.VIRTUAL
      ) {
        // si el condition tiene algun nodo virtual , entonces de unen los nodos con estos
        tempNodes.push(...tempLeaf.nodes);
      } else {
        tempNodes.push(tempLeaf);
      }
    }

    for (const leaf of tempNodes) {
      if (this.isConditions2Node(leaf) || this.isConditions2Childs(leaf) || (leaf.nodes && !leaf.is_multiple_cardinality)) {
        const result2 = this.walk(leaf, firtsTime, conditionToSend.nodes);
        if (result2 === ERROR_RULES.ERROR_RANGE || result2 === ERROR_RULES.ERROR_CENTER) {
          return result2;
        }
        if (leaf.nodes && !leaf.multiple_cardinality && result2.nodes) {
          resultTrim.nodes.push(...result2.nodes);
        } else {
          resultTrim.nodes.push(result2);
        }
      } else {
        if (this.isCondition2Simple(leaf)) {
          const haveCenter = this.checkRangesWithCenter(leaf, conditionToSend.nodes, parent);
          const haveRanges = this.checkCentersWithRanges(leaf, conditionToSend.nodes, parent);
          // si es false deberia lanzar un error
          if (!haveCenter) {
            return ERROR_RULES.ERROR_RANGE;
          }
          if (!haveRanges) {
            return ERROR_RULES.ERROR_CENTER;
          }
          resultTrim.nodes.push(this.simpleValue(leaf));
        } else if (this.isConditions2Reference(leaf)) {
          const dataReference: Condition2Reference = {
            name: leaf.reference_name || '',
            reference_id: leaf.reference_id,
            node_type: leaf.node_type,
          };
          resultTrim.nodes.push(dataReference);
        } else {
          resultTrim.nodes.push(leaf);
        }
      }
    }
    return resultTrim;
  }

  private checkCategorieValue(leaf: any) {
    if (this.getFullIdAsString(leaf.attribute) === this.attributeIds?.categories && leaf.node_type === Conditions2LeafTypes.INCLUDE_ANY) {
      this.categorieList.push(...leaf.value);
    }
  }

  private checkRangesWithCenter(leaf: Condition2Simple, nodes: Conditions2[], parent: any): boolean {
    if (leaf.attribute?.identifier === this.attributeIds?.price || leaf.attribute?.identifier === this.attributeIds?.discount) {
      return (
        nodes.some((node: any) => node.attribute?.identifier === this.attributeIds?.centers) ||
        parent?.some((node: any) => {
          return node?.attribute?.identifier === this.attributeIds?.centers;
        })
      );
    }
    return true;
  }

  private checkCentersWithRanges(leaf: Condition2Simple, nodes: Conditions2[], parent: any): boolean {
    if (leaf.attribute?.identifier === this.attributeIds?.centers) {
      return nodes?.some((node: any) => {
        return (
          node?.nodes?.some(
            (n: any) => n.attribute?.identifier === this.attributeIds?.price || n.attribute?.identifier === this.attributeIds?.discount
          ) ||
          node.attribute?.identifier === this.attributeIds?.price ||
          node.attribute?.identifier === this.attributeIds?.discount
        );
      });
    }
    return true;
  }
}
