import {
  Condition2Simple,
  Conditions2,
  Conditions2LeafTypes,
  Conditions2Node,
  Conditions2NodeTypes,
  Conditions2ReferenceNodeType,
} from '../models/conditions2.model';
import { ERROR_RULES } from './../../conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { Conditions2UtilsService } from './conditions2-utils.service';

describe('Conditions2UtilsService', () => {
  it('isCondition2Simple is a guard that indicates if is a Simple Component', () => {
    const service = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const conditions1: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1',
      attribute_data_type: 'STRING',
    } as Condition2Simple;
    expect(service.isCondition2Simple(conditions1)).toBeTruthy();

    const conditions2: Conditions2 = {
      node_type: Conditions2ReferenceNodeType.reference,
      name: 'name',
      reference_id: 'id',
    };
    expect(service.isCondition2Simple(conditions2)).toBeFalsy();
  });

  it('isConditions2Reference is a guard that indicates if is a ReferenceComponent', () => {
    const service = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const conditions1: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1',
      attribute_data_type: 'STRING',
    } as Condition2Simple;
    expect(service.isConditions2Reference(conditions1)).toBeFalsy();

    const conditions2: Conditions2 = {
      node_type: Conditions2ReferenceNodeType.reference,
      name: 'name',
      reference_id: 'id',
    };
    expect(service.isConditions2Reference(conditions2)).toBeTruthy();
  });

  it('filterSimples extract simples from a condition ', () => {
    const service = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const data: Conditions2 = {
      node_type: Conditions2NodeTypes.and,
      nodes: [
        {
          node_type: Conditions2LeafTypes.eq,
          attribute_id: 'id1',
        } as Condition2Simple,
        {
          node_type: Conditions2LeafTypes.eq,
          attribute_id: 'id2',
        } as Condition2Simple,
        {
          node_type: Conditions2ReferenceNodeType.reference,
          name: 'name',
          reference_id: 'id',
        },
      ],
    };
    expect(service.filterSimples(data).length).toBe(2);
  });

  it('filterReferences extract simples from a condition ', () => {
    const service = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const data: Conditions2 = {
      node_type: Conditions2NodeTypes.and,
      nodes: [
        {
          node_type: Conditions2LeafTypes.eq,
          attribute_id: 'id1',
        } as Condition2Simple,
        {
          node_type: Conditions2LeafTypes.eq,
          attribute_id: 'id2',
        } as Condition2Simple,
        {
          node_type: Conditions2ReferenceNodeType.reference,
          name: 'name',
          reference_id: 'id',
        },
      ],
    };
    expect(service.filterReferences(data).length).toBe(1);
  });

  it('trim omit empty arrays ', () => {
    const service = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const simple = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1',
    } as Condition2Simple;
    const data: Conditions2Node = {
      node_type: Conditions2NodeTypes.or,
      nodes: [
        {
          node_type: Conditions2NodeTypes.and,
          nodes: [],
        },
        {
          node_type: Conditions2NodeTypes.and,
          nodes: [simple],
        },
      ],
    };
    const trimmed = service.trim(data) as Condition2Simple;
    expect(trimmed).not.toBeNull();
    expect(trimmed.attribute_id).toBe('id1');
  });

  it('trim fusion arrays with 1 length', () => {
    const service = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const simple = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1',
    } as Condition2Simple;

    const nodeOne: Conditions2Node = {
      node_type: Conditions2NodeTypes.and,
      nodes: [simple],
    };

    const data: Conditions2Node = {
      node_type: Conditions2NodeTypes.or,
      nodes: [
        {
          node_type: Conditions2NodeTypes.and,
          nodes: [nodeOne],
        },
        {
          node_type: Conditions2NodeTypes.and,
          nodes: [simple],
        },
      ],
    };
    const trimmed = service.trim(data) as Conditions2Node;
    expect(trimmed).not.toBeNull();
    expect(trimmed.nodes.length).toBe(2);
  });

  /*it('valueToSend for conditions simple', () => {
    const service = new Conditions2UtilsService(null, null, null, null, null)

    const simple = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1'
    } as Condition2Simple;

    const data: Conditions2Node = {
      node_type: Conditions2NodeTypes.or,
      nodes: [simple]
    } as Conditions2Node;
    expect(service.valueToSend(data).hasOwnProperty('nodes')).toBeFalsy();
  });*/

  it('valueToSend for conditions simple and data type BOOLEAN', () => {
    const service = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const data: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1',
      attribute_data_type: 'BOOLEAN',
    } as Condition2Simple;

    expect(Object.prototype.hasOwnProperty.call(service.valueToSend(data), 'value')).toBeTruthy();
    expect(service.valueToSend(null)).toBeUndefined();
  });

  it('ValueToSend for reference', () => {
    const service = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const data: Conditions2 = {
      node_type: Conditions2ReferenceNodeType.reference,
      name: 'name',
      reference_id: 'id',
    };

    const value = service.valueToSend(data);
    if (value !== ERROR_RULES.ERROR_RANGE && value !== ERROR_RULES.ERROR_CENTER) {
      expect(value?.node_type).toBe(Conditions2ReferenceNodeType.reference);
    } else {
      expect(value).toEqual('Deberia ser node');
    }
  });
});
