import { TranslateService } from '@ngx-translate/core';
import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { Conditions2LeafTypes } from '../models/conditions2.model';
import { Conditions2TransformEntityService } from './conditions2-transform-entity.service';
import { Conditions2UtilsService } from './conditions2-utils.service';

describe('conditions2-transform-entity.service', () => {
  let service: Conditions2TransformEntityService;

  beforeEach(() => {
    service = new Conditions2TransformEntityService(
      new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any),
      { instant: () => {} } as unknown as TranslateService,
      {}
    );
  });

  it('Transform in node entity simple', () => {
    const node = service.entitiesToCondition(
      [
        {
          name: 'entidad nombre',
          id: 'entidad id',
        },
      ],
      Conditions2LeafTypes.eq,
      'ruta',
      ControlPanelConditions.sites,
      'refrencia_entidad',
      false,
      false,
      true
    );
    const node2 = node?.value && node?.value[0];
    expect(node?.multiple_cardinality).toBeFalsy();
    expect(node?.entity_reference).toEqual('refrencia_entidad');
    expect(node?.attribute_data_type).toEqual('ENTITY');
    expect(node?.node_type).toEqual(Conditions2LeafTypes.in);
    expect(node?.attribute.identifier).toEqual('ruta');
    expect(node?.value?.length).toEqual(1);
    expect(node2).toEqual('entidad id');
  });

  it('Transform in node entity multiple', () => {
    const node = service.entitiesToCondition(
      [
        {
          name: 'entidad nombre',
          id: 'entidad id',
        },
      ],
      Conditions2LeafTypes.eq,
      'ruta',
      ControlPanelConditions.sites,
      'refrencia_entidad',
      true,
      false,
      true
    );
    const node2 = node?.value && node?.value[0];

    expect(node?.multiple_cardinality).toBeTruthy();
    expect(node?.entity_reference).toEqual('refrencia_entidad');
    expect(node?.attribute_data_type).toEqual('ENTITY');
    expect(node?.node_type).toEqual(Conditions2LeafTypes.INCLUDE_ANY);
    expect(node?.attribute.identifier).toEqual('ruta');
    expect(node?.value?.length).toEqual(1);
    expect(node2).toEqual('entidad id');
  });

  it('fail entity', () => {
    const node = service.entitiesToCondition(
      [],
      Conditions2LeafTypes.eq,
      'ruta',
      ControlPanelConditions.sites,
      'refrencia_entidad',
      true,
      false,
      true
    );

    expect(node).toBeNull();
  });
});
