import { Conditions2ReferenceNodeType } from './../models/conditions2.model';
import { Conditions2NodeTypes } from '../models/conditions2.model';
import { Conditions2TransformReferenceService } from './conditions2-transform-reference.service';

describe('conditions2-transform-reference.service', () => {
  it('category rule to condition ', () => {
    const service = new Conditions2TransformReferenceService();
    const nodes = service.categoryRuleToCondition([
      {
        id: 'id1',
        name: 'Referencia 1',
      },
      {
        id: 'id2',
        name: 'Referencia 2',
      },
    ]) as any;
    console.log(nodes, 'nodes');
    expect(nodes?.node_type).toEqual(Conditions2NodeTypes.or);
    expect(nodes.nodes.length).toEqual(2);
    expect(nodes.nodes[0].reference_id).toEqual('id1');
  });

  it('one category rule to condition ', () => {
    const service = new Conditions2TransformReferenceService();
    const nodes = service.categoryRuleToCondition([
      {
        id: 'id1',
        name: 'Referencia 1',
      },
    ]) as any;
    console.log(nodes, 'nodes');
    expect(nodes?.node_type).toEqual(Conditions2ReferenceNodeType.reference);
    expect(nodes.nodes).toBeUndefined();
    expect(nodes.reference_id).toEqual('id1');
  });
});
