import { Injectable } from '@angular/core';
import { Attribute } from '@core/models/attribute.model';
import { Condition2Simple, Condition2SimpleChilds, Conditions2LeafTypes } from '../models/conditions2.model';

// private types to handle operators onfiguration
enum Condition2OperatorFlags {
  flagString = 'flagString',
  flagDate = 'flagDate',
  flagStringMultiple = 'flagStringMultiple',
  flagNumber = 'flagNumber',
  flagNumberMultiple = 'flagNumberMultiple',
  flagBoolean = 'flagBoolean',
  flagEntity = 'flagEntity',
  flagEntityMultiple = 'flagEntityMultiple',
  flagElementWithChilds = 'flagElementWithChilds',
}

type Condition2OperatorFlagValues = Partial<Record<Condition2OperatorFlags, boolean>>;
type Condition2OperatorConfiguration = Partial<Record<Condition2Simple['node_type'], Condition2OperatorFlagValues>>;

@Injectable({
  providedIn: 'root',
})
export class Conditions2OperatorsService {
  public listOperators: string[] = Object.keys(Conditions2LeafTypes);

  private operatorsConfig: Condition2OperatorConfiguration = {
    eq: {
      flagBoolean: true,
      flagNumber: true,
      flagEntity: true,
      flagString: true,
      flagDate: true,
    },
    distinct: {
      flagBoolean: true,
      flagNumber: true,
      flagEntity: true,
      flagString: true,
      flagDate: true,
    },
    /*NOT_START_WITH_IGNORE_CASE: {
      flagString: true,
    },
    NOT_START_WITH: {
      flagString: true,
    },*/
    gt: {
      flagNumber: true,
      flagDate: true,
    },
    lt: {
      flagNumber: true,
      flagDate: true,
    },
    GREATER_THAN_OR_EQUALS: {
      flagNumber: true,
      flagDate: true,
    },
    LESS_THAN_OR_EQUALS: {
      flagNumber: true,
      flagDate: true,
    },
    in: {
      flagString: true,
      flagDate: true,
      flagEntity: true,
      flagNumber: true,
      // flagNumberMultiple: true,
      // flagStringMultiple: true,
      // flagEntityMultiple: true,
    },
    notin: {
      flagString: true,
      flagDate: true,
      flagEntity: true,
      flagNumber: true,
      // flagNumberMultiple: true,
      // flagStringMultiple: true,
      // flagEntityMultiple: true,
    },
    INCLUDE_ALL: {
      flagStringMultiple: true,
      flagEntityMultiple: true,
      flagNumberMultiple: true,
    },
    MATCH_ALL: {
      flagElementWithChilds: true,
    },
    NOT_INCLUDE_ALL: {
      flagStringMultiple: true,
      flagEntityMultiple: true,
      flagNumberMultiple: true,
      // flagElementWithChilds: true
    },
    like: {
      // NOTFOUND like????
      flagString: true,
      flagEntity: true,
    },
    NOT_LIKE: {
      flagString: true,
      flagEntity: true,
    },
    INCLUDE_ANY: {
      flagStringMultiple: true,
      flagEntityMultiple: true,
      flagNumberMultiple: true,
    },
    MATCH_ANY: {
      flagElementWithChilds: true,
    },
    NOT_INCLUDE_ANY: {
      flagStringMultiple: true,
      flagEntityMultiple: true,
      flagNumberMultiple: true,
      //  flagElementWithChilds: true
    },
    LIKE_IGNORE_CASE: {
      flagEntity: true,
      flagString: true,
    },
    NOT_LIKE_IGNORE_CASE: {
      flagEntity: true,
      flagString: true,
    },
    START_WITH: {
      flagString: true,
      flagEntity: true,
    },
    START_WITH_IGNORE_CASE: {
      flagString: true,
      flagEntity: true,
    },
    NOT_START_WITH: {
      flagString: true,
      flagEntity: true,
    },
    IS_DEFINED: {
      flagString: true,
      flagDate: true,
      flagNumber: true,
      flagBoolean: true,
      flagEntity: true,
      flagStringMultiple: true,
      flagNumberMultiple: true,
      flagEntityMultiple: true,
    },
    IS_NOT_DEFINED: {
      flagString: true,
      flagDate: true,
      flagNumber: true,
      flagBoolean: true,
      flagEntity: true,
      flagStringMultiple: true,
      flagNumberMultiple: true,
      flagEntityMultiple: true,
    },
    NOT_MATCH_ANY: {
      flagElementWithChilds: true,
    },
    NOT_MATCH_ALL: {
      flagElementWithChilds: true,
    },
  };

  private _stringMultipleOperators!: Condition2Simple['node_type'][];
  private _stringOperators!: Condition2Simple['node_type'][];
  private _numberOperators!: Condition2Simple['node_type'][];
  private _numberMultipleOperators!: Condition2Simple['node_type'][];
  private _booleanOperators!: Condition2Simple['node_type'][];
  private _dateOperators!: Condition2Simple['node_type'][];
  private _entityOperators!: Condition2Simple['node_type'][];
  private _entityMultipleOperators!: Condition2Simple['node_type'][];
  private _elementWithChildsOperators!: Condition2Simple['node_type'][];

  constructor() {}

  get stringOperators() {
    if (!this._stringOperators) {
      this._stringOperators = this.getOperators(Condition2OperatorFlags.flagString, this.operatorsConfig);
    }
    return this._stringOperators;
  }

  get stringMultipleOperators() {
    if (!this._stringMultipleOperators) {
      this._stringMultipleOperators = this.getOperators(Condition2OperatorFlags.flagStringMultiple, this.operatorsConfig);
    }
    return this._stringMultipleOperators;
  }

  get numberOperators() {
    if (!this._numberOperators) {
      this._numberOperators = this.getOperators(Condition2OperatorFlags.flagNumber, this.operatorsConfig);
    }
    return this._numberOperators;
  }

  get numberMultipleOperators() {
    if (!this._numberMultipleOperators) {
      this._numberMultipleOperators = this.getOperators(Condition2OperatorFlags.flagNumberMultiple, this.operatorsConfig);
    }
    return this._numberMultipleOperators;
  }

  get booleanOperators() {
    if (!this._booleanOperators) {
      this._booleanOperators = this.getOperators(Condition2OperatorFlags.flagBoolean, this.operatorsConfig);
    }
    return this._booleanOperators;
  }

  get entityOperators() {
    if (!this._entityOperators) {
      this._entityOperators = this.getOperators(Condition2OperatorFlags.flagEntity, this.operatorsConfig);
    }
    return this._entityOperators;
  }

  get entityMultipleOperators() {
    if (!this._entityMultipleOperators) {
      this._entityMultipleOperators = this.getOperators(Condition2OperatorFlags.flagEntityMultiple, this.operatorsConfig);
    }
    return this._entityMultipleOperators;
  }

  get dateOperators() {
    if (!this._dateOperators) {
      this._dateOperators = this.getOperators(Condition2OperatorFlags.flagDate, this.operatorsConfig);
    }
    return this._dateOperators;
  }

  get elementWithChildsOperator() {
    if (!this._elementWithChildsOperators) {
      this._elementWithChildsOperators = this.getOperators(Condition2OperatorFlags.flagElementWithChilds, this.operatorsConfig);
    }
    return this._elementWithChildsOperators;
  }

  getAvailableOperators(
    data_type: Attribute['data_type'],
    multiple_cardinality: Attribute['multiple_cardinality'],
    nodes?: Condition2SimpleChilds['nodes']
  ): Condition2Simple['node_type'][] {
    if (data_type === 'STRING') {
      if (multiple_cardinality) {
        return this.stringMultipleOperators;
      } else {
        return this.stringOperators;
      }
    } else if (data_type === 'DOUBLE') {
      if (multiple_cardinality) {
        return this.numberMultipleOperators;
      } else {
        return this.numberOperators;
      }
    } else if (data_type === 'BOOLEAN') {
      return this.booleanOperators;
    } else if (data_type === 'ENTITY') {
      if (multiple_cardinality) {
        if (!nodes) {
          return this.entityMultipleOperators;
        } else {
          return this.elementWithChildsOperator;
        }
      } else {
        return this.entityOperators;
      }
    } else if (data_type === 'EMBEDDED') {
      return this.elementWithChildsOperator;
    } else if (data_type === 'DATE_TIME') {
      return this.dateOperators;
    } else {
      return this._stringOperators;
    }
  }

  private getOperators(leafType: Condition2OperatorFlags, config: Condition2OperatorConfiguration): Condition2Simple['node_type'][] {
    const result: Condition2Simple['node_type'][] = [];
    const operatotsToTest = Object.keys(config) as Condition2Simple['node_type'][];
    // eslint-disable-next-line guard-for-in
    for (const operatorName of operatotsToTest) {
      const flagsToTest: Condition2OperatorFlagValues | undefined = config[operatorName];
      if (flagsToTest && flagsToTest[leafType]) {
        result.push(operatorName);
      }
    }
    return result;
  }
}
