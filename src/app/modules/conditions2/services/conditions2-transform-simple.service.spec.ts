import { TranslateService } from '@ngx-translate/core';
import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { Conditions2LeafTypes, Conditions2NodeTypes } from '../models/conditions2.model';
import { Conditions2TransformSimpleService } from './conditions2-transform-simple.service';
import { Conditions2UtilsService } from './conditions2-utils.service';

describe('conditions2-transform-simple.service', () => {
  it('transform mutliples barra in conditioon', () => {
    const service = new Conditions2TransformSimpleService(
      new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any),
      {
        instant: () => {},
      } as unknown as TranslateService,
      {
        barra: 'barra',
      }
    ) as any;
    const node = service.transformValuesToCondition(['barra1', 'barra2'], ControlPanelConditions.barra, Conditions2LeafTypes.eq);
    console.log(node, 'node');
    expect(node?.node_type).toEqual(Conditions2NodeTypes.or);
    expect(node.nodes.length).toEqual(2);
  });

  it('transform barra in condition', () => {
    const service = new Conditions2TransformSimpleService(
      new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any),
      {
        instant: () => {},
      } as unknown as TranslateService,
      {
        barra: 'barra',
      }
    ) as any;
    const node = service.transformValuesToCondition(['barra1'], ControlPanelConditions.barra, Conditions2LeafTypes.eq);
    console.log(node, 'node');
    expect(node?.node_type).toEqual(Conditions2LeafTypes.eq);
    expect(node.nodes).toBeUndefined();
    expect(node.value).toEqual('barra1');
  });

  it('transform barra notin condition', () => {
    const service = new Conditions2TransformSimpleService(
      new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any),
      {
        instant: () => {},
      } as unknown as TranslateService,
      {
        barra: 'barra',
      }
    ) as any;
    const node = service.transformValuesToCondition(['barra1', 'barra2'], ControlPanelConditions.barra, Conditions2LeafTypes.notin);
    expect(node?.node_type).toEqual(Conditions2LeafTypes.notin);
    expect(node.value.length).toEqual(2);
  });
});
