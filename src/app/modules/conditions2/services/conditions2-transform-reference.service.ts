import { Injectable } from '@angular/core';
import { ContentGroupCondition } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { Condition2Reference, Conditions2Node, Conditions2ReferenceNodeType, Conditions2NodeTypes } from '../models/conditions2.model';

@Injectable({
  providedIn: 'root',
})
export class Conditions2TransformReferenceService {
  public categoryRuleToCondition(value: ContentGroupCondition[]): Condition2Reference | Conditions2Node | null {
    if (value?.length === 0 || value === undefined) {
      return null;
    }

    if (value?.length === 1) {
      return {
        name: value[0].name || '',
        reference_id: value[0].id,
        node_type: Conditions2ReferenceNodeType.reference,
      };
    } else {
      const nodes: Condition2Reference[] = [];
      value?.forEach((contentGroup) => {
        nodes.push({
          name: contentGroup.name || '',
          reference_id: contentGroup.id,
          node_type: Conditions2ReferenceNodeType.reference,
        });
      });

      return {
        node_type: Conditions2NodeTypes.or,
        nodes,
      };
    }
  }
}
