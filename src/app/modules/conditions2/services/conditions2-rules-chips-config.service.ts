import { Inject, Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { GenericApiRequest } from '@model/base-api.model';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { MastersConfiguration } from 'src/app/configuration/models/masters.model';
import {
  FF_ENTITY_TYPE_GOOD_TYPE,
  FF_ENTITY_TYPE_ID_BRAND,
  FF_ENTITY_TYPE_ID_CATEGORIES,
  FF_ENTITY_TYPE_ID_CENTERS,
  FF_ENTITY_TYPE_ID_PROVIDER,
  FF_ENTITY_TYPE_ID_SITES,
  FF_MASTERS,
} from 'src/app/configuration/tokens/configuracion';
import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { CategoryRuleFolderService } from 'src/app/rule-engine/category-rule/services/category-rule-folder.service';
import { CategoryRuleService } from 'src/app/rule-engine/category-rule/services/category-rule.service';
import { EntityService } from 'src/app/rule-engine/entity/services/entity.service';
import { checkSpaceValidator } from 'src/app/shared/validations/check-space.validator';
import { minMaxLengthValidator } from 'src/app/shared/validations/min-max-length.validator';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../chips-control/components/chips.control.model';
import { ExplodedTreeNodeComponent } from '../../exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { ExplodedEvent } from '../../exploded-tree/models/exploded-tree.model';
import { UIModalRangeValue } from '../../ui-modal-select/models/modal-select.model';

@Injectable({
  providedIn: 'root',
})
export class Conditions2RulesChipsConfig {
  constructor(
    private categoryRuleService: CategoryRuleService,
    private apiEntityService: EntityService,
    private ruleFoldersService: CategoryRuleFolderService,

    @Inject(FF_MASTERS) private masterIds: MastersConfiguration['masters'],
    @Inject(FF_ENTITY_TYPE_ID_CATEGORIES)
    private entityTypeCategoriesId: ConfigurationDefinition['entityTypeIdCategories'],
    @Inject(FF_ENTITY_TYPE_ID_SITES)
    private entityTypeSitesId: ConfigurationDefinition['entityTypeIdSites'],
    @Inject(FF_ENTITY_TYPE_ID_CENTERS)
    private entityTypeCenters: ConfigurationDefinition['entityTypeIdCenters'],
    @Inject(FF_ENTITY_TYPE_GOOD_TYPE) private entityTypeIdGoodType: ConfigurationDefinition['entityTypeIdGoodType'],
    @Inject(FF_ENTITY_TYPE_ID_BRAND) private entityTypeIdBrand: ConfigurationDefinition['entityTypeIdBrand'],
    @Inject(FF_ENTITY_TYPE_ID_PROVIDER) private entityTypeIdProvider: ConfigurationDefinition['entityTypeIdProvider']
  ) {}

  getData(idAdminGroup: string): Partial<Record<ControlPanelConditions, ChipsControlSelectConfig>> {
    return {
      atue: {
        label: 'SPENTER.INCLUDED_FORM.ATUE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.ATUE',
        },
      },
      gtin: {
        label: 'SPENTER.INCLUDED_FORM.GTIN',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.GTIN',
          canImportCSV: true,
        },
      },
      barra: {
        label: 'SPENTER.INCLUDED_FORM.BARRA',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.BARRA',
          inputValidators: [minMaxLengthValidator(12, 12), checkSpaceValidator()],
          validatorErrors: [
            { type: 'invalidLength', message: 'SPENTER.ERRORS.BARRA_INVALID_LENGTH' },
            { type: 'spaceValidator', message: 'SPENTER.ERRORS.SPACE' },
          ],
        },
      },
      brand: {
        label: 'SPENTER.INCLUDED_FORM.BRAND',
        modalType: ModalChipsTypes.select,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.BRAND',
          multiple: true,
          searchFnOnInit: true,
          itemLabelKey: ['name', 'label'],
          searchParam: 'label_attribute',
          searchFn: (x) => {
            return this.apiEntityService.listWithFilter(this.entityTypeIdBrand ? this.entityTypeIdBrand : '', x);
          },
        },
      },
      categories: {
        label: 'SPENTER.INCLUDED_FORM.CATEGORIES_TITLE',
        modalType: ModalChipsTypes.select,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.CATEGORIES_LABEL',
          multiple: true,
          searchFnOnInit: true,
          keyTooltip: 'name',
          itemLabelKey: ['name', 'label'],
          searchParam: 'label_attribute',
          searchFn: (x) => {
            return this.apiEntityService.listWithFilter(this.entityTypeCategoriesId ? this.entityTypeCategoriesId : '', x);
          },
        },
      },
      categoryRules: {
        label: 'SPENTER.INCLUDED_FORM.CONTENT_GROUP_TITLE',
        modalType: ModalChipsTypes.tree,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.CONTENT_GROUP_LABEL',
          multiple: true,
          searchFnOnInit: true,
          searchFn: (x: GenericApiRequest) => {
            return this.categoryRuleService.list({
              ...x,
              admin_group_type_id: idAdminGroup,
            });
          },
          tree: () => this.ruleFoldersService.listFolders(),
          clickNode: ({ data, target }: ExplodedEvent<ExplodedTreeNodeComponent>) => {
            if (data.id && !data.value?.isRule) {
              if (!data.children) {
                target.setLoading(true);
                this.ruleFoldersService.detail(data.id).subscribe((response) => {
                  data.children = (data.children || []).concat(response.folders || []).concat(
                    (response.category_rules || []).map((rule) => {
                      return { ...rule, id: rule.identifier, value: { isRule: true } };
                    })
                  );

                  if (!data.children.length) {
                    target.setWarning('Carpeta vacía');
                  }

                  target.expand();
                  target.setLoading(false);
                });
              }
              return false;
            }
            return true;
          },
          iconFilter: (target: ExplodedTreeNodeComponent) => {
            if (target.data.value?.isRule) {
              return 'icon-agrupaciones';
            }

            return 'icon-folder';
          },
        },
      },
      centers: {
        label: 'SPENTER.INCLUDED_FORM.CENTERS',
        modalType: ModalChipsTypes.select,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.CENTERS',
          multiple: true,
          searchFnOnInit: true,
          itemLabelKey: ['name', 'label'],
          searchParam: 'label_attribute',
          searchFn: (x) => {
            return this.apiEntityService.listWithFilter(this.entityTypeCenters ? this.entityTypeCenters : '', x);
          },
        },
      },
      classifications1: {
        label: 'SPENTER.INCLUDED_FORM.CALIFICATION1',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.CALIFICATION1',
        },
      },
      classifications2: {
        label: 'SPENTER.INCLUDED_FORM.CALIFICATION2',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.CALIFICATION2',
        },
      },
      classifications3: {
        label: 'SPENTER.INCLUDED_FORM.CALIFICATION3',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.CALIFICATION3',
        },
      },
      classifications4: {
        label: 'SPENTER.INCLUDED_FORM.CALIFICATION4',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.CALIFICATION4',
        },
      },
      classifications5: {
        label: 'SPENTER.INCLUDED_FORM.CALIFICATION5',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.CALIFICATION5',
        },
      },
      classifications6: {
        label: 'SPENTER.INCLUDED_FORM.CALIFICATION6',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.CALIFICATION6',
        },
      },
      customisedRequest: {
        label: 'SPENTER.INCLUDED_FORM.CUSTOM_ORDER',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.CUSTOM_ORDER',
        },
      },
      department: {
        label: 'SPENTER.INCLUDED_FORM.DEPARTAMENT_TITLE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.DEPARTAMENT_LABEL',
          inputValidators: [minMaxLengthValidator(4, 4), checkSpaceValidator()],
          validatorErrors: [
            { type: 'invalidLength', message: 'SPENTER.ERRORS.DEPARTMENT_INVALID_LENGTH' },
            { type: 'spaceValidator', message: 'SPENTER.ERRORS.SPACE' },
          ],
        },
      },
      family: {
        label: 'SPENTER.INCLUDED_FORM.FAMILY_TITLE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.FAMILY_LABEL',
          inputValidators: [minMaxLengthValidator(7, 7), checkSpaceValidator()],
          validatorErrors: [
            { type: 'invalidLength', message: 'SPENTER.ERRORS.FAMILY_INVALID_LENGTH' },
            { type: 'spaceValidator', message: 'SPENTER.ERRORS.SPACE' },
          ],
        },
      },
      goodTypes: {
        label: 'SPENTER.INCLUDED_FORM.MERCHANDISE_TYPE',
        modalType: ModalChipsTypes.select,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.MERCHANDISE_TYPE',
          multiple: true,
          searchFnOnInit: true,
          itemLabelKey: ['name', 'label'],
          searchParam: 'label_attribute',
          searchFn: (x) => {
            return this.apiEntityService.listWithFilter(this.entityTypeIdGoodType ? this.entityTypeIdGoodType : '', x);
          },
        },
      },
      infiniteStock: {
        label: 'SPENTER.INCLUDED_FORM.INFINITE_STOCK',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.INFINITE_STOCK',
        },
      },
      internetSignal: {
        label: 'SPENTER.INCLUDED_FORM.INTERNET_SIGNAL',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.INTERNET_SIGNAL',
        },
      },
      luxuryReference: {
        label: 'SPENTER.INCLUDED_FORM.LUXURY_REFERENCE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.LUXURY_REFERENCE',
        },
      },
      maker: {
        label: 'SPENTER.INCLUDED_FORM.MAKER',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.MAKER',
        },
      },
      managementType: {
        label: 'SPENTER.INCLUDED_FORM.GESTION_TYPE_TITLE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.GESTION_TYPE_LABEL',
        },
      },
      margin: {
        label: 'SPENTER.INCLUDED_FORM.MARGIN',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.MARGIN',
        },
      },
      product: {
        label: 'SPENTER.INCLUDED_FORM.PRODUCT',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          canImportCSV: true,
          title: 'SPENTER.INCLUDED_FORM.PRODUCT',
        },
      },
      provider: {
        label: 'SPENTER.INCLUDED_FORM.SUPLIER',
        modalType: ModalChipsTypes.select,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.SUPLIER',
          multiple: true,
          searchFnOnInit: true,
          itemLabelKey: ['name', 'label'],
          searchParam: 'label_attribute',
          searchFn: (x) => {
            return this.apiEntityService.listWithFilter(this.entityTypeIdProvider ? this.entityTypeIdProvider : '', x);
          },
        },
      },
      ranges: {
        label: 'SPENTER.INCLUDED_FORM.RANGES.LABEL',
        modalType: ModalChipsTypes.range,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.RANGES.LABEL',
          // message: 'SPENTER.INCLUDED_FORM.RANGES.EXTRA_MESSAGE',
          ranges: [
            {
              key: 'price',
              label: 'SPENTER.INCLUDED_FORM.RANGES.PRICE.LABEL',
              startLabel: 'SPENTER.INCLUDED_FORM.RANGES.PRICE.START',
              endLabel: 'SPENTER.INCLUDED_FORM.RANGES.PRICE.END',
              prefix: '€',
              error: 'SPENTER.INCLUDED_FORM.RANGES.PRICE.ERROR',
              placeholder: 'SPENTER.INCLUDED_FORM.RANGES.PRICE.PLACEHOLDER',
              min: 0,
            },
            {
              key: 'discount',
              label: 'SPENTER.INCLUDED_FORM.RANGES.DISCOUNTS.LABEL',
              startLabel: 'SPENTER.INCLUDED_FORM.RANGES.DISCOUNTS.START',
              endLabel: 'SPENTER.INCLUDED_FORM.RANGES.DISCOUNTS.END',
              prefix: '%',
              error: 'SPENTER.INCLUDED_FORM.RANGES.DISCOUNTS.ERROR',
              placeholder: 'SPENTER.INCLUDED_FORM.RANGES.DISCOUNTS.PLACEHOLDER',
              min: 0,
              max: 100,
            },
          ],
        },
        chipFormat: (v: UIModalRangeValue[]) => {
          const value = v?.length >= 0 ? v[0] : null;
          const label = [];
          const price = [];
          const discount = [];

          if (!value) {
            return '';
          }

          if (value.price && Object.keys(value.price).length) {
            if (value.price.from === null) {
              price.push(`${0}€`);
            }
            if (value.price.from !== null) {
              price.push(`${value.price.from}€`);
            }
            if (value.price.to !== null && value.price.to !== undefined) {
              price.push(`${value.price.to}€`);
            }

            if (price.length) {
              label.push(price.join(' - '));
            }
          }

          if (value.discount && Object.keys(value.discount).length) {
            if (value.discount.from === null) {
              price.push(`${0}%`);
            }
            if (value.discount.from !== null) {
              discount.push(`${value.discount.from}%`);
            }
            if (value.discount.to !== null && value.discount.to !== undefined) {
              discount.push(`${value.discount.to}%`);
            }

            if (discount.length) {
              label.push(discount.join(' - '));
            }
          }

          if (label.length) {
            return label.join(' / ');
          }

          return '';
        },
      },
      referenceType: {
        label: 'SPENTER.INCLUDED_FORM.REFERENCE_TYPE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.REFERENCE_TYPE',
        },
      },
      saleReference: {
        label: 'SPENTER.INCLUDED_FORM.REFERENCE_TITLE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.REFERENCE_LABEL',
          canImportCSV: true,
          inputValidators: [minMaxLengthValidator(18, 18)],
          validatorErrors: [{ type: 'invalidLength', message: 'SPENTER.ERRORS.REFERENCE_INVALID_LENGTH' }],
        },
      },
      serie: {
        label: 'SPENTER.INCLUDED_FORM.SERIE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.SERIE',
        },
      },
      sites: {
        label: 'SPENTER.INCLUDED_FORM.SITES_TITLE',
        modalType: ModalChipsTypes.select,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.SITES_LABEL',
          multiple: true,
          searchFnOnInit: true,
          itemLabelKey: ['name', 'label'],
          searchParam: 'label_attribute',
          searchFn: (x) => {
            return this.apiEntityService.listWithFilter(this.entityTypeSitesId ? this.entityTypeSitesId : '', x);
          },
        },
      },
      sizeCode: {
        label: 'SPENTER.INCLUDED_FORM.SIZE_CODE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.SIZE_CODE',
        },
      },
      volume: {
        label: 'SPENTER.INCLUDED_FORM.VOLUME',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'SPENTER.INCLUDED_FORM.VOLUME',
        },
      },
    };
  }
}
