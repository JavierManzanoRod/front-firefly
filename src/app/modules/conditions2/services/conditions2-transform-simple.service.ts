import { Inject, Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_ATTRIBUTE_PATHS } from 'src/app/configuration/tokens/configuracion';
import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import {
  Conditions2LeafTypes,
  Conditions2,
  Conditions2NodeTypes,
  Condition2SimpleString,
  Condition2Attribute,
  Condition2SimpleDataTypes,
} from '../models/conditions2.model';
import { Conditions2UtilsService } from './conditions2-utils.service';

@Injectable({
  providedIn: 'root',
})
export class Conditions2TransformSimpleService {
  constructor(
    private conditionsUtils: Conditions2UtilsService,
    private translateSrv: TranslateService,
    @Inject(FF_ATTRIBUTE_PATHS) private attributeIds: ConfigurationDefinition['attributePath']
  ) {}

  public transformValuesToCondition(
    values: string[],
    key: ControlPanelConditions,
    node_type: Conditions2LeafTypes,
    routeTooltip: string
  ): Conditions2 | null {
    if (values && values.length && node_type === Conditions2LeafTypes.eq) {
      // Si hay mas de un valor se devuelve un grupo de condiciones OR con todos los posibles valores
      if (values.length > 1) {
        return {
          node_type: Conditions2NodeTypes.or,
          nodes: values.map((value) => {
            return this.valueToCondition(value, key, node_type, routeTooltip);
          }),
        };
      } else {
        // Si solo hay un valor devolvemos la condicion
        return this.valueToCondition(values[0], key, node_type, routeTooltip);
      }
    } else if (values && values.length && node_type === Conditions2LeafTypes.notin) {
      return this.valueToNotinCondition(values, key, routeTooltip);
    }
    return null;
  }

  private valueToCondition(
    value: string,
    key: ControlPanelConditions,
    node_type: Conditions2LeafTypes,
    routeTooltip: string
  ): Condition2SimpleString {
    let attr: Condition2Attribute = { identifier: '' };
    let attributeId = '';

    if (typeof this.attributeIds !== 'undefined') {
      const path = this.attributeIds[key];
      attr = path ? this.conditionsUtils.getAttributeIdentifierPath(path) : { identifier: '' };
      attributeId = path ? this.conditionsUtils.getLastSonAttributeId(path) : '';
    }

    return {
      attribute_data_type: Condition2SimpleDataTypes.STRING,
      attribute_id: attributeId,
      attribute_label: this.keyToLabel(key),
      entity_type_name: '',
      multiple_cardinality: false,
      entity_type_id: '',
      node_type,
      value,
      attribute: attr,
      tooltip_label: routeTooltip,
    };
  }

  private keyToLabel(key: ControlPanelConditions): string {
    return this.translateSrv.instant(`CONDITIONS_BASIC_LABEL.${key.toUpperCase()}`);
  }

  private valueToNotinCondition(values: string[], key: ControlPanelConditions, routeTooltip: string): Condition2SimpleString {
    let attr: Condition2Attribute = { identifier: '' };
    let attributeId = '';

    if (typeof this.attributeIds !== 'undefined') {
      const path = this.attributeIds[key];
      attr = path ? this.conditionsUtils.getAttributeIdentifierPath(path) : { identifier: '' };
      attributeId = path ? this.conditionsUtils.getLastSonAttributeId(path) : '';
    }

    return {
      attribute_data_type: Condition2SimpleDataTypes.STRING,
      attribute_id: attributeId,
      attribute_label: this.keyToLabel(key),
      entity_type_name: '',
      multiple_cardinality: false,
      entity_type_id: '',
      node_type: Conditions2LeafTypes.notin,
      value: values,
      attribute: attr,
      tooltip_label: routeTooltip,
    };
  }
}
