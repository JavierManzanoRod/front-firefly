import { MayHaveIdName } from '@model/base-api.model';

export const enum Conditions2NodeTypes {
  and = 'and',
  or = 'or',
}

export interface Conditions2Node {
  node_type: Conditions2NodeTypes.and | Conditions2NodeTypes.or;
  range?: boolean;
  nodes: Conditions2[];
}

export enum Conditions2LeafTypes {
  eq = 'eq',
  // NOT_START_WITH_IGNORE_CASE = 'NOT_START_WITH_IGNORE_CASE',
  // NOT_START_WITH = 'NOT_START_WITH',
  distinct = 'distinct',
  like = 'like',
  NOT_LIKE = 'NOT_LIKE',
  gt = 'gt',
  lt = 'lt',
  in = 'in',
  notin = 'notin',
  INCLUDE_ALL = 'INCLUDE_ALL',
  NOT_INCLUDE_ALL = 'NOT_INCLUDE_ALL',
  INCLUDE_ANY = 'INCLUDE_ANY',
  NOT_INCLUDE_ANY = 'NOT_INCLUDE_ANY',
  START_WITH = 'START_WITH',
  IS_DEFINED = 'IS_DEFINED',
  LESS_THAN_OR_EQUALS = 'LESS_THAN_OR_EQUALS',
  GREATER_THAN_OR_EQUALS = 'GREATER_THAN_OR_EQUALS',
  IS_NOT_DEFINED = 'IS_NOT_DEFINED',
  LIKE_IGNORE_CASE = 'LIKE_IGNORE_CASE',
  NOT_LIKE_IGNORE_CASE = 'NOT_LIKE_IGNORE_CASE',
  START_WITH_IGNORE_CASE = 'START_WITH_IGNORE_CASE',
  MATCH_ANY = 'MATCH_ANY',
  MATCH_ALL = 'MATCH_ALL',
  NOT_MATCH_ANY = 'NOT_MATCH_ANY',
  NOT_MATCH_ALL = 'NOT_MATCH_ALL',
  NOT_START_WITH = 'NOT_START_WITH',
}

export enum Condition2SimpleDataTypes {
  STRING = 'STRING',
  DOUBLE = 'DOUBLE',
  BOOLEAN = 'BOOLEAN',
  ENTITY = 'ENTITY',
  VIRTUAL = 'VIRTUAL',
  EMBEDDED = 'EMBEDDED',
  DATE_TIME = 'DATE_TIME',
}

export interface Condition2Attribute {
  identifier: string;
  reference?: Condition2Attribute;
  is_multiple_cardinality?: boolean;
  multiple_cardinality?: boolean;
  recursive_filter?: boolean;
  is_tree_attribute?: boolean;
  is_sub_entity_type?: boolean;
  entity_reference?: string;
  is_i18n?: boolean;
  label?: string;
  name?: string;
  attribute_label?: boolean;
  data_type?: string;
}

export interface Condition2SimpleBase {
  attribute_id: string;
  attribute_label: string;
  entity_type_id: string;
  is_tree_attribute?: boolean;
  entity_type_name: string;
  entity_info?: any;
  tooltip_label?: string;
  multiple_cardinality: boolean;
  attribute: Condition2Attribute;
  range?: boolean;
  node_type:
    | Conditions2LeafTypes.eq
    | Conditions2LeafTypes.like
    | Conditions2LeafTypes.distinct
    // | Conditions2LeafTypes.NOT_START_WITH
    // | Conditions2LeafTypes.NOT_START_WITH_IGNORE_CASE
    | Conditions2LeafTypes.gt
    | Conditions2LeafTypes.lt
    | Conditions2LeafTypes.GREATER_THAN_OR_EQUALS
    | Conditions2LeafTypes.LESS_THAN_OR_EQUALS
    | Conditions2LeafTypes.in
    | Conditions2LeafTypes.notin
    | Conditions2LeafTypes.NOT_LIKE
    | Conditions2LeafTypes.INCLUDE_ALL
    | Conditions2LeafTypes.INCLUDE_ANY
    | Conditions2LeafTypes.NOT_INCLUDE_ALL
    | Conditions2LeafTypes.NOT_INCLUDE_ANY
    | Conditions2LeafTypes.START_WITH
    | Conditions2LeafTypes.START_WITH_IGNORE_CASE
    | Conditions2LeafTypes.IS_DEFINED
    | Conditions2LeafTypes.IS_NOT_DEFINED
    | Conditions2LeafTypes.LIKE_IGNORE_CASE
    | Conditions2LeafTypes.MATCH_ALL
    | Conditions2LeafTypes.MATCH_ANY
    | Conditions2LeafTypes.NOT_LIKE_IGNORE_CASE
    | Conditions2LeafTypes.NOT_MATCH_ANY
    | Conditions2LeafTypes.NOT_MATCH_ALL
    | Conditions2LeafTypes.NOT_START_WITH;
}

export interface Condition2SimpleString extends Condition2SimpleBase {
  attribute_data_type: Condition2SimpleDataTypes.STRING | Condition2SimpleDataTypes.VIRTUAL;
  value?: string | string[] | null;
}
export interface Condition2SimpleDate extends Condition2SimpleBase {
  attribute_data_type: Condition2SimpleDataTypes.DATE_TIME;
  value?: string | string[] | null;
}

export interface Condition2SimpleDouble extends Condition2SimpleBase {
  attribute_data_type: Condition2SimpleDataTypes.DOUBLE;
  value?: number | number[] | null;
}

export interface Condition2SimpleBoolean extends Condition2SimpleBase {
  attribute_data_type: Condition2SimpleDataTypes.BOOLEAN;
  value?: boolean | null;
}

export interface Condition2SimpleChilds extends Condition2SimpleBase {
  parent: any;
  notParentVirtual?: boolean;
  attribute_data_type: Condition2SimpleDataTypes.EMBEDDED | Condition2SimpleDataTypes.ENTITY;
  nodes: Conditions2[];
  value?: any;
}

export interface ConditionsSimpleEntityInfo {
  entity_id: string;
  entity_name: string;
  entity_type_id: string;
  entity_type_name: string;
  labels?: {
    [label: string]: string;
  };
}

export interface Condition2SimpleEntity extends Condition2SimpleBase {
  attribute_data_type: Condition2SimpleDataTypes.ENTITY;
  entity_reference: string;
  entity_reference_name: string;
  value?: string | string[] | null | { id: string; name: string } | { id: string; name: string }[];
  recursive_filter: boolean;
  entity_info: {
    [label: string]: ConditionsSimpleEntityInfo;
  };
}

export type Condition2Simple =
  | Condition2SimpleString
  | Condition2SimpleBoolean
  | Condition2SimpleDouble
  | Condition2SimpleEntity
  | Condition2SimpleDate
  | Condition2SimpleChilds;

export const enum Conditions2ReferenceNodeType {
  reference = 'reference',
}

export interface Condition2Reference {
  node_type: Conditions2ReferenceNodeType.reference;
  reference_id: string;
  name: string;
  range?: boolean;
  reference_name?: string;
}

export interface Conditions2PriceSpecifications {
  node_type: Conditions2LeafTypes.MATCH_ANY;
  multiple_cardinality: boolean;
  attribute_data_type: string;
  attribute_label: string;
  entity_reference?: string;
  attribute?: Condition2Attribute;
  tooltip_label?: string;
  nodes: Condition2MultipleRanges[] | Conditions2PriceSpecificationNode[];
}

export interface Condition2MultipleRanges {
  node_type: Conditions2NodeTypes.and;
  nodes: Conditions2PriceSpecificationNode[];
}

export interface Conditions2PriceSpecificationNode {
  node_type:
    | Conditions2LeafTypes.INCLUDE_ANY
    | Conditions2LeafTypes.NOT_INCLUDE_ANY
    | Conditions2LeafTypes.LESS_THAN_OR_EQUALS
    | Conditions2LeafTypes.GREATER_THAN_OR_EQUALS;
  attribute: Condition2Attribute;
  entity_info?: {
    [label: string]: ConditionsSimpleEntityInfo;
  };
  attribute_data_type: Condition2SimpleDataTypes;
  multiple_cardinality?: boolean;
  entity_reference?: string;
  attribute_label: string;
  value: string[] | number | MayHaveIdName[];
  tooltip_label?: string;
}

export type Conditions2 = Conditions2Node | Condition2Simple | Condition2Reference;

export enum ConditionsTypeSearch {
  searchAttributes = 'searchAttributes',
  searchReferences = 'searchReferences',
}

export interface Condition2MultipleNode {
  node_type: Conditions2NodeTypes.and;
}
export interface Condition2Multiple {
  node_type:
    | Conditions2LeafTypes.MATCH_ALL
    | Conditions2LeafTypes.MATCH_ANY
    | Conditions2LeafTypes.NOT_INCLUDE_ANY
    | Conditions2LeafTypes.INCLUDE_ANY;
  attribute: Condition2Attribute;
  nodes?: Conditions2Node[];
  recursive_filter?: boolean;
  parent?: string[];
  value?: string[];
  entity_type_name?: string;
  entity_type_id?: string;
  multiple_cardinality?: boolean;
  entity_reference?: string;
  attribute_label?: string;
  attribute_data_type?: string;
  reference_id?: string;
}

export interface Condition2EntityNoMultivalued {
  node_type: Conditions2LeafTypes.in | Conditions2LeafTypes.notin;
  attribute: Condition2Attribute;
  nodes?: Conditions2Node[];
  recursive_filter?: boolean;
  value?: string[];
  entity_type_name?: string;
  entity_type_id?: string;
  multiple_cardinality?: boolean;
  entity_reference?: string;
  attribute_label?: string;
  attribute_data_type?: string;
  is_tree_attribute?: boolean;
}

export enum RouteTooltipLabels {
  BARRA = '/Offer/barra',
  BRAND = '/Offer/item_offered/is_variant_of/brand',
  CATEGORIES = '/Offer/item_offered/is_variant_of/categories',
  CENTERS = '/Offer/price_specifications/centers',
  CLASSIFICATIONS = '/Offer/item_offered/pucharse_management_stock/classifications',
  CLASSIFICATIONS_ID = '/Offer/item_offered/pucharse_management_stock/classifications/identifier',
  CLASSIFICATIONS_VALUE = '/Offer/item_offered/pucharse_management_stock/classifications/value',
  CUSTOMISED_REQUEST = '/Offer/item_offered/customised_request',
  DEPARTMENT = '/Offer/department',
  GTIN = '/Offer/item_offered/gtin',
  DISCOUNT = '/Offer/price_specifications/discount_percentage',
  FAMILY = '/Offer/family',
  GOODTYPES = '/Offer/good_types',
  INFINITE_STOCK = 'Infinite Stock', // aun no está
  LUXURY_REFERENCE = 'Luxury Reference', // aun no está
  MAKER = 'Maker', // aun no está
  MANAGEMENT_TYPE = '/offer/item_offered/pucharse_management_stock/management_type',
  MARGIN = 'Margin', // aun no está
  PRICE = '/Offer/price_specifications/sale_price',
  PRICE_SPECIFICATION = '/Offer/price_specifications',
  PRODUCT = '/Offer/item_offered/is_variant_of/product_id',
  PROVIDER = '/Offer/item_offered/is_variant_of/provider',
  REFERENCE_TYPE = 'Reference Type', // aun no está
  SALE_REFERENCE = '/Offer/sales_reference',
  SERIE = 'Serie', // aun no está
  SITE = '/Offer/site',
  SIZECODE = '/Offer/size_code',
}

export interface ConditionsDTO {
  attribute: Condition2Attribute;
  is_recursive_filter: boolean;
  nodes?: Conditions2Node[];
  reference_id?: string;
  visual_path?: string;
  type: Conditions2LeafTypes;
  value?: any;
}
