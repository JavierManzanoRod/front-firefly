import { Directive, forwardRef } from '@angular/core';
import { FormControl, NG_VALIDATORS } from '@angular/forms';
import { Condition2SimpleDataTypes, Conditions2LeafTypes } from '../models/conditions2.model';
import { Conditions2UtilsService } from '../services/conditions2-utils.service';

function isEmptyArray(value: any) {
  if (Array.isArray(value)) {
    return value.some((val) => !val || (typeof val !== 'object' && typeof val === 'string' && val?.trim() === '') || val === null);
  } else {
    return false;
  }
}

function ValidatorFactory(utils: Conditions2UtilsService) {
  return (c: FormControl) => {
    if (!c.value) {
      return null;
    }
    const arrConditions = utils.flatMap(c.value);
    for (const condition of arrConditions) {
      if (
        utils.isCondition2Simple(condition) &&
        condition.attribute_data_type !== Condition2SimpleDataTypes.BOOLEAN &&
        condition.node_type !== Conditions2LeafTypes.IS_DEFINED &&
        condition.node_type !== Conditions2LeafTypes.IS_NOT_DEFINED &&
        condition.attribute_data_type !== Condition2SimpleDataTypes.VIRTUAL
      ) {
        const value = typeof condition.value === 'string' ? condition.value.trim() : condition.value;

        if (value == null || value === '' || isEmptyArray(value)) {
          return { isFilled: true };
        }
      }

      if (utils.isCondition2Simple(condition) && condition.node_type === 'gt') {
        // busco su par y hago la comprobacion
        const findLt = arrConditions.find((v) => v.node_type === 'lt' && v.attribute_id === condition.attribute_id) as any;
        if (findLt && condition.value >= findLt.value) {
          return {
            errorRange: true,
          };
        }
      }
    }
  };
}

@Directive({
  selector: '[ffConditions2FilledValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: forwardRef(() => Conditions2FilledValidatorDirective), multi: true }],
})
export class Conditions2FilledValidatorDirective {
  // eslint-disable-next-line @typescript-eslint/ban-types
  validator: Function;

  constructor(utils: Conditions2UtilsService) {
    this.validator = ValidatorFactory(utils);
  }

  validate(c: FormControl) {
    return this.validator(c);
  }
}
