import { Directive, forwardRef } from '@angular/core';
import { FormControl, NG_VALIDATORS } from '@angular/forms';
import { Conditions2, Conditions2LeafTypes, Conditions2Node, Conditions2NodeTypes } from '../models/conditions2.model';
import { Conditions2UtilsService } from '../services/conditions2-utils.service';

function isEmptyArray(value: any) {
  if (Array.isArray(value)) {
    return value.some((val) => val.trim() === '' || val === null);
  } else {
    return false;
  }
}

function ValidatorFactory(utils: Conditions2UtilsService) {
  return (c: FormControl) => {
    const checkGroup = (group: Conditions2Node) => {
      let hasError = false;

      group.nodes.forEach((node: Conditions2) => {
        if (utils.isConditions2Node(node)) {
          hasError = checkGroup(node);
        } else if (utils.isCondition2Simple(node) && node.attribute_data_type === 'DOUBLE' && node.node_type === Conditions2LeafTypes.gt) {
          const cond = group.nodes.find((item: Conditions2) => {
            return (
              utils.isCondition2Simple(item) &&
              item.attribute.identifier === node.attribute.identifier &&
              item.node_type === Conditions2LeafTypes.lt
            );
          });
          if (!cond) {
            hasError = true;
          }
        }
      });

      return hasError;
    };

    if (!c.value) {
      return null;
    }

    if (utils.isConditions2Node(c.value)) {
      return checkGroup(c.value) ? { doubleNotLT: true } : undefined;
    }

    return checkGroup({
      node_type: Conditions2NodeTypes.or,
      nodes: [c.value],
    })
      ? { doubleNotLT: true }
      : undefined;
  };
}

@Directive({
  selector: '[ffConditions2DoubleValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: forwardRef(() => Conditions2DoubleValidatorDirective), multi: true }],
})
export class Conditions2DoubleValidatorDirective {
  // eslint-disable-next-line @typescript-eslint/ban-types
  validator: Function;

  constructor(utils: Conditions2UtilsService) {
    this.validator = ValidatorFactory(utils);
  }

  validate(c: FormControl) {
    return this.validator(c);
  }
}
