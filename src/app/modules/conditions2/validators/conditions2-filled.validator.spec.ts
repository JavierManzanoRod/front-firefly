import { FormControl } from '@angular/forms';
import { Condition2Simple, Conditions2, Conditions2LeafTypes } from '../models/conditions2.model';
import { Conditions2UtilsService } from '../services/conditions2-utils.service';
import { Conditions2FilledValidatorDirective } from './conditions2-filled.validator';

describe('Conditions2FilledValidatorDirective', () => {
  it('if no value then validate returns truthy', () => {
    const utils = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const directiveToTest = new Conditions2FilledValidatorDirective(utils);
    const data: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id1',
      attribute_data_type: 'STRING',
    } as Condition2Simple;
    const control = { value: data } as FormControl;
    expect(directiveToTest.validate(control)).toBeTruthy();
  });

  it('if control is null as any no validates and returns ok', () => {
    const utils = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const directiveToTest = new Conditions2FilledValidatorDirective(utils);
    const control = { value: null as any } as FormControl;
    expect(directiveToTest.validate(control)).toBeNull();
  });

  it('if control is empty value no validates and returns ok', () => {
    const utils = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const directiveToTest = new Conditions2FilledValidatorDirective(utils);
    const data: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id2',
      attribute_data_type: 'STRING',
      value: '',
    } as Condition2Simple;
    const control = { value: data } as FormControl;
    expect(directiveToTest.validate(control)).toBeTruthy();
  });

  it('if control has right value with trim validate and no return nothing', () => {
    const utils = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const directiveToTest = new Conditions2FilledValidatorDirective(utils);
    const data: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id2',
      attribute_data_type: 'STRING',
      value: '   value   ',
    } as Condition2Simple;
    const control = { value: data } as FormControl;
    expect(directiveToTest.validate(control)).toBeFalsy();
  });

  it('if control has right value with trim no validate and returns ok', () => {
    const utils = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const directiveToTest = new Conditions2FilledValidatorDirective(utils);
    const data: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id2',
      attribute_data_type: 'STRING',
      value: '   ',
    } as Condition2Simple;
    const control = { value: data } as FormControl;
    expect(directiveToTest.validate(control)).toBeTruthy();
  });

  it('if control is empty multivalue no validates and returns ok', () => {
    const utils = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const directiveToTest = new Conditions2FilledValidatorDirective(utils);
    const data: Conditions2 = {
      node_type: Conditions2LeafTypes.eq,
      attribute_id: 'id2',
      attribute_data_type: 'STRING',
      value: ['value1', ''],
    } as Condition2Simple;
    const control = { value: data } as FormControl;
    expect(directiveToTest.validate(control)).toBeTruthy();
  });

  it('if node_type is IS_DEFINED no validates and returns ok', () => {
    const utils = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const directiveToTest = new Conditions2FilledValidatorDirective(utils);
    const data: Conditions2 = {
      node_type: Conditions2LeafTypes.IS_DEFINED,
      attribute_id: 'id1',
      attribute_data_type: 'STRING',
    } as Condition2Simple;
    const control = { value: data } as FormControl;
    expect(directiveToTest.validate(control)).toBeFalsy();
  });

  it('if node_type is IS_NOT_DEFINED no validates and returns ok', () => {
    const utils = new Conditions2UtilsService(null as any, null as any, null as any, null as any, null as any);
    const directiveToTest = new Conditions2FilledValidatorDirective(utils);
    const data: Conditions2 = {
      node_type: Conditions2LeafTypes.IS_NOT_DEFINED,
      attribute_id: 'id1',
      attribute_data_type: 'STRING',
    } as Condition2Simple;
    const control = { value: data } as FormControl;
    expect(directiveToTest.validate(control)).toBeFalsy();
  });
});
