import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormControl } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SearchProvider } from 'src/app/shared/components/list-search/providers/search-provider';
import { SearchFormFieldsComponent } from './search-form-fields.component';

describe('SearchComponent', () => {
  let component: SearchFormFieldsComponent;
  let fixture: ComponentFixture<SearchFormFieldsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchFormFieldsComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        FormBuilder,
        {
          provide: SearchProvider,
          useFactory: () => ({
            data: {
              selector: '1',
            },
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormFieldsComponent);
    component = fixture.componentInstance;
    component.arrayConfig = [
      {
        key: 'selector',
        type: 'select',
        label: 'Selecciona',
        value: '2',
        selectOptions: [
          {
            value: '1',
            label: 'Primero',
            filter: ['texto'],
          },
          {
            value: '2',
            label: 'Segundo',
          },
        ],
      },
      {
        key: 'texto',
        type: 'text',
        label: 'Texto Bonito',
      },
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('emit event submit', () => {
    const s = spyOn(component.search, 'emit');
    component.submit();
    expect(s).toHaveBeenCalled();
  });

  it('emit event clear', () => {
    const s = spyOn(component.clean, 'emit');
    component.form.addControl('test', new FormControl('valor'));
    component.clearData();
    expect(component.form.value.test).toBeFalsy();
    expect(s).toHaveBeenCalled();
  });
});
