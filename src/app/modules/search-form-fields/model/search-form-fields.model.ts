import { ValidatorFn } from '@angular/forms';
import { ChipsControlSelectConfig } from 'src/app/modules/chips-control/components/chips.control.model';

export interface SearchFormFields {
  type: TypeForm;
  configChips?: ChipsControlSelectConfig;
  key: string;
  value?: any;
  disabled?: boolean;
  class?: string;
  label: string;
  placeholder?: string;
  selectOptions?: SearchFormFieldsSelectOption[];
  error?: SearchFormFieldsError[];
  validators?: ValidatorFn[];
}

export interface SearchFormFieldsError {
  type: string;
  message: string;
  params?: string;
}

export interface SearchFormFieldsSelectOption {
  label?: string;
  value: any;
  filter?: string[];
}

export type TypeForm = 'select' | 'text' | 'chips' | 'date' | 'checkbox' | 'time' | 'search';
