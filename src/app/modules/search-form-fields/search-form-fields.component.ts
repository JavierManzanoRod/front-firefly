import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { SearchProvider } from '../../shared/components/list-search/providers/search-provider';
import { SearchFormFields, SearchFormFieldsSelectOption } from './model/search-form-fields.model';

@Component({
  selector: 'ff-search-form-fields',
  templateUrl: './search-form-fields.component.html',
  styleUrls: ['./search-form-fields.component.scss'],
})
export class SearchFormFieldsComponent implements OnInit {
  @Input() arrayConfig: SearchFormFields[] = [];
  @Input() classRoot = 'container row';
  @Input() showButton = true;
  @Input() keepFilter = true;
  @Input() isReadOnlyOnsubmit = false;

  @Output() search: EventEmitter<any> = new EventEmitter<any>();
  @Output() clean: EventEmitter<any> = new EventEmitter<any>();
  @Output() fieldsChange: EventEmitter<any> = new EventEmitter<any>();
  form: FormGroup;

  filter: string[] = [];
  clear = false;

  constructor(protected fb: FormBuilder, private provider: SearchProvider) {
    this.form = new FormGroup({});
  }

  ngOnInit() {
    this.arrayConfig?.forEach((config) => {
      if (config.type === 'select' && config.value) {
        this.changeSelect(config.value, config.selectOptions || []);
      }
      this.form.addControl(
        config.key,
        new FormControl(
          {
            value: config.value,
            disabled: config.disabled,
          },
          config.validators
        )
      );
    });

    if (this.provider.data && this.keepFilter) {
      const select = this.arrayConfig.find((e) => e.type === 'select' && e.selectOptions?.filter((o) => o.filter));
      if (select) {
        this.changeSelect(this.provider.data[select.key], select.selectOptions || []);
      }
      this.form.patchValue(this.provider.data);
    }

    this.form.valueChanges.subscribe((data) => {
      this.clear = false;
      this.fieldsChange.emit(data);
    });
  }

  submit() {
    this.form.markAllAsTouched();
    if (this.clear) {
      this.form.reset();
    }

    if (this.form.valid) {
      this.provider.data = this.form.value;
      this.clear = !this.clear;
      return this.search.emit(this.form.value);
    }
  }

  clearData() {
    this.form.reset();
    this.clean.emit(null);
  }

  changeSelect(value: string, options: SearchFormFieldsSelectOption[]) {
    const filter = options.find((e) => e.value === value)?.filter;
    const includeFilter = options.find((e) => e.filter);

    if (filter) {
      this.filter = filter;
    } else {
      this.filter = includeFilter ? [] : this.filter;
    }
  }
}
