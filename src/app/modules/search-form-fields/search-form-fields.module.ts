import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ChipsControlModule } from '../chips-control/chips-control.module';
import { FormErrorModule } from '../form-error/form-error.module';
import { SearchFormFieldsComponent } from './search-form-fields.component';

@NgModule({
  declarations: [SearchFormFieldsComponent],
  providers: [],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    TranslateModule,
    NgSelectModule,
    BsDatepickerModule,
    TimepickerModule,
    FormErrorModule,
    ChipsControlModule,
  ],
  exports: [SearchFormFieldsComponent],
})
export class UiEntityTypesSelectorsModule {}
