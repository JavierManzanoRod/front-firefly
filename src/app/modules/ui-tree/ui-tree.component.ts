import { ChangeDetectionStrategy, Component, EventEmitter, Host, Input, OnChanges, Output } from '@angular/core';
import { MayHaveLabel } from '@model/base-api.model';
import { UITree } from './models/ui-tree.model';
import { UiTreeStateService } from './services/ui-tree-state.service';

@Component({
  selector: 'ff-ui-tree',
  template: `<ff-ui-tree-ref [data]="data" [draggable]="draggable" (clickedToResolve)="handleOnResolve()"></ff-ui-tree-ref> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [UiTreeStateService],
})
export class UiTreeComponent implements OnChanges {
  @Input() data!: UITree;
  @Input() draggable = false;
  @Output() clicked: EventEmitter<MayHaveLabel> = new EventEmitter<MayHaveLabel>();
  @Output() resolve: EventEmitter<any> = new EventEmitter<any>();

  isResolved = false;

  constructor(@Host() public uiTreeState: UiTreeStateService) {}

  ngOnChanges() {
    if (!this.isResolved && this.data.childrens) {
      this.data.childrens.forEach((child) => {
        child.expanded = true;
      });
      this.isResolved = true;
    }

    this.uiTreeState.clicks$.subscribe((clickeElement) => {
      this.clicked.emit(clickeElement);
    });
  }

  handleOnResolve() {
    if ((this.data.childrens || []).length === 0) {
      this.resolve.emit();
    }
  }
}
