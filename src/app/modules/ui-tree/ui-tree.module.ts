import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DndModule } from 'ngx-drag-drop';
import { UiTreeRefComponent } from './components/ui-tree-ref.component';
import { UiTreeComponent } from './ui-tree.component';

@NgModule({
  declarations: [UiTreeRefComponent, UiTreeComponent],
  imports: [CommonModule, DndModule],
  exports: [UiTreeComponent, UiTreeRefComponent],
})
export class UiTreeModule {}
