import { CommonModule } from '@angular/common';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { DndModule } from 'ngx-drag-drop';
import { UiTreeComponent } from './ui-tree.component';
import { UiTreeRefComponent } from './components/ui-tree-ref.component';

export default {
  component: UiTreeComponent,
  decorators: [
    moduleMetadata({
      declarations: [UiTreeComponent, UiTreeRefComponent],
      imports: [CommonModule, DndModule],
    }),
  ],
  title: 'UI Tree / UI Tree',
  args: {
    data: {
      childrens: [
        {
          value: {
            name: 'Hijo',
          },
          expanded: true,
        },
        {
          value: {
            name: 'Hijo2',
          },
          expanded: true,
        },
        {
          value: {
            name: 'Hijo3',
          },
          expanded: true,
        },
      ],
      value: {
        name: 'Padre',
      },
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Arbol de carpetas y ficheros',
      },
    },
  },
} as Meta;

const Template: Story<UiTreeComponent> = (args) => ({
  props: {
    ...args,
  },
});

export const Default = Template.bind({});
Default.args = {};

export const Draggable = Template.bind({});
Draggable.args = {
  draggable: true,
};
