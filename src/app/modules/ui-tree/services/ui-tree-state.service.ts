import { Injectable } from '@angular/core';
import { MayHaveLabel } from '@model/base-api.model';
import { Observable, Subject } from 'rxjs';
import { UITreeEvent } from '../models/ui-tree.model';

@Injectable({
  providedIn: 'root',
})
export class UiTreeStateService {
  clickedElement = new Subject<UITreeEvent>();
  clicks$: Observable<MayHaveLabel>;

  constructor() {
    this.clicks$ = this.clickedElement.asObservable();
  }
}
