import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, SkipSelf } from '@angular/core';
import { UITree } from '../models/ui-tree.model';
import { UiTreeStateService } from '../services/ui-tree-state.service';

@Component({
  selector: 'ff-ui-tree-ref',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './ui-tree-ref.component.html',
  styleUrls: ['./ui-tree-ref.component.scss'],
})
export class UiTreeRefComponent {
  @Input() isRoot = true;
  @Input() draggable = false;
  @Input() data!: UITree;
  @Output() clickedToResolve: EventEmitter<string> = new EventEmitter<string>();

  constructor(@SkipSelf() private stateTree: UiTreeStateService) {}

  get isLeaf(): boolean {
    return (this.data.childrens || []).length === 0;
  }

  toogle() {
    if (this.isRoot && this.isLeaf) {
      this.clickedToResolve.emit();
      this.stateTree.clickedElement.next({
        eventName: 'init',
        data: this.data.value,
      });
    }

    if (!this.isLeaf) {
      this.stateTree.clickedElement.next({
        eventName: this.data.childrens && this.data.childrens[0].expanded ? 'contract' : 'expand',
        data: this.data.value,
      });
      this.data.childrens?.forEach((child) => {
        child.expanded = !child.expanded;
      });
    } else {
      if (!this.isRoot) {
        this.stateTree.clickedElement.next({
          eventName: 'click',
          data: this.data.value,
        });
      }
    }
  }
}
