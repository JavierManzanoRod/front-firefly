import { MayHaveLabel } from '@model/base-api.model';

interface UITree1<T> {
  value: T;
  childrens?: UITree1<T>[];
  expanded?: boolean;
}

export interface UITreeEvent {
  eventName: 'click' | 'expand' | 'contract' | 'init';
  data: MayHaveLabel;
}

export type UITree = UITree1<MayHaveLabel>;
