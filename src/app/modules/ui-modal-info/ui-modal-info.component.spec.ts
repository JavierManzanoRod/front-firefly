import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { UiModalInfoComponent } from './ui-modal-info.component';

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}
  public get(key: any): any {
    of(key);
  }
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

describe('UiModalInfoComponent', () => {
  let fixture: ComponentFixture<UiModalInfoComponent>;
  let component: UiModalInfoComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UiModalInfoComponent, TranslatePipeMock],
      providers: [{ provide: TranslateService, useClass: TranslateServiceStub }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiModalInfoComponent);
    fixture.detectChanges();
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
