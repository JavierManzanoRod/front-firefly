import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ff-ui-modal-info',
  templateUrl: './ui-modal-info.component.html',
  styleUrls: ['./ui-modal-info.component.scss'],
})
export class UiModalInfoComponent {
  @Input() title = '';
  @Input() showClose = false;
  @Input() variantData: string[] = [];
  @Output() isModalClosed = new EventEmitter<boolean>();
  constructor() {}

  closeModal(event: boolean) {
    if (event) {
      this.isModalClosed.emit(true);
    }
  }
}
