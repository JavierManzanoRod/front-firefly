import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { LoadingModule } from 'src/app/modules/loading/loading.module';
import { CommonModalModule } from '../common-modal/common-modal.module';
import { ConditionsBasicComponent } from './components/conditions-basic/conditions-basic.component';
@NgModule({
  declarations: [ConditionsBasicComponent],
  imports: [CommonModule, CommonModalModule, ReactiveFormsModule, FormsModule, LoadingModule, TranslateModule, ChipsControlModule],
  exports: [ConditionsBasicComponent],
})
export class ConditionsBasicModule {}
