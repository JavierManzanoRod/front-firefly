import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GenericApiRequest } from '@model/base-api.model';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { Conditions2 } from 'src/app/modules/conditions2/models/conditions2.model';
import { CategoryRuleService } from 'src/app/rule-engine/category-rule/services/category-rule.service';
import { ConditionsBasic, ConditionsBasicCriteria } from '../../models/conditions-basic.model';

@Component({
  selector: 'ff-conditions-basic',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './conditions-basic.component.html',
  styleUrls: ['./conditions-basic.component.scss'],
})
export class ConditionsBasicComponent implements OnInit, OnChanges {
  @Output()
  changed: EventEmitter<ConditionsBasic | null> = new EventEmitter<ConditionsBasic | null>();
  @Input() data!: Conditions2;
  // @Input() entityTypes: EntityType[];
  flag = false;

  form: FormGroup;
  configSelectFields: Record<ConditionsBasicCriteria, ChipsControlSelectConfig>;

  canSetFormOnChanges = true;

  constructor(private fb: FormBuilder, private categoryRuleService: CategoryRuleService) {
    const formConfig: Record<ConditionsBasicCriteria, any> = {
      categoryRules: [{ value: [], disabled: true }],
      departments: [{ value: [], disabled: true }],
      // departmentFamilies: [{ value: [], disabled: true }],
      references: [{ value: [], disabled: true }],
      brands: [{ value: [], disabled: true }],
      centers: [{ value: [], disabled: true }],
      // categories: [{ value: [], disabled: true }],
      // suppliers: [{ value: [], disabled: true }],
      // eans: [{ value: [], disabled: true }],
      productsId: [{ value: [], disabled: true }],
    };

    this.form = this.fb.group(formConfig);

    this.configSelectFields = {
      categoryRules: {
        label: 'CONDITIONS_BASIC.CATEGORY_RULES.TITLE',
        modalType: ModalChipsTypes.select,
        modalConfig: {
          title: 'CONDITIONS_BASIC.CATEGORY_RULES.LABEL',
          multiple: true,
          searchFn: (x: GenericApiRequest) => {
            return this.categoryRuleService.list(x);
          },
        },
      },
      departments: {
        label: 'CONDITIONS_BASIC.DEPARTMENT.TITLE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'CONDITIONS_BASIC.DEPARTMENT.LABEL',
        },
      },
      // departmentFamilies: {
      //   label: 'CONDITIONS_BASIC.FAMILY.TITLE',
      //   modalType: 'select',
      //   modalConfig: {
      //     title: 'CONDITIONS_BASIC.FAMILY.LABEL',
      //     multiple: true,
      //     searchFn: (x: GenericApiRequest) => {
      //       const name = x.name;
      //       const content: MayHaveLabel[] = [{ id: '1', name }, { id: '2', name: name + ' 2' }];
      //       return of({
      //         content,
      //         page: { page_number: 0, page_size: 5, total_elements: 5, total_pages: 8, }
      //       });
      //     }
      //   }
      // },
      references: {
        label: 'CONDITIONS_BASIC.REFERENCES.TITLE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'CONDITIONS_BASIC.REFERENCES.LABEL',
          canImportCSV: true,
        },
      },
      brands: {
        label: 'CONDITIONS_BASIC.BRAND.TITLE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'CONDITIONS_BASIC.BRAND.LABEL',
        },
      },
      centers: {
        label: 'CONDITIONS_BASIC.CENTERS.TITLE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'CONDITIONS_BASIC.CENTERS.LABEL',
        },
      },
      productsId: {
        label: 'CONDITIONS_BASIC.PRODUCTS.TITLE',
        modalType: ModalChipsTypes.free,
        modalConfig: {
          title: 'CONDITIONS_BASIC.PRODUCTS.LABEL',
          canImportCSV: true,
        },
      },
      // categories: {
      //   label: 'CONDITIONS_BASIC.CATEGORY.TITLE',
      //   modalType: 'select',
      //   modalConfig: {
      //     title: 'CONDITIONS_BASIC.CATEGORY.LABEL',
      //     multiple: true,
      //     searchFn: (x: GenericApiRequest) => {
      //       const name = x.name;
      //       const content: MayHaveLabel[] = [{ id: '1', name }, { id: '2', name: name + ' 2' }];
      //       return of({
      //         content,
      //         page: { page_number: 0, page_size: 5, total_elements: 5, total_pages: 8, }
      //       });
      //     }
      //   }
      // },
      // suppliers: {
      //   label: 'CONDITIONS_BASIC.SUPPLIER.TITLE',
      //   modalType: 'select',
      //   modalConfig: {
      //     title: 'CONDITIONS_BASIC.SUPPLIER.LABEL',
      //     multiple: true,
      //     searchFn: (x: GenericApiRequest) => {
      //       const name = x.name;
      //       const content: MayHaveLabel[] = [{ id: '1', name }, { id: '2', name: name + ' 2' }];
      //       return of({
      //         content,
      //         page: { page_number: 0, page_size: 5, total_elements: 5, total_pages: 8, }
      //       });
      //     }
      //   }
      // },
      // eans: {
      //   label: 'CONDITIONS_BASIC.EANS.TITLE',
      //   modalType: 'free',
      //   modalConfig: {
      //     title: 'CONDITIONS_BASIC.EANS.LABEL',
      //   }
      // },
    };
  }

  get isEmpty() {
    return Object.values(this.form.controls).every((val) => {
      const valueField = val.value;
      return valueField == null || valueField.length === 0;
    });
  }

  ngOnInit() {
    if (this.data) {
      this.form.patchValue(this.data, { emitEvent: false });
    }

    this.form.valueChanges.subscribe((data) => {
      this.canSetFormOnChanges = false;
      this.changed.emit(this.isEmpty ? null : JSON.parse(JSON.stringify(data)));
      this.flag = true;
      setTimeout(() => {
        this.canSetFormOnChanges = true;
      }, 100);
    });
  }

  ngOnChanges() {
    if (this.data && this.canSetFormOnChanges) {
      this.form.patchValue(this.data, { emitEvent: false });
    }
  }

  submit() {
    console.log('value of form', this.form.value);
  }
}
