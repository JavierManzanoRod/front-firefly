import { FormBuilder } from '@angular/forms';
import { ConditionsBasicComponent } from './conditions-basic.component';

describe('ConditionsBasicComponent', () => {
  it('it should create ', () => {
    const component = new ConditionsBasicComponent(new FormBuilder(), null as any);
    expect(component).toBeDefined();
  });
});
