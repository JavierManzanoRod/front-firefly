import { MayHaveIdName } from '@model/base-api.model';

export enum ConditionsBasicCriteria {
  categoryRules = 'categoryRules',
  departments = 'departments',
  // departmentFamilies = 'departmentFamilies',
  references = 'references',
  brands = 'brands',
  centers = 'centers',
  productsId = 'productsId',
}

export type ConditionsBasic = Partial<Record<ConditionsBasicCriteria, MayHaveIdName[] | string[]>>;
