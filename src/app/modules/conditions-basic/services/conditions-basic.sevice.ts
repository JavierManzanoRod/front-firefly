/* eslint-disable @typescript-eslint/no-unused-vars */
import { Inject, Injectable } from '@angular/core';
import { EntityType } from '@core/models/entity-type.model';
import { MayHaveIdName } from '@model/base-api.model';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_ATTRIBUTE_PATHS, FF_GROUPED_ATTRIBUTES, FF_LIMIT_SALES_CONDITIONS_BASIC_IDS } from 'src/app/configuration/tokens/configuracion';
import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import {
  Condition2Reference,
  Condition2SimpleDataTypes,
  Condition2SimpleString,
  Conditions2,
  Conditions2LeafTypes,
  Conditions2NodeTypes,
  Conditions2ReferenceNodeType,
} from 'src/app/modules/conditions2/models/conditions2.model';
import { Conditions2BasicService } from 'src/app/modules/conditions2/services/conditions2-basic.service';
import { Conditions2UtilsService } from 'src/app/modules/conditions2/services/conditions2-utils.service';
import { ConditionsBasic, ConditionsBasicCriteria } from '../models/conditions-basic.model';

@Injectable({
  providedIn: 'root',
})
export class ConditionsBasicUtilsService {
  private readonly criteriasSorted: ConditionsBasicCriteria[] = [
    ConditionsBasicCriteria.departments,
    ConditionsBasicCriteria.references,
    ConditionsBasicCriteria.brands,
    ConditionsBasicCriteria.centers,
    ConditionsBasicCriteria.productsId,
  ];
  private readonly assortmentId = 'assortment';
  constructor(
    private conditions2UtilsService: Conditions2UtilsService,
    private controlPanelConditionsSrv: Conditions2BasicService,
    @Inject(FF_LIMIT_SALES_CONDITIONS_BASIC_IDS) private criteriaAttributesIds: string[],
    @Inject(FF_ATTRIBUTE_PATHS) private attributePath: { [key in ControlPanelConditions]: string },
    @Inject(FF_GROUPED_ATTRIBUTES) private groupedAttributes: ConfigurationDefinition['groupedAttributes']
  ) {}

  // haveChildsNodeTypeAnd(condition: Conditions2): boolean {
  //   if (condition.node_type === Conditions2NodeTypes.and || condition.node_type === Conditions2NodeTypes.or) {
  //     return condition.nodes.some(element => this.haveAdvancedNodes(element));
  //   }

  //   return false;
  // }

  // private haveAdvancedNodes(condition: Conditions2): boolean {
  //   if (condition.node_type === Conditions2NodeTypes.and) {
  //     return true;
  //   }

  //   if (condition.node_type === Conditions2NodeTypes.or) {
  //     return condition.nodes.some(element => this.haveAdvancedNodes(element));
  //   }

  //   return false;
  // }

  // isBasicCondition(condition: Conditions2 | undefined): boolean {
  //   console.log('entrando', condition);
  //   if (!condition) {
  //     return true;
  //   }

  //   if (this.haveMoreThan3sons(condition)) {
  //     return false;
  //   }

  //   if (this.haveChildsNodeTypeAnd(condition)) {
  //     return false;
  //   }

  //   if (!this.isCondition2Simple(condition) && this.haveAdvancedGroupedConditions(condition))
  //   {
  //     return false;
  //   }

  //   const simples: Condition2Simple[] = this.conditions2UtilsService.filterSimples(condition);
  //   return simples.every(
  //     (simple) =>
  //       this.getAttributePathArrayAsString().indexOf(this.conditions2UtilsService.getFullIdAsString(simple.attribute)) > -1 &&
  //       ( simple.node_type === Conditions2LeafTypes.eq ||  simple.node_type === Conditions2LeafTypes.notin )
  //   );
  // }

  // parseAsBasic(condition: Conditions2): ConditionsBasic | undefined {
  //   if (!this.isBasicCondition(condition)) {
  //     throw Error('condition provided is not Basic');
  //   }

  //   const parseSimple: (xs: Condition2Simple[]) => string[] | undefined = (
  //     conditionSimple: Condition2Simple[]
  //   ) => {
  //     if (!conditionSimple) {
  //       return undefined;
  //     } else {
  //       return conditionSimple.map((e) => e.value as string);
  //     }
  //   };

  //   let simples: Condition2Simple[] = [];
  //   if (condition) {
  //     simples = this.conditions2UtilsService.filterSimples(condition);
  //   }

  //   return {
  //     departments: parseSimple(
  //       simples.filter(({ attribute_id }) => attribute_id === this.criteriaAttributesIds[0])
  //     ),
  //     references: parseSimple(
  //       simples.filter(({ attribute_id }) => attribute_id === this.criteriaAttributesIds[1])
  //     ),
  //     brands: parseSimple(
  //       simples.filter(({ attribute_id }) => attribute_id === this.criteriaAttributesIds[2])
  //     ),
  //     centers: parseSimple(
  //       simples.filter(({ attribute_id }) => attribute_id === this.criteriaAttributesIds[3])
  //     ),
  //     productsId: parseSimple(
  //       simples.filter(({ attribute_id }) => attribute_id === this.criteriaAttributesIds[4])
  //     ),
  //     categoryRules: this.parseCategoryRules(condition),
  //   } as ConditionsBasic;
  // }

  toConditions(conditionBasic: { [key: string]: any }, assortment: EntityType) {
    if (!conditionBasic) {
      return null;
    }
  }

  toConditions2(conditionBasic: ConditionsBasic | undefined, assortment: EntityType): Conditions2 | null {
    if (!conditionBasic) {
      return null;
    }

    const data = [
      ...this.toConditions2Criteria(ConditionsBasicCriteria.departments, conditionBasic, assortment),
      ...this.toConditions2Criteria(ConditionsBasicCriteria.references, conditionBasic, assortment),
      ...this.toConditions2Criteria(ConditionsBasicCriteria.brands, conditionBasic, assortment),
      ...this.toConditions2Criteria(ConditionsBasicCriteria.centers, conditionBasic, assortment),
      ...this.toConditions2Criteria(ConditionsBasicCriteria.productsId, conditionBasic, assortment),
    ];

    const nodes = [...data, ...this.toConditions2CategoryRules(conditionBasic)];

    return nodes.length
      ? {
          node_type: Conditions2NodeTypes.or,
          nodes,
        }
      : null;
  }

  // private haveMoreThan3sons(condition: Conditions2): boolean {
  //   if (condition.node_type === Conditions2NodeTypes.and || condition.node_type === Conditions2NodeTypes.or) {
  //     return condition.nodes.some(element => {
  //       if (element.node_type === Conditions2NodeTypes.or) {
  //         return element.nodes.some( elem => {
  //           if (elem.node_type === Conditions2NodeTypes.or) {
  //             return elem.nodes.some( e => {
  //               if (e.node_type === Conditions2NodeTypes.or) {
  //                 return true;
  //               }
  //               else {
  //                 return false;
  //               }
  //             });
  //           }
  //         });
  //       }
  //     });
  //   }
  //   return false;
  // }

  // private isCondition2Simple(condition: Conditions2): condition is Condition2Simple {
  //   return !this.conditions2UtilsService.isConditions2Node(condition) && !this.conditions2UtilsService.isConditions2Childs(condition);
  // }

  // private haveAdvancedGroupedConditions(condition: Conditions2): boolean {
  //   let eqGroupedCount = 0;
  //   let notinGroupedCount = 0;

  //   if (condition.node_type === Conditions2NodeTypes.or) {

  //     const simpleConditions = this.conditions2UtilsService.flatMap(condition);
  //     const allNotin = simpleConditions.every(element => element.node_type === Conditions2LeafTypes.notin);
  //     const allEq = simpleConditions.every(element => element.node_type === Conditions2LeafTypes.eq);
  //     const allGrouped = simpleConditions.every(element => {
  //       const ele = element as Condition2SimpleString | Condition2Reference;
  //       if (ele.node_type === Conditions2ReferenceNodeType.reference) {
  //         return false;
  //       } else {
  //         const path = this.conditions2UtilsService.getFullIdAsString(ele.attribute);
  //         return this.groupedAttributes ? Object.values(this.groupedAttributes).indexOf(path) > -1 : true;
  //       }
  //     });

  //     if ((!allNotin || !allEq) && !allGrouped) {
  //       return true;
  //     }
  //   }

  //   if (condition.node_type === Conditions2NodeTypes.and || condition.node_type === Conditions2NodeTypes.or) {
  //     condition.nodes.forEach(element => {
  //       if (element.node_type === Conditions2LeafTypes.eq) {
  //         const path = this.conditions2UtilsService.getFullIdAsString(element.attribute);
  //         if (this.groupedAttributes) {
  //           if (Object.values(this.groupedAttributes).includes(path)) {
  //             eqGroupedCount += 1;
  //           }
  //         }
  //       }
  //       if (element.node_type === Conditions2LeafTypes.notin) {
  //         if (GROUPED_ATTRIBUTES.includes(element.attribute_label.toLowerCase())) {
  //           const path = this.conditions2UtilsService.getFullIdAsString(element.attribute);
  //           if (this.groupedAttributes) {
  //             if (Object.values(this.groupedAttributes).includes(path)) {
  //               notinGroupedCount += 1;
  //             }
  //           }
  //         }
  //       }
  //     });
  //   }

  //   if (condition.node_type === Conditions2NodeTypes.and) {
  //     return ((eqGroupedCount > 1 && notinGroupedCount === 0) ||
  //           (eqGroupedCount > 1 && notinGroupedCount > 0) ||
  //           (eqGroupedCount > 0 && notinGroupedCount > 1) )
  //         || (notinGroupedCount > 1 && eqGroupedCount === 0);
  //   } else if (condition.node_type === Conditions2NodeTypes.or) {
  //     return eqGroupedCount >= 1 && notinGroupedCount >= 1;
  //   }

  //   return true;
  // }

  // private parseCategoryRules(condition: Conditions2): MayHaveIdName[] | null {
  //   let data: MayHaveIdName[] | null = null;
  //   const arrCondReferences: Condition2Reference[] = condition
  //     ? this.conditions2UtilsService.filterReferences(condition)
  //     : [];
  //   if (arrCondReferences.length) {
  //     data = arrCondReferences.map((cond) => {
  //       return {
  //         id: cond.reference_id,
  //         name: cond.reference_name || cond.name,
  //       };
  //     });
  //   }
  //   return data;
  // }

  private toConditions2CategoryRules(conditionBasic: ConditionsBasic): Conditions2[] {
    let result: Conditions2[] = [];
    const accumulator: Condition2Reference[] = [];
    if (conditionBasic.categoryRules) {
      for (const categoryRule of conditionBasic.categoryRules) {
        accumulator.push({
          node_type: Conditions2ReferenceNodeType.reference,
          reference_id: (categoryRule as MayHaveIdName).id || '',
          name: (categoryRule as MayHaveIdName).name || '',
        });
      }
    }
    if (accumulator.length > 1) {
      result = [
        {
          nodes: accumulator,
          node_type: Conditions2NodeTypes.or,
        },
      ];
    } else if (accumulator.length === 1) {
      result = accumulator;
    }

    return result;
  }

  private toConditions2Criteria(
    criteriaName: ConditionsBasicCriteria,
    conditionBasic: ConditionsBasic,
    assortment: EntityType
  ): Conditions2[] {
    const values = conditionBasic[criteriaName] as string[];
    if (values && values.length) {
      const index = this.criteriasSorted.indexOf(criteriaName);
      const criteriaId = this.criteriaAttributesIds[index];
      const attAssortment = assortment.attributes.find((e) => e.id === criteriaId);
      const nodes = values.map((val) => {
        return {
          attribute_data_type: Condition2SimpleDataTypes.STRING,
          attribute_id: criteriaId,
          attribute_label: attAssortment ? attAssortment.label : undefined,
          entity_type_name: assortment.name,
          multiple_cardinality: attAssortment ? attAssortment.multiple_cardinality : undefined,
          entity_type_id: this.assortmentId,
          node_type: Conditions2LeafTypes.eq,
          value: val,
        };
      }) as Condition2SimpleString[];

      return nodes;
    } else {
      return [];
    }
  }

  private getAttributePathArrayAsString(): string[] {
    const attributePath: string[] = [];
    const keys: string[] = Object.keys(this.attributePath);

    keys.forEach((key) => {
      attributePath.push(this.attributePath[key as ControlPanelConditions]);
    });

    return attributePath;
  }
}

function clone<T>(x: T): T {
  return JSON.parse(JSON.stringify(x)) as T;
}

function isNumbers(array: number[] | string[]): array is number[] {
  return typeof array[0] === 'number';
}
