import { StorybookTranslateModule } from '.storybook/StorybookTranslate.module';
import { CommonModule } from '@angular/common';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { of } from 'rxjs';
import { UiTreeListComponent } from './ui-tree-list.component';
import { UiTreeListModule } from './ui-tree-list.module';

export default {
  component: UiTreeListComponent,
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [CommonModule, UiTreeListModule, StorybookTranslateModule],
    }),
  ],
  title: 'UI Tree / UI Tree List',
  args: {
    items: [
      {
        value: {
          label: 'Padre 1',
          childrens: [
            {
              value: {
                name: 'Hijo',
              },
              expanded: true,
            },
            {
              value: {
                name: 'Hijo2',
              },
              expanded: true,
            },
            {
              value: {
                name: 'Hijo3',
              },
              expanded: true,
            },
          ],
        },
        loadChildrens: (v: any) => of(v.childrens),
      },
      {
        value: {
          label: 'Padre 2',
          childrens: [
            {
              value: {
                name: 'Hijo',
              },
              expanded: true,
            },
          ],
        },
        loadChildrens: (v: any) => of(v.childrens),
      },
    ],
  },
} as Meta;

const Template: Story<UiTreeListComponent<any>> = (args) => ({
  props: {
    ...args,
  },
});

export const Default = Template.bind({});
Default.args = {};

export const HideSearch = Template.bind({});
HideSearch.args = {
  hideSearch: true,
};

export const Loading = Template.bind({});
Loading.args = {
  loading: true,
  items: [],
};
