import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { DndModule } from 'ngx-drag-drop';
import { UiTreeRefListComponent } from './components/ui-tree-ref-list.component';
import { UiTreeListComponent } from './ui-tree-list.component';
import { UiTreeListService } from './ui-tree-list.service';

@NgModule({
  declarations: [UiTreeRefListComponent, UiTreeListComponent],
  imports: [CommonModule, FormsModule, TooltipModule, DndModule, TranslateModule],
  providers: [UiTreeListService],
  exports: [UiTreeListComponent, UiTreeRefListComponent],
})
export class UiTreeListModule {}
