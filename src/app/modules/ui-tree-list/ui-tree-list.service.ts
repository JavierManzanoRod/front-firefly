import { Injectable } from '@angular/core';
import { UiTreeClickEventItem, UITreeListItem } from './models/ui-tree-list.model';

@Injectable()
export class UiTreeListService {
  /**
   * Devuelve un array de items de tree2 en orden hasta encontrar el item especificado
   * Por defecto se devuelve donde el primero es el primer padre y el ultimo item del array es el hijo a buscar
   * Si reverse true se devuelve alreves, primero el hijo y ultimo el padre
   */
  getItemsPath(item: UITreeListItem, tree: UITreeListItem[], reverse: boolean = false, deepIndex = 0): UiTreeClickEventItem<any>[] {
    let result: UiTreeClickEventItem<any>[] = [];
    let currentTreeItem: UITreeListItem;
    let child: UiTreeClickEventItem<any>[];
    for (let i = 0, t = tree.length; i < t; i++) {
      currentTreeItem = tree[i];
      if (item && item.value && currentTreeItem.value === item.value) {
        result.push({
          index: i,
          deepIndex,
          item: currentTreeItem.value,
          hasChildrens: !!(currentTreeItem.childrens && currentTreeItem.childrens.length),
        });
        break;
      } else if (currentTreeItem.childrens && currentTreeItem.childrens.length) {
        child = this.getItemsPath(item, currentTreeItem.childrens, reverse, deepIndex + 1);
        if (child && child.length) {
          if (!reverse) {
            result.push({
              index: i,
              deepIndex,
              item: currentTreeItem.value,
              hasChildrens: true,
            });
          }
          result = result.concat(child);
          if (reverse) {
            result.push({
              index: i,
              deepIndex,
              item: currentTreeItem.value,
              hasChildrens: true,
            });
          }
          break;
        }
      }
    }
    return result;
  }
}
