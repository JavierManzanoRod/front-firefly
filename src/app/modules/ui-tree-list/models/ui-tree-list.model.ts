import { MayHaveLabel } from '@model/base-api.model';
import { Observable } from 'rxjs';

export interface UiTreeClickEventItem<T extends MayHaveLabel> {
  // Devuelve el indice con respecto alos hermanos del mismo nivel
  index: number;
  // Devuelve el indice con respecto al padre
  deepIndex: number;
  item: T;
  hasChildrens: boolean;
}

export interface UiTreeClickEvent<T> {
  path: UiTreeClickEventItem<T>[];
  selectedItem: T;
  selectedItemHasChildrens: boolean;
}

export enum UITreeStatusCode {
  warning = 'warning',
  error = 'error',
}
interface UITreeListI<T> {
  value: T;
  icon?: string;
  hide?: boolean;
  isOpen?: boolean;
  status?: UITreeStatusCode;
  parents?: T[];
  draggable?: boolean;
  childrens?: UITreeListI<T>[];
  loadChildrens?: (value: T) => Observable<UITreeListI<T>[]>;
}

export type UITreeListItem = UITreeListI<any>;

export type UITreeListItem2 = UITreeListI<MayHaveLabel>;

export interface UITreeListError {
  item: UITreeListItem;
  error: Error;
}
