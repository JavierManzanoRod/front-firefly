import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, TemplateRef } from '@angular/core';
import { configuration } from '../../../configuration/configuration';
import { UITreeListError, UITreeListItem, UITreeStatusCode } from '../models/ui-tree-list.model';

@Component({
  selector: 'ff-ui-tree-ref-list',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './ui-tree-ref-list.component.html',
  styleUrls: ['./ui-tree-ref-list.component.scss'],
})
export class UiTreeRefListComponent implements OnChanges {
  @Output() clicked: EventEmitter<UITreeListItem> = new EventEmitter<UITreeListItem>();
  @Output() collapsed: EventEmitter<UITreeListItem> = new EventEmitter<UITreeListItem>();
  @Output() expanded: EventEmitter<UITreeListItem> = new EventEmitter<UITreeListItem>();
  @Output() loadError: EventEmitter<UITreeListError> = new EventEmitter<UITreeListError>();

  @Input() nameTemplateRef!: TemplateRef<any>;
  @Input() lastSelectedItem!: UITreeListItem;
  @Input() textToSearch: string | null = null;
  @Input() parentItemsIds: string[] = [];
  @Input() parentItems: any[] = [];
  @Input() item!: UITreeListItem;
  @Input() showMatches = false;
  @Input() calculeItemsPath = false;
  @Input() outputAllItemOnDrag = false;

  isLoadingChildrens = false;
  errorOnLoadingChildrens = false;
  enumStatus = UITreeStatusCode;
  noHaveChildrens = false;
  myParentItemsIds: string[] = [];
  myParentItems: any[] = [];
  errorDefineRules!: boolean;

  constructor(private ref: ChangeDetectorRef) {}

  calculePath() {
    let item;
    let offer_path = '';
    let field_path = '';
    let underItemOffered = false;

    const isOffer = this.myParentItems[0].id === configuration.offerEntityType;

    if (isOffer) {
      for (let i = 0, l = this.myParentItems.length; i < l; i++) {
        item = this.myParentItems[i];
        if (underItemOffered) {
          // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
          field_path = field_path + (item.name[0] === '/' ? '' : '/') + item.name;
        } else {
          // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
          offer_path = offer_path + (item.name[0] === '/' ? '' : '/') + item.name;
        }
        if (item.name === 'item_offered') {
          underItemOffered = true;
        }
      }

      this.item.value.offer_path = offer_path;
      this.item.value.field_path = field_path;
      this.item.value.attribute_id = this.item.value.id;
    } else {
      let firstEmbeddedName = null;
      for (let i = 0, l = this.myParentItems.length; i < l; i++) {
        item = this.myParentItems[i];

        if (firstEmbeddedName) {
          // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
          field_path = field_path + (item.name[0] === '/' ? '' : '/') + item.name;
        }

        if (item.data_type === 'EMBEDDED') {
          firstEmbeddedName = item.name;
        }
      }

      this.item.value.field_path = field_path;
      this.item.value.attribute_id = firstEmbeddedName ? firstEmbeddedName : this.item.value.id;

      if (this.myParentItems.length > 1) {
        this.item.value.parent_data_type = this.myParentItems[this.myParentItems.length - 2].data_type;
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.parentItemsIds) {
      try {
        this.myParentItemsIds = [...this.parentItemsIds];
      } catch (e) {
        console.log('error cloning', e, changes.parentItemsIds);
        this.myParentItemsIds = [];
      }
      this.myParentItemsIds.push(this.item.value.id);
      if (this.item.value.entity_type_id) {
        this.myParentItemsIds.push(this.item.value.entity_type_id);
      }
      if (this.item.value.entity_reference) {
        this.myParentItemsIds.push(this.item.value.entity_reference);
      }
      if (this.item.value.entity_view && this.item.value.entity_view.id) {
        this.myParentItemsIds.push(this.item.value.entity_view.id);
      }
    }
    if (this.parentItems) {
      this.myParentItems = JSON.parse(this.stringify(this.parentItems));
    }
    this.myParentItems.push(this.item.value);
    this.item.parents = this.myParentItems;
    if (this.calculeItemsPath) {
      this.calculePath();
    }
  }

  emitClicked(event: any) {
    this.clicked.emit(event);
  }

  stringify(obj: any) {
    const cache: any = [];
    return JSON.stringify(obj, (key, value) => {
      if (typeof value === 'object' && value !== null) {
        // Duplicate reference found, discard key
        if (cache.includes(value)) {
          return;
        }

        // Store value in our collection
        cache.push(value);
      }
      return value;
    });
  }

  canShow(value: any): boolean {
    const ids = [value.id];
    if (value.entity_type_id) {
      ids.push(value.entity_type_id);
    }
    if (value.entity_reference) {
      ids.push(value.entity_reference);
    }
    if (value.entity_view && value.entity_view.id) {
      ids.push(value.entity_view.id);
    }
    return !ids.some((r: any) => this.myParentItemsIds.indexOf(r) >= 0);
  }

  toggle() {
    this.errorOnLoadingChildrens = false;
    this.errorDefineRules = false;
    this.item.isOpen = !this.item.isOpen;
    // this.clicked.emit(this.item);
    if (this.item.isOpen && !this.item.childrens && this.item.loadChildrens && !this.noHaveChildrens) {
      this.isLoadingChildrens = true;
      this.item.loadChildrens(this.item.value).subscribe(
        (data: any) => {
          if (data && data.define_rules) {
            this.item.draggable = false;
            this.isLoadingChildrens = false;
            this.errorDefineRules = true;
            this.ref.markForCheck();
            return;
          }
          if (data && data.length) {
            this.item.childrens = data;
            if (this.item.isOpen) {
              this.expanded.emit(this.item);
            } else {
              this.collapsed.emit(this.item);
            }
          } else {
            this.noHaveChildrens = true;
          }
          this.isLoadingChildrens = false;

          this.clicked.emit(this.item);
          this.ref.markForCheck();
        },
        (err) => {
          console.error(err);
          this.isLoadingChildrens = false;
          this.errorOnLoadingChildrens = true;
          this.loadError.emit({ error: err, item: this.item });
          this.ref.markForCheck();
        }
      );
    } else {
      this.clicked.emit(this.item);
      if (this.item.isOpen) {
        this.expanded.emit(this.item);
      } else {
        this.collapsed.emit(this.item);
      }
    }
  }

  get iconClasses() {
    // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
    return this.item.icon + ' icon';
  }

  getName() {
    let name = this.item.value?.label || this.item.value?.name;
    if (this.textToSearch && this.showMatches) {
      const regexp = new RegExp(this.textToSearch, 'gi');
      const responses = [];
      let response;
      // eslint-disable-next-line no-cond-assign
      while ((response = regexp.exec(name))) {
        responses.push(response);
      }
      [...responses].reverse().forEach((value) => {
        name =
          // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
          name.substr(0, value.index) + '<span class="text-highlight">' + value[0] + '</span>' + name.substr(value.index + value[0].length);
      });
    }

    return name;
  }
}
