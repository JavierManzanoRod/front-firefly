import { UiTreeStateService } from '../../ui-tree/services/ui-tree-state.service';
import { UiTreeComponent } from '../../ui-tree/ui-tree.component';

describe('UiTreeComponent', () => {
  it('it should create ', () => {
    const component = new UiTreeComponent(new UiTreeStateService());
    expect(component).toBeDefined();
  });
});
