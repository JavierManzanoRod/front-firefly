import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { Page } from '@model/metadata.model';
import { UiTreeClickEvent, UITreeListError, UITreeListItem } from './models/ui-tree-list.model';
import { UiTreeListService } from './ui-tree-list.service';

@Component({
  selector: 'ff-ui-tree-list',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './ui-tree-list.component.html',
  styleUrls: ['./ui-tree-list.component.scss'],
})
export class UiTreeListComponent<T> {
  // Se llama siempre que se pincha en un elemento, independientemente si es para abrir o cerrar
  @Output() clicked: EventEmitter<UiTreeClickEvent<T>> = new EventEmitter<UiTreeClickEvent<T>>();
  // Se llama siempre que se cierra un elemento
  @Output() collapsed: EventEmitter<UiTreeClickEvent<T>> = new EventEmitter<UiTreeClickEvent<T>>();
  // Se llama siempre que se abre un elemento
  @Output() expanded: EventEmitter<UiTreeClickEvent<T>> = new EventEmitter<UiTreeClickEvent<T>>();
  // Se llama cuando hay un error de carga contra el backend
  @Output() loadError: EventEmitter<UITreeListError> = new EventEmitter<UITreeListError>();
  // Se llama cada vez que se escribe en el input search si searchOnline = true
  @Output() filter: EventEmitter<string> = new EventEmitter<string>();
  @Output() loadMore: EventEmitter<number> = new EventEmitter<number>();

  @Input() searchOnline = false;

  @Input() hideSearch = false;
  @Input() loading = false;
  @Input() items: UITreeListItem[] = [];
  @Input() nameTemplateRef!: TemplateRef<any>;
  @Input() searchOnlyOnParentItems = false;
  @Input() searchPlaceholder = 'Términos de búsqueda...';
  @Input() showMatches = false;
  @Input() pagination!: Page;
  @Input() adviceMessage!: string;
  @Input() calculeItemsPath = false;
  @Input() outputAllItemOnDrag = false;

  lastSelectedItem!: UITreeListItem;
  searchModel = '';

  constructor(private uiTreeListService: UiTreeListService) {}

  filterTreeByName(criteria: string, items: UITreeListItem[]) {
    let hasChildsWithHideFalse = false;
    let showOneChild = false;
    console.log('🚀 ~ file: ui-tree-list.component.ts ~ line 47 ~ UiTreeListComponent<T> ~ filterTreeByName ~ false', false);

    items.forEach((item: UITreeListItem) => {
      item.hide = true;
      showOneChild = false;

      if (item.childrens && item.childrens.length && !this.searchOnlyOnParentItems) {
        showOneChild = this.filterTreeByName(criteria, item.childrens);
      }

      if (
        (item.value.name && item.value.name.toLowerCase().indexOf(criteria.toLowerCase()) >= 0) ||
        (item.value.label && item.value.label.toLowerCase().indexOf(criteria.toLowerCase()) >= 0) ||
        showOneChild
      ) {
        item.hide = false;
        hasChildsWithHideFalse = true;
      }
    });

    return hasChildsWithHideFalse;
  }
  search() {
    this.filter.emit(this.searchModel);
  }

  resetSearch(items: UITreeListItem[]) {
    items.forEach((item) => {
      item.hide = false;
      if (item.childrens && item.childrens.length) {
        this.resetSearch(item.childrens);
      }
    });
  }

  onCollapsed(event: UITreeListItem) {
    this.collapsed.emit(this.getDataToEmitEvent(event));
  }

  onExpand(event: UITreeListItem) {
    this.expanded.emit(this.getDataToEmitEvent(event));
  }

  onClicked(event: UITreeListItem) {
    this.lastSelectedItem = event;
    this.clicked.emit(this.getDataToEmitEvent(event));
  }

  private getDataToEmitEvent(event: UITreeListItem): UiTreeClickEvent<T> {
    return {
      path: this.uiTreeListService.getItemsPath(event, this.items),
      selectedItem: event.value,
      selectedItemHasChildrens: !!(event.childrens && event.childrens.length),
    };
  }
}
