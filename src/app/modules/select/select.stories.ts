import { FormWrapper } from '.storybook/form-wrapper.component';
import { StorybookWrappersModule } from '.storybook/StorybookWrappers.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { componentWrapperDecorator, Meta, moduleMetadata, Story } from '@storybook/angular';
import { of } from 'rxjs';
import { SelectComponent } from './select.component';

export default {
  component: SelectComponent,
  decorators: [
    moduleMetadata({
      declarations: [SelectComponent],
      imports: [FormsModule, ReactiveFormsModule, CommonModule, NgSelectModule, HttpClientModule, StorybookWrappersModule],
    }),
    componentWrapperDecorator(FormWrapper),
  ],
  title: 'SelectComponent',
  parameters: {
    docs: {
      description: {
        component: 'Lo mas básico, donde se almacena el valor seleccionado en una variable usando ngModel.',
      },
    },
  },
  args: {
    ngModel: 'Type anything',
    name: 'something',
  },
} as Meta;

const Template: Story<SelectComponent> = (args) => ({
  props: {
    ...args,
  },
});

const generateLabel = (label: string) => ({ label, name: label, icon: label });
const searchFn = () => of(['Label 1', 'Label 2', 'Label 3'].map(generateLabel));
export const Default = Template.bind({});
Default.args = {
  searchFn,
};
