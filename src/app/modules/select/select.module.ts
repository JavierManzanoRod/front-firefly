import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SelectMultipleComponent } from './select-multiple.component';
import { SelectComponent } from './select.component';

@NgModule({
  declarations: [SelectComponent, SelectMultipleComponent],
  imports: [FormsModule, ReactiveFormsModule, CommonModule, NgSelectModule],
  exports: [SelectComponent, SelectMultipleComponent],
})
export class SelectModule {}
