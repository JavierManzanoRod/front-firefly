import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { IApiSearchable } from '@core/base/api.service';
import { MayHaveIdName } from '@model/base-api.model';
import { SelectSearchFn } from '@model/search-fn.model';
import { concat, Observable, of, Subject } from 'rxjs';
import { catchError, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';

interface MayHaveLabel extends MayHaveIdName {
  label?: string;
}

@Component({
  selector: 'ff-select',
  templateUrl: 'select.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    },
  ],
})
export class SelectComponent implements OnInit {
  @Input() searchFn?: SelectSearchFn;
  isDisabled = false;
  initialized = false;
  item$!: Observable<any[]>;
  itemLoading = false;
  itemInput$ = new Subject<string>();
  entity!: MayHaveLabel;
  searchMethod!: SelectSearchFn | IApiSearchable<any>['search'];

  constructor(protected apiEntityType: EntityTypeService) {}

  ngOnInit() {
    if (this.searchFn) {
      this.searchMethod = this.searchFn;
    } else {
      this.searchMethod = (key) => this.apiEntityType.search(key);
    }
    this.loadItems();
  }

  handleChangeValue($event: any) {
    this.writeValue($event);
  }

  trackByFn(item: MayHaveIdName) {
    return item.id;
  }

  writeValue(data: any): void {
    this.entity = data;
    this.onChange(data);
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  // Allows Angular to register a function to call when the input has been touched.
  // Save the function as a property to call later here.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  registerOnTouched(fn: () => void): void {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange = (data: any) => {
    // don't delete!
  };

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }
  private loadItems() {
    this.item$ = concat(
      of([]), // default items
      this.itemInput$.pipe(
        distinctUntilChanged(),
        tap(() => (this.itemLoading = true)),
        switchMap((term) => {
          if (!term) {
            return of([]).pipe(tap(() => (this.itemLoading = false)));
          }
          return this.searchMethod(term).pipe(
            catchError(() => of([])), // empty list on error
            tap(() => (this.itemLoading = false))
          );
        })
      )
    );
  }
}
