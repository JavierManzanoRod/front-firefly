import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IApiSearchable } from '@core/base/api.service';
import { MayHaveIdName } from '@model/base-api.model';
import { SelectSearchFn } from '@model/search-fn.model';
import { concat, Observable, of, Subject } from 'rxjs';
import { catchError, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'ff-select-multiple',
  templateUrl: 'select-multiple.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectMultipleComponent),
      multi: true,
    },
  ],
})
export class SelectMultipleComponent implements OnInit {
  @Input() label!: string;
  @Input() id!: string;
  @Input() searchFn!: SelectSearchFn;

  options$!: Observable<MayHaveIdName[]>;
  model: MayHaveIdName[] = [];
  itemInput$ = new Subject<string>();
  isDisabled = false;
  initialized = false;
  itemLoading = false;
  formSelect: FormGroup;
  searchMethod!: SelectSearchFn | IApiSearchable<any>['search'];

  constructor(private fb: FormBuilder) {
    this.formSelect = this.fb.group({
      select: [null],
    });
  }

  ngOnInit() {
    if (this.searchFn) {
      this.searchMethod = this.searchFn;
    }

    this.options$ = concat(
      of([]), // default items
      this.itemInput$.pipe(
        distinctUntilChanged(),
        tap(() => (this.itemLoading = true)),
        switchMap((term) => {
          return this.searchMethod(term).pipe(
            map((list) => {
              return list.filter((backendRecord) => {
                if (!this.model) {
                  return true;
                } else {
                  const alreadySelected = this.model.findIndex((selectedItem) => backendRecord.id === selectedItem.id);
                  return alreadySelected === -1;
                }
              });
            }),
            catchError(() => of([])), // empty list on error
            tap(() => (this.itemLoading = false))
          );
        })
      )
    );
  }

  selectedOption(newSelection: MayHaveIdName) {
    this.model.push(newSelection);
    this.formSelect.controls.select.setValue(null);
    this.writeValue(this.model);
  }

  removeSelectedItem(index: number) {
    this.model.splice(index, 1);
    this.writeValue(this.model);
  }

  trackByFn(item: MayHaveIdName) {
    return item.id;
  }

  writeValue(data: any): void {
    const dataToRegister = data && data.length ? data : null;
    this.onChange(dataToRegister);
    this.model = dataToRegister || [];
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  // Allows Angular to register a function to call when the input has been touched.
  // Save the function as a property to call later here.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  registerOnTouched(fn: () => void): void {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange(data: any) {
    // don't delete!
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }
}
