import { FormBuilder } from '@angular/forms';
import { SelectMultipleComponent } from './select-multiple.component';

describe('SelectMultipleComponent', () => {
  it('it creates ', () => {
    const comp = new SelectMultipleComponent(new FormBuilder());
    comp.ngOnInit();
    expect(comp).toBeDefined();
  });

  it('setDisabled set isDisabled flag ', () => {
    const comp = new SelectMultipleComponent(new FormBuilder());
    comp.setDisabledState(true);
    expect(comp.isDisabled).toBeTruthy();
  });
});
