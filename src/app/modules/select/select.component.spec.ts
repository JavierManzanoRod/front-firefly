import { SelectComponent } from './select.component';

describe('SelectComponent', () => {
  it('it creates ', () => {
    const comp = new SelectComponent(null as any);
    expect(comp).toBeDefined();
  });

  it('setDisabled set isDisabled flag ', () => {
    const comp = new SelectComponent(null as any);
    comp.setDisabledState(true);
    expect(comp.isDisabled).toBeTruthy();
  });
});
