import { Component, Input } from '@angular/core';

@Component({
  selector: 'ff-badge-i18n',
  template: `<span class="badge badge-pill badge-info badge-i18n">
    {{ 'LANGUAGES.' + text | translate }}
  </span> `,
  styleUrls: ['./badge-i18n.component.scss'],
})
export class BadgeI18nComponent {
  @Input() text!: string;

  constructor() {}
}
