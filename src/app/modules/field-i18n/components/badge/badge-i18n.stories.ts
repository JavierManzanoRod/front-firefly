import { CommonModule } from '@angular/common';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { StorybookTranslateModule } from '../../../../../../.storybook/StorybookTranslate.module';
import { BadgeI18nComponent } from './badge-i18n.component';

export default {
  component: BadgeI18nComponent,
  decorators: [
    moduleMetadata({
      declarations: [BadgeI18nComponent],
      imports: [CommonModule, StorybookTranslateModule],
    }),
  ],
  title: 'I18n/Badge',
  parameters: {
    docs: {
      description: {
        component:
          'Añade textos en los diferentes idiomas preestablecidos y la componente te devuelve un objeto idioma:valor usando formularios de angular',
      },
    },
  },
} as Meta;

const Template: Story<BadgeI18nComponent> = (args) => ({
  props: {
    ...args,
  },
});

export const Default = Template.bind({});
Default.args = {
  text: 'es_ES',
};
