import { ChangeDetectorRef } from '@angular/core';
import { UiModalCommonService } from 'src/app/modules/common-modal/services/ui-modal-common.service';
import { FieldI18nMultipleComponent } from './field-i18-multiple.component';

let change: ChangeDetectorRef;

describe('FieldI18nMultipleComponent', () => {
  it('it should create ', () => {
    const component = new FieldI18nMultipleComponent(null as unknown as UiModalCommonService, [], change);
    expect(component).toBeDefined();
  });
});
