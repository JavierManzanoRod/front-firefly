import { FormWrapper } from '.storybook/form-wrapper.component';
import { CommonModule } from '@angular/common';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { componentWrapperDecorator, Meta, moduleMetadata, Story } from '@storybook/angular';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { TableWithFixedElementsModule } from 'src/app/modules/table-with-fixed-elements/table-with-fixed-elements.module';
import { StorybookTranslateModule } from '../../../../../../.storybook/StorybookTranslate.module';
import { StorybookWrappersModule } from '../../../../../../.storybook/StorybookWrappers.module';
import { FieldI18nMultipleComponent } from './field-i18-multiple.component';

export default {
  component: FieldI18nMultipleComponent,
  decorators: [
    moduleMetadata({
      declarations: [FieldI18nMultipleComponent],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AccordionModule.forRoot(),
        StorybookTranslateModule,
        StorybookWrappersModule,
        NgSelectModule,
        CommonModalModule,
        TableWithFixedElementsModule,
        //AlertModule,
        // ModalModule.forRoot(),
      ],
    }),
    componentWrapperDecorator(FormWrapper),
  ],
  title: 'I18n/Multiple',
  parameters: {
    docs: {
      description: {
        component:
          'Añade textos en los diferentes idiomas preestablecidos y la componente te devuelve un objeto idioma:valor usando formularios de angular',
      },
    },
  },
} as Meta;

const Template: Story<FieldI18nMultipleComponent> = (args) => ({
  props: {
    ...args,
    ngModel: 'Type anything',
    name: 'something',
  },
});

export const Default = Template.bind({});
Default.args = {
  heading: 'Heading of the form',
  control: new FormControl(),
};
