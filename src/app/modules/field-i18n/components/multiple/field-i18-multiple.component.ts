import { ChangeDetectorRef, Component, ElementRef, forwardRef, Inject, Input, QueryList, ViewChildren } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FF_LANGUAGES } from 'src/app/configuration/tokens/language.token';
import { UiModalCommonService } from '../../../common-modal/services/ui-modal-common.service';

@Component({
  selector: 'ff-field-multiple-i18n',
  templateUrl: 'field-i18n-multiple.component.html',
  styleUrls: ['./field-i18n-multiple.component.scss'],
  providers: [
    UiModalCommonService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FieldI18nMultipleComponent),
      multi: true,
    },
  ],
})
export class FieldI18nMultipleComponent {
  @ViewChildren('input') input!: QueryList<ElementRef>;
  @Input() control!: FormControl;
  @Input() heading = '';
  value: { [key: string]: string[] } | null = null;
  initialized = false;
  resolvedData = false;
  // languages: string[] = environment.LANGUAGES;
  currentLanguage: string = this.languages[0];

  toucheds: boolean[] = [];

  canAddValue = true;
  showError = false;
  disabled = false;

  constructor(
    private readonly uiModalCommonService: UiModalCommonService,
    @Inject(FF_LANGUAGES) public languages: string[],
    private ref: ChangeDetectorRef
  ) {}

  removeValue(language: string, index: number) {
    if (language === this.languages[0]) {
      this.uiModalCommonService
        .showWarning({
          title: 'FIELD_I18N_MULTIPLE.MODAL_REMOVE.TITLE',
          errorMessage: 'FIELD_I18N_MULTIPLE.MODAL_REMOVE.WARNING',
          bodyMessage: 'FIELD_I18N_MULTIPLE.MODAL_REMOVE.MESSAGE',
          okButtonText: 'ACCEPT',
          cancelButtonText: 'CANCEL',
        })
        .subscribe((accept: boolean) => {
          if (accept) {
            this.languages.forEach((lang) => {
              if (!!this.value && this.value[lang]) {
                this.value[lang].splice(index, 1);
              }
              this.toucheds.splice(index, 1);
            });
            if (!!this.value && !this.value[language].length) {
              this.value = null;
            }
            this.writeValue(this.value, false, true);
            this.checkIfShowError();
            this.checkIfcanAddNewColumn();
          }
        });
    } else {
      if (this.value) {
        this.value[language][index] = '';
      }
      this.writeValue(this.value, false, true);
      this.checkIfShowError();
      this.checkIfcanAddNewColumn();
    }
  }

  focusInput(i: number, x: number) {
    if (this.value) {
      const l = this.value[this.languages[0]].length;
      const index = x * l + i;
      if (this.input && this.input.toArray()[index]) {
        this.input.toArray()[index].nativeElement.focus();
      }
    }
  }

  track(index: number) {
    return index;
  }

  addNewValue() {
    if (!this.value) {
      this.value = {};
    }
    this.languages.forEach((language) => {
      if (this.value) {
        if (!this.value[language]) {
          this.value[language] = [];
        }
        this.value[language].push('');
      }
    });

    this.checkIfcanAddNewColumn();
  }

  checkIfcanAddNewColumn() {
    if (this.value) {
      if (this.languages[0] in this.value) {
        const values = this.value[this.languages[0]];
        if (!values || !values.length) {
          this.canAddValue = true;
        }
        if (values.length) {
          const emptyValues = values.filter((val) => {
            return !val;
          });
          this.canAddValue = !emptyValues.length;
        }
        this.checkIfShowError();
      }
    }
  }

  checkIfShowError() {
    if (this.value && this.value[this.languages[0]]) {
      const itemsError = this.value[this.languages[0]].filter((val: string, index: number) => {
        return !val && this.toucheds[index];
      });
      this.showError = !!itemsError.length;
    } else {
      this.showError = false;
    }
  }

  inputChange(language: string, index: number) {
    if (language === this.languages[0]) {
      this.toucheds[index] = true;
    }
    this.writeValue(this.value, false, true);
  }

  markAllTouched() {
    if (this.value && this.value[this.languages[0]]) {
      this.value[this.languages[0]].forEach((value, index) => {
        this.toucheds[index] = true;
      });
    }
  }

  initValues(data: any) {
    if (data) {
      if (data.es_ES) {
        const columns = data.es_ES.length;
        this.languages.forEach((lang) => {
          for (let x = 0; x < columns; x++) {
            if (!data[lang]) {
              data[lang] = [];
            }
            if (!data[lang][x]) {
              data[lang][x] = '';
            }
          }
        });

        this.value = data;
      }
    }
  }

  writeValue(data: { [key: string]: string[] } | null, innerUpdate = false, haveChange: boolean): void {
    if (!innerUpdate) {
      this.initValues(data);
    }
    this.checkIfcanAddNewColumn();
    this.markAllTouched();
    this.checkIfShowError();

    if (haveChange) {
      this.onChange(data);
    }
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  // Allows Angular to register a function to call when the input has been touched.
  // Save the function as a property to call later here.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  registerOnTouched(fn: () => void): void {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange(data: any) {
    // don't delete!
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    this.ref.markForCheck();
  }
}
