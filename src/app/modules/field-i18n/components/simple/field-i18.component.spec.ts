import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { FieldI18nComponent } from './field-i18.component';

describe('FieldI18nComponent', () => {
  let component: FieldI18nComponent;
  let fixture: ComponentFixture<FieldI18nComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FieldI18nComponent],
      providers: [
        {
          provide: BsModalService,
          useFactory: () => ({
            error: () => {},
            show: () => {
              return {
                hide: () => {},
                content: {
                  confirm: of({}),
                  cancel: of({}),
                },
              };
            },
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldI18nComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
