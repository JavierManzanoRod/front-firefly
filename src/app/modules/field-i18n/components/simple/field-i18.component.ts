import { Component, ElementRef, forwardRef, Inject, Input, QueryList, ViewChildren } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { FF_LANGUAGES } from 'src/app/configuration/tokens/language.token';
import { UiModalSelectService } from '../../../ui-modal-select/ui-modal-select.service';

@Component({
  selector: 'ff-field-i18n',
  templateUrl: 'field-i18n.component.html',
  styleUrls: ['./field-i18.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FieldI18nComponent),
      multi: true,
    },
  ],
})
export class FieldI18nComponent {
  @ViewChildren('input') input!: QueryList<ElementRef>;
  @Input() control!: FormControl;
  @Input() heading = '';
  @Input() readonly = false;

  @Input() requiredFirstLanguage = true;
  disabled = false;
  currentLanguage = this.languages[0];

  value: { [key: string]: string } = {};

  showError = false;

  constructor(
    private readonly uiModalSelectService: UiModalSelectService,
    private readonly bsModalService: BsModalService,
    @Inject(FF_LANGUAGES) public languages: string[]
  ) {}

  openBlockModal() {
    this.uiModalSelectService
      .showI18N(
        {
          items: this.value,
          title: 'FIELD_I18N.EDIT_ATTR_I18N',
        },
        this.bsModalService
      )
      .subscribe((data) => {
        this.value = data;
        this.writeValue(this.value, true, true);
      });
  }

  getInitValue(data: { [key: string]: string }) {
    const value: any = {};
    for (const country of this.languages) {
      value[country] = (data && data[country]) || null;
    }
    return value;
  }

  isFilled(data: { [key: string]: string }) {
    if (!data) {
      return false;
    } else {
      for (const country of this.languages) {
        const val = data[country];
        if (val !== null && val !== '') {
          return true;
        }
      }
      return false;
    }
  }

  checkIfShowError() {
    if (this.requiredFirstLanguage) {
      const firstHaveValue = !!this.value[this.languages[0]];
      const othersHaveValue = this.languages.reduce((acc: boolean, value: string) => {
        if (!acc && this.value[value] && value !== this.languages[0]) {
          return true;
        }
        return acc;
      }, false);

      this.showError = false;
      if (!firstHaveValue && othersHaveValue) {
        this.showError = true;
      }
    }
  }

  inputChange() {
    this.writeValue(this.value, false, true);
    this.checkIfShowError();
  }

  removeValue(language: string) {
    this.value[language] = '';
    this.writeValue(this.value, false, true);
    this.checkIfShowError();
  }

  writeValue(data: { [key: string]: string }, innerUpdate = false, haveChange: boolean): void {
    if (!innerUpdate) {
      this.value = this.getInitValue(data);
    }
    this.checkIfShowError();
    if (haveChange) {
      this.onChange(data);
    }
  }

  focusInput(index: number) {
    if (this.input && this.input.toArray()[index]) {
      this.input.toArray()[index].nativeElement.focus();
    }
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  registerOnTouched(fn: () => void): void {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange(data: any) {
    // don't delete!
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }
}
