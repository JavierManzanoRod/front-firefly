import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { TableWithFixedElementsModule } from 'src/app/modules/table-with-fixed-elements/table-with-fixed-elements.module';
import { StorybookTranslateModule } from '../../../../../../.storybook/StorybookTranslate.module';
import { generateFormAttributes, StorybookWrappersModule } from '../../../../../../.storybook/StorybookWrappers.module';
import { FieldI18nComponent } from './field-i18.component';

export default {
  component: FieldI18nComponent,
  decorators: [
    moduleMetadata({
      declarations: [FieldI18nComponent],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AccordionModule.forRoot(),
        StorybookTranslateModule,
        StorybookWrappersModule,
        NgSelectModule,
        TableWithFixedElementsModule,
        CommonModalModule,
      ],
    }),
  ],
  title: 'I18n/Simple',
  args: {
    heading: 'Edita los idiomas',
  },
  parameters: {
    docs: {
      description: {
        component:
          'Añade textos en los diferentes idiomas preestablecidos y la componente te devuelve un objeto idioma:valor usando formularios de angular',
      },
    },
  },
} as Meta;

const Template: Story<FieldI18nComponent> = (args) => ({
  props: {
    ...args,
  },
  template: `
  <ff-form-wrapper>
      <ff-field-i18n  ${generateFormAttributes(args)} >
      </ff-field-i18n>
  </ff-form-wrapper>`,
});

export const Default = Template.bind({});
Default.args = {
  requiredFirstLanguage: true,
};

export const NoFirstLang = Template.bind({});
NoFirstLang.args = {
  requiredFirstLanguage: false,
};
