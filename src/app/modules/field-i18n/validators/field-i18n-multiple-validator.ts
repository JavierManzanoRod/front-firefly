import { AbstractControl, ValidatorFn } from '@angular/forms';
import { environment } from '@env/environment';

export function fieldI18nMultipleValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const val: { [key: string]: string[] } = control.value;
    const lang = environment.LANGUAGES.split(',');
    if (!val || !Object.prototype.hasOwnProperty.call(val, lang[0])) {
      return null;
    }
    let allItemsOfRequiredLanguageHaveValue = true;
    val[lang[0]].forEach((value: any) => {
      if (!value) {
        allItemsOfRequiredLanguageHaveValue = false;
      }
    });

    if (!val.es_ES || !val.es_ES.length) {
      allItemsOfRequiredLanguageHaveValue = false;
    }

    return allItemsOfRequiredLanguageHaveValue ? null : { i18nRequiredLaguageNoHaveValueInAllIndex: true };
  };
}
