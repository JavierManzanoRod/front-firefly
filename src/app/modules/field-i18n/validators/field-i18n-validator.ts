import { AbstractControl, ValidatorFn } from '@angular/forms';

export function fieldI18nValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const val: { [key: string]: string } = control.value;
    const keys = (val && Object.keys(val)) || [];

    if (!val || !keys.reduce((count: number, key: string) => count + val[key].length, 0)) {
      return null;
    }

    let allItemsOfRequiredLanguageHaveValue = true;

    if (!val.es_ES || !val.es_ES.length) {
      allItemsOfRequiredLanguageHaveValue = false;
    }

    return allItemsOfRequiredLanguageHaveValue ? null : { i18nRequiredLaguageNoHaveValue: true };
  };
}
