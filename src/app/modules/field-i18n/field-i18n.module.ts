import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TableWithFixedElementsModule } from '../table-with-fixed-elements/table-with-fixed-elements.module';
import { BadgeI18nComponent } from './components/badge/badge-i18n.component';
import { FieldI18nMultipleComponent } from './components/multiple/field-i18-multiple.component';
import { FieldI18nComponent } from './components/simple/field-i18.component';

@NgModule({
  declarations: [FieldI18nComponent, FieldI18nMultipleComponent, BadgeI18nComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    CommonModule,
    AccordionModule.forRoot(),
    NgSelectModule,
    TableWithFixedElementsModule,
    AlertModule,
  ],
  exports: [FieldI18nComponent, FieldI18nMultipleComponent, BadgeI18nComponent],
})
export class FieldI18nModule {}
