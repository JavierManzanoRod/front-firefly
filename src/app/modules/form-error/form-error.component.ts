import { Component, Input } from '@angular/core';

@Component({
  selector: 'ff-form-error',
  templateUrl: './form-error.component.html',
  styleUrls: ['./form-error.component.scss'],
})
export class FormErrorComponent {
  @Input() field!: any;
  @Input() messages!: Array<any>;
}
