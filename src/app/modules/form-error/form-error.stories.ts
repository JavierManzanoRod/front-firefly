import { FormWrapper } from '.storybook/form-wrapper.component';
import { StorybookTranslateModule } from '.storybook/StorybookTranslate.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { LoadingModule } from '../loading/loading.module';
import { SelectModule } from '../select/select.module';
import { FormErrorComponent } from './form-error.component';

export default {
  component: FormErrorComponent,
  decorators: [
    moduleMetadata({
      declarations: [FormWrapper, FormErrorComponent],
      imports: [CommonModule, ReactiveFormsModule, FormsModule, LoadingModule, StorybookTranslateModule, NgSelectModule, SelectModule],
    }),
  ],
  title: 'Form Error',
  parameters: {
    docs: {
      description: {
        component: 'Ejemplo de como manejar los errores de los formularios usando esta componente.',
      },
    },
  },
} as Meta;

const Template: Story<FormErrorComponent> = (args) => ({
  props: {
    ...args,
  },
  template: `<ff-form-wrapper>
  <input type="text" #i="ngModel" required ngModel name="example">
  <ff-form-error [field]="i.control" [messages]="[{'type':'required','message':'El campo es requerido'}]" ></ff-form-error>
  </ff-form-wrapper>

  `,
});

export const Default = Template.bind({});
Default.args = {};
