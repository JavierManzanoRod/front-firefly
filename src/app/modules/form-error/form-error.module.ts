import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { LoadingModule } from 'src/app/modules/loading/loading.module';
import { SelectModule } from '../select/select.module';
import { FormErrorComponent } from './form-error.component';

@NgModule({
  declarations: [FormErrorComponent],
  imports: [CommonModule, ReactiveFormsModule, FormsModule, LoadingModule, TranslateModule, NgSelectModule, SelectModule],
  exports: [FormErrorComponent],
})
export class FormErrorModule {}
