import { Injectable } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { ModalWarningComponent } from '../components/modal-warning/modal-warning.component';
import { UICommonModalConfig } from '../components/modal-warning/modal-warning.model';

@Injectable({
  providedIn: 'root',
})
export class UiModalWarningUnChangesService {
  constructor(public modalService: BsModalService) {}

  show(reason: UICommonModalConfig): Observable<boolean> {
    return this.modal({
      ...this.defaultLabelConfig(),
      ...reason,
    });
  }

  private defaultLabelConfig(): UICommonModalConfig {
    return {
      title: 'COMMON_MODALS.UNSAVED_CHANGES_TITLE',
      errorMessage: 'COMMON_MODALS.UNSAVED_CHANGES_ERROR_MESSAGE',
      bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_TRANSLATIONS_1',
      questionMessage: 'COMMON_MODALS.UNSAVED_ATTS_TRANSLATIONS_2',
      cancelButtonText: 'COMMON_MODALS.UNSAVED_CHANGES_BACK',
      okButtonText: 'COMMON_MODALS.UNSAVED_CHANGES_CONTINUE',
    };
  }

  private modal(config: UICommonModalConfig): Observable<boolean> {
    const modal = this.modalService.show(ModalWarningComponent as any, {
      initialState: { config },
    });

    const content = modal.content as ModalWarningComponent;
    return content.confirm.asObservable();
  }
}
