import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from '../../shared/shared.module';
import { ModalFooterComponent } from './components/base/modal-footer.component';
import { ModalHeaderComponent } from './components/base/modal-header.component';
import { ModalCloneComponent } from './components/modal-clone/modal-clone.component';
import { ModalCreateComponent } from './components/modal-create/modal-create.component';
import { ModalDeleteComponent } from './components/modal-delete/modal-delete.component';
import { ModalPublishComponent } from './components/modal-publish/modal-publish.component';
import { ModalSaveComponent } from './components/modal-save/modal-save.component';
import { ModalStopComponent } from './components/modal-stop/modal-stop.component';
import { ModalUnsavedTabComponent } from './components/modal-unsaved-tab/modal-unsaved-tab.component';
import { ModalWarningComponent } from './components/modal-warning/modal-warning.component';

@NgModule({
  declarations: [
    ModalFooterComponent,
    ModalHeaderComponent,
    ModalDeleteComponent,
    ModalSaveComponent,
    ModalCloneComponent,
    ModalUnsavedTabComponent,
    ModalCreateComponent,
    ModalWarningComponent,
    ModalPublishComponent,
    ModalStopComponent,
  ],
  imports: [ModalModule.forRoot(), SharedModule],
  exports: [ModalFooterComponent, ModalHeaderComponent, ModalWarningComponent, ModalPublishComponent, ModalStopComponent],
})
export class CommonModalModule {}
