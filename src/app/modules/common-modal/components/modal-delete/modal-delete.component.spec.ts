import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { BsModalRef, BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { ModalDeleteComponent } from './modal-delete.component';

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Component({
  selector: 'ff-modal-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockModalHeaderComponent {
  @Input() loading!: boolean;
  @Input() item: any;
  @Input() entityType: any;
  @Input() entityTypesRef: any;
}

@Component({
  selector: 'ff-modal-footer',
  template: '<p>Mock Product Editor Component</p>',
})
class MockModalFooterComponent {
  @Input() loading!: boolean;
  @Input() item: any;
  @Input() entityType: any;
  @Input() entityTypesRef: any;
}

describe('ModalDeleteComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TranslatePipeMock, MockModalHeaderComponent, MockModalFooterComponent, ModalDeleteComponent],
        imports: [ModalModule.forRoot()],
        providers: [BsModalService, BsModalRef],
      }).compileComponents();
    })
  );
  it('ModalDeleteComponent should create ', () => {
    const fixture = TestBed.createComponent(ModalDeleteComponent);
    expect(fixture).toBeDefined();
  });
});
