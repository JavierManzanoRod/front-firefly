import { StorybookTranslateModule } from '.storybook/StorybookTranslate.module';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CommonModalModule } from '../common-modal.module';
import { ModalFooterComponent } from './base/modal-footer.component';
import { ModalHeaderComponent } from './base/modal-header.component';
import { ModalCloneComponent } from './modal-clone/modal-clone.component';
import { ModalCreateComponent } from './modal-create/modal-create.component';
import { ModalDeleteComponent } from './modal-delete/modal-delete.component';
import { ModalSaveComponent } from './modal-save/modal-save.component';
import { ModalStopComponent } from './modal-stop/modal-stop.component';
import { ModalUnsavedTabComponent } from './modal-unsaved-tab/modal-unsaved-tab.component';
import { ModalWarningComponent } from './modal-warning/modal-warning.component';

@Component({
  selector: 'ff-toast-story',
  template: `
    <div class="mt-4">
      <div class="page-section p-4">
        <div class="row">
          <div class="col-4 mb-4" *ngFor="let modal of modales">
            <button class="btn btn-dark btn-block" (click)="openModal(modal)">
              {{ modal.name }}
            </button>
          </div>
        </div>
      </div>
    </div>
  `,
})
class ModalStoryComponent {
  modales = [
    { name: 'Stop', modal: ModalStopComponent },
    { name: 'Footer', modal: ModalFooterComponent },
    { name: 'Header', modal: ModalHeaderComponent, config: { title: 'Modal de Header' } },
    { name: 'Clone', modal: ModalCloneComponent },
    { name: 'Create', modal: ModalCreateComponent },
    { name: 'Delete', modal: ModalDeleteComponent },
    { name: 'Save', modal: ModalSaveComponent },
    { name: 'UnSave', modal: ModalUnsavedTabComponent, config: { tabName: 'Unsave Modal' } },
    {
      name: 'Warning',
      modal: ModalWarningComponent,
      config: {
        config: {
          name: 'Warning',
          title: 'Modal de warning',
          errorMessage: 'Esto es una prueba',
          bodyMessage: 'Mensaje de prueba',
          questionMessage: '¿Hola?',
          okButtonText: 'Ok',
          cancelButtonText: 'Calcel',
        },
      },
    },
  ];
  constructor(private modalService: BsModalService) {}
  openModal(modal: any) {
    this.modalService.show(modal.modal, {
      initialState: {
        item: {
          name: modal?.name,
        },
        ...modal?.config,
      },
    });
  }
}
export default {
  component: ModalStoryComponent,
  decorators: [
    moduleMetadata({
      declarations: [ModalStoryComponent],
      imports: [CommonModule, CommonModalModule, StorybookTranslateModule],
    }),
  ],
  title: 'Modals',
  parameters: {
    docs: {
      description: {
        component: 'Distintos modales de la aplicacion',
      },
    },
  },
} as Meta;

const Template: Story<ModalStoryComponent> = (args) => ({
  props: {
    ...args,
  },
});

export const Default = Template.bind({});
Default.args = {};
