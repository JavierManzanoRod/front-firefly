import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { BsModalRef, BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { ModalStopComponent } from './modal-stop.component';

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Component({
  selector: 'ff-modal-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockModalHeaderComponent {
  @Input() loading!: boolean;
  @Input() item: any;
  @Input() entityType: any;
  @Input() entityTypesRef: any;
}

@Component({
  selector: 'ff-modal-footer',
  template: '<p>Mock Product Editor Component</p>',
})
class MockModalFooterComponent {
  @Input() loading!: boolean;
  @Input() item: any;
  @Input() entityType: any;
  @Input() entityTypesRef: any;
}

describe('ModalStopComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TranslatePipeMock, MockModalHeaderComponent, MockModalFooterComponent, ModalStopComponent],
        imports: [ModalModule.forRoot()],
        providers: [BsModalService, BsModalRef],
      }).compileComponents();
    })
  );
  it('ModalStopComponent should create ', () => {
    const fixture = TestBed.createComponent(ModalStopComponent);
    expect(fixture).toBeDefined();
  });
});
