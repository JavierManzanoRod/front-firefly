import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UICommonModalConfig } from './modal-warning.model';

@Component({
  selector: 'ff-modal-warning',
  templateUrl: './modal-warning.component.html',
  styleUrls: ['./modal-warning.component.scss'],
})
export class ModalWarningComponent {
  @Input() config: UICommonModalConfig = {};
  @Output() confirm: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public modalRef: BsModalRef, public modalService: BsModalService) {
    this.enableModal();
  }

  handleCancel() {
    this.confirm.emit(false);
    this.closeModal();
  }

  handleConfirm() {
    this.confirm.emit(true);
    this.closeModal();
  }

  private closeModal() {
    this.modalRef.hide();
  }

  private disableModal() {
    this.modalService.config.ignoreBackdropClick = true;
    this.modalService.config.keyboard = false;
    this.modalService.config.focus = false;
  }

  private enableModal() {
    this.modalService.config.ignoreBackdropClick = false;
    this.modalService.config.keyboard = true;
    this.modalService.config.focus = true;
  }
}
