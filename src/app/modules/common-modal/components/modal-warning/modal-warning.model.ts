// @Input() title: string;
// @Input() bodyMessage: string;
// @Input() questionMessage: string;
// @Input() okButtonText: string;
// @Input() cancelButtonText: string;

interface UICommonModalConfig1 {
  title: string;
  errorMessage: string;
  bodyMessage: string;
  questionMessage: string;
  okButtonText: string;
  cancelButtonText: string;
  name: string;
}

export type UICommonModalConfig = Readonly<Partial<UICommonModalConfig1>>;
