import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ff-modal-footer',
  template: `<footer class="modal-footer">
    <ng-content></ng-content>
  </footer> `,
  styleUrls: ['./modal-footer.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalFooterComponent {}
