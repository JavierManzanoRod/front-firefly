import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ff-modal-header',
  template: `<header class="modal-header">
    <div class="modal-title">
      <ng-container *ngIf="title">{{ title }}</ng-container>
    </div>

    <button (click)="onClose()" type="button" class="close">
      <em class="icon-cancel"></em>
    </button>
  </header> `,
  styleUrls: ['modal-header.component.scss'],
})
export class ModalHeaderComponent {
  @Input() title!: string;
  @Output() closeMe = new EventEmitter();

  onClose() {
    this.closeMe.emit(true);
  }
}
