import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { MayHaveIdName } from '@model/base-api.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
  selector: 'ff-modal-clone',
  templateUrl: './modal-clone.component.html',
  styleUrls: ['./modal-clone.component.scss'],
})
export class ModalCloneComponent implements OnChanges {
  @Input() item!: MayHaveIdName;
  @Input() loading!: boolean;
  @Input() loading$: Subject<boolean>;
  @Input() errorMessage!: string;
  @Input() error409!: { error: boolean; message: string };

  @Output() cancel: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() confirm: EventEmitter<boolean> = new EventEmitter<boolean>();

  name = '';
  isMessageError = false;

  constructor(public modalRef: BsModalRef, public modalService: BsModalService) {
    this.enableModal();

    this.loading$ = new Subject<boolean>();
    this.loading$.subscribe((isLoading) => {
      if (isLoading) {
        this.disableModal();
        modalService.config.ignoreBackdropClick = true;
      } else {
        this.enableModal();
      }
    });
  }

  ngOnChanges() {
    this.item.name = this.name;
  }

  onChangeName(e: any) {
    this.name = e.target.value;
    this.item.name = e.target.value;
    this.isMessageError = !this.name;
  }

  closeModal() {
    this.modalRef.hide();
  }

  handleCancel() {
    if (this.eventExist(this.cancel)) {
      this.cancel.emit(true);
    } else {
      this.closeModal();
    }
  }

  handleConfirm() {
    if (this.eventExist(this.confirm)) {
      if (this.name) {
        this.confirm.emit(true);
      } else {
        this.isMessageError = true;
      }
    } else {
      this.closeModal();
    }
  }

  private eventExist(event: EventEmitter<boolean>): boolean {
    return event.observers.length > 0;
  }

  private disableModal() {
    this.modalService.config.ignoreBackdropClick = true;
    this.modalService.config.keyboard = false;
    this.modalService.config.focus = false;
  }

  private enableModal() {
    this.modalService.config.ignoreBackdropClick = false;
    this.modalService.config.keyboard = true;
    this.modalService.config.focus = true;
  }
}
