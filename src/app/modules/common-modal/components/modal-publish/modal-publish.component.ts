import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MayHaveIdName } from '@model/base-api.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
  selector: 'ff-modal-publish',
  templateUrl: './modal-publish.component.html',
  styleUrls: ['./modal-publish.component.scss'],
})
export class ModalPublishComponent {
  @Input() item!: MayHaveIdName;
  @Input() loading!: boolean;
  @Input() loading$: Subject<boolean>;
  @Input() errorMessage!: string;
  @Input() error409!: { error: boolean; message: string };

  @Output() cancel: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() confirm: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public modalRef: BsModalRef, public modalService: BsModalService) {
    this.enableModal();

    this.loading$ = new Subject<boolean>();
    this.loading$.subscribe((isLoading) => {
      if (isLoading) {
        this.disableModal();
        modalService.config.ignoreBackdropClick = true;
      } else {
        this.enableModal();
      }
    });
  }

  closeModal() {
    this.modalRef.hide();
  }

  handleCancel() {
    if (this.eventExist(this.cancel)) {
      this.cancel.emit(true);
    } else {
      this.closeModal();
    }
  }

  handleConfirm() {
    if (this.eventExist(this.confirm)) {
      this.confirm.emit(true);
    } else {
      this.closeModal();
    }
  }

  private eventExist(event: EventEmitter<boolean>): boolean {
    return event.observers.length > 0;
  }

  private disableModal() {
    this.modalService.config.ignoreBackdropClick = true;
    this.modalService.config.keyboard = false;
    this.modalService.config.focus = false;
  }

  private enableModal() {
    this.modalService.config.ignoreBackdropClick = false;
    this.modalService.config.keyboard = true;
    this.modalService.config.focus = true;
  }
}
