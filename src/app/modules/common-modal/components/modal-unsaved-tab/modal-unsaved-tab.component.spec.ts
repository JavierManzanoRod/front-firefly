import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalUnsavedTabComponent } from './modal-unsaved-tab.component';

xdescribe('ModalUnsavedTabComponent', () => {
  let component: ModalUnsavedTabComponent;
  let fixture: ComponentFixture<ModalUnsavedTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalUnsavedTabComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalUnsavedTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
