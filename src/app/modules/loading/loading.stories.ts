import { moduleMetadata, Story, Meta } from '@storybook/angular';

import { CommonModule } from '@angular/common';

import { LoadingComponent } from './loading.component';

export default {
  component: LoadingComponent,
  decorators: [
    moduleMetadata({
      declarations: [LoadingComponent],
      imports: [CommonModule],
    }),
  ],
  excludeStories: /.*Data$/,
  title: 'LoadingComponent',
  parameters: {
    docs: {
      description: {
        component: 'Llamar al modulo LoadingModule para el uso del loading.',
      },
    },
  },
} as Meta;

const Template: Story<LoadingComponent> = (args) => ({
  props: {
    ...args,
  },
});

export const Default = Template.bind({});
Default.args = {
  loading: true,
  overlay: false,
};

export const Overlayed = Template.bind({});
Overlayed.args = {
  loading: true,
  overlay: true,
};

export const Empty = Template.bind({});
Empty.args = {
  loading: false,
  overlay: false,
};
