import { Component, Input } from '@angular/core';

@Component({
  selector: 'ff-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
})
export class LoadingComponent {
  @Input() loading!: boolean;
  @Input() overlay!: boolean;

  constructor() {}
}
