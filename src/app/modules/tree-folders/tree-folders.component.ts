import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ContentGroupTreeEvents } from 'src/app/catalog/content-group/model/content-group-tree.models';
import { TreeFolders } from './models/tree-folders.model';
import { TreeFoldersStateService } from './services/tree-folders-state.service';

// COMPONENTE DEPRECADO POR EL USO DE EXPLODDED-TREE EN CONTENT-GROUP Y AGRUPACIONES DE PRODUCT
// DE AQUÍ RECOGEMOS EL SERVICIO Y EL MODELO, QUE SE PUEDEN EXPORTAR A OTRO COMPONENTE EN SHARED PARA TERMINAR DE LIMPIAR ESTE CÓDIGO.
@Component({
  selector: 'ff-tree-folders',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './tree-folders.component.html',
  styleUrls: ['./tree-folders.component.scss'],
})
export class TreeFoldersComponent implements OnInit {
  @Input() folders: TreeFolders | null = null;
  @Input() loading = false;
  @Input() link = '/catalog/content-group';
  @Input() linkCategoryRule = '/rule-engine/category-rule/';
  @Input() type: 'contentGroup' | 'categoryFolder' = 'contentGroup';
  @Output() clickRule = new EventEmitter<{ route: string; id: string }>();
  @Output() clickFolder = new EventEmitter<string>();
  @Output() deleteRule = new EventEmitter<TreeFolders>();
  searchModel = '';

  constructor(public state: TreeFoldersStateService, private route: Router) {}

  ngOnInit(): void {}

  search() {}

  createFolder() {
    const typeLink = `${this.type === 'categoryFolder' ? this.linkCategoryRule : this.link}/folder/new`;
    this.route.navigate([typeLink]).then(() => {
      setTimeout(() => {
        this.state.eventsTree.next({
          parent: null,
          event: ContentGroupTreeEvents.refreshFolder,
        });
      }, 200);
    });
  }
}
