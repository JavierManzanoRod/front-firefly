import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { ContentGroupService } from 'src/app/catalog/content-group/services/content-group.service';
import { TreeFoldersStateService } from './services/tree-folders-state.service';
import { TreeFoldersComponent } from './tree-folders.component';

describe('TreeFoldersComponent', () => {
  let component: TreeFoldersComponent;
  let fixture: ComponentFixture<TreeFoldersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TreeFoldersComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: ContentGroupService,
          useFactory: () => ({}),
        },
        {
          provide: TreeFoldersStateService,
          useFactory: () => ({}),
        },
        {
          provide: Router,
          useFactory: () => ({
            navigate: () => {
              return of(null);
            },
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeFoldersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
