import { ContentGroupAdminGroup } from 'src/app/catalog/content-group/model/content-group.models';

export interface TreeFolders {
  name: string;
  id: string;
  identifier?: string;
  category_rules?: CategoryRulesTree[];
  folders?: TreeFolders[];
  admin_groups?: ContentGroupAdminGroup[];
  expanded?: boolean;
  root?: boolean;
  tempFolder?: boolean;
  route?: string;
}

export interface CategoryRulesTree {
  identifier: string;
  name: string;
  id: string;
}
