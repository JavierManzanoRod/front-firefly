import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ContentGroupTree, ContentGroupTreeDataEvents } from '../../../catalog/content-group/model/content-group-tree.models';
import { ContentGroupsParams } from '../../../catalog/content-group/model/content-group.models';

@Injectable()
export class TreeFoldersStateService {
  eventsTree = new Subject<ContentGroupTreeDataEvents>();
  idItemActive: string | null | undefined = '';
  subjectItemActive = new Subject<ContentGroupTree | null>();
  params: ContentGroupsParams | null = null;
  routeId = '';
}
