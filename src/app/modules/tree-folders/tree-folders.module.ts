import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { FormErrorModule } from '../form-error/form-error.module';
import { TreeFoldersComponent } from './tree-folders.component';

@NgModule({
  declarations: [TreeFoldersComponent],
  imports: [CommonModule, TranslateModule, FormsModule, FormErrorModule, ReactiveFormsModule, SimplebarAngularModule],
  exports: [TreeFoldersComponent],
})
export class TreeFoldersModule {}
