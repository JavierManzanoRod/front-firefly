import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { SimplebarAngularModule } from 'simplebar-angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModalModule } from '../common-modal/common-modal.module';
import { DatetimepickerModule } from '../datetimepicker/datetimepicker.module';
import { ExplodedTreeModule } from '../exploded-tree/exploded-tree.module';
import { FieldI18nModule } from '../field-i18n/field-i18n.module';
import { FormErrorModule } from '../form-error/form-error.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { UiTreeListModule } from '../ui-tree-list/ui-tree-list.module';
import { UiModalSelectComponent } from './components/modal-select/modal-select.component';
import { UiListSelectOptionsComponent } from './components/ui-list-select-options/ui-list-select-options.component';
import { UiModalI18nTranslateComponent } from './components/ui-modal-i18n-translate/ui-modal-i18n-translate.component';
import { UiModalI18nComponent } from './components/ui-modal-i18n/ui-modal-i18n.component';
import { UiModalInfoComponent } from './components/ui-modal-info/ui-modal-info.component';
import { UiModalKeyValueExpertsComponent } from './components/ui-modal-key-value-experts/ui-modal-key-value-experts.component';
import { UiModalKeyValueComponent } from './components/ui-modal-key-value/ui-modal-key-value.component';
import { UiModalRangeComponent } from './components/ui-modal-range/ui-modal-range.component';
import { UiConditionsModalComponent } from './components/ui-modal-rules-preview/ui-conditions-modal/ui-conditions-modal.component';
import { UiModalRulesPreviewComponent } from './components/ui-modal-rules-preview/ui-modal-rules-preview.component';
import { UiModalSelectDateComponent } from './components/ui-modal-select-date/ui-modal-select-date.component';
import { UiModalSelectProductsComponent } from './components/ui-modal-select-products/ui-modal-select-products.component';
import { UiModalSelectRulesComponent } from './components/ui-modal-select-rules/ui-modal-select-rules.component';
import { UiModalSelectSubsitesComponent } from './components/ui-modal-select-subsites/ui-modal-select-subsite.component';
import { UiModalSelectTextsComponent } from './components/ui-modal-select-texts/ui-modal-select-texts.component';
import { UiModalSelectTreeFolderComponent } from './components/ui-modal-select-tree-folder/ui-modal-select-tree-folder.component';
import { UiModalSelectTreeComponent } from './components/ui-modal-select-tree/ui-modal-select-tree.component';
import { UiModalSelectComponent as NewUiModalSelectComponent } from './components/ui-modal-select/ui-modal-select.component';
import { UiModalSitesPreviewComponent } from './components/ui-modal-sites-preview/ui-modal-sites-preview.component';

@NgModule({
  declarations: [
    UiModalSelectComponent,
    UiListSelectOptionsComponent,
    UiModalSelectTextsComponent,
    UiModalSelectDateComponent,
    NewUiModalSelectComponent,
    UiModalSelectRulesComponent,
    UiModalI18nComponent,
    UiModalKeyValueComponent,
    UiModalRangeComponent,
    UiModalKeyValueExpertsComponent,
    UiModalInfoComponent,
    UiModalI18nTranslateComponent,
    UiModalRulesPreviewComponent,
    UiModalSelectProductsComponent,
    UiConditionsModalComponent,
    UiModalSelectSubsitesComponent,
    UiModalSelectTreeComponent,
    UiModalSitesPreviewComponent,
    UiModalSelectTreeFolderComponent,
  ],
  imports: [
    ModalModule.forRoot(),
    CommonModule,
    CommonModalModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    FieldI18nModule,
    DatetimepickerModule,
    SimplebarAngularModule,
    UiTreeListModule,
    NgSelectModule,
    FormErrorModule,
    TooltipModule,
    ExplodedTreeModule,
    PipesModule,
  ],
  exports: [UiModalRulesPreviewComponent, UiModalSitesPreviewComponent],
})
export class UiModalSelectModule {}
