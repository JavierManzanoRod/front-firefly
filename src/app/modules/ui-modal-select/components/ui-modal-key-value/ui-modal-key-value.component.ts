import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UIModalKeyValue } from '../../models/modal-select.model';

@Component({
  selector: 'ff-ui-modal-key-value',
  templateUrl: './ui-modal-key-value.component.html',
  styleUrls: ['./ui-modal-key-value.component.scss'],
})
export class UiModalKeyValueComponent implements OnInit {
  @Output() selectItems: EventEmitter<{ [key: string]: string }> = new EventEmitter<{
    [key: string]: string;
  }>();
  @Output() cancel: EventEmitter<{ [key: string]: string }> = new EventEmitter<{
    [key: string]: string;
  }>();
  @Input() config!: UIModalKeyValue;

  items: { key: string; value: string }[] = [];

  get objectValue() {
    const result: any = {};
    this.items.forEach((item) => {
      if (item.key && item.key.trim() !== '' && item.value && item.value.trim() !== '') {
        result[item.key] = item.value;
      }
    });
    return result;
  }

  constructor(public modalRef: BsModalRef) {}

  ngOnInit() {
    this.items = this.config.items ? this.getArrayFromObject(JSON.parse(JSON.stringify(this.config.items))) : [];
  }

  add() {
    this.items.push({ key: '', value: '' });
  }

  getArrayFromObject(object: { [key: string]: string }) {
    const result: any[] = [];
    Object.keys(object).forEach((key) => {
      result.push({ key, value: object[key] });
    });
    return result;
  }

  handleDone() {
    this.selectItems.emit(this.objectValue);
    this.modalRef.hide();
  }

  handleCancel() {
    this.cancel.emit(this.config.items ? this.config.items : {});
    this.modalRef.hide();
  }
}
