import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UIModalSelectTextsView } from '../../models/modal-select.model';
import { DatetimepickerComponent } from './../../../datetimepicker/datetimepicker.component';

@Component({
  selector: 'ff-ui-modal-select-date',
  templateUrl: './ui-modal-select-date.component.html',
  styleUrls: ['./ui-modal-select-date.component.scss'],
})
export class UiModalSelectDateComponent implements OnInit {
  @ViewChild('newTextElement') newTextElement!: DatetimepickerComponent;
  @Output() selectItems: EventEmitter<string[]> = new EventEmitter<string[]>();
  @Output() cancel: EventEmitter<string[]> = new EventEmitter<string[]>();
  @Input() config!: UIModalSelectTextsView;

  items: string[] = [];
  itemsEditing: any = {};
  editingForms: any = {};

  canShowFormError = false;

  form!: FormGroup;

  constructor(public modalRef: BsModalRef, private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      newText: [null, this.config.inputValidators ? this.config.inputValidators : null],
    });
    this.form.valueChanges.subscribe(() => {
      this.canShowFormError = false;
    });
    if (this.config.items) {
      this.items = JSON.parse(JSON.stringify(this.config.items));
    }
  }

  handleDone() {
    this.selectItems.emit(this.items);
    this.modalRef.hide();
  }

  handleCancel() {
    this.cancel.emit(this.config.items ? this.config.items : []);
    this.modalRef.hide();
  }

  addText() {
    this.canShowFormError = true;
    if (this.form.valid && this.isValidDate(this.form.value.newText)) {
      if (this.newTextElement.dateTimeModel) {
        if (this.items.indexOf(this.newTextElement.dateTimeModel.toISOString()) < 0) {
          this.items.push(this.newTextElement.dateTimeModel.toISOString());
        }
        this.newTextElement.dateTimeModel = null;
        this.form.controls.newText.setValue('');
      }
    }
  }

  isValidDate(d: any) {
    return !isNaN((d as Date).getTime());
  }

  editItem(item: any) {
    this.itemsEditing[item] = item;
    this.editingForms[item] = this.fb.group({
      text: [item, this.config.inputValidators ? this.config.inputValidators : null],
    });
    setTimeout(() => {
      const input = document.getElementById(`id ${item}`);
      if (input) {
        input.focus();
      }
    }, 100);
  }

  removeItem(item: any) {
    const index = this.items.indexOf(item);
    if (index >= 0) {
      this.items.splice(index, 1);
    }
  }

  removeAll() {
    this.items = [];
  }
}
