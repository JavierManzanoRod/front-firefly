import { UiListSelectOptionsComponent } from './ui-list-select-options.component';

describe('UiListSelectOptionsComponent', () => {
  it('it should create ', () => {
    const component = new UiListSelectOptionsComponent();
    expect(component).toBeDefined();
  });

  it('selectRow emits the value selected', () => {
    const item = {
      id: '1',
    };
    const component = new UiListSelectOptionsComponent();
    component.selected.subscribe(() => {
      expect(true).toBeTruthy();
    });
    component.selectRow(item);
  });
});
