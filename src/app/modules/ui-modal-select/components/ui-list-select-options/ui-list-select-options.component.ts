import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MayHaveLabel } from '@model/base-api.model';

@Component({
  selector: 'ff-ui-list-select-options',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './ui-list-select-options.component.html',
  styleUrls: ['./ui-list-select-options.component.scss'],
})
export class UiListSelectOptionsComponent {
  @Input() list!: MayHaveLabel;
  @Output() selected: EventEmitter<MayHaveLabel> = new EventEmitter<MayHaveLabel>();

  constructor() {}

  selectRow(item: MayHaveLabel) {
    this.selected.emit(item);
  }

  identify(index: number, item: any) {
    return item.id;
  }
}
