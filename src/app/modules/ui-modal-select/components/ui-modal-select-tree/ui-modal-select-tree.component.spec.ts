import { FormBuilder } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { DestroyService } from 'src/app/shared/services/destroy.service';
import { UiModalSelectTreeComponent } from './ui-modal-select-tree.component';

describe('ConditionsBasicModalSelectComponent', () => {
  it('it should create ', () => {
    const component = new UiModalSelectTreeComponent(
      (null as unknown) as BsModalRef,
      new FormBuilder(),
      (null as unknown) as ToastService,
      (null as unknown) as DestroyService
    );
    expect(component).toBeDefined();
  });
});
