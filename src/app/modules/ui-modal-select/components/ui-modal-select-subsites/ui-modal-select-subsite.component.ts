import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Site, Subsite } from 'src/app/sites/site/models/sites.model';
import { SiteService } from 'src/app/sites/site/services/site.service';
import { SubsiteService } from 'src/app/sites/subsite/services/subsite.service';
import { UiTreeClickEvent, UITreeListItem } from '../../../ui-tree-list/models/ui-tree-list.model';
import { UiTreeListComponent } from '../../../ui-tree-list/ui-tree-list.component';
import { UIModalSelectSubsites } from '../../models/modal-select.model';

export interface SubsiteWithSite {
  site: string;
  subsite: Subsite;
}

@Component({
  selector: 'ff-ui-modal-select-subsites',
  templateUrl: './ui-modal-select-subsites.component.html',
  styleUrls: ['./ui-modal-select-subsites.component.scss'],
})
export class UiModalSelectSubsitesComponent implements OnInit {
  @ViewChild(UiTreeListComponent) uiTree!: UiTreeListComponent<any>;
  @Output() selectedEvent: EventEmitter<SubsiteWithSite[]> = new EventEmitter<SubsiteWithSite[]>();
  @Output() canceledEvent: EventEmitter<Subsite[]> = new EventEmitter<Subsite[]>();
  @Input() config!: UIModalSelectSubsites;

  loadingSearch = false;
  selectedSubsites: Subsite[] = [];
  selectedSubsitesValues: string[] = [];
  showNoResults = false;
  items: UITreeListItem[] = [];

  subsiteWithSite: SubsiteWithSite[] = [];

  loading = true;

  constructor(
    public modalRef: BsModalRef,
    private siteService: SiteService,
    private subsiteService: SubsiteService,
    private ref: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.loading = true;
    this.siteService.list({ size: 10000 }).subscribe((response) => {
      this.items = response.content.map((site: Site) => {
        // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
        site.name = '(' + site.total_subsites + ') ' + site.name;
        return {
          value: site,
          icon: 'icon-sites',
          loadChildrens: (loadSite: Site) => this.loadSubsites(loadSite),
        };
      }) as UITreeListItem[];
      this.loading = false;
    });
    if (this.isSiteSubsite(this.config.selectedSubsites)) {
      this.subsiteWithSite = this.config.selectedSubsites;
      const subsites: Subsite[] = [];
      this.config.selectedSubsites.forEach((item) => {
        subsites.push(item.subsite);
      });
      this.selectedSubsites = JSON.parse(JSON.stringify(subsites));
    } else {
      this.selectedSubsites = JSON.parse(JSON.stringify(this.config.selectedSubsites));
    }
    this.updateSelectedValues();
  }

  loadSubsites(site: Site): Observable<UITreeListItem[]> {
    return this.subsiteService.list(site).pipe(
      map((response) => {
        return response.content.map((subsite: Subsite) => {
          return {
            value: subsite,
            icon: 'icon-flow-tree-subsite',
          };
        });
      })
    );
  }

  handleDone() {
    this.selectedEvent.emit(this.subsiteWithSite);
    this.modalRef.hide();
  }

  handleCancel() {
    this.canceledEvent.emit(this.config.selectedSubsites);
    this.modalRef.hide();
  }

  filterItems(event: any) {
    const text = event.target.value.toLowerCase();
    this.items.forEach((item) => {
      item.hide = text ? item.value.name.toLowerCase().indexOf(text) < 0 : false;
    });
  }

  updateSelectedValues() {
    this.selectedSubsitesValues = this.selectedSubsites.map((subsite: Subsite) => {
      return subsite.id || '';
    });
  }

  removeAll() {
    this.selectedSubsites = [];
    this.subsiteWithSite = [];
    this.updateSelectedValues();
  }

  orderAZ() {
    this.selectedSubsites.sort((a, b) => {
      if (this.getItemValue(a) > this.getItemValue(b)) {
        return 1;
      }
      if (this.getItemValue(a) < this.getItemValue(b)) {
        return -1;
      }
      return 0;
    });
    this.updateSelectedValues();
  }

  getItemValue(item: any) {
    return item.name;
  }

  getSiteSubsite() {}

  removeSelected(item: any) {
    const index = this.selectedSubsitesValues.indexOf(item.id);
    if (index >= 0) {
      this.selectedSubsites.splice(index, 1);
      this.subsiteWithSite.splice(index, 1);
    }
    this.updateSelectedValues();
  }

  onClickedElement(event: UiTreeClickEvent<Subsite>) {
    if (event.path.length === 2) {
      const index = this.selectedSubsitesValues.indexOf(event.selectedItem.id || '');
      if (index < 0) {
        this.selectedSubsites.push(event.selectedItem);
        const siteName = event.path[0].item.name?.substring(4);
        this.subsiteWithSite.push({ site: siteName || '', subsite: event.selectedItem });
        this.updateSelectedValues();
      }
    }
  }

  isSiteSubsite(value: any): value is SubsiteWithSite {
    return value;
  }
}
