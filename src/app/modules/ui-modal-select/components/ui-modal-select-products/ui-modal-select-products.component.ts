import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UIModalSelectProducts } from '../../models/modal-select.model';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'ff-ui-modal-select-products',
  templateUrl: './ui-modal-select-products.component.html',
  styleUrls: ['./ui-modal-select-products.component.scss'],
})
export class UiModalSelectProductsComponent implements OnInit {
  @Output() selectItems: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Output() cancel: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Input() config!: UIModalSelectProducts;

  items: any[] = [];
  selectedItems: any[] = [];
  selectedItemIds: string[] = [];

  constructor(public modalRef: BsModalRef) {}

  ngOnInit() {
    this.selectedItems = this.config.selectedProducts ? this.config.selectedProducts : [];
    this.selectedItemIds = this.selectedItems.map((item: { id: string }) => item.id);
    this.getBackendItems().subscribe((items) => {
      this.items = items;
    });
  }

  getBackendItems() {
    const items = [];
    for (let i = 0, l = 20; i < l; i++) {
      items.push({
        id: i,
        name: `Producto ${i}`,
      });
    }
    return of(items).pipe(delay(500));
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  filterItems(event: any) {}

  toggleAll() {
    if (this.selectedItems.length === this.items.length) {
      this.selectedItems = [];
      this.selectedItemIds = [];
    } else {
      this.selectedItems = JSON.parse(JSON.stringify(this.items));
      this.selectedItemIds = this.items.map((item: { id: string }) => item.id);
    }
  }

  selectItem(item: any) {
    const index = this.selectedItemIds.indexOf(item.id);
    if (index >= 0) {
      this.selectedItems.splice(index, 1);
      this.selectedItemIds.splice(index, 1);
    } else {
      this.selectedItems.push(item);
      this.selectedItemIds.push(item.id);
    }
  }

  handleDone() {
    this.selectItems.emit(this.selectedItems);
    this.modalRef.hide();
  }

  handleCancel() {
    this.cancel.emit(this.config.selectedProducts ? this.config.selectedProducts : []);
    this.modalRef.hide();
  }
}
