import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { FormGroup, ValidatorFn } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { fromEvent } from 'rxjs';
import { FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { UIModalRange } from '../../models/modal-select.model';
import { isOptionalRange, isRange, requireOneRange } from './ui-moda-range.validator';

@Component({
  selector: 'ff-ui-modal-range',
  templateUrl: './ui-modal-range.component.html',
  styleUrls: ['./ui-modal-range.component.scss'],
})
export class UiModalRangeComponent implements OnInit, AfterViewInit {
  @Input() config!: UIModalRange;

  @Output() selectItems = new EventEmitter<any>();
  @Output() cancel = new EventEmitter<any>();

  @ViewChild('input', { read: ElementRef }) input!: ElementRef<HTMLInputElement>;
  @ViewChildren('from', { read: ElementRef }) from!: QueryList<ElementRef<HTMLInputElement>>;
  @ViewChildren('to', { read: ElementRef }) to!: QueryList<ElementRef<HTMLInputElement>>;
  @ViewChild(NgSelectComponent) select!: NgSelectComponent;
  form!: FormGroup;
  submit = false;
  formErrors: any = {};
  options: { key: string; value: string }[] = [];

  get selectMultipleItems() {
    if (this.config?.selectOptions) {
      return Object.keys(this.config?.selectOptions).reduce<{ [key: string]: string }>((items: { [key: string]: string }, key: string) => {
        if (
          this.config?.selectOptions &&
          (!this.config?.selectMultiple || !((this.form.get('select')?.value || []) as string[]).includes(key))
        ) {
          items[key] = this.config?.selectOptions[key];
        }

        return items;
      }, {} as { [key: string]: string });
    }
    return {};
  }

  get required(): boolean {
    let required = false;
    this.config.ranges?.forEach((range) => {
      if (!required) {
        required = !!this.form.get(range.key)?.errors?.required;
      }
    });
    return required;
  }

  constructor(private modalRef: BsModalRef) {}

  ngOnInit() {
    const headerConfigForm = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      select: {
        value: null,
        validators: { required: true },
      },
    });

    this.form = headerConfigForm.form;
    this.formErrors = headerConfigForm.errors;

    this.config.ranges?.forEach((range) => {
      const group = new FormGroup(
        FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
          from: {
            value: null,
            updateOn: 'blur',
          },
          to: {
            value: null,
          },
        }).form.controls
      );

      group.setValidators((range.autcomplete ? isRange : isOptionalRange) as ValidatorFn);

      this.form.addControl(range.key, group);
    });

    this.form.setValidators(requireOneRange as ValidatorFn);

    if (!this.config.selectOptions) {
      this.form.controls.select.clearValidators();
    }

    if (this.config.value) {
      this.form.patchValue(JSON.parse(JSON.stringify(this.config.value[0])));
    }
  }

  ngAfterViewInit(): void {
    this.from.forEach(({ nativeElement }, index) => {
      const config = this.config.ranges && this.config.ranges[index];

      if (config) {
        const form = this.form.get(config.key) as FormGroup;
        fromEvent(nativeElement, 'blur').subscribe(() => {
          if (nativeElement.value !== '') {
            let value = nativeElement.value || 0;

            if (config.max !== undefined && value > config.max) {
              form.patchValue({ from: (value = +config.max) }, { emitEvent: false });
            }
            if (config.min !== undefined && value < config.min) {
              form.patchValue({ from: (value = +config.min) }, { emitEvent: false });
            }

            if (!value) {
              form.patchValue({ from: 0 }, { emitEvent: false });
            }
            if (!form.value.to && value !== '' && config.autcomplete) {
              form.patchValue({ to: +value }, { emitEvent: false });
            }
          }
        });
      }
    });

    this.to.forEach(({ nativeElement }, index) => {
      const config = this.config.ranges && this.config.ranges[index];
      if (config) {
        const form = this.form.get(config.key) as FormGroup;

        fromEvent(nativeElement, 'blur').subscribe(() => {
          if (nativeElement.value !== '') {
            let value = nativeElement.value || 0;

            if (!form.controls.from.value) {
              form.patchValue({ from: 0 }, { emitEvent: false });
            }
            if (config.max !== undefined && value > config.max) {
              form.patchValue({ to: (value = +config.max) }, { emitEvent: false });
            }
            if (config.min !== undefined && value < config.min) {
              form.patchValue({ to: (value = +config.min) }, { emitEvent: false });
            }
          }
        });
      }
    });
  }

  handleDone() {
    this.submit = true;

    Object.keys(this.form.controls).forEach((key) => {
      this.form.get(key)?.markAllAsTouched();
      this.form.get(key)?.updateValueAndValidity();
    });

    if (this.form.valid) {
      this.selectItems.emit(JSON.parse(JSON.stringify([this.form.value])));
      this.modalRef.hide();
    }
  }

  handleCancel() {
    // this.cancel.emit(this.config.value ? this.config.value : undefined);
    this.modalRef.hide();
  }

  change(text: string | HTMLInputElement, event?: Event) {
    if (text && this.config.selectMultiple) {
      let value = text;
      if (text instanceof HTMLInputElement) {
        value = text.value;
        text.value = '';
        text.focus();
      }

      if (event) {
        event.preventDefault();
        event.stopPropagation();
      }

      if (!value) {
        return;
      }

      const values: string[] = this.form.get('select')?.value || [];
      values.push(value as string);
      this.form.get('select')?.setValue(values);

      if (this.select) {
        this.select.handleClearClick();
      }
    }
  }

  remove(item: string) {
    const values: string[] = this.form.get('select')?.value || [];

    if (values.length) {
      const index = values.findIndex((val) => val === item);
      values.splice(index, 1);
      this.form.get('select')?.setValue(values);
    }
  }
}
