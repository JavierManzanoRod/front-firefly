import { FormGroup } from '@angular/forms';

function validator(group: FormGroup, optional?: boolean) {
  const values = group.value;
  const keys = Object.keys(values);

  const error =
    group.controls[keys[0]].touched &&
    group.controls[keys[1]].touched &&
    (!optional || values[keys[1]] !== null) &&
    +values[keys[0]] > +values[keys[1]];

  group.controls[keys[0]].setErrors(error ? { range: true } : null);

  if (!optional || values[keys[1]] !== null) {
    group.controls[keys[1]].setErrors(error ? { range: true } : null);
  }

  return null;
}

export function isRange(group: FormGroup) {
  return validator(group);
}

export function isOptionalRange(group: FormGroup) {
  return validator(group, true);
}

export function requireOneRange(group: FormGroup) {
  const values = group.value;
  const keys = Object.keys(values);
  let error = true;
  let tocuehd = false;

  keys
    .filter((key) => key !== 'select')
    .forEach((key) => {
      if (!tocuehd && group.controls[key].touched) {
        tocuehd = true;
      }

      if (error) {
        if (values[key].from !== null || values[key].to !== null) {
          error = false;
        }
      }
    });

  keys
    .filter((key) => key !== 'select')
    .forEach((key) => {
      group.controls[key].setErrors(tocuehd && error ? { required: true } : null);
    });
}
