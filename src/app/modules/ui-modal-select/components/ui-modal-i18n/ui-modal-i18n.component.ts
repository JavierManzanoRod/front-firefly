import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FF_LANGUAGES } from 'src/app/configuration/tokens/language.token';
import { UIModalI18N } from '../../models/modal-select.model';

@Component({
  selector: 'ff-ui-modal-i18n',
  templateUrl: './ui-modal-i18n.component.html',
  styleUrls: ['./ui-modal-i18n.component.scss'],
})
export class UiModalI18nComponent implements OnInit {
  @Output() selectItems: EventEmitter<{ [key: string]: string }> = new EventEmitter<{
    [key: string]: string;
  }>();
  @Output() cancel: EventEmitter<{ [key: string]: string }> = new EventEmitter<{
    [key: string]: string;
  }>();
  @Input() config: UIModalI18N | undefined;
  @Input() validatorEs = false;

  firstLanguage = '';
  showNeedFirstLanguageError = false;

  value: { [key: string]: string } = {};

  constructor(
    public modalRef: BsModalRef,
    private readonly translateService: TranslateService,
    @Inject(FF_LANGUAGES) public languages: string[]
  ) {}

  ngOnInit() {
    this.firstLanguage = this.translateService.instant('LANGUAGES.' + this.languages[0]);
    this.value = this.config?.items ? JSON.parse(JSON.stringify(this.config.items)) : {};
  }

  handleDone() {
    this.showNeedFirstLanguageError = false;
    if (this.value[this.languages[0]]) {
      const result: any = {};
      Object.keys(this.value).forEach((key) => {
        if (this.value[key] && this.value[key].trim() !== '') {
          result[key] = this.value[key];
        }
      });
      this.selectItems.emit(result);
      if (!this.validatorEs || this.value[this.languages[0]]) {
        this.modalRef.hide();
      }
    } else {
      this.showNeedFirstLanguageError = true;
    }
  }

  handleCancel() {
    this.cancel.emit(this.config?.items ? this.config.items : {});
    this.modalRef.hide();
  }
}
