import { ToastService } from './../../../toast/toast.service';
/* eslint-disable @typescript-eslint/no-unsafe-call */
import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { deepEquals } from 'src/app/shared/utils/utils';
import { UIModalSelectTextsView } from '../../models/modal-select.model';
import { downloadFile, writeCsv } from 'src/app/shared/utils/utilsCsv';

@Component({
  selector: 'ff-ui-modal-select-texts',
  templateUrl: './ui-modal-select-texts.component.html',
  styleUrls: ['./ui-modal-select-texts.component.scss'],
})
export class UiModalSelectTextsComponent implements OnInit, AfterViewInit {
  @ViewChild('newTextElement') newTextElement!: ElementRef;
  @Output() selectItems: EventEmitter<string[]> = new EventEmitter<string[]>();
  @Output() cancel: EventEmitter<string[]> = new EventEmitter<string[]>();
  @Input() config!: UIModalSelectTextsView;

  items: string[] = [];
  itemsEditing: any = {};
  editingForms: any = {};

  canShowFormError = false;
  textNoSave = false;

  form!: FormGroup;
  itemsError: any[] = [];
  loading = false;

  constructor(public modalRef: BsModalRef, private fb: FormBuilder, private toastService: ToastService) {}

  ngOnInit() {
    this.form = this.fb.group({
      newText: [null, this.config.inputValidators ? this.config.inputValidators : null],
    });
    this.form.valueChanges.subscribe(() => {
      this.canShowFormError = false;
    });
    if (this.config.items) {
      this.items = JSON.parse(JSON.stringify(this.config.items));
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.newTextElement.nativeElement.focus();
    });
  }

  handleDone() {
    if (this.newTextElement.nativeElement.value && this.newTextElement.nativeElement.value !== '') {
      this.textNoSave = true;
    } else {
      this.selectItems.emit(this.items);
      this.modalRef.hide();
    }
  }

  get haveChange() {
    return !deepEquals(this.items, this.config.items);
  }

  handleCancel() {
    // this.cancel.emit(this.config.items ? this.config.items : []);
    this.modalRef.hide();
  }

  addText() {
    this.canShowFormError = true;
    if (this.form.valid) {
      if (this.newTextElement.nativeElement.value) {
        const clearedNewText = this.newTextElement.nativeElement.value;
        if (this.items.indexOf(clearedNewText) < 0) {
          this.items.push(clearedNewText);
          this.textNoSave = false;
          this.form.controls.newText.setValue('');
        } else {
          const newTextForm = this.form.get('newText');
          newTextForm?.markAsTouched();
          newTextForm?.setErrors({
            exists: true,
          });
        }
      }
    }
  }

  editItem(item: any) {
    this.itemsEditing[item] = item;
    this.editingForms[item] = this.fb.group({
      text: [item, this.config.inputValidators ? this.config.inputValidators : null],
    });
    setTimeout(() => {
      const input = document.getElementById(`id${item}`);
      if (input) {
        input.focus();
      }
    }, 100);
  }

  removeItem(item: any) {
    const index = this.items.indexOf(item);
    if (index >= 0) {
      this.items.splice(index, 1);
    }
  }

  confirmEdit(item: any) {
    if (this.editingForms[item].valid) {
      const index = this.items.indexOf(item);
      if (index >= 0) {
        this.items[index] = this.editingForms[item].get('text').value;
      }
      delete this.itemsEditing[item];
    }
  }

  cancelEdit(item: any) {
    delete this.itemsEditing[item];
  }

  removeAll() {
    this.items = [];
  }

  order() {
    if (Object.prototype.hasOwnProperty.call(this.config, 'orderFunction')) {
      this.items.sort(this.config.orderFunction);
    } else {
      this.items.sort((a, b) => {
        if (a > b) {
          return 1;
        }
        if (a < b) {
          return -1;
        }
        return 0;
      });
    }
  }

  importCSV() {
    const input = document.createElement('input');
    input.accept = '.csv';
    input.type = 'file';
    input.onchange = (event: any) => {
      const file = event.target.files[0];
      if (!file) {
        return;
      }
      const reader = new FileReader();
      reader.onload = (e) => {
        this.itemsError = [];
        this.loading = true;
        setTimeout(() => {
          const contents = this.CSVToArray(e.target?.result);
          let index = 0;
          let end = false;
          for (index = 0; index < contents.length; index++) {
            const content = contents[index];
            if (content[0]) {
              const ref = content[0];
              if (this.items.indexOf(ref) < 0) {
                this.form.controls.newText.setValue(ref);
                if (this.form.controls.newText.valid) {
                  this.items.push(ref);
                } else {
                  this.itemsError.push({
                    codigo: ref,
                  });
                }
              }
            }

            if (index === content.length) {
              end = true;
            }
          }
          if (end) {
            this.form.controls.newText.setValue('');
            this.loading = false;
            if (this.itemsError.length) {
              // show toast and excel
              this.toastService.error('CONDITIONS.ERROR_CSV');
            }
          }
        }, 1000);
      };
      reader.readAsText(file);
    };
    input.click();
  }

  private CSVToArray(strData: any, strDelimiter?: string) {
    strDelimiter = strDelimiter || ',';

    const objPattern = new RegExp(
      // Delimiters.
      '(\\' +
        strDelimiter +
        '|\\r?\\n|\\r|^)' +
        // Quoted fields.
        '(?:"([^"]*(?:""[^"]*)*)"|' +
        // Standard fields.
        '([^"\\' +
        strDelimiter +
        '\\r\\n]*))',
      'gi'
    );

    const arrData: any = [[]];
    let arrMatches = null;

    // eslint-disable-next-line no-cond-assign
    while ((arrMatches = objPattern.exec(strData))) {
      const strMatchedDelimiter = arrMatches[1];
      if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter) {
        arrData.push([]);
      }
      let strMatchedValue;
      if (arrMatches[2]) {
        strMatchedValue = arrMatches[2].replace(new RegExp('""', 'g'), '"');
      } else {
        strMatchedValue = arrMatches[3];
      }
      strMatchedValue.replace('/n', '');
      arrData[arrData.length - 1].push(strMatchedValue);
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return arrData;
  }

  clickCsvError() {
    const data = writeCsv(this.itemsError);
    downloadFile([data], 'errorImport.csv');
  }
}
