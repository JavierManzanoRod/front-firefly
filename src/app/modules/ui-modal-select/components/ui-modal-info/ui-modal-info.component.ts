import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UIModalInfo } from '../../models/modal-select.model';

@Component({
  selector: 'ff-ui-modal-info',
  templateUrl: 'ui-modal-info.component.html',
  styleUrls: ['./ui-modal-info.component.scss'],
})
export class UiModalInfoComponent implements OnInit {
  @Input() config!: UIModalInfo;

  items: any;
  filteredItems: any;
  loading = true;

  constructor(public modalRef: BsModalRef, public changed?: ChangeDetectorRef) {}

  ngOnInit() {
    this.loading = true;
    if (this.config && this.config.searchFn) {
      this.config.searchFn().subscribe((res) => {
        this.items = res;
        this.filteredItems = res;
        this.loading = false;
      });
    }
  }

  filterItems(event: any) {
    const value = event.target.value;
    this.filteredItems = !value.length
      ? this.items
      : this.items.filter((item: any) => item.key.toUpperCase().includes(value.toUpperCase()));
  }

  handleCancel() {
    this.modalRef.hide();
  }

  checkTruncate(element: HTMLElement) {
    const range = document.createRange();
    range.selectNodeContents(element.firstChild as HTMLElement);

    const truncated = range.getBoundingClientRect().width > element.clientWidth;

    return truncated;
  }
}
