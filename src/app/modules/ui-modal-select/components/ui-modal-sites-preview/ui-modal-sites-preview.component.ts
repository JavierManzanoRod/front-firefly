import { Component, EventEmitter, Input, OnInit, Optional, Output } from '@angular/core';
import { MayHaveIdName, MayHaveLabel } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import moment from 'moment';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DestroyService } from 'src/app/shared/services/destroy.service';
import {
  HIERARCHIES_ATTRIBUTES,
  LOCALE_ATTRIBUTES,
  MAP_ATTRIBUTES,
  MERCHANDISE_ATTRIBUTES,
  ORDERS_ATTRIBUTES,
  OTHERS_ATTRIBUTES,
  PRICES_ATTRIBUTES,
  PROMOTIONS_ATTRIBUTES,
  SHIPPING_METHODS_ATTRIBUTES,
  SitesAttributes,
  SitesPreview,
  SITES_ATTRIBUTES_ARRAY,
  SortedSitesSectionsAttributes,
  UPDATE_INFO_ATTRIBUTES,
  EXCLUDED_CATEGORIES,
} from 'src/app/sites/site/models/sites-tree-attributes.model';
import { StatusRulesPreview } from '../ui-modal-rules-preview/models/modal-rules-preview.model';

interface FinalPreview {
  [key: string]: {
    [key: string]: {
      values: string | string[] | MayHaveIdName | MayHaveIdName[] | MayHaveLabel | MayHaveLabel[];
      status: StatusRulesPreview;
    };
  };
}

@Component({
  selector: 'ff-ui-modal-sites-preview',
  templateUrl: './ui-modal-sites-preview.component.html',
  styleUrls: ['./ui-modal-sites-preview.component.scss'],
  providers: [DestroyService],
})
export class UiModalSitesPreviewComponent implements OnInit {
  @Output() selectedEvent: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Input() sitesRulesPrevew!: SitesPreview;

  isCollapsedConfig: any = {};
  filledKeys: SitesPreview = {};
  finalPreview: FinalPreview = {};
  status = StatusRulesPreview;
  sortedSiteSections = SITES_ATTRIBUTES_ARRAY;
  sortedSectionsAttributse: SortedSitesSectionsAttributes = {};

  constructor(@Optional() public modalRef: BsModalRef, private translateSrv: TranslateService) {}

  ngOnInit(): void {
    this.sortedSectionsAttributse = this.setOrderedSectionsAttributes();
    this.cleanPreviewObject();
    this.finalPreview = this.createFinalPreview();
  }

  checkIfHaveValue(data: SitesPreview): boolean {
    return Object.keys(data).some((key) => data[key]?.values);
  }

  cleanPreviewObject() {
    Object.keys(this.sitesRulesPrevew).forEach((key) => {
      const values = this.sitesRulesPrevew[key].values;
      if (key === 'store_code_by_hierarchy' || key === 'index_map') {
        if (values && Object.keys(values)?.length > 0) {
          this.filledKeys[key] = {
            values: this.sitesRulesPrevew[key].values,
            status: this.sitesRulesPrevew[key].status,
          };
        }
      } else if (values && values !== undefined && this.sitesRulesPrevew[key].values.length !== 0) {
        this.filledKeys[key] = {
          values: this.sitesRulesPrevew[key].values,
          status: this.sitesRulesPrevew[key].status,
        };
      }
    });
  }

  createFinalPreview(): FinalPreview {
    const keys = Object.keys(this.filledKeys);
    const finalPreview: FinalPreview = {};
    if (keys.includes('locale_default')) {
      finalPreview[SitesAttributes.locale] = {
        locale_default: {
          values: this.getValueByKey(this.filledKeys.locale_default.values as MayHaveIdName, 'name'),
          status: this.filledKeys.locale_default.status,
        },
      };
    }

    if (keys.includes('locale_valid')) {
      finalPreview[SitesAttributes.locale] = {
        ...finalPreview[SitesAttributes.locale],
        locale_valid: {
          values: this.getArrayValuesByKey(this.filledKeys.locale_valid.values as [], 'name'),
          status: this.filledKeys.locale_valid.status,
        },
      };
    }

    if (keys.includes('included_products')) {
      finalPreview[SitesAttributes.merchandise] = {
        ...finalPreview[SitesAttributes.merchandise],
        included_products: {
          values: this.getValueByKey(this.filledKeys.included_products.values as MayHaveIdName, 'name'),
          status: this.filledKeys.included_products.status,
        },
      };
    }

    if (keys.includes('override_tags')) {
      finalPreview[SitesAttributes.merchandise] = {
        ...finalPreview[SitesAttributes.merchandise],
        override_tags: {
          values: this.filledKeys.override_tags.values,
          status: this.filledKeys.override_tags.status,
        },
      };
    }

    if (keys.includes('is_the_cocktail_sold')) {
      finalPreview[SitesAttributes.merchandise] = {
        ...finalPreview[SitesAttributes.merchandise],
        is_the_cocktail_sold: {
          values: this.filledKeys.is_the_cocktail_sold.values,
          status: this.filledKeys.is_the_cocktail_sold.status,
        },
      };
    }

    if (keys.includes('is_insurance_sold')) {
      finalPreview[SitesAttributes.merchandise] = {
        ...finalPreview[SitesAttributes.merchandise],
        is_insurance_sold: {
          values: this.filledKeys.is_insurance_sold.values,
          status: this.filledKeys.is_insurance_sold.status,
        },
      };
    }

    if (keys.includes('index_main')) {
      finalPreview[SitesAttributes.map] = {
        index_main: {
          values: this.filledKeys.index_main.values,
          status: this.filledKeys.index_main.status,
        },
      };
    }

    if (keys.includes('index_map')) {
      finalPreview[SitesAttributes.map] = {
        ...finalPreview[SitesAttributes.map],
        index_map: {
          values: this.getKeyValue(this.filledKeys.index_map.values),
          status: this.filledKeys.index_map.status,
        },
      };
    }

    if (keys.includes('default_shipping_method')) {
      finalPreview[SitesAttributes.shippingMethods] = {
        default_shipping_method: {
          values: this.filledKeys.default_shipping_method.values,
          status: this.filledKeys.default_shipping_method.status,
        },
      };
    }

    if (keys.includes('shipping_methods')) {
      finalPreview[SitesAttributes.shippingMethods] = {
        ...finalPreview[SitesAttributes.shippingMethods],
        shipping_methods: {
          values: this.filledKeys.shipping_methods.values,
          status: this.filledKeys.shipping_methods.status,
        },
      };
    }

    if (keys.includes('available_centers')) {
      finalPreview[SitesAttributes.shippingMethods] = {
        ...finalPreview[SitesAttributes.shippingMethods],
        available_centers: {
          values: this.filledKeys.available_centers.values,
          status: this.filledKeys.available_centers.status,
        },
      };
    }

    if (keys.includes('hierarchy_sales')) {
      finalPreview[SitesAttributes.hierarchies] = {
        hierarchy_sales: {
          values: this.filledKeys.hierarchy_sales.values,
          status: this.filledKeys.hierarchy_sales.status,
        },
      };
    }

    if (keys.includes('hierarchy_campaign')) {
      finalPreview[SitesAttributes.hierarchies] = {
        ...finalPreview[SitesAttributes.hierarchies],
        hierarchy_campaign: {
          values: this.filledKeys.hierarchy_campaign.values,
          status: this.filledKeys.hierarchy_campaign.status,
        },
      };
    }

    if (keys.includes('hierarchy_special_ratio')) {
      finalPreview[SitesAttributes.hierarchies] = {
        ...finalPreview[SitesAttributes.hierarchies],
        hierarchy_special_ratio: {
          values: this.filledKeys.hierarchy_special_ratio.values,
          status: this.filledKeys.hierarchy_special_ratio.status,
        },
      };
    }

    if (keys.includes('store_default_code')) {
      finalPreview[SitesAttributes.hierarchies] = {
        ...finalPreview[SitesAttributes.hierarchies],
        store_default_code: {
          values: this.filledKeys.store_default_code.values,
          status: this.filledKeys.store_default_code.status,
        },
      };
    }

    if (keys.includes('store_code_by_hierarchy')) {
      finalPreview[SitesAttributes.hierarchies] = {
        ...finalPreview[SitesAttributes.hierarchies],
        store_code_by_hierarchy: {
          values: this.getKeyValue(this.filledKeys.store_code_by_hierarchy.values),
          status: this.filledKeys.store_code_by_hierarchy.status,
        },
      };
    }

    if (keys.includes('center_type')) {
      finalPreview[SitesAttributes.prices] = {
        center_type: {
          values: this.translateSrv.instant(this.keyTranslate(this.filledKeys.center_type.values as string)),
          status: this.filledKeys.center_type.status,
        },
      };
    }

    if (keys.includes('fixed_center')) {
      finalPreview[SitesAttributes.prices] = {
        ...finalPreview[SitesAttributes.prices],
        fixed_center: {
          values: this.filledKeys.fixed_center.values,
          status: this.filledKeys.fixed_center.status,
        },
      };
    }

    if (keys.includes('price_available_centers')) {
      finalPreview[SitesAttributes.prices] = {
        ...finalPreview[SitesAttributes.prices],
        price_available_centers: {
          values: this.filledKeys.price_available_centers.values,
          status: this.filledKeys.price_available_centers.status,
        },
      };
    }

    if (keys.includes('start_time')) {
      finalPreview[SitesAttributes.updateInfo] = {
        start_time: {
          values: moment(this.filledKeys.start_time.values as Date).format('hh:mm'),
          status: this.filledKeys.start_time.status,
        },
      };
    }

    if (keys.includes('end_time')) {
      finalPreview[SitesAttributes.updateInfo] = {
        ...finalPreview[SitesAttributes.updateInfo],
        end_time: {
          values: moment(this.filledKeys.end_time.values as Date).format('hh:mm'),
          status: this.filledKeys.end_time.status,
        },
      };
    }

    if (keys.includes('notification_url')) {
      finalPreview[SitesAttributes.updateInfo] = {
        ...finalPreview[SitesAttributes.updateInfo],
        notification_url: {
          values: this.filledKeys.notification_url.values,
          status: this.filledKeys.notification_url.status,
        },
      };
    }

    if (keys.includes('minimum_discount')) {
      finalPreview[SitesAttributes.promotions] = {
        minimum_discount: {
          values: this.filledKeys.minimum_discount.values,
          status: this.filledKeys.minimum_discount.status,
        },
      };
    }

    if (keys.includes('promotional_logic')) {
      finalPreview[SitesAttributes.promotions] = {
        ...finalPreview[SitesAttributes.promotions],
        promotional_logic: {
          values: this.filledKeys.promotional_logic.values,
          status: this.filledKeys.promotional_logic.status,
        },
      };
    }

    if (keys.includes('price_logic')) {
      finalPreview[SitesAttributes.promotions] = {
        ...finalPreview[SitesAttributes.promotions],
        price_logic: {
          values: this.filledKeys.price_logic.values,
          status: this.filledKeys.price_logic.status,
        },
      };
    }

    if (keys.includes('price_centers')) {
      finalPreview[SitesAttributes.promotions] = {
        ...finalPreview[SitesAttributes.promotions],
        price_centers: {
          values: this.filledKeys.price_centers.values,
          status: this.filledKeys.price_centers.status,
        },
      };
    }

    if (keys.includes('modified_price_center')) {
      finalPreview[SitesAttributes.promotions] = {
        ...finalPreview[SitesAttributes.promotions],
        modified_price_center: {
          values: this.filledKeys.modified_price_center.values,
          status: this.filledKeys.modified_price_center.status,
        },
      };
    }

    if (keys.includes('order_enterprise_code')) {
      finalPreview[SitesAttributes.orders] = {
        order_enterprise_code: {
          values: this.filledKeys.order_enterprise_code.values,
          status: this.filledKeys.order_enterprise_code.status,
        },
      };
    }

    if (keys.includes('order_fixed_center')) {
      finalPreview[SitesAttributes.orders] = {
        ...finalPreview[SitesAttributes.orders],
        order_fixed_center: {
          values: this.filledKeys.order_fixed_center.values,
          status: this.filledKeys.order_fixed_center.status,
        },
      };
    }

    if (keys.includes('order_channel')) {
      finalPreview[SitesAttributes.orders] = {
        ...finalPreview[SitesAttributes.orders],
        order_channel: {
          values: this.filledKeys.order_channel.values,
          status: this.filledKeys.order_channel.status,
        },
      };
    }

    if (keys.includes('order_sub_channel')) {
      finalPreview[SitesAttributes.orders] = {
        ...finalPreview[SitesAttributes.orders],
        order_sub_channel: {
          values: this.filledKeys.order_sub_channel.values,
          status: this.filledKeys.order_sub_channel.status,
        },
      };
    }

    if (keys.includes('order_business_line')) {
      finalPreview[SitesAttributes.orders] = {
        ...finalPreview[SitesAttributes.orders],
        order_business_line: {
          values: this.filledKeys.order_business_line.values,
          status: this.filledKeys.order_business_line.status,
        },
      };
    }

    if (keys.includes('order_company_dvd')) {
      finalPreview[SitesAttributes.orders] = {
        ...finalPreview[SitesAttributes.orders],
        order_company_dvd: {
          values: this.filledKeys.order_company_dvd.values,
          status: this.filledKeys.order_company_dvd.status,
        },
      };
    }

    if (keys.includes('order_channel_dvd')) {
      finalPreview[SitesAttributes.orders] = {
        ...finalPreview[SitesAttributes.orders],
        order_channel_dvd: {
          values: this.filledKeys.order_channel_dvd.values,
          status: this.filledKeys.order_channel_dvd.status,
        },
      };
    }

    if (keys.includes('order_center_type')) {
      finalPreview[SitesAttributes.orders] = {
        ...finalPreview[SitesAttributes.orders],
        order_center_type: {
          values: this.translateSrv.instant(this.keyTranslate(this.filledKeys.order_center_type.values as string)),
          status: this.filledKeys.order_center_type.status,
        },
      };
    }

    if (keys.includes('excluded_categories')) {
      finalPreview[SitesAttributes.excludedCategories] = {
        ...finalPreview[SitesAttributes.excludedCategories],
        excluded_categories: {
          values: this.filledKeys.excluded_categories.values,
          status: this.filledKeys.excluded_categories.status,
        },
      };
    }

    if (keys.includes('amount_printed')) {
      finalPreview[SitesAttributes.others] = {
        amount_printed: {
          values: this.filledKeys.amount_printed.values,
          status: this.filledKeys.amount_printed.status,
        },
      };
    }

    if (keys.includes('registered_user')) {
      finalPreview[SitesAttributes.others] = {
        ...finalPreview[SitesAttributes.others],
        registered_user: {
          values: this.filledKeys.registered_user.values,
          status: this.filledKeys.registered_user.status,
        },
      };
    }

    if (keys.includes('view_in_csc')) {
      finalPreview[SitesAttributes.others] = {
        ...finalPreview[SitesAttributes.others],
        view_in_csc: {
          values: this.filledKeys.view_in_csc.values,
          status: this.filledKeys.view_in_csc.status,
        },
      };
    }

    // finalPreview = this.orderPreview(finalPreview);
    console.log('final preview --->', finalPreview);
    return finalPreview;
  }

  getArrayValuesByKey(attributes: [], key: string): string {
    let values = '';
    attributes.forEach((item, index) => {
      values += item[key];
      values += ', ';
      if (index === attributes.length - 1) {
        values = values.slice(0, -2);
      }
    });
    return values;
  }

  hasKey(key: string): boolean {
    return Object.keys(this.finalPreview).includes(key);
  }

  hasAttribute(section: string, attribute: string): boolean {
    if (Object.keys(this.finalPreview).includes(section)) {
      return Object.keys(this.finalPreview[section]).includes(attribute);
    }
    return false;
  }

  getValueByKey(attribute: MayHaveIdName | MayHaveLabel, key: string): string {
    return attribute[key];
  }

  getKeyValue(attribute: any): string {
    let keyValue = '';
    Object.keys(attribute).forEach((key, index) => {
      keyValue += `(${key}) ${attribute[key]}, `;
      if (index === Object.keys(attribute).length - 1) {
        keyValue = keyValue.slice(0, -2);
      }
    });
    return keyValue;
  }

  getValuesArrays(section: string, attribute: string): string[] {
    const values = this.finalPreview[section][attribute].values;
    if (typeof values === 'boolean') {
      return ['Activo'];
    } else if (typeof values === 'string') {
      return values.split(',');
    } else {
      return values as [];
    }
  }

  handleCancel() {
    this.modalRef.hide();
  }

  keyTranslate(key: string): string {
    return `SITES_PREVIEW.${key.toUpperCase()}`;
  }

  setOrderedSectionsAttributes(): SortedSitesSectionsAttributes {
    const sortedAttributes = {
      locale: LOCALE_ATTRIBUTES,
      merchandise: MERCHANDISE_ATTRIBUTES,
      map: MAP_ATTRIBUTES,
      shippingMethods: SHIPPING_METHODS_ATTRIBUTES,
      hierarchies: HIERARCHIES_ATTRIBUTES,
      prices: PRICES_ATTRIBUTES,
      updateInfo: UPDATE_INFO_ATTRIBUTES,
      promotions: PROMOTIONS_ATTRIBUTES,
      orders: ORDERS_ATTRIBUTES,
      excludedCategories: EXCLUDED_CATEGORIES,
      others: OTHERS_ATTRIBUTES,
    };

    return sortedAttributes;
  }

  getSortedAttributes(key: string): string[] {
    return this.sortedSectionsAttributse[key];
  }

  getStatus(section: string, attribute: string): string {
    const status = this.finalPreview[section][attribute].status;

    switch (status) {
      case StatusRulesPreview.saved:
        return 'icon-icon-check';
      case StatusRulesPreview.deleted:
        return 'icon-icon-multiply';
      case StatusRulesPreview.edited:
        return 'icon-pencil';
      default:
        return '';
    }
  }
}
