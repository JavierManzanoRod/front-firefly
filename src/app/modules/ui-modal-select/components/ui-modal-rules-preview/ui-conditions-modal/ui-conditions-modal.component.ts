import { ChangeDetectionStrategy, Component, Inject, Input, OnInit } from '@angular/core';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_GROUPED_ATTRIBUTES } from 'src/app/configuration/tokens/configuracion';
import { ConditionsBasic, ControlPanelConditionsNonGroupped } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { clone } from 'src/app/shared/utils/utils';
import { ORDER_RULES, RulesPreview, StatusRulesPreview } from '../models/modal-rules-preview.model';
@Component({
  selector: 'ff-ui-conditions-modal',
  templateUrl: './ui-conditions-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./ui-conditions-modal.component.scss'],
})
export class UiConditionsModalComponent implements OnInit {
  @Input() config!: RulesPreview;
  @Input() grouped!: boolean;
  @Input() showOrder: string[] = ORDER_RULES;
  notGrouped = ControlPanelConditionsNonGroupped;
  hasAnd = false;
  hasOr = false;
  hasOne = false;
  onlyOr = false;
  status = StatusRulesPreview;
  keysGrouped: string[] = [];
  constructor(@Inject(FF_GROUPED_ATTRIBUTES) public groupedAttributes: ConfigurationDefinition['groupedAttributes']) {}

  ngOnInit() {
    this.config = Object.keys(this.config).reduce((stack: any, current: string) => {
      if (this.config[current]?.values) {
        stack[current] = this.config[current];

        if (!this.showOrder.includes(current) && this.config[current].values && !this.grouped) {
          this.showOrder.push(current);
        }
      }
      return stack;
    }, {});

    if (this.config?.ranges?.values?.length) {
      this.config.ranges = clone(this.config.ranges);
      this.config.ranges.values.forEach((element: any) => {
        const value = element?.length >= 0 ? element[0] : element;
        const label = [];
        const price = [];
        const discount = [];
        if (!value) {
          return '';
        }

        if (value.price && Object.keys(value.price).length) {
          if (value.price.from === null) {
            price.push(`${0}€`);
          }
          if (value.price.from !== null) {
            price.push(`${value.price.from}€`);
          }
          if (value.price.to !== null && value.price.to !== undefined) {
            price.push(`${value.price.to}€`);
          }

          if (price.length) {
            label.push(price.join(' - '));
          }
        }

        if (value.discount && Object.keys(value.discount).length) {
          if (value.discount.from === null) {
            price.push(`${0}%`);
          }
          if (value.discount.from !== null) {
            discount.push(`${value.discount.from}%`);
          }
          if (value.discount.to !== null && value.discount.to !== undefined) {
            discount.push(`${value.discount.to}%`);
          }

          if (discount.length) {
            label.push(discount.join(' - '));
          }
        }

        if (label.length) {
          this.config.ranges.values = [label.join(' / ')];
          return '';
        }
        this.config.ranges.values = label;
      });
    }

    const tempGrouped = this.groupedAttributes as any;
    this.keysGrouped = clone(Object.keys(this.groupedAttributes || {}));

    this.hasOr = Object.keys(this.config).filter((value) => tempGrouped[value] && this.config[value]?.values?.length).length > 0;

    this.hasAnd =
      !!Object.keys(this.config).reduce((prev: number, key: string) => {
        if (this.config[key as keyof ConditionsBasic]?.values?.length) {
          prev++;
        }
        return prev;
      }, 0) && !this.grouped;

    //comprueba que tenga sólo una condición y la suma al tipo and
    if (Object.keys(this.config).length === 1) {
      this.hasOne = true;
      this.hasOr = false;
    } else {
      this.hasOne = false;
      this.hasOr = false;
      const intersection = Object.keys(this.config).find(
        (value) => Object.keys(this.notGrouped || {}).includes(value) && (this.notGrouped as any)[value]
      );
      if (intersection) {
        this.onlyOr = false;
      } else {
        this.onlyOr = true;
      }
      Object.keys(this.config).forEach((key: string) => {
        if (this.groupedAttributes && tempGrouped[key] && intersection) {
          this.hasOr = true;
          this.config[key] = this.config[key as keyof ConditionsBasic];
          if (this.showOrder.find((v) => v === 'grouped')) {
            const index = this.showOrder.findIndex((v) => v === key);
            if (index && index >= 0) {
              this.showOrder.splice(index, 1);
            }
          }
        }
      });
    }
  }

  getClass(key: string): string {
    const relations = {
      saved: 'icon-icon-check',
      deleted: 'icon-icon-multiply',
      edited: 'icon-pencil',
    };

    return relations[this.config[key].status as keyof typeof relations] || '';
  }

  keyTranslate(key: string): string {
    return `SPENTER.MODAL_PREVIEW.${key.toUpperCase()}`;
  }
}
