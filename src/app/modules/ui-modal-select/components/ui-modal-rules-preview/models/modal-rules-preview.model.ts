import { IPricesAndDiscounts } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { MayHaveLabel } from './../../../../../model/base-api.model';

export interface RulesPreview {
  [key: string]: {
    values: string[] | MayHaveLabel[] | IPricesAndDiscounts[];
    status: StatusRulesPreview;
  };
}

export enum StatusRulesPreview {
  saved = 'saved',
  edited = 'edited',
  deleted = 'deleted',
}

export const ORDER_RULES = [
  'sites',
  'categoryRules',
  'saleReference',
  'provider',
  'brand',
  'categories',
  'goodTypes',
  'classifications1',
  'classifications2',
  'classifications3',
  'classifications4',
  'classifications5',
  'classifications6',
  'managementType',
  'customisedRequest',
  'centers',
  'ranges',
  'grouped',
  'department',
  'family',
  'barra',
  'product',
  'sizeCode',
];
