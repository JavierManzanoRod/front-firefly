import { Component, EventEmitter, Input, Optional, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ConfigPreview } from 'src/app/modules/conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { DestroyService } from 'src/app/shared/services/destroy.service';
import { RulesPreview } from './models/modal-rules-preview.model';

@Component({
  selector: 'ff-ui-modal-rules-preview',
  templateUrl: './ui-modal-rules-preview.component.html',
  styleUrls: ['./ui-modal-rules-preview.component.scss'],
  providers: [DestroyService],
})
export class UiModalRulesPreviewComponent {
  @Output() selectedEvent: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Input() config!: ConfigPreview;
  @Input() readonly = false;

  isCollapsedConfig: any = {};

  constructor(@Optional() public modalRef: BsModalRef) {}

  get includedKeys() {
    return Object.keys(this.config.included)?.length > 0 && this.checkIfHaveValue(this.config.included);
  }
  get excludedKeys() {
    return Object.keys(this.config.excluded)?.length > 0 && this.checkIfHaveValue(this.config.excluded);
  }

  checkIfHaveValue(data: RulesPreview): boolean {
    return Object.keys(data).some((key) => data[key]?.values);
  }

  handleCancel() {
    this.modalRef.hide();
  }
}
