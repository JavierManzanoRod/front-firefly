import { BsModalRef } from 'ngx-bootstrap/modal';
import { UiModalRulesPreviewComponent } from './ui-modal-rules-preview.component';

describe('UiModalRulesPreviewComponent', () => {
  it('it should create ', () => {
    const component = new UiModalRulesPreviewComponent((null as unknown) as BsModalRef);
    expect(component).toBeDefined();
  });
});
