import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GenericApiResponse, MayHaveLabel } from '@model/base-api.model';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, switchMap, tap } from 'rxjs/operators';
import { MayHaveLabelPayLoad, UIModalSelectView } from '../../models/modal-select.model';

// TODO Borrar cuando se deje de usar, modal cambiada por src/app/ui-modal-select/components/ui-modal-select

const SIZE = 5;

@Component({
  selector: 'ff-ui-modal-select',
  templateUrl: './modal-select.component.html',
  styleUrls: ['./modal-select.component.scss'],
})
export class UiModalSelectComponent implements OnInit, AfterViewInit {
  @ViewChild('searchElement') searchElement!: ElementRef;
  @Output() selectItem: EventEmitter<any> = new EventEmitter<any>();
  @Output() cancel: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() config!: UIModalSelectView;

  loading = true;
  loadingSearch = false;
  loadingLoadMore = false;
  form: FormGroup;
  listResults: MayHaveLabel[] = [];
  hasMoreItems = false;
  showNoResults = false;
  pageNumber = 0;

  constructor(public modalRef: BsModalRef, private fb: FormBuilder) {
    this.form = this.fb.group({
      search: [null],
    });
  }

  ngOnInit() {
    this.loading = false;
    this.form.controls.search.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap((text) => text.trim()),
        filter((text) => text !== null && text !== ''),
        tap(() => (this.loadingSearch = true)),
        tap(() => this.resetResults()),
        switchMap((name: string) => {
          if (this.config && this.config.searchFn) {
            return this.config.searchFn({ name, size: SIZE });
          }
          return of();
        }),
        tap(() => (this.loadingSearch = false))
      )
      .subscribe((data: any) => {
        this.setResults(data);
      });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.searchElement.nativeElement.focus();
    });
  }

  get currentSearch(): string {
    const val = this.form.controls.search.value;
    return val !== null && val.trim();
  }

  loadMoreItems() {
    this.loadingLoadMore = true;
    if (this.config && this.config.searchFn) {
      this.config.searchFn({ name: this.currentSearch, page: ++this.pageNumber, size: SIZE }).subscribe((data) => {
        this.setResults(data);
        this.loadingLoadMore = false;
      });
    }
  }

  onSelectItem(item: MayHaveLabelPayLoad<any>) {
    this.selectItem.emit(item);
    this.modalRef.hide();
  }

  handleCancel() {
    this.cancel.emit(false);
    this.modalRef.hide();
  }

  private setResults({ content, page }: GenericApiResponse<MayHaveLabel>): void {
    this.listResults = [...this.listResults, ...content];
    this.hasMoreItems = page.page_number < page.total_pages;
    this.showNoResults = this.listResults.length === 0;
  }

  private resetResults(): void {
    this.listResults = [];
  }
}
