import { FormBuilder } from '@angular/forms';
import { UiModalSelectComponent } from './modal-select.component';

describe('ConditionsBasicModalSelectComponent', () => {
  it('it should create ', () => {
    const component = new UiModalSelectComponent(null as any, new FormBuilder());
    expect(component).toBeDefined();
  });
});
