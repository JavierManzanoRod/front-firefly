import { ChangeDetectorRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { UiModalSelectTreeFolderComponent } from './ui-modal-select-tree-folder.component';

describe('ConditionsBasicModalSelectComponent', () => {
  it('it should create ', () => {
    const component = new UiModalSelectTreeFolderComponent(
      null as unknown as BsModalRef,
      null as unknown as TranslateService,
      null as unknown as ChangeDetectorRef,
      null as unknown as ToastService
    );
    expect(component).toBeDefined();
  });
});
