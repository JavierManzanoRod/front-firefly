import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GenericApiResponse, MayHaveLabel } from '@model/base-api.model';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DestroyService } from 'src/app/shared/services/destroy.service';
import { ToastService } from '../../../toast/toast.service';
import { UIModalTree } from '../../models/modal-select.model';
import { ExplodedClickEvent, ExplodedTree, ExplodedTreeNodeType } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { FoldersExplodedTreeComponent } from 'src/app/modules/exploded-tree/components/adapters/folders-exploded-tree/folders-exploded-tree.component';
import { TranslateService } from '@ngx-translate/core';

const SIZE = 10;

@Component({
  selector: 'ff-ui-modal-select-tree-folder',
  templateUrl: './ui-modal-select-tree-folder.component.html',
  styleUrls: ['./ui-modal-select-tree-folder.component.scss'],
  providers: [DestroyService],
})
export class UiModalSelectTreeFolderComponent implements OnInit {
  @Output() selectedEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() canceledEvent: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild(FoldersExplodedTreeComponent) explodedTree!: FoldersExplodedTreeComponent;
  @Input() config!: UIModalTree;

  configCopy!: UIModalTree;

  tree: ExplodedTree[] = [];
  searchResult?: ExplodedTree[];

  loading = true;
  loadingSearch = false;
  loadingLoadMore = false;
  hasMoreItems = false;
  showNoResults = false;
  pageNumber = 0;
  total_elements = 0;

  selected: any[] = [];
  selectedValues: any[] = [];

  form!: FormGroup;

  items: any[] = [];

  isAutoFirstSearch = false;
  configToast = {
    positionClass: 'modal-select-component',
  };

  constructor(
    public modalRef: BsModalRef,
    private translate: TranslateService,
    private change: ChangeDetectorRef,
    public toastService: ToastService
  ) {}

  ngOnInit() {
    if (this.config.tree) {
      this.config.tree.call(this).subscribe((tree: ExplodedTree[]) => {
        this.tree = tree;
      });
    }
    if (this.config.selected) {
      this.selected = this.config.multiple
        ? JSON.parse(JSON.stringify(this.config.selected))
        : [JSON.parse(JSON.stringify(this.config.selected))];
    }
    if (!Object.prototype.hasOwnProperty.call(this.config, 'showInputFilter')) {
      this.config.showInputFilter = true;
    }
    if (!this.config.itemValueKey) {
      this.config.itemValueKey = 'id';
    }
    if (!this.config.itemLabelKey) {
      this.config.itemLabelKey = ['label', 'name'];
    }
    if (!this.config.selectFilterValueKey) {
      this.config.selectFilterValueKey = 'id';
    }
    this.updateSelectedValues();
  }

  showFilter() {
    return true;
  }

  searchNodes(filter: string) {
    this.explodedTree.loading = true;

    if (filter && this.config.searchFn && (this.config?.filterFn?.(filter) || !this.config.filterFn)) {
      this.config.searchFn.call(this, { name: filter, size: SIZE }).subscribe((response) => {
        if (response.content.length) {
          this.searchResult = response.content.map((rule) => ({ ...rule, value: { isRule: true } }));
        } else {
          this.searchResult = [
            {
              icon: 'icon-icon-warning',
              node_type: ExplodedTreeNodeType.WARNING,
              label: this.translate.instant('COMMON_ERRORS.ERROR_404'),
            },
          ];
        }

        this.explodedTree.loading = false;
        this.change.markForCheck();
      });
    } else {
      this.searchResult = undefined;

      this.explodedTree.loading = false;
      this.change.markForCheck();
    }
  }

  updateSelectedValues() {
    if (this.explodedTree) {
      this.explodedTree.update();
    }
    this.selectedValues = this.selected.map((item) => {
      return item[this.config.itemValueKey || ''];
    });
  }

  handleDone() {
    this.selectedEvent.emit(this.config.multiple ? this.selected : this.selected[0]);
    this.modalRef.hide();
  }

  handleCancel() {
    if (this.config.multiple) {
      this.canceledEvent.emit(Array.isArray(this.config.selected) && this.config.selected.length ? this.config.selected : []);
    } else {
      this.canceledEvent.emit(this.config.selected ? this.config.selected : undefined);
    }
    this.modalRef.hide();
  }

  removeSelected(item: any) {
    const index = this.selectedValues.indexOf(item[this.config.itemValueKey || '']);
    if (index >= 0) {
      this.selected.splice(index, 1);
    }
    this.updateSelectedValues();
  }

  selectItem(item: any) {
    const index = this.selectedValues.indexOf(item[this.config.itemValueKey || '']);
    if (this.config.multiple) {
      if (index < 0) {
        this.selected.push(item);
      }
    } else {
      if (index < 0) {
        this.selected = [item];
      } else {
        this.selected = [];
      }
    }
    this.updateSelectedValues();
  }

  addAll() {
    this.items.forEach((item) => {
      this.selectItem(item);
    });
  }

  removeAll() {
    this.selected = [];
    this.updateSelectedValues();
  }

  getItemValue(item: any) {
    let value = '';
    this.config.itemLabelKey?.forEach((key: any) => {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        value = item[key];
        return false;
      }
    });
    return value;
  }

  orderAZ() {
    this.selected.sort((a, b) => {
      if (this.getItemValue(a) > this.getItemValue(b)) {
        return 1;
      }
      if (this.getItemValue(a) < this.getItemValue(b)) {
        return -1;
      }
      return 0;
    });
    this.updateSelectedValues();
  }

  clickNode(event: ExplodedClickEvent<ExplodedTreeNodeComponent>) {
    if (!this.config.clickNode || this.config.clickNode.call(this, event)) {
      this.selectItem(event.data);
      event.target.update();
    }
  }

  classFilter(node: ExplodedTreeNodeComponent) {
    const index = this.selectedValues.indexOf(node.data?.id as any);
    if (index !== -1) {
      return 'selected';
    }
    return null;
  }

  private setResults({ content, page }: GenericApiResponse<MayHaveLabel>): void {
    this.items = [...this.items, ...content];
    this.hasMoreItems = page.page_number < page.total_pages - 1;
    this.showNoResults = this.items.length === 0;
    this.total_elements = page.total_elements;
    if (this.isAutoFirstSearch) {
      if (this.showNoResults) {
        this.showNoResults = false;
        this.toastService.warning('MODAL_SELECT.NO_DATA_DESCRIPTION', '', this.configToast);
      }
    }
    this.explodedTree.update();
  }
}
