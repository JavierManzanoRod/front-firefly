/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GenericApiResponse, MayHaveLabel } from '@model/base-api.model';
import { NgSelectComponent } from '@ng-select/ng-select';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { DestroyService } from 'src/app/shared/services/destroy.service';
import { ToastService } from '../../../toast/toast.service';
import { UIModalSelect } from '../../models/modal-select.model';

const SIZE = 10;

@Component({
  selector: 'ff-ui-modal-select-rules',
  templateUrl: './ui-modal-select-rules.component.html',
  styleUrls: ['./ui-modal-select-rules.component.scss'],
  providers: [DestroyService],
})
export class UiModalSelectRulesComponent implements OnInit, AfterViewInit {
  @ViewChild('selector') selector!: NgSelectComponent;
  @Output() selectedEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() canceledEvent: EventEmitter<any> = new EventEmitter<any>();
  @Input() config!: UIModalSelect;

  configCopy!: UIModalSelect;
  loading = true;
  readonly = false;
  loadingSearch = false;
  loadingLoadMore = false;
  hasMoreItems = false;
  showNoResults = false;
  pageNumber = 0;
  total_elements = 0;
  selectedChips: any[] = [];
  selected: any[] = [];
  selectedValues: any[] = [];

  form!: FormGroup;

  items: any[] = [];

  isAutoFirstSearch = false;
  configToast = {
    positionClass: 'modal-select-component',
  };

  get availableItems() {
    if (this.config.chips) {
      return [...new Set(this.config.chips.filter((item) => !this.selectedChips.includes(item)))];
    }

    return [];
  }
  constructor(public modalRef: BsModalRef, private fb: FormBuilder, public toastService: ToastService, private destroy$: DestroyService) {}

  ngOnInit() {
    this.configCopy = JSON.parse(JSON.stringify(this.config));
    this.form = this.fb.group({
      input: [null],
      select: [null],
    });
    if (this.config.searchFn) {
      this.form.controls.input.valueChanges
        .pipe(
          debounceTime(500),
          map((text) => (text as string).trim()),
          distinctUntilChanged(),
          // Clean items if new seach
          tap(() => {
            this.filterItems();
            this.pageNumber = 0;
            this.loadingSearch = true;
            this.hasMoreItems = false;
          }),
          tap(() => (this.items = [])),
          switchMap((name) => {
            // filter if we have filter fn
            if (this.config.searchFn && (this.config?.filterFn?.(name) || !this.config.filterFn)) {
              return this.config.searchFn.call(this, { name, size: SIZE }, this.getChips());
            }
            return of(null);
          }),
          tap(() => (this.loadingSearch = false)),
          takeUntil(this.destroy$)
        )
        .subscribe(
          (data: any) => {
            if (data) {
              this.setResults(data);
            } else {
              this.items = [];
            }
          },
          () => {
            this.loadingSearch = false;
            this.hasMoreItems = false;
            this.toastService.error('MODAL_SELECT.ERROR_DESCRIPTION', 'MODAL_SELECT.ERROR_TITLE', this.configToast);
          }
        );
      if (this.config.searchFnOnInit) {
        this.isAutoFirstSearch = true;
        this.form.controls.input.setValue('');
      }
    } else {
      this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
        this.filterItems();
      });
    }
    if (this.config.selected) {
      this.selected = this.config.multiple
        ? JSON.parse(JSON.stringify(this.config.selected))
        : [JSON.parse(JSON.stringify(this.config.selected))];
    }
    if (!Object.prototype.hasOwnProperty.call(this.config, 'showInputFilter')) {
      this.config.showInputFilter = true;
    }
    if (!this.config.itemValueKey) {
      this.config.itemValueKey = 'id';
    }
    if (!this.config.itemLabelKey) {
      this.config.itemLabelKey = ['label', 'name'];
    }
    if (!this.config.selectFilterValueKey) {
      this.config.selectFilterValueKey = 'id';
    }
    this.selectedChips = [...(this.config.chips || [])];
    this.updateSelectedValues();
    this.filterItems();
  }

  updateSelectedValues() {
    this.selectedValues = this.selected.map((item) => {
      return item[this.config.itemValueKey || ''];
    });
  }

  cleanInput() {
    if (this.form.controls.input.value) {
      this.showNoResults = false;
      this.form.controls.input.setValue('');
    }
    this.selected = [];
    this.updateSelectedValues();
  }

  ngAfterViewInit() {}

  get currentSearch(): string {
    const val: string = this.form.controls.input.value;
    return val !== null ? val.trim() : '';
  }

  loadMoreItems() {
    this.loadingLoadMore = true;
    if (this.config.searchFn) {
      this.config.searchFn
        .call(this, { name: this.currentSearch, page: ++this.pageNumber, size: SIZE }, this.getChips())
        .pipe(
          finalize(() => (this.loadingLoadMore = false)),
          takeUntil(this.destroy$)
        )
        .subscribe((data) => {
          this.setResults(data);
          this.loadingLoadMore = false;
        });
    }
  }

  handleDone() {
    if (this.selected && this.selected.length) {
      this.selectedEvent.emit(this.config.multiple ? this.selected : this.selected[0]);
      this.modalRef.hide();
    }
  }

  handleCancel() {
    if (this.config.multiple) {
      this.canceledEvent.emit(Array.isArray(this.config.selected) && this.config.selected.length ? this.config.selected : []);
    } else {
      this.canceledEvent.emit(this.config.selected ? this.config.selected : undefined);
    }
    this.modalRef.hide();
  }

  removeSelected(item: any) {
    const index = this.selectedValues.indexOf(item[this.config.itemValueKey || '']);
    if (index >= 0) {
      this.selected.splice(index, 1);
    }
    this.updateSelectedValues();
  }

  selectItem(item: any) {
    const index = this.selectedValues.indexOf(item[this.config.itemValueKey || '']);
    if (this.config.multiple) {
      if (index < 0) {
        this.selected.push(item);
      }
    } else {
      if (index < 0) {
        this.selected = [item];
      } else {
        this.selected = [];
      }
    }
    this.updateSelectedValues();
  }

  filterItems() {
    let items = this.config.items ? this.config.items : [];
    if (this.form.controls.select.value) {
      items = items.filter((item) => {
        return item[this.config.selectFilterValueKey || ''].toString() === this.form.controls.select.value.toString();
      });
    }
    if (this.form.controls.input.value) {
      items = items.filter((item) => {
        return this.getItemValue(item).toLowerCase().indexOf(this.form.controls.input.value.toLowerCase()) >= 0;
      });
    }
    this.items = items;
  }

  addAll() {
    this.items.forEach((item) => {
      this.selectItem(item);
    });
  }

  removeAll() {
    this.selected = [];
    this.updateSelectedValues();
  }

  add(item: any) {
    this.selectedChips = [...this.selectedChips, item];
    this.onChipChange();
    this.selector.writeValue('');
  }

  remove(item: any) {
    if (this.loadingSearch) {
      return;
    }
    const index = this.selectedChips.indexOf(item);
    if (index !== -1) {
      this.selectedChips.splice(index, 1);
    }

    if (this.getChips()) {
      this.onChipChange();
    } else {
      this.cleanInput();
    }
  }

  getItemValue(item: any) {
    let value = '';
    this.config.itemLabelKey?.forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        value = item[key];
        return false;
      }
    });
    return value;
  }

  onChipChange() {
    if (this.form.value.input || this.form.value.select) {
      this.items = [];
      this.loadingSearch = true;
      this.hasMoreItems = false;
      if (this.config.searchFn) {
        this.config.searchFn
          .call(this, { name: this.currentSearch, size: SIZE }, this.getChips())
          .pipe(takeUntil(this.destroy$))
          .subscribe((data) => {
            this.setResults(data);
            this.loadingSearch = false;
          });
      }
    }
  }

  addAllFilters() {
    const all = this.config.chips || [];
    if (this.selectedChips) {
      all.push(...this.selectedChips);
    }
    this.selectedChips = [...new Set(all)];
  }
  removeAllFilters() {
    this.selectedChips = [];
    this.cleanInput();
  }

  orderAZ() {
    this.selected.sort((a, b) => {
      if (this.getItemValue(a) > this.getItemValue(b)) {
        return 1;
      }
      if (this.getItemValue(a) < this.getItemValue(b)) {
        return -1;
      }
      return 0;
    });
    this.updateSelectedValues();
  }

  private setResults({ content, page }: GenericApiResponse<MayHaveLabel>): void {
    this.items = [...this.items, ...content].sort((a, b) => {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    });

    this.hasMoreItems = page.page_number < page.total_pages - 1;
    this.showNoResults = this.items.length === 0;
    this.total_elements = page.total_elements;
    if (this.isAutoFirstSearch) {
      if (this.showNoResults) {
        this.showNoResults = false;
        this.toastService.warning('MODAL_SELECT.NO_DATA_DESCRIPTION', '', this.configToast);
      }
    }
  }

  private getChips() {
    let chips: any = null;

    if (this.selectedChips && this.selectedChips.length) {
      chips = {};
      this.selectedChips.forEach((check) => {
        chips[check.key] = check.value;
      });
    }

    return chips;
  }
}
