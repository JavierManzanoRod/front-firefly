import { FormBuilder } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { DestroyService } from 'src/app/shared/services/destroy.service';
import { UiModalSelectRulesComponent } from './ui-modal-select-rules.component';

describe('ConditionsBasicModalSelectRulesComponent', () => {
  it('it should create ', () => {
    const component = new UiModalSelectRulesComponent(
      (null as unknown) as BsModalRef,
      new FormBuilder(),
      (null as unknown) as ToastService,
      (null as unknown) as DestroyService
    );
    expect(component).toBeDefined();
  });
});
