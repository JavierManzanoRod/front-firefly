import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { throwError } from 'rxjs';
import { catchError, finalize, retryWhen, mergeMap } from 'rxjs/operators';
import { AttributeTranslations } from 'src/app/catalog/attribute-translation/models/attribute-translation.model';
import { AttributeTranslationService } from 'src/app/catalog/attribute-translation/services/attribute-translation.service';
import { FREE_TEXT, KEYFRONT, SECTION_WITH_LINE } from 'src/app/catalog/technical-characteristics/models/technical-characteristics.model';
import { FF_LANGUAGES } from 'src/app/configuration/tokens/language.token';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { UIModalI18NTranslate } from '../../models/modal-select.model';

@Component({
  selector: 'ff-ui-modal-i18n-translate',
  templateUrl: './ui-modal-i18n-translate.component.html',
  styleUrls: ['./ui-modal-i18n-translate.component.scss'],
})
export class UiModalI18nTranslateComponent implements OnInit {
  @Output() selectItems: EventEmitter<AttributeTranslations> = new EventEmitter<AttributeTranslations>();
  @Output() cancel: EventEmitter<AttributeTranslations | null> = new EventEmitter<AttributeTranslations | null>();
  @Input() config: UIModalI18NTranslate | undefined;
  @Input() validatorEs = false;

  firstLanguage = '';
  showNeedFirstLanguageError = false;
  loading = false;

  value: { [key: string]: string } = {};

  constructor(
    public modalRef: BsModalRef,
    private readonly translateService: TranslateService,
    private toast: ToastService,
    private service: AttributeTranslationService,
    @Inject(FF_LANGUAGES) public languages: string[]
  ) {}

  ngOnInit() {
    this.firstLanguage = this.translateService.instant('LANGUAGES.' + this.languages[0]);
    this.value = this.config?.items.translations ? JSON.parse(JSON.stringify(this.config.items.translations)) : {};
  }

  handleDone() {
    this.showNeedFirstLanguageError = false;
    if (this.value[this.languages[0]]) {
      const result: any = {};
      Object.keys(this.value).forEach((key) => {
        if (this.value[key] && this.value[key].trim() !== '') {
          result[key] = this.value[key];
        }
      });

      if (!this.validatorEs || this.value[this.languages[0]]) {
        if (
          this.config?.items.pathCache &&
          this.config.items.id !== SECTION_WITH_LINE &&
          this.config.items.id !== FREE_TEXT &&
          this.config.items.pathCache?.indexOf(KEYFRONT) === -1
        ) {
          this.loading = true;
          const item = {
            ...this.config?.items,
            path: this.config.items.pathCache || this.config.items.visual_path || this.config.items.data.visual_path,
            translations: this.value,
          };
          const method = this.config.items.canPut ? this.service.update(item) : this.service.post(item);
          const methodTry = this.config.items.canPut ? this.service.post(item) : this.service.update(item);

          methodTry.pipe(
            finalize(() => (this.loading = false)),
            catchError((err) => {
              this.toast.error('COMMON_ERRORS.INTERNAL_ERROR');
              return throwError(err);
            })
          );

          method
            .pipe(
              finalize(() => (this.loading = false)),
              catchError((err) => {
                // this.toast.error('COMMON_ERRORS.INTERNAL_ERROR');
                return throwError(err);
              }),
              retryWhen((obs) => {
                return obs.pipe(
                  mergeMap((err) => {
                    if (err.status === 404) {
                      this.loading = true;
                      return methodTry;
                    }
                    return throwError(err);
                  })
                );
              })
            )
            .subscribe((r) => {
              this.toast.success('COMMON.SUCCESS');
              if (this.config) {
                this.config.items.canPut = true;
              }
              this.selectItems.emit(r?.data);
              this.modalRef.hide();
            });
        } else {
          this.selectItems.emit({
            translations: result,
          });
        }
      }
    } else {
      this.showNeedFirstLanguageError = true;
    }
  }

  handleCancel() {
    this.cancel.emit(this.config?.items ? this.config.items : null);
    this.modalRef.hide();
  }
}
