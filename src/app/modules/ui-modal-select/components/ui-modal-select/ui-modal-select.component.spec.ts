import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { DestroyService } from 'src/app/shared/services/destroy.service';
import { UiModalSelectComponent } from './ui-modal-select.component';

describe('ConditionsBasicModalSelectComponent', () => {
  it('it should create ', () => {
    const component = new UiModalSelectComponent(
      (null as unknown) as BsModalRef,
      new FormBuilder(),
      (null as unknown) as ToastService,
      (null as unknown) as DestroyService,
      (null as unknown) as TranslateService
    );
    expect(component).toBeDefined();
  });
});
