import { EventEmitter, Injectable } from '@angular/core';
import { MayHaveLabel } from '@model/base-api.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable, race } from 'rxjs';
import { first } from 'rxjs/operators';
import { SitesPreview } from 'src/app/sites/site/models/sites-tree-attributes.model';
import { ConfigPreview } from '../conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { UiModalI18nComponent } from './components/ui-modal-i18n/ui-modal-i18n.component';
import { UiModalInfoComponent } from './components/ui-modal-info/ui-modal-info.component';
import { UiModalKeyValueExpertsComponent } from './components/ui-modal-key-value-experts/ui-modal-key-value-experts.component';
import { UiModalKeyValueComponent } from './components/ui-modal-key-value/ui-modal-key-value.component';
import { UiModalRangeComponent } from './components/ui-modal-range/ui-modal-range.component';
import { UiModalRulesPreviewComponent } from './components/ui-modal-rules-preview/ui-modal-rules-preview.component';
import { UiModalSelectDateComponent } from './components/ui-modal-select-date/ui-modal-select-date.component';
import { UiModalSelectProductsComponent } from './components/ui-modal-select-products/ui-modal-select-products.component';
import { UiModalSelectRulesComponent } from './components/ui-modal-select-rules/ui-modal-select-rules.component';
import { UiModalSelectSubsitesComponent } from './components/ui-modal-select-subsites/ui-modal-select-subsite.component';
import { UiModalSelectTextsComponent } from './components/ui-modal-select-texts/ui-modal-select-texts.component';
import { UiModalSelectTreeComponent } from './components/ui-modal-select-tree/ui-modal-select-tree.component';
import { UiModalSelectComponent as NewUiModalSelectComponent } from './components/ui-modal-select/ui-modal-select.component';
import { UiModalSitesPreviewComponent } from './components/ui-modal-sites-preview/ui-modal-sites-preview.component';
import { UiModalSelectTreeFolderComponent } from './components/ui-modal-select-tree-folder/ui-modal-select-tree-folder.component';
import {
  UIModalI18N,
  UIModalInfo,
  UIModalRange,
  UIModalSelect,
  UIModalSelectProducts,
  UIModalSelectSubsites,
  UIModalSelectTextsView,
  UIModalSelectView,
  UIModalTree,
} from './models/modal-select.model';

@Injectable({
  providedIn: 'root',
})
export class UiModalSelectService {
  constructor() {}

  show(config: UIModalSelectView, service: BsModalService): Observable<MayHaveLabel | false> {
    // Integracion de los parametros de la modal antigua con la nueva
    const goodConfig = config as UIModalSelect;
    if (Object.prototype.hasOwnProperty.call(config, 'label')) {
      goodConfig.inputFilterLabel = config.label;
    }
    /*
    const modal = service.show(UiModalSelectComponent, {
      initialState: { config },
    });
    const content = modal.content;
    return race(content.selectItem.asObservable(), content.cancel.asObservable()).pipe(first());
     */
    return this.showSelect(goodConfig, service);
  }

  showSelect(config: UIModalSelect, service: BsModalService): Observable<any> {
    const modal = service.show(NewUiModalSelectComponent, {
      initialState: { config },
      class: config.multiple ? 'modal-lg' : '',
    });
    const content = modal.content as NewUiModalSelectComponent;
    return race(content.selectedEvent.asObservable(), content.canceledEvent.asObservable()).pipe(first());
  }

  showSelectRules(config: UIModalSelect, service: BsModalService): Observable<any> {
    const modal = service.show(UiModalSelectRulesComponent, {
      initialState: { config },
      class: config.multiple || config.lg ? 'modal-lg' : '',
    });
    const content = modal.content as UiModalSelectRulesComponent;
    return race(content.selectedEvent.asObservable(), content.canceledEvent.asObservable()).pipe(first());
  }

  showSelectTexts(config: UIModalSelectTextsView, service: BsModalService): Observable<string[]> {
    const modal = service.show(UiModalSelectTextsComponent, {
      initialState: { config },
    });
    const content = modal.content as UiModalSelectTextsComponent;
    return race(content.selectItems.asObservable(), content.cancel.asObservable()).pipe(first());
  }

  showSelectDate(config: UIModalSelectTextsView, service: BsModalService): Observable<string[]> {
    const modal = service.show(UiModalSelectDateComponent, {
      initialState: { config },
    });
    const content = modal.content as UiModalSelectDateComponent;
    return race(content.selectItems.asObservable(), content.cancel.asObservable()).pipe(first());
  }

  showI18N(config: UIModalI18N, service: BsModalService): Observable<{ [key: string]: string }> {
    const modal = service.show(UiModalI18nComponent, {
      initialState: { config },
    });
    const content = modal.content as UiModalI18nComponent;
    const selectItems = content.selectItems as EventEmitter<{ [key: string]: string }>;
    const cancel = content.cancel as EventEmitter<{ [key: string]: string }>;
    return race(selectItems.asObservable(), cancel.asObservable()).pipe(first());
  }

  showKeyValue(config: UIModalI18N, service: BsModalService): Observable<{ [key: string]: string }> {
    const modal = service.show(UiModalKeyValueComponent, {
      initialState: { config },
    });
    const content = modal.content as UiModalKeyValueComponent;
    const selectItems = content.selectItems as EventEmitter<{ [key: string]: string }>;
    const cancel = content.cancel as EventEmitter<{ [key: string]: string }>;
    return race(selectItems.asObservable(), cancel.asObservable()).pipe(first());
  }

  showKeyValueExperts(config: UIModalI18N, service: BsModalService): Observable<{ [key: string]: string }> {
    const modal = service.show(UiModalKeyValueExpertsComponent, {
      initialState: { config },
    });
    const content = modal.content as UiModalKeyValueExpertsComponent;
    const selectItems = content.selectItems as EventEmitter<{ [key: string]: string }>;
    const cancel = content.cancel as EventEmitter<{ [key: string]: string }>;
    return race(selectItems.asObservable(), cancel.asObservable()).pipe(first());
  }

  showRange(config: UIModalRange, service: BsModalService): Observable<{ [key: string]: string }> {
    const modal = service.show(UiModalRangeComponent, {
      initialState: { config },
    });
    const content = modal.content as UiModalRangeComponent;
    const selectItems = content.selectItems as EventEmitter<{ [key: string]: string }>;
    const cancel = content.cancel as EventEmitter<{ [key: string]: string }>;
    return race(selectItems.asObservable(), cancel.asObservable()).pipe(first());
  }

  showTree(config: UIModalTree, service: BsModalService): Observable<any> {
    const modal = service.show(UiModalSelectTreeComponent, {
      initialState: { config },
      class: config.multiple ? 'modal-lg' : '',
    });
    const content = modal.content as UiModalSelectTreeComponent;
    return race(content.selectedEvent.asObservable(), content.canceledEvent.asObservable()).pipe(first());
  }

  showTreeFolder(config: UIModalTree, service: BsModalService): Observable<any> {
    const modal = service.show(UiModalSelectTreeFolderComponent, {
      initialState: { config },
      class: config.multiple ? 'modal-lg' : '',
    });
    const content = modal.content as UiModalSelectTreeFolderComponent;
    return race(content.selectedEvent.asObservable(), content.canceledEvent.asObservable()).pipe(first());
  }

  showInfo(config: UIModalInfo, service: BsModalService) {
    service.show(UiModalInfoComponent, {
      initialState: { config },
    });
    return race();
  }

  showSelectProducts(config: UIModalSelectProducts, service: BsModalService): Observable<any> {
    const modal = service.show(UiModalSelectProductsComponent, {
      initialState: { config },
    });
    const content = modal.content as UiModalSelectProductsComponent;
    return race(content.selectItems.asObservable(), content.cancel.asObservable()).pipe(first());
  }

  showSelectSubsites(config: UIModalSelectSubsites, service: BsModalService) {
    const modal = service.show(UiModalSelectSubsitesComponent, {
      initialState: { config },
      class: 'modal-lg',
    });
    const content = modal.content as UiModalSelectSubsitesComponent;
    return race(content.selectedEvent.asObservable(), content.canceledEvent.asObservable()).pipe(first());
  }

  showRulesPreview(config: ConfigPreview, service: BsModalService): Observable<any> {
    const modal = service.show(UiModalRulesPreviewComponent, {
      initialState: { config },
      class: 'modal-md',
    });
    const content = modal.content as UiModalRulesPreviewComponent;
    return race(content.selectedEvent.asObservable()).pipe(first());
  }

  showSitePreview(sitesRulesPrevew: SitesPreview, service: BsModalService): Observable<any> {
    const modal = service.show(UiModalSitesPreviewComponent, {
      initialState: { sitesRulesPrevew },
      class: 'modal-md',
    });
    const content = modal.content as UiModalSitesPreviewComponent;
    return race(content.selectedEvent.asObservable()).pipe(first());
  }
}
