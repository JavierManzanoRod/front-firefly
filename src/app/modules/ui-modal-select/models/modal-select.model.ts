import { ValidatorFn } from '@angular/forms';
import { GenericApiRequest, GenericApiResponse, MayHaveLabel } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { AttributeTranslations } from 'src/app/catalog/attribute-translation/models/attribute-translation.model';
import { ExplodedTree } from '../../exploded-tree/models/exploded-tree.model';

export interface MayHaveLabelPayLoad<T> {
  label?: string;
  name?: string;
  id?: string;
  payload?: T | null;
}

export interface UIModalSelectView {
  title: string;
  label: string;
  searchFn?: (x: GenericApiRequest) => Observable<GenericApiResponse<MayHaveLabel>>;
  suffixResults: string;
  itemValueKey?: string;
}

export interface UIModalSelectTextsView {
  title: string;
  items?: string[];
  inputFilterPlaceholder?: string;
  description?: string;
  /** Muestra el boton para ordenar los textos */
  showOrder?: boolean;
  /** Cambia el boton de ordenar al texto especificado */
  orderText?: string;
  /** Array de validaciones que tiene que pasar el texto escrito por el usuario para poder ser añadido */
  inputValidators?: ValidatorFn | ValidatorFn[] | null;
  /** Array de mensajes de error, necesario junto con inputValidators */
  validatorErrors?: { type: string; message: string }[];
  /** Funcion custom para ordenar los textos añadidos por el usuario */
  orderFunction?: (a: string, b: string) => number;
  canImportCSV?: boolean;
  importCSVLabel?: string;
}

export interface UIModalSelect {
  /** Oculta/Muestras el input que se usa para los filtros/busquedas (Default: true) */
  showInputFilter?: boolean;
  /** Placeholder del input */
  inputFilterPlaceholder?: string;
  /** Label del input */
  inputFilterLabel?: string;
  chips?: { value: boolean; key: string; name: string }[];
  title?: string;
  /** Label del numero de elementos (Default: X Elementos) */
  nItemsLabel?: string;
  /** Label del numero de elementos seleccionados (Default: X Elementos) */
  nItemsSelectedLabel?: string;
  /** Listado de posibles items a seleccionar, Ignorado si se especifica searchFn */
  items?: MayHaveLabel[];
  /** Listado de valores ya seleccionados, se ignora si se usan los formularios de angular */
  selected?: MayHaveLabel[] | MayHaveLabel;
  /** Si es true se pueden seleccionar multiples items */
  multiple?: boolean;
  /** Define un array de posibles claves a usar para pintar el label de los posibles items a seleccionar (Default: ['name', 'label']) */
  itemLabelKey?: string[];
  /** Define la clave a buscar que se usara como campo identificador para diferenciar los items seleccionados (Default: 'id') */
  itemValueKey?: string;
  /** Define el texto del boton de cerrar modal (Default: 'Cancelar') */
  cancelBtnText?: string;
  /** Define el texto del boton de aceptar los cambios (Default: 'Seleccionar') */
  okBtnText?: string;
  /** Define el icono que se va a mostrar a la izquierda del texto de los items (Default: 'icon-file') */
  icon?: string;
  keyTooltip?: string;
  selectFilterLabel?: string;
  selectFilterItems?: { label: string; value: any }[];
  selectFilterValueKey?: string;
  suffixResults?: string;
  /** Función a la que se llama cada vez que el usuario escribe en el input, que debe devolver los resultados */
  searchFn?: (x: GenericApiRequest, checkbox?: { [key: string]: boolean }) => Observable<GenericApiResponse<MayHaveLabel>>;
  /** Nombre del parametro que se usa en la busqueda */
  searchParam?: string;
  /** Si esta a true se lanza el searchFn al cargar el componente */
  searchFnOnInit?: boolean;
  /** Muestras los checkboxs como filtros, estos se devuelven en searchFn */
  checkboxs?: { value: boolean; key: string; name: string }[];
  checkboxsLabel?: string;
  data?: any;
  /**Funcion que va a filtrar el input de busqueda, si no cumple la condicion no se lanza */
  filterFn?: (x: string) => boolean;
  labelFilter?: (item: any, value: string) => string;
  clickFilter?: (item: any) => boolean;
  classFilter?: (item: any) => string;
  lg?: boolean;
}

export interface UIModalTree {
  showInputFilter?: boolean;
  /** Placeholder del input */
  inputFilterPlaceholder?: string;
  /** Label del input */
  inputFilterLabel?: string;
  title?: string;
  /** Label del numero de elementos (Default: X Elementos) */
  nItemsLabel?: string;
  /** Label del numero de elementos seleccionados (Default: X Elementos) */
  nItemsSelectedLabel?: string;
  /** Listado de posibles items a seleccionar, Ignorado si se especifica searchFn */
  items?: MayHaveLabel[];
  /** Listado de valores ya seleccionados, se ignora si se usan los formularios de angular */
  selected?: MayHaveLabel[] | MayHaveLabel;
  /** Si es true se pueden seleccionar multiples items */
  multiple?: boolean;
  /** Define un array de posibles claves a usar para pintar el label de los posibles items a seleccionar (Default: ['name', 'label']) */
  itemLabelKey?: string[];
  /** Define la clave a buscar que se usara como campo identificador para diferenciar los items seleccionados (Default: 'id') */
  itemValueKey?: string;
  /** Define el texto del boton de cerrar modal (Default: 'Cancelar') */
  cancelBtnText?: string;
  /** Define el texto del boton de aceptar los cambios (Default: 'Seleccionar') */
  okBtnText?: string;
  /** Define el icono que se va a mostrar a la izquierda del texto de los items (Default: 'icon-file') */
  icon?: string;
  selectFilterLabel?: string;
  selectFilterItems?: { label: string; value: any }[];
  selectFilterValueKey?: string;
  suffixResults?: string;
  /** Función a la que se llama cada vez que el usuario escribe en el input, que debe devolver los resultados */
  searchFn?: (x: GenericApiRequest, checkbox?: { [key: string]: boolean }) => Observable<GenericApiResponse<MayHaveLabel>>;
  /** Si esta a true se lanza el searchFn al cargar el componente */
  searchFnOnInit?: boolean;
  /** Muestras los checkboxs como filtros, estos se devuelven en searchFn */
  checkboxs?: { value: boolean; key: string; name: string }[];
  checkboxsLabel?: string;
  data?: any;
  /**Funcion que va a filtrar el input de busqueda, si no cumple la conicion no se lanza */
  filterFn?: (x: string) => boolean;

  tree?: (request?: GenericApiRequest) => Observable<ExplodedTree[]>;
  clickNode?: (event: any) => boolean;
  iconFilter?: (item: any) => string;
}

export interface UIModalI18N {
  title?: string;
  items?: { [key: string]: string };
  /** Define el texto del boton de cerrar modal (Default: 'Cancelar') */
  cancelBtnText?: string;
  /** Define el texto del boton de aceptar los cambios (Default: 'Seleccionar') */
  okBtnText?: string;
}

export interface UIModalI18NTranslate {
  title?: string;
  items: AttributeTranslations;
  /** Define el texto del boton de cerrar modal (Default: 'Cancelar') */
  cancelBtnText?: string;
  /** Define el texto del boton de aceptar los cambios (Default: 'Seleccionar') */
  okBtnText?: string;
}

export interface UIModalKeyValue {
  title?: string;
  items?: { [key: string]: string };
  // Texto boton cancelar
  cancelBtnText?: string;
  // Texto boton ok
  okBtnText?: string;
  /** Define el label de la columna clave (Default: 'Clave') */
  keyLabel?: string;
  /** Define el label de la columna value (Default: 'Valor') */
  valueLabel?: string;
}

export type UIModalRangeValue = {
  [key: string]: { from?: number; to?: number };
} & { select: string | string[] };

export interface UIModalRange {
  title?: string;
  selectOptions?: { [key: string]: string };
  selectLabel?: string;
  selectPlaceholder?: string;
  selectMultiple?: boolean;
  cancelBtnText?: string;
  okBtnText?: string;
  value?: UIModalRangeValue[];
  message?: string;
  ranges?: {
    key: string;
    label: string;
    prefix?: string;
    startLabel: string;
    endLabel: string;
    autcomplete?: boolean;
    error?: string;
    max?: number;
    min?: number;
    placeholder?: string;
  }[];
}

export interface UIModalSelectSubsites {
  // Productos hijos ya seleccionados
  selectedSubsites?: any[];
  // Texto boton cancelar
  cancelBtnText?: string;
  // Texto boton ok
  okBtnText?: string;
}

export interface UIModalSelectProducts {
  // Producto padre
  parentProduct: any;
  // Productos hijos ya seleccionados
  selectedProducts?: any[];
  // Texto boton cancelar
  cancelBtnText?: string;
  // Texto boton ok
  okBtnText?: string;
}

export interface UIModalInfo {
  title: string;
  prefix: string;
  headLabels?: string[];
  searchPlaceholder?: string;
  searchFn?: () => Observable<MayHaveLabel>;
}
