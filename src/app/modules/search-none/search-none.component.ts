import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ff-search-none',
  templateUrl: './search-none.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./search-none.component.scss'],
})
export class SearchNoneComponent implements OnInit {
  @Input() title = 'name';
  @Input() numberItems = 0;
  @Input() loading = false;

  constructor() {}

  ngOnInit(): void {}
}
