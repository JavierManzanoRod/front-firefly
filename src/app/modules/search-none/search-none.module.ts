import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PluralizedPipe } from './pluralized.pipe';
import { SearchNoneComponent } from './search-none.component';

@NgModule({
  declarations: [SearchNoneComponent, PluralizedPipe],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  exports: [SearchNoneComponent],
})
export class SearchNoneModule {}
