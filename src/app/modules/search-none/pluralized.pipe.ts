import { I18nPluralPipe, NgLocalization } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

type PluralsMap = {
  [count: string]: string;
};

export type ValuesType = {
  total: number;
} & { [key: string]: any };

@Pipe({
  name: 'translatePlural',
})
export class PluralizedPipe implements PipeTransform {
  constructor(private _localization: NgLocalization, private translate: TranslateService) {}

  public transform(key: string, value: ValuesType, locale?: string): string {
    if (!value.total) {
      throw new Error(
        'The PluralizedPipe needs to report the quantity to perform the pluralize, please insert the "total" property. Example: translatePlural:{total: 232}'
      );
    }
    const i18nPluralPipe = new I18nPluralPipe(this._localization);

    const plurals: PluralsMap = {
      '=0': `${key}.ZERO`,
      '=1': `${key}.ONE`,
      other: `${key}.OTHER`,
    };
    return this.translate.instant(i18nPluralPipe.transform(value.total, plurals, locale), value);
  }
}
