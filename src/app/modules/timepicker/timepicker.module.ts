import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimepickerComponent } from './timepicker/timepicker.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DigitOnlyModule } from '@uiowa/digit-only';
@NgModule({
  declarations: [TimepickerComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, DigitOnlyModule],
  exports: [TimepickerComponent],
})
export class TimepickerModule {}
