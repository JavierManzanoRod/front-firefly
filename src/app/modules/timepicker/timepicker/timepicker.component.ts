import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
} from '@angular/forms';
import { map, takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { DestroyService } from 'src/app/shared/services/destroy.service';

interface TimeObject {
  hours: string;
  minutes: string;
  seconds: string;
}

@Component({
  selector: 'ff-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TimepickerComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: TimepickerComponent,
      multi: true,
    },
    DestroyService,
  ],
})
export class TimepickerComponent implements OnInit, ControlValueAccessor {
  @Input() set disabled(val: boolean) {
    this._disabled = val;
    this.disableForm(val);
  }
  get disabled() {
    return this._disabled;
  }

  @Input() placeholderHours = 'hh';
  @Input() placeholderMinutes = 'mm';
  @Input() placeholderSeconds = 'ss';
  @Input() showUnit: [boolean, boolean, boolean] = [true, true, true];
  @Input() cancelButton = false;
  @Output() timeChange = new EventEmitter<Date>();
  public form = new FormGroup(
    {
      hours: new FormControl(null, { updateOn: 'blur' }),
      minutes: new FormControl(null, { updateOn: 'blur' }),
      seconds: new FormControl(null, { updateOn: 'blur' }),
    },
    { validators: this.validateInternal.bind(this) }
  );
  pattern = /^(\d{1,2})?$/;
  get showHours() {
    return this.showUnit[0];
  }
  get showMinutes() {
    return this.showUnit[1];
  }
  get showSeconds() {
    return this.showUnit[2];
  }

  get hours() {
    return this.form?.get('hours') as AbstractControl;
  }
  get minutes() {
    return this.form?.get('minutes') as AbstractControl;
  }
  get seconds() {
    return this.form?.get('seconds') as AbstractControl;
  }

  public maxHours = 23;
  public minTime = 0;
  public maxMinutes = 59;
  private _disabled = false;
  constructor(private ref: ChangeDetectorRef, private destroyService: DestroyService) {}

  ngOnInit(): void {
    this.form.valueChanges
      .pipe(
        map((val: TimeObject | null) => {
          if (val != null && this.allInputsFilled(val)) {
            return this.timeObject2Date(val);
          }
          return null;
        }),
        distinctUntilChanged(),
        takeUntil(this.destroyService)
      )
      .subscribe((val) => {
        const newValue = val ? new Date(val) : val;
        this.onChange(val);
        this.timeChange.emit(newValue as Date);
      });
  }

  onTouch = () => {};
  onChange = (data: any) => {};
  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouch = fn;
  }

  writeValue(date: Date): void {
    if (this.isValidDate(date)) {
      const [hours, minutes, seconds] = this.getPaddedTime(date);
      this.form.setValue(
        {
          hours,
          minutes,
          seconds,
        },
        { emitEvent: false }
      );
    } else {
      this.reset();
    }
    this.ref.markForCheck();
  }

  setDisabledState(isDisabled: boolean) {
    this.disableForm(isDisabled);
    this._disabled = isDisabled;
    this.ref.detectChanges();
  }

  disableForm(state: boolean) {
    if (state) {
      this.form.disable({ emitEvent: false });
    } else {
      this.form.enable({ emitEvent: false });
    }
  }

  reset() {
    this.form.reset();
    this.form.updateValueAndValidity();
    this.onTouch();
    this.ref.detectChanges();
  }
  /**
   *  Validate form to show invalid state (red lines)
   *  Data comes from form directly (TimeObject)
   *
   * @param param0
   * @returns
   */
  validateInternal({ value }: AbstractControl) {
    const validDate = this.allInputsNull(value) || this.allInputsFilled(value);
    return validDate
      ? null
      : {
          invalidDate: true,
        };
  }
  /**
   * Validate form to pass error to parent form, its called from parent
   * Data comes from subscribtion (null|Date)
   *
   * @param param0
   * @returns
   */
  validate({ value }: AbstractControl): ValidationErrors | null {
    if (!value) {
      return null;
    }
    const validDate = this.isValidDate(value) || this.allInputsFilled(this.form.value);
    return validDate
      ? null
      : {
          invalidDate: true,
        };
  }

  /**
   * Padd with 0 input if only one character written
   *
   * @param name
   */
  transform(name: string) {
    const form = this.form.get(name) as AbstractControl;
    const value: string = form.value;
    if (value?.length === 1) {
      form?.setValue(this.getPaddedNumber(value));
    }
  }
  private timeObject2Date({ hours, minutes, seconds }: TimeObject): Date {
    const d = new Date();
    d.setHours(+hours ?? 0);
    d.setMinutes(+minutes ?? 0);
    d.setSeconds(+seconds ?? 0);
    return d;
  }
  /**
   * Transform Date into form values (hours,minutes,seconds) padded
   *
   * @param date
   * @returns
   */
  private getPaddedTime(date: Date) {
    return [this.getPaddedNumber(date.getHours()), this.getPaddedNumber(date.getMinutes()), this.getPaddedNumber(date.getSeconds())];
  }
  private getPaddedNumber(n: number | string) {
    return n.toString().padStart(2, '0');
  }
  private isValidDate(d: any): d is Date {
    return d instanceof Date;
  }
  /**
   * Check if input that are shown have value.
   *
   * @param value    *
   * @returns
   */
  private allInputsFilled(value: TimeObject | Date) {
    const { hours, minutes, seconds } = this.isValidDate(value)
      ? { hours: value.getHours(), minutes: value.getMinutes(), seconds: value.getSeconds() }
      : value;

    const [hourReq, minReq, secReq] = this.showUnit;
    const hourMatch = (hourReq && hours) || !hourReq;
    const minMatch = (minReq && minutes) || !minReq;
    const secMatch = (secReq && seconds) || !secReq;
    return hourMatch && minMatch && secMatch;
  }
  private allInputsNull(value: TimeObject | Date) {
    const { hours, minutes, seconds } = this.isValidDate(value)
      ? { hours: value.getHours(), minutes: value.getMinutes(), seconds: value.getSeconds() }
      : value;

    const [hourReq, minReq, secReq] = this.showUnit;
    const hourMatch = (!hours && hourReq) || !hourReq;
    const minMatch = (!minutes && minReq) || !minReq;
    const secMatch = (!seconds && secReq) || !secReq;
    return hourMatch && minMatch && secMatch;
  }
}
