import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { PaginationModule as BTPaginationModule } from 'ngx-bootstrap/pagination';
import { Pagination2Component } from './pagination-2.component';

@NgModule({
  declarations: [Pagination2Component],
  imports: [FormsModule, CommonModule, TranslateModule, BTPaginationModule.forRoot()],
  exports: [Pagination2Component],
})
export class Pagination2Module {}
