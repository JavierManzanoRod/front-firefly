import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

@Component({
  selector: 'ff-pagination-2',
  templateUrl: './pagination-2.component.html',
  styleUrls: ['./pagination-2.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class Pagination2Component {
  @Output() pageChanged: EventEmitter<PageChangedEvent> = new EventEmitter<PageChangedEvent>();
  @Output() currentPageChange: EventEmitter<number> = new EventEmitter<number>();
  @Input() totalItems!: number;
  @Input() currentPage = 1;
  @Input() pageSize = 10;
  @ViewChild('pageManual', { read: ElementRef }) inputText!: ElementRef<HTMLInputElement>;
  totalPages = 1;

  constructor() {}

  onPageChanged(event: PageChangedEvent) {
    setTimeout(() => {
      this.pageChanged.emit(event);
      this.currentPage = event.page;
      this.currentPageChange.emit(this.currentPage);
      this.inputText.nativeElement.value = '';
    });
  }

  onPageChangedManual(page: number) {
    if (page && page >= 0 && page <= this.totalPages) {
      const event: PageChangedEvent = { itemsPerPage: this.pageSize, page: +page };
      this.onPageChanged(event);
    }
  }

  checkPoint(e: any) {
    const typedValue = e.key || e.keyCode;
    const exceptions = ['Backspace', 'Ennter', '-'];
    if ((typedValue < 48 && typedValue > 57) || exceptions.includes(e.key)) {
      // If the value is not a number, we skip the min/max comparison
      return;
    }

    const typedNumber = parseInt(e.key, 10);

    const min = parseInt(e.target?.min, 10);
    const max = parseInt(e.target?.max, 10);
    const currentVal = parseInt(e.target?.value, 10) || '';
    const newVal = parseInt(currentVal.toString() + typedNumber.toString(), 10);

    if (newVal < min || newVal > max) {
      e.preventDefault();
      e.stopPropagation();
    }
  }
}
