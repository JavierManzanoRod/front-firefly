export interface IdentityClaims {
  groups: string;
  preferred_username: string;
  sub: string;
}
