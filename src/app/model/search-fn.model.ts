import { Observable } from 'rxjs';
import { MayHaveLabel } from './base-api.model';

export type SelectSearchFn = (x: string) => Observable<MayHaveLabel[]>;
