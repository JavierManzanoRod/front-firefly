export interface Page {
  ordenation_fields?: OrdenationFields[];
  page_number: number;
  page_size: number;
  total_elements: number;
  total_pages: number;
}

interface OrdenationFields {
  field: string;
  ordenation: string;
}

export interface GenericApiResponse<T> {
  content: T[];
  page: Page;
}

export interface BusinessApiResponse<T> {
  business_object: T[];
  pagination_object: Page;
}
export interface MayHaveIdName {
  [key: string]: any;
  name?: string;
  id?: string;
  /**
   * promotions- departments has department_id as name
   */
  department_id?: string;
  /**
   * promotions- prioritylevels has priority as name
   */
  priority?: number | string;
  /**
   * sites - Sites has priority as name
   */
  site_id?: string;
  /**
   * facet - Facets
   */
  id_facet?: string;
  /**
   * dashboard - query
   */
  product_id?: string;
}

export interface MayHaveLabel extends MayHaveIdName {
  label?: string;
  name?: string;
  icon?: string;
}

export interface MayHaveLabelAndName {
  label?: string;
  attribute_label?: string;
  name?: string;
  hidden?: boolean;
  attribute_data_type?: string;
}

export interface BusinessApiResponse<T> {
  business_object: T[];
  pagination_object: Page;
}

export interface GenericApiRequest {
  page?: number;
  size?: number;
  name?: string;
  department_id?: string;
  page_number?: number;
  page_size?: number;
  status?: string;
  /**
   * @deprecated
   */
  id_attribute?: string;
  attribute_id?: string;
  scope?: string;
  type?: string;
  attribute_name?: string;
  products_name?: string;
  brand?: string;
  isManageable?: boolean;
  ean?: string;
  title?: string;
  id?: string;
  site?: string;
  shop?: string;
  filename?: string;
  admin_group_type_id?: string;
  is_market_place?: boolean;
}

export interface ModalEngineService {
  show: any;
}
