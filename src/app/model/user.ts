export interface User {
  at_hash?: string;

  /**
   * sort name
   */
  sub?: string;
  aud?: string[];
  azp?: string;
  iss?: string;
  given_name?: string;
  groups?: string;
  preferred_username?: string;
  exp?: string;
  role: RolesUser[] | string;
  nonce?: string;
  iat?: string;
}

export enum RolesUser {
  ADMIN = 'B2E/AQF2ADGlobal',
  BUSINESS = 'B2E/AQF2NEGlobal',
  READ = 'B2E/AQF2REGlobal',
}
