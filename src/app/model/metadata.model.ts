export interface Page {
  ordenation_fields?: OrdenationFields[];
  page_number: number;
  page_size: number;
  total_elements: number;
  total_pages: number;
}

interface OrdenationFields {
  field: string;
  ordenation: string;
}
