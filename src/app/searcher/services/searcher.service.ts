import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_PRODUCTS_MANAGE_SCHEMA } from 'src/app/configuration/tokens/api-versions.token';

@Injectable({
  providedIn: 'root',
})
export class SearcherService {
  constructor(private http: HttpClient, @Inject(FF_API_PATH_VERSION_PRODUCTS_MANAGE_SCHEMA) private version: string) {}

  uploadSchema(file: any): Observable<any> {
    const headers = new HttpHeaders();
    const formData = new FormData();
    formData.append('file', file);
    return this.http
      .post(`${environment.API_URL_BACKOFFICE}/products/backoffice-searcher-schemas/${this.version}products/manage-schemas`, formData, {
        headers,
      })
      .pipe(
        map((res) => {
          return {
            success: true,
            data: res,
          };
        })
      );
  }
}
