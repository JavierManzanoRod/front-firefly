import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from '../modules/common-modal/common-modal.module';
import { ToastModule } from '../modules/toast/toast.module';
import { SharedModule } from '../shared/shared.module';
import { FileUploadModule } from './../file-upload/components/file-upload.module';
import { SearcherViewContainerComponent } from './containers/searcher-view/searcher-view-container.component';
import { SearcherRoutingModule } from './searcher-routing.module';

@NgModule({
  declarations: [SearcherViewContainerComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SearcherRoutingModule,
    CommonModalModule,
    SharedModule,
    ToastModule,
    SimplebarAngularModule,
    FileUploadModule,
  ],
})
export class SearcherModule {}
