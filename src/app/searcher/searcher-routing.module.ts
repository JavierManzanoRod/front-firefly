import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearcherViewContainerComponent } from './containers/searcher-view/searcher-view-container.component';

const routes: Routes = [
  {
    path: '',
    component: SearcherViewContainerComponent,
    data: {
      header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearcherRoutingModule {}
