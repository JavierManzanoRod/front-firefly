import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ToastModule } from 'src/app/modules/toast/toast.module';
import { SearcherService } from './../../services/searcher.service';
import { SearcherViewContainerComponent } from './searcher-view-container.component';

describe('SearcherViewComponent', () => {
  let component: SearcherViewContainerComponent;
  let fixture: ComponentFixture<SearcherViewContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearcherViewContainerComponent],
      imports: [TranslateModule.forRoot(), ToastModule],
      providers: [
        {
          provide: SearcherService,
          useFactory: () => ({
            uploadSchema: () => {},
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearcherViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
