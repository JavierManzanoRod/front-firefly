import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { SearcherService } from './../../services/searcher.service';

@Component({
  selector: 'ff-searcher-view',
  template: `<ff-page-header [pageTitle]="'SEARCHER.TITLE' | translate"> </ff-page-header>
    <ngx-simplebar class="page-container">
      <div class="page-container-padding">
        <div class="row">
          <div class="col-12">
            <div class="page-section">
              <ng-container>
                <div class="row" *ngIf="!loading">
                  <div class="col upload-file">
                    <div class="col-4">
                      <ff-file-upload [key]="'file'" [eventClean]="clean" (fileLoad)="file = $event"></ff-file-upload>
                    </div>
                  </div>
                </div>
                <div class="text-center pt-5 pb-5" *ngIf="loading">
                  <div class="loader"></div>
                </div>
              </ng-container>
            </div>
          </div>
        </div>
        <div class="text-right page-footer">
          <button type="submit" (click)="form.reset(); clean.next(null)" class="btn btn-secondary ml-2 btn-sm">
            {{ 'SEARCHER.CLEAN' | translate | uppercase }}
          </button>
          <button type="submit" (click)="submit()" class="btn btn-primary ml-2 btn-sm">
            {{ 'SEARCHER.SAVE' | translate | uppercase }}
          </button>
        </div>
      </div>
    </ngx-simplebar> `,
  styleUrls: ['./searcher-view-container.component.scss'],
})
export class SearcherViewContainerComponent implements OnInit {
  form = new FormGroup({
    file: new FormControl(),
  });

  file!: any;
  loading = false;
  clean = new Subject<any>();

  constructor(private searcherService: SearcherService, private toast: ToastService, private translate: TranslateService) {}

  ngOnInit(): void {}

  submit() {
    this.form.markAllAsTouched();
    if (this.form.valid && this.file) {
      this.loading = true;
      this.searcherService.uploadSchema(this.file).subscribe(
        () => {
          this.toast.success(this.translate.instant('COMMON.SUCCESS'));
          this.loading = false;
        },
        () => {
          this.toast.error(this.translate.instant('COMMON_ERRORS.SAVE_ERROR'));
          this.loading = false;
        }
      );
    }
  }
}
