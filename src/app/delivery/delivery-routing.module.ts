import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
  },
  {
    path: 'scopes',
    loadChildren: () => import('./scope/scope.module').then((m) => m.ScopeModule),
  },
  {
    path: 'targets',
    loadChildren: () => import('./target/target.module').then((m) => m.TargetModule),
  },
  // {
  //   path: 'shipping-cost',
  //   loadChildren: () =>
  //     import('./shipping-cost/shipping-cost.module').then(m => m.ShippingCostModule)
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeliveryRoutingModule {}
