import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { FF_OBS_CITIES, FF_OBS_COUNTRIES } from 'src/app/configuration/tokens/delivery.token';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { Entity } from 'src/app/rule-engine/entity/models/entity.model';
import { ScopeService } from '../services/scope.service';
import { ScopeDetailContainerComponent } from './scope-detail-container.component';

describe('ScopeDetailContainerComponent', () => {
  let fixture: ComponentFixture<ScopeDetailContainerComponent>;
  let component: ScopeDetailContainerComponent;
  let routerService: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ScopeDetailContainerComponent],
      imports: [RouterTestingModule.withRoutes([]), TranslateModule.forRoot()],
      providers: [
        { provide: ScopeService, useFactory: () => ({}) },
        { provide: ToastService, useFactory: () => ({}) },
        {
          provide: CrudOperationsService,
          useFactory: () => ({
            updateOrSaveModal: () => of({}),
          }),
        },
        {
          provide: FF_OBS_CITIES,
          useValue: of([{ name: 'Ciudad 1' }, { name: 'Ciudad 2' }] as unknown as Entity[]),
        },
        {
          provide: FF_OBS_COUNTRIES,
          useValue: of([
            { name: 'Pais 1', group: null, attributes: [] },
            { name: 'Pais 2', group: null, attributes: [] },
          ] as unknown as Entity[]),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(ScopeDetailContainerComponent);
    routerService = TestBed.inject(Router);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('handleSubmitForm post', () => {
    const utilsService = fixture.debugElement.injector.get(CrudOperationsService);
    const spyRouter = spyOn(routerService, 'navigate');
    const s = spyOn(utilsService, 'updateOrSaveModal').and.returnValue(
      of({
        status: 200,
        data: null,
      })
    );

    component.handleSubmitForm({
      scope: 'prueba scope',
      countries: ['country1', 'country2'],
      cities: ['city1', 'city2'],
    });
    expect(s).toHaveBeenCalled();
    expect(spyRouter).toHaveBeenCalled();
  });

  it('handleSubmitForm put', () => {
    const utilsService = fixture.debugElement.injector.get(CrudOperationsService);
    const spyRouter = spyOn(routerService, 'navigate');
    const s = spyOn(utilsService, 'updateOrSaveModal').and.returnValue(
      of({
        status: 201,
        data: null,
      })
    );

    component.handleSubmitForm({
      scope: 'prueba scope',
      countries: ['country1', 'country2'],
      cities: ['city1', 'city2'],
      id: '9',
    });
    expect(s).toHaveBeenCalled();
    expect(spyRouter).toHaveBeenCalledTimes(0);
  });
});
