import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { GenericApiRequest } from '@model/base-api.model';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { Scope } from '../models/scope.model';
import { ScopeService } from '../services/scope.service';

@Component({
  selector: 'ff-scope-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header pageTitle="{{ 'SCOPE.SCOPE_LIST' | translate }}">
        <a [routerLink]="['/delivery/scopes/new']" class="btn btn-primary">
          {{ 'SCOPE.CREATE_BUTTON' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [list]="list$ | async"
                [loading]="loading"
                [page]="page"
                [valueSearch]="searchName"
                [currentData]="currentData"
                (delete)="delete($event)"
                (pagination)="handlePagination($event)"
                [placeHolder]="'SCOPE.AMBIT_NAME'"
                [title]="''"
                (search)="search($event)"
                [type]="type"
                [route]="link"
                [arrayKeys]="arrayKeys"
                [key]="'scope'"
              >
              </ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class ScopeListContainerComponent extends BaseListContainerComponent<any> implements OnInit {
  hide = false;
  public filter: GenericApiRequest = { size: 10, page: 0 };
  type = TypeSearch.simple;
  arrayKeys: GenericListConfig[] = [
    {
      key: 'scope',
      headerName: 'SCOPE.AMBIT_TABLE_NAME',
    },
  ];
  link = '/delivery/scopes/view/';

  constructor(public apiService: ScopeService, public utils: CrudOperationsService) {
    super(utils, apiService);
  }

  ngOnInit() {
    this.getDataList(this.filter);
  }

  search(criteria: any) {
    this.searchName = criteria.scope;
    this.getDataList({ scope: criteria.scope });
  }

  delete(event: Scope) {
    event.name = event.scope;
    super.delete(event);
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
