import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ScopeService } from '../services/scope.service';
import { ScopeListContainerComponent } from './scope-list-container.component';

describe('ScopeListContainerComponent', () => {
  let fixture: ComponentFixture<ScopeListContainerComponent>;
  let component: ScopeListContainerComponent;
  let routerService: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ScopeListContainerComponent],
      imports: [RouterTestingModule.withRoutes([]), TranslateModule.forRoot()],
      providers: [
        {
          provide: ScopeService,
          useFactory: () => ({
            list: () => of({}),
          }),
        },
        { provide: CrudOperationsService, useFactory: () => ({}) },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(ScopeListContainerComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('should search', () => {
    const scopeService = fixture.debugElement.injector.get(ScopeService);
    const spyApi = spyOn(scopeService, 'list').and.returnValue(of(null as any));

    component.search({ name: 'scope' });
    expect(spyApi).toHaveBeenCalled();
  });
});
