import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { FF_CITIES, FF_COUNTRIES, FF_OBS_CITIES, FF_OBS_COUNTRIES } from 'src/app/configuration/tokens/delivery.token';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { Entity } from 'src/app/rule-engine/entity/models/entity.model';
import { FormItemConfig, FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { ToastService } from '../../../modules/toast/toast.service';
import { scopeDetailForm } from '../components/scope-detail/scope-detail-form';
import { ScopeDetailComponent } from '../components/scope-detail/scope-detail.component';
import { Scope } from '../models/scope.model';
import { ScopeService } from '../services/scope.service';

@Component({
  selector: 'ff-scope-detail-container',
  template: `<ff-page-header
      [pageTitle]="!id ? ('SCOPES_DETAIL_CONTAINER.TITLE_CREATE' | translate) : ('SCOPES_DETAIL_CONTAINER.TITLE_DETAIL' | translate)"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'COMMON.RETURN_TO_LIST' | translate"
    >
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-scope-detail
        (formSubmit)="handleSubmitForm($event)"
        (delete)="delete($event)"
        [loading]="loading"
        [item]="detail$ | async"
        [editingScopes]="false"
        [formErrors]="formErrors"
        [citiesData]="citiesData"
        [countriesData]="countriesData"
        #detailScopeElement
        [form]="form"
      >
      </ff-scope-detail>
    </ngx-simplebar> `,
})
export class ScopeDetailContainerComponent extends BaseDetailContainerComponent<any> implements OnInit {
  @ViewChild('detailScopeElement', { static: false }) detailScopeElement!: ScopeDetailComponent;
  @Output() changed = new EventEmitter();

  urlListado = '/delivery/scopes';
  citiesData!: Entity[];
  countriesData!: Entity[];
  form!: FormGroup;
  formConfig: { [key: string]: FormItemConfig } = {};
  formErrors: any = {};

  constructor(
    public activeRoute: ActivatedRoute,
    public apiService: ScopeService,
    public apiScopeService: ScopeService,
    public router: Router,
    @Inject(FF_OBS_CITIES) public citySrv: Observable<Entity[]>,
    @Inject(FF_OBS_COUNTRIES) public countrySrv: Observable<Entity[]>,
    @Inject(FF_CITIES) public citiesId: string,
    @Inject(FF_COUNTRIES) public countryId: string,
    public utils: CrudOperationsService,
    public toastService: ToastService,
    public menuLeftSrv: MenuLeftService
  ) {
    super(utils, menuLeftSrv, activeRoute, router, apiService as any, toastService);
  }

  ngOnInit() {
    this.loading = true;
    this.formConfig = scopeDetailForm;
    const config = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig(this.formConfig);
    this.form = config.form;
    this.formErrors = config.errors;

    this.citySrv.subscribe((cities) => {
      this.citiesData = cities;
      this.citiesData.sort((a: Entity, b: Entity) => a.name?.localeCompare(b.name || '') as any);
    });

    this.countrySrv.subscribe((countries) => {
      this.countriesData = countries;
      this.countriesData.sort((a: Entity, b: Entity) => a.name?.localeCompare(b.name || '') as any);
      this.countriesData.forEach((country) => {
        country.group = country.attributes[this.countryId as any] as any;
      });
      this.loading = false;
    });

    super.ngOnInit();
  }

  handleSubmitForm(item: Scope) {
    const itemToSave: any = { ...item };
    itemToSave.countries = [];
    itemToSave.cities = [];
    item.countries?.forEach((country: any) => itemToSave.countries.push(country.name));
    item.cities?.forEach((city: any) => itemToSave.cities.push(city.name));
    itemToSave.name = itemToSave.scope;
    this.utils
      .updateOrSaveModal(
        {
          apiService: this.apiService as any,
        },
        itemToSave
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          if (e.status === 409) {
            this.detailScopeElement.form.controls.name.setErrors({ required: true });
          }
          return throwError(e);
        })
      )
      .subscribe((result) => {
        if (result.status === 200 || result.status === 201) {
          if (!itemToSave.id) {
            this.changed.emit(result.data);
            this.router.navigate([this.urlListado]);
          }
        }
      });
  }

  canDeactivate = () => {
    if (this.detailScopeElement.isDirty) {
      return this.detailScopeElement.isDirty;
    }
    return false;
  };
}
