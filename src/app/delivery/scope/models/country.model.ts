export interface Country {
  id?: string;
  entity_type_id?: string;
  name: string;
  attributes?: Record<string, unknown>;
  group?: string;
}
