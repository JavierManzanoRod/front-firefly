import { GenericApiResponse } from '@model/base-api.model';
import { City } from './city.model';
import { Country } from './country.model';

export interface Scope {
  id?: string;
  readonly scope: string;
  readonly countries?: string[] | Country[];
  readonly zip_codes?: string[];
  readonly district_codes?: string[];
  readonly cities?: string[] | City[];
  name?: string;
}

export type ScopeResponse = GenericApiResponse<Scope>;
