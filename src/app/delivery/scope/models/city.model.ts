export interface City {
  id?: string;
  entity_type_id?: string;
  name: string;
  attributes?: Record<string, any>;
}
