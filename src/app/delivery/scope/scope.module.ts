import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { LoadingModule } from 'src/app/modules/loading/loading.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ToastModule } from '../../modules/toast/toast.module';
import { GenericListComponentModule } from './../../modules/generic-list/generic-list.module';
import { ScopeDetailComponent } from './components/scope-detail/scope-detail.component';
import { ScopeDetailContainerComponent } from './containers/scope-detail-container.component';
import { ScopeListContainerComponent } from './containers/scope-list-container.component';
import { ScopeRoutingModule } from './scope-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ScopeRoutingModule,
    TypeaheadModule.forRoot(),
    CommonModalModule,
    ToastModule,
    GenericListComponentModule,
    FormErrorModule,
    ChipsControlModule,
    SimplebarAngularModule,
    CoreModule,
    LoadingModule,
  ],
  declarations: [ScopeListContainerComponent, ScopeDetailComponent, ScopeDetailContainerComponent],
  providers: [],
  exports: [],
})
export class ScopeModule {}
