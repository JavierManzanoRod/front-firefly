import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_GEOZONES_SCOPES } from 'src/app/configuration/tokens/api-versions.token';
import { Scope } from '../models/scope.model';
@Injectable({
  providedIn: 'root',
})
export class ScopeService extends ApiService<Scope> {
  endPoint = `products/backoffice-geozones/${this.version}geo-zones/scopes`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_GEOZONES_SCOPES) private version: string) {
    super();
  }

  detail(idSelected: string): any {
    return super.list({ size: 300 }).pipe(map((item) => item.content.find((element) => element.id === idSelected)));
  }
}
