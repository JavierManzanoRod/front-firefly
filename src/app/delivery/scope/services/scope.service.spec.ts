import { GenericApiResponse } from '@model/base-api.model';
import { of } from 'rxjs';
import { Scope } from '../models/scope.model';
import { ScopeService } from './scope.service';
let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: ScopeService;

const version = 'v1/';
const expectedData: Scope = { scope: 'scope' } as Scope;
const expectedList: GenericApiResponse<Scope> = {
  content: [expectedData],
  page: null as any,
};

describe('ScopeService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`ScopeService points to products/backoffice-geozones/${version}geo-zones/scopes`, () => {
    myService = new ScopeService(null as any, version);
    expect(myService.endPoint).toEqual(`products/backoffice-geozones/${version}geo-zones/scopes`);
  });

  it('ScopeService.list calls to get api method', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new ScopeService(httpClientSpy as any, version);
    const site = { scope: 'scope' } as Scope;
    myService.list(site).subscribe((response) => {});
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
