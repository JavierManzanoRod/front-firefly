import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { ScopeDetailContainerComponent } from './containers/scope-detail-container.component';
import { ScopeListContainerComponent } from './containers/scope-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: ScopeListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.SEND',
      breadcrumb: [
        {
          label: 'SCOPE.SCOPE_LIST',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'new',
        component: ScopeDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.SEND',
          title: 'new',
          breadcrumb: [
            {
              label: 'REGION.LIST_TITLE',
              url: '/delivery/scope',
            },
            {
              label: 'REGION.BREADCRUMB_ADD_NEW',
              url: '',
            },
          ],
        },
      },
      {
        path: 'view/:id',
        component: ScopeDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.SEND',
          title: 'view',
          breadcrumb: [
            {
              label: 'REGION.LIST_TITLE',
              url: '/delivery/scope',
            },
            {
              label: 'REGION.REGION',
              url: '',
            },
          ],
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScopeRoutingModule {}
