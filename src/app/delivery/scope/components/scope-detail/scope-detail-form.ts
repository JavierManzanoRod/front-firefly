import { FormConfig } from 'src/app/shared/services/form-utils.service';

export const scopeDetailForm: FormConfig = {
  scope: {
    value: null,
    validators: {
      required: true,
      pattern: {
        expression: '^[0-9a-zA-ZÀ-ÿÿ\u00f1\u00d1\u005f\u002d]*$',
        errorTranslateMessage: 'COMMON_ERRORS.NAME',
      },
      maxlength: 100,
    },
  },
  countries: {
    value: null,
  },
  cities: {
    value: null,
  },
  zip_codes: {
    value: null,
  },
  district_codes: {
    value: null,
  },
};
