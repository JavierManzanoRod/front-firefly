import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MayHaveIdName } from '@model/base-api.model';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { FormItemConfig, FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { districtCodeAndRangeValidator } from 'src/app/shared/validations/district-code.validator';
import { zipCodeAndRangeValidator } from 'src/app/shared/validations/zip-code.validator';
import { ToastService } from '../../../../modules/toast/toast.service';
import { City } from '../../models/city.model';
import { Country } from '../../models/country.model';
import { Scope } from '../../models/scope.model';

@Component({
  selector: 'ff-scope-detail',
  templateUrl: 'scope-detail.component.html',
  styleUrls: ['./scope-detail.component.scss'],
})
export class ScopeDetailComponent implements OnChanges, OnInit {
  @Input() loading!: boolean;
  @Input() item!: Scope;
  @Input() citiesData: City[] = [];
  @Input() countriesData: Country[] = [];
  @Input() editingScopes!: boolean;
  @Input() form!: FormGroup;
  @Input() formErrors: any = {};
  @Output() formSubmit: EventEmitter<Scope> = new EventEmitter<Scope>();
  @Output() delete: EventEmitter<Scope> = new EventEmitter<Scope>();
  formConfig: { [key: string]: FormItemConfig } = {};
  sended = false;

  listPostalCodesToRead = [];

  configCountries: ChipsControlSelectConfig = {
    label: 'SCOPES_DETAIL_COMPONENT.COUNTRIES',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'SCOPES_DETAIL_COMPONENT.ENTER_COUNTRIES',
      multiple: true,
      items: this.countriesData,
      selectFilterItems: [
        { label: 'Todos', value: '' },
        { label: 'Resto de Europa', value: 'Resto de Europa' },
        { label: 'Resto del mundo', value: 'Resto del Mundo' },
        { label: 'América', value: 'América' },
      ],
      selectFilterLabel: 'SCOPES_DETAIL_COMPONENT.COUNTRY_TYPE',
      selectFilterValueKey: 'group',
    },
  };

  configCities: ChipsControlSelectConfig = {
    label: 'SCOPES_DETAIL_COMPONENT.CITIES',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'SCOPES_DETAIL_COMPONENT.ENTER_CITIES',
      multiple: true,
      items: this.citiesData,
    },
  };

  configZipCodes: ChipsControlSelectConfig = {
    label: 'SCOPES_DETAIL_COMPONENT.ZIP_CODES',
    modalType: ModalChipsTypes.free,
    modalConfig: {
      title: 'SCOPES_DETAIL_COMPONENT.ENTER_ZIP_CODES',
      items: [],
      inputValidators: zipCodeAndRangeValidator(),
      validatorErrors: [{ type: 'invalidZip', message: 'COMMON_ERRORS.ZIP_CODE_ERROR' }],
    },
  };

  configDistrictCodes: ChipsControlSelectConfig = {
    label: 'SCOPES_DETAIL_COMPONENT.DISTRICT_CODES',
    modalType: ModalChipsTypes.free,
    modalConfig: {
      title: 'SCOPES_DETAIL_COMPONENT.ENTER_DISTRICT_CODES',
      items: [],
      inputValidators: districtCodeAndRangeValidator(),
      validatorErrors: [{ type: 'invalidDistrict', message: 'COMMON_ERRORS.DISTRICT_CODE_ERROR' }],
    },
  };

  get formData() {
    return {
      scope: this.form.getRawValue().scope,
      countries: this.form.value.countries,
      id: this.item?.id,
      cities: this.form.value.cities,
      zip_codes: this.form.value.zip_codes,
      district_codes: this.form.value.district_codes,
    } as Scope;
  }

  constructor(protected route: ActivatedRoute, protected router: Router, private toast: ToastService) {}

  ngOnInit() {}

  ngOnChanges() {
    if (this.configCities.modalType === ModalChipsTypes.select) {
      this.configCities.modalConfig.items = this.citiesData;
    }
    if (this.configCountries.modalType === ModalChipsTypes.select) {
      this.configCountries.modalConfig.items = this.countriesData;
    }
    if (this.item && Object.keys(this.item).length) {
      this.form.patchValue({
        ...this.item,
      });
      this.form.controls.scope.disable();
    }
    if (this.item) {
      if (this.item.countries) {
        this.setChipValues(this.item.countries as string[], this.form.controls.countries);
      }
      if (this.item.cities) {
        this.setChipValues(this.item.cities as string[], this.form.controls.cities);
      }
    }
    this.form.markAsPristine();
  }

  submit() {
    FormUtilsService.markFormGroupTouched(this.form);
    this.sended = true;
    if (this.form.valid) {
      this.formSubmit.emit(this.formData);
      this.form.markAsPristine();
    } else {
      this.toast.commonFormError();
    }
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  get isDirty(): false | UICommonModalConfig {
    if (this.form.pristine) {
      return false;
    }
    if (this.item?.id) {
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.item?.scope,
      };
    } else {
      return {};
    }
  }

  private setChipValues(items: string[], control: AbstractControl) {
    if (items.length > 0) {
      const selectedValues: MayHaveIdName[] = [];
      items.forEach((item) => selectedValues.push({ id: item, name: item }));
      control.setValue(selectedValues);
    }
  }
}
