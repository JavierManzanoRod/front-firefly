import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ToastModule } from '../../modules/toast/toast.module';
import { TargetDetailComponent } from './components/target-detail/target-detail.component';
import { TargetDetailContainerComponent } from './containers/target-detail-container.component';
import { TargetListContainerComponent } from './containers/target-list-container.component';
import { TargetRoutingModule } from './target-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TargetRoutingModule,
    GenericListComponentModule,
    ToastModule,
    TypeaheadModule.forRoot(),
    CommonModalModule,
    NgSelectModule,
    ChipsControlModule,
    FormErrorModule,
    SimplebarAngularModule,
    CoreModule,
  ],
  declarations: [TargetListContainerComponent, TargetDetailContainerComponent, TargetDetailComponent],
  providers: [],
})
export class TargetModule {}
