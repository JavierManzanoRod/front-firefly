import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_GEOZONES_TARGETS } from 'src/app/configuration/tokens/api-versions.token';
import { Target } from '../models/target.model';
interface List {
  id: string;
  name: string;
}

@Injectable({
  providedIn: 'root',
})
export class TargetService extends ApiService<Target> {
  endPoint = `products/backoffice-geozones/${this.version}geo-zones/targets`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_GEOZONES_TARGETS) private version: string) {
    super();
  }

  detail(idSelected: string): any {
    return super.list({ size: 300 }).pipe(map((item) => item.content.find((element) => element.id === idSelected)));
  }
}
