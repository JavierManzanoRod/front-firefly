import { GenericApiResponse } from '@model/base-api.model';
import { of } from 'rxjs';
import { Target } from '../models/target.model';
import { TargetService } from './target.service';
let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: TargetService;
const version = 'v1/';
const expectedData: Target = { name: 'name', gift_ticket: true, tax_type: 'IVA' } as Target;
const expectedList: GenericApiResponse<Target> = {
  content: [expectedData],
  page: null as any,
};

describe('TargetService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`TargetService points to products/backoffice-geozones/${version}geo-zones/targets`, () => {
    myService = new TargetService(null as any, version);
    expect(myService.endPoint).toEqual(`products/backoffice-geozones/${version}geo-zones/targets`);
  });

  it('TargetService.list calls to get api method', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new TargetService(httpClientSpy as any, version);
    const site = { name: 'name', gift_ticket: true, tax_type: 'IVA' } as Target;
    myService.list(site).subscribe((response) => {});
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
