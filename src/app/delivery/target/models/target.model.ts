import { GenericApiResponse } from '@model/base-api.model';
import { Scope } from '../../scope/models/scope.model';

export interface Target {
  id?: string;
  name: string;
  description?: string;
  order?: number;
  gift_ticket: boolean;
  tax_type: 'IVA' | 'IGIC' | 'Ninguno';
  line_items?: number;
  scopes?: Scope[] | string[];
  master?: boolean;
}

export interface ListTargets {
  id: string;
  scope: string;
}

export type TargetResponse = GenericApiResponse<Target>;

// export interface DestionationPayload {
//   region?: Region;
//   destination: Destination;
//   indexNewRegion: number;
// }
