import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { TargetService } from '../services/target.service';
import { TargetListContainerComponent } from './target-list-container.component';

describe('TargetListContainerComponent', () => {
  let fixture: ComponentFixture<TargetListContainerComponent>;
  let component: TargetListContainerComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TargetListContainerComponent],
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      providers: [
        { provide: ToastrService, useFactory: () => ({}) },
        { provide: BsModalService, useFactory: () => ({}) },
        {
          provide: TargetService,
          useFactory: () => ({
            list: () => {
              return of({});
            },
          }),
        },
        { provide: CrudOperationsService, useFactory: () => ({}) },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(TargetListContainerComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });
});
