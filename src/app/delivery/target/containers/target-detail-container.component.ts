import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { UpdateResponse } from '@core/base/api.service';
import { CanComponentDeactivate } from '@core/guards/can-deactivate-guard';
import { of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from '../../../modules/toast/toast.service';
import { ScopeService } from '../../scope/services/scope.service';
import { Target } from '../models/target.model';
import { TargetService } from '../services/target.service';
import { TargetDetailComponent } from './../components/target-detail/target-detail.component';

@Component({
  selector: 'ff-target-detail-container',
  template: `<ff-page-header
      [pageTitle]="!id ? ('TARGET_DETAIL_CONTAINER.TITLE_CREATE' | translate) : ('TARGET_DETAIL_CONTAINER.TITLE_DETAIL' | translate)"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'COMMON.RETURN_TO_LIST' | translate"
    >
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-target-detail
        #detailElement
        (formSubmit)="handleSubmitForm($event)"
        (delete)="delete($event)"
        [loading]="loading"
        [item]="detail$ | async"
        [editingScopes]="false"
      >
      </ff-target-detail>
    </ngx-simplebar> `,
})
export class TargetDetailContainerComponent extends BaseDetailContainerComponent<any> implements OnInit, CanComponentDeactivate {
  @ViewChild('detailElement') targetDetailElement!: TargetDetailComponent;
  @Output() changed = new EventEmitter();
  urlListado = '/delivery/targets';

  constructor(
    public activeRoute: ActivatedRoute,
    public apiService: TargetService,
    public apiScopeService: ScopeService,
    public router: Router,
    private utilsSrv: CrudOperationsService,
    public menuLeftSrv: MenuLeftService,
    public toastService: ToastService
  ) {
    super(utilsSrv, menuLeftSrv, activeRoute, router, apiService as any, toastService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  handleSubmitForm(item: Target) {
    const itemToSave = { ...item };
    itemToSave.scopes = [];
    item.scopes?.forEach((scope: any) => itemToSave.scopes?.push(scope.id));
    this.utilsSrv
      .updateOrSaveModal(
        {
          apiService: this.apiService as any,
          methodToApply: itemToSave.id ? 'PUT' : 'POST',
        },
        itemToSave
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          if (e.status === 409) {
            this.targetDetailElement.form.controls.name.setErrors({ required: true });
          }
          return throwError(e);
        })
      )
      .subscribe((result: UpdateResponse<any>) => {
        if (result.status === 200 || result.status === 201) {
          if (!itemToSave.id) {
            this.changed.emit(result.data);
            this.router.navigate([this.urlListado]);
          }
          this.detail$ = of(result.data);
        }
      });
  }

  canDeactivate() {
    if (this.targetDetailElement.isDirty) {
      return this.targetDetailElement.isDirty as any;
    }
  }
}
