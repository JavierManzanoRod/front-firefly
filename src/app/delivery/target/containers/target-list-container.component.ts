import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { GenericApiRequest } from '@model/base-api.model';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { TargetService } from '../services/target.service';

@Component({
  selector: 'ff-target-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header pageTitle="{{ 'TARGET.TARGETS_LIST' | translate }}">
        <a [routerLink]="['/delivery/targets/new']" class="btn btn-primary">
          {{ 'TARGET.CREATE_TARGET' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-container-padding">
          <ff-generic-list
            (search)="search($event)"
            [arrayKeys]="arrayConfig"
            [list]="list$ | async"
            [type]="type"
            [loading]="loading"
            [page]="page"
            [valueSearch]="searchName"
            [currentData]="currentData"
            (delete)="delete($event)"
            (pagination)="handlePagination($event)"
            [title]="''"
            [placeHolder]="'TARGET.TARGET_TABLE_NAME'"
            [route]="'/delivery/targets/view/'"
          ></ff-generic-list>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class TargetListContainerComponent extends BaseListContainerComponent<any> implements OnInit {
  hide = false;
  public filter: GenericApiRequest = { size: 10, page: 0 };

  arrayConfig: GenericListConfig[] = [
    {
      key: 'name',
      headerName: 'TARGET.TARGET_TABLE_NAME',
    },
    {
      key: 'order',
      headerName: 'TARGET.ORDER',
      textCenter: true,
    },
    {
      key: 'gift_ticket',
      headerName: 'TARGET.GIFT_TICKET',
      canActiveClass: true,
      textCenter: true,
    },
    {
      key: 'tax_type',
      headerName: 'TARGET.TAX_TYPE',
      textCenter: true,
    },
    {
      key: 'scopes.length',
      headerName: 'TARGET.AMBITS',
      textCenter: true,
    },
  ];

  type = TypeSearch.simple;

  constructor(public apiService: TargetService, public utils: CrudOperationsService) {
    super(utils, apiService as any);
  }

  ngOnInit() {
    this.getDataList(this.filter);
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
