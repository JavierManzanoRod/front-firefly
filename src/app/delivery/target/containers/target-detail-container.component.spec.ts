import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastModule } from '../../../modules/toast/toast.module';
import { ScopeService } from '../../scope/services/scope.service';
import { TargetService } from '../services/target.service';
import { TargetDetailContainerComponent } from './target-detail-container.component';

describe('TargetDetailContainerComponent', () => {
  let fixture: ComponentFixture<TargetDetailContainerComponent>;
  let component: TargetDetailContainerComponent;
  let routerService: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TargetDetailContainerComponent],
      imports: [RouterTestingModule.withRoutes([]), TranslateModule.forRoot(), ToastModule],
      providers: [
        { provide: TargetService, useFactory: () => ({}) },
        { provide: ScopeService, useFactory: () => ({}) },
        {
          provide: CrudOperationsService,
          useFactory: () => ({
            updateOrSaveModal: () => of({}),
          }),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(TargetDetailContainerComponent);
    routerService = TestBed.inject(Router);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('handleSubmitForm post', () => {
    const utilsService = fixture.debugElement.injector.get(CrudOperationsService);

    const spyRouter = spyOn(routerService, 'navigate');

    const s = spyOn(utilsService, 'updateOrSaveModal').and.returnValue(
      of({
        status: 200,
        data: null,
      })
    );
    component.handleSubmitForm({
      name: 'prueba target',
      gift_ticket: true,
      tax_type: 'IVA',
      scopes: ['scope test'],
    });
    expect(s).toHaveBeenCalled();
    expect(spyRouter).toHaveBeenCalled();
  });

  it('handleSubmitForm put', () => {
    const utilsService = fixture.debugElement.injector.get(CrudOperationsService);

    const spyRouter = spyOn(routerService, 'navigate');

    const s = spyOn(utilsService, 'updateOrSaveModal').and.returnValue(
      of({
        status: 201,
        data: null,
      })
    );
    component.handleSubmitForm({
      name: 'prueba target',
      gift_ticket: true,
      id: '12',
      tax_type: 'IVA',
    });
    expect(s).toHaveBeenCalled();
    expect(spyRouter).toHaveBeenCalledTimes(0);
  });
});
