import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericApiRequest, MayHaveIdName } from '@model/base-api.model';
import { map } from 'rxjs/operators';
import { ScopeService } from 'src/app/delivery/scope/services/scope.service';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { FormItemConfig, FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { ToastService } from '../../../../modules/toast/toast.service';
import { Target } from '../../models/target.model';
import { targetDetailForm } from './target-detail-form';

@Component({
  selector: 'ff-target-detail',
  templateUrl: 'target-detail.component.html',
  styleUrls: ['./target-detail.component.scss'],
})
export class TargetDetailComponent implements OnInit {
  @Input() editingScopes!: boolean;
  @Output() formSubmit: EventEmitter<Target> = new EventEmitter<Target>();
  @Output() delete: EventEmitter<Target> = new EventEmitter<Target>();
  @Input() loading!: boolean;
  _item!: Target;
  get item(): Target {
    return this._item;
  }

  @Input() set item(value: Target) {
    this._item = value;
    if (this._item && Object.keys(this._item).length) {
      this.form.patchValue({
        ...this._item,
      });
      this.setScopes(this._item);
      this.form.controls.name.disable();
    }
    this.changesValue = false;
    console.log(value, 'new item');
  }

  formConfig: { [key: string]: FormItemConfig } = {};
  form!: FormGroup;
  sended = false;
  formErrors: any = {};
  changesValue!: boolean;
  listPostalCodesToRead = [];

  configScopes: ChipsControlSelectConfig = {
    label: 'TARGET_DETAIL_COMPONENT.AMBITS',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'TARGET_DETAIL_COMPONENT.ENTER_AMBITS',
      multiple: true,
      searchFn: (x: GenericApiRequest) => {
        return this.apiScopeService.list(x).pipe(
          map((data) => {
            data.content.forEach((item) => (item.name = item.scope));
            return data;
          })
        );
      },
      searchFnOnInit: true,
    },
  };

  get formData() {
    return {
      name: this.form.getRawValue().name,
      description: this.form.value.description,
      gift_ticket: this.form.value.gift_ticket,
      id: this._item?.id,
      order: this.form.value.order,
      scopes: this.form.value.scopes,
      tax_type: this.form.value.tax_type,
    } as Target;
  }

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    private toast: ToastService,
    private apiScopeService: ScopeService
  ) {}

  ngOnInit() {
    this.formConfig = targetDetailForm;
    const config = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig(this.formConfig);
    this.form = config.form;
    this.formErrors = config.errors;
    this.form.valueChanges.subscribe(() => {
      this.changesValue = true;
      this.form.markAsDirty();
    });
    this.form.markAsPristine();
  }

  handleDelete() {
    this.delete.emit(this.formData);
  }

  submit() {
    FormUtilsService.markFormGroupTouched(this.form);
    this.sended = true;
    if (this.form.valid) {
      const data = this.formData;
      this.changesValue = true;
      this.form.markAsPristine();
      this.formSubmit.emit(data);
    } else {
      this.toast.commonFormError();
    }
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  get isDirty(): false | UICommonModalConfig {
    if (this.form.pristine) {
      return false;
    }

    if (this._item?.name) {
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this._item?.name,
      };
    }

    return {};
  }
  private setScopes(item: Target) {
    if (item.scopes) {
      const selectedScopes: MayHaveIdName[] = [];
      item.scopes.forEach((itemScope: any) => {
        selectedScopes.push({ id: itemScope.id, name: itemScope.scope });
      });
      this.form.controls.scopes.setValue(selectedScopes);
      this.form.markAsPristine();
    }
  }
}
