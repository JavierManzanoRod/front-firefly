import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ScopeService } from 'src/app/delivery/scope/services/scope.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { TargetDetailComponent } from './target-detail.component';

describe('TargetDetailComponent', () => {
  let fixture: ComponentFixture<TargetDetailComponent>;
  let component: TargetDetailComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TargetDetailComponent],
      imports: [FormsModule, ReactiveFormsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: ActivatedRoute,
          useFactory: () => ({}),
        },
        {
          provide: ToastService,
          useFactory: () => ({
            commonFormError: () => {},
          }),
        },
        {
          provide: ScopeService,
          useFactory: () => ({}),
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TargetDetailComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the component', () => {
    expect(component).toBeDefined();
  });

  it('init form', () => {
    component.ngOnInit();
    expect(component.form).toBeTruthy();
  });

  it('load scopes', () => {
    component.ngOnInit();
    component.item = {
      name: 'test',
      gift_ticket: true,
      tax_type: 'IGIC',
      scopes: ['123', '456'],
    };
    expect(component.form.controls.scopes.value.length).toBe(2);
  });

  it('emit event delete', () => {
    component.ngOnInit();
    const spy = spyOn(component.delete, 'emit');
    component.handleDelete();
    expect(spy).toHaveBeenCalled();
  });

  it('submit form valid', () => {
    component.form = new FormGroup({});
    const spy = spyOn(component.formSubmit, 'emit');
    component.submit();
    expect(spy).toHaveBeenCalled();
  });

  it('submit form not valid', () => {
    component.ngOnInit();
    const toastService = fixture.debugElement.injector.get(ToastService);
    const spy = spyOn(toastService, 'commonFormError');
    component.submit();
    expect(spy).toHaveBeenCalled();
  });

  it('cancel and redirect', () => {
    const router = fixture.debugElement.injector.get(Router);
    const spy = spyOn(router, 'navigate');
    component.cancel();
    expect(spy).toHaveBeenCalled();
  });
});
