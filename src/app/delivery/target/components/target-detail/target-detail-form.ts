import { FormConfig } from 'src/app/shared/services/form-utils.service';

export const targetDetailForm: FormConfig = {
  name: {
    value: null,
    validators: {
      required: true,
      pattern: {
        expression: '^[0-9a-zA-ZÀ-ÿÿ\u00f1\u00d1\u005f\u002d]*$',
        errorTranslateMessage: 'COMMON_ERRORS.NAME',
      },
      maxlength: 100,
    },
  },
  description: {
    value: null,
  },
  order: {
    value: null,
    validators: {
      min: 1,
      max: 10000,
    },
  },
  gift_ticket: {
    value: null,
    validators: {
      required: true,
    },
  },
  tax_type: {
    value: null,
    validators: {
      required: true,
    },
  },
  line_items: {
    value: null,
  },
  scopes: {
    value: null,
  },
};
