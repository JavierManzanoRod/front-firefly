import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { TargetDetailContainerComponent } from './containers/target-detail-container.component';
import { TargetListContainerComponent } from './containers/target-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: TargetListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.SEND',
      breadcrumb: [
        {
          label: 'DESTINATION.DESTINATION_LIST_TITLE',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'new',
        canDeactivate: [CanDeactivateGuard],
        component: TargetDetailContainerComponent,
        data: {
          header_title: 'MENU_LEFT.SEND',
          title: 'new',
          breadcrumb: [
            {
              label: 'DESTINATION.DESTINATION_LIST_TITLE',
              url: '/delivery/target',
            },
            {
              label: 'DESTINATION.DESTINATION_ADD_NEW',
              url: '',
            },
          ],
        },
      },
      {
        path: 'view/:id',
        canDeactivate: [CanDeactivateGuard],
        component: TargetDetailContainerComponent,
        data: {
          header_title: 'MENU_LEFT.SEND',
          title: 'view',
          breadcrumb: [
            {
              label: 'DESTINATION.DESTINATION_LIST_TITLE',
              url: '/delivery/target',
            },
            {
              label: 'DESTINATION.DESTINATION',
              url: '',
            },
          ],
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TargetRoutingModule {}
