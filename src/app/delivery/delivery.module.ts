import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DeliveryRoutingModule } from './delivery-routing.module';

@NgModule({
  imports: [CommonModule, FormsModule, DeliveryRoutingModule],
  declarations: [],
  providers: [],
  exports: [],
})
export class DeliveryModule {}
