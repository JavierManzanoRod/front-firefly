import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { DndModule } from 'ngx-drag-drop';
import { SimplebarAngularModule } from 'simplebar-angular';
import { LoadingModule } from 'src/app/modules/loading/loading.module';
import { ChipsControlModule } from '../modules/chips-control/chips-control.module';
import { DatetimepickerModule } from '../modules/datetimepicker/datetimepicker.module';
import { ExplodedTreeModule } from '../modules/exploded-tree/exploded-tree.module';
import { FormErrorModule } from '../modules/form-error/form-error.module';
import { SelectModule } from '../modules/select/select.module';
import { SharedModule } from '../shared/shared.module';
import { ExamplesRoutingModule } from './examples-routing.module';
import { ExampleChipControlComponent } from './pages/example-chip-control/example-chip-control.component';
import { ExampleDatetimepickerComponent } from './pages/example-datetimepicker/example-datetimepicker.component';
import { ExampleExplodedTreeComponent } from './pages/example-exploded-tree/example-exploded-tree.component';
import { ExampleModalRangeComponent } from './pages/example-modal-range/example-modal-range.component';
import { ExamplesFormErrorComponent } from './pages/examples-form-error/examples-form-error.component';
import { ExamplesComponent } from './pages/examples/examples.component';
import { ExampleModalesAvisosComponent } from './pages/example-modales-avisos/example-modales-avisos.component';
import { CommonModalModule } from '../modules/common-modal/common-modal.module';
import { ExampleModalTreeComponent } from './pages/example-modal-tree/example-modal-tree.component';

@NgModule({
  declarations: [
    ExamplesComponent,
    ExamplesFormErrorComponent,
    ExampleDatetimepickerComponent,
    ExampleChipControlComponent,
    ExampleExplodedTreeComponent,
    ExampleModalRangeComponent,
    ExampleModalesAvisosComponent,
    ExampleModalTreeComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LoadingModule,
    NgSelectModule,
    SelectModule,
    ExamplesRoutingModule,
    SharedModule,
    FormErrorModule,
    DatetimepickerModule,
    ChipsControlModule,
    ExplodedTreeModule,
    SimplebarAngularModule,
    DndModule,
    CommonModalModule,
  ],
  exports: [
    ExamplesComponent,
    ExamplesFormErrorComponent,
    ExampleDatetimepickerComponent,
    ExampleChipControlComponent,
    ExampleModalRangeComponent,
    ExampleExplodedTreeComponent,
    ExampleModalesAvisosComponent,
  ],
})
export class ExamplesModule {}
