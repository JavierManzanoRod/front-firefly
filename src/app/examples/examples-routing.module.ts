import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExampleChipControlComponent } from './pages/example-chip-control/example-chip-control.component';
import { ExampleDatetimepickerComponent } from './pages/example-datetimepicker/example-datetimepicker.component';
import { ExampleExplodedTreeComponent } from './pages/example-exploded-tree/example-exploded-tree.component';
import { ExampleModalRangeComponent } from './pages/example-modal-range/example-modal-range.component';
import { ExamplesFormErrorComponent } from './pages/examples-form-error/examples-form-error.component';
import { ExamplesComponent } from './pages/examples/examples.component';
import { ExampleModalesAvisosComponent } from './pages/example-modales-avisos/example-modales-avisos.component';
import { ExampleModalTreeComponent } from './pages/example-modal-tree/example-modal-tree.component';

const routes: Routes = [
  {
    path: '',
    component: ExamplesComponent,
    data: {
      header_title: 'Menu ejemplos',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'datetimepicker',
    component: ExampleDatetimepickerComponent,
    data: {
      header_title: 'Ejemplo date time picker',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'formerror',
    component: ExamplesFormErrorComponent,
    data: {
      header_title: 'Ejemplo form error',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'chips-control',
    component: ExampleChipControlComponent,
    data: {
      header_title: 'Ejemplo chips control',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'exploded-tree',
    component: ExampleExplodedTreeComponent,
    data: {
      header_title: 'Ejemplo exploded tree',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'modal-range',
    component: ExampleModalRangeComponent,
    data: {
      header_title: 'Ejemplo modal range',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'modal-tree',
    component: ExampleModalTreeComponent,
    data: {
      header_title: 'Ejemplo modal tree',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
  {
    path: 'modals-aviso',
    component: ExampleModalesAvisosComponent,
    data: {
      header_title: 'Ejemplo de modales de aviso',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExamplesRoutingModule {}
