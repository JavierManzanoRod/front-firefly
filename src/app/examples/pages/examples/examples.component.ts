import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: './examples.component.html',
})
export class ExamplesComponent {
  examples = [
    { name: 'Date time picker', url: '/examples/datetimepicker' },
    { name: 'Form error', url: '/examples/formerror' },
    { name: 'Chips Control Select', url: '/examples/chips-control' },
    { name: 'Exploded Tree', url: '/examples/exploded-tree' },
    { name: 'Modal Range', url: '/examples/modal-range' },
    { name: 'Modales de aviso', url: '/examples/modals-aviso' },
    { name: 'Modal Tree', url: '/examples/modal-tree' },
  ];

  constructor(private router: Router) {}

  goUrl(url: string) {
    this.router.navigate([url]);
  }
}
