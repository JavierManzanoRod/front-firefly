import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormUtilsService } from '../../../shared/services/form-utils.service';

@Component({
  templateUrl: './example-datetimepicker.component.html',
})
export class ExampleDatetimepickerComponent {
  form: FormGroup;
  formMessages = {
    date: [{ type: 'required', message: 'Campo fecha necesario', params: {} }],
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      date: [null, [Validators.required]],
      date2: ['2016-04-26T22:10:00.000Z', [Validators.required]],
    });
  }

  send() {
    FormUtilsService.markFormGroupTouched(this.form);
  }
}
