import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { FF_API_PATH_VERSION_EXPLODED_TREE } from 'src/app/configuration/tokens/api-versions.token';
import { ExplodedTree, ExplodedTreeNode } from 'src/app/modules/exploded-tree/models/exploded-tree.model';

@Injectable({
  providedIn: 'root',
})
export class OfferExplodedTreeService extends ApiService<ExplodedTree> {
  endPoint = `products/backoffice-exploded-trees/${this.version}exploded-trees`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_EXPLODED_TREE) private version: string) {
    super();
  }

  get(id: string): Observable<ExplodedTreeNode> {
    return this.http.get<ExplodedTreeNode>(`${this.urlBase}/${id}`);
  }

  search(filter: GenericApiRequest): Observable<GenericApiResponse<ExplodedTree>> {
    return this.http.get<GenericApiResponse<ExplodedTree>>(this.urlBase, {
      params: this.getParams(this.getFilter(filter)),
    });
  }
}
