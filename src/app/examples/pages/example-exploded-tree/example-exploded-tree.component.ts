import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import {
  ExplodedClickEvent,
  ExplodedDropEvent,
  ExplodedTree,
  ExplodedTreeElementType,
  ExplodedTreeNode,
  ExplodedTreeNodeType,
} from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { OfferExplodedTreeService } from './offer-exploded-tree.service';

type Props = {
  control: {
    pristine: boolean;
  };
};

@Component({
  templateUrl: './example-exploded-tree.component.html',
  styleUrls: ['./example-exploded-tree.component.scss'],
})
export class ExampleExplodedTreeComponent {
  offerId = '2a27030a-573a-4a4a-b234-15edf2eee679';

  tree: Subject<ExplodedTree[]> = new Subject<ExplodedTree[]>();
  clickData: ExplodedTreeNode | null = null;
  dropData: ExplodedTreeNode | null = null;
  target: ExplodedTreeNodeComponent | null = null;

  loading = true;
  path: string | undefined;

  constructor(private offerExplodedTreeService: OfferExplodedTreeService) {
    this.offerExplodedTreeService
      .get(this.offerId)
      .pipe(map((response) => response || {}))
      .subscribe((content) => this.tree.next([content]));

    this.tree.subscribe((content) => {
      this.loading = false;
    });
  }

  click(event: ExplodedClickEvent<ExplodedTreeNodeComponent>) {
    this.clickData = event.data;
    this.target = event.target;

    if (
      !event.data.children?.length &&
      event.data.node_type &&
      [ExplodedTreeNodeType.ATTRIBUTE_TREE_NODE, ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE].includes(event.data.node_type) &&
      event.data.node_id
    ) {
      event.target.setLoading(true);
      this.offerExplodedTreeService
        .get(event.data.node_id)
        .pipe(map((response) => response || {}))
        .subscribe((content) => {
          if (content) {
            event.data.children = content.children;
            event.target.expand();
            event.target.setLoading(false);
          }
        });
    }
  }

  search(value: string) {
    this.loading = true;

    this.offerExplodedTreeService
      .search({ attribute_name: value })
      .pipe(map((response) => response.content || []))
      .subscribe((content) => {
        this.tree.next(content);
        // TODO: Expanded all nodes :D
      });
  }

  drop({ data }: ExplodedDropEvent<ExplodedTreeNode>) {
    this.dropData = data as ExplodedTreeNode;
  }

  calculatePath() {
    if (!this.target) {
      return (this.path = 'Select a node');
    }

    this.path = this.target.generateFieldPath();
  }

  clickFilter(node: ExplodedTreeNodeComponent) {
    return node.data.element_type !== ExplodedTreeElementType.EMBEDDED;
  }

  iconFilter(node: ExplodedTreeNodeComponent) {
    if (node.isRoot) {
      return 'icon-folder';
    }

    if (
      node.data.children?.length ||
      (node.data.node_type &&
        [ExplodedTreeNodeType.INCLUDE_PRODUCT_TYPE, ExplodedTreeNodeType.ATTRIBUTE_TREE_NODE].includes(node.data.node_type))
    ) {
      return 'icon-atributos';
    }

    return 'icon-agrupaciones';
  }

  badgeFilter(node: ExplodedTreeNodeComponent<Props>) {
    if (node.isRoot) {
      return 'icon-folder';
    }

    if (node.data.element_type === ExplodedTreeElementType.STRING) {
      return 'info-light';
    }

    return '';
  }
}
