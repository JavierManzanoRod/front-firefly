import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GenericApiRequest } from '@model/base-api.model';
import { ChipsUIModalSelect, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { LimitSaleService } from 'src/app/stock/limit-sale/services/limit-sale.service';

@Component({
  templateUrl: './example-chip-control.component.html',
})
export class ExampleChipControlComponent implements OnInit {
  form!: FormGroup;
  chipsConfig!: ChipsUIModalSelect;

  constructor(private fb: FormBuilder, private apiService: LimitSaleService) {}

  ngOnInit() {
    this.form = this.fb.group({
      chips: [null],
    });
    this.chipsConfig = {
      label: 'Reglas de limitar ventas',
      modalType: ModalChipsTypes.select,
      modalConfig: {
        title: 'Reglas de limitar ventas',
        multiple: false,
        searchFn: (x: GenericApiRequest) => {
          return this.apiService.list({
            ...x,
          });
        },
        searchFnOnInit: true,
      },
    };
  }
}
