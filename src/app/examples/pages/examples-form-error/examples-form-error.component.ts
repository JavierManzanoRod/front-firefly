import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenericApiRequest, MayHaveLabel } from '@model/base-api.model';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../modules/chips-control/components/chips.control.model';
import { FormUtilsService } from '../../../shared/services/form-utils.service';

@Component({
  templateUrl: './examples-form-error.component.html',
})
export class ExamplesFormErrorComponent {
  form: FormGroup;
  formMessages = {
    email: [
      { type: 'required', message: 'Campo email necesario', params: {} },
      { type: 'email', message: 'No es un email valido', params: {} },
    ],
  };

  chipConfig: ChipsControlSelectConfig = {
    label: 'TARGET_DETAIL_COMPONENT.ENTER_AMBITS',
    modalType: ModalChipsTypes.select,
    modalConfig: {
      title: 'title',
      searchFn: (x: GenericApiRequest) => {
        const name = x.name;
        const content: MayHaveLabel[] = [
          { id: '1', name },
          { id: '2', name: `${name} 2` },
        ];
        return of({
          content,
          page: { page_number: 0, page_size: 5, total_elements: 5, total_pages: 8 },
        }).pipe(delay(4000));
      },
    },
  };

  showTooltip = false;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      chip: [null],
    });
  }

  sendForTooltipExample() {
    FormUtilsService.markFormGroupTouched(this.form);
    if (!this.form.valid) {
      this.showTooltip = true;
    }
  }
}
