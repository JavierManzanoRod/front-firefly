import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { CategoryRuleFolderService } from '../../../rule-engine/category-rule/services/category-rule-folder.service';
import { GenericApiRequest } from '@model/base-api.model';
import { CategoryRuleService } from 'src/app/rule-engine/category-rule/services/category-rule.service';
import { ExplodedEvent } from '../../../modules/exploded-tree/models/exploded-tree.model';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';

@Component({
  templateUrl: './example-modal-tree.component.html',
})
export class ExampleModalTreeComponent {
  form: FormGroup;

  chipConfig: ChipsControlSelectConfig = {
    modalType: ModalChipsTypes.tree,
    label: 'Rango de precios  y descuentos',
    modalConfig: {
      title: 'Rango de precios y descuentos',
      multiple: true,
      searchFnOnInit: true,
      searchFn: (x: GenericApiRequest) => {
        return this.categoryRuleService.list({
          ...x,
        });
      },
      tree: () => this.ruleFoldersService.listFolders(),
      clickNode: ({ data, target }: ExplodedEvent<ExplodedTreeNodeComponent>) => {
        if (data.id && !data.value?.isRule) {
          if (!data.children) {
            target.setLoading(true);
            this.ruleFoldersService.detail(data.id).subscribe((response) => {
              data.children = (data.children || []).concat(response.folders || []).concat(
                (response.category_rules || []).map((rule) => {
                  return { ...rule, id: rule.identifier, value: { isRule: true } };
                })
              );

              if (!data.children.length) {
                target.setWarning('Carpeta vacía');
              }

              target.expand();
              target.setLoading(false);
            });
          }
          return false;
        }
        return true;
      },
      iconFilter: (target: ExplodedTreeNodeComponent) => {
        if (target.data.value?.isRule) {
          return 'icon-agrupaciones';
        }

        return 'icon-folder';
      },
    },
  };

  constructor(
    private fb: FormBuilder,
    private ruleFoldersService: CategoryRuleFolderService,
    private categoryRuleService: CategoryRuleService
  ) {
    this.form = this.fb.group({
      tree: [],
    });
  }
}
