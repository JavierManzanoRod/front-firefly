import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { UIModalRangeValue } from 'src/app/modules/ui-modal-select/models/modal-select.model';

@Component({
  templateUrl: './example-modal-range.component.html',
})
export class ExampleModalRangeComponent {
  form: FormGroup;

  sites: { [key: string]: string } = { eci: 'Esci', portugal: 'Portugal', spain: 'España' };

  priceConfig: ChipsControlSelectConfig = {
    modalType: ModalChipsTypes.range,
    label: 'Rango de precios  y descuentos',
    modalConfig: {
      title: 'Rango de precios y descuentos',
      selectMultiple: true,
      selectLabel: 'Centros',
      selectPlaceholder: 'Selecciona una opción',
      selectOptions: this.sites,
      ranges: [
        {
          key: 'price',
          label: 'Precio',
          startLabel: 'Desde',
          endLabel: 'Hasta',
          prefix: '€',
          error: 'El precio mínimo no puede ser superior al precio máximo o estar vacío',
        },
        {
          key: 'discount',
          label: 'Descuento',
          startLabel: 'Desde',
          endLabel: 'Hasta',
          prefix: '%',
          autcomplete: true,
          error: 'El descuento mínimo no puede ser superior al descuento máximo o estar vacío',
          min: 0,
          max: 100,
        },
      ],
    },
    chipFormat: (value: UIModalRangeValue[]) => {
      /*if (value && value.select) {
        return `${value.select}: ${value.price.from}€ - ${value.price.to}€ / ${value.discount.from}% - ${value.discount.to}%`;
      }
      return '';*/
    },
  };

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      range: [],
    });
  }
}
