import { Component } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalFooterComponent } from 'src/app/modules/common-modal/components/base/modal-footer.component';
import { ModalHeaderComponent } from 'src/app/modules/common-modal/components/base/modal-header.component';
import { ModalCloneComponent } from 'src/app/modules/common-modal/components/modal-clone/modal-clone.component';
import { ModalCreateComponent } from 'src/app/modules/common-modal/components/modal-create/modal-create.component';
import { ModalDeleteComponent } from 'src/app/modules/common-modal/components/modal-delete/modal-delete.component';
import { ModalSaveComponent } from 'src/app/modules/common-modal/components/modal-save/modal-save.component';
import { ModalStopComponent } from 'src/app/modules/common-modal/components/modal-stop/modal-stop.component';
import { ModalUnsavedTabComponent } from 'src/app/modules/common-modal/components/modal-unsaved-tab/modal-unsaved-tab.component';
import { ModalWarningComponent } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.component';

@Component({
  selector: 'ff-example-modales-avisos',
  templateUrl: './example-modales-avisos.component.html',
  styleUrls: ['./example-modales-avisos.component.scss'],
})
export class ExampleModalesAvisosComponent {
  modales = [
    { name: 'Stop', modal: ModalStopComponent },
    { name: 'Footer', modal: ModalFooterComponent },
    { name: 'Header', modal: ModalHeaderComponent, config: { title: 'Modal de Header' } },
    { name: 'Clone', modal: ModalCloneComponent },
    { name: 'Create', modal: ModalCreateComponent },
    { name: 'Delete', modal: ModalDeleteComponent },
    { name: 'Save', modal: ModalSaveComponent },
    { name: 'UnSave', modal: ModalUnsavedTabComponent, config: { tabName: 'Unsave Modal' } },
    {
      name: 'Warning',
      modal: ModalWarningComponent,
      config: {
        config: {
          name: 'Warning',
          title: 'Modal de warning',
          errorMessage: 'Esto es una prueba',
          bodyMessage: 'Mensaje de prueba',
          questionMessage: '¿Hola?',
          okButtonText: 'Ok',
          cancelButtonText: 'Calcel',
        },
      },
    },
  ];

  constructor(private modalService: BsModalService) {}

  openModal(modal: any) {
    this.modalService.show(modal.modal, {
      initialState: {
        item: {
          name: modal?.name,
        },
        ...modal?.config,
      },
    });
  }
}
