import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { of } from 'rxjs';
import { catchError, finalize, takeUntil, tap } from 'rxjs/operators';
import { Subsite } from 'src/app/sites/site/models/sites.model';
import { DestroyService } from '../../../shared/services/destroy.service';
import { DashboardFilterData, DashBoardQueryItem } from '../models/dashboard-query.model';
import { DashboardQueryService } from '../services/dashboard-query.service';

@Component({
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<ff-page-header [pageTitle]="'DASHBOARD.CATALOG_STATUS' | translate"></ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-dashboard-query-search
        (filtering)="updateUrl($event)"
        (actualTypeCode)="updateType($event)"
        (submitedState)="submitedState($event)"
        [defaultValues]="defaultValues"
        [subsites]="subsites"
      ></ff-dashboard-query-search>
      <div>
        <ff-dashboard-query-list
          [data]="data"
          [loading]="loading"
          [updatedType]="updatedType"
          [submited]="submited"
          [subsites]="subsites"
        ></ff-dashboard-query-list>
      </div>
    </ngx-simplebar> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./dashboard-query-search-container.component.scss'],
  providers: [DestroyService],
})
export class DashboardQuerySearchContainerComponent implements AfterViewInit {
  @Input() defaultValues!: { [key: string]: string };

  subsites: Subsite[] = [];
  submited = false;
  data: DashBoardQueryItem | null = null;
  loading = false;
  updatedType = 'item_code';

  constructor(
    private dashboardQueryService: DashboardQueryService,
    public utils: CrudOperationsService,
    private change: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router,
    private destroyService: DestroyService
  ) {
    if (this.route.snapshot.data.subsites) {
      this.subsites = this.route.snapshot.data.subsites.content;
    }

    this.route.params.subscribe(({ code, type, ...params }) => {
      if (code && type) {
        const criteria: { [key: string]: string } = { code, type };
        Object.keys(params).forEach((key) => {
          if (key.startsWith('key')) {
            const index = key.replace(/key/, '');
            const value = params[`value${index}`];
            if (value && value !== 'null' && value !== 'undefined') {
              criteria[params[`key${index}`]] = value;
            }
          }
        });

        this.applyFilter(criteria as DashboardFilterData);
        this.defaultValues = criteria;
      }
    });
  }

  ngAfterViewInit() {}

  updateType(type: string) {
    this.updatedType = type;
  }
  submitedState(isSubmited: boolean) {
    this.submited = isSubmited;
  }

  updateUrl({ type, code, ...criteria }: DashboardFilterData) {
    if (criteria.site && criteria.site instanceof Object) {
      this.router.navigateByUrl(`/dashboard/${type}/${code}/site/${criteria.site?.id}/site_name/${criteria.site_name}`);
    } else {
      this.router.navigateByUrl(`/dashboard/${type}/${code}`);
    }
  }

  applyFilter(criteria: DashboardFilterData) {
    if (criteria) {
      this.loading = true;
      this.dashboardQueryService
        .list({ ...criteria })
        .pipe(
          tap(({ data }) => {
            if (data.length === 0) {
              this.utils.toast.warning('COMMON_ERRORS.SEARCH');
            }
          }),
          finalize(() => {
            this.loading = false;
            this.change.markForCheck();
          }),
          catchError((err) => {
            if (err.status === 404) {
              this.utils.toast.warning('COMMON_ERRORS.SEARCH');
            } else if (err.status) {
              this.utils.toast.error('COMMON_ERRORS.INTERNAL_ERROR');
            }
            return of(null);
          }),
          takeUntil(this.destroyService)
        )
        .subscribe((data) => {
          this.data = {
            type: data?.type || '',
            data: data?.data.filter((product) => product.subsite && !['ALL'].includes(product.subsite)) || [],
          };
          this.loading = false;
          this.change.markForCheck();
        });
    }
  }
}
