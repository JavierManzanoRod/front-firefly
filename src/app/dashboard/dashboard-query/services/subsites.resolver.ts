import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Subsite } from 'src/app/sites/site/models/sites.model';
import { SubsiteService } from './subsite-api.service';

@Injectable({ providedIn: 'root' })
export class SubSiteResolver implements Resolve<Subsite[]> {
  constructor(private subsiteService: SubsiteService) {}

  resolve(): Observable<any> | Promise<any> | any {
    return this.subsiteService.list({ size: 99999 });
  }
}
