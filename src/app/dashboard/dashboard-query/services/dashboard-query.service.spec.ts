import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { DashboardQueryService } from './dashboard-query.service';

describe('DashboardQueryService', () => {
  let service: DashboardQueryService;
  let version: 'v1/';

  beforeEach(() => {
    service = new DashboardQueryService(null as unknown as any, null as unknown as EntityTypeService, version);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
