/* eslint-disable @typescript-eslint/no-unsafe-return */
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Summary } from '../models/dashboard-query.model';

@Injectable({
  providedIn: 'root',
})
export class ProductStateService {
  constructor(private translate: TranslateService) {}

  public getCode(code: Summary): string {
    return this.translate.instant('DASHBOARD.INFO.CODE.' + code.code, code.values && this.formatValues(code.values));
  }

  private formatValues(values: { key: string; value: string }[]) {
    return (
      (values &&
        values.reduce((stack: { [key: string]: string }, current) => {
          stack[current.key] = current.value;
          return stack;
        }, {})) ||
      {}
    );
  }
}
