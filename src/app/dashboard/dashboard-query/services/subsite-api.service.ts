import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, IApiService2 } from '@core/base/api.service';
import { FF_API_PATH_VERSION_SUBSITES } from 'src/app/configuration/tokens/api-versions.token';
// import { environment } from '@env/environment';
import { Subsite } from 'src/app/sites/site/models/sites.model';
// const urlBase = `${environment.API_URL_BACKOFFICE}`;

@Injectable({
  providedIn: 'root',
})
export class SubsiteService extends ApiService<Subsite> implements IApiService2<Subsite> {
  endPoint = `products/backoffice-sites-subsites/${this.version}subsites`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_SUBSITES) private version: string) {
    super();
  }
}
