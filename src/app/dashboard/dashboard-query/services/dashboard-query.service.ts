/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_AUDIT_CATALOG_DASHBOARD } from 'src/app/configuration/tokens/api-versions.token';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import {
  DashboardFilterData,
  DashBoardQueryCodeItemResponse,
  DashboardQueryEanUniqueItem,
  DashboardQueryEanUniqueItemResponse,
  DashBoardQueryItem,
  DashBoardQueryReferenceItemResponse,
} from '../models/dashboard-query.model';

@Injectable({
  providedIn: 'root',
})
export class DashboardQueryService {
  urlBase = `${environment.API_URL_BACKOFFICE}/products/backoffice-audit-catalog-dashboard/${this.version}`;

  constructor(
    protected http: HttpClient,
    private entityService: EntityTypeService,
    @Inject(FF_API_PATH_VERSION_AUDIT_CATALOG_DASHBOARD) private version: string
  ) {}

  list(params: DashboardFilterData): Observable<DashBoardQueryItem> {
    if (params.type === 'sale_reference') {
      return this.listByReference(params);
    }
    if (params.type === 'item_code') {
      return this.listByCodeItem(params);
    }
    if (params.type === 'ean' || params.type === 'unique-code') {
      return this.listByEanUnique(params);
    }
    return this.listByCodeItem(params);
  }

  listByReference(params: DashboardFilterData): Observable<DashBoardQueryItem> {
    params.code = encodeURIComponent(params.code);
    return this.makeRequest<DashBoardQueryReferenceItemResponse>(params).pipe(
      map((item) => {
        return {
          type: params.type,
          data: item.references,
        };
      })
    );
  }
  listByCodeItem(params: DashboardFilterData): Observable<DashBoardQueryItem> {
    return this.makeRequest<DashBoardQueryCodeItemResponse>(params).pipe(
      map((item) => {
        return {
          type: params.type,
          data: item.products,
        };
      })
    );
  }
  listByEanUnique(params: DashboardFilterData): Observable<DashBoardQueryItem> {
    return this.makeRequest<DashboardQueryEanUniqueItemResponse>(params).pipe(
      map((item: DashboardQueryEanUniqueItem) => {
        const variants = item.variants;
        if (variants instanceof Array) {
          item.variants = variants.reduce((prev: Array<any>, curr: any) => {
            const find = prev.find((variant) => variant.site === curr.site && variant.subsite === curr.subsite);
            if (!find) {
              prev.push(curr);
            } else {
              find.offers.push(...curr.offers);
            }

            return prev;
          }, []);
        }

        return {
          type: params.type,
          data: item.variants,
        };
      })
    );
  }

  makeRequest<T>({ code, type, site: subsite, seller: seller_identifier }: DashboardFilterData): Observable<T | any> {
    /**
     * TODO: descomentar cuando quede resuelto el conflicto del name de entity
     */
    let url = '';
    const headers: { [key: string]: string } = {};

    if (type === 'ean' || type === 'unique-code') {
      url = `${this.urlBase}product-variants/${code}/offers`;
      headers['eci-type'] = type.toUpperCase().replace('-', '_');
    }
    if (type === 'sale_reference') {
      url = `${this.urlBase}offers/${code}`;
    }
    if (type === 'item_code') {
      url = `${this.urlBase}products/${code}/offers`;
    }

    const params: { [key: string]: string } = {};

    if (subsite) {
      params.subsite = `${subsite}`;
    }

    if (seller_identifier) {
      params.seller_identifier = seller_identifier;
    }

    return this.http
      .get(url, {
        params,
        headers,
      })
      .pipe(finalize(() => {}));
  }

  /**
   * TODO: descomentar cuando quede resuelto el conflictod del name de entity
   */
  // getSeller(id: string) {
  //   this.entityService.detail(id).subscribe((res) => {});
  // }
}
