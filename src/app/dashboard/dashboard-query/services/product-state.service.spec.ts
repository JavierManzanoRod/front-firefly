import { TranslateService } from '@ngx-translate/core';
import { ProductStateService } from './product-state.service';

describe('ProductStateService', () => {
  let service: ProductStateService;

  beforeEach(() => {
    service = new ProductStateService(null as unknown as TranslateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
