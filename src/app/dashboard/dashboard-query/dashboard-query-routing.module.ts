import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardQuerySearchContainerComponent } from './containers/dashboard-query-search-container.component';
export const routes: Routes = [
  {
    path: '',
    component: DashboardQuerySearchContainerComponent,
    data: {
      header_title: 'MENU_LEFT.QUERIES',
      breadcrumb: [
        {
          label: 'DASHBOARD.CATALOG_STATUS',
          url: '',
        },
      ],
    },
  },
  {
    path: ':type/:code',
    component: DashboardQuerySearchContainerComponent,
    // resolve: {
    //   subsites: SubSiteResolver,
    // },
  },
  {
    path: ':type/:code/:key1/:value1',
    component: DashboardQuerySearchContainerComponent,
    // resolve: {
    //   subsites: SubSiteResolver,
    // },
  },
  {
    path: ':type/:code/:key1/:value1/:key2/:value2',
    component: DashboardQuerySearchContainerComponent,
    // resolve: {
    //   subsites: SubSiteResolver,
    // },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardQueryRoutingModule {}
