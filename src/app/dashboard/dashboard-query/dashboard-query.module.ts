import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { ToastModule } from 'src/app/modules/toast/toast.module';
import { UiModalInfoModule } from 'src/app/modules/ui-modal-info/ui-modal-info.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConditionsReadonlyModule } from '../../modules/conditions-readonly/conditions-readonly.module';
import { Conditions2Module } from '../../modules/conditions2/conditions2.module';
import { FormErrorModule } from '../../modules/form-error/form-error.module';
import { LoadingModule } from '../../modules/loading/loading.module';
import { DashboardQueryListComponent } from './components/dashboard-query-list/dashboard-query-list.component';
import { DashboardQuerySearchComponent } from './components/dashboard-query-search/dashboard-query-search.component';
import { DashboardQuerySearchContainerComponent } from './containers/dashboard-query-search-container.component';
import { DashboardQueryRoutingModule } from './dashboard-query-routing.module';

@NgModule({
  declarations: [DashboardQuerySearchContainerComponent, DashboardQueryListComponent, DashboardQuerySearchComponent],
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    CommonModalModule,
    LoadingModule,
    FormErrorModule,
    Conditions2Module,
    ConditionsReadonlyModule,
    DatetimepickerModule,
    SimplebarAngularModule,
    NgSelectModule,
    ToastModule,
    UiModalInfoModule,
    DashboardQueryRoutingModule,
    ChipsControlModule,
    TooltipModule,
  ],
  providers: [CrudOperationsService],
  exports: [],
})
export class DashboardQueryModule {}
