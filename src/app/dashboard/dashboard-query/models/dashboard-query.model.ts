import { Site } from './../../../sites/site/models/sites.model';
export interface DashBoardQuerySearchParams {
  typeOfCode?: string;
  code: string;
  site?: string;
  subsite?: string;
  seller?: string;
}

export interface DashBoardQuerySubParams {
  site: {
    id: string;
    name: string;
  }[];
  seller: {
    id: string;
    name: string;
  }[];
}

export interface DashBoardQueryItem {
  type: string;
  data: {
    identifier?: string;
    sales_reference?: string;
    unique_code?: string;
    gtin?: string;
    name?: {
      locale: string;
      value: string;
    }[];
    site: string;
    subsite: string;
    is_valid: boolean;
    is_publish: boolean;
    offers: Offer[];
  }[];
}

export interface DashboardQueryReferenceItem {
  references: {
    sales_reference: string;
    site: string;
    subsite: string;
    is_valid: boolean;
    is_publish: boolean;
    offers: Offer[];
  }[];
}

export interface DashboardQueryCodeItem {
  products: {
    identifier: string;
    name: [
      {
        locale: string;
        value: string;
      }
    ];
    site: string;
    subsite: string;
    is_valid: boolean;
    is_publish: boolean;
    offers: Offer[];
  }[];
}

export interface DashboardQueryEanUniqueItem {
  variants: {
    unique_code: string;
    gtin: string;
    name: {
      locale: string;
      value: string;
    }[];
    site: string;
    subsite: string;
    is_valid: boolean;
    is_publish: boolean;
    offers: Offer[];
  }[];
}

export interface Offer {
  sales_reference: string;
  name: {
    locale: string;
    value: string;
  };
  product_id: string;
  unique_code: string;
  gtin: string;
  seller: string;
  metadata: Metadata;
}

export interface Metadata {
  overwrite_summary: Summary[];
  validation_summary: Summary[];
  publish_summary: Summary[];
  is_valid: boolean;
  is_publish: boolean;
}

export interface Summary {
  code: string;
  values?: {
    key: string;
    value: string;
  }[];
}

export type DashboardFilterData = {
  type: string;
  code: string;
  site: Site | string;
  site_name: string;
  // subsite: string;
  seller: string;
  isSalable: string | boolean;
  isPublished: string | boolean;
  isValid: string;
};

export type DashboardFilterDataKeys = keyof DashboardFilterData;
export type DashBoardQueryReferenceItemResponse = DashboardQueryReferenceItem;
export type DashBoardQueryCodeItemResponse = DashboardQueryCodeItem;
export type DashboardQueryEanUniqueItemResponse = DashboardQueryEanUniqueItem;
