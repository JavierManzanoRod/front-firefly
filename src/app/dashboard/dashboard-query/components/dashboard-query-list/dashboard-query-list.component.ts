/* eslint-disable @typescript-eslint/member-ordering */
import { ChangeDetectorRef, Component, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EntityService } from 'src/app/rule-engine/entity/services/entity.service';
import { BaseListComponent } from 'src/app/shared/components/base-component-list.component';
import { Subsite } from 'src/app/sites/site/models/sites.model';
import { DashBoardQueryItem, Metadata } from '../../models/dashboard-query.model';
import { ProductStateService } from '../../services/product-state.service';
import { UiModalInfoComponent } from '../../../../modules/ui-modal-info/ui-modal-info.component';

@Component({
  selector: 'ff-dashboard-query-list',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './dashboard-query-list.component.html',
  styleUrls: ['./dashboard-query-list.component.scss'],
})
export class DashboardQueryListComponent extends BaseListComponent<DashBoardQueryItem> {
  @Input() loading = false;
  @Input() data: DashBoardQueryItem | null = null;
  @Input() get updatedType() {
    return this._updateType;
  }
  set updatedType(updateType: string) {
    this._updateType = updateType;

    switch (this._updateType) {
      case 'sale_reference':
        this.title = 'DASHBOARD.LIST.HEADER.REFERENCE';
        this.displayColumns = ['reference', 'product', 'ean', 'seller', 'published', 'valid', 'detail', 'name'];

        break;

      case 'ean':
        this.title = 'DASHBOARD.LIST.HEADER.EAN';
        this.displayColumns = ['ean', 'reference', 'product', 'seller', 'published', 'valid', 'detail', 'name'];
        break;

      case 'unique-code':
        this.title = 'DASHBOARD.LIST.HEADER.UNIQUE';
        this.displayColumns = ['unique_code', 'product', 'reference', 'ean', 'seller', 'published', 'valid', 'detail', 'name'];

        break;

      default:
        this.title = 'DASHBOARD.LIST.HEADER.PRODUCT';
        this.displayColumns = ['product', 'reference', 'ean', 'seller', 'published', 'valid', 'detail', 'name'];
        break;
    }
  }

  @Input() submited: boolean | null = false;
  @Input() subsites!: Subsite[];

  @ViewChild('variantsModalTemplate') conditionModalElement!: UiModalInfoComponent;

  _updateType = 'item_code';
  sellerCache: { [key: string]: string } = {};
  showSubitems: { [key: string]: boolean } = {};
  loadingModal = false;
  modalRef!: BsModalRef;
  formModal: FormGroup;
  titleModal = '';
  variantData: string[] = [];
  title = '';
  displayColumns: string[] = [];
  centerColumns: string[] = ['published', 'valid', 'detail'];

  constructor(
    private modalService: BsModalService,
    private router: Router,
    private productStateService: ProductStateService,
    private entityService: EntityService,
    private change: ChangeDetectorRef
  ) {
    super();
    this.formModal = new FormGroup({
      node: new FormControl({ value: null, disabled: true }),
    });
  }

  toggle(id: string) {
    this.showSubitems[id] = !this.showSubitems[id];
  }

  retry() {
    this.backToFilter();
  }

  backToFilter() {
    const parentRoute = '/dashboard';
    this.redirectTo(parentRoute);
  }

  redirectTo(uri: string) {
    this.router.navigateByUrl(uri);
  }

  hasPublishSummary(variant: Metadata) {
    return !!variant.publish_summary.length;
  }

  showPublishDetail(variant: Metadata) {
    this.titleModal = 'No publicado';
    this.variantData = variant.publish_summary.map((v) => this.productStateService.getCode(v));
    this.loadingModal = true;
    this.modalRef = this.modalService.show(this.conditionModalElement as any, { class: 'modal-lg' });
  }

  hasValidSummary(variant: Metadata) {
    return !!variant.validation_summary.length;
  }

  showValidDetail(variant: Metadata) {
    this.titleModal = 'No válido';
    this.variantData = variant.validation_summary.map((v) => this.productStateService.getCode(v));
    this.loadingModal = true;
    this.modalRef = this.modalService.show(this.conditionModalElement as any, { class: 'modal-lg' });
  }

  hasOverwriteSummary(variant: Metadata) {
    return !!variant.overwrite_summary.length;
  }

  showOverwrite(variant: Metadata) {
    this.titleModal = 'Detalle de SobreEscritura';
    this.variantData = variant.overwrite_summary.map((v) => this.productStateService.getCode(v));
    this.loadingModal = true;
    this.modalRef = this.modalService.show(this.conditionModalElement as any, { class: 'modal-lg' });
  }

  hideModal() {
    this.loadingModal = false;
    this.modalRef.hide();
  }

  showSellerTooltip(id: string) {
    if (!id) {
      return;
    }
    if (this.sellerCache[id]) {
      return this.sellerCache[id];
    }

    setTimeout(() => {
      if (!this.sellerCache[id]) {
        this.sellerCache[id] = 'loading...';
        this.change.markForCheck();
        this.entityService
          .detail(id)
          .pipe(catchError(() => of({ label: 'error' })))
          .subscribe(({ label }) => {
            this.sellerCache[id] = label || 'error';
            this.change.markForCheck();
          });
      }
    });
  }

  getSubsiteLabel(product: { subsite: string }) {
    const data = this.subsites.find((subsite) => subsite.id === product.subsite);

    return data?.name || product.subsite;
  }
}
