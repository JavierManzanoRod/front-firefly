import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { DashboardQuerySearchComponent } from './dashboard-query-search.component';

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}
  public get(key: any): any {
    of(key);
  }
}
@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

describe('DashboardQuerySearchComponent', () => {
  let component: DashboardQuerySearchComponent;
  let fixture: ComponentFixture<DashboardQuerySearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [DashboardQuerySearchComponent, TranslatePipeMock],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceStub },
        {
          provide: HttpClient,
          useFactory: () => ({
            error: () => {},
          }),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardQuerySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
