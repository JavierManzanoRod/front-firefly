import { ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { GenericApiRequest } from '@model/base-api.model';
import { Subject } from 'rxjs';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_ENTITY_TYPE_ID_SELLERS } from 'src/app/configuration/tokens/configuracion';
import { ChipsControlSelectConfig, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { EntityService } from 'src/app/rule-engine/entity/services/entity.service';
import { ProductRule, TypeCode } from 'src/app/rule-query/models/product.model';
import { FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { Subsite } from 'src/app/sites/site/models/sites.model';
import { DashBoardQuerySearchParams } from '../../models/dashboard-query.model';
import { SubsiteService } from '../../services/subsite-api.service';

@Component({
  selector: 'ff-dashboard-query-search',
  templateUrl: './dashboard-query-search.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./dashboard-query-search.component.scss'],
})
export class DashboardQuerySearchComponent implements OnInit {
  @Input() defaultValues!: { [key: string]: any };
  @Input() subsites!: Subsite[];

  @Output() filtering = new EventEmitter<DashBoardQuerySearchParams | null>();
  @Output() actualTypeCode = new EventEmitter<string | null>();
  @Output() submitedState = new EventEmitter<boolean | null>();

  form!: FormGroup;
  type = 'item_code';
  product!: ProductRule | undefined;
  productCode!: string | undefined;
  referenceCode!: string | undefined;
  productCodeStatus!: 'done' | 'error' | 'loading' | undefined;
  referenceCodeStatus!: 'done' | 'error' | 'loading' | undefined;
  errorStatus!: number | undefined;
  typeCode: TypeCode = 'item_code';
  itemLoading = false;
  isSubmited = false;
  isReadOnlyOnsubmit = false;
  productCodeChanged: Subject<string> = new Subject<string>();
  typeCodeLabel = 'PRODUCT_CODE';
  collapsed = false;
  chipsConfigSeller!: ChipsControlSelectConfig;
  chipsConfigSubsite!: ChipsControlSelectConfig;

  formErrors: any = {};

  cache: { [key: string]: string } = {};
  constructor(
    private entityService: EntityService,
    private subsiteService: SubsiteService,
    @Inject(FF_ENTITY_TYPE_ID_SELLERS)
    private entityTypeSellersId: ConfigurationDefinition['entityTypeIdSellers'],
    private router: Router
  ) {
    const configForm = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      type: {
        value: this.typeCode,
      },
      code: {
        value: null,
        validators: {
          required: true,
        },
      },
      site: {
        value: null,
      },
      seller: {
        value: null,
      },
    });
    this.form = configForm.form;
    this.formErrors = configForm.errors;
    this.form.controls.type.valueChanges.subscribe((res) => {
      this.changeCodeType(res);
    });
  }

  get value() {
    return this.form.value;
  }

  ngOnInit(): void {
    this.chipsConfigSeller = {
      label: 'DASHBOARD.SELLERS_TITLE',
      modalType: ModalChipsTypes.select,
      modalConfig: {
        title: 'DASHBOARD.INFO.PLACEHOLDER.SELLER',
        items: [],
        searchFnOnInit: true,
        itemLabelKey: ['name', 'label'],
        searchParam: 'label_attribute',
        searchFn: (x: GenericApiRequest) => this.entityService.listWithFilter(this.entityTypeSellersId || '', x),
      },
    };
    this.chipsConfigSubsite = {
      label: 'DASHBOARD.SITE_TITLE',
      modalType: ModalChipsTypes.select,
      modalConfig: {
        title: 'DASHBOARD.INFO.PLACEHOLDER.SITE',
        items: [],
        searchFnOnInit: true,
        searchFn: (x: GenericApiRequest) => this.subsiteService.list({ ...x, status: 'true' }),
      },
    };
    const repeat = history?.state?.repeat;

    if (this.defaultValues?.site) {
      this.defaultValues.site = {
        id: this.defaultValues.site,
        name: this.defaultValues.site_name,
      };
    }

    if (this.defaultValues || repeat) {
      this.form.patchValue(this.defaultValues || repeat);
      this.isSubmited = true && !repeat;
      this.isReadOnlyOnsubmit = true && !history.state.repeat;
      this.form.controls.code.setValue(decodeURIComponent(this.form.controls.code.value));
    }
  }

  changeCodeType(event: any) {
    this.typeCode = event;
    this.actualTypeCode.emit(event);
    this.productCode = undefined;
    this.product = undefined;
    this.productCodeStatus = undefined;
    this.changeLabel();
  }

  submit() {
    FormUtilsService.markFormGroupTouched(this.form);

    if (!this.form.valid) {
      return;
    }

    this.isSubmited = true;
    this.submitedState.emit(this.isSubmited);
    this.isReadOnlyOnsubmit = true;

    if (this.value.seller !== null && this.value.seller) {
      this.value.seller = this.value.seller.id;
    }

    this.value.site_name = this.value?.site?.name;

    const formValue = this.value;

    this.filtering.emit(formValue);
  }

  cancel() {
    this.form.reset({
      type: 'item_code',
    });
  }

  back() {
    this.router.navigate(['/dashboard'], { state: { repeat: this.value } });
  }

  changeLabel() {
    if (this.typeCode === 'sale_reference') {
      this.typeCodeLabel = 'REFERENCE_CODE';
    } else if (this.typeCode === 'item_code') {
      this.typeCodeLabel = 'PRODUCT_CODE';
    } else if (this.typeCode === 'ean') {
      this.typeCodeLabel = 'EAN_CODE';
    } else if (this.typeCode === 'unique-code') {
      this.typeCodeLabel = 'UNIQUE_CODE';
    }
  }
}
