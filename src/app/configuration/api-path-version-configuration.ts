import { environment } from '@env/environment';
import { ApiPathVersionConfigurationDefinition } from './models/apis-configuration.model';
import { emptyPropertyValitator } from './validators/empty-property-validator';

function parseStr(str: string = ''): string {
  return str.trim();
}

export const apiPathVersionConfiguration: ApiPathVersionConfigurationDefinition = {
  limitSale: parseStr(environment?.API_PATH_LIMIT_SALES_VERSION),
  sites: parseStr(environment?.API_PATH_SITES_VERSION),
  subSites: parseStr(environment?.API_PATH_SUBSITES_VERSION),
  technicalCharacteristics: parseStr(environment?.API_PATH_TECHNICAL_CHARACTERISTICS_VERSION),
  attributeTranslation: parseStr(environment?.API_PATH_ATTRIBUTE_TRANSLATION_VERSION),
  facets: parseStr(environment?.API_PATH_FACETS_VERSION),
  entityTypes: parseStr(environment?.API_PATH_ENTITY_TYPES_VERSION),
  entities: parseStr(environment?.API_PATH_ENTITIES_VERSION),
  adminGroupFolders: parseStr(environment?.API_PATH_ADMIN_GROUP_FOLDERS_VERSION),
  contentGroups: parseStr(environment?.API_PATH_CONTENT_GROUPS_VERSION),
  facetLinks: parseStr(environment?.API_PATH_FACET_LINKS_VERSION),
  adminGroups: parseStr(environment?.API_PATH_ADMIN_GROUPS_VERSION),
  adminGroupsTypes: parseStr(environment?.API_PATH_ADMIN_GROUPS_TYPES_VERSION),
  categoryRules: parseStr(environment?.API_PATH_CATEGORY_RULES_VERSION),
  attributeTreeNode: parseStr(environment?.API_PATH_ATTRIBUTE_TREE_NODE_VERSION),
  productTypes: parseStr(environment?.API_PATH_PRODUCT_TYPES_VERSION),
  excludeSearches: parseStr(environment?.API_PATH_EXCLUDE_SEARCHES_VERSION),
  promotions: parseStr(environment?.API_PATH_PROMOTIONS_VERSION),
  geozonesScopes: parseStr(environment?.API_PATH_GEOZONES_SCOPES_VERSION),
  geozonesTargets: parseStr(environment?.API_PATH_GEOZONES_TARGETS_VERSION),
  productsManageSchema: parseStr(environment?.API_PATH_PRODUCTS_MANAGER_SCHEMA_VERSION),
  marketplaceSellers: parseStr(environment?.API_PATH_MARKETPLACE_SELLERS_VERSION),
  marketplaceProductTypes: parseStr(environment?.API_PATH_MARKETPLACE_PRODUCT_TYPES_VERSION),
  marketplaceProductRelations: parseStr(environment?.API_PATH_MARKETPLACE_PRODUCT_RELATIONS_VERSION),
  facetAttributes: parseStr(environment?.API_PATH_FACET_ATTRIBUTES_VERSION),
  explodedTree: parseStr(environment?.API_PATH_EXPLODEDE_TREE_VERSION),
  assortmentProductSearchers: parseStr(environment?.API_PATH_ASSORMENT_PRODUCT_SEARCHERS_VERSION),
  evaluateRules: parseStr(environment?.API_PATH_EVALUATE_RULES_VERSION),
  auditCatalogDashboard: parseStr(environment?.API_PATH_AUDIT_CATALOG_DASHBOARD_VERSION),
  expert: parseStr(environment?.API_PATH_EXPERT_VERSION),
  productSearch: parseStr(environment?.API_PATH_ATTRIBUTE_PRODUCT_SEARCH),
};

interface MaybeConfiguration {
  [key: string]: string | string[] | MaybeConfiguration;
}

setTimeout(() => {
  try {
    console.log('validating apiPathVersionConfiguration...');
    const configErrors = emptyPropertyValitator(apiPathVersionConfiguration as MaybeConfiguration);
    for (const error of configErrors) {
      console.log(' found empty apiPathversionConfiguration:', error);
    }
    if (!configErrors.length) {
      console.log('all configs are filled');
    }
  } catch (e) {
    console.log('error Validating config', apiPathVersionConfiguration);
  }
});
