import { ControlPanelConditions, MultivaluedAttributes } from '../../control-panel/shared/models/control-panel-conditions.model';

export enum AdminGroupTypeMaster {
  excludeSearch = 'excludeSearch',
  limitSale = 'limitSale',
  priceInheritance = 'priceInheritance',
  contentGroup = 'contentGroup',
  specialProducts = 'specialProducts',
  sizeGuide = 'sizeGuide',
  expert = 'expert',
  fluorGases = 'fluorGases',
  loyalty = 'loyalty',
  badge = 'badge',
}
interface Configuration1 {
  cities: string;
  countries: string;
  department: string;
  brand: string;
  references: string;
  products: string;
  languages: string[];
  offerEntityType: string;
  entityTypeIdCategories: string;
  entityTypeIdSites: string;
  entityTypeIdOffer: string;
  entityTypeIdCenters: string;
  entityTypeIdPriceSpecifications: string;
  entityTypeIdGoodType: string;
  entityTypeIdSellers: string;
  entityTypeIdBrand: string;
  entityTypeIdBundleOf: string;
  entityTypeIdProvider: string;
  activeFunctionalities: string[];
  urlApis: string;
  entityTypeIdItemOffered: string;
  urlSearcherApi: string;
  ruleEngine: {
    conditionsBasicLimitSaleIds: string[];
  };
  attributePath: {
    [key in ControlPanelConditions]?: string;
  };
  adminGroupType: {
    [key in AdminGroupTypeMaster]?: string;
  };
  multivaluedIds: {
    [key in MultivaluedAttributes]: {
      identifier: string;
      value: string;
    };
  };
  groupedAttributes: {
    [key in ControlPanelConditions]?: string;
  };
}

export type ConfigurationDefinition = Readonly<Partial<Configuration1>>;
