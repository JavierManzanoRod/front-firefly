interface ApiPathVersionConfiguration {
  limitSale: string;
  sites: string;
  subSites: string;
  technicalCharacteristics: string;
  attributeTranslation: string;
  facets: string;
  entityTypes: string;
  entities: string;
  adminGroupFolders: string;
  contentGroups: string;
  facetLinks: string;
  adminGroups: string;
  adminGroupsTypes: string;
  categoryRules: string;
  attributeTreeNode: string;
  productTypes: string;
  excludeSearches: string;
  promotions: string;
  geozonesTargets: string;
  geozonesScopes: string;
  productsManageSchema: string;
  marketplaceSellers: string;
  marketplaceProductTypes: string;
  marketplaceProductRelations: string;
  facetAttributes: string;
  explodedTree: string;
  assortmentProductSearchers: string;
  evaluateRules: string;
  auditCatalogDashboard: string;
  expert: string;
  productSearch: string;
}

export type ApiPathVersionConfigurationDefinition = Readonly<Partial<ApiPathVersionConfiguration>>;
