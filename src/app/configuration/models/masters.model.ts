export enum Masters {
  calificacion1 = 'calificacion1',
  calificacion2 = 'calificacion2',
  calificacion3 = 'calificacion3',
  calificacion4 = 'calificacion4',
  calificacion5 = 'calificacion5',
  calificacion6 = 'calificacion6',
  canal = 'canal',
  categorias = 'categorias',
  centros = 'centros',
  channel_dvd = 'channel_dvd',
  codigo_tienda = 'codigo_tienda',
  company_dvd = 'company_dvd',
  departamentos = 'departamentos',
  empresa = 'empresa',
  fabricante = 'fabricante',
  familias = 'familias',
  jerarquia_campana = 'jerarquia_campana',
  jerarquia_venta = 'jerarquia_venta',
  jerarquias_ratio_especial = 'jerarquias_ratio_especial',
  linea_negocio = 'linea_negocio',
  marcas = 'marcas',
  metodos_envio = 'metodos_envio',
  pedido_personalizado = 'pedido_personalizado',
  productos = 'productos',
  proveedor = 'proveedor',
  serie = 'serie',
  subcanal = 'subcanal',
  tipo_centro = 'tipo_centro',
  tipo_gestion = 'tipo_gestion',
  tipo_referencia = 'tipo_referencia',
  volumen = 'volumen',
}

export interface MastersConfiguration {
  masters: {
    [key in Masters]?: {
      id: string;
    };
  };
}
