import { environment } from '@env/environment';
import { ConfigurationDefinition } from './models/configuration.model';
import { emptyPropertyValitator } from './validators/empty-property-validator';

function split(str: string): string[] {
  if (!str) {
    return [];
  }
  return str.replace(' ', '').split(',');
}

function parseStr(str: string = ''): string {
  return str.trim();
}

export const configuration: ConfigurationDefinition = {
  activeFunctionalities: split(environment?.ACTIVE_FUNCTIONALITIES),
  cities: parseStr(environment?.ID_CITIES),
  countries: parseStr(environment?.ID_COUNTRIES),
  urlApis: parseStr(environment?.API_URL_BACKOFFICE),
  languages: split(environment?.LANGUAGES),
  offerEntityType: '2a27030a-573a-4a4a-b234-15edf2eee679',
  entityTypeIdGoodType: parseStr(environment?.ENTITY_TYPE_ID_GOOD_TYPE),
  entityTypeIdCategories: parseStr(environment?.ENTITY_TYPE_ID_CATEGORIES),
  entityTypeIdCenters: parseStr(environment.ENTITY_TYPE_ID_CENTERS),
  entityTypeIdItemOffered: parseStr(environment?.ENTITY_TYPE_ID_ITEM_OFFERED),
  entityTypeIdOffer: parseStr(environment?.ENTITY_TYPE_ID_OFFER),
  entityTypeIdBrand: parseStr(environment.ENTITY_TYPE_ID_BRAND),
  entityTypeIdBundleOf: parseStr(environment.ENTITY_TYPE_ID_BUNDLE_OF),
  entityTypeIdProvider: parseStr(environment.ENTITY_TYPE_ID_PROVIDER),
  entityTypeIdPriceSpecifications: parseStr(environment.ENTITY_TYPE_ID_PRICE_SPECIFICATIONS),
  entityTypeIdSellers: 'f6c2384d-ade2-4549-9405-2e8f48efef3b',
  entityTypeIdSites: parseStr(environment?.ENTITY_TYPE_ID_SITES),
  ruleEngine: {
    conditionsBasicLimitSaleIds: [
      'Department',
      'Family',
      'Brand',
      'Barra',
      'Categories',
      'Provider',
      'Good Types',
      'Classifications',
      'Management Type',
      'Customised Request',
      'Sales Reference',
      'Size Code',
      'Product',
    ],
  },
  attributePath: {
    department: parseStr(environment?.ATTRIBUTE_ROUTE_DEPARTMENT),
    family: parseStr(environment?.ATTRIBUTE_ROUTE_FAMILY),
    brand: parseStr(environment?.ATTRIBUTE_ROUTE_BRAND_IDENTIFIER),
    categories: parseStr(environment?.ATTRIBUTE_ROUTE_CATEGORIES),
    provider: parseStr(environment?.ATTRIBUTE_ROUTE_PROVIDER_IDENTIFIER),
    goodTypes: parseStr(environment?.ATTRIBUTE_ROUTE_GOOD_TYPES),
    classifications1: parseStr(environment?.ATTRIBUTE_ROUTE_CLASSIFICATION1_6),
    classifications2: parseStr(environment?.ATTRIBUTE_ROUTE_CLASSIFICATION1_6),
    classifications3: parseStr(environment?.ATTRIBUTE_ROUTE_CLASSIFICATION1_6),
    classifications4: parseStr(environment?.ATTRIBUTE_ROUTE_CLASSIFICATION1_6),
    classifications5: parseStr(environment?.ATTRIBUTE_ROUTE_CLASSIFICATION1_6),
    classifications6: parseStr(environment?.ATTRIBUTE_ROUTE_CLASSIFICATION1_6),
    managementType: parseStr(environment?.ATTRIBUTE_ROUTE_MANAGEMENT_TYPE),
    customisedRequest: parseStr(environment?.ATTRIBUTE_ROUTE_CUSTOMISED_REQUEST),
    saleReference: parseStr(environment?.ATTRIBUTE_ROUTE_SALEREFERENCE),
    barra: parseStr(environment?.ATTRIBUTE_ROUTE_BARRA),
    sizeCode: parseStr(environment?.ATTRIBUTE_ROUTE_SIZECODE),
    sites: parseStr(environment?.ATTRIBUTE_ROUTE_SITES),
    centers: parseStr(environment.ATTRIBUTE_ROUTE_CENTERS),
    price: parseStr(environment.ATTRIBUTE_ROUTE_PRICE),
    discount: parseStr(environment.ATTRIBUTE_ROUTE_DISCOUNT),
    product: parseStr(environment?.ATTRIBUTE_ROUTE_PRODUCT_ID),
    gtin: parseStr(environment?.ATTRIBUTE_ROUTE_GTIN),
  },
  multivaluedIds: {
    classifications: {
      identifier: parseStr(environment?.ATTRIBUTE_ROUTE_CLASSIFICATION1_6_IDENTIFIER),
      value: parseStr(environment?.ATTRIBUTE_ROUTE_CLASSIFICATION1_6_VALUE),
    },
  },
  adminGroupType: {
    excludeSearch: parseStr(environment?.ADMIN_GROUP_EXCLUDE_SEARCH),
    limitSale: parseStr(environment?.ADMIN_GROUP_LIMIT_SALE),
    priceInheritance: parseStr(environment?.ADMIN_GROUP_PRICE_INHERITANCE),
    contentGroup: parseStr(environment?.ADMIN_GROUP_CONTENT_GROUP),
    sizeGuide: parseStr(environment?.ENTITY_TYPE_ID_SIZE_GUIDE),
    specialProducts: parseStr(environment?.ENTITY_TYPE_ID_SPECIAL_PRODUTCS),
    expert: parseStr(environment?.ENTITY_TYPE_ID_EXPERT),
    fluorGases: parseStr(environment?.ENTITY_TYPE_ID_FLUOR_GASES),
    loyalty: parseStr(environment.ENTITY_TYPE_ID_LOYALTY),
    badge: parseStr(environment.ENTITY_TYPE_ID_BADGE),
  },
  groupedAttributes: {
    department: parseStr(environment?.ATTRIBUTE_ROUTE_DEPARTMENT),
    family: parseStr(environment?.ATTRIBUTE_ROUTE_FAMILY),
    barra: parseStr(environment?.ATTRIBUTE_ROUTE_BARRA),
    product: parseStr(environment?.ATTRIBUTE_ROUTE_PRODUCT_ID),
    saleReference: parseStr(environment?.ATTRIBUTE_ROUTE_SALEREFERENCE),
    sizeCode: parseStr(environment?.ATTRIBUTE_ROUTE_SIZECODE),
    gtin: parseStr(environment?.ATTRIBUTE_ROUTE_GTIN),
  },
};

interface MaybeConfiguration {
  [key: string]: string | string[] | MaybeConfiguration;
}

setTimeout(() => {
  try {
    console.log('validating config...');
    const configErrors = emptyPropertyValitator(configuration as MaybeConfiguration);
    for (const error of configErrors) {
      console.log(' found empty config:', error);
    }
    if (!configErrors.length) {
      console.log('all configs are filled');
    }
  } catch (e) {
    console.log('error Validating config', configuration);
  }
});
