interface MaybeConfiguration {
  [key: string]: string | string[] | MaybeConfiguration;
}

export function emptyPropertyValitator(obj: MaybeConfiguration): string[] {
  const dataToCompare: Record<string, string>[] = Array.from(unfold(obj));
  const result = [];
  for (const { key, val } of dataToCompare) {
    if (Array.isArray(val)) {
      for (const val1 of val) {
        if (val1 === '') {
          result.push(key);
          break;
        }
      }
    } else {
      if (val === '') {
        result.push(key);
      }
    }
  }
  return result;
}

function* unfold(obj: MaybeConfiguration): any {
  const keys = Object.keys(obj);
  for (const key of keys) {
    const val = obj[key];
    if (typeof val === 'string' || Array.isArray(val)) {
      yield { key, val };
    } else {
      yield* unfold(val);
    }
  }
}
