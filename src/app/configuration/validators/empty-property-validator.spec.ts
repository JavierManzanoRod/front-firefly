import { emptyPropertyValitator } from './empty-property-validator';
interface MaybeConfiguration {
  [key: string]: string | string[] | MaybeConfiguration;
}

describe('emptyPropertyValitator', () => {
  it('empty string gives some message with the key', () => {
    const val: MaybeConfiguration = {
      a: '',
      b: 'ddd',
      c: {
        a: '1',
        b: ['c'],
      },
    };

    const validation = emptyPropertyValitator(val);
    expect(validation).toEqual(['a']);
  });

  it('empty string in array gives error', () => {
    const val = {
      a: '1',
      b: [''],
      c: {
        a: '1',
      },
    };
    const validation = emptyPropertyValitator(val);
    expect(validation).toEqual(['b']);
  });

  it('two empty values accumulate the result', () => {
    const val = {
      a: '',
      b: '',
      c: {
        a: '1',
      },
    };
    const validation = emptyPropertyValitator(val);
    expect(validation).toEqual(['a', 'b']);
  });

  it('valid config gives empty array', () => {
    const val = {
      a: '1',
      b: ['ee', 'ccc'],
      c: {
        a: '1',
      },
    };
    const validation = emptyPropertyValitator(val);
    expect(validation).toEqual([]);
  });
});
