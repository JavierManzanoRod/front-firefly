import { InjectionToken } from '@angular/core';
import { IDCOMPOSITOR } from 'src/app/catalog/technical-characteristics/models/technical-characteristics.model';
import { ExplodedTree } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { configuration } from '../configuration';
import { mastersConfiguration } from '../masters';
import { ConfigurationDefinition } from '../models/configuration.model';
import { MastersConfiguration } from '../models/masters.model';

export const FF_LIMIT_SALES_CONDITIONS_BASIC_IDS = new InjectionToken<string[]>('ruleEngine limitSales conditionsIds', {
  factory: () => (configuration.ruleEngine ? configuration.ruleEngine.conditionsBasicLimitSaleIds : []),
});

export const FF_MASTERS = new InjectionToken<MastersConfiguration['masters']>('Masters', {
  factory: () => mastersConfiguration.masters,
});

export const FF_ATTRIBUTE_PATHS = new InjectionToken<ConfigurationDefinition['attributePath']>('Attribute Paths', {
  factory: () => configuration.attributePath,
});

export const FF_ENTITY_TYPE_ID_OFFER = new InjectionToken<ConfigurationDefinition['entityTypeIdOffer']>('Entity Type Id Offer', {
  factory: () => configuration.entityTypeIdOffer,
});

export const FF_ENTITY_TYPE_ID_CATEGORIES = new InjectionToken<ConfigurationDefinition['entityTypeIdCategories']>(
  'Entity Type Id Categories',
  {
    factory: () => configuration.entityTypeIdCategories,
  }
);

export const FF_ENTITY_TYPE_ID_SITES = new InjectionToken<ConfigurationDefinition['entityTypeIdSites']>('Entity Type Id Sites', {
  factory: () => configuration.entityTypeIdSites,
});

export const FF_ENTITY_TYPE_ID_CENTERS = new InjectionToken<ConfigurationDefinition['entityTypeIdCenters']>('Entity Type Id Centers', {
  factory: () => configuration.entityTypeIdCenters,
});

export const FF_ENTITY_TYPE_ID_SELLERS = new InjectionToken<ConfigurationDefinition['entityTypeIdSellers']>('Entity Type Id Sellers', {
  factory: () => configuration.entityTypeIdSellers,
});

export const FF_ENTITY_TYPE_ID_BRAND = new InjectionToken<ConfigurationDefinition['entityTypeIdBrand']>('Entity Type Id Brand', {
  factory: () => configuration.entityTypeIdBrand,
});

export const FF_ENTITY_TYPE_ID_PROVIDER = new InjectionToken<ConfigurationDefinition['entityTypeIdProvider']>('Entity Type Id Provider', {
  factory: () => configuration.entityTypeIdProvider,
});

export const FF_ENTITY_TYPE_GOOD_TYPE = new InjectionToken<ConfigurationDefinition['entityTypeIdGoodType']>('Entity Type Id Good Type', {
  factory: () => configuration.entityTypeIdGoodType,
});

export const FF_ENTITY_TYPE_PRICE_SPECIFICATIONS = new InjectionToken<ConfigurationDefinition['entityTypeIdPriceSpecifications']>(
  'Entity Type Id Price Specifications',
  {
    factory: () => configuration.entityTypeIdPriceSpecifications,
  }
);

export const FF_MULTIVALUED_IDS = new InjectionToken<ConfigurationDefinition['multivaluedIds']>('Multivalued ids', {
  factory: () => configuration.multivaluedIds,
});

export const FF_GROUPED_ATTRIBUTES = new InjectionToken<ConfigurationDefinition['groupedAttributes']>('Grouped attributes', {
  factory: () => configuration.groupedAttributes,
});

export const FF_COMPOSER_TREE = new InjectionToken<ExplodedTree>('Tree Composer', {
  factory: () => {
    return {
      id: IDCOMPOSITOR,
      name: 'Compositor',
      path: 'ROOT',
      fixed: true,
      entityType: null,
      children: [
        {
          id: 'FREE_TEXT',
          label: 'Texto Libre',
          icon: 'icon-texto-libre',
        },
        {
          id: 'NEW_LINE',
          label: 'Salto de Línea',
          icon: 'icon-salto-de-linea',
        },
        {
          label: 'Espacio',
          id: 'SPACE',
          icon: 'icon-espacio',
        },
        {
          label: 'Símbolo X',
          id: 'SYMBOL_X',
          icon: 'icon-simbolo-x',
        },
        {
          label: 'Símbolo Asterisco',
          id: 'SYMBOL_STAR',
          icon: 'icon-simbolo-asterisco',
        },
        {
          label: 'Paréntesis (apertura)',
          id: 'OPENING_BRACKETS',
          icon: 'icon-abre-parentesis',
        },
        {
          label: 'Paréntesis (cierre)',
          id: 'CLOSING_BRACKETS',
          icon: 'icon-cierra-parentesis',
        },
        {
          label: 'Comillas',
          id: 'QUOTE',
          icon: 'icon-comillas',
        },
        {
          label: 'Barra',
          id: 'DASH',
          icon: 'icon-barra',
        },
        {
          label: 'Sección con Línea',
          id: 'SECTION_WITH_LINE',
          icon: 'icon-seccion-con-linea',
        },
      ],
    };
  },
});
