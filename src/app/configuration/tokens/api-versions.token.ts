import { InjectionToken } from '@angular/core';
import { apiPathVersionConfiguration } from '../api-path-version-configuration';

export const FF_API_PATH_VERSION_LIMIT_SALE = new InjectionToken<string>('Api path version Limit sale', {
  factory: () => apiPathVersionConfiguration?.limitSale || '',
});

export const FF_API_PATH_VERSION_SITES = new InjectionToken<string>('Api path version Sites', {
  factory: () => apiPathVersionConfiguration?.sites || '',
});

export const FF_API_PATH_VERSION_SUBSITES = new InjectionToken<string>('Api path version SubSites', {
  factory: () => apiPathVersionConfiguration?.subSites || '',
});

export const FF_API_PATH_VERSION_TECHNICAL_CHARACTERISTICS = new InjectionToken<string>('Api path version technicalCharacteristics', {
  factory: () => apiPathVersionConfiguration?.technicalCharacteristics || '',
});

export const FF_API_PATH_VERSION_ATTRIBUTE_TRANSLATION = new InjectionToken<string>('Api path version attributeTranslation', {
  factory: () => apiPathVersionConfiguration?.attributeTranslation || '',
});

export const FF_API_PATH_VERSION_FACETS = new InjectionToken<string>('Api path version facets', {
  factory: () => apiPathVersionConfiguration?.facets || '',
});

export const FF_API_PATH_VERSION_ENTITY_TYPES = new InjectionToken<string>('Api path version entity types', {
  factory: () => apiPathVersionConfiguration?.entityTypes || '',
});

export const FF_API_PATH_VERSION_ENTITIES = new InjectionToken<string>('Api path version entities', {
  factory: () => apiPathVersionConfiguration?.entities || '',
});

export const FF_API_PATH_VERSION_ADMIN_GROUP_FOLDERS = new InjectionToken<string>('Api path version admin group folders', {
  factory: () => apiPathVersionConfiguration?.adminGroupFolders || '',
});

export const FF_API_PATH_VERSION_CONTENT_GROUPS = new InjectionToken<string>('Api path version content groups', {
  factory: () => apiPathVersionConfiguration?.contentGroups || '',
});

export const FF_API_PATH_VERSION_FACET_LINKS = new InjectionToken<string>('Api path version facet links', {
  factory: () => apiPathVersionConfiguration?.facetLinks || '',
});

export const FF_API_PATH_VERSION_ADMIN_GROUPS = new InjectionToken<string>('Api path version admin groups', {
  factory: () => apiPathVersionConfiguration?.adminGroups || '',
});

export const FF_API_PATH_VERSION_ADMIN_GROUPS_TYPES = new InjectionToken<string>('Api path version admin groups types', {
  factory: () => apiPathVersionConfiguration?.adminGroupsTypes || '',
});

export const FF_API_PATH_VERSION_CATEGORY_RULES = new InjectionToken<string>('Api path version category rules', {
  factory: () => apiPathVersionConfiguration?.categoryRules || '',
});

export const FF_API_PATH_VERSION_ATTRIBUTE_TREE_NODE = new InjectionToken<string>('Api path version attribute tree node', {
  factory: () => apiPathVersionConfiguration?.attributeTreeNode || '',
});

export const FF_API_PATH_VERSION_PRODUCT_TYPES = new InjectionToken<string>('Api path version product types', {
  factory: () => apiPathVersionConfiguration?.productTypes || '',
});

export const FF_API_PATH_VERSION_EXCLUDE_SEARCHES = new InjectionToken<string>('Api path version exclude searches', {
  factory: () => apiPathVersionConfiguration?.excludeSearches || '',
});

export const FF_API_PATH_VERSION_PROMOTIONS = new InjectionToken<string>('Api path version promotions', {
  factory: () => apiPathVersionConfiguration?.promotions || '',
});

export const FF_API_PATH_VERSION_GEOZONES_SCOPES = new InjectionToken<string>('Api path version geozones scopes', {
  factory: () => apiPathVersionConfiguration?.geozonesScopes || '',
});

export const FF_API_PATH_VERSION_GEOZONES_TARGETS = new InjectionToken<string>('Api path version geozones targets', {
  factory: () => apiPathVersionConfiguration?.geozonesTargets || '',
});

export const FF_API_PATH_VERSION_PRODUCTS_MANAGE_SCHEMA = new InjectionToken<string>('Api path version products manage schema', {
  factory: () => apiPathVersionConfiguration?.productsManageSchema || '',
});

export const FF_API_PATH_VERSION_MARKETPLACE_SELLERS = new InjectionToken<string>('Api path version marketplace sellers', {
  factory: () => apiPathVersionConfiguration?.marketplaceSellers || '',
});

export const FF_API_PATH_VERSION_MARKETPLACE_PRODUCT_TYPES = new InjectionToken<string>('Api path version marketplace product types', {
  factory: () => apiPathVersionConfiguration?.marketplaceProductTypes || '',
});

export const FF_API_PATH_VERSION_MARKETPLACE_PRODUCT_RELATIONS = new InjectionToken<string>(
  'Api path version marketplace product relations',
  {
    factory: () => apiPathVersionConfiguration?.marketplaceProductRelations || '',
  }
);

export const FF_API_PATH_VERSION_FACET_ATTRIBUTES = new InjectionToken<string>('Api path version facet attributes', {
  factory: () => apiPathVersionConfiguration?.facetAttributes || '',
});

export const FF_API_PATH_VERSION_EXPLODED_TREE = new InjectionToken<string>('Api path version exploded tree', {
  factory: () => apiPathVersionConfiguration?.explodedTree || '',
});

export const FF_API_PATH_VERSION_ASSORTMENT_PRODUCT_SEARCHERS = new InjectionToken<string>(
  'Api path version assortment product searchers',
  {
    factory: () => apiPathVersionConfiguration?.assortmentProductSearchers || '',
  }
);

export const FF_API_PATH_VERSION_EVALUATE_RULES = new InjectionToken<string>('Api path version assortment evalute rules', {
  factory: () => apiPathVersionConfiguration?.evaluateRules || '',
});

export const FF_API_PATH_VERSION_AUDIT_CATALOG_DASHBOARD = new InjectionToken<string>('Api path version assortment evalute rules', {
  factory: () => apiPathVersionConfiguration?.auditCatalogDashboard || '',
});

export const FF_API_PATH_VERSION_EXPERT = new InjectionToken<string>('Api path version expert list', {
  factory: () => apiPathVersionConfiguration?.expert || '',
});

export const FF_API_PATH_VERSION_PRODUCT_SEARCH = new InjectionToken<string>('Api path version productSearch', {
  factory: () => apiPathVersionConfiguration?.productSearch || '',
});
