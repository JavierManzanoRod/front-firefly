import { InjectionToken } from '@angular/core';
import { configuration } from '../configuration';
import { AdminGroupTypeMaster } from '../models/configuration.model';

export const FF_EXCLUDE_SEARCH = new InjectionToken<string>('Exclude Search', {
  factory: () => configuration.adminGroupType?.excludeSearch || '',
});

export const FF_LIMIT_SALE = new InjectionToken<string>('Limit Sale', {
  factory: () => configuration.adminGroupType?.limitSale || '',
});

export const FF_PRICE_INHERITANCE = new InjectionToken<string>('Price Inheritance', {
  factory: () => configuration.adminGroupType?.priceInheritance || '',
});

export const FF_CONTENT_GROUP = new InjectionToken<string>('Content Group', {
  factory: () => configuration.adminGroupType?.contentGroup || '',
});

export const FF_ITEM_OFFERED = new InjectionToken<string>('Item Offered', {
  factory: () => configuration.entityTypeIdItemOffered || '',
});

export const FF_SIZE_GUIDE = new InjectionToken<string>('Size Guide', {
  factory: () => configuration.adminGroupType?.sizeGuide || '',
});

export const FF_SPECIAL_PRODUCTS = new InjectionToken<string>('Special Produtcs', {
  factory: () => configuration.adminGroupType?.specialProducts || '',
});
export const FF_FLUOR_GASES = new InjectionToken<string>('Fluorinated gases', {
  factory: () => configuration.adminGroupType?.fluorGases || '',
});

export const FF_EXPERT = new InjectionToken<string>('Consulta de experto', {
  factory: () => configuration.adminGroupType?.expert || '',
});

export const FF_LOYALTY = new InjectionToken<string>('Consulta de fidelización', {
  factory: () => configuration.adminGroupType?.loyalty || '',
});

export const FF_BADGE = new InjectionToken<string>('Consulta de BADGE', {
  factory: () => configuration.adminGroupType?.badge || '',
});

export const FF_BUNDLE_OF = new InjectionToken<string>('Id Bundle Of', {
  factory: () => configuration.entityTypeIdBundleOf || '',
});

export const FF_ALL_ADMIN_GROUP_TYPE = new InjectionToken<{ [key in AdminGroupTypeMaster]: string } | Record<string, unknown>>(
  'Objet with all admin group type',
  {
    factory: () => configuration.adminGroupType || {},
  }
);
