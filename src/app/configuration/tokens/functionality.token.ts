import { InjectionToken } from '@angular/core';
import { Functionality } from 'src/app/components/menu-left/model/menu-left-model';
import { configuration } from '../configuration';

// todos son negaciones o todos son afirmaciones
function allAffirmative(xs: string[]) {
  return xs.every((str) => !str.startsWith('!'));
}

function allNegative(xs: string[]) {
  return xs.every((str) => str.startsWith('!'));
}

function valid(xs: string[]) {
  return allAffirmative(xs) || allNegative(xs);
}

function getFunctionalities(arrConfig: string[]) {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  const funcionalityValues = Object.keys(Functionality).map((e) => Functionality[e as unknown as Functionality]);
  let result = [];
  for (const txtConfig of arrConfig) {
    const parseText = txtConfig.replace('!', '');
    if (funcionalityValues.indexOf(parseText as Functionality) >= 0) {
      result.push(txtConfig);
    } else {
      console.warn('configManager. config ' + txtConfig + ' is not a valid Functionality');
    }
  }

  if (!valid(arrConfig)) {
    console.warn('Las funcionalidades del config manager deben obien afirmativos o negativos. Cogemos los positivos');
    result = arrConfig.filter((item) => !item.startsWith('!'));
  }

  return result;
}

function checkFuncionalities(arrConfig: string[]): Functionality[] {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  const all = Object.keys(Functionality).map((e) => Functionality[e as unknown as Functionality]);
  if (arrConfig.indexOf('*') >= 0) {
    return all;
  }

  const functionalitiesActivesAndInactives = getFunctionalities(arrConfig);

  if (functionalitiesActivesAndInactives.findIndex((item) => item.startsWith('!')) >= 0) {
    const result: any[] = [];

    all.forEach((funcionality: Functionality) => {
      if (arrConfig.indexOf('!' + funcionality) === -1) {
        result.push(funcionality);
      }
    });
    return result;
  }

  return functionalitiesActivesAndInactives as any;
}

export const FF_ACTIVE_FUNCIONALITIES = new InjectionToken<Functionality[]>('actives Functionalities', {
  factory: () => checkFuncionalities(configuration.activeFunctionalities || []),
});
