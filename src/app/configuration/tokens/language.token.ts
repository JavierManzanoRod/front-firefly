import { InjectionToken } from '@angular/core';
import { configuration } from '../configuration';

export const FF_LANGUAGES = new InjectionToken<string[]>('Languages', {
  factory: () => configuration.languages || [],
});
