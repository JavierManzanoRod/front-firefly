import { inject, InjectionToken } from '@angular/core';
import { EntityType } from '@core/models/entity-type.model';
import { Observable, throwError } from 'rxjs';
import { EntityRepository } from 'src/app/rule-engine/entity/models/entity.model';
import { EntityService } from 'src/app/rule-engine/entity/services/entity.service';
import { configuration } from '../configuration';

export const FF_CITIES = new InjectionToken<string>('Cities', {
  factory: () => configuration.cities || '',
});

export const FF_COUNTRIES = new InjectionToken<string>('Countries', {
  factory: () => configuration.countries || '',
});

function getObs(key: string | undefined, name = ''): Observable<EntityRepository[]> {
  if (!key) {
    return throwError('Failed key ' + name);
  }
  return inject(EntityService).search({ id: key } as unknown as EntityType);
}

export const FF_OBS_COUNTRIES = new InjectionToken<Observable<EntityRepository[]>>('Countries OBS', {
  factory: () => {
    const key = configuration.countries;
    return getObs(key, 'countries');
  },
});

export const FF_OBS_CITIES = new InjectionToken<Observable<EntityRepository[]>>('Cities OBS', {
  factory: () => {
    const key = configuration.cities;
    return getObs(key, 'cities');
  },
});
