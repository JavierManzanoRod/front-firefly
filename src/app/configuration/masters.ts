import { MastersConfiguration } from './models/masters.model';

export const mastersConfiguration: MastersConfiguration = {
  masters: {
    calificacion1: {
      id: 'idCalificacion1',
    },
    calificacion2: {
      id: 'idCalificacion2',
    },
    calificacion3: {
      id: 'idCalificacion3',
    },
    calificacion4: {
      id: 'idCalificacion4',
    },
    calificacion5: {
      id: 'idCalificacion5',
    },
    calificacion6: {
      id: 'idCalificacion6',
    },
    canal: {
      id: 'canal',
    },
    categorias: {
      id: 'categorias',
    },
    centros: {
      id: 'categorias',
    },
    channel_dvd: {
      id: 'channel_dvd',
    },
    codigo_tienda: {
      id: 'codigo_tienda',
    },
    company_dvd: {
      id: 'company_dvd',
    },
    departamentos: {
      id: 'departamentos',
    },
    empresa: {
      id: 'empresa',
    },
    fabricante: {
      id: 'fabricante',
    },
    familias: {
      id: 'familias',
    },
    jerarquia_campana: {
      id: 'jerarquia_campana',
    },
    jerarquia_venta: {
      id: 'jerarquia_venta',
    },
    jerarquias_ratio_especial: {
      id: 'jerarquias_ratio_especial',
    },
    linea_negocio: {
      id: 'linea_negocio',
    },
    marcas: {
      id: 'marcas',
    },
    metodos_envio: {
      id: 'metodos_envio',
    },
    pedido_personalizado: {
      id: 'pedido_personalizado',
    },
    productos: {
      id: 'productos',
    },
    proveedor: {
      id: 'proveedor',
    },
    serie: {
      id: 'serie',
    },
    subcanal: {
      id: 'subcanal',
    },
    tipo_centro: {
      id: 'tipo_centro',
    },
    tipo_gestion: {
      id: 'tipo_gestion',
    },
    tipo_referencia: {
      id: 'tipo_referencia',
    },
    volumen: {
      id: 'volumen',
    },
  },
};
