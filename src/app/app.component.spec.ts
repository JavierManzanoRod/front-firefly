import { Component, Input } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { User } from '@model/user';
import { TranslateService } from '@ngx-translate/core';
import { LoginOptions, OAuthService } from 'angular-oauth2-oidc';
import { of, Subject } from 'rxjs';
import { AppComponent } from './app.component';
import { ToastService } from './modules/toast/toast.service';
import { UserService } from './shared/services/user.service';

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}

  public get(key: any): any {
    of(key);
  }
}

class OAuthServiceStub {
  public configure(data?: any) {}

  public setupAutomaticSilentRefresh(data?: any) {}

  public loadDiscoveryDocumentAndTryLogin(): Promise<boolean> {
    return Promise.resolve(true);
  }

  public getIdentityClaims() {
    return {} as User;
  }
  tryLogin(options?: LoginOptions) {
    return Promise.resolve();
  }
}

@Component({
  selector: 'ff-menu-left',
  template: '',
})
class MockMenuLeftComponent {
  @Input() isMenuOpen!: boolean;
}

@Component({
  selector: 'ff-menu-top',
  template: '',
})
class MockMenuTopComponent {
  @Input() isMenuOpen!: boolean;
  @Input() user!: User;
}

describe('AppComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AppComponent, MockMenuTopComponent, MockMenuLeftComponent],
        imports: [RouterTestingModule],
        providers: [
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: OAuthService, useClass: OAuthServiceStub },
          {
            provide: ToastService,
            useFactory: () => ({
              error: () => {},
            }),
          },
          {
            provide: UserService,
            useFactory: () => ({
              userSubject: new Subject(),
              getUser: () => null,
            }),
          },
        ],
      }).compileComponents();
    })
  );

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should render the menuTop', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('ff-menu-top')).toEqual(null);
  });

  it('should render the menuLeft', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('ff-menu-left')).toEqual(null);
  });
});
