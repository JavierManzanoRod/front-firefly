import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'admin-group-type',
    loadChildren: () => import('./admin-group-type/admin-group-type.module').then((m) => m.AdminGroupTypeModule),
  },
  {
    path: 'entity-types',
    loadChildren: () => import('./entity-type/entity-type.module').then((m) => m.EntityTypeModule),
  },
  {
    path: 'entity',
    loadChildren: () => import('./entity/entity.module').then((m) => m.EntityModule),
  },
  {
    path: 'admin-group',
    loadChildren: () => import('./admin-group/admin-group.module').then((m) => m.AdminGroupModule),
  },
  {
    path: 'category-rule',
    loadChildren: () => import('./category-rule/category-rule.module').then((m) => m.CategoryRuleModule),
  },
  {
    path: 'price-inheritance',
    loadChildren: () => import('./price-inheritance/price-inheritance.module').then((m) => m.PriceInheritanceModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RuleEngineRoutingModule {}
