import { MayHaveIdName } from '@model/base-api.model';
import { I18nValue } from './entity.model';

// export interface EntityDTO {
//     identifier?: string;
//     entity_type_id: string;
//     entity_type_name?: string;
//     name?: string;
//     type?: string;
//     attributes: AttributeEntityDTO[];
//     group?: string;
//   }

// export interface AttributeEntityDTO {
//     value?: ValueAttEntityRepositoryDTO | I18nValueDTO;
//     identifier?: string;
//     label: string;
//     name: string;
//     weight?: number;
//     data_type: 'STRING' | 'DOUBLE' | 'BOOLEAN' | 'ENTITY' | 'EMBEDDED' | 'DATE_TIME';
//     entity_type_id?: string;
//     label_attribute?: boolean;
//     entity_reference?: string;
//     entity_reference_name?: string;
//     multiple_cardinality?: boolean;
//     is_i18n?: boolean;
//   }

export type ValueDTO = MayHaveIdName | number | string | EntityRepositoryDTO | EntityRepositoryDetailDTO | boolean;

export type ValueAttEntityRepositoryDTO = ValueDTO | ValueDTO[];

export type AttributeOriginal = { [key: string]: ValueAttEntityRepositoryDTO | I18nValue | undefined };

export interface AttributeContainer {
  identifier: string;
  is_label_attribute: boolean;
  value: ValueAttEntityRepositoryDTO | I18nValue | undefined;
}

export interface EntityRepositoryDTO {
  identifier?: string;
  entity_type_id: string;
  attributes: AttributeOriginal | AttributeContainer[];
  name?: string;
  is_master?: boolean;
  type?: string;
}

export interface EntityRepositoryDetailDTO {
  identifier?: string;
  name?: string;
  type?: string;
  attributes?: any;
  is_master?: boolean;
}

export interface I18nValueDTO {
  [key: string]: string | (string | null)[];
}
