import { GenericApiResponse, MayHaveIdName } from '@model/base-api.model';
import { ChipsControlSelectConfig } from 'src/app/modules/chips-control/components/chips.control.model';

export type Value = MayHaveIdName | number | string | EntityRepository | EntityRepositoryDetail | boolean;
export type ValueAttEntityRepository = Value | Value[];

export interface I18nValue {
  [key: string]: string | (string | null)[];
}

export interface AttributeEntityView {
  id: string;
  label: string;
  name: string;
}

export interface AttributeEntity {
  value?: any;
  id?: string;
  path?: string;
  label: string;
  configChips?: ChipsControlSelectConfig;
  name: string;
  weight?: number;
  data_type: 'STRING' | 'DOUBLE' | 'BOOLEAN' | 'ENTITY' | 'EMBEDDED' | 'DATE_TIME' | 'VIRTUAL';
  entity_type_id?: string;
  label_attribute?: boolean;
  entity_reference?: string;
  entity_reference_name?: string;
  multiple_cardinality?: boolean;
  is_i18n?: boolean;
  entity_view?: AttributeEntityView;
  childrens?: AttributeEntity[];
}

export interface EntityRepository {
  id?: string;
  entity_type_id: string;
  attributes: { [key: string]: ValueAttEntityRepository | I18nValue | undefined };
  name?: string;
  label?: string;
  type?: string;
  values?: { [key: string]: ValueAttEntityRepository | I18nValue | undefined };
}

export interface EntityRepositoryDetail {
  id?: string;
  name?: string;
  type?: string;
  attributes?: any;
}

export type EntityRepositoryResponse = GenericApiResponse<EntityRepository>;

export interface Entity {
  id?: string;
  entity_type_id: string;
  entity_type_name?: string;
  name?: string;
  type?: string;
  is_master: boolean;
  attributes: AttributeEntity[];
  values?: { [key: string]: any };
  group?: string;
}

export type EntityStructureAttribute = {
  value?: any;
  attributes?: EntityStructureAttribute[];
  path: string;
};

export type EntityResponse = GenericApiResponse<Entity>;
