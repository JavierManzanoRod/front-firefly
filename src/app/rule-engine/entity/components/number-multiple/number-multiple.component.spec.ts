import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NumberMultipleComponent } from './number-multiple.component';

describe('NumberMultipleComponent', () => {
  let component: NumberMultipleComponent;
  let fixture: ComponentFixture<NumberMultipleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NumberMultipleComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
