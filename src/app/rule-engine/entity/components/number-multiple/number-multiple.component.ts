import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../modules/chips-control/components/chips.control.model';
import { numberValidator } from '../../../../shared/validations/number-validator';

@Component({
  selector: 'ff-number-multiple',
  templateUrl: 'number-multiple.component.html',
  styleUrls: ['./number-multiple.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NumberMultipleComponent),
      multi: true,
    },
  ],
})
export class NumberMultipleComponent implements OnInit, ControlValueAccessor {
  @Input() label = '';
  disabled = false;

  value: any[] = [];
  chipsSelectConfig: ChipsControlSelectConfig = {
    modalType: ModalChipsTypes.free,
    modalConfig: {
      title: 'FIELD_NUMBER_MULTIPLE.MODAL_TITLE',
      inputValidators: [numberValidator()],
      validatorErrors: [{ type: 'number', message: 'COMMON_ERRORS.NUMBER' }],
    },
  };

  constructor() {}

  ngOnInit() {
    this.chipsSelectConfig.label = this.label;
  }

  writeValue(data: number[]): void {
    this.value = data;
  }

  onModelChange() {
    this.onChange(this.value ? this.value.map((d) => parseFloat(d)) : this.value);
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  // Allows Angular to register a function to call when the input has been touched.
  // Save the function as a property to call later here.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  registerOnTouched(fn: () => void): void {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange(data: any) {
    // don't delete!
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }
}
