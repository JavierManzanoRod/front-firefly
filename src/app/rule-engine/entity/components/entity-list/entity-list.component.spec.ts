import { EntityListComponent } from './entity-list.component';
import { EntityUtilsService } from '../../services/entity.utils.service';
import { EntityType } from '@core/models/entity-type.model';
import { Entity } from '../../models/entity.model';
import { TranslateService } from '@ngx-translate/core';
import { EntityApiFactoryService } from '../../services/entity-api-factory.service';

let entity: EntityApiFactoryService;

describe('EntityListComponent', () => {
  it('should create the component', () => {
    const comp = new EntityListComponent(
      new EntityUtilsService((null as unknown) as TranslateService, ['es_ES,ca_ES,en_US,en_GB,pt_PT,de_DE,fr_FR']),
      entity as EntityApiFactoryService
    );
    expect(comp).toBeDefined();
  });

  it('ngOnChanes create rows to show', () => {
    const comp = new EntityListComponent(
      new EntityUtilsService((null as unknown) as TranslateService, ['es_ES,ca_ES,en_US,en_GB,pt_PT,de_DE,fr_FR']),
      entity as EntityApiFactoryService
    );
    const data = {
      id: '1',
      name: 'name',
      attributes: [
        {
          id: 'att1',
          name: 'name',
          data_type: 'STRING',
          label: 'label',
          weight: 0,
        },
      ],
    } as EntityType;
    comp.entityType = data;
    comp.list = [{ id: 'entity1', name: 'name1', attributes: [{ id: 'att1' }] } as Entity];
    comp.ngOnChanges();
    expect(comp.rows.length).toBe(comp.list.length);
  });
});
