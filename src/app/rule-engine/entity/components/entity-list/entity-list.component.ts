import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { MayHaveIdName } from '@model/base-api.model';
import { BaseListComponent } from 'src/app/shared/components/base-component-list.component';
import { EntityType } from '../../../../core/models/entity-type.model';
import { Entity } from '../../models/entity.model';
import { EntityApiFactoryService } from '../../services/entity-api-factory.service';
import { EntityUtilsService } from '../../services/entity.utils.service';

@Component({
  selector: 'ff-entity-list',
  templateUrl: './entity-list.component.html',
  styleUrls: ['./entity-list.component.scss'],
})
export class EntityListComponent extends BaseListComponent<Entity> implements OnChanges {
  @Input() entityType: EntityType | undefined;
  @Input() totalItems = 0;
  @Input() hasChanges = false;
  @Output() go: EventEmitter<Entity> = new EventEmitter<Entity>();
  @Output() searched: EventEmitter<string> = new EventEmitter<string>();
  rows: { value: string; tooltip?: string }[][] = [];
  fields: MayHaveIdName[] = [];

  constructor(private utils: EntityUtilsService, public entityApiFactoryService: EntityApiFactoryService) {
    super();
    this.loading = true;
  }

  ngOnChanges() {
    if (this.list && this.entityType) {
      this.fields = [{ name: 'ENTITY_TYPE_DETAIL.NEW_ATTR_MODAL.NAME' }, ...this.entityType.attributes];

      const rows: { value: string; tooltip?: string }[][] = [];
      for (const entity of this.list) {
        const row: { value: string; tooltip?: string }[] = [
          { value: entity.name || '', tooltip: entity.name || '' },
          ...entity.attributes.map((e) => this.utils.valueAsString(e)),
        ];
        rows.push(row);
      }

      this.rows = rows;
      this.loading = false;
    }
  }

  detailEntity(index: number) {
    this.go.emit(this.list[index]);
  }

  handleDelete2(index: number) {
    this.delete.emit(this.list[index]);
  }

  isBooleanValue(value: string) {
    return value === 'true' || value === 'false';
  }

  search(event: any) {
    if (this.entityType) {
      this.entityType.name = event.name;
      this.currentPage = 1;
      this.entityApiFactoryService
        .create(this.entityType)
        .list({
          ...event,
          size: this.pageSize,
        })
        .subscribe((list) => {
          const rows: { value: string; tooltip?: string }[][] = [];
          this.list = list.content;
          for (const entity of list.content) {
            const row: { value: string; tooltip?: string }[] = [
              { value: entity.name || '', tooltip: entity.name || '' },
              ...entity.attributes.map((e) => this.utils.valueAsString(e)),
            ];
            rows.push(row);
          }
          this.rows = rows;
          this.totalItems = list.page.total_elements;
          this.loading = false;
          this.searched.emit(event.name);
        });
    }
  }

  getValue(value: string) {
    if (value === 'true' || value === 'false') {
      return value === 'true' ? 'COMMON.YES' : 'COMMON.NO';
    } else {
      return value;
    }
  }
}
