import { ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ToastService } from '../../../../modules/toast/toast.service';
import { EntityTypeService } from '../../../../shared/services/apis/entity-type/entity-type.service';
import { Entity } from '../../models/entity.model';
import { EntityService } from '../../services/entity.service';
import { EntityDetailComponent } from './entity-detail.component';

describe('EntityDetailComponent', () => {
  let toastServiceSpy: { error: jasmine.Spy };
  let toastService: ToastService;
  let changeDetectionSpy: { error: jasmine.Spy };
  let changeDetection: ChangeDetectorRef;
  let entitySerivceSpy: { error: jasmine.Spy };
  let entitySerivce: EntityService;
  let entityTypeServiceSpy: { error: jasmine.Spy };
  let entityTypeService: EntityTypeService;

  beforeEach(() => {
    toastServiceSpy = jasmine.createSpyObj('ToastService', ['error']);
    toastServiceSpy.error.and.returnValue(null);

    changeDetectionSpy = jasmine.createSpyObj('ChangeDetectorRef', ['error']);
    entitySerivceSpy = jasmine.createSpyObj('EntityService', ['error']);
    entityTypeServiceSpy = jasmine.createSpyObj('EntityTypeService', ['error']);
    entityTypeServiceSpy.error.and.returnValue(null);

    toastService = (toastServiceSpy as unknown) as ToastService;
    changeDetection = (changeDetectionSpy as unknown) as ChangeDetectorRef;
    entitySerivce = (entitySerivceSpy as unknown) as EntityService;
    entityTypeService = (entityTypeServiceSpy as unknown) as EntityTypeService;
  });

  it('it should create ', () => {
    const component = new EntityDetailComponent(new FormBuilder(), entitySerivce, entityTypeService, toastService, changeDetection);
    expect(component).toBeDefined();
  });

  it('ngChanges creates a form with the entity provided. ', () => {
    const component = new EntityDetailComponent(new FormBuilder(), entitySerivce, entityTypeService, toastService, changeDetection);
    const item = {
      id: 'id',
      entity_type_name: 'name',
      attributes: [
        {
          id: 'idatt',
          data_type: 'STRING',
        },
      ],
    } as Entity;
    component.item = item;
    component.ngOnChanges();
    expect(component.form?.controls.name.value).toBe(item.name);
  });

  it('submit emit event if the form is valid', () => {
    const component = new EntityDetailComponent(new FormBuilder(), entitySerivce, entityTypeService, toastService, changeDetection);
    spyOn(component.formSubmit, 'emit');
    const item = {
      id: 'id',
      entity_type_name: 'name',
      attributes: [
        {
          id: 'idatt',
          data_type: 'STRING',
        },
      ],
    } as Entity;
    component.item = item;
    component.ngOnChanges();
    component.form?.controls.name.setValue('someName');
    component.form?.controls.attributes.patchValue(['someVal']);
    component.submit();
    expect(component.formSubmit.emit).toHaveBeenCalled();
  });

  xit('submit does not emit event if the form is invalid', () => {
    const component = new EntityDetailComponent(new FormBuilder(), entitySerivce, entityTypeService, toastService, changeDetection);
    spyOn(component.formSubmit, 'emit');
    const item = {
      id: 'id',
      entity_type_id: 'someId',
      entity_type_name: 'name',
      attributes: [
        {
          id: 'idatt',
          data_type: 'STRING',
        },
      ],
    } as Entity;
    component.item = item;
    component.ngOnChanges();
    component.submit();
    expect(component.formSubmit.emit).not.toHaveBeenCalled();
  });
});
