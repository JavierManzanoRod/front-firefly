import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { GenericApiRequest } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { ExplodedTreeComponent } from 'src/app/modules/exploded-tree/exploded-tree.component';
import { ExplodedClickEvent, ExplodedTreeElementType, ExplodedTreeNode } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { fieldI18nMultipleValidator } from 'src/app/modules/field-i18n/validators/field-i18n-multiple-validator';
import { fieldI18nValidator } from 'src/app/modules/field-i18n/validators/field-i18n-validator';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { arrSomeValidator } from 'src/app/shared/validations/array-some.validator';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../modules/chips-control/components/chips.control.model';
import { ToastService } from '../../../../modules/toast/toast.service';
import { AttributeEntity, Entity, EntityRepository } from '../../models/entity.model';
import { EntityService } from '../../services/entity.service';
import { cloneAbstractControl } from '../../utils/clone-form.utils';

@Component({
  selector: 'ff-entity-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './entity-detail.component.html',
  styleUrls: ['./entity-detail.component.scss'],
})
export class EntityDetailComponent implements OnChanges, OnInit {
  @Input() item: Entity | undefined;
  @Input() original: Entity | undefined;
  @Input() readonly = false;
  @Output() formSubmit: EventEmitter<any> = new EventEmitter<any>();
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() delete: EventEmitter<EntityRepository | Entity> = new EventEmitter<EntityRepository | Entity>();
  @ViewChild(ExplodedTreeComponent) tree!: ExplodedTreeComponent;

  form: FormGroup | undefined;
  item2: Entity | undefined;
  sended = false;
  dataResolved = false;
  values: any[] = [];
  loadTree: ExplodedTreeNode[] = [];

  loading = true;
  show: any = null;
  hasMultipleLabels = false;

  entityChipsSelectConfig: ChipsControlSelectConfig[] = [];
  tab: any = {};
  arrayPathError: string[] = [];
  arrayPathModify: string[] = [];
  arrayPathClear: string[] = [];

  constructor(
    public fb: FormBuilder,
    public apiEntityService: EntityService,
    public apiEntityTypeService: EntityTypeService,
    public toastService: ToastService,
    public change: ChangeDetectorRef
  ) {
    this.form = this.fb.group({
      // eslint-disable-next-line @typescript-eslint/unbound-method
      name: [null, Validators.required],
      // eslint-disable-next-line @typescript-eslint/unbound-method
      entityName: [{ value: '', disabled: true }, Validators.required],
      attributes: this.fb.array([]),
    });
  }

  ngOnChanges() {
    if (this.item) {
      this.item.is_master = true;
      if (this.item.is_master) {
        this.form?.disable();
      }
      if (this.original?.attributes) {
        this.original.attributes = this.parseDataOriginal();
      }

      this.item.attributes?.forEach((att) => {
        att.path = att.id;
        this.loadTree.push(this.loadChildren(att));
        // this.loadTree.push(this.loadChildren(att, this.attributes, undefined, null, this.original?.attributes));
      });
      this.item2 = clone(this.item);
      this.loading = false;
      this.dataResolved = true;

      if (this.form) {
        this.form.patchValue({
          name: this.item2.name,
          entityName: this.item2.entity_type_name,
        });
      }

      this.loadFormArray();

      if (this.form) {
        this.form.controls.attributes.statusChanges.subscribe((a: any) => {
          this.arrayPathError = [];
          this.arrayPathModify = [];
          this.arrayPathClear = [];

          this.getErrorOrModifyAndMarkStatus();
        });
      }
    }
  }

  ngOnInit() {
    if (this.item) {
      this.item.is_master = true;
    }
    // Disable
    if (this.readonly) {
      this.form?.disable();
    }
  }

  get attributes() {
    return this.form && (this.form.get('attributes') as FormArray);
  }

  loadFormArray(firstLvel = true, i = 0, attributes = this.attributes, indexMultiple = 0, valuesBack = this.item?.values) {
    const labelAtts = this.item2?.attributes.filter(({ label_attribute }) => label_attribute);
    this.hasMultipleLabels = !!labelAtts && labelAtts.length > 1;

    if (attributes && this.item2?.attributes) {
      this.entityChipsSelectConfig = [...this.entityChipsSelectConfig, ...new Array(this.item2.attributes.length - i)];

      for (i; i < this.item2?.attributes?.length; i++) {
        const att = this.item2?.attributes[i];

        if (att.data_type === 'ENTITY') {
          att.configChips = this.fnSearchFactory(att.entity_reference || att.entity_view?.id || '', att.label, att.multiple_cardinality);
        }

        if (att.data_type === 'EMBEDDED' && att.multiple_cardinality) {
          attributes.push(
            this.fb.group({
              data: att,
              path: att.path,
              multiples: this.fb.array([]),
            })
          );
        } else if (att.data_type === 'EMBEDDED' && !att.multiple_cardinality) {
          attributes.push(
            this.fb.group({
              data: att,
              path: att.path,
              attributes: this.fb.array([]),
            })
          );
        } else {
          let value = att.data_type === 'BOOLEAN' ? false : null;
          if (valuesBack) {
            if (Array.isArray(valuesBack)) {
              if (valuesBack.length) {
                value = valuesBack[indexMultiple][att.path || ''] || valuesBack[indexMultiple][att.id || ''] || null;
              }
            } else {
              value = valuesBack[att.path || ''] || valuesBack[att.id || ''] || null;
            }
          }
          const formC = new FormControl(value);
          this.loadValidators(att, formC, firstLvel);
          attributes.push(
            this.fb.group({
              data: att,
              path: att.path,
              value: formC,
            })
          );
        }
      }
    }
  }

  public showTab(id: number, index: number) {
    this.tab = {
      [id]: { [index]: true },
    };
  }

  fnSearchFactory(id: string, label: string, multiple = false): ChipsControlSelectConfig {
    return {
      label,
      modalType: ModalChipsTypes.select,
      modalConfig: {
        multiple,
        searchFnOnInit: true,
        searchFn: (x: GenericApiRequest) => {
          return this.apiEntityService.listWithFilter(id, x);
        },
      },
    };
  }

  getDataToSend() {
    if (this.form && this.item2) {
      const attributesFormArray = this.form.controls.attributes as FormArray;
      // no usar los valores sino el control para mirar cual esta modificado
      // si el control no esta touched usar valores originales

      return this.parseAttributes(attributesFormArray);
    }
  }

  submit() {
    // TODO: comprobar valid y controlar error
    if (this.form && this.item2) {
      this.form.markAllAsTouched();
      const validatorsAttributes = [arrSomeValidator()];

      this.form.controls.attributes.setValidators(validatorsAttributes);

      this.form.updateValueAndValidity();
      if (this.form.valid) {
        const attributesValue = { ...this.getDataToSend() };
        this.strinfyAttributes(attributesValue);
        this.formSubmit.emit({
          ...this.item2,
          name: this.form.controls.name.value,
          attributes: attributesValue,
        });
      } else {
        this.getErrorOrModifyAndMarkStatus();
        if (this.form.controls.name.errors?.required) {
          this.toastService.error('SITES.FORM_ERROR.MESSAGE', 'SITES.FORM_ERROR.TITLE');
        } else if (this.form.controls.attributes.errors?.some) {
          this.toastService.error('ENTITY.ONE_SECTION_EMPTY');
        } else if (this.form.controls.attributes.errors?.someLabel) {
          this.toastService.error('ENTITY.ONE_LABEL_MIN');
        } else {
          this.toastService.commonFormError();
        }
      }
    }
  }

  clear() {
    this.form?.markAsPristine();
    this.attributes?.controls?.forEach((control) => control.markAsPristine());

    this.arrayPathError = [];
    this.arrayPathModify = [];
    this.arrayPathClear = [];
    this.getErrorOrModifyAndMarkStatus();
    this.change.markForCheck();
  }

  clickNode(
    event: ExplodedClickEvent<
      ExplodedTreeNodeComponent,
      AttributeEntity & { loadChildrens: (entity: AttributeEntity) => Observable<ExplodedTreeNode[]> }
    >
  ) {
    this.show = null;

    if (event.data.element_type !== ExplodedTreeElementType.EMBEDDED) {
      this.show = event.data.value?.path;
    } else if (!event.data.children?.length) {
      event.data.value?.loadChildrens(event.data.value).subscribe((attributes: ExplodedTreeNode[]) => {
        event.data.children = attributes;
      });
    }
  }

  badgeFilter(node: ExplodedTreeNodeComponent<AttributeEntity>) {
    if (this.arrayPathError.length && this.arrayPathError.find((path) => path.startsWith(node.data?.value?.path || ''))) {
      return 'error-light';
    }

    if (this.arrayPathModify.length && this.arrayPathModify.find((path) => path.startsWith(node.data?.value?.path || ''))) {
      return 'warning-light';
    }
  }

  private loadValidators(att: AttributeEntity, control: FormControl, firstLevel = false) {
    if (att.label_attribute && !this.hasMultipleLabels && firstLevel) {
      control.setValidators(Validators.required);
    } else if (att.data_type === 'DOUBLE' && !att.multiple_cardinality) {
      const maxLenghtSevenWithTwoDecRegEx = '^[0-9]{0,7}([.][0-9]{0,2})?$';
      control.setValidators(Validators.pattern(maxLenghtSevenWithTwoDecRegEx));
    } else if (att.data_type === 'STRING' && att.is_i18n && att.multiple_cardinality) {
      control.setValidators(fieldI18nMultipleValidator());
    } else if (att.data_type === 'STRING' && att.is_i18n && !att.multiple_cardinality) {
      control.setValidators(fieldI18nValidator());
    }
  }

  private getErrorOrModifyAndMarkStatus(controls = this.attributes?.controls) {
    for (const control of controls || []) {
      const valueControl = control.get('value');
      const multipleControl = control.get('multiples') as FormArray;
      const attributesEmbebed = control.get('attributes') as FormArray;

      if (multipleControl) {
        const controlsMultiples = multipleControl.controls;
        for (const multiple of controlsMultiples) {
          const attributes = multiple.get('attributes') as FormArray;
          this.getErrorOrModifyAndMarkStatus(attributes.controls);
        }
      }

      if (attributesEmbebed) {
        this.getErrorOrModifyAndMarkStatus(attributesEmbebed.controls);
      }

      if (
        valueControl &&
        valueControl.errors &&
        // Object.keys(valueControl.errors).length &&
        (valueControl.touched || !valueControl.pristine)
      ) {
        this.arrayPathError.push(control.value.path);
      }

      if (valueControl && !valueControl.pristine) {
        this.arrayPathModify.push(control.value.path);
      } else if (!this.arrayPathError.includes(control.value.path) && !this.arrayPathClear.includes(control.value.path)) {
        this.arrayPathClear.push(control.value.path);
      }
    }

    if (this.tree) {
      this.tree.update();
    }
  }

  private loadChildren(
    att: AttributeEntityType,
    formGroup = this.attributes,
    idMultiple = '',
    controlParent: any = null,
    values: any = this.original?.attributes
  ): ExplodedTreeNode {
    const loadChildrens =
      att.data_type === 'EMBEDDED'
        ? (x: any) => {
            this.show = null;
            return this.apiEntityTypeService.detail(x.entity_reference || x.entity_view.id || '').pipe(
              map((v) => {
                values = values ? values[x.id] || values : values;
                if (this.item2) {
                  v.attributes = v.attributes.map((value) => {
                    return {
                      ...value,
                      path: `${att.path}/${value.id}`,
                    };
                  });

                  const newIndice = this.item2.attributes?.length;
                  this.item2.attributes.push(...v.attributes);
                  const controls = formGroup?.controls;
                  const indexControl = controls?.findIndex((control) => {
                    return control.value?.data?.entity_view?.id === v.id || control.value?.data?.entity_reference === v.id;
                  });
                  if (indexControl !== undefined && indexControl >= 0) {
                    const tempControl = formGroup?.at(indexControl).get('multiples') as FormArray;

                    if (tempControl) {
                      if (values) {
                        const arrValues = values[att.id || ''];
                        const parentControls = controlParent?.controls as Array<FormGroup>;
                        // Tengo que saber el campo del parent
                        if (parentControls) {
                          parentControls?.forEach((arr: any, index) => {
                            const valuesMultiples = values[index] ? values[index][att.id || ''] || [{}] : [{}];
                            const controlA = arr.controls.attributes.controls[indexControl] as FormGroup;
                            const multipleA = controlA.get('multiples') as any;
                            valuesMultiples?.forEach((arrM: any, index2: number) => {
                              multipleA.push(
                                this.fb.group({
                                  attributes: this.fb.array([]),
                                })
                              );
                              this.loadFormArray(
                                false,
                                newIndice,
                                multipleA.at(index2).get('attributes') as FormArray,
                                index2,
                                valuesMultiples
                              );
                            });
                          });
                        } else {
                          if (Array.isArray(values)) {
                            const multipleValues = values;
                            multipleValues?.forEach((multpleValue, index) => {
                              tempControl.push(
                                this.fb.group({
                                  attributes: this.fb.array([]),
                                })
                              );
                              this.loadFormArray(
                                false,
                                newIndice,
                                tempControl.at(index).get('attributes') as FormArray,
                                index,
                                multpleValue[att.id || ''] || multpleValue
                              );
                            });
                          } else {
                            (arrValues || [{}]).forEach((arr: any[], index: number) => {
                              tempControl.push(
                                this.fb.group({
                                  attributes: this.fb.array([]),
                                })
                              );
                              this.loadFormArray(
                                false,
                                newIndice,
                                tempControl.at(index).get('attributes') as FormArray,
                                index,
                                Array.isArray(values) ? values[index][att.id || ''] || values[index] : [{}]
                              );
                            });
                          }
                        }
                      } else {
                        tempControl.push(
                          this.fb.group({
                            attributes: this.fb.array([]),
                          })
                        );
                        this.loadFormArray(false, newIndice, tempControl.at(0).get('attributes') as FormArray, values);
                      }
                      // Recorro controlParent FormArray menos la primera posicion para añadir los controles que no se conocian
                      if (controlParent) {
                        const controlsParents = controlParent.controls as Array<any>;
                        controlsParents?.forEach((control: any, index: number) => {
                          if (index > 0) {
                            const formGroupParent = control.controls.attributes.controls[indexControl] as FormGroup;
                            const multipleParent = formGroupParent?.controls.multiples as FormArray;
                            if (multipleParent.length === 0) {
                              // Añado los attributos
                              const attributosToCopy = tempControl.at(0) as FormGroup;
                              multipleParent.push(cloneAbstractControl(attributosToCopy));
                            }
                          }
                        });
                      }

                      return v.attributes.map((s) =>
                        this.loadChildren(s, tempControl.at(0).get('attributes') as FormArray, att.id, tempControl, values)
                      );
                    }

                    if (!tempControl && formGroup) {
                      const group = formGroup.at(indexControl).get('attributes') as FormArray;
                      // si padre es multiple , tengo que cargar los datos de los hijos
                      if (idMultiple && controlParent) {
                        // Recorrer controlParent y en cada control pillar attributes y la posicion indexContrl
                        controlParent?.controls?.forEach((element: any, index: number) => {
                          const formArray = element.controls.attributes;
                          const group2 = formArray.at(indexControl).get('attributes') as FormArray;
                          this.loadFormArray(false, newIndice, group2, index, values);
                        });
                      } else {
                        this.loadFormArray(false, newIndice, group, undefined, values);
                      }
                      return v.attributes.map((s) => this.loadChildren(s, group, undefined, null, values));
                    }
                  }
                }
                return v.attributes.map((s) => this.loadChildren(s, this.attributes, undefined, null, values));
              })
            );
          }
        : undefined;

    return {
      value: {
        ...att,
        name: att.name,
        path: att.path || att.id,
        loadChildrens,
      },
      name: att.name,
      element_type: ExplodedTreeElementType[att.data_type],
      icon: att.data_type === 'ENTITY' || att.data_type === 'EMBEDDED' ? 'icon-icon-atributos' : 'icon-icon-agrupaciones',
    };
  }

  private parseAttributes(attributesForm: FormArray, lvl = 0, attributesOriginal = this.original?.attributes): any {
    const field: { [key: string]: any } = {};
    const controls = attributesForm.controls as FormGroup[];
    controls?.forEach((control: FormGroup) => {
      const controlChild = control.controls;
      const valuePath = controlChild.path?.value.split('/')[lvl];

      const value: any = attributesOriginal ? attributesOriginal[valuePath] : null;
      if (controlChild?.attributes) {
        // si controls esta vacio sustituir todo el value
        const controlAttributes = controlChild.attributes as FormArray;
        if (controlAttributes.length) {
          const tempData = this.parseAttributes(controlAttributes, lvl + 1, value || []);
          field[valuePath] = {
            ...field[valuePath],
            ...tempData,
          };
        } else {
          // Este value debe ser el objeto de attributes que toque e irlo pasando y cortando el value path
          field[valuePath] = {
            ...field[valuePath],
            ...value,
          };
        }
      }

      if (controlChild?.multiples) {
        const controlMultipes = controlChild.multiples as FormArray;
        if (controlMultipes.controls.length) {
          const t = this.parseMultiple(controlChild.data.value, controlMultipes, lvl, value);
          field[valuePath] = t;
        } else {
          field[valuePath] = value;
        }
      }

      if (controlChild.value) {
        field[valuePath] = this.parseData(
          {
            data: controlChild.data.value,
            path: controlChild.path.value,
            value: controlChild.value.value,
          },
          {}
        );
      }
    });

    return field;
  }

  private parseMultiple(attribute: any, multiples: FormArray, lvl: number, value: any, lvlMultiple = 0) {
    const obj = {
      ...attribute.data,
      value: [],
    };
    multiples.controls?.forEach((multiple) => {
      const field: { [key: string]: any } = {};
      const multipleControl = multiple as FormGroup;
      const controls = multipleControl.controls.attributes as FormArray;
      controls.controls?.forEach((att: any) => {
        if (att.controls.value) {
          this.parseData(att.value, field);
        }

        if (att.controls?.multiples) {
          // Recursividad
          if (att.controls?.multiples.pristine) {
            field[att.value.data.id] = value ? value[lvlMultiple][att.value.data.id] : null;
          } else {
            const tempData = this.parseMultiple(att.value, att.controls?.multiples, lvl, value, lvlMultiple + 1);
            if (tempData && tempData.length) {
              field[att.value.data.id] = tempData;
            }
          }
        }

        if (att.controls.attributes) {
          field[att.value.data.id] = this.parseAttributes(att.controls?.attributes, lvl + 1, value);
        }
      });
      obj.value.push(field);
    });
    return obj.value;
  }

  private parseData(att: any, field: any) {
    if (!att.multiples && !att.attributes) {
      if (att.data.is_i18n) {
        if (att.value) {
          att.value = Object.keys(att.value).reduce((val: { [key: string]: string }, current: string) => {
            if (att.value[current] && !(att.value[current] instanceof Array)) {
              val[current] = att.value[current];
            } else if (att.value[current] instanceof Array && att.value[current].filter((v: string) => v).length) {
              val[current] = att.value[current].filter((value: string) => value);
            }
            return val;
          }, {});
        }
      }

      const tempValue =
        att.data.data_type === 'ENTITY'
          ? att.value instanceof Array && att.value.length
            ? att.value.map((value: any) => value.id)
            : att.value?.id
          : att.value;

      field[att.data.id] = att.value
        ? att.data?.data_type === 'DOUBLE' && !(att.value instanceof Array)
          ? +tempValue
          : tempValue
        : undefined;

      field[att.data.id] = att.data?.data_type === 'BOOLEAN' ? att.value || false : field[att.data.id];
      return field[att.data.id];
    }
  }

  private parseDataOriginal(): any {
    const keys = Object.keys(this.original?.attributes || {});
    const field: { [key: string]: any } = {};
    keys?.forEach((att: string) => {
      const value = this.original?.attributes[att as any];
      const tryParse = tryParseJSON(value);
      field[att] = tryParse || value;
    });

    return field;
  }

  private strinfyAttributes(attributesValue: any) {
    if (attributesValue) {
      const keys = Object.keys(attributesValue);
      keys?.forEach((k) => {
        const findTree = this.loadTree.find((v) => v.value.id === k && v.value.data_type === 'EMBEDDED');
        if (findTree) {
          if (findTree.value.multiple_cardinality) {
            attributesValue[k] = attributesValue[k]?.map((multiple: any) => JSON.stringify(multiple));
          } else {
            attributesValue[k] = JSON.stringify(attributesValue[k]);
          }
        }
      });
    }
  }
}

function clone<T>(x: T): T {
  return JSON.parse(JSON.stringify(x)) as T;
}

function tryParseJSON(jsonString: any) {
  try {
    const o = JSON.parse(jsonString);
    if (o && typeof o === 'object') {
      return o;
    }
    // eslint-disable-next-line no-empty
  } catch (e) {}

  return false;
}
