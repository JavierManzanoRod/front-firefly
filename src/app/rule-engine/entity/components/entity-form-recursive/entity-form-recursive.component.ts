import { ChangeDetectionStrategy, Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { UiModalCommonService } from 'src/app/modules/common-modal/services/ui-modal-common.service';
import { cloneAbstractControl } from '../../utils/clone-form.utils';

@Component({
  selector: 'ff-entity-form-recursive',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './entity-form-recursive.component.html',
  providers: [UiModalCommonService],
  styleUrls: ['./entity-form-recursive.component.scss'],
})
export class EntityFormRecursiveComponent implements OnInit {
  @Input() form: FormGroup | null = null;
  @Input() show = '';
  @Input() readonly = false;

  _controls: FormArray | null = null;
  get controls(): FormArray | null {
    return this._controls;
  }
  @Input() set controls(value: FormArray | null) {
    this._controls = value;
  }

  tabActive: any = null;

  constructor(private modal: UiModalCommonService, private ref: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (this.readonly) {
      this.form?.disable();
    }
  }

  addControl(formGroup: FormGroup) {
    const formArray = formGroup.get('multiples') as FormArray;
    const path = formGroup.value?.data?.path;
    const index = (formGroup.get('multiples') as FormGroup)?.controls.length;

    const first = formArray.at(0) as FormGroup;

    const control = cloneAbstractControl(first, true);
    formArray.push(control);

    // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
    this.clickTab(path + index);

    if (this.readonly) {
      formGroup.disable();
    }
  }

  isMultiple(formGroup: FormGroup): boolean {
    const multiple = formGroup.get('multiples') as FormArray;
    return formGroup.value?.data?.data_type === 'EMBEDDED' && multiple?.controls?.length > 0;
  }

  clickTab(id: string) {
    this.tabActive = {
      [id]: true,
    };
  }

  checkAddMore(id: string) {
    return this.show.indexOf(id) !== -1;
  }

  removeControl(form: FormGroup, index: number) {
    const formArray = form.get('multiples') as FormArray;
    const path = form.value?.data?.path;
    if (formArray.length > 1) {
      this.modal
        .showWarning({
          title: 'ENTITY_TYPES.MODAL_WARNING.TITLE',
          errorMessage: 'ENTITY_TYPES.MODAL_WARNING.ERRORMESSAGE',
          bodyMessage: 'ENTITY_TYPES.MODAL_WARNING.BODY',
          cancelButtonText: 'ENTITY_TYPES.MODAL_WARNING.CANCEL',
          name: `${form.value?.data?.name.toUpperCase()} Valor: ${index}`,
          okButtonText: 'ENTITY_TYPES.MODAL_WARNING.CONFIRM',
        })
        .subscribe((confirm: boolean) => {
          if (confirm) {
            formArray.removeAt(index);
            this.clickTab(`${path}0`);
            this.ref.detectChanges();
          }
        });
    }
  }
}
