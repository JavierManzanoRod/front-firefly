import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BsModalService } from 'ngx-bootstrap/modal';
import { UiModalCommonService } from 'src/app/modules/common-modal/services/ui-modal-common.service';
import { EntityFormRecursiveComponent } from './entity-form-recursive.component';

describe('EntityFormRecursiveComponent', () => {
  let component: EntityFormRecursiveComponent;
  let fixture: ComponentFixture<EntityFormRecursiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EntityFormRecursiveComponent],
      providers: [
        {
          provide: UiModalCommonService,
          useFactory: () => {},
        },
        {
          provide: BsModalService,
          useFactory: () => {},
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityFormRecursiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
