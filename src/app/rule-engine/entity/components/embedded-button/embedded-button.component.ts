import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { AttributeEntityType } from '../../../../core/models/entity-type.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { EmbeddedModalComponent } from '../embedded-modal/embedded-modal.component';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';

@Component({
  selector: 'ff-embedded-button',
  templateUrl: './embedded-button.component.html',
  styleUrls: ['./embedded-button.component.scss'],
})
export class EmbededdButttonComponent implements OnInit {
  @HostBinding('class.loading') loading = false;
  @Input() field!: AttributeEntityType;
  @Input() value!: string;

  constructor(private modal: BsModalService, private entityTypeService: EntityTypeService) {}

  ngOnInit(): void {}

  openJSONModal() {
    this.loading = true;

    if (this.field.entity_reference) {
      this.entityTypeService.detail(this.field.entity_reference).subscribe((type: any) => {
        const initialState = { type, json: this.value };
        this.modal.show(EmbeddedModalComponent, {
          initialState,
          id: new Date().getTime(),
        });
        this.loading = false;
      });
    }
  }
}
