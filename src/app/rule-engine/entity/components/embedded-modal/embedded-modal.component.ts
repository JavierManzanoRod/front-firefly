import { Component, OnInit } from '@angular/core';
import { EntityType } from '@core/models/entity-type.model';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AttributeEntityType } from '../../../../core/models/entity-type.model';
import { ExplodedTreeElementType, ExplodedTreeNode, ExplodedClickEvent } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ff-embedded-modal',
  templateUrl: './embedded-modal.component.html',
  styleUrls: ['./embedded-modal.component.scss'],
})
export class EmbeddedModalComponent implements OnInit {
  type!: EntityType;
  json!: string;
  tree!: ExplodedTreeNode[];

  data: {
    attribute: AttributeEntityType;
    value: string;
  }[] = [];

  constructor(private entityTypeService: EntityTypeService, public modalRef: BsModalRef, private translate: TranslateService) {}

  ngOnInit(): void {
    let data: { [key: string]: any } = {};

    if (this.json) {
      data = JSON.parse(this.json);
    }

    this.tree = this.parseAttr(this.type, data);

    this.type.attributes.forEach((attribute) => {
      this.data.push({
        attribute,
        value: (data && data[attribute.id || '']) || '',
      });
    });
  }

  parseAttr(type: EntityType, value: { [key: string]: any }) {
    const tree: ExplodedTreeNode[] = [];

    if (type.attributes) {
      type.attributes.forEach(({ entity_type_reference_id, is_i18n, ...attr }) => {
        tree.push({
          name: attr.label,
          element_type: attr.data_type as ExplodedTreeElementType,
          data: { ...value, entity_type_reference_id, is_i18n },
          id: attr.id,
        });
      });
    }

    return tree;
  }

  expand({ data, target }: ExplodedClickEvent<ExplodedTreeNodeComponent>) {
    const info = data && data.data;
    const value = info && info[data.id || ''];
    const is_i18n = info?.is_i18n;

    if (!target.data.children || !target.data.children?.length) {
      if (data.element_type === ExplodedTreeElementType.EMBEDDED) {
        if (data.data) {
          target.setLoading(true);

          this.entityTypeService.detail(data.data.entity_type_reference_id).subscribe((type: any) => {
            const children = this.parseAttr(type, data.data && data.data[data.id || '']);

            target.data.children = children;

            target.setLoading(false);
            target.expand();
          });
        }
      } else if (value) {
        if (!target.data.children) {
          target.data.children = [];
        }

        if (value instanceof Array) {
          value.forEach((val) => target.data.children?.push({ name: val }));
        } else if (is_i18n) {
          target.data.children.push({
            name: value.es_ES,
            i18n: true,
            children: Object.keys(value).reduce((prev, current) => {
              prev.push({
                name: `<strong>${this.translate.instant('LANGUAGES.' + current)}</strong>: ${value[current]}`,
              });

              return prev;
            }, [] as ExplodedTreeNode[]),
          });
        } else {
          target.data.children.push({ name: value });
        }

        target.expand();
      } else if (data.element_type) {
        target.setWarning('ENTITY_TYPE_DETAIL.ERROR_NOT_VALUE');
      }
    }
  }

  iconFilter(node: ExplodedTreeNodeComponent) {
    if (node.data.element_type) {
      return 'icon-atributos';
    }

    if (node.data.i18n) {
      return 'icon-icon-language';
    }

    return 'icon-agrupaciones';
  }

  clickFilter(node: ExplodedTreeNodeComponent) {
    if (node.data.element_type) {
      return true;
    }

    return false;
  }
}
