import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Injector, Input } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ChipsControlSelectConfig } from 'src/app/modules/chips-control/components/chips.control.model';
import { AttributeEntity } from '../../models/entity.model';
import { EntityService } from '../../services/entity.service';

@Component({
  selector: 'ff-entity-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './entity-form.component.html',
  styleUrls: ['./entity-form.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EntityFormComponent),
      multi: true,
    },
  ],
})
export class EntityFormComponent implements ControlValueAccessor, AfterViewInit {
  @Input() config: AttributeEntity | null = null;
  @Input() configChips: ChipsControlSelectConfig | null = null;
  @Input() index = 0;
  disabled = false;
  control: FormControl | null = null;
  value: any = '';

  get entityLoaded(): boolean {
    if (this.config?.data_type === 'ENTITY' && this.config?.multiple_cardinality && this.control && this.control.value instanceof Array) {
      return !this.control.value.reduce((old, current) => {
        if (typeof current === 'string') {
          // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
          return old + 1;
        }
        return old;
      }, 0);
    }

    return true;
  }

  constructor(private injector: Injector, private ref: ChangeDetectorRef, private entityService: EntityService) {}

  writeValue(value: any) {
    this.value = value;
  }

  ngAfterViewInit(): void {
    const ngControl: NgControl | null = this.injector.get(NgControl, null);
    if (ngControl) {
      this.control = ngControl?.control as FormControl;
      this.ref.detectChanges();
    }

    if (this.config?.data_type === 'ENTITY' && this.config?.multiple_cardinality && this.control && this.control.value instanceof Array) {
      this.control.value.forEach((value, index) => {
        if (typeof value === 'string') {
          this.entityService.detail(value).subscribe((entity) => {
            const entities = this.control?.value;
            entities[index] = entity;

            // this.control?.setValue(entities, { emitEvent: false });
            // this.writeValue(entities);
            this.ref.markForCheck();
          });
        }
      });
    }
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  onBlur() {
    if (!this.control?.touched) {
      this.onTouched();
    }
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  changeValue() {
    this.onTouched();
    this.onChange(this.value);
  }

  changeValueI18n() {
    if (this.value) {
      const values: { [key: string]: string } = {};
      const keys = Object.keys(this.value);
      keys.forEach((k) => {
        values[k] = this.value[k] || '';
      });

      this.value = values;
    }
    this.changeValue();
  }

  changeValueI18nMultiple() {
    if (this.value) {
      const values: { [key: string]: string[] } = {};
      const keys = Object.keys(this.value);
      keys.forEach((k) => {
        values[k] = this.value[k].map((value: string) => value || '');
      });

      this.value = values;
    }
    this.changeValue();
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange(data: any) {
    // don't delete!
  }

  onTouched() {
    // don't delete!
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }
}
