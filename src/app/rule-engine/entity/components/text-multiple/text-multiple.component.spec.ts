import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TextMultipleComponent } from './text-multiple.component';

describe('TextMultipleComponent', () => {
  let component: TextMultipleComponent;
  let fixture: ComponentFixture<TextMultipleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TextMultipleComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
