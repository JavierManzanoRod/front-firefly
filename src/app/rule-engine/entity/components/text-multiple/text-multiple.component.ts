import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../modules/chips-control/components/chips.control.model';

@Component({
  selector: 'ff-text-multiple',
  templateUrl: 'text-multiple.component.html',
  styleUrls: ['./text-multiple.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextMultipleComponent),
      multi: true,
    },
  ],
})
export class TextMultipleComponent implements OnInit, ControlValueAccessor {
  @Input() label = '';
  disabled = false;

  value: string[] = [];
  chipsSelectConfig: ChipsControlSelectConfig = {
    modalType: ModalChipsTypes.free,
    modalConfig: {
      title: 'FIELD_TEXT_MULTIPLE.MODAL_TITLE',
    },
  };

  constructor() {}

  ngOnInit() {
    this.chipsSelectConfig.label = this.label;
  }

  writeValue(data: string[]): void {
    this.value = data;
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  // Allows Angular to register a function to call when the input has been touched.
  // Save the function as a property to call later here.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  registerOnTouched(fn: () => void): void {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange(data: any) {
    // don't delete!
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }
}
