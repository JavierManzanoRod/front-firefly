import { AbstractControl } from '@angular/forms';
import { multipleNumbers } from './multiple-numbers.validator';

describe('multipleNumbers', () => {
  it('multipleNumbers returns true is some is true', () => {
    const validatorToTest = multipleNumbers();
    const mockControl = { value: [1, 44, 33].join(', ') } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeNull();
  });

  it('multipleNumbers  returns false if every is false', () => {
    const validatorToTest = multipleNumbers();
    const mockControl = { value: ['aaa', 1].join(', ') } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeTruthy();
  });

  it('multipleNumbers  returns true when null is provided', () => {
    const validatorToTest = multipleNumbers();
    const mockControl = { value: null } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeNull();
  });
});
