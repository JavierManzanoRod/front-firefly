import { AbstractControl, ValidatorFn } from '@angular/forms';

const isNumber = (value: string) => {
  const num = Number(value);
  return typeof num === 'number' && isFinite(num);
};

export function multipleNumbers(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const val = control.value;
    if (val == null) {
      return null;
    } else {
      const valid = split(val)?.every(isNumber);
      if (valid) {
        return null;
      } else {
        return { invalidNumber: true };
      }
    }
  };
}

function split(x: string, fn: (x: string) => any = (e) => e): string[] | null {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return x && x.length
    ? x
        .split(',')
        .map((e) => e.trim())
        .filter((e) => e.length)
        .map(fn)
    : [];
}
