import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { ExplodedTreeModule } from 'src/app/modules/exploded-tree/exploded-tree.module';
import { FieldI18nModule } from 'src/app/modules/field-i18n/field-i18n.module';
import { SelectModule } from 'src/app/modules/select/select.module';
import { UiTreeListModule } from 'src/app/modules/ui-tree-list/ui-tree-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChipsControlModule } from '../../modules/chips-control/chips-control.module';
import { TableWithFixedElementsModule } from '../../modules/table-with-fixed-elements/table-with-fixed-elements.module';
import { EntityDetailComponent } from './components/entity-detail/entity-detail.component';
import { EntityFormRecursiveComponent } from './components/entity-form-recursive/entity-form-recursive.component';
import { EntityFormComponent } from './components/entity-form/entity-form.component';
import { EntityListComponent } from './components/entity-list/entity-list.component';
import { NumberMultipleComponent } from './components/number-multiple/number-multiple.component';
import { TextMultipleComponent } from './components/text-multiple/text-multiple.component';
import { EntityDetailContainerComponent } from './containers/entity-detail-container.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { EmbeddedModalComponent } from './components/embedded-modal/embedded-modal.component';
import { EmbededdButttonComponent } from './components/embedded-button/embedded-button.component';
import { SearchSimpleModule } from 'src/app/modules/search-simple/search-simple.module';
import { Pagination2Module } from 'src/app/modules/pagination-2/pagination-2.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Pagination2Module,
    TypeaheadModule.forRoot(),
    AccordionModule.forRoot(),
    CommonModalModule,
    SelectModule,
    DatetimepickerModule,
    SearchSimpleModule,
    FieldI18nModule,
    SimplebarAngularModule,
    TableWithFixedElementsModule,
    TranslateModule,
    ChipsControlModule,
    UiTreeListModule,
    TooltipModule,
    ModalModule,
    ExplodedTreeModule,
  ],
  declarations: [
    EntityDetailContainerComponent,
    EntityListComponent,
    EntityDetailComponent,
    TextMultipleComponent,
    NumberMultipleComponent,
    EntityFormComponent,
    EntityFormRecursiveComponent,
    EmbededdButttonComponent,
    EmbeddedModalComponent,
  ],
  providers: [],
  exports: [EntityListComponent, EntityDetailComponent],
})
export class EntityModule {}
