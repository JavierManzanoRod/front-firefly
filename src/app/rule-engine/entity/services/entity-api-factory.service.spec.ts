import { EntityType } from '@core/models/entity-type.model';
import { GenericApiResponse } from '@model/base-api.model';
import { of } from 'rxjs';
import { Entity, EntityRepository } from '../models/entity.model';
import { EntityApiFactoryService } from './entity-api-factory.service';
import { EntityService } from './entity.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };
let entityServiceSpy: { get: jasmine.Spy; update: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy; detail: jasmine.Spy };

const expectedData: Entity = {
  id: '1',
  name: 'name',
  attributes: [
    {
      id: 'idAtt1',
      data_type: 'STRING',
    },
  ] as any,
} as Entity;
const expectedList: GenericApiResponse<Entity> = {
  content: [expectedData],
  page: null as any,
};

const version = 'v1/';

const entityType = {
  id: '1',
  name: 'name',
  attributes: [
    {
      id: 'idAtt1',
      data_type: 'STRING',
    },
  ],
} as EntityType;

xdescribe('EntityApiFactoryService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it('from creates a Entity from a EntityRepository', () => {
    const myService = new EntityApiFactoryService(null as any, (null as unknown) as EntityService, version);
    const entityRepository = {
      id: '1',
      entity_type_id: 'idEntityType',
      attributes: {
        idAtt1: 'someVal',
      },
    } as EntityRepository;
    const entity = myService.from(entityType, entityRepository);
    expect(entity.attributes[0].id).toEqual(entityType.attributes[0].id);
  });

  it('to creates a EntityRepository from a Entity', () => {
    const myService = new EntityApiFactoryService(null as any, null as any, version);
    const entity = {
      id: '1',
      entity_type_id: 'idEntityType',
      attributes: [
        {
          id: 'someId',
          data_type: 'STRING',
          value: 'someVal',
        },
      ],
    } as Entity;
    const entityRepository = myService.to(entity);
    expect(entityRepository.attributes.someId).toEqual(entity.attributes[0].value);
  });

  it('create creates an EntityApi', () => {
    const myService = new EntityApiFactoryService(null as any, null as any, version);
    const entityApi = myService.create(entityType);
    expect(entityApi.list).toBeDefined();
    expect(entityApi.post).toBeDefined();
    expect(entityApi.delete).toBeDefined();
    expect(entityApi.delete).toBeDefined();
  });

  it('generated crud api delete points to delete api repository', () => {
    entityServiceSpy = jasmine.createSpyObj('entityService', ['delete']);
    entityServiceSpy.delete.and.returnValue(of(null));
    const myService = new EntityApiFactoryService(null as any, null as any, version);
    myService.entityService = ({ ...entityServiceSpy } as unknown) as EntityService;
    const entityApi = myService.create(entityType);
    entityApi.delete(('id' as unknown) as Entity).subscribe();
    expect(entityServiceSpy.delete.calls.count()).toBe(1);
  });

  it('generated crud api detail points to detail api repository', () => {
    entityServiceSpy = jasmine.createSpyObj('entityService', ['detail']);
    entityServiceSpy.detail.and.returnValue(of(null as any));
    const myService = new EntityApiFactoryService(null as any, null as any, version);

    myService.entityService = ({ ...entityServiceSpy } as unknown) as EntityService;
    myService.from = (e) => null as any;
    const entityApi = myService.create(entityType);
    entityApi.detail({ id: '111' } as Entity).subscribe();
    expect(entityServiceSpy.detail.calls.count()).toBe(1);
  });

  it('generated crud api update points to detail api repository', () => {
    entityServiceSpy = jasmine.createSpyObj('entityService', ['update']);
    entityServiceSpy.update.and.returnValue(of(null));
    const myService = new EntityApiFactoryService(null as any, null as any, version);
    myService.to = (e) => null as any;
    myService.entityService = ({ ...entityServiceSpy } as unknown) as EntityService;
    const entityApi = myService.create(entityType);
    entityApi.update({ id: '111' } as Entity).subscribe();
    expect(entityServiceSpy.update.calls.count()).toBe(1);
  });

  it('generated crud api post points to post api repository', () => {
    entityServiceSpy = jasmine.createSpyObj('entityService', ['post']);
    entityServiceSpy.post.and.returnValue(of(null));
    const myService = new EntityApiFactoryService(null as any, null as any, version);
    myService.to = (e) => null as any;
    myService.entityService = ({ ...entityServiceSpy } as unknown) as EntityService;
    const entityApi = myService.create(entityType);
    entityApi.post({ id: '111' } as Entity).subscribe();
    expect(entityServiceSpy.post.calls.count()).toBe(1);
  });
});
