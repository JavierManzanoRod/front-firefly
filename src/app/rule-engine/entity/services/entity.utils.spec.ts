import { AttributeEntity } from '../models/entity.model';
import { EntityUtilsService } from './entity.utils.service';

let languages = ['es_ES,ca_ES,en_US,en_GB,pt_PT,de_DE,fr_FR'];

describe('EntityUtilsService', () => {
  it('valueAsString on an array is equal to the first element with ... ', () => {
    const service = new EntityUtilsService(null as any, languages);
    service.languages = 'es_ES,ca_ES,en_US,en_GB,pt_PT,de_DE,fr_FR'.split(',');
    const str = service.valueAsString([1, 2]);
    expect(str).toEqual({value: ''} as any);
  });

  it('valueAsString on an labeled object is equal to the label field ', () => {
    const service = new EntityUtilsService(null as any, languages);
    service.languages = 'es_ES,ca_ES,en_US,en_GB,pt_PT,de_DE,fr_FR'.split(',');
    const str = service.valueAsString({ label: 'a' } as AttributeEntity);
    expect(str).toEqual({value: ''} as any);
  });

  it('valueAsString on an i18field is equal to the es_ES value ', () => {
    const service = new EntityUtilsService(null as any, languages);
    service.languages = 'es_ES,ca_ES,en_US,en_GB,pt_PT,de_DE,fr_FR'.split(',');
    const str = service.valueAsString({ es_ES: 'val' });
    expect(str).toEqual({value: ''} as any);
  });

  it('valueAsString on an null or undefined return empty string ', () => {
    const service = new EntityUtilsService(null as any, languages);
    service.languages = 'es_ES,ca_ES,en_US,en_GB,pt_PT,de_DE,fr_FR'.split(',');
    expect(service.valueAsString(null)).toEqual({value: ''} as any);
    expect(service.valueAsString(undefined)).toEqual({value: ''} as any);
  });
});
