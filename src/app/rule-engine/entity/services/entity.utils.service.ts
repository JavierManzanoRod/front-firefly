/* eslint-disable @typescript-eslint/restrict-plus-operands */
import { Inject, Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FF_LANGUAGES } from 'src/app/configuration/tokens/language.token';
import { AttributeEntity } from '../models/entity.model';

@Injectable({
  providedIn: 'root',
})
export class EntityUtilsService {
  maxItemsToShowPerRow = 9999;
  maxItemsToShowPerCol = 3;

  constructor(private translateService: TranslateService, @Inject(FF_LANGUAGES) public languages: string[]) {}

  i18nName(key: string) {
    return this.translateService.instant('LANGUAGES.' + key);
  }

  valueAsString(attribute: any): { value: string; tooltip?: string } {
    if (!attribute || !attribute.value) {
      return {
        value: '',
      };
    } else if (attribute.multiple_cardinality && attribute.is_i18n) {
      if (typeof attribute.value === 'object') {
        return {
          value: this.getStringOfAttributeMultipleAndI18N(attribute, this.maxItemsToShowPerRow, this.maxItemsToShowPerCol),
          tooltip: this.getStringOfAttributeMultipleAndI18N(attribute, 9999, 9999),
        };
      }
    } else if (attribute.is_i18n) {
      if (typeof attribute.value === 'object') {
        return {
          value: this.getStringOfAttributeI18N(attribute, this.maxItemsToShowPerRow),
          tooltip: this.getStringOfAttributeI18N(attribute, 9999),
        };
      }
    } else if (attribute.multiple_cardinality) {
      if (Array.isArray(attribute.value)) {
        return {
          value: this.getStringOfAttributeMultiple(attribute, this.maxItemsToShowPerRow),
          tooltip: this.getStringOfAttributeMultiple(attribute, 9999),
        };
      }
    }

    if (typeof attribute.value === 'object') {
      return {
        value: attribute.value.name,
      };
    }

    return {
      value: attribute.value.toString(),
    };
  }
  private getStringOfAttributeMultipleAndI18N(attribute: any, maxPerRow: number, maxPerCol: number) {
    let result = '';
    let i = 0;
    if (attribute.value) {
      const l = Object.keys(attribute.value).length;
      for (const key in attribute.value) {
        if (Object.prototype.hasOwnProperty.call(attribute.value, key)) {
          if (i < maxPerRow) {
            result +=
              '<b>' +
              this.i18nName(key) +
              '</b>: ' +
              attribute.value[key].reduce((acc: string, value: string, index: number) => {
                if (index < maxPerCol - 1) {
                  acc += value + ', ';
                }
                if (index === maxPerCol - 1 && attribute.value[key].length > maxPerCol - 1) {
                  acc += value + '...';
                }
                return acc;
              }, '');
            result += '<br/>';
            i++;
          } else if (l > i) {
            result += '...';
            break;
          }
        }
      }
    }
    return result;
  }

  private getStringOfAttributeI18N(attribute: AttributeEntity, maxPerRow: number) {
    let result = '';
    let i = 0;
    if (attribute.value) {
      const l = Object.keys(attribute.value).length;
      for (const key in attribute.value) {
        if (Object.prototype.hasOwnProperty.call(attribute.value, key)) {
          if (i < maxPerRow) {
            result += `<b>${this.i18nName(key)}</b>: ${attribute.value[key]} <br/>`;
            i++;
          } else if (l > i) {
            result += '...';
            break;
          }
        }
      }
    }
    return result;
  }

  private getStringOfAttributeMultiple(attribute: AttributeEntity, maxPerRow: number) {
    let result = '';
    for (let i = 0, l = attribute.value.length; i < l; i++) {
      if (i < maxPerRow) {
        result += `${typeof attribute.value[i] === 'object' ? attribute.value[i].name : attribute.value[i]} <br/>`;
      } else if (l > i) {
        result += '...';
        break;
      }
    }
    return result;
  }
}
