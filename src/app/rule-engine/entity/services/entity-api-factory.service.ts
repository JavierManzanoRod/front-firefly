import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { IApiService2 } from '@core/base/api.service';
import { EntityType } from '@core/models/entity-type.model';
import { environment } from '@env/environment';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_ENTITIES } from 'src/app/configuration/tokens/api-versions.token';
import { EntityRepositoryDTO } from '../models/entity.dto';
import { AttributeEntity, Entity, EntityRepository, EntityResponse, I18nValue, ValueAttEntityRepository } from '../models/entity.model';
import { EntityRepositoryInMapper } from './dto/entity-repository/entity-repository-in-mapper.class';
import { EntityService } from './entity.service';

const urlBase = `${environment.API_URL_BACKOFFICE}`;

@Injectable({
  providedIn: 'root',
})
export class EntityApiFactoryService {
  constructor(
    public http: HttpClient,
    public entityService: EntityService,
    @Inject(FF_API_PATH_VERSION_ENTITIES) private version: string
  ) {}

  from(entityType: EntityType, entityRepo: EntityRepository): Entity {
    const resolvedEntity = {
      id: entityRepo.id,
      name: entityRepo.name,
      entity_type_id: entityType.id,
      entity_type_name: entityType.name,
      is_master: entityType.is_master,
      attributes: [],
      values: entityRepo.values,
    } as Entity;

    for (const att of entityType.attributes) {
      let valToSet: ValueAttEntityRepository | I18nValue | undefined;
      if (att.id) {
        valToSet = entityRepo.attributes && entityRepo.attributes[att.id];
      }

      const attEntityResolved: AttributeEntity = {
        data_type: att.data_type,
        id: att.id,
        label: att.label,
        name: att.name,
        is_i18n: att.is_i18n,
        label_attribute: att.label_attribute,
        multiple_cardinality: att.multiple_cardinality,
        value: valToSet,
        entity_reference: att?.entity_view?.id || att.entity_type_reference_id,
        entity_reference_name: att?.entity_view?.name || att.entity_reference_name,
      };
      resolvedEntity.attributes.push(attEntityResolved);
    }
    return resolvedEntity;
  }

  to(entity: Entity): EntityRepository {
    const accumulator = {} as EntityRepository['attributes'];
    const entityRepo: EntityRepository = {
      id: entity.id,
      entity_type_id: entity.entity_type_id,
      name: entity.name,
      attributes: accumulator,
    };

    for (const att of entity.attributes) {
      if (att.id) {
        if (att.is_i18n && att?.value) {
          // string and i18n. COERCING
          const i18Value = att.value as I18nValue;
          let parsedI18Value;
          if (att.multiple_cardinality) {
            parsedI18Value = this.parseI18NValueMulti(i18Value);
          } else {
            parsedI18Value = this.parseI18NValue(i18Value);
          }
          accumulator[att.id] = parsedI18Value || '';
        } else if (att.data_type === 'ENTITY') {
          if (att?.value) {
            if (att.multiple_cardinality) {
              accumulator[att.id] = (att.value as EntityRepository[]).map(({ id }) => id);
            } else {
              accumulator[att.id] = (att.value as EntityRepository).id;
            }
          }
        } else if (att.data_type === 'BOOLEAN') {
          accumulator[att.id] = !!att.value;
        } else if (att.data_type === 'STRING') {
          if (att.value !== null && att.value !== '') {
            accumulator[att.id] = att.value;
          }
          // string The value should a date with format [uuuu-MM-dd'T'HH:mm'Z']
        } else if (att.data_type === 'DOUBLE') {
          if (att.value !== null && att.value !== '') {
            if (Array.isArray(att.value)) {
              accumulator[att.id] = att.value;
            } else {
              accumulator[att.id] = Number(att.value);
            }
          }
        } else if (att.data_type === 'DATE_TIME') {
          if (att.value !== null && att.value !== '' && att.value) {
            accumulator[att.id] = new Date(att.value as string);
          }
          // The value should a date with format [uuuu-MM-dd'T'HH:mm'Z']
        }
      }
    }

    return entityRepo;
  }

  parseI18NValue(i18nVal: I18nValue): I18nValue | null {
    const languages = Object.keys(i18nVal);
    const result = {} as I18nValue;
    let empty = true;
    for (const language of languages) {
      const val = i18nVal[language];
      if (val !== null) {
        result[language] = val;
        empty = false;
      }
    }
    return empty ? null : result;
  }

  parseI18NValueMulti(i18nVal: I18nValue): I18nValue | null {
    const languages = Object.keys(i18nVal);
    const result = {} as I18nValue;
    let empty = true;
    for (const language of languages) {
      const val = (i18nVal[language] as (string | null)[]) || [];
      const trimmedVal = val.filter((e) => e !== null);
      if (trimmedVal.length) {
        result[language] = trimmedVal;
        empty = false;
      }
    }
    return empty ? null : result;
  }

  create(entityType: EntityType | string): IApiService2<Entity> {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this;
    const entityService = this.entityService;
    const version = this.version;
    const http = this.http;
    let entityTypeRef: EntityType;
    if (typeof entityType === 'string') {
      entityTypeRef = { id: entityType } as EntityType;
    } else {
      entityTypeRef = entityType;
    }

    class EntityDedicatedService implements IApiService2<Entity> {
      endPoint = '';

      getParams(filter: GenericApiRequest): HttpParams {
        let params: HttpParams = new HttpParams();
        if (filter) {
          params = filter.page_number ? params.set('page_number', filter.page_number.toString()) : params;
          params = filter.page_size ? params.set('page_size', filter.page_size.toString()) : params;
          params = filter.size ? params.set('size', filter.size.toString()) : params;
          params = filter.page ? params.set('page', filter.page.toString()) : params;
          params = filter.name ? params.set('name', filter.name.toString()) : params;
          params = filter.department_id ? params.set('department_id', filter.department_id.toString()) : params;
        }
        return params;
      }
      public detail(x: string): Observable<any> {
        return entityService.detail(x).pipe(
          map((entityRepo) => {
            entityRepo.values = this.parseAttributeDetail(entityRepo.attributes);
            return {
              original: entityRepo,
              parse: that.from(entityTypeRef, entityRepo),
            };
          })
        );
      }

      public update(x: Entity): Observable<any> {
        const dataToSend = that.to(x);
        return entityService.update(dataToSend);
      }

      public delete(x: any): Observable<any> {
        return entityService.delete(x);
      }

      public post(x: Entity): Observable<any> {
        const dataToSend = that.to(x);
        return entityService.post(dataToSend);
      }

      public list(filter: any, expanded = true): Observable<EntityResponse> {
        const url = `${urlBase}/products/backoffice-entities/${version}entities`;
        return http
          .get<GenericApiResponse<EntityRepository>>(url, {
            params: this.getParams(filter).set('entity_type_id', entityTypeRef.id || ''),
          })
          .pipe(
            map(({ content, page }) => ({
              page,
              content: content.map((c) => new EntityRepositoryInMapper(c as EntityRepositoryDTO).data),
            })),
            map(({ content, page }) => {
              return {
                content: expanded
                  ? content.map((entityRepo) => that.from(entityTypeRef, entityRepo))
                  : content.map((entityRepo) => {
                      return {
                        id: entityRepo.id,
                        name: entityRepo.name,
                      } as Entity;
                    }),
                page,
              };
            })
          );
      }

      parseAttributeDetail(attributes: { [key: string]: any }, path = ''): { [key: string]: any } {
        let arrValues: { [key: string]: any } = {};
        const keys = Object.keys(attributes);
        for (const key of keys) {
          const id = path ? path + '/' + key : key;
          const value = attributes[key];
          if (value) {
            const tryParse = this.tryParseJSON(value);
            if (Array.isArray(value)) {
              let arr: any = [];
              value.forEach((v) => {
                const parse = this.tryParseJSON(v);
                if (parse) {
                  arr.push(this.parseAttributeDetail(parse, id));
                } else {
                  arr = value;
                }
              });
              arrValues[id] = arr;
            } else if (tryParse) {
              arrValues = {
                ...arrValues,
                ...this.parseAttributeDetail(tryParse, id),
              };
            } else if (typeof value === 'object' && value !== null && !value.es_ES && !value.entity_type_id) {
              arrValues = {
                ...arrValues,
                ...this.parseAttributeDetail(value, id),
              };
            } else {
              arrValues[id] = value;
            }
          }
        }
        return arrValues;
      }

      private tryParseJSON(jsonString: any) {
        try {
          const o = JSON.parse(jsonString);
          if (o && typeof o === 'object') {
            return o;
          }
          // eslint-disable-next-line no-empty
        } catch (e) {}

        return false;
      }
    }

    return new EntityDedicatedService();
  }

  searchFn(entityTypeId: string): (x: string) => Observable<GenericApiResponse<Entity>> {
    const clazz = this.create(entityTypeId);
    return (textToSearch) => {
      return clazz.list(textToSearch, false);
    };
  }
}
