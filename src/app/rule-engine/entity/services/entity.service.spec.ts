import { HttpClient } from '@angular/common/http';
import { GenericApiResponse, Page } from '@model/base-api.model';
import { EntityType } from '@core/models/entity-type.model';
import { of } from 'rxjs';
import { EntityRepository } from '../models/entity.model';
import { EntityService } from './entity.service';
import { EntityUtilsService } from './entity.utils.service';
import { AttributeOriginal, EntityRepositoryDTO } from '../models/entity.dto';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: EntityService;

const version = 'v1/';

const expectedData: EntityRepositoryDTO = ({
  identifier: '1',
  name: 'name',
  type: 'type',
  entity_type_id: 'a',
  attributes: { algo: undefined } as AttributeOriginal,
} as unknown) as EntityRepository;
const expectedList: GenericApiResponse<EntityRepository> = {
  content: [(expectedData as unknown) as EntityRepository],
  page: (null as unknown) as Page,
};

describe('EntityService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`EntityService points to products/backoffice-entities/${version}entities`, () => {
    myService = new EntityService((null as unknown) as HttpClient, {} as EntityUtilsService, version);
    expect(myService.endPoint).toEqual(`products/backoffice-entities/${version}entities`);
  });

  it('EntityService.list calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new EntityService(httpClientSpy as any, {} as EntityUtilsService, version);
    myService.list((expectedList as unknown) as EntityType).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });

  it('EntityService.search calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new EntityService(httpClientSpy as any, {} as EntityUtilsService, version);
    myService.search(('a' as unknown) as EntityType).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      expect(response.length).toBe(expectedList.content.length);
      done();
    });
  });

  it('EntityService.get handle detail and return one element', () => {
    httpClientSpy.get.and.returnValue(of(expectedData));
    myService = new EntityService(httpClientSpy as any, {} as EntityUtilsService, version);
    myService.detail(null as any).subscribe((response) => {
      expect(response.id).toEqual(expectedData.identifier, 'expected data');
    });
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
