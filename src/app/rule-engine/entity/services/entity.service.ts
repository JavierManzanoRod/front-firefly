import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, IApiService2, UpdateResponse } from '@core/base/api.service';
import { AuditQuery } from '@core/models/audit.model';
import { EntityType } from '@core/models/entity-type.model';
import { environment } from '@env/environment';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_ENTITIES } from 'src/app/configuration/tokens/api-versions.token';
import { EntityRepository } from '../models/entity.model';
import { EntityRepositoryInMapper } from './dto/entity-repository/entity-repository-in-mapper.class';
import { EntityRepositoryOutMapper } from './dto/entity-repository/entity-repository-out-mapper.class';
import { EntityUtilsService } from './entity.utils.service';

const urlBase = `${environment.API_URL_BACKOFFICE}`;

@Injectable({
  providedIn: 'root',
})
export class EntityService extends ApiService<EntityRepository> implements IApiService2<EntityRepository> {
  endPoint = `products/backoffice-entities/${this.version}entities`;

  constructor(
    protected http: HttpClient,
    protected utils: EntityUtilsService,
    @Inject(FF_API_PATH_VERSION_ENTITIES) private version: string
  ) {
    super();
  }

  listWithFilter(entityTypeId: string, filter: GenericApiRequest): Observable<GenericApiResponse<any>> {
    const url = `${urlBase}/${this.endPoint}`;
    const new_filter: any = this.getFilter(filter);
    new_filter.entity_type_id = entityTypeId;
    return this.http.get<GenericApiResponse<EntityRepository>>(url, { params: this.getParams(new_filter) }).pipe(
      map(({ page, content }) => ({
        page,
        content: content.map((c) => new EntityRepositoryInMapper(c).data),
      }))
    ) as Observable<GenericApiResponse<any>>;
  }

  list(entityType: EntityType): Observable<GenericApiResponse<EntityRepository>> {
    const url = `${urlBase}/${this.endPoint}/type/${entityType.id as string}`;
    return this.http.get<GenericApiResponse<EntityRepository>>(url).pipe(
      map(({ page, content }) => ({
        page,
        content: content.map((c) => new EntityRepositoryInMapper(c).data),
      }))
    );
  }

  search(entityType: EntityType): Observable<EntityRepository[]> {
    const url = `${urlBase}/${this.endPoint}/type/${entityType.id as string}?size=9999`;
    return this.http.get<GenericApiResponse<EntityRepository>>(url).pipe(map(({ content }) => content));
  }

  detail(id: string): Observable<EntityRepository> {
    return super.detail(id).pipe(map((content) => new EntityRepositoryInMapper(content).data));
  }

  post(data: EntityRepository): Observable<UpdateResponse<EntityRepository>> {
    return super.post(new EntityRepositoryOutMapper(data).data as EntityRepository).pipe(
      map((response) => ({
        status: response.status,
        data: new EntityRepositoryInMapper(response.data).data,
      }))
    );
  }

  update(data: EntityRepository): Observable<UpdateResponse<EntityRepository>> {
    return super.update(new EntityRepositoryOutMapper(data).data as EntityRepository).pipe(
      map((response) => ({
        status: response.status,
        data: new EntityRepositoryInMapper(response.data).data,
      }))
    );
  }

  auditDetail({ page = 0, size = 10, ...query }: Partial<AuditQuery>) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http
      .request('post', `${this.urlBase.replace(/\/entities/g, '/detailed-entities-audits/search')}`, {
        params: { page, size },
        body: query,
        headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            content: res.body?.content,
            page: res.body?.page,
          };
        })
      );
  }
}
