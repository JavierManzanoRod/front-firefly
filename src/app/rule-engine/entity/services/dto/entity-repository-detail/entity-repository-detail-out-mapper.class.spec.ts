import { EntityRepository } from '../../../models/entity.model';
import { EntityRepositoryDetailOutMapper } from './entity-repository-detail-out-mapper.class';

const entities = [
  {
    id: '39dbae10-b44e-4003-ae6d-9f5a94e0c9c6',
    type: 'A',
    name: 'aaa',
    attributes: {
      '41829a2b-7036-4bf6-b94e-7d6b1bb4ed2b': 11,
      '465557ab-5284-4186-b63f-1d07ea772c7a': [11.4, 11.6],
    },
  },
];

describe('EntityRepository mapper out', () => {
  it('parse a outgoing request as expected primitives numbers', () => {
    const mapper = new EntityRepositoryDetailOutMapper(entities[0]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.identifier).toEqual('39dbae10-b44e-4003-ae6d-9f5a94e0c9c6');
    expect(entityRepositoryDTO.type).toEqual('A');
    expect(entityRepositoryDTO.name).toEqual('aaa');
    const attributes = entityRepositoryDTO.attributes;
    expect(attributes['41829a2b-7036-4bf6-b94e-7d6b1bb4ed2b']).toEqual(11);

    const att2 = attributes['465557ab-5284-4186-b63f-1d07ea772c7a'];
    expect(att2).toEqual([11.4, 11.6]);
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem: EntityRepository = {} as EntityRepository;

    const mapper = new EntityRepositoryDetailOutMapper(remoteItem);
    const dto = mapper.data;
    expect(dto.identifier).toEqual(undefined);
    expect(dto.name).toBeUndefined();
    expect(dto.type).toBeUndefined();
    expect(dto.attributes).toEqual(undefined);
  });
});
