import { EntityRepositoryDetailDTO } from '../../../models/entity.dto';
import { EntityRepositoryDetail } from '../../../models/entity.model';

export class EntityRepositoryDetailInMapper {
  data = {} as EntityRepositoryDetail;

  constructor(data: EntityRepositoryDetailDTO) {
    this.data = {
      id: data.identifier,
      name: data.name,
      type: data.type,
      attributes: data.attributes,
    };
  }
}
