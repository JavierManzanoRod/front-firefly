import { EntityRepositoryDetailDTO } from '../../../models/entity.dto';
import { EntityRepositoryDetail } from '../../../models/entity.model';

export class EntityRepositoryDetailOutMapper {
  data = {} as EntityRepositoryDetailDTO;

  constructor(data: EntityRepositoryDetail) {
    this.data = {
      ...(data.id && { identifier: data.id }),
      name: data.name,
      type: data.type,
      attributes: data.attributes,
    };
  }
}
