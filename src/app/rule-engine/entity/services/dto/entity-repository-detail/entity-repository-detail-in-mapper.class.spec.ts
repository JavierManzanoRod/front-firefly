import { EntityRepository } from '../../../models/entity.model';
import { EntityRepositoryDetailInMapper } from './entity-repository-detail-in-mapper.class';

const entities = [
  {
    identifier: '1427edc2-6978-428b-bf36-4886e6abce06',
    type: 'A',
    name: 'entidadmulti',
    attributes: {
      'fe15c901-5ddc-4417-87fd-8989f0499f35': {
        es_ES: 'hola',
      },
      '0adcac4b-6842-4a35-9eb6-7a073cee1bf2': {
        identifier: '542c7f03-4e6c-408b-af08-18321d199b78',
        entity_type_id: '2b6bc15d-44bf-49f0-8b6c-7cc2f10c70e8',
        name: 'renault',
        attributes: {
          'd9adde89-5c50-46b9-8036-2c2d0c2f05ea': '4',
        },
      },
    },
  },
];

describe('EntityRepository mapper in', () => {
  it('parse a outgoing request as expected EntityRepositoryDTO', () => {
    const mapper = new EntityRepositoryDetailInMapper(entities[0]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.id).toEqual('1427edc2-6978-428b-bf36-4886e6abce06');
    expect(entityRepositoryDTO.name).toEqual('entidadmulti');
    expect(entityRepositoryDTO.type).toEqual('A');
    const attributes = entityRepositoryDTO.attributes;
    expect(attributes['fe15c901-5ddc-4417-87fd-8989f0499f35']).toEqual({
      es_ES: 'hola',
    });
    const att2 = attributes['0adcac4b-6842-4a35-9eb6-7a073cee1bf2'];
    expect(att2.identifier).toEqual('542c7f03-4e6c-408b-af08-18321d199b78');
    expect(att2.entity_type_id).toEqual('2b6bc15d-44bf-49f0-8b6c-7cc2f10c70e8');
    expect(att2.name).toEqual('renault');
    expect(att2.attributes).toEqual({
      'd9adde89-5c50-46b9-8036-2c2d0c2f05ea': '4',
    });
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem: EntityRepository = {} as EntityRepository;

    const mapper = new EntityRepositoryDetailInMapper(remoteItem);
    const dto = mapper.data;
    expect(dto.id).toEqual(undefined);
    expect(dto.name).toBeUndefined();
    expect(dto.type).toBeUndefined();
    expect(dto.attributes).toEqual(undefined);
  });
});
