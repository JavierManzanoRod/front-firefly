import { AttributeOriginal, EntityRepositoryDTO } from '../../../models/entity.dto';
import { EntityRepository } from '../../../models/entity.model';
import { EntityRepositoryOutMapper } from './entity-repository-out-mapper.class';

const entities = [
  {
    id: '1427edc2-6978-428b-bf36-4886e6abce06',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'entidadmulti',
    attributes: {
      'fe15c901-5ddc-4417-87fd-8989f0499f35': {
        es_ES: 'hola',
      },
      '0adcac4b-6842-4a35-9eb6-7a073cee1bf2': {
        id: '542c7f03-4e6c-408b-af08-18321d199b78',
        entity_type_id: '2b6bc15d-44bf-49f0-8b6c-7cc2f10c70e8',
        name: 'renault',
        attributes: {
          'd9adde89-5c50-46b9-8036-2c2d0c2f05ea': '4',
        },
      },
    },
  },
  {
    id: '39dbae10-b44e-4003-ae6d-9f5a94e0c9c6',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'aaa',
    attributes: {
      '41829a2b-7036-4bf6-b94e-7d6b1bb4ed2b': 11,
      '465557ab-5284-4186-b63f-1d07ea772c7a': [11.4, 11.6],
    },
  },
  {
    id: '9be3995e-afcf-4bea-b216-1d85b41da2d4',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'sssss',
    attributes: {
      '56960ae5-b837-4fc9-bc61-fc02207bfb28': ['542c7f03-4e6c-408b-af08-18321d199b78', '81cf47db-6717-4d5d-889b-f7bf8f76633b'],
    },
  },
  {
    id: 'af7e5717-5fad-4002-9051-d07e39cc7b37',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'nums2',
    attributes: {
      '41829a2b-7036-4bf6-b94e-7d6b1bb4ed2b': 11.4,
      '465557ab-5284-4186-b63f-1d07ea772c7a': [11.4, 13.5],
    },
  },
  {
    id: 'bce02779-03be-4f77-a733-f82b8301e105',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'xxx',
    attributes: {
      string_simple: 'hello',
      // 'id-boolean': true,
      'fe15c901-5ddc-4417-87fd-8989f0499f35': {
        es_ES: 'a',
        de_DE: 'c',
        pt_PT: 'b',
      },
      '841ae0fe-da51-4693-aafb-89ab7290bdc9': {
        es_ES: ['aa', 'bbb'],
        pt_PT: ['ccc', 'ddd'],
      },
      number_multy: [33.6],
      number_simple: 110,
      att_label: 'label si',
    },
  },
  {
    id: 'cff0e431-f052-4b63-91fe-39549049a365',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'entidades-multis',
    attributes: {
      '56960ae5-b837-4fc9-bc61-fc02207bfb28': ['542c7f03-4e6c-408b-af08-18321d199b78', '81cf47db-6717-4d5d-889b-f7bf8f76633b'],
    },
  },
];

describe('EntityRepository mapper out', () => {
  it('parse a outgoing request as expected EntityRepositoryDTO', () => {
    const mapper = new EntityRepositoryOutMapper(entities[0]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.identifier).toEqual('1427edc2-6978-428b-bf36-4886e6abce06');
    expect(entityRepositoryDTO.entity_type_id).toEqual('e1e11af9-8132-4d12-bbf0-4becd523d422');
    expect(entityRepositoryDTO.name).toEqual('entidadmulti');
    const attributes = entityRepositoryDTO.attributes as AttributeOriginal;
    expect(attributes['fe15c901-5ddc-4417-87fd-8989f0499f35']).toEqual({
      es_ES: 'hola',
    });
    const att2 = attributes['0adcac4b-6842-4a35-9eb6-7a073cee1bf2'] as EntityRepositoryDTO;
    expect(att2.identifier).toEqual('542c7f03-4e6c-408b-af08-18321d199b78');
    expect(att2.entity_type_id).toEqual('2b6bc15d-44bf-49f0-8b6c-7cc2f10c70e8');
    expect(att2.name).toEqual('renault');
    expect(att2.attributes).toEqual({
      'd9adde89-5c50-46b9-8036-2c2d0c2f05ea': '4',
    });
  });

  it('parse a outgoing request as expected primitives numbers', () => {
    const mapper = new EntityRepositoryOutMapper(entities[1]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.identifier).toEqual('39dbae10-b44e-4003-ae6d-9f5a94e0c9c6');
    expect(entityRepositoryDTO.entity_type_id).toEqual('e1e11af9-8132-4d12-bbf0-4becd523d422');
    expect(entityRepositoryDTO.name).toEqual('aaa');
    const attributes = entityRepositoryDTO.attributes as AttributeOriginal;
    expect(attributes['41829a2b-7036-4bf6-b94e-7d6b1bb4ed2b']).toEqual(11);

    const att2 = attributes['465557ab-5284-4186-b63f-1d07ea772c7a'];
    expect(att2).toEqual([11.4, 11.6]);
  });

  it('parse a outgoing request as expected primitives strings', () => {
    const mapper = new EntityRepositoryOutMapper(entities[2]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.identifier).toEqual('9be3995e-afcf-4bea-b216-1d85b41da2d4');
    expect(entityRepositoryDTO.entity_type_id).toEqual('e1e11af9-8132-4d12-bbf0-4becd523d422');
    expect(entityRepositoryDTO.name).toEqual('sssss');
    const attributes = entityRepositoryDTO.attributes as AttributeOriginal;
    expect(attributes['56960ae5-b837-4fc9-bc61-fc02207bfb28']).toEqual([
      '542c7f03-4e6c-408b-af08-18321d199b78',
      '81cf47db-6717-4d5d-889b-f7bf8f76633b',
    ]);
  });

  it('parse a outgoing request as expected primitives numbers and arrays', () => {
    const mapper = new EntityRepositoryOutMapper(entities[3]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.identifier).toEqual('af7e5717-5fad-4002-9051-d07e39cc7b37');
    expect(entityRepositoryDTO.entity_type_id).toEqual('e1e11af9-8132-4d12-bbf0-4becd523d422');
    expect(entityRepositoryDTO.name).toEqual('nums2');
    const attributes = entityRepositoryDTO.attributes as AttributeOriginal;
    expect(attributes['41829a2b-7036-4bf6-b94e-7d6b1bb4ed2b']).toEqual(11.4);

    const att2 = attributes['465557ab-5284-4186-b63f-1d07ea772c7a'];
    expect(att2).toEqual([11.4, 13.5]);
  });

  it('parse a outgoing request as expected primitives i18n', () => {
    const mapper = new EntityRepositoryOutMapper(entities[4]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.identifier).toEqual('bce02779-03be-4f77-a733-f82b8301e105');
    expect(entityRepositoryDTO.entity_type_id).toEqual('e1e11af9-8132-4d12-bbf0-4becd523d422');
    expect(entityRepositoryDTO.name).toEqual('xxx');
    const attributes = entityRepositoryDTO.attributes as AttributeOriginal;
    expect(attributes.string_simple).toEqual('hello');

    const att2 = attributes['fe15c901-5ddc-4417-87fd-8989f0499f35'];
    expect(att2).toEqual({
      es_ES: 'a',
      de_DE: 'c',
      pt_PT: 'b',
    });
    const att3 = attributes['841ae0fe-da51-4693-aafb-89ab7290bdc9'];
    expect(att3).toEqual({
      es_ES: ['aa', 'bbb'],
      pt_PT: ['ccc', 'ddd'],
    });
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem: EntityRepository = {} as EntityRepository;

    const mapper = new EntityRepositoryOutMapper(remoteItem);
    const dto = mapper.data;
    expect(dto.identifier).toEqual(undefined);
    expect(dto.name).toBeUndefined();
    expect(dto.type).toBeUndefined();
    expect(dto.attributes).toEqual(undefined);
  });
});
