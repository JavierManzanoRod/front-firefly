import { EntityRepository } from '../../../models/entity.model';
import { EntityRepositoryInMapper } from './entity-repository-in-mapper.class';
import { EntityRepositoryOutMapper } from './entity-repository-out-mapper.class';

const entities = [
  {
    identifier: '1427edc2-6978-428b-bf36-4886e6abce06',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'entidadmulti',
    attributes: {
      'fe15c901-5ddc-4417-87fd-8989f0499f35': {
        es_ES: 'hola',
      },
      '0adcac4b-6842-4a35-9eb6-7a073cee1bf2': {
        identifier: '542c7f03-4e6c-408b-af08-18321d199b78',
        entity_type_id: '2b6bc15d-44bf-49f0-8b6c-7cc2f10c70e8',
        name: 'renault',
        attributes: {
          'd9adde89-5c50-46b9-8036-2c2d0c2f05ea': '4',
        },
      },
    },
  },
  {
    identifier: '39dbae10-b44e-4003-ae6d-9f5a94e0c9c6',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'aaa',
    attributes: {
      '41829a2b-7036-4bf6-b94e-7d6b1bb4ed2b': 11,
      '465557ab-5284-4186-b63f-1d07ea772c7a': [11.4, 11.6],
    },
  },
  {
    identifier: '9be3995e-afcf-4bea-b216-1d85b41da2d4',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'sssss',
    attributes: {
      '56960ae5-b837-4fc9-bc61-fc02207bfb28': ['542c7f03-4e6c-408b-af08-18321d199b78', '81cf47db-6717-4d5d-889b-f7bf8f76633b'],
    },
  },
  {
    identifier: 'af7e5717-5fad-4002-9051-d07e39cc7b37',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'nums2',
    attributes: {
      '41829a2b-7036-4bf6-b94e-7d6b1bb4ed2b': 11.4,
      '465557ab-5284-4186-b63f-1d07ea772c7a': [11.4, 13.5],
    },
  },
  {
    identifier: 'bce02779-03be-4f77-a733-f82b8301e105',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'xxx',
    attributes: {
      string_simple: 'hello',
      // 'id-boolean': true,
      'fe15c901-5ddc-4417-87fd-8989f0499f35': {
        es_ES: 'a',
        de_DE: 'c',
        pt_PT: 'b',
      },
      '841ae0fe-da51-4693-aafb-89ab7290bdc9': {
        es_ES: ['aa', 'bbb'],
        pt_PT: ['ccc', 'ddd'],
      },
      number_multy: [33.6],
      number_simple: 110,
      att_label: 'label si',
    },
  },
  {
    identifier: 'cff0e431-f052-4b63-91fe-39549049a365',
    entity_type_id: 'e1e11af9-8132-4d12-bbf0-4becd523d422',
    name: 'entidades-multis',
    attributes: {
      '56960ae5-b837-4fc9-bc61-fc02207bfb28': ['542c7f03-4e6c-408b-af08-18321d199b78', '81cf47db-6717-4d5d-889b-f7bf8f76633b'],
    },
  },
];
const entities2 = [
  {
    name: 'Hipercor',
    attributes: [
      { identifier: '276db477-3008-4ae6-85c2-8cabbb9283e7', value: 'Hipercor', is_label_attribute: true },
      { identifier: 'eeeb7b79-a880-4f9b-ba89-c6ae9db0f5ea', value: 'hipercorStore', is_label_attribute: false },
    ],
    identifier: 'e733221e-f8d9-4a2a-867b-4a9279ec024d',
    entity_type_id: '765ba9fb-5034-42f3-a4a3-026978f72753',
  },
  {
    name: 'PEE&PLAY',
    attributes: [
      {
        identifier: '8168ac7b-6133-4292-ba80-9759c768ac14',
        value: 'PEE&PLAY',
        is_label_attribute: false,
      },
      {
        identifier: 'eff6f32e-41e4-4a77-943c-28c0355e4088',
        value: {
          es_ES: 'Pee&Play',
        },
        is_label_attribute: true,
      },
    ],
    identifier: 'ffffe127-ee00-49e3-b321-243047c6bc5e',
    entity_type_id: '81b640b8-f620-4533-b2c7-a13bd94e87e3',
  },
];
describe('EntityRepository mapper in', () => {
  it('parse a outgoing request as expected EntityRepositoryDTO', () => {
    const mapper = new EntityRepositoryInMapper(entities[0]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.id).toEqual('1427edc2-6978-428b-bf36-4886e6abce06');
    expect(entityRepositoryDTO.entity_type_id).toEqual('e1e11af9-8132-4d12-bbf0-4becd523d422');
    expect(entityRepositoryDTO.name).toEqual('entidadmulti');
    const attributes = entityRepositoryDTO.attributes;
    expect(attributes['fe15c901-5ddc-4417-87fd-8989f0499f35']).toEqual({
      es_ES: 'hola',
    });
    const att2 = attributes['0adcac4b-6842-4a35-9eb6-7a073cee1bf2'] as EntityRepository;
    expect(att2.id).toEqual('542c7f03-4e6c-408b-af08-18321d199b78');
    expect(att2.entity_type_id).toEqual('2b6bc15d-44bf-49f0-8b6c-7cc2f10c70e8');
    expect(att2.name).toEqual('renault');
    expect(att2.attributes).toEqual({
      'd9adde89-5c50-46b9-8036-2c2d0c2f05ea': '4',
    });
  });

  it('parse a outgoing request as expected primitives numbers', () => {
    const mapper = new EntityRepositoryInMapper(entities[1]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.id).toEqual('39dbae10-b44e-4003-ae6d-9f5a94e0c9c6');
    expect(entityRepositoryDTO.entity_type_id).toEqual('e1e11af9-8132-4d12-bbf0-4becd523d422');
    expect(entityRepositoryDTO.name).toEqual('aaa');
    const attributes = entityRepositoryDTO.attributes;
    expect(attributes['41829a2b-7036-4bf6-b94e-7d6b1bb4ed2b']).toEqual(11);

    const att2 = attributes['465557ab-5284-4186-b63f-1d07ea772c7a'];
    expect(att2).toEqual([11.4, 11.6]);
  });

  it('parse a outgoing request as expected primitives strings', () => {
    const mapper = new EntityRepositoryInMapper(entities[2]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.id).toEqual('9be3995e-afcf-4bea-b216-1d85b41da2d4');
    expect(entityRepositoryDTO.entity_type_id).toEqual('e1e11af9-8132-4d12-bbf0-4becd523d422');
    expect(entityRepositoryDTO.name).toEqual('sssss');
    const attributes = entityRepositoryDTO.attributes;
    expect(attributes['56960ae5-b837-4fc9-bc61-fc02207bfb28']).toEqual([
      '542c7f03-4e6c-408b-af08-18321d199b78',
      '81cf47db-6717-4d5d-889b-f7bf8f76633b',
    ]);
  });

  it('parse a outgoing request as expected primitives numbers and arrays', () => {
    const mapper = new EntityRepositoryInMapper(entities[3]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.id).toEqual('af7e5717-5fad-4002-9051-d07e39cc7b37');
    expect(entityRepositoryDTO.entity_type_id).toEqual('e1e11af9-8132-4d12-bbf0-4becd523d422');
    expect(entityRepositoryDTO.name).toEqual('nums2');
    const attributes = entityRepositoryDTO.attributes;
    expect(attributes['41829a2b-7036-4bf6-b94e-7d6b1bb4ed2b']).toEqual(11.4);

    const att2 = attributes['465557ab-5284-4186-b63f-1d07ea772c7a'];
    expect(att2).toEqual([11.4, 13.5]);
  });

  it('parse a outgoing request as expected primitives i18n', () => {
    const mapper = new EntityRepositoryInMapper(entities[4]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.id).toEqual('bce02779-03be-4f77-a733-f82b8301e105');
    expect(entityRepositoryDTO.entity_type_id).toEqual('e1e11af9-8132-4d12-bbf0-4becd523d422');
    expect(entityRepositoryDTO.name).toEqual('xxx');
    const attributes = entityRepositoryDTO.attributes;
    expect(attributes.string_simple).toEqual('hello');

    const att2 = attributes['fe15c901-5ddc-4417-87fd-8989f0499f35'];
    expect(att2).toEqual({
      es_ES: 'a',
      de_DE: 'c',
      pt_PT: 'b',
    });
    const att3 = attributes['841ae0fe-da51-4693-aafb-89ab7290bdc9'];
    expect(att3).toEqual({
      es_ES: ['aa', 'bbb'],
      pt_PT: ['ccc', 'ddd'],
    });
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem: EntityRepository = {} as EntityRepository;

    const mapper = new EntityRepositoryOutMapper(remoteItem);
    const dto = mapper.data;
    expect(dto.identifier).toEqual(undefined);
    expect(dto.name).toBeUndefined();
    expect(dto.type).toBeUndefined();
    expect(dto.attributes).toEqual(undefined);
  });

  it('parse the new version of Attributes (sites)', () => {
    const mapper = new EntityRepositoryInMapper(entities2[0]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.id).toEqual('e733221e-f8d9-4a2a-867b-4a9279ec024d');
    expect(entityRepositoryDTO.label).toEqual('Hipercor');
    expect(entityRepositoryDTO.entity_type_id).toEqual('765ba9fb-5034-42f3-a4a3-026978f72753');
    expect(entityRepositoryDTO.name).toEqual('Hipercor');
    const attributes = entityRepositoryDTO.attributes;
    expect(attributes['276db477-3008-4ae6-85c2-8cabbb9283e7']).toEqual('Hipercor');

    const att2 = attributes['eeeb7b79-a880-4f9b-ba89-c6ae9db0f5ea'];
    expect(att2).toEqual('hipercorStore');
  });

  it('parse the new version of Attributes (brand)', () => {
    const mapper = new EntityRepositoryInMapper(entities2[1]);
    const entityRepositoryDTO = mapper.data;
    expect(entityRepositoryDTO.id).toEqual('ffffe127-ee00-49e3-b321-243047c6bc5e');
    expect(entityRepositoryDTO.label).toEqual('Pee&Play');
    expect(entityRepositoryDTO.entity_type_id).toEqual('81b640b8-f620-4533-b2c7-a13bd94e87e3');
    expect(entityRepositoryDTO.name).toEqual('PEE&PLAY');
    const attributes = entityRepositoryDTO.attributes;
    expect(attributes['8168ac7b-6133-4292-ba80-9759c768ac14']).toEqual('PEE&PLAY');

    const att2 = attributes['eff6f32e-41e4-4a77-943c-28c0355e4088'];
    expect(att2).toEqual({
      es_ES: 'Pee&Play',
    });
  });
});
