import { EntityRepositoryDTO, ValueAttEntityRepositoryDTO, ValueDTO } from '../../../models/entity.dto';
import { EntityRepository, EntityRepositoryDetail, I18nValue, Value, ValueAttEntityRepository } from '../../../models/entity.model';
import { EntityRepositoryDetailOutMapper } from '../entity-repository-detail/entity-repository-detail-out-mapper.class';

export class EntityRepositoryOutMapper {
  data = {} as EntityRepositoryDTO;

  constructor(data: EntityRepository) {
    this.data = {
      ...(data.id && { identifier: data.id }),
      entity_type_id: data.entity_type_id,
      name: data.name,
      type: data.type,
      attributes:
        data.attributes &&
        Object.entries(data.attributes).reduce((prev, [key, value]) => {
          return { ...prev, [key]: this.mapAttribute(value) };
        }, {}),
    };
  }

  mapAttribute(attribute: ValueAttEntityRepository | I18nValue | undefined) {
    if (!attribute) {
      return attribute;
    } else if (this.isI18nValue(attribute)) {
      return Array.isArray(attribute) ? [...attribute] : { ...attribute };
    } else {
      return new EntityRepositoryValueOutMapper(attribute).data;
    }
  }

  isI18nValue(data: any): data is I18nValue {
    return (
      typeof data === 'object' &&
      !data.id &&
      Object.values(data).every((value) => this.isString(value) || (Array.isArray(value) && value.every((v) => this.isStringOrNull(v))))
    );
  }

  isString(val: any) {
    return typeof val === 'string';
  }
  isNull(val: any) {
    return val === null;
  }

  isStringOrNull(val: any) {
    return this.isString(val) || this.isNull(val);
  }
}

export class EntityRepositoryValueOutMapper {
  data = {} as ValueAttEntityRepositoryDTO;

  constructor(data: ValueAttEntityRepository) {
    this.data = Array.isArray(data) ? data.map((remote) => this.mapData(remote)) : this.mapData(data);
  }

  mapData(attribute: Value): ValueDTO | ValueDTO[] {
    if (['number', 'string', 'boolean'].includes(typeof attribute)) {
      return attribute;
    } else if (typeof attribute === 'object') {
      if (this.isEntityRepository(attribute)) {
        return new EntityRepositoryOutMapper(attribute).data;
      } else if (this.isEntityRepositoryDetail(attribute)) {
        return new EntityRepositoryDetailOutMapper(attribute).data;
      } else {
        return { ...attribute };
      }
    }
    return attribute;
  }

  isEntityRepository(data: any): data is EntityRepository {
    return data.entity_type_id && this.isEntityRepositoryDetail(data);
  }

  isEntityRepositoryDetail(data: any): data is EntityRepositoryDetail {
    const isData: boolean = data.id && data.name && data.attributes;
    return isData;
  }
}
