import {
  AttributeContainer,
  EntityRepositoryDetailDTO,
  EntityRepositoryDTO,
  I18nValueDTO,
  ValueAttEntityRepositoryDTO,
  ValueDTO,
} from '../../../models/entity.dto';
import { EntityRepository, I18nValue, Value, ValueAttEntityRepository } from '../../../models/entity.model';
import { EntityRepositoryDetailInMapper } from '../entity-repository-detail/entity-repository-detail-in-mapper.class';

export class EntityRepositoryInMapper {
  data = {} as EntityRepository;
  private ES = 'es_ES';
  constructor(data: EntityRepositoryDTO) {
    let label: string | null = null;
    this.data = {
      id: data.identifier,
      entity_type_id: data.entity_type_id,
      name: data.name,
      type: data.type,
      attributes: Array.isArray(data.attributes)
        ? data.attributes.reduce((prev, curr) => {
            if (curr.is_label_attribute) {
              label = this.parseAttributeLabel(curr, data.name);
            }
            return { ...prev, [curr.identifier]: this.mapAttribute(curr.value) };
          }, {})
        : Object.entries(data.attributes).reduce((prev, [key, value]) => {
            return { ...prev, [key]: this.mapAttribute(value) };
          }, {}),
      label: label || data.name || '',
    };
  }

  parseAttributeLabel({ value }: AttributeContainer, name: string | undefined) {
    if (this.isArrayOfStrings(value)) {
      return value[0];
    } else if (this.isI18nValue(value)) {
      const data = value[this.ES] || this.getFirstTranslation(value);
      return Array.isArray(data) ? data[0] : data;
    } else if (this.isString(value)) {
      return value;
    }
    return name || '';
  }

  getFirstTranslation(i18n: I18nValue) {
    return Object.entries(i18n)[0][1];
  }

  mapAttribute(attribute: ValueAttEntityRepositoryDTO | I18nValueDTO | undefined) {
    if (typeof attribute === 'undefined') {
      return undefined;
    } else if (this.isI18nValue(attribute)) {
      return Array.isArray(attribute) ? [...attribute] : { ...attribute };
    } else {
      return new EntityRepositoryValueInMapper(attribute).data;
    }
  }

  isI18nValue(data: any): data is I18nValueDTO {
    return (
      typeof data === 'object' &&
      !data.identifier &&
      Object.values(data).every((value) => this.isString(value) || (Array.isArray(value) && value.every((v) => this.isStringOrNull(v))))
    );
  }

  isString(val: any): val is string {
    return typeof val === 'string';
  }
  isNull(val: any) {
    return val === null;
  }

  isStringOrNull(val: any) {
    return this.isString(val) || this.isNull(val);
  }

  isArrayOfStrings(val: any): val is string[] {
    return Array.isArray(val) && val.length > 0 && this.isString(val[0]);
  }
}

export class EntityRepositoryValueInMapper {
  data = {} as ValueAttEntityRepository;

  constructor(remoteData: ValueAttEntityRepositoryDTO) {
    this.data = Array.isArray(remoteData) ? remoteData.map((remote) => this.mapData(remote)) : this.mapData(remoteData);
  }

  mapData(attribute: ValueDTO): Value | Value[] {
    if (['number', 'string', 'boolean'].includes(typeof attribute)) {
      return attribute;
    } else if (typeof attribute === 'object') {
      if (this.isEntityRepositoryDTO(attribute)) {
        return new EntityRepositoryInMapper(attribute).data;
      } else if (this.isEntityRepositoryDetailDTO(attribute)) {
        return new EntityRepositoryDetailInMapper(attribute).data;
      } else {
        return { ...attribute };
      }
    }
    return attribute;
  }

  isEntityRepositoryDTO(data: any): data is EntityRepositoryDTO {
    return data.entity_type_id && this.isEntityRepositoryDetailDTO(data);
  }

  isEntityRepositoryDetailDTO(data: any): data is EntityRepositoryDetailDTO {
    const isData: boolean = data.identifier && data.name && data.attributes;
    return isData;
  }
}
