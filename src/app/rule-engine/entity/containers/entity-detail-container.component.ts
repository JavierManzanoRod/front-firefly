import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityType } from '@core/models/entity-type.model';
import { MayHaveLabel } from '@model/base-api.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { BaseDetailContainerComponent } from 'src/app/shared/components/base-component-detail-container.component';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { ToastService } from '../../../modules/toast/toast.service';
import { EntityDetailComponent } from '../components/entity-detail/entity-detail.component';
import { Entity, EntityRepository } from '../models/entity.model';
import { EntityApiFactoryService } from '../services/entity-api-factory.service';
import { EntityService } from '../services/entity.service';
import { CrudOperationsService } from './../../../core/services/crud-operations.service';

@Component({
  selector: 'ff-detail-container',
  template: `<ff-page-header
      [pageTitle]="entity && entity.id ? ('ENTITY.TITLE_EDIT' | translate) : ('ENTITY.TITLE_CREATE' | translate)"
      [newBreadCrumb]="true"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'COMMON.BACK' | translate"
    >
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-entity-detail
        (cancel)="cancel()"
        (formSubmit)="handleSubmitForm2($event)"
        (delete)="delete()"
        [item]="entity"
        [original]="original"
        #detailElement
      ></ff-entity-detail>
    </ngx-simplebar> `,
})
export class EntityDetailContainerComponent extends BaseDetailContainerComponent<MayHaveLabel> implements OnInit {
  @ViewChild(EntityDetailComponent) detail!: EntityDetailComponent;
  @Output() changed = new EventEmitter();

  urlPreviousPage = '';
  urlListado = '/rule-engine/entity-types/view?tab=entity';
  suffixTitle = '';
  entity: Entity | undefined;
  entityType: EntityType | undefined;
  original: Entity | undefined;

  constructor(
    public route: ActivatedRoute,
    public apiService: EntityTypeService,
    public apiEntity: EntityService,
    public entityApiFactoryService: EntityApiFactoryService,
    public router: Router,
    public crud: CrudOperationsService,
    public menuLeft: MenuLeftService,
    public modalService: BsModalService,
    public toastService: ToastService
  ) {
    super(menuLeft, toastService, apiService, modalService, router, route);
  }

  ngOnInit() {
    this.loading = true;
    this.route.params?.subscribe(({ id, idEntity }) => {
      this.apiService?.detail(id)?.subscribe((entityType) => {
        this.entityType = entityType;
        this.urlListado = `/rule-engine/entity-types/view/${this.entityType.id}?tab=entity`;
        if (idEntity) {
          const apiEntities = this.entityApiFactoryService.create(this.entityType);
          apiEntities.detail(idEntity)?.subscribe((entity: any) => {
            this.entity = entity.parse;
            this.original = entity.original;
            this.loading = false;
          });
        } else {
          this.entity = this.entityApiFactoryService.from(this.entityType, {} as EntityRepository);
          this.loading = false;
        }
      });
    });
  }

  getPreviousPage({ id }: EntityType): string {
    return id ? '/rule-engine/entity-types/view/' + id : this.urlPreviousPage;
  }

  handleSubmitForm2(itemToSave: Entity) {
    if (this.entityType) {
      this.crud
        .updateOrSaveModal(
          {
            ...this,
            methodToApply: itemToSave.id ? 'PUT' : 'POST',
            toast: this.toastService,
            apiService: this.apiEntity,
          },
          itemToSave
        )
        .subscribe((v) => {
          this.detail.clear();
          if (!itemToSave.id) {
            this.changed.emit(v.data);
            this.router.navigateByUrl(this.urlListado);
          } else {
            this.router.navigateByUrl(`/rule-engine/entity-types/view/${this.entityType?.id}/${v?.data?.id}`);
          }
        });
    }
  }

  cancel() {
    this.router.navigateByUrl(this.urlListado);
  }

  canDeactivate(): UICommonModalConfig | false {
    if (this.detail.form?.dirty) {
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_TRANSLATIONS_1',
        name: this.entityType?.name,
      };
    }
    return false;
  }
}
