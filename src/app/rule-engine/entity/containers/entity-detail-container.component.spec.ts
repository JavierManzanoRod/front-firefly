import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { EntityType } from '@core/models/entity-type.model';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { ToastModule } from '../../../modules/toast/toast.module';
import { EntityRepository } from '../models/entity.model';
import { EntityApiFactoryService } from '../services/entity-api-factory.service';
import { EntityService } from '../services/entity.service';
import { CrudOperationsService } from './../../../core/services/crud-operations.service';
import { EntityDetailContainerComponent } from './entity-detail-container.component';

// starts global mocks

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}

  public get(key: any): any {
    of(key);
  }
}

class ToastrServiceStub {
  public success() {}
}

class BsModalServiceStub {}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Component({
  selector: 'ff-page-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockHeaderComponent {
  @Input() pageTitle: any;
  @Input() breadcrumbTitle: any;
  @Input() breadcrumbLink: any;
}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };

  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }

  public hide() {
    return true;
  }
}

@Component({
  selector: 'ff-entity-detail',
  template: '<p>Mock Product Editor Component</p>',
})
class MockDetailComponent {
  @Input() loading!: boolean;
  @Input() item: any;
  @Input() entityType: any;
  @Input() entityTypesRef: any;
}

let apiSpyFactoryEntity: { create: jasmine.Spy };
// end mocks

describe('EntityDetailContainerComponent', () => {
  const spyApiEntityType: jasmine.SpyObj<EntityTypeService> = jasmine.createSpyObj('EntityTypeService', [
    'detail',
    'update',
    'post',
    'delete',
  ]);
  const spyApiEntity: jasmine.SpyObj<EntityService> = jasmine.createSpyObj('EntityService', ['list', 'detail', 'resolveDept']);

  const idEntity = 'idEntity';
  const idEntityType = 'idEntityType';

  apiSpyFactoryEntity = jasmine.createSpyObj('EntityApiFactoryService', ['create']);
  apiSpyFactoryEntity.create.and.returnValue({
    detail() {
      return of({ id: idEntity });
    },
  });

  beforeEach(async () => {
    await
      TestBed.configureTestingModule({
        declarations: [EntityDetailContainerComponent, MockDetailComponent, TranslatePipeMock, MockHeaderComponent],
        imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule, ToastModule],
        providers: [
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: ToastrService, useClass: ToastrServiceStub },
          { provide: BsModalService, useClass: ModalServiceMock },
          { provide: CrudOperationsService, useFactory: () => ({}) },
          { provide: EntityTypeService, useValue: spyApiEntityType },
          { provide: EntityService, useValue: spyApiEntity },
          { provide: EntityApiFactoryService, useValue: apiSpyFactoryEntity },
          {
            provide: ActivatedRoute,
            useValue: {
              params: of({ idEntity, idEntityType }),
              paramMap: of({}),
            },
          },
        ],
      }).compileComponents();
    }
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(EntityDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  xit('ngOnInit initialize entity fields', () => {
    spyApiEntity.detail.and.returnValue(of({ id: idEntity } as EntityRepository));
    const fixture = TestBed.createComponent(EntityDetailContainerComponent);
    const component = fixture.debugElement.componentInstance as EntityDetailContainerComponent;
    component.ngOnInit();
  });
});
