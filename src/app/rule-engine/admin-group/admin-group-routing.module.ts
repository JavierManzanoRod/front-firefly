import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { UiModalWarningUnChangesService } from 'src/app/modules/common-modal/services/ui-modal-warning-unchanges.service';
import { AdminGroupDetailContainerComponent } from './containers/admin-group-detail-container';
import { AdminGroupListContainerComponent } from './containers/admin-group-list-container';

const routes: Routes = [
  {
    path: '',
    component: AdminGroupListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'new',
        component: AdminGroupDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
        },
      },
      {
        path: 'view/:id',
        component: AdminGroupDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard, UiModalWarningUnChangesService],
})
export class AdminGroupRoutingModule {}
