import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateTimeService } from '@core/base/date-time.service';
import { Attribute } from '@core/models/attribute.model';
import { MayHaveIdName } from '@model/base-api.model';
import { ERROR_RULES, RulesType } from 'src/app/modules/conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { Conditions2UtilsService } from 'src/app/modules/conditions2/services/conditions2-utils.service';
import { IRulesHeaderProperties } from 'src/app/modules/rules-header-form/models/rules-header.model';
import { RulesHeaderFormService } from 'src/app/modules/rules-header-form/services/rules-header-form.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { AdminGroupType } from 'src/app/rule-engine/admin-group-type/models/admin-group-type.model';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { CategoryRuleService } from 'src/app/rule-engine/category-rule/services/category-rule.service';
import { dateRangeValidator } from '../../../../shared/validations/date-range.validator';

@Component({
  selector: 'ff-admin-group-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./admin-group-detail.component.scss'],
  templateUrl: './admin-group-detail.component.html',
})
export class AdminGroupDetailComponent implements OnChanges, OnInit {
  /**
   * loading flag
   */
  @Input() loading!: boolean;
  /**
   * api item to show in the screen;
   */
  @Input() item!: AdminGroup;
  @Input() adminGroupType!: AdminGroupType;
  @Input() readonly = false;

  @Output() formSubmit: EventEmitter<AdminGroup> = new EventEmitter<AdminGroup>();
  @Output() delete: EventEmitter<AdminGroup> = new EventEmitter<AdminGroup>();
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();

  tmpAttributes: Attribute[] = [];
  tmpEntityTypes!: AdminGroupType['entity_types'];
  tmpAdminGroupType!: AdminGroupType;
  itemLoading = false;
  form: FormGroup;
  sended = false;
  showConditions = false;
  isCollapsedDefinition = false;
  haveChanges = false;
  headerFields!: IRulesHeaderProperties[];
  type = RulesType.admin_group;

  constructor(
    private fb: FormBuilder,
    public dateTimeService: DateTimeService,
    public categoryRulesSearchService: CategoryRuleService,
    private utilsConditions: Conditions2UtilsService,
    private toastService: ToastService,
    private rulesHeaderFormSrv: RulesHeaderFormService
  ) {
    this.form = this.fb.group(
      {
        // eslint-disable-next-line @typescript-eslint/unbound-method
        name: [null, Validators.required],
        active: [false],
        start_date: [null],
        end_date: [null],
        // eslint-disable-next-line @typescript-eslint/unbound-method
        node: [null, Validators.required],
      },
      {
        validators: [dateRangeValidator()],
      }
    );
    this.form.valueChanges.subscribe(() => (this.haveChanges = true));
  }

  ngOnInit() {
    if (this.readonly) {
      this.form.disable();
    }
  }

  ngOnChanges() {
    this.item = JSON.parse(JSON.stringify(this.item));
    if (this.item) {
      this.item.start_date = this.item.start_date ? new Date(this.item.start_date) : undefined;
      this.item.end_date = this.item.end_date ? new Date(this.item.end_date) : undefined;
      this.item.node = this.item.node || undefined;
      this.form.patchValue(
        {
          ...this.item,
        },
        { emitEvent: false }
      );
      // this.form.controls.start_date.markAsPristine();
      // this.form.markAsPristine();
      if (this.adminGroupType && this.adminGroupType.entity_types && this.adminGroupType.entity_types.length) {
        this.getAttrEntityTypeSelected(this.adminGroupType);
      }
      this.showConditions = true;
      setTimeout(() => (this.haveChanges = false), 1000);
    }
    this.headerFields = this.rulesHeaderFormSrv?.getRulesTypeHeader(this.type);
  }

  get isDirty(): boolean {
    return this.haveChanges;
  }

  getAttrEntityTypeSelected(adminGroupType: AdminGroupType) {
    this.tmpEntityTypes = adminGroupType.entity_types;
    this.tmpAdminGroupType = adminGroupType;
    if (this.tmpEntityTypes && this.tmpEntityTypes.length) {
      let tmpAttributes: Attribute[] = [];
      this.tmpEntityTypes.forEach((entityType) => {
        tmpAttributes = tmpAttributes.concat(...entityType.attributes);
      });
      this.tmpAttributes = tmpAttributes;
    }
  }

  changeAttrEntityTypeSelected(adminGroupType: AdminGroupType) {
    this.tmpEntityTypes = adminGroupType.entity_types;
    this.tmpAdminGroupType = adminGroupType;
    this.form.controls.node.reset();
    if (this.tmpEntityTypes && this.tmpEntityTypes.length) {
      let tmpAttributes: Attribute[] = [];
      this.tmpEntityTypes.forEach((entityType) => {
        tmpAttributes = tmpAttributes.concat(...entityType.attributes);
      });
      this.tmpAttributes = tmpAttributes;
    }
  }

  trackByFn(item: MayHaveIdName) {
    return item.id;
  }

  getDataToSend(): AdminGroup | ERROR_RULES {
    // eslint-disable-next-line @typescript-eslint/naming-convention,no-underscore-dangle,id-blacklist,id-match
    const start_date = this.form.value.start_date ? this.dateTimeService.convertUTCDateToSend(this.form.value.start_date) : undefined;

    // eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
    const end_date = this.form.value.end_date ? this.dateTimeService.convertUTCDateToSend(this.form.value.end_date) : undefined;

    const nodeValue = this.utilsConditions.valueToSend(this.form.controls.node.value);
    if (nodeValue === ERROR_RULES.ERROR_RANGE || nodeValue === ERROR_RULES.ERROR_CENTER) {
      return nodeValue;
    }

    return {
      id: this.item && this.item.id,
      name: this.form.value.name,
      active: this.form.value.active,
      admin_group_type_id: this.tmpAdminGroupType.id || '',
      start_date,
      end_date,
      node: nodeValue,
      is_basic: true,
      result: '{}',
      admin_group_type_name: this.tmpAdminGroupType.name,
    };
  }

  submit() {
    this.form.markAllAsTouched();
    this.sended = true;
    this.form.get('node')?.updateValueAndValidity();
    this.form.updateValueAndValidity();
    if (this.form.valid) {
      const node = this.getDataToSend();
      if (node === ERROR_RULES.ERROR_RANGE || node === ERROR_RULES.ERROR_CENTER) {
        this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.RANGES_WITHOUT_CENTERS');
      } else {
        this.formSubmit.emit(node);
        this.form.markAsPristine();
      }
    } else {
      if (this.form.controls.node.errors?.required) {
        this.toastService.error('LIMIT_SALE.ENTER_CONDITION');
      }
      if (this.form.controls.node.errors?.errorRange) {
        this.toastService.error('LIMIT_SALE.ERROR_RANGE');
      }
      if (this.form.controls.node.errors?.isFilled) {
        this.toastService.error('LIMIT_SALE.INCOMPLETE_RULES');
      }
      if (this.form.controls.node.errors?.doubleNotLT) {
        this.toastService.error('LIMIT_SALE.DOUBLE_NOT_LT_ERROR');
      }
    }
  }
}
