import { AdminGroupDetailComponent } from './admin-group-detail.component';
import { FormBuilder } from '@angular/forms';
import { DateTimeService } from '@core/base/date-time.service';
import { AdminGroup } from '../../models/admin-group.model';
import { AdminGroupType } from 'src/app/rule-engine/admin-group-type/models/admin-group-type.model';

describe('AdminGroupDetailComponent', () => {
  it('it creates ', () => {
    const comp = new AdminGroupDetailComponent(
      new FormBuilder(),
      new DateTimeService(),
      null as any,
      null as any,
      null as any,
      null as any
    );
    expect(comp).toBeDefined();
  });

  it('setDisabled set isDisabled flag ', () => {
    const comp = new AdminGroupDetailComponent(
      new FormBuilder(),
      new DateTimeService(),
      null as any,
      null as any,
      null as any,
      null as any
    );

    comp.item = {
      id: 'id',
      active: true,
      admin_group_type_id: 'admin_group_type_id',
      admin_group_type_name: '',
    } as AdminGroup;

    comp.adminGroupType = {
      id: 'idAdminGroupType',
      entity_types: [
        {
          id: 'id1',
          attributes: [
            {
              id: 'idAtt',
            },
            {
              id: 'idAtt2',
            },
          ],
        },
      ],
    } as AdminGroupType;

    comp.ngOnChanges();
  });
});
