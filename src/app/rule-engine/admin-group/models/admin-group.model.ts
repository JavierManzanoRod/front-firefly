import { Conditions2DTO } from 'src/app/catalog/content-group/model/content-group-dto-model';
import { Conditions2 } from 'src/app/modules/conditions2/models/conditions2.model';
import { GenericApiResponse } from '@model/base-api.model';

export interface AdminGroup {
  id?: string;
  name: string;
  start_date?: string | Date;
  start_time?: string | Date;
  end_date?: string | Date;
  end_time?: string | Date;
  admin_group_type_id: string;
  admin_group_type_name: string;
  active: boolean;
  is_basic: boolean;
  node?: Conditions2;
  product_name?: string;
  product_type?: string;
  product_url?: string;
  parent_id?: string;
  folder_id?: string;
  result: string;
}

export interface AdminGroupDTO {
  identifier?: string;
  is_basic: boolean;
  name: string;
  start_date?: string | Date;
  start_time?: string | Date;
  end_date?: string | Date;
  end_time?: string | Date;
  admin_group_type_id: string;
  admin_group_type_name: string;
  is_active: boolean;
  folder_id?: string;
  node?: Conditions2DTO;
  result: string;
}

export type AdminGroupResponse = GenericApiResponse<AdminGroup>;
