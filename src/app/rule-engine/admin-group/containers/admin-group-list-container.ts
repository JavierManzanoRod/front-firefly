import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { SortApiType } from '@core/models/sort.model';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { SearchProvider } from 'src/app/shared/components/list-search/providers/search-provider';
import { AdminGroup } from '../models/admin-group.model';
import { AdminGroupService } from '../services/admin-group.service';

@Component({
  selector: 'ff-admin-group-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header pageTitle="{{ 'ADMIN_GROUP.HEADER_LIST' | translate }}">
        <a [routerLink]="['/rule-engine/admin-group/new']" class="btn btn-primary" data-cy="admin-group-btn-create">
          {{ 'ADMIN_GROUP.ADD' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [list]="list$ | async"
                [loading]="loading"
                [page]="page"
                [checkDateActive]="checkDateActive"
                [valueSearch]="searchName"
                (search)="search($event)"
                [type]="type"
                [currentData]="currentData"
                (delete)="delete($event)"
                [arrayKeys]="arrayKeys"
                [title]="''"
                [customGoDetail]="true"
                (sortEvent)="sort($event)"
                [route]="'/rule-engine/admin-group/view'"
                [placeHolder]="'COMMON.TYPE_SEARCH_PLACEHOLDER'"
                (pagination)="handlePagination($event)"
              >
              </ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class AdminGroupListContainerComponent extends BaseListContainerComponent<AdminGroup> implements OnInit {
  hide = false;
  type = TypeSearch.simple;
  arrayKeys: GenericListConfig[] = [
    {
      key: 'name',
      headerName: 'ADMIN_GROUP.NAME',
      showOrder: true,
    },
    {
      key: 'admin_group_type_name',
      headerName: 'ADMIN_GROUP.TYPE',
      textCenter: true,
    },
    {
      key: 'start_date',
      headerName: 'ADMIN_GROUP.START_DATE',
      textCenter: true,
      canFormatDate: true,
      formatDate: 'dd-MM-yyyy h:mm',
    },
    {
      key: 'end_date',
      headerName: 'ADMIN_GROUP.END_DATE',
      textCenter: true,
      canFormatDate: true,
      formatDate: 'dd-MM-yyyy h:mm',
    },
    {
      key: 'active',
      headerName: 'ADMIN_GROUP.ACTIVE',
      textCenter: true,
      canActiveClass: true,
    },
  ];

  constructor(public apiService: AdminGroupService, public crud: CrudOperationsService, private providerSearch: SearchProvider) {
    super(crud, apiService);
  }

  ngOnInit() {
    this.searchName = this.providerSearch?.data;
    if (this.searchName.length) {
      this.search({
        name: this.searchName || '',
      });
    } else this.sort({ name: 'name', type: SortApiType.asc });
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
