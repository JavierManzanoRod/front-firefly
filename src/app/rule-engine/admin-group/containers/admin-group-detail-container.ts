import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { CanComponentDeactivate } from '@core/guards/can-deactivate-guard';
import { Observable, of, throwError } from 'rxjs';
import { catchError, delay, switchMap, tap } from 'rxjs/operators';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { CrudOperationsService, groupType } from 'src/app/core/services/crud-operations.service';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { ToastService } from '../../../modules/toast/toast.service';
import { AdminGroupType } from '../../admin-group-type/models/admin-group-type.model';
import { AdminGroupTypeService } from '../../admin-group-type/services/admin-group-type.service';
import { AdminGroupDetailComponent } from '../components/admin-group-detail/admin-group-detail.component';
import { AdminGroup } from '../models/admin-group.model'; // , DistrictCode, RegionView
import { AdminGroupService } from '../services/admin-group.service';

@Component({
  selector: 'ff-detail-container',
  template: `<ff-page-header
      [pageTitle]="!id ? ('ADMIN_GROUP.HEADER_NEW' | translate) : ('ADMIN_GROUP.HEADER_DETAIL' | translate)"
      breadcrumbLink="/rule-engine/admin-group"
      [breadcrumbTitle]="'COMMON.RETURN_TO_LIST' | translate"
    >
      <button class="btn btn-dark mr-2" (click)="clone(detail)" data-cy="clone-rule" *ngIf="id">
        {{ 'COMMON.CLONE' | translate }}
      </button>
      <button class="btn btn-dark mr-2" (click)="delete(detail)" data-cy="remove-rule" *ngIf="id">
        {{ 'COMMON.DELETE' | translate }}
      </button>
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-admin-group-detail
        #detailElement
        (formSubmit)="handleSubmitForm($event)"
        (delete)="(undefined)"
        (cancel)="cancel()"
        [loading]="loading"
        [item]="detail$ | async"
        [adminGroupType]="adminGroupType$ | async"
      >
      </ff-admin-group-detail>
    </ngx-simplebar> `,
})
export class AdminGroupDetailContainerComponent extends BaseDetailContainerComponent<AdminGroup> implements OnInit, CanComponentDeactivate {
  @ViewChild('detailElement') detailElement!: AdminGroupDetailComponent;
  @Output() changed = new EventEmitter();

  detail$!: Observable<AdminGroup>;
  urlListado = '/rule-engine/admin-group';
  loading = true;

  isNew = true;

  adminGroupType$: Observable<AdminGroupType> = new Observable<AdminGroupType>();

  constructor(
    public activeRoute: ActivatedRoute,
    public apiService: AdminGroupService,
    public router: Router,
    public toastService: ToastService,
    public adminGroupTypeService: AdminGroupTypeService,
    public menuLeft: MenuLeftService,
    public toast: ToastService,
    public crud: CrudOperationsService
  ) {
    super(crud, menuLeft, activeRoute, router, apiService, toast);
    this.id = this.activeRoute.snapshot.paramMap.get('id') || '';
  }

  ngOnInit() {
    if (this.id) {
      this.detail$ = this.apiService.detail(this.id).pipe(
        tap((detail) => {
          this.detail = detail;
        })
      );
    } else {
      this.detail$ = of(this.apiService.emptyRecord()).pipe(delay(250));
    }

    this.adminGroupType$ = this.detail$.pipe(
      tap(() => (this.loading = true)),
      switchMap(({ admin_group_type_id }) => {
        if (admin_group_type_id) {
          return this.adminGroupTypeService.detail(admin_group_type_id);
        } else {
          return of({} as AdminGroupType);
        }
      }),
      tap(() => (this.loading = false)),
      catchError((err) => {
        this.router.navigateByUrl(this.urlListado).then(() => {
          this.toastService.error('COMMON_ERRORS.NOT_FOUND_DESCRIPTION', 'COMMON_ERRORS.NOT_FOUND_TITLE');
        });
        return throwError(err);
      })
    );
  }

  handleSubmitForm(itemToSave: AdminGroup) {
    itemToSave.is_basic = false;
    this.crudSrv
      .updateOrSaveModal(this, itemToSave, groupType.ADMIN_GROUP)
      .pipe(
        tap((response) => {
          this.detailElement.haveChanges = false;
          this.changed.emit(response.data);
          if (!itemToSave.id) {
            this.router.navigate([`${this.urlListado}/view/${response.data?.id}`]);
          }
        }),
        catchError((e: HttpErrorResponse) => {
          if (e.status === 409) {
            this.detailElement.form.controls.name.setErrors({ 409: true });
          }
          return of(null);
        })
      )
      .subscribe();
  }

  canDeactivate(): UICommonModalConfig | false {
    if (this.detailElement.isDirty) {
      if (this.isNew) {
        return {};
      }
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.detailElement?.item.name,
      };
    }
    return false;
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.activeRoute.parent });
  }

  clone(detail: AdminGroup | undefined) {
    if (detail) {
      detail = this.detailElement.getDataToSend() as AdminGroup;
    }
    if (this.detailElement.isDirty) {
      this.toastService.error('COMMON_ERRORS.CANNOT_CLONE');
      return;
    }
    if (this.detailElement?.form?.valid) {
      super.clone(detail, this.urlListado + '/view');
    } else {
      this.crudSrv.toast.warning('COMMON_ERRORS.CLONE_FORM_ERROR');
    }
  }
}
