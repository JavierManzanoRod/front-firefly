import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Component, Input, PipeTransform, Pipe } from '@angular/core';
import { of, throwError } from 'rxjs';

import { TranslateService } from '@ngx-translate/core';
import { AdminGroupService } from '../services/admin-group.service';
import { AdminGroupDetailContainerComponent } from './admin-group-detail-container';
import { AdminGroup } from '../models/admin-group.model';
import { AdminGroupType } from '../../admin-group-type/models/admin-group-type.model';
import { AdminGroupTypeService } from '../../admin-group-type/services/admin-group-type.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { CrudOperationsService } from '@core/services/crud-operations.service';

const textToShowWhenError = 'Se ha producido un error inesperado: el sistema no ha podido guardar el registro.';

// starts global mocks

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}
  public get(key: any): any {
    of(key);
  }
}
class ToastrServiceStub {
  public success() {}
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Component({
  selector: 'ff-page-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockHeaderComponent {
  @Input() pageTitle: any;
  @Input() breadcrumbTitle: any;
  @Input() breadcrumbLink: any;
}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };
  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }
  public hide() {
    return true;
  }
}

@Component({
  selector: 'ff-admin-group-detail',
  template: '<p>Mock Product Editor Component</p>',
})
class MockDetailComponent {
  @Input() loading!: boolean;
  @Input() item: any;
  @Input() adminGroupType!: AdminGroupType;
}

xdescribe('AdminGroupDetailContainerComponent', () => {
  let itemServiceSpy: jasmine.SpyObj<AdminGroupService>;
  let modalMockInstance: ModalServiceMock;
  let router: Router;

  beforeEach(
    waitForAsync(() => {
      const spy: jasmine.SpyObj<AdminGroupService> = jasmine.createSpyObj('AdminGroupService', [
        'detail',
        'update',
        'post',
        'delete',
        'emptyRecord',
      ]);
      const spyApiAdminGroupType: jasmine.SpyObj<AdminGroupTypeService> = jasmine.createSpyObj('AdminGroupTypeService', [
        'detail',
        'update',
        'post',
        'delete',
        'emptyRecord',
      ]);
      TestBed.configureTestingModule({
        declarations: [AdminGroupDetailContainerComponent, MockDetailComponent, TranslatePipeMock, MockHeaderComponent],
        imports: [
          RouterTestingModule.withRoutes([{ path: 'rule-engine/admin-group', component: MockDetailComponent }]),
          HttpClientTestingModule,
        ],
        providers: [
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: ToastService, useClass: ToastrServiceStub },
          { provide: BsModalService, useClass: ModalServiceMock },
          { provide: CrudOperationsService },
          { provide: AdminGroupService, useValue: spy },
          { provide: AdminGroupTypeService, useValue: spyApiAdminGroupType },
        ],
      }).compileComponents();

      // eslint-disable-next-line , import/no-deprecated
      router = TestBed.get(Router);

      // eslint-disable-next-line , import/no-deprecated
      itemServiceSpy = TestBed.get(AdminGroupService);
      itemServiceSpy.update.and.returnValue(of(null as any));
      itemServiceSpy.delete.and.returnValue(of(null as any));

      //   eslint-disable-next-line import/no-deprecated
      modalMockInstance = TestBed.get(BsModalService);
    })
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(AdminGroupDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should handle updates existing items', () => {
    itemServiceSpy.update.and.returnValue(of({ status: 200, data: null as any }));
    const fixture = TestBed.createComponent(AdminGroupDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const data: AdminGroup = {
      name: 'name',
      id: '1',
      admin_group_type_id: '',
      admin_group_type_name: '',
      active: true,
      result: '',
      node: null as any,
    } as AdminGroup;
    component.handleSubmitForm(data);
    expect(itemServiceSpy.update.calls.count()).toBe(1, 'spy update method was called once');
  });

  it('should set message error in the modal if some error occurs', () => {
    itemServiceSpy.post.and.returnValue(throwError('error'));
    const fixture = TestBed.createComponent(AdminGroupDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const data: AdminGroupType = {
      name: 'name',
    } as AdminGroupType;
    component.handleSubmitForm(data);
    expect(modalMockInstance.errorMessage).toBe(textToShowWhenError, ' message error');
  });

  it('should set custom message error in the modal if 409 error occurs', () => {
    itemServiceSpy.post.and.returnValue(
      throwError({
        status: 409,
      })
    );
    const fixture = TestBed.createComponent(AdminGroupDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const item: AdminGroupType = {
      name: 'name',
    } as AdminGroupType;
    component.handleSubmitForm(item);
    expect(modalMockInstance.errorMessage).toBe('');
  });

  it('should handle deleting items and then redirect to the list page', () => {
    const navigateSpy = spyOn(router, 'navigate');
    const fixture = TestBed.createComponent(AdminGroupDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const item: AdminGroup = {
      id: '1',
      name: 'name',
    } as AdminGroup;
    component.delete(item);
    expect(itemServiceSpy.delete.calls.count()).toBe(1, 'spy delete method was called once');
    expect(navigateSpy).toHaveBeenCalledWith(['/rule-engine/admin-group']);
  });

  it('ngOnInit initialize AdminGroup, adminGroupType fields', (done) => {
    const idAdminGroupType = 'idAdminGroupType';
    const fixture = TestBed.createComponent(AdminGroupDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    component.ngOnInit();
    component.detail$.subscribe((adminGroup = { admin_group_type_id: 'idAdminGroupType' }) => {
      expect(adminGroup.admin_group_type_id).toBe(idAdminGroupType);
      done();
    });
  });
});
