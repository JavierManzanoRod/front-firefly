import { HttpClient } from '@angular/common/http';
import { GenericApiRequest, GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { AdminGroupDTO } from '../models/admin-group.model';
import { AdminGroupService } from './admin-group.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: AdminGroupService;

const version = 'v1/';
const expectedData: AdminGroupDTO = {
  identifier: '12312321',
  name: 'pruebas mock',
  start_date: '2020-08-23T10:20Z',
  is_basic: true,
  end_date: '2022-11-30T23:03Z',
  admin_group_type_id: '0b062b13-89ec-4892-8ea6-294c53930c25',
  admin_group_type_name: 'lui21',
  result: "{stringProp: 'eeee'}",
  is_active: true,
  node: undefined,
};

const expectedList: GenericApiResponse<AdminGroupDTO> = {
  content: [expectedData],
  page: (null as unknown) as Page,
};

describe('AdminGroupService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`AdminGroupService points to products/backoffice-admin-groups/${version}admin-groups`, () => {
    myService = new AdminGroupService(null as any, version);
    expect(myService.endPoint).toEqual(`products/backoffice-admin-groups/${version}admin-groups`);
  });

  it('AdminGroupService.list calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new AdminGroupService(httpClientSpy as any, version);
    myService.list((null as unknown) as GenericApiRequest).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });

  it('AdminGroupService.emptyRecord record gives me some data', () => {
    myService = new AdminGroupService((null as unknown) as HttpClient, version);
    const record = myService.emptyRecord();
    expect(record).toBeDefined();
  });
});
