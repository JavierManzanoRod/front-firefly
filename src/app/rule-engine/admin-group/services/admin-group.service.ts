import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { UpdateResponse } from '@core/base/api.service';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_ADMIN_GROUPS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroupServiceGeneral } from 'src/app/shared/services/apis/admin-group-general.service';
import { AdminGroup, AdminGroupDTO } from '../models/admin-group.model';
import { AdminGroupInMapper } from './DTO/admin-group-in-mapper-class';
import { AdminGroupOutMapper } from './DTO/admin-group-out-mapper-class';

@Injectable({
  providedIn: 'root',
})
export class AdminGroupService extends AdminGroupServiceGeneral<AdminGroup> {
  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS) protected version: string) {
    super(http, version);
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<AdminGroup>> {
    if (filter?.type) {
      filter.admin_group_type_id = filter.type;
      delete filter.type;
    }
    return super.list(filter).pipe(
      map((response) => {
        response.content = response.content.map((res) => new AdminGroupOutMapper(res as unknown as AdminGroupDTO).data);
        return response;
      })
    );
  }

  detail(id: string): Observable<AdminGroup> {
    return super.detail(id).pipe(map((detail) => new AdminGroupOutMapper(detail as unknown as AdminGroupDTO).data));
  }

  post(data: AdminGroup): Observable<UpdateResponse<AdminGroup>> {
    return super.post(new AdminGroupInMapper(data).data as unknown as AdminGroup).pipe(
      map((res: UpdateResponse<AdminGroup>) => {
        return {
          ...res,
          data: new AdminGroupOutMapper(res.data as unknown as AdminGroupDTO).data as unknown as AdminGroup,
        };
      })
    );
  }

  update(data: AdminGroup): Observable<UpdateResponse<AdminGroup>> {
    return super.update(new AdminGroupInMapper(data).data as unknown as AdminGroup).pipe(
      map((res: UpdateResponse<AdminGroup>) => {
        return {
          ...res,
          data: new AdminGroupOutMapper(res.data as unknown as AdminGroupDTO).data as unknown as AdminGroup,
        };
      })
    );
  }
}
