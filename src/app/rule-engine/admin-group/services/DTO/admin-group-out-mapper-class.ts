import { Condition2AttributeDTO, Condition2MultipleDTO, Conditions2DTO } from 'src/app/catalog/content-group/model/content-group-dto-model';
import {
  Condition2Attribute,
  Condition2Multiple,
  Condition2SimpleDataTypes,
  Conditions2,
  Conditions2Node,
} from 'src/app/modules/conditions2/models/conditions2.model';
import { AdminGroup, AdminGroupDTO } from '../../models/admin-group.model';

export class AdminGroupOutMapper {
  data = {} as AdminGroup;
  nodeVirtual: any[] = [];

  constructor(remoteData: AdminGroupDTO, private attributesTransform = false) {
    this.data = {
      id: remoteData.identifier,
      name: remoteData.name,
      start_date: remoteData.start_date,
      start_time: remoteData.start_time,
      is_basic: remoteData.is_basic,
      end_date: remoteData.end_date,
      end_time: remoteData.end_time,
      admin_group_type_id: remoteData.admin_group_type_id,
      admin_group_type_name: remoteData.admin_group_type_name,
      active: remoteData.is_active,
      node: remoteData.node && this.transform(remoteData.node as Conditions2DTO & Condition2MultipleDTO),
      result: remoteData.result,
      folder_id: remoteData.folder_id,
    } as AdminGroup;
  }

  public transformTooltip(attribute: Condition2AttributeDTO, field: string): string {
    if (attribute) {
      field = `${field}/${attribute.label}`;
    }

    if (attribute.reference) {
      field = this.transformTooltip(attribute.reference, field);
    }

    return field;
  }

  private transform(
    {
      type,
      attribute,
      label,
      is_recursive_filter,
      is_multiple_cardinality,
      entity_type_reference_id,
      ...item
    }: Conditions2DTO & Condition2MultipleDTO,
    multiple?: boolean,
    parent: string[] = [],
    tooltip_parent?: string
  ): Conditions2 & Condition2Multiple {
    let response = item as any;
    if (type) {
      response.node_type = type;
    }

    if (attribute) {
      response.attribute = this.transformNode(attribute);
      response.tooltip_label = `${tooltip_parent || ''}/${response.attribute.label}`;
      if (this.attributesTransform) {
        const attr = this.getAttribute(attribute);
        response.multiple_cardinality = attr.multiple_cardinality || false;
        response.attribute_label = attr.label || '';
        response.attribute_data_type = (attr.data_type || '') as Condition2SimpleDataTypes;
      }
    }

    if (response.visual_path) {
      response.tooltip_label = response.visual_path;
    }

    if (label !== undefined) {
      response.attribute_label = label || '';
    }

    if (is_recursive_filter !== undefined) {
      response.recursive_filter = is_recursive_filter || false;
    }

    if (response.entity_info) {
      const keys = Object.keys(response.entity_info);
      keys.forEach((index: any) => {
        const entity = response.entity_info[index];
        const keysLabels = Object.keys(entity?.labels);
        entity.entity_name = entity?.labels[keysLabels[0]] || entity.entity_name;
      });
    }

    if (is_multiple_cardinality !== undefined) {
      response.multiple_cardinality = is_multiple_cardinality || false;
    }

    response.entity_reference = entity_type_reference_id;

    response.parent = [...parent];
    if (response.attribute?.identifier && response.nodes && !response.attribute.reference) {
      response.parent.push(response.attribute?.identifier);
    }
    if (response.attribute && response.attribute.reference) {
      response.parent.push(...this.getAttributeParents(response.attribute, [], response.attribute_id));
    }

    response = this.parseNodeVirtual(response, multiple, is_multiple_cardinality);
    if (response.nodes) {
      response.nodes = response.nodes?.map((node: any) => {
        return this.transform(
          node as unknown as Conditions2DTO & Condition2MultipleDTO,
          is_multiple_cardinality || multiple,
          response.parent,
          response.tooltip_label
        ) as unknown as Conditions2Node;
      });
    }

    return response;
  }

  private parseNodeVirtual(response: Conditions2 & Condition2Multiple, multiple?: boolean, is_multiple_cardinality?: boolean) {
    const temp: any = response.node_type ? (response.nodes ? response.nodes[0] : response) : response;

    if (response.multiple_cardinality && response.value) {
      return response;
    }

    if (
      temp.attribute &&
      temp.attribute.reference &&
      !temp.parentVirtual &&
      !temp.attribute.multiple_cardinality &&
      // temp.nodes &&
      (multiple || is_multiple_cardinality)
    ) {
      temp.parentVirtual = true;
      const findParent = this.nodeVirtual.find((v) => v.attribute_id === temp.attribute.identifier);
      if (findParent) {
        findParent.nodes.push(response);
        // response.parent = [...parent, response.attribute?.identifier];
      } else {
        const tempAtt = {
          ...temp.attribute,
          attribute: temp.attribute,
          parentVirtual: true,
          attribute_label: temp.attribute.label,
          attribute_id: temp.attribute.identifier,
          attribute_data_type: Condition2SimpleDataTypes.ENTITY,
          id: temp.attribute.identifier,
          tooltip_label: `/${temp.attribute.label}`,
        };
        if (response.multiple_cardinality) {
          const t = {
            ...tempAtt,
            nodes: response.nodes,
          };
          response.nodes = [t];
        } else {
          response = {
            ...tempAtt,
            nodes: [response],
          };
        }
        this.nodeVirtual.push(response);
      }

      if (response.attribute.reference?.reference && !response.multiple_cardinality) {
        const t = {
          ...response.attribute.reference,
          attribute: response.attribute.reference,
          parentVirtual: true,
          attribute_label: response.attribute.reference.label,
          attribute_id: response.attribute.reference.identifier,
          attribute_data_type: Condition2SimpleDataTypes.ENTITY,
          id: response.attribute.reference.identifier,
          tooltip_label: `/${response.attribute.reference.label}`,
          nodes: response.nodes,
        };
        const node = this.parseNodeVirtual(t as any, multiple, is_multiple_cardinality) as any;
        node.nodes = response.nodes;
        response.nodes = [node];
      } else if (temp.attribute.reference?.reference) {
        const t = {
          ...temp.attribute.reference,
          attribute: temp.attribute.reference,
          parentVirtual: true,
          attribute_label: temp.attribute.reference.label,
          attribute_id: temp.attribute.reference.identifier,
          attribute_data_type: Condition2SimpleDataTypes.ENTITY,
          id: temp.attribute.reference.identifier,
          tooltip_label: `/${temp.attribute.reference.label}`,
          nodes: [temp.nodes],
        };
        const node = this.parseNodeVirtual(t, multiple, is_multiple_cardinality) as any;
        node.nodes = [temp];
        if (response.nodes && response.nodes.length && response.nodes[0].nodes) {
          response.nodes[0].nodes = [node];
        }
      }
    }
    return response;
  }

  private transformNode({
    is_label_attribute,
    is_multiple_cardinality,
    is_recursive_filter,
    entity_type_reference_id,
    ...item
  }: Condition2AttributeDTO): Condition2Attribute {
    const response = item as Condition2Attribute;

    if (is_label_attribute !== undefined) {
      response.attribute_label = is_label_attribute;
    }

    if (is_recursive_filter !== undefined) {
      response.recursive_filter = is_recursive_filter || false;
    }

    if (is_multiple_cardinality !== undefined) {
      response.multiple_cardinality = is_multiple_cardinality;
    }

    response.entity_reference = entity_type_reference_id;

    return response;
  }

  private getAttributeParents(attribute: Condition2Attribute, arr: string[], attribute_id: string): string[] {
    if (attribute.identifier === attribute_id) {
      return arr;
    }
    arr.push(attribute.identifier);

    if (attribute.reference && attribute.identifier !== attribute_id) {
      this.getAttributeParents(attribute.reference, arr, attribute_id);
    }
    return arr;
  }

  private getAttribute(attribute: Condition2Attribute): Condition2Attribute {
    if (!this.attributesTransform) {
      return attribute;
    }

    let attr = attribute;
    if (attribute.reference) {
      attr = this.getAttribute(attribute.reference);
    }

    return attr;
  }
}
