import { AdminGroup, AdminGroupDTO } from '../../models/admin-group.model';
import { ConditionsDTO } from './../../../../modules/conditions2/models/conditions2.model';

export class AdminGroupInMapper {
  data = {} as AdminGroupDTO;

  constructor(remoteData: AdminGroup) {
    this.data = {
      identifier: remoteData.id,
      name: remoteData.name,
      start_date: remoteData.start_date,
      start_time: remoteData.start_time,
      end_date: remoteData.end_date,
      end_time: remoteData.end_time,
      admin_group_type_id: remoteData.admin_group_type_id,
      admin_group_type_name: remoteData.admin_group_type_name,
      is_active: remoteData.active,
      is_basic: remoteData.is_basic,
      node: remoteData.node && this.transform(remoteData.node),
      result: remoteData.result,
      folder_id: remoteData.folder_id,
    } as AdminGroupDTO;
  }

  private transform({ node_type, recursive_filter, ...item }: any): ConditionsDTO {
    const response = item;
    if (node_type) {
      response.type = node_type;
    }

    if (recursive_filter) {
      response.is_recursive_filter = recursive_filter;
    }

    if (item.nodes) {
      response.nodes = response.nodes?.map((node: any) => {
        return this.transform(node);
      });
    }

    return {
      attribute: response.attribute,
      type: response.type || response.node_type,
      value: response.value,
      visual_path: response.tooltip_label || response.visual_path,
      reference_id: response.reference_id,
      is_recursive_filter: response.is_recursive_filter || response.recursive_filter,
      nodes: response.nodes,
    };
  }
}
