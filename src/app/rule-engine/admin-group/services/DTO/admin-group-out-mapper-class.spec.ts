import { AdminGroupDTO } from '../../models/admin-group.model';
import { AdminGroupOutMapper } from './admin-group-out-mapper-class';

describe('content-group mapper out', () => {
  it('parse a outgoing request as expected', () => {
    const remoteItem: AdminGroupDTO = ({
      admin_group_type_id: 'content_group',
      admin_group_type_name: 'Content Group',
      end_date: '2020-05-02T01:00Z',
      identifier: '0981988a-8988-4c2b-b8ff-8bc7c20688df',
      is_active: true,
      is_manageable: true,
      name: 'AdminGroup_TV_LG_2',
      node: {
        type: 'eq',
        attribute: {
          identifier: '27d8bb5b-97bb-44ac-8dfa-3a56c20813a3',
          multiple_cardinality: false,
          is_i18n: false,
          label: 'Volume Limit',
          name: 'volume_limit',
          label_attribute: false,
          data_type: 'DOUBLE',
        },
        value: 7.5,
      },
      result: 'Correct_result',
      start_date: '2020-05-02T01:00Z',
    } as unknown) as AdminGroupDTO;

    const mapper = new AdminGroupOutMapper(remoteItem);
    const group = mapper.data;
    expect(group.end_date).toEqual('2020-05-02T01:00Z');
    expect(group.admin_group_type_id).toEqual('content_group');
    expect(group.admin_group_type_name).toEqual('Content Group');
    expect(group.id).toEqual('0981988a-8988-4c2b-b8ff-8bc7c20688df');
    expect(group.active).toEqual(true);
    expect(group.node?.node_type).toEqual('eq');
    expect(group.name).toEqual('AdminGroup_TV_LG_2');
    expect(group.result).toEqual('Correct_result');
    expect(group.start_date).toEqual('2020-05-02T01:00Z');
  });

  it('parse an incoming group with some fields null or undefined does not break anything', () => {
    const remoteItem: AdminGroupDTO = ({
      end_date: undefined,
      folder_id: undefined,
      identifier: null,
      is_active: null,
      node: { type: undefined },
    } as unknown) as AdminGroupDTO;

    const mapper = new AdminGroupOutMapper(remoteItem);
    const group = mapper.data;
    expect(group.end_date).toEqual(undefined);
    expect(group.node?.node_type).toEqual(undefined);
    expect(group.id).toEqual(null);
    expect(group.active).toEqual(null);
  });
});
