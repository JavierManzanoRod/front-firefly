import { AdminGroup } from '../../models/admin-group.model';
import { AdminGroupInMapper } from './admin-group-in-mapper-class';

describe('Admin-group mapper in', () => {
  it('parse an incoming admin group as expected', () => {
    const remoteItem: AdminGroup = ({
      admin_group_type_id: 'content_group',
      admin_group_type_name: 'Content Group',
      end_date: '2020-05-02T01:00Z',
      id: '0981988a-8988-4c2b-b8ff-8bc7c20688df',
      active: true,
      is_manageable: true,
      name: 'AdminGroup_TV_LG_2',
      node: {
        node_type: 'eq',
      },
      result: 'Correct_result',
      start_date: '2020-05-02T01:00Z',
    } as unknown) as AdminGroup;

    const mapper = new AdminGroupInMapper(remoteItem);
    const group = mapper.data;
    expect(group.end_date).toEqual('2020-05-02T01:00Z');
    expect(group.admin_group_type_id).toEqual('content_group');
    expect(group.admin_group_type_name).toEqual('Content Group');
    expect(group.identifier).toEqual('0981988a-8988-4c2b-b8ff-8bc7c20688df');
    expect(group.is_active).toEqual(true);
    expect(group.node?.type).toEqual('eq');
    expect(group.name).toEqual('AdminGroup_TV_LG_2');
    expect(group.result).toEqual('Correct_result');
    expect(group.start_date).toEqual('2020-05-02T01:00Z');
  });

  it('parse an incoming group with some fields null or undefined does not breaks anything', () => {
    const remoteItem: AdminGroup = ({
      end_date: undefined,
      folder_id: undefined,
      id: null,
      active: null,
      node: { type: undefined },
    } as unknown) as AdminGroup;

    const mapper = new AdminGroupInMapper(remoteItem);
    const group = mapper.data;
    expect(group.end_date).toEqual(undefined);
    expect(group.node?.type).toEqual(undefined);
    expect(group.identifier).toEqual(null);
    expect(group.is_active).toEqual(null);
  });
});
