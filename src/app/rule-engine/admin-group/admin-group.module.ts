import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { SortablejsModule } from 'ngx-sortablejs';
import { SimplebarAngularModule } from 'simplebar-angular';
import { Conditions2Module } from 'src/app/modules/conditions2/conditions2.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { RulesHeaderFormModule } from 'src/app/modules/rules-header-form/rules-header-form.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModalModule } from '../../modules/common-modal/common-modal.module';
import { SelectModule } from '../../modules/select/select.module';
import { AdminGroupRoutingModule } from './admin-group-routing.module';
import { AdminGroupDetailComponent } from './components/admin-group-detail/admin-group-detail.component';
import { AdminGroupDetailContainerComponent } from './containers/admin-group-detail-container';
import { AdminGroupListContainerComponent } from './containers/admin-group-list-container';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminGroupRoutingModule,
    RulesHeaderFormModule,
    CommonModalModule,
    NgSelectModule,
    SelectModule,
    AccordionModule.forRoot(),
    SortablejsModule.forRoot({ animation: 150 }),
    Conditions2Module,
    SimplebarAngularModule,
    GenericListComponentModule,
    CoreModule,
    TranslateModule,
  ],
  declarations: [AdminGroupListContainerComponent, AdminGroupDetailContainerComponent, AdminGroupDetailComponent],
  providers: [DatePipe],
  exports: [AdminGroupDetailComponent],
})
export class AdminGroupModule {}
