import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RuleEngineRoutingModule } from './rule-engine-routing.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, RuleEngineRoutingModule],
  declarations: [],
  providers: [],
  exports: [],
})
export class RuleEngineModule {}
