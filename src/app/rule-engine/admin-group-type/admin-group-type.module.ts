import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChipsControlModule } from '../../modules/chips-control/chips-control.module';
import { GenericListComponentModule } from '../../modules/generic-list/generic-list.module';
import { AdminGroupTypeRoutingModule } from './admin-group-type-routing.module';
import { AdminGroupTypeAttributeComponent } from './components/admin-group-type-attribute/admin-group-type-attribute.component';
import { AdminGroupTypeDetailComponent } from './components/admin-group-type-detail/admin-group-type-detail.component';
import { AdminGroupTypeDetailContainerComponent } from './containers/admin-group-type-detail-container';
import { AdminGroupTypeListContainerComponent } from './containers/admin-group-type-list-container';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminGroupTypeRoutingModule,
    CommonModalModule,
    NgSelectModule,
    SimplebarAngularModule,
    CoreModule,
    GenericListComponentModule,
    TranslateModule,
    ChipsControlModule,
  ],
  declarations: [
    AdminGroupTypeListContainerComponent,
    AdminGroupTypeDetailContainerComponent,
    AdminGroupTypeDetailComponent,
    AdminGroupTypeAttributeComponent,
  ],
  exports: [AdminGroupTypeDetailComponent],
})
export class AdminGroupTypeModule {}
