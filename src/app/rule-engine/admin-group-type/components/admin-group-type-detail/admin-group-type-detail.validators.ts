import { FormGroup } from '@angular/forms';

export function oneHasValue(group: FormGroup) {
  const values = group.value;
  let valid = false;
  let validValues = 0;

  Object.keys(values).forEach((key) => {
    if (values[key]) {
      validValues++;
    }
  });

  if (!validValues || validValues === Object.keys(values).length) {
    valid = true;
  }

  Object.keys(values).forEach((key) => {
    group.controls[key].setErrors(values[key] || valid ? null : { required: true });
  });

  return null;
}
