import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { ChipsControlSelectConfig, ModalChipsTypes } from '../../../../modules/chips-control/components/chips.control.model';
import { UiEntityTypesSelectorsMultipleItem } from '../../../../modules/entity-types-selectors/components/ui-entity-types-selectors-multiple/ui-entity-types-selectors-multiple.model';
import { AdminGroupType, AdminGroupTypeToPost } from '../../models/admin-group-type.model';
import { oneHasValue } from './admin-group-type-detail.validators';

@Component({
  selector: 'ff-admin-group-type-detail-component',
  styleUrls: ['admin-group-type-detail.component.scss'],
  templateUrl: 'admin-group-type-detail.component.html',
})
export class AdminGroupTypeDetailComponent implements OnChanges, OnInit {
  @Input() loading!: boolean;
  @Input() item!: AdminGroupType;
  @Input() readonly = false;
  @Output() formSubmit: EventEmitter<AdminGroupTypeToPost> = new EventEmitter<AdminGroupTypeToPost>();
  @Output() delete: EventEmitter<AdminGroupTypeToPost> = new EventEmitter<AdminGroupTypeToPost>();
  form: FormGroup;
  sended = false;
  isManageable = false;

  chipsConfig: ChipsControlSelectConfig = {
    modalType: ModalChipsTypes.entitiesAtributes,
    label: 'ADMIN_GROUP_TYPE.ATTRIBUTES_LABEL',
    modalConfig: {
      okBtnText: 'ADMIN_GROUP_TYPE.ENTITY_ATTRIBUTES_MODAL.SELECT',
      cancelBtnText: 'ADMIN_GROUP_TYPE.ENTITY_ATTRIBUTES_MODAL.CANCEL',
      titleText: 'ADMIN_GROUP_TYPE.ENTITY_ATTRIBUTES_MODAL.TITLE',
    },
  };

  constructor(public activeRoute: ActivatedRoute, private fb: FormBuilder, protected router: Router) {
    this.form = this.fb.group({
      // eslint-disable-next-line @typescript-eslint/unbound-method
      name: [null, Validators.required],
      description: [null],
      manageable: [this.isManageable],
      entity_types: [[], Validators.required],
      result_types: this.fb.array([]),
    });
  }

  ngOnInit() {
    if (this.readonly) {
      this.form.disable();
    }
  }

  ngOnChanges() {
    if (this.item && Object.keys(this.item).length) {
      this.form.patchValue({
        name: this.item.name,
        description: this.item.description,
        manageable: this.item.manageable,
      });
      this.isManageable = Boolean(this.item.manageable);
      this.removeItemsResultTypes();
      const arrayKeyValues = Object.keys(this.item?.result_types || {});
      this.buildResultTypes(arrayKeyValues, this.isManageable);

      const entity_types_values: UiEntityTypesSelectorsMultipleItem[] = [];
      for (const entityType of this.item.entity_types) {
        entityType.attributes.forEach((attr: AttributeEntityType) => {
          entity_types_values.push({ ...attr, parents: [entityType as any] });
        });
      }
      this.entity_types.setValue(entity_types_values);
      /*

      for (const entityType of this.item.entity_types) {
        this.entity_types.push(this.fb.control(entityType, [emptyAdminGroupTypeAttribute()]));
      }
      this.entity_types.push(this.fb.control(null, [emptyAdminGroupTypeAttribute()]));
      this.entity_types.setValidators(adminGroupDetailSomeAttribute);

       */
    }
  }

  addEntity() {
    this.entity_types.push(this.fb.control(null));
  }

  deleteEntity(index: number) {
    if (this.entity_types.length > 1) {
      this.entity_types.removeAt(index);
    } else {
      this.entity_types.removeAt(index);
      this.entity_types.push(this.fb.control(null));
    }
  }

  get entity_types() {
    return this.form && (this.form.get('entity_types') as FormArray);
  }

  addItemResultTypes() {
    this.result_types.push(
      this.fb.group(
        {
          key: [null],
          value: [null],
        },
        { validator: oneHasValue }
      )
    );
  }

  removeItemResultType(i: number) {
    this.result_types.removeAt(i);
    this.form.markAsDirty();
  }

  removeItemsResultTypes() {
    while (this.result_types.length !== 0) {
      this.result_types.removeAt(0);
    }
  }

  buildResultTypes(arrayResultType: any[], manageable: boolean) {
    for (const key of arrayResultType) {
      this.result_types.push(
        this.fb.group(
          {
            key: [key],
            value: [this.item.result_types[key]],
          },
          { validator: oneHasValue }
        )
      );
    }

    if (!arrayResultType.length && manageable) {
      this.addItemResultTypes();
    }
  }

  get result_types() {
    return this.form && (this.form.get('result_types') as FormArray);
  }

  toggleResultTypes() {
    this.isManageable = !this.isManageable;
  }

  handleDelete() {
    this.delete.emit(this.getDataToSend());
  }

  stringify(obj: any) {
    const cache: any = [];
    return JSON.stringify(obj, (key, value) => {
      if (typeof value === 'object' && value !== null) {
        // Duplicate reference found, discard key
        if (cache.includes(value)) {
          return;
        }

        // Store value in our collection
        cache.push(value);
      }
      return value;
    });
  }

  getDataToSend(): AdminGroupTypeToPost {
    const formValue: any = this.form.value;
    const accumulator: AdminGroupTypeToPost['entity_types'] = {};

    let key: string;
    let item: any;
    for (const entityType of formValue.entity_types) {
      key = entityType.parents[0].id as string;
      item = JSON.parse(this.stringify(entityType));
      delete item.parents;
      if (!accumulator[key]) {
        accumulator[key] = [];
      }
      accumulator[key].push(item.id);
    }

    const newResultType: any = {};
    for (const resultType of this.result_types.value) {
      if (resultType.key) {
        newResultType[resultType.key] = resultType.value;
      }
    }

    const dataToSend: AdminGroupTypeToPost = {
      id: this.item.id,
      name: formValue.name,
      description: formValue.description,
      entity_types: accumulator,
      manageable: formValue.manageable,
      result_types: newResultType,
    };
    return dataToSend;
  }

  submit() {
    this.form.markAllAsTouched();
    this.entity_types.updateValueAndValidity();
    this.form.updateValueAndValidity();
    this.sended = true;
    if (this.form.valid) {
      this.formSubmit.emit(this.getDataToSend());
    }
    this.form.markAsPristine();
    return false;
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.activeRoute.parent });
  }
}
