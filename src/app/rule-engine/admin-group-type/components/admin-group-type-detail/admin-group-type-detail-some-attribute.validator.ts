import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { EntityList } from '@core/models/attribute.model';

export const adminGroupDetailSomeAttribute: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const value = control.value as EntityList[];
  if (value && value.length) {
    const isValid = value.some((e) => e && e.name);
    if (isValid) {
      return null;
    }
  }
  return { errorSomeAtt: true };
};
