import { FormBuilder } from '@angular/forms';
import { AttributeEntityType } from '@core/models/entity-type.model';
import { AdminGroupType } from '../../models/admin-group-type.model';
import { AdminGroupTypeDetailComponent } from './admin-group-type-detail.component';

describe('AdminGroupTypeDetailComponent', () => {
  it('it should create ', () => {
    const builder = new FormBuilder();
    const comp = new AdminGroupTypeDetailComponent(null as any, builder, null as any);
    expect(comp).toBeDefined();
  });

  it('ngChanges initialize the form of the component ', () => {
    const builder = new FormBuilder();
    const att: AttributeEntityType = {
      name: 'attname',
      data_type: 'STRING',
      label: 'label',
    };
    const item: AdminGroupType = {
      name: 'name',
      description: '',
      manageable: true,
      entity_types: [
        {
          id: '1',
          name: 'nameEntityType',
          attributes: [att, att],
          is_master: false,
        },
      ],
      result_types: {
        result: 'STRING',
      },
    };
    const comp = new AdminGroupTypeDetailComponent(null as any, builder, null as any);
    comp.item = item;
    comp.ngOnChanges();
    console.log(comp.form);
    expect(comp.form.controls.name.value).toBe(item.name);
    expect(comp.form.controls.entity_types.value.length).toBe(1 + item.entity_types.length);
  });
});
