import { AbstractControl } from '@angular/forms';
import { emptyAdminGroupTypeAttribute } from './admin-group-type-attribute.validator';

describe('emptyAdminGroupTypeAttribute', () => {
  it('returns valid (null) with attributes', () => {
    const validatorToTest = emptyAdminGroupTypeAttribute();
    const mockControl = { value: { id: 'id', name: 'name', attributes: [1, 2] } } as AbstractControl;
    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeNull();
  });

  it('return some error with no atts', () => {
    const validatorToTest = emptyAdminGroupTypeAttribute();
    const mockControl = { value: { id: 'id', name: 'name', attributes: [] } } as AbstractControl;

    const isValid = validatorToTest(mockControl);
    expect(isValid).toBeTruthy();
  });
});
