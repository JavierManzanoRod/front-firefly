import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EntityList } from '@core/models/attribute.model';
import { MayHaveIdName } from '@model/base-api.model';
import { LoadingComponent } from 'src/app/modules/loading/loading.component';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { AdminGroupTypeAttributeComponent } from './admin-group-type-attribute.component';

describe('AdminGroupTypeAttributeComponent', () => {
  let component: LoadingComponent;
  let fixture: ComponentFixture<LoadingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoadingComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingComponent);
    fixture.detectChanges();
  });

  it('should create', () => {
    const comp = new AdminGroupTypeAttributeComponent((null as unknown) as EntityTypeService);
    comp.ngOnInit();
    comp.onChangeEntity(null);
    comp.onChangeSelectAttribute();
    expect(comp).toBeDefined();
  });

  it('traker of the candidates is the id', () => {
    const comp = new AdminGroupTypeAttributeComponent((null as unknown) as EntityTypeService);
    const item = { id: '1' } as MayHaveIdName;
    const id = comp.trackByFn(item);
    expect(id).toBe(item.id);
  });

  it('removeAddSelected', () => {
    const comp = new AdminGroupTypeAttributeComponent((null as unknown) as EntityTypeService);
    comp.entity = { id: 'id', attributes: [{ id: 'idAtt' }] } as EntityList;
    comp.removeAddSelected(0);
    expect(comp.availableAttributes?.length).toBe(1);
  });

  it('selectAll', () => {
    const comp = new AdminGroupTypeAttributeComponent((null as unknown) as EntityTypeService);
    comp.entity = { id: 'id', attributes: [{ id: 'idAtt' }] } as EntityList;
    comp.selectAll();
    expect(comp.entity.attributes?.length).toBe(1);
  });
});
