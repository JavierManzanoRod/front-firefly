import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Attribute, EntityList } from '@core/models/attribute.model';
import { EntityType } from '@core/models/entity-type.model';
import { MayHaveIdName } from '@model/base-api.model';
import { concat, Observable, of, Subject } from 'rxjs';
import { catchError, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';

@Component({
  selector: 'ff-admin-group-type-attribute',
  templateUrl: 'admin-group-type-attribute.component.html',
  styleUrls: ['./admin-group-type-search.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AdminGroupTypeAttributeComponent),
      multi: true,
    },
  ],
})
export class AdminGroupTypeAttributeComponent implements OnInit {
  @Input() showDelete!: boolean;
  @Input() showAdd!: boolean;

  @Output() add: EventEmitter<any> = new EventEmitter();
  @Output() delete: EventEmitter<any> = new EventEmitter();

  item$!: Observable<EntityType[]>;
  itemLoading = false;
  itemInput$ = new Subject<string>();
  entity!: EntityList;
  availableAttributes: EntityList['attributes'] = [];

  tmpSelectedAttribute: Attribute | undefined;

  constructor(protected apiEntityTypes: EntityTypeService) {}

  ngOnInit() {
    this.loadItems();
  }

  trackByFn(item: MayHaveIdName) {
    return item.id;
  }

  onChangeEntity($event: any) {
    if ($event) {
      this.entity = {
        ...$event,
        attributes: [],
      };
      this.availableAttributes = $event.attributes;
    }
    this.writeValue(this.entity, false);
  }

  onChangeSelectAttribute() {
    if (this.tmpSelectedAttribute) {
      this.entity.attributes?.push(this.tmpSelectedAttribute);
      const indexToDelete = this.availableAttributes?.findIndex((att) => att.id === this.tmpSelectedAttribute?.id);
      if (typeof indexToDelete === 'number' && indexToDelete > -1) {
        this.availableAttributes?.splice(indexToDelete, 1);
      }
      this.writeValue(this.entity, false);
    }
    this.tmpSelectedAttribute = undefined;
  }

  removeAddSelected(index: number) {
    const removedItem = this.entity.attributes?.splice(index, 1);
    this.availableAttributes?.push(...(removedItem || []));
    this.writeValue(this.entity, false);
    this.tmpSelectedAttribute = undefined;
  }

  selectAll() {
    this.entity.attributes = [...(this.entity.attributes || []), ...clone(this.availableAttributes || [])];

    this.availableAttributes = [];
  }

  writeValue(data: EntityList, updateSelect = true): void {
    this.entity = data;
    if (updateSelect && data && data.id) {
      this.apiEntityTypes.detail(data.id).subscribe((entityBackend) => {
        this.availableAttributes = [];
        entityBackend.attributes.forEach((attBackEnd) => {
          const index = data.attributes?.findIndex((att2) => att2.id === attBackEnd.id);
          if (index === -1) {
            this.availableAttributes?.push(attBackEnd);
          }
        });
      });
    }

    this.onChange(data);
  }

  addNewEntity() {
    this.add.emit();
  }

  deleteEntity() {
    this.delete.emit();
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  // Allows Angular to register a function to call when the input has been touched.
  // Save the function as a property to call later here.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  registerOnTouched(fn: () => void): void {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange = (data: any) => {
    // don't delete!
  };

  private loadItems() {
    this.item$ = concat(
      of([]), // default items
      this.itemInput$.pipe(
        distinctUntilChanged(),
        tap(() => (this.itemLoading = true)),
        switchMap((term) => {
          return this.apiEntityTypes.search(term).pipe(
            catchError(() => of([])), // empty list on error
            tap(() => (this.itemLoading = false))
          );
        })
      )
    );
  }
}

function clone<T>(x: T): T {
  return JSON.parse(JSON.stringify(x)) as T;
}
