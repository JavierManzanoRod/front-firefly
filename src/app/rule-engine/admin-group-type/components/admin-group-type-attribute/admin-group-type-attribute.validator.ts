import { AbstractControl, ValidatorFn } from '@angular/forms';
import { EntityList } from '@core/models/attribute.model';

export function emptyAdminGroupTypeAttribute(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const val = control.value as EntityList;
    const hasToValidate = val && val.name;
    let result = null;
    if (hasToValidate) {
      if (val?.attributes?.length) {
        result = null;
      } else {
        result = { emptyAtts: true };
      }
    }
    return result;
  };
}
