import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { AdminGroupTypeDetailContainerComponent } from './containers/admin-group-type-detail-container';
import { AdminGroupTypeListContainerComponent } from './containers/admin-group-type-list-container';

const routes: Routes = [
  {
    path: '',
    component: AdminGroupTypeListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'new',
        component: AdminGroupTypeDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
          breadcrumb: [
            {
              label: 'SHIPPING_COST.BREADCRUMB_LIST_TITLE',
              url: '/rule-engine/admin-group-type',
            },
            {
              label: 'SHIPPING_COST.BREADCRUMB_ADD_NEW',
              url: '',
            },
          ],
        },
      },
      {
        path: ':id',
        component: AdminGroupTypeDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
          breadcrumb: [
            {
              label: 'SHIPPING_COST.BREADCRUMB_LIST_TITLE',
              url: '/rule-engine/admin-group-type',
            },
            {
              label: 'SHIPPING_COST.SHIPPING_COST',
              url: '',
            },
          ],
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminGroupTypeRoutingModule {}
