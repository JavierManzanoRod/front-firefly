import { EntityTypeDTO } from '@core/models/entity-type.dto';
import { CustomFieldSetConfig } from './admin-group-type.model';

export interface AdminGroupTypeDTO {
  identifier?: string;
  name: string;
  description: string;
  is_manageable?: boolean;
  entity_types: EntityTypeDTO[];
  result_types: CustomFieldSetConfig;
}

export interface AdminGroupTypeToPostDTO {
  identifier?: string;
  name: string;
  description: string;
  is_manageable?: boolean;
  entity_types: {
    [key: string]: string[];
  };
  result_types?: {
    [key: string]: 'DOUBLE' | 'STRING' | 'BOOLEAN';
  };
}

export interface AdminGroupTypePeekedDTO {
  identifier?: string;
  name: string;
  description: string;
  is_manageable?: boolean;
  entity_types: { [key: string]: string[] };
  result_types: CustomFieldSetConfig;
}
