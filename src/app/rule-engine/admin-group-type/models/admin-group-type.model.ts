import { GenericApiResponse } from '@model/base-api.model';
import { EntityType } from '../../../core/models/entity-type.model';

export type CustomFieldSetValueDefinition = 'DOUBLE' | 'STRING' | 'BOOLEAN' | 'ENTITY';

export interface CustomFieldSetConfig {
  [key: string]: CustomFieldSetValueDefinition;
}

export interface CustomFieldSetValue {
  [key: string]: number | string | boolean;
}

export interface AdminGroupType {
  id?: string;
  name: string;
  description: string;
  manageable?: boolean;
  entity_types: EntityType[];
  result_types: CustomFieldSetConfig;
  admin_group_type_id?: string;
}

export interface AdminGroupTypePeeked {
  id?: string;
  name: string;
  description: string;
  manageable?: boolean;
  entity_types: { [key: string]: string[] };
  result_types: CustomFieldSetConfig;
}

export interface AdminGroupTypeToPost {
  id?: string;
  name: string;
  description: string;
  manageable?: boolean;
  entity_types: {
    [key: string]: string[];
  };
  result_types?: {
    [key: string]: 'DOUBLE' | 'STRING' | 'BOOLEAN';
  };
}

export type AdminGroupTypeResponse = GenericApiResponse<AdminGroupType>;
export type AdminGroupTypePeekedResponse = GenericApiResponse<AdminGroupTypePeeked>;
