import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { CrudOperationsService, groupType } from 'src/app/core/services/crud-operations.service';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { AdminGroupTypeDetailComponent } from '../components/admin-group-type-detail/admin-group-type-detail.component';
import { AdminGroupType } from '../models/admin-group-type.model';
import { AdminGroupTypeService } from '../services/admin-group-type.service';

@Component({
  selector: 'ff-admin-group-type-detail-container',
  template: `<ff-page-header
      [pageTitle]="
        id ? ('ADMIN_GROUP_TYPE_DETAIL_CONTAINER.TITLE_DETAIL' | translate) : ('ADMIN_GROUP_TYPE_DETAIL_CONTAINER.TITLE_CREATE' | translate)
      "
      breadcrumbLink="/rule-engine/admin-group-type"
      [breadcrumbTitle]="'ADMIN_GROUP_TYPE_DETAIL_CONTAINER.BREAD_CRUMB_TITLE' | translate"
    >
      <button class="btn btn-dark mr-2" (click)="clone(detail)" *ngIf="id && !readonly" data-cy="agt-clone-button">
        {{ 'COMMON.CLONE' | translate }}
      </button>
      <button class="btn btn-dark mr-2" (click)="delete(detail)" *ngIf="id && !readonly" data-cy="agt-delete-button">
        {{ 'COMMON.DELETE' | translate }}
      </button>
    </ff-page-header>
    <div class="page-container page-container-padding">
      <ff-admin-group-type-detail-component
        (formSubmit)="handleSubmitForm($event)"
        (delete)="delete($event)"
        [loading]="loading"
        [readonly]="readonly"
        [item]="detail$ | async"
      >
      </ff-admin-group-type-detail-component>
    </div> `,
})
export class AdminGroupTypeDetailContainerComponent extends BaseDetailContainerComponent<AdminGroupType> implements OnInit {
  @ViewChild(AdminGroupTypeDetailComponent) type!: AdminGroupTypeDetailComponent;
  @Output() changed = new EventEmitter();
  readonly = true; // de momento no se pueden editar ni borrar ni crear admin group types
  urlListado = '/rule-engine/admin-group-type';

  constructor(
    public activeRoute: ActivatedRoute,
    public apiService: AdminGroupTypeService,
    public router: Router,
    public menuLeft: MenuLeftService,
    public crud: CrudOperationsService,
    public toastService: ToastService
  ) {
    super(crud, menuLeft, activeRoute, router, apiService, toastService);
  }

  handleSubmitForm(itemToSave: AdminGroupType) {
    if (itemToSave) {
      this.crud
        .updateOrSaveModal(
          {
            ...this,
            methodToApply: itemToSave.id ? 'PUT' : 'POST',
          },
          itemToSave,
          groupType.ADMIN_GROUP_TYPE,
          (e: HttpErrorResponse, defaultMessage: string) => {
            if (e.error.error.detail.error_code === 'F-400') {
              return 'ADMIN_GROUP_TYPE.ERROR_F_400';
            } else if (e.error.error.detail.error_code === 'BUS-405') {
              return 'ADMIN_GROUP_TYPE.ERROR_B_405';
            }
            return defaultMessage || 'COMMON_ERRORS.SAVE_ERROR';
          }
        )
        .pipe(
          tap((response) => {
            this.changed.emit(response.data);
            if (!itemToSave.id) {
              this.router.navigate([`${this.urlListado}/${response.data?.id}`]);
            }
          }),
          catchError((e: HttpErrorResponse) => {
            if (e.status === 409) {
              this.type.form.setErrors({ error_409: true });
            }
            return throwError(e);
          })
        )
        .subscribe();
    }
  }

  clone(detail: AdminGroupType | undefined) {
    if (detail) {
      detail = this.type.getDataToSend() as unknown as AdminGroupType;
    }
    if (this.type?.form.dirty) {
      this.toastService.error('COMMON_ERRORS.CANNOT_CLONE');
      return;
    }
    if (this.type?.form?.valid) {
      super.clone(detail, this.urlListado);
    } else {
      this.crudSrv.toast.warning('COMMON_ERRORS.CLONE_FORM_ERROR');
    }
  }
  // si vuelve a ser editable, descomentar
  canDeactivate(): UICommonModalConfig | false {
    /*    if (this.type?.form.dirty) {
      return {
        bodyMessage: this.type.item.name ? 'COMMON_MODALS.UNSAVED_ATTS_ID' : 'COMMON_MODALS.UNSAVED_ATTS_TRANSLATIONS_1',
        name: this.type.item?.name || this.type.form.value,
      };
    } */
    return false;
  }
}
