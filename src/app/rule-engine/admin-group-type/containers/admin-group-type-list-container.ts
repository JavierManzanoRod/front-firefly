import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { tap } from 'rxjs/operators';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from '../../../modules/generic-list/models/generic-list.model';
import { AdminGroupType } from '../models/admin-group-type.model';
import { AdminGroupTypeService } from '../services/admin-group-type.service';

@Component({
  selector: 'ff-admin-group-type-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header pageTitle="{{ 'ADMIN_GROUP_TYPE.HEADER_LIST' | translate }}">
        <a [routerLink]="['/rule-engine/admin-group-type/new']" class="btn btn-primary" *ngIf="!readonly" data-cy="agt-create-button">
          {{ 'ADMIN_GROUP_TYPE_LIST_CONTAINER.BUTTON' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [list]="list$ | async"
                [loading]="loading"
                [canDelete]="canDelete"
                [page]="page"
                [valueSearch]="searchName"
                [currentData]="currentData"
                (delete)="delete($event)"
                (pagination)="handlePagination($event)"
                [type]="type"
                (search)="search($event)"
                [title]="''"
                [isViewOnly]="true"
                [arrayKeys]="arrayKeys"
                [route]="'/rule-engine/admin-group-type/'"
              >
              </ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class AdminGroupTypeListContainerComponent extends BaseListContainerComponent<AdminGroupType> implements OnInit {
  canDelete = false; //read only por el momento
  readonly = true;
  hide = false;
  type = TypeSearch.simple;
  arrayKeys: GenericListConfig[] = [
    {
      key: 'name',
      headerName: 'ADMIN_GROUP_TYPE_LIST.NAME',
    },
  ];

  constructor(public apiService: AdminGroupTypeService, public crud: CrudOperationsService) {
    super(crud, apiService);
  }

  delete(item: AdminGroupType) {
    this.utils
      .deleteActionModal(this.apiService, item, (e: HttpErrorResponse, defaultMessage: string) => {
        if (e.error.error.detail.error_code === 'BUS-405') {
          return 'ADMIN_GROUP_TYPE.ERROR_BUS_405';
        }
        return defaultMessage || 'COMMON_ERRORS.DELETE_ERROR';
      })
      .pipe(
        tap(() => {
          const dataPage = {
            ...this.filter,
            page: this.page?.page_number,
            size: this.page?.page_size,
          };
          this.getDataList(dataPage);
        })
      )
      .subscribe();
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
