import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { AdminGroupTypeService } from '../services/admin-group-type.service';
import { AdminGroupTypeListContainerComponent } from './admin-group-type-list-container';

// starts global mocks

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}
  public get(key: any): any {
    of(key);
  }
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Component({
  selector: 'ff-admin-group-type-list-component',
  template: '<p>Mock Product Editor Component</p>',
})
class MockListComponent {
  @Input() loading!: boolean;
  @Input() list: any;
  @Input() page: any;
  @Input() currentData: any;
}

@Component({
  selector: 'ff-page-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockHeaderComponent {
  @Input() pageTitle: any;
  @Input() breadcrumbTitle: any;
  @Input() breadcrumbLink: any;
}

@Component({
  selector: 'ff-list-search',
  template: '<p>Mock Product Editor Component</p>',
})
class MockRegionSearchComponent {
  @Input() pageTitle: any;
  @Input() label!: string;
}

class ToastrServiceStub {
  public success() {}
}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };
  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }
  public hide() {
    return true;
  }
}

// eslint-disable-next-line prefer-const
let apiSpy: {
  list: jasmine.Spy;
  detail: jasmine.Spy;
  post: jasmine.Spy;
  update: jasmine.Spy;
  delete: jasmine.Spy;
};

// end global mocks

describe('AdminGroupTypeListContainerComponent', () => {
  beforeEach(
    waitForAsync(() => {
      apiSpy = jasmine.createSpyObj('AdminGroupTypeService', ['list', 'detail', 'post', 'update', 'delete']);
      apiSpy.list.and.returnValue(of([{ a: 1 }]));

      TestBed.configureTestingModule({
        declarations: [
          AdminGroupTypeListContainerComponent,
          TranslatePipeMock,
          MockListComponent,
          MockHeaderComponent,
          MockRegionSearchComponent,
        ],
        imports: [RouterTestingModule],
        providers: [
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: ToastrService, useClass: ToastrServiceStub },
          { provide: BsModalService, useClass: ModalServiceMock },
          { provide: AdminGroupTypeService, useValue: apiSpy },
          { provide: CrudOperationsService, useFactory: () => {} },
        ],
      }).compileComponents();
    })
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(AdminGroupTypeListContainerComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should call the proper api.list once after ngOnInit', () => {
    const fixture = TestBed.createComponent(AdminGroupTypeListContainerComponent);
    const component = fixture.debugElement.componentInstance;
    component.ngOnInit();
    expect(apiSpy.list.calls.count()).toBe(1, 'one call');
  });
});
