import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastModule } from '../../../modules/toast/toast.module';
import { AdminGroupType } from '../models/admin-group-type.model';
import { AdminGroupTypeService } from '../services/admin-group-type.service';
import { AdminGroupTypeDetailContainerComponent } from './admin-group-type-detail-container';

const textToShowWhenError = 'Se ha producido un error inesperado: el sistema no ha podido guardar el registro.';
const textToShowWhenError409 = 'Error, el registro está siendo usado en otra entidad';

// starts global mocks

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}

  public get(key: any): any {
    of(key);
  }
}

class ToastrServiceStub {
  public success() {}
}

class BsModalServiceStub {}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Component({
  selector: 'ff-page-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockHeaderComponent {
  @Input() pageTitle: any;
  @Input() breadcrumbTitle: any;
  @Input() breadcrumbLink: any;
}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };

  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }

  public hide() {
    return true;
  }
}

@Component({
  selector: 'ff-admin-group-type-detail-component',
  template: '<p>Mock Product Editor Component</p>',
})
class MockDetailComponent {
  @Input() loading!: boolean;
  @Input() item: any;
  @Input() isFromDestination!: boolean;
}

describe('AdminGroupTypeDetailContainerComponent', () => {
  let router: Router;
  let itemServiceSpy: jasmine.SpyObj<AdminGroupTypeService>;
  beforeEach(
    waitForAsync(() => {
      const spy: jasmine.SpyObj<AdminGroupTypeService> = jasmine.createSpyObj('AdminGroupTypeService', ['update', 'post', 'delete']);
      TestBed.configureTestingModule({
        declarations: [AdminGroupTypeDetailContainerComponent, MockDetailComponent, TranslatePipeMock, MockHeaderComponent],
        imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule, ToastModule],
        providers: [
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: ToastrService, useClass: ToastrServiceStub },
          { provide: BsModalService, useClass: ModalServiceMock },
          { provide: AdminGroupTypeService, useValue: spy },
          { provide: CrudOperationsService, useFactory: () => {} },
        ],
      }).compileComponents();

      router = TestBed.inject(Router);

        itemServiceSpy = TestBed.get(AdminGroupTypeService);
      itemServiceSpy.update.and.returnValue(of(null as any));
      itemServiceSpy.delete.and.returnValue(of(null as any));
    })
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(AdminGroupTypeDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should has loading setted to true when is created', () => {
    const fixture = TestBed.createComponent(AdminGroupTypeDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const loading = component.loading;
    expect(loading).toBeTruthy();
  });

   xit('should handle updates existing items', () => {
    itemServiceSpy.update.and.returnValue(of({ status: 200, data: null as any }));
    const fixture = TestBed.createComponent(AdminGroupTypeDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const data: AdminGroupType = {
      name: 'name',
      id: '1',
      description: 'description',
      entity_types: [],
      result_types: {},
    };
    component.handleSubmitForm(data);
    expect(itemServiceSpy.update.calls.count()).toBe(1, 'spy update method was called once');
  });

  xit('should handle posts of new items and then redirect to the list the list page', () => {
    const navigateSpy = spyOn(router, 'navigate');
    const fixture = TestBed.createComponent(AdminGroupTypeDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    component.apiService.post.and.returnValue(of({ status: 200, data: null }));
    const data: AdminGroupType = {
      name: 'name',
    } as AdminGroupType;
    component.handleSubmitForm(data);
    expect(component.apiService.post.calls.count()).toBe(1, 'spy post method was called once');
    expect(navigateSpy).toHaveBeenCalledWith(['/rule-engine/admin-group-type']);
  });

  xit('should set message error in the modal if some error occurs', () => {
    const fixture = TestBed.createComponent(AdminGroupTypeDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    component.apiService.post.and.returnValue(throwError('error'));
    const data: AdminGroupType = {
      name: 'name',
    } as AdminGroupType;
    component.handleSubmitForm(data);
    expect(component.modalService.errorMessage).toBe(textToShowWhenError, ' message error');
  });

  xit('should set custom message error in the modal if 409 error occurs', () => {
    const fixture = TestBed.createComponent(AdminGroupTypeDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    component.apiService.post.and.returnValue(
      throwError({
        status: 409,
      })
    );
    const item: AdminGroupType = {
      name: 'name',
    } as AdminGroupType;
    component.handleSubmitForm(item);
    expect(component.modalService.errorMessage).toBe(textToShowWhenError409, ' message error');
  });

  xit('should handle deleting existing items', () => {
    const fixture = TestBed.createComponent(AdminGroupTypeDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    component.apiService.delete.and.returnValue(of({ status: 200, data: null }));
    const data: AdminGroupType = {
      name: 'name',
      id: '1',
    } as AdminGroupType;
    const navigateSpy = spyOn(router, 'navigate');
    navigateSpy.and.returnValue(of(true).toPromise());
    component.delete(data);
    expect(component.apiService.delete.calls.count()).toBe(1, 'spy delete method was called once');
  });
});
