import { EntityTypeInMapper } from 'src/app/shared/services/apis/entity-type/dto/entity-type/entity-type-in-mapper.class';
import { AdminGroupTypeDTO } from '../../models/admin-group-type-dto.model';
import { AdminGroupType } from '../../models/admin-group-type.model';

export class AdminGroupTypeOutMapper {
  data = {} as AdminGroupType;

  constructor(remoteData: AdminGroupTypeDTO) {
    this.data = {
      id: remoteData.identifier,
      name: remoteData.name,
      description: remoteData.description,
      manageable: remoteData.is_manageable,
      entity_types:
        remoteData.entity_types instanceof Array
          ? remoteData.entity_types.map((entity) => new EntityTypeInMapper(entity).data)
          : remoteData.entity_types,
      result_types: remoteData.result_types,
    } as AdminGroupType;
  }
}
