import { AdminGroupType } from '../../models/admin-group-type.model';
import { AdminGroupTypeInMapper } from './admin-group-type-in-mapper-class';

describe('Admin-group-type mapper in', () => {
  it('parse an incoming admin group type as expected', () => {
    const remoteItem: AdminGroupType = ({
      description: 'The description of this AdminGroup Type',
      entity_types: {
        additionalProp1: ['string'],
        additionalProp2: ['string'],
        additionalProp3: ['string'],
      },
      id: '0981988a-8988-4c2b-b8ff-8bc7c20688df',
      manageable: true,
      name: 'Content Group',
      result_types: {
        additionalProp1: 'string',
        additionalProp2: 'string',
        additionalProp3: 'string',
      },
    } as unknown) as AdminGroupType;

    const mapper = new AdminGroupTypeInMapper(remoteItem);
    const group = mapper.data;
    expect(group.description).toEqual('The description of this AdminGroup Type');
    expect(group.identifier).toEqual('0981988a-8988-4c2b-b8ff-8bc7c20688df');
    expect(group.is_manageable).toEqual(true);
    expect(group.name).toEqual('Content Group');
  });

  it('parse an incoming group-type with some fields null or undefined does not break anything', () => {
    const remoteItem: AdminGroupType = ({
      description: '',
      id: null,
      manageable: null,
      name: '',
    } as unknown) as AdminGroupType;

    const mapper = new AdminGroupTypeInMapper(remoteItem);
    const group = mapper.data;
    expect(group.identifier).toEqual(null);
    expect(group.description).toEqual('');
    expect(group.is_manageable).toEqual(null);
    expect(group.name).toEqual('');
  });
});
