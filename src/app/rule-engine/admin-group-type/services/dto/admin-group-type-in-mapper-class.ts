import { EntityTypeOutMapper } from 'src/app/shared/services/apis/entity-type/dto/entity-type/entity-type-out-mapper.class';
import { AdminGroupTypeDTO } from '../../models/admin-group-type-dto.model';
import { AdminGroupType } from '../../models/admin-group-type.model';

export class AdminGroupTypeInMapper {
  data = {} as AdminGroupTypeDTO;

  constructor(remoteData: AdminGroupType) {
    this.data = {
      identifier: remoteData.id,
      name: remoteData.name,
      description: remoteData.description,
      is_manageable: remoteData.manageable,
      entity_types:
        remoteData.entity_types instanceof Array
          ? remoteData.entity_types.map((entity) => new EntityTypeOutMapper(entity).data)
          : remoteData.entity_types,
      result_types: remoteData.result_types,
    } as AdminGroupTypeDTO;
  }
}
