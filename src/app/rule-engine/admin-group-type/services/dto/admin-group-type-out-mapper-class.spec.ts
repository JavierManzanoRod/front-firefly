import { AdminGroupTypeDTO } from '../../models/admin-group-type-dto.model';
import { AdminGroupTypeOutMapper } from './admin-group-type-out-mapper-class';

describe('Admin-group-type mapper out', () => {
  it('parse an outgoing admin group type as expected', () => {
    const remoteItem: AdminGroupTypeDTO = ({
      description: 'The description of this AdminGroup Type',
      entity_types: {
        additionalProp1: ['string'],
        additionalProp2: ['string'],
        additionalProp3: ['string'],
      },
      identifier: '0981988a-8988-4c2b-b8ff-8bc7c20688df',
      is_manageable: true,
      name: 'Content Group',
      result_types: {
        additionalProp1: 'string',
        additionalProp2: 'string',
        additionalProp3: 'string',
      },
    } as unknown) as AdminGroupTypeDTO;

    const mapper = new AdminGroupTypeOutMapper(remoteItem);
    const group = mapper.data;
    expect(group.description).toEqual('The description of this AdminGroup Type');
    expect(group.id).toEqual('0981988a-8988-4c2b-b8ff-8bc7c20688df');
    expect(group.manageable).toEqual(true);
    expect(group.name).toEqual('Content Group');
  });

  it('parse an outgoing group-type with some fields null or undefined does not break anything', () => {
    const remoteItem: AdminGroupTypeDTO = ({
      description: '',
      identifier: null,
      is_manageable: null,
      name: '',
    } as unknown) as AdminGroupTypeDTO;

    const mapper = new AdminGroupTypeOutMapper(remoteItem);
    const group = mapper.data;
    expect(group.id).toEqual(null);
    expect(group.description).toEqual('');
    expect(group.manageable).toEqual(null);
    expect(group.name).toEqual('');
  });
});
