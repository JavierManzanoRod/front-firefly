import { HttpClient } from '@angular/common/http';
import { AdminGroupTypeService } from './admin-group-type.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: AdminGroupTypeService;

const version = 'v1/';

describe('AdminGroupTypeService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`AdminGroupTypeService points to products/backoffice-admin-group-types/${version}types`, () => {
    myService = new AdminGroupTypeService(null as any, version, '', '');
    expect(myService.endPoint).toEqual(`products/backoffice-admin-group-types/${version}types`);
  });

  it('AdminGroupTypeService.emptyRecord record gives me some data', () => {
    myService = new AdminGroupTypeService(null as unknown as HttpClient, version, '', '');
    const record = myService.emptyRecord();
    expect(record).toBeDefined();
  });
});
