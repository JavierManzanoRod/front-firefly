import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, IApiSearchable, IApiService2, UpdateResponse } from '@core/base/api.service';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_ALL_ADMIN_GROUP_TYPE, FF_BUNDLE_OF } from 'src/app/configuration/tokens/admin-group-type.token';
import { FF_API_PATH_VERSION_ADMIN_GROUPS_TYPES } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroupTypeDTO } from '../models/admin-group-type-dto.model';
import { AdminGroupType, AdminGroupTypePeeked } from '../models/admin-group-type.model';
import { AdminGroupTypeInMapper } from './dto/admin-group-type-in-mapper-class';
import { AdminGroupTypeOutMapper } from './dto/admin-group-type-out-mapper-class';

@Injectable({
  providedIn: 'root',
})
export class AdminGroupTypeService
  extends ApiService<AdminGroupType>
  implements IApiService2<AdminGroupType>, IApiSearchable<AdminGroupType>
{
  endPoint = `products/backoffice-admin-group-types/${this.version}types`;

  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS_TYPES) private version: string,
    @Inject(FF_BUNDLE_OF) private idBundleOf: string,
    @Inject(FF_ALL_ADMIN_GROUP_TYPE) private allAdminGroupType: string
  ) {
    super();
  }

  search(nameToSearch: string, page?: any): Observable<AdminGroupTypePeeked[]> {
    return this.list({ name: nameToSearch, ...page }).pipe(map(({ content }) => content as unknown as AdminGroupTypePeeked[]));
  }

  detail(id: string, hiddenNodes = false): Observable<AdminGroupType> {
    return super.detail(id).pipe(
      map((detail) => {
        const data = new AdminGroupTypeOutMapper(detail as unknown as AdminGroupTypeDTO).data;
        if (hiddenNodes) {
          this.parseAttributes(data);
        }
        return data;
      })
    );
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<AdminGroupType>> {
    return super.list(filter).pipe(
      map((response) => {
        response.content = response.content.map((r) => new AdminGroupTypeOutMapper(r as unknown as AdminGroupTypeDTO).data);
        return response;
      })
    );
  }

  listWithOutListingAdmin(filter: GenericApiRequest): Observable<GenericApiResponse<AdminGroupType>> {
    return super.list(filter).pipe(
      map((response) => {
        response.content = response.content.map((r) => new AdminGroupTypeOutMapper(r as unknown as AdminGroupTypeDTO).data);
        return response;
      }),
      map((adminType: any) => {
        const content = adminType.content.filter((type: any) => !Object.values(this.allAdminGroupType).includes(type.id));
        return {
          page: adminType.page,
          content,
        };
      })
    );
  }

  emptyRecord(): AdminGroupType {
    return {
      name: '',
      description: '',
      entity_types: [],
      result_types: {},
      manageable: true,
    } as AdminGroupType;
  }

  post(data: AdminGroupType): Observable<UpdateResponse<AdminGroupType>> {
    return super.post(new AdminGroupTypeInMapper(data).data as unknown as AdminGroupType).pipe(
      map((res: UpdateResponse<AdminGroupType>) => {
        return {
          ...res,
          data: new AdminGroupTypeOutMapper(res.data as unknown as AdminGroupTypeDTO).data as unknown as AdminGroupType,
        };
      })
    );
  }

  update(data: AdminGroupType): Observable<UpdateResponse<AdminGroupType>> {
    return super.update(new AdminGroupTypeInMapper(data).data as unknown as AdminGroupType).pipe(
      map((res: UpdateResponse<AdminGroupType>) => {
        return {
          ...res,
          data: new AdminGroupTypeOutMapper(res.data as unknown as AdminGroupTypeDTO).data as unknown as AdminGroupType,
        };
      })
    );
  }

  entityTypes(id: string) {
    return this.detail(id).pipe(
      map((v) => {
        this.parseAttributes(v);
        return v.entity_types;
      })
    );
  }

  private parseAttributes(v: AdminGroupType) {
    v.entity_types?.forEach((value) => {
      value?.attributes?.sort((a, b) => (a.name > b.name ? 1 : -1));
      value.attributes = value.attributes.filter((att) => att.id !== this.idBundleOf);
    });
  }
}
