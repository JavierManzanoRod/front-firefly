import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { PriceInheritanceDetailContainerComponent } from './containers/price-inheritance-detail-container.component';
import { PriceInheritanceListContainerComponent } from './containers/price-inheritance-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: PriceInheritanceListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
      breadcrumb: [
        {
          label: 'PRICE_INHERITANCE.LIST_TITLE',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'list',
        component: PriceInheritanceListContainerComponent,
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          breadcrumb: [
            {
              label: 'PRICE_INHERITANCE.LIST_TITLE',
              url: '',
            },
          ],
        },
      },
      {
        path: 'new',
        component: PriceInheritanceDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'new',
          breadcrumb: [
            {
              label: 'PRICE_INHERITANCE.BREADCRUMB_LIST_TITLE',
              url: '/rule-engine/price-inheritance',
            },
            {
              label: 'PRICE_INHERITANCE.BREADCRUMB_ADD_NEW',
              url: '',
            },
          ],
        },
      },
      {
        path: 'view/:id',
        component: PriceInheritanceDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'view',
          breadcrumb: [
            {
              label: 'PRICE_INHERITANCE.BREADCRUMB_LIST_TITLE',
              url: '/rule-engine/price-inheritance',
            },
            {
              label: 'PRICE_INHERITANCE.PRICE_INHERITANCE',
              url: '',
            },
          ],
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LimitSaleRoutingModule {}
