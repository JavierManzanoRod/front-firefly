import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ControlPanelSharedModule } from 'src/app/control-panel/shared/control-panel-shared.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { ConditionsBasicAndAdvanceModule } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.module';
import { ConditionsBasicModule } from 'src/app/modules/conditions-basic/conditions-basic.module';
import { Conditions2Module } from 'src/app/modules/conditions2/conditions2.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { GenericListComponentModule } from 'src/app/modules/generic-list/generic-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PriceInheritanceDetailContainerComponent } from './containers/price-inheritance-detail-container.component';
import { PriceInheritanceListContainerComponent } from './containers/price-inheritance-list-container.component';
import { LimitSaleRoutingModule } from './price-inheritance-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LimitSaleRoutingModule,
    TabsModule.forRoot(),
    Conditions2Module,
    CommonModalModule,
    DatetimepickerModule,
    ConditionsBasicModule,
    SimplebarAngularModule,
    CoreModule,
    GenericListComponentModule,
    ControlPanelSharedModule,
    ConditionsBasicAndAdvanceModule,
  ],
  declarations: [PriceInheritanceListContainerComponent, PriceInheritanceDetailContainerComponent],
  providers: [],
  exports: [],
})
export class PriceInheritanceModule {}
