import { Component, Inject, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { SortApiType } from '@core/models/sort.model';
import { FF_PRICE_INHERITANCE } from 'src/app/configuration/tokens/admin-group-type.token';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { AdminGroup } from '../../admin-group/models/admin-group.model';
import { PriceInheritanceService } from '../services/price-inheritance.service';

@Component({
  selector: 'ff-price-inheritance-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header [pageTitle]="'PRICE_INHERITANCE.HEADER_LIST' | translate">
        <a [routerLink]="['/rule-engine/price-inheritance/new']" class="btn btn-primary">
          {{ 'PRICE_INHERITANCE.CREATE_RULE' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [list]="list$ | async"
                [loading]="loading"
                [page]="page"
                [checkDateActive]="checkDateActive"
                [valueSearch]="searchName"
                [currentData]="currentData"
                (delete)="delete($event)"
                (pagination)="handlePagination($event)"
                (search)="search($event)"
                [type]="type"
                [title]="''"
                [arrayKeys]="arrayKeys"
                [placeHolder]="'PRICE_INHERITANCE.SEARCH' | translate"
                (sortEvent)="sort($event)"
                [route]="'/rule-engine/price-inheritance/view/'"
              >
              </ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class PriceInheritanceListContainerComponent extends BaseListContainerComponent<AdminGroup> implements OnInit {
  hide = false;
  type = TypeSearch.simple;
  arrayKeys: GenericListConfig[] = [
    {
      key: 'name',
      headerName: 'PRICE_INHERITANCE.NAME',
      showOrder: true,
    },
    {
      key: 'start_date',
      headerName: 'PRICE_INHERITANCE.START_DATE',
      textCenter: true,
      canFormatDate: true,
      formatDate: 'dd-MM-yyyy H:mm',
    },
    {
      key: 'end_date',
      headerName: 'PRICE_INHERITANCE.END_DATE',
      textCenter: true,
      canFormatDate: true,
      formatDate: 'dd-MM-yyyy H:mm',
    },
    {
      key: 'active',
      headerName: 'PRICE_INHERITANCE.ACTIVE',
      textCenter: true,
      canActiveClass: true,
    },
  ];

  constructor(
    public apiService: PriceInheritanceService,
    public crudSrv: CrudOperationsService,
    @Inject(FF_PRICE_INHERITANCE) public typePriceInheritance: string
  ) {
    super(crudSrv, apiService);
  }

  ngOnInit() {
    this.sort({ name: 'name', type: SortApiType.asc });
  }

  delete(item: AdminGroup) {
    return super.delete(item);
  }

  search(criteria: any) {
    return super.search({
      ...criteria,
      type: this.typePriceInheritance,
    });
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
