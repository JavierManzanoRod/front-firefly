import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { FF_PRICE_INHERITANCE } from 'src/app/configuration/tokens/admin-group-type.token';
import { FF_API_PATH_VERSION_ADMIN_GROUPS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroupTypeService } from 'src/app/shared/services/apis/admin-group-type.service';

@Injectable({
  providedIn: 'root',
})
export class PriceInheritanceService extends AdminGroupTypeService {
  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS) protected version: string,
    @Inject(FF_PRICE_INHERITANCE) protected typePriceInheritance: string
  ) {
    super(http, version, typePriceInheritance);
  }
}
