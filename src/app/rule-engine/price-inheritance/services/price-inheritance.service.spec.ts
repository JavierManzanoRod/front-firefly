
import { GenericApiRequest, GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { AdminGroup } from '../../admin-group/models/admin-group.model';
import { PriceInheritanceService } from './price-inheritance.service';


let httpClientSpy: { get: jasmine.Spy; delete: jasmine.Spy; request: jasmine.Spy };

let myService: PriceInheritanceService;

const version = 'v1/';

const expectedData: AdminGroup = {
    id: '1',
    name: 'name',
    start_date: '2020-01-01',
    start_time: '00:00:00',
    is_basic: false,
    end_date: '2020-01-01',
    end_time:  '00:00:00',
    admin_group_type_id: '1',
    admin_group_type_name: 'admin_group_type_name',
    result: 'result',
    active: true,
    node: {}
  } as AdminGroup;
const expectedList: GenericApiResponse<AdminGroup> = {
  content: [expectedData],
  page: (null as unknown) as Page,
};

describe('PriceInheritanceService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'delete', 'request']);
  });

  it('PriceInheritanceService.list calls to get api method', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new PriceInheritanceService(httpClientSpy as any, version, 'id');
    myService.list((null as unknown) as GenericApiRequest).subscribe(() => {});
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('PriceInheritanceService.update handle detail and return one element', () => {
    const rawHttpResponse = { status: 200, body: { name: 'api-response-name' } };
    httpClientSpy.request.and.returnValue(of(rawHttpResponse));
    myService = new PriceInheritanceService(httpClientSpy as any, version, 'id');
    myService.update(expectedData).subscribe((response) => {
      expect(response.status).toEqual(rawHttpResponse.status, 'expected status');
      expect(response.data.name).toEqual(rawHttpResponse.body.name, 'expected status');
    });
    expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
  });

  it('PriceInheritanceService.post handle detail and return one element', () => {
    const rawHttpResponse = { status: 200, body: { name: 'api-response-name' } };
    httpClientSpy.request.and.returnValue(of(rawHttpResponse));
    myService = new PriceInheritanceService(httpClientSpy as any, version, 'id');
    myService.post(expectedData).subscribe((response) => {
      expect(response.status).toEqual(rawHttpResponse.status, 'expected status');
      expect(response.data.name).toEqual(rawHttpResponse.body.name, 'expected status');
    });
    expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
  });

    it('PriceInheritanceService.delete handle detail and return one element', () => {
    httpClientSpy.delete.and.returnValue(of(expectedData));
    myService = new PriceInheritanceService(httpClientSpy as any, version, 'id');
    myService.delete(expectedData).subscribe((response) => {
      expect(response).toEqual(expectedData, 'expected data');
    });
    expect(httpClientSpy.delete.calls.count()).toBe(1, 'one call');
  });

});
