import { Conditions2DTO } from 'src/app/catalog/content-group/model/content-group-dto-model';
export interface PriceInheritanceDTO {
  identifier?: string;
  name: string;
  start_date?: string | Date;
  start_time?: string | Date;
  is_basic: boolean;
  admin_group_type_id?: string;
  end_date?: string | Date;
  end_time?: string | Date;
  is_active: boolean;
  node?: Conditions2DTO;
}
