import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ExplodedTreeModule } from 'src/app/modules/exploded-tree/exploded-tree.module';
import { Pagination2Module } from 'src/app/modules/pagination-2/pagination-2.module';
import { SearchSimpleModule } from 'src/app/modules/search-simple/search-simple.module';
import { SelectModule } from 'src/app/modules/select/select.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModalModule } from '../../modules/common-modal/common-modal.module';
import { FormErrorModule } from '../../modules/form-error/form-error.module';
import { GenericListComponentModule } from '../../modules/generic-list/generic-list.module';
import { TableWithFixedElementsModule } from '../../modules/table-with-fixed-elements/table-with-fixed-elements.module';
import { ToastModule } from '../../modules/toast/toast.module';
import { UiTreeListModule } from '../../modules/ui-tree-list/ui-tree-list.module';
import { EntityModule } from '../entity/entity.module';
import { EntityTypeAddAttributeModalComponent } from './components/entity-type-add-attribute-modal/entity-type-add-attribute-modal.component';
import { EntityTypeDetailAttributeInfoComponent } from './components/entity-type-detail-attribute-info/entity-type-detail-attribute-info.component';
import { EntityTypeDetailAttributeComponent } from './components/entity-type-detail-attribute/entity-type-detail-attribute.component';
import { EntityTypeViewHeaderComponent } from './components/entity-type-view-header/entity-type-view-header.component';
import { EntityTypeListContainerComponent } from './containers/entity-type-list.container';
import { EntityTypeViewContainerComponent } from './containers/entity-type-view.container';
import { EntityTypeRoutingModule } from './entity-type-routing.module';

@NgModule({
  providers: [],
  declarations: [
    EntityTypeListContainerComponent,
    EntityTypeViewContainerComponent,
    EntityTypeViewHeaderComponent,
    EntityTypeDetailAttributeComponent,
    EntityTypeAddAttributeModalComponent,
    EntityTypeDetailAttributeInfoComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EntityTypeRoutingModule,
    Pagination2Module,
    SearchSimpleModule,
    CommonModalModule,
    NgSelectModule,
    AccordionModule.forRoot(),
    EntityModule,
    SelectModule,
    SimplebarAngularModule,
    TabsModule,
    FormErrorModule,
    CommonModalModule,
    UiTreeListModule,
    TableWithFixedElementsModule,
    ToastModule,
    CoreModule,
    GenericListComponentModule,
    ExplodedTreeModule,
  ],
  exports: [EntityTypeDetailAttributeInfoComponent, EntityTypeViewHeaderComponent],
})
export class EntityTypeModule {}
