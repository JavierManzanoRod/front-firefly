import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { EntityType } from '@core/models/entity-type.model';
import { tap } from 'rxjs/operators';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { BUS406 } from 'src/app/shared/services/apis/entity-type/utils/constError';
import { CrudOperationsService } from '../../../core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from '../../../modules/generic-list/models/generic-list.model';
import { ToastService } from '../../../modules/toast/toast.service';

@Component({
  selector: 'ff-entity-type-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header [pageTitle]="'ENTITY_TYPES.HEADER_LIST' | translate">
        <a [routerLink]="['/rule-engine/entity-types/new']" class="btn btn-primary" *ngIf="canDelete">
          {{ 'ADMIN_GROUP_TYPE_LIST.BUTTON' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [route]="'/rule-engine/entity-types/view/'"
                [showPagination]="true"
                [arrayKeys]="listConfigs"
                [loading]="loading"
                [page]="page"
                [valueSearch]="searchName"
                [currentData]="currentData"
                (delete)="delete($event)"
                (pagination)="handlePagination($event)"
                [list]="list$ | async"
                [type]="type"
                [isViewOnly]="true"
                [canDelete]="canDelete"
                [title]="''"
                [placeHolder]="'ENTITY_TYPES.SEARCH_PLACEHOLDER' | translate"
                (search)="search($event)"
              ></ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class EntityTypeListContainerComponent extends BaseListContainerComponent<EntityType> implements OnInit {
  hide = false;
  listConfigs: GenericListConfig[] = [
    { key: 'name', headerName: 'ENTITY_TYPES_LIST.ENTITY_NAME' },
    { key: 'label', headerName: 'COMMON.LABEL' },
    { key: 'attributes.length', headerName: 'ENTITY_TYPES_LIST.NUM_ATTS', textCenter: true },
  ];
  type = TypeSearch.simple;
  canDelete = false; // De momento no podremos modificar ni crear ni eliminar

  constructor(public apiService: EntityTypeService, public crudOperationsService: CrudOperationsService, public toast: ToastService) {
    super(crudOperationsService, apiService);
  }

  delete(item: EntityType) {
    this.utils
      .deleteActionModal(this.apiService, item)
      .pipe(
        tap(() => {
          const dataPage = {
            ...this.filter,
            page: this.page?.page_number,
            size: this.page?.page_size,
          };
          this.getDataList(dataPage);
        })
      )
      .subscribe((result) => {
        if (result?.error) {
          let errorMessage = '';
          if (result.error.indexOf(BUS406) >= 0) {
            errorMessage = 'ENTITY_TYPES.BUS-406';
          }
          this.toast.error(errorMessage, 'ENTITY_TYPES.CANT_REMOVE_TITLE');
        }
      });
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
