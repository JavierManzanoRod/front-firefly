import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { MayHaveIdName } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { ToastService } from '../../../modules/toast/toast.service';
import { Entity } from '../../entity/models/entity.model';
import { EntityApiFactoryService } from '../../entity/services/entity-api-factory.service';
import { EntityTypeUtilsService } from '../services/entity-type-utils.service';
import { EntityTypeViewContainerComponent } from './entity-type-view.container';

// starts global mocks

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}

  public get(key: any): any {
    of(key);
  }
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Component({
  selector: 'ff-entity-type-list',
  template: '<p>Mock Product Editor Component</p>',
})
class MockListComponent {
  @Input() loading!: boolean;
  @Input() list: any;
  @Input() page: any;
  @Input() currentData: any;
}

@Component({
  selector: 'ff-page-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockHeaderComponent {
  @Input() pageTitle: any;
  @Input() breadcrumbTitle: any;
  @Input() breadcrumbLink: any;
}

@Component({
  selector: 'ff-list-search',
  template: '<p>Mock Product Editor Component</p>',
})
class MockRegionSearchComponent {
  @Input() pageTitle: any;
}

class ToastrServiceStub {
  public success() {}
}

class BsModalServiceStub {}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };

  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }

  public hide() {
    return true;
  }
}

// eslint-disable-next-line prefer-const
let apiSpy: {
  list: jasmine.Spy;
  detail: jasmine.Spy;
  post: jasmine.Spy;
  update: jasmine.Spy;
  delete: jasmine.Spy;
};

let apiSpyFactoryEntity: { create: jasmine.Spy };

describe('EntityTypeViewContainerComponent', () => {
  let router: Router;

  beforeEach(
    waitForAsync(() => {
      apiSpy = jasmine.createSpyObj('EntityTypeService', ['list', 'detail', 'post', 'update', 'delete']);
      apiSpy.detail.and.returnValue(of([{ id: 1 }]));
      apiSpyFactoryEntity = jasmine.createSpyObj('EntityApiFactoryService', ['create']);
      apiSpyFactoryEntity.create.and.returnValue({
        list() {
          return of(null);
        },
      });

      TestBed.configureTestingModule({
        declarations: [
          EntityTypeViewContainerComponent,
          TranslatePipeMock,
          MockListComponent,
          MockHeaderComponent,
          MockRegionSearchComponent,
        ],
        providers: [
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: BsModalService, useClass: ModalServiceMock },
          { provide: EntityTypeService, useValue: apiSpy },
          { provide: ToastService, useClass: ToastrServiceStub },
          { provide: CrudOperationsService, useFactory: () => ({}) },
          { provide: EntityApiFactoryService, useValue: apiSpyFactoryEntity },
          {
            provide: Router,
            useValue: {
              url: '/path/view/1',
              navigateByUrl() {},
            },
          },
          {
            provide: ActivatedRoute,
            useValue: {
              params: of({ id: 1 }),
            },
          },
        ],
      }).compileComponents();

      router = TestBed.inject(Router);
    })
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(EntityTypeViewContainerComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should init data', () => {
    const fixture = TestBed.createComponent(EntityTypeViewContainerComponent);
    const component = fixture.debugElement.componentInstance;
    component.ngOnInit();
    expect(component.entities).toBeUndefined();
  });

  it('should handle deleteEntityType with utils component', () => {
    const component = new EntityTypeViewContainerComponent(
      (null as unknown) as ActivatedRoute,
      (null as unknown) as EntityTypeService,
      (null as unknown) as Router,
      (null as unknown) as ToastService,
      (null as unknown) as EntityApiFactoryService,
      (null as unknown) as CrudOperationsService<MayHaveIdName>,
      (null as unknown) as EntityTypeUtilsService,
      (null as unknown) as BsModalService
    );
    const utilsSpy: {
      deleteActionModal: jasmine.Spy;
      updateOrSaveModal: jasmine.Spy;
      modal: jasmine.Spy;
      cloneActionModal: jasmine.Spy;
    } = jasmine.createSpyObj('utils', ['deleteActionModal']);
    utilsSpy.deleteActionModal.and.returnValue(true);
    component.utils = utilsSpy;
    component.deleteEntity((null as unknown) as Entity);
    expect(utilsSpy.deleteActionModal.calls.count()).toBe(1, 'one call');
  });

  it('should handle deleteEntity with utils component', () => {
    const component = new EntityTypeViewContainerComponent(
      (null as unknown) as ActivatedRoute,
      (null as unknown) as EntityTypeService,
      (null as unknown) as Router,
      (null as unknown) as ToastService,
      (null as unknown) as EntityApiFactoryService,
      (null as unknown) as CrudOperationsService<MayHaveIdName>,
      (null as unknown) as EntityTypeUtilsService,
      (null as unknown) as BsModalService
    );
    component.entityApiFactoryService = ({
      create() {
        return null;
      },
    } as unknown) as EntityApiFactoryService;

    const utilsSpy: {
      deleteActionModal: jasmine.Spy;
      updateOrSaveModal: jasmine.Spy;
      modal: jasmine.Spy;
      cloneActionModal: jasmine.Spy;
    } = jasmine.createSpyObj('utils', ['deleteActionModal']);
    utilsSpy.deleteActionModal.and.returnValue(true);
    component.utils = utilsSpy;
    component.deleteEntity((null as unknown) as Entity);
    expect(utilsSpy.deleteActionModal.calls.count()).toBe(1, 'one call');
  });

  it('goEntity with null redirect to create a new entity', () => {
    const fixture = TestBed.createComponent(EntityTypeViewContainerComponent);
    const component = fixture.debugElement.componentInstance;
    component.entityTypeId = 'undefined';
    const navigateSpy = spyOn(router, 'navigateByUrl');
    component.goEntity(null);
    expect(navigateSpy).toHaveBeenCalledWith('/rule-engine/entity-types/view/undefined/new');
  });

  it('goEntity with data redirect to edit an entity', () => {
    const fixture = TestBed.createComponent(EntityTypeViewContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const navigateSpy = spyOn(router, 'navigateByUrl');
    component.goEntity({ id: '2' } as Entity);
    expect(navigateSpy).toHaveBeenCalledWith('/rule-engine/entity-types/view/undefined/2');
  });
});
