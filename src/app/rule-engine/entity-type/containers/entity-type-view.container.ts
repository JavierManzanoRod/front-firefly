import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UpdateResponse } from '@core/base/api.service';
import { UtilsComponent } from '@core/base/utils-component';
import { AttributeEntityType, EntityType } from '@core/models/entity-type.model';
import { CrudOperationsService, groupType } from '@core/services/crud-operations.service';
import { GenericApiResponse } from '@model/base-api.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { ToastService } from '../../../modules/toast/toast.service';
import { FormUtilsService } from '../../../shared/services/form-utils.service';
import { Entity } from '../../entity/models/entity.model';
import { EntityApiFactoryService } from '../../entity/services/entity-api-factory.service';
import { EntityTypeViewHeaderComponent } from '../components/entity-type-view-header/entity-type-view-header.component';
import { EntityTypeUtilsService } from '../services/entity-type-utils.service';

@Component({
  selector: 'ff-entity-type-view-container',
  template: `<ff-page-header
      [pageTitle]="entityType.id ? ('ENTITY_TYPES.HEADER_DETAIL' | translate) : ('ENTITY_TYPES.HEADER_NEW' | translate)"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'ENTITY_TYPES.BREAD_CRUMB_TITLE' | translate"
    >
    </ff-page-header>
    <div class="page-container page-container-padding">
      <ff-entity-type-view-header
        [(changed)]="itemChanged"
        [item]="entityType"
        [loading]="loading"
        [(activeTab)]="activeTab"
        [readonly]="true"
        [entitiesError]="entitiesError"
        (changedName)="onChangeEntityTypeName($event)"
        (tabChange)="onActiveTabChange($event)"
      >
      </ff-entity-type-view-header>

      <ff-entity-type-detail-attribute
        [loading]="loading"
        [entityTypeName]="entityTypeName"
        [entityType]="entityType"
        [(changed)]="itemChanged"
        (save)="save($event)"
        [readonly]="true"
        (cancel)="cancel()"
        *ngIf="activeTab === 'attribute'"
      ></ff-entity-type-detail-attribute>

      <ff-entity-list
        *ngIf="activeTab === 'entity'"
        [list]="entities?.content"
        [page]="entities?.page"
        [totalItems]="entities?.page ? entities?.page.total_elements : 0"
        [entityType]="entityType"
        [pageSize]="10"
        [hasChanges]="itemChanged"
        (go)="goEntity($event)"
        (delete)="deleteEntity($event)"
        (pagination)="handlePagination($event)"
        (searched)="newSearch($event)"
      >
      </ff-entity-list>
    </div> `,
})
export class EntityTypeViewContainerComponent implements OnInit {
  @ViewChild(EntityTypeViewHeaderComponent) header: EntityTypeViewHeaderComponent | undefined;
  urlListado = '/rule-engine/entity-types';

  loading = true;
  itemChanged = false;

  entitiesError = false;
  entities: GenericApiResponse<Entity> | undefined;
  entityType: EntityType = {
    name: '',
    attributes: [],
    is_master: false,
  };
  entityTypeName = '';
  searchParam = '';
  utils = new UtilsComponent();

  activeTab = 'attribute';
  readonly = false;

  constructor(
    public route: ActivatedRoute,
    public apiService: EntityTypeService,
    public router: Router,
    public toast: ToastService,
    public entityApiFactoryService: EntityApiFactoryService,
    public crudOperationsService: CrudOperationsService,
    public entityTypeUtilsService: EntityTypeUtilsService,
    public modalService: BsModalService
  ) {}

  get methodToApply() {
    return this.entityType.id ? 'PUT' : 'POST';
  }

  ngOnInit(): void {
    const id = this.route.snapshot?.paramMap.get('id');
    if (id) {
      this.apiService.detail(id).subscribe((entityType) => {
        this.readonly = !this.entityTypeUtilsService.isEditable(entityType);
        this.entityType = entityType;
        //this.entityTypeName = entityType.name;
        this.loadEntites(entityType);
      });
    } else {
      this.loading = false;
    }
  }

  onErrorLoad() {
    this.router.navigateByUrl(this.urlListado).then(() => {
      this.toast.error('COMMON_ERRORS.NOT_FOUND_DESCRIPTION', 'COMMON_ERRORS.NOT_FOUND_TITLE');
    });
  }

  onChangeEntityTypeName(newName: string) {
    this.entityTypeName = newName;
  }

  newSearch(param: string) {
    this.searchParam = param;
  }

  goEntity(item: Entity | null) {
    let url: string;
    if (item && item.id) {
      url = `/rule-engine/entity-types/view/${this.entityType?.id}/${item.id}`;
    } else {
      url = `/rule-engine/entity-types/view/${this.entityType?.id}/new`;
    }
    this.router.navigateByUrl(url);
  }

  loadEntites(entityType: any) {
    const apiEntities = this.entityApiFactoryService?.create(entityType);
    apiEntities.list({ size: 10 }).subscribe(
      (entities) => {
        this.entities = entities;
        this.loading = false;
      },
      () => {
        this.toast.error('ENTITY_TYPES.ERRROR_ENTITY_404');
        this.entitiesError = true;
        this.loading = false;
      }
    );
  }

  cancel() {
    if (this.header) {
      this.router.navigate(['.'], { relativeTo: this.route.parent });
    }
  }

  onActiveTabChange(tab: string) {
    if (tab === 'attribute') {
      this.searchParam = '';
      this.loadEntites(this.entityType);
    }
  }

  save(attributes: AttributeEntityType[]) {
    if (this.header && this.header.form) {
      FormUtilsService.markFormGroupTouched(this.header.form);
      let hasLabel = false;
      attributes.forEach((attribute) => {
        if (attribute.label_attribute) {
          hasLabel = true;
        }
      });
      if (attributes.length) {
        if (hasLabel) {
          if (this.header.form?.valid) {
            this.entityType.name = this.header.form.value.name;
            this.entityType.label = this.header.form.value.label;
            this.entityType.define_rules = this.header.form.value.define_rules;
            this.entityType.attributes = attributes;
            this.crudOperationsService
              .updateOrSaveModal(this, this.entityType, groupType.ENTITY_TYPE)
              .pipe(
                catchError((e: HttpErrorResponse) => {
                  if (e.status === 409) {
                    this.header?.form?.controls.name.setErrors({ error_409: true });
                  }
                  return throwError(e);
                })
              )
              .subscribe((data: UpdateResponse<any>) => {
                this.itemChanged = false;
                this.header?.form?.markAsPristine();
                if (!this.entityType.id && data.data?.id) {
                  this.router.navigate([`${this.urlListado}/view/${data?.data?.id}`]);
                } else if (this.entityType.id) {
                  this.entityType = data.data;
                }
              });
          }
        } else {
          this.toast.error('ENTITY_TYPE_DETAIL.ERROR_NEED_ONE_LABEL');
        }
      }
      this.header.form.markAsPristine();
    }
  }

  handlePagination($event: any) {
    const apiEntities = this.entityApiFactoryService?.create(this.entityType);
    const name = this.searchParam;
    apiEntities.list({ ...$event, name }).subscribe((entities) => {
      this.entities = entities;
    });
  }

  deleteEntity(itemToDelete: Entity) {
    return this.utils.deleteActionModal(
      {
        modalService: this.modalService,
        toast: this.toast,
        apiService: this.entityApiFactoryService?.create(this.entityType),
      },
      itemToDelete,
      () => {
        this.ngOnInit();
      }
    );
  }

  canDeactivate(): UICommonModalConfig | false {
    if (this.itemChanged || (this.header && this.header.form?.dirty)) {
      return {
        bodyMessage: this.entityType.name ? 'COMMON_MODALS.UNSAVED_ATTS_ID' : 'COMMON_MODALS.UNSAVED_ATTS_TRANSLATIONS_1',
        name: this.entityType.name,
      };
    }
    return false;
  }
}
