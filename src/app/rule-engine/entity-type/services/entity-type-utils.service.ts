import { Injectable } from '@angular/core';
import { EntityType } from '../../../core/models/entity-type.model';

@Injectable({
  providedIn: 'root',
})
export class EntityTypeUtilsService {
  isEditable(entityType: EntityType | null) {
    const id = entityType?.id;
    if (id == null || id === '') {
      return true;
    } else {
      return this.isUuid(id);
    }
  }

  private isUuid(id: string) {
    const regex_uuid = /(?:^[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[a-f0-9]{4}-[a-f0-9]{12}$)|(?:^0{8}-0{4}-0{4}-0{4}-0{12}$)/;
    return regex_uuid.test(id);
  }
}
