import { EntityType } from '../../../core/models/entity-type.model';
import { EntityTypeUtilsService } from './entity-type-utils.service';

describe('EntityTypeUtilsService', () => {
  it('isEditable returns true if is an uuid ', () => {
    const service = new EntityTypeUtilsService();
    const result = service.isEditable({ id: '68d49087-9c28-44d9-85e3-ce4ae6bf8c4e' } as EntityType);
    expect(result).toBeTruthy();
  });

  it('isEditable returns true if is empty the id ', () => {
    const service = new EntityTypeUtilsService();
    const result = service.isEditable({ id: '' } as EntityType);
    expect(result).toBeTruthy();
  });

  it('isEditable returns true if is empty the id ', () => {
    const service = new EntityTypeUtilsService();
    const result = service.isEditable(null);
    expect(result).toBeTruthy();
  });

  it('isEditable returns false not is not an uuid ', () => {
    const service = new EntityTypeUtilsService();
    const result = service.isEditable({ id: 'pivq72e33iy5ys' } as EntityType);
    expect(result).toBeFalsy();
  });
});
