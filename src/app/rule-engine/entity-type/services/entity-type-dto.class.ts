import { EntityType } from '@core/models/entity-type.model';

export class EntityTypeDTO {
  data = {} as EntityType;

  constructor(data: any) {
    this.data.id = data.id;
    this.data.label = data.label;
    this.data.name = data.name;
    this.data.define_rules = data.define_rules;
    this.data.attributes = (data.attributes || []).map((att: any) => {
      return {
        id: att.id,
        label: att.label,
        name: att.name,
        weight: att.weight,
        data_type: att.data_type,
        entity_type_id: att.entity_type_id,
        label_attribute: att.label_attribute,
        entity_reference: att.entity_reference,
        entity_reference_name: att.entity_reference_name,
        multiple_cardinality: att.multiple_cardinality,
        is_i18n: att.is_i18n,
        is_tree_attribute: att.is_tree_attribute,
        sub_entity_type: att.sub_entity_type,
      };
    });
  }
}
