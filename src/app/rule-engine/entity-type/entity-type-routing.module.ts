import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { EntityDetailContainerComponent } from '../entity/containers/entity-detail-container.component';
import { EntityTypeListContainerComponent } from './containers/entity-type-list.container';
import { EntityTypeViewContainerComponent } from './containers/entity-type-view.container';

const routes: Routes = [
  {
    path: '',
    component: EntityTypeListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
      breadcrumb: [
        {
          label: 'A',
          url: '/rule-engine/entity-types',
        },
        {
          label: 'SHIPPING_COST.SHIPPING_COST',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'new',
        component: EntityTypeViewContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
          breadcrumb: [
            {
              label: 'ENTITY_TYPES.BREAD_CRUMB_TITLE',
              url: '/rule-engine/entity-types/new',
            },
            {
              label: 'SHIPPING_COST.SHIPPING_COST',
              url: '',
            },
          ],
        },
      },
      {
        path: 'view/:id/new',
        component: EntityDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
          title: 'view',
          breadcrumb: [
            {
              label: 'ENTITY.LIST_TITLE',
              url: '/rule-engine/entity-types',
            },
            {
              label: 'ENTITY.ENTITY',
              url: '/rule-engine/entity-types/view/:id?tab=entity',
            },
          ],
        },
      },
      {
        path: 'view/:id/:idEntity',
        component: EntityDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
          title: 'view',
          breadcrumb: [
            {
              label: 'ENTITY.LIST_TITLE',
              url: '/rule-engine/entity-types',
            },
            {
              label: 'ENTITY.ENTITY',
              url: '/rule-engine/entity-types/view/:id?tab=entity',
            },
          ],
        },
      },

      {
        path: 'view/:id',
        component: EntityTypeViewContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
          breadcrumb: [
            {
              label: 'ENTITY_TYPES.BREAD_CRUMB_TITLE',
              url: '/rule-engine/admin-group-type',
            },
            {
              label: 'ENTITY_TYPES.BREAD_CRUMB_TITLE',
              url: '',
            },
          ],
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntityTypeRoutingModule {}
