import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ExplodedTreeNode } from 'src/app/modules/exploded-tree/models/exploded-tree.model';

@Component({
  selector: 'ff-entity-type-detail-attribute-info',
  templateUrl: 'entity-type-detail-attribute-info.component.html',
  styleUrls: ['./entity-type-detail-attribute-info.component.scss'],
})
export class EntityTypeDetailAttributeInfoComponent {
  @Output() addAttribute: EventEmitter<void> = new EventEmitter<void>();
  @Output() removeAttribute: EventEmitter<ExplodedTreeNode> = new EventEmitter<ExplodedTreeNode>();
  @Output() viewAttribute: EventEmitter<ExplodedTreeNode> = new EventEmitter<ExplodedTreeNode>();
  @Input() readonly = false;
  @Input() selectedItem: ExplodedTreeNode | undefined;
  @Input() tree: ExplodedTreeNode | undefined;

  constructor() {}
}
