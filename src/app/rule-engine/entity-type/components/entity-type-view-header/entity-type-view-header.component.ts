import { AfterViewInit, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityType } from '@core/models/entity-type.model';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalUnsavedTabComponent } from '../../../../modules/common-modal/components/modal-unsaved-tab/modal-unsaved-tab.component';
import { FormsErrorsConfig, FormUtilsService } from '../../../../shared/services/form-utils.service';

@Component({
  selector: 'ff-entity-type-view-header',
  templateUrl: 'entity-type-view-header.component.html',
  styleUrls: ['./entity-type-view-header.component.scss'],
})
export class EntityTypeViewHeaderComponent implements OnChanges, AfterViewInit {
  @Output() activeTabChange: EventEmitter<string> = new EventEmitter<string>();
  @Output() tabChange: EventEmitter<string> = new EventEmitter<string>();
  @Output() changedChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() changedName: EventEmitter<string> = new EventEmitter<string>();
  @Input() readonly = false;
  @Input() activeTab = '';
  @Input() entitiesError = false;

  @Input() item: EntityType | undefined;
  @Input() loading = false;
  @Input() changed = false;

  form: FormGroup | undefined;
  formErrors: FormsErrorsConfig | undefined;

  constructor(
    private modalService: BsModalService,
    private translateService: TranslateService,
    private activeRoute: ActivatedRoute,
    private router: Router
  ) {
    const form = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      name: {
        validators: {
          required: true,
          error_409: true,
        },
      },
      label: {
        validators: {
          required: true,
        },
      },
      define_rules: {
        value: true,
      },
    });
    this.form = form.form;
    this.formErrors = form.errors;
  }

  onBlurName(newVal: string) {
    this.changedName.emit(newVal);
  }

  ngAfterViewInit() {
    this.activeRoute.queryParams.subscribe((params) => {
      if (params && params.tab && this.activeTab !== params.tab) {
        setTimeout(() => {
          this.setTab(params.tab);
        }, 10);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!this.loading && this.item && (changes.loading || changes.item)) {
      this.form?.patchValue({
        name: this.item.name,
        label: this.item.label,
        define_rules: Object.prototype.hasOwnProperty.call(this.item, 'define_rules') ? this.item.define_rules : true,
      });
    }
  }

  changeTab(tab: string) {
    if (this.activeTab !== tab) {
      if (this.changed) {
        const modal = this.modalService.show(ModalUnsavedTabComponent, {
          initialState: {
            tabName: this.translateService.instant('ENTITY_TYPE_DETAIL.ATTRIBUTES'),
            errorMessage: this.translateService.instant('COMMON_MODALS.UNSAVED_CHANGES_ERROR_MESSAGE'),
          },
        });
        if (modal.content) {
          modal.content.cancel.subscribe((res: boolean) => {
            if (res) {
              modal.hide();
            }
          });
          modal.content.confirm.subscribe((res: boolean) => {
            if (res) {
              this.setTab(tab);
              this.tabChange.emit(tab);
              this.changedChange.emit(false);
              modal.hide();
            }
          });
        }
      } else {
        this.tabChange.emit(tab);
        this.setTab(tab);
      }
    }
  }

  setTab(tab: string) {
    this.router.navigate([], {
      queryParams: { tab },
      relativeTo: this.activeRoute,
    });
    this.activeTabChange.emit(tab);
  }
}
