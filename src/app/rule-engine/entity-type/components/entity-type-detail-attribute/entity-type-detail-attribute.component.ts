/* eslint-disable @typescript-eslint/no-unsafe-call */
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { UtilsComponent } from '@core/base/utils-component';
import { AttributeEntityType, EntityType } from '@core/models/entity-type.model';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { ExplodedTreeComponent } from 'src/app/modules/exploded-tree/exploded-tree.component';
import { ExplodedClickEvent, ExplodedTree, ExplodedTreeNode } from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { EntityTypeAddAttributeModalComponent } from '../entity-type-add-attribute-modal/entity-type-add-attribute-modal.component';

@Component({
  selector: 'ff-entity-type-detail-attribute',
  templateUrl: 'entity-type-detail-attribute.component.html',
  styleUrls: ['./entity-type-detail-attribute.component.scss'],
})
export class EntityTypeDetailAttributeComponent implements OnChanges {
  @ViewChild(ExplodedTreeComponent, { static: false }) tree: ExplodedTreeComponent<any> | undefined;
  @Output() save: EventEmitter<AttributeEntityType[]> = new EventEmitter<AttributeEntityType[]>();
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() changedChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() changed = false;
  @Input() loading = false;
  @Input() entityType: EntityType | undefined;
  @Input() entityTypeName = '';
  @Input() readonly = false;

  treeList: ExplodedTree[] = [
    {
      name: '',
      value: {
        name: '',
        isEntityType: true,
      },
      icon: 'icon-atributos-1',
      children: [],
    },
  ];

  isShake = false;

  selectedItem: ExplodedTreeNode | undefined = this.treeList[0];
  selectedTarget!: ExplodedTreeNodeComponent;
  viewNodes: ExplodedTreeNode[] = [];

  utils: UtilsComponent = new UtilsComponent();

  constructor(private modalService: BsModalService, private translateService: TranslateService, private apiService: EntityTypeService) {
    this.translateService.get('COMMON.NAME').subscribe((value: string) => {
      this.treeList[0].name = value;
    });
  }

  onChange() {
    this.tree?.update();
    this.changedChange.emit(true);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.loading && !this.loading && this.entityType && this.entityType.attributes.length) {
      this.calculeTreeListFromAttributes();
      /*
      setTimeout(() => {
        if (this.uiTreeList) {
          this.uiTreeList.lastSelectedItem = this.uiTreeList.items[0];
        }
      }, 10);
      */
    }
    if (this.entityTypeName) {
      this.treeList[0].name = this.entityTypeName;
      this.tree?.update();
    }
  }

  addAttribute() {
    const namesAlreadyUsed: string[] =
      this.selectedItem && this.selectedItem.children ? this.selectedItem.children.map((item) => item.name as unknown as string) : [];
    const labelsAlreadyUsed: string[] =
      this.selectedItem && this.selectedItem.children ? this.selectedItem.children.map((item) => item.label as unknown as string) : [];

    const modal = this.modalService.show(EntityTypeAddAttributeModalComponent, {
      initialState: {
        namesAlreadyUsed,
        labelsAlreadyUsed,
        entityTypeId: this.entityType?.id,
      },
      class: 'modal-center modal-600-w',
    });
    if (modal.content) {
      modal.content.add.subscribe((form: AttributeEntityType) => {
        this.addAttributeToTreeList(form);
      });
    }
  }

  viewAttribute(item: ExplodedTreeNode) {
    if (!this.selectedTarget) {
      this.selectedTarget = this.tree?.nodes?.get(0) as ExplodedTreeNodeComponent;
    }

    if (this.selectedItem && this.selectedItem.children) {
      const index = this.selectedItem.children.indexOf(item);

      this.selectedTarget = this.selectedTarget.nodes?.get(index) as ExplodedTreeNodeComponent;
      this.selectedTarget?.clickNode.next({
        target: this.selectedTarget,
        data: this.selectedTarget.data,
      });
    }
    this.selectedItem = item;
  }

  removeAttribute(item: ExplodedTreeNode) {
    if (this.selectedItem && this.selectedItem.children) {
      const index = this.selectedItem.children.indexOf(item);
      if (index >= 0) {
        this.selectedItem.children.splice(index, 1);
      }
    }
    this.onChange();
  }

  addAttributeToTreeList(attribute: AttributeEntityType) {
    if (this.treeList[0].children) {
      this.treeList[0].children.push(this.getItemForUiTreeListFromAttributeEntityType(attribute));
    }
    this.onChange();
  }

  // CBAEURASVZPKCV
  loadEntityChildrens(attributeEntity: AttributeEntityType) {
    if (attributeEntity.entity_view?.id) {
      return this.apiService.detail(attributeEntity.entity_view.id).pipe(
        map((result) => {
          return result.attributes.map((attribute) => {
            return this.getItemForUiTreeListFromAttributeEntityType(attribute);
          });
        })
      );
    }
    return of([]);
  }

  getItemForUiTreeListFromAttributeEntityType(attributeEntityType: AttributeEntityType) {
    const item: ExplodedTreeNode = {
      value: attributeEntityType,
      icon: 'icon-atributos',
      name: attributeEntityType.name,
      label: attributeEntityType.label,
      identifier: attributeEntityType.id,
    };

    if (attributeEntityType.data_type === 'ENTITY' || attributeEntityType.data_type === 'EMBEDDED') {
      item.value.loadChildrens = (event: any) => this.loadEntityChildrens(event);
      item.icon = 'icon-atributos-1';
    }

    return item;
  }

  calculeTreeListFromAttributes() {
    if (this.entityType) {
      this.treeList[0].children = this.entityType.attributes.map((value) => {
        return this.getItemForUiTreeListFromAttributeEntityType(value);
      });
    }
  }

  clickNode({ data, target }: ExplodedClickEvent<ExplodedTreeNodeComponent>) {
    if (
      !data.children?.length &&
      data.value.loadChildrens &&
      target.data.value.entity_reference !== target.parent?.data.value.entity_reference
    ) {
      target.setLoading(true);
      data.value.loadChildrens(data.value).subscribe((children: ExplodedTreeNode[]) => {
        data.children = children;
        target.setLoading(false);
      });
    }

    target.expand();

    this.selectedTarget = target;
    this.selectedItem = data;
  }

  getDataForSave(): AttributeEntityType[] {
    const result: AttributeEntityType[] = [];
    if (this.treeList[0].children) {
      this.treeList[0].children.forEach((item) => {
        if (item.value.data_type !== 'ENTITY' && item.value.data_type !== 'EMBEDDED') {
          result.push(item.value);
        } else {
          result.push({
            ...item.value,
            entity_type_id: this.entityType?.id,
          });
        }
      });
    }
    return result;
  }

  shakeWarning() {
    this.isShake = true;
    setTimeout(() => {
      this.isShake = false;
    }, 400);
  }

  search(event: any) {
    if (this.treeList[0].children) {
      if (event.name) {
        this.treeList[0]?.children
          ?.filter((item) => !item.value.name.toLowerCase().includes(event.name.toLowerCase()))
          .forEach((item) => (item.value.hidden = true));
      } else {
        this.treeList[0]?.children?.forEach((item) => (item.value.hidden = false));
      }
      this.tree?.update();
    }
  }

  sendSave() {
    const attributes = this.getDataForSave();
    if (!attributes.length) {
      this.shakeWarning();
    }
    this.save.emit(attributes);
  }

  showFilter(node: ExplodedTreeNode) {
    if (node.value?.hidden) {
      return false;
    }
    return true;
  }
}
