import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { map } from 'rxjs/operators';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { FormsErrorsConfig, FormUtilsService } from '../../../../shared/services/form-utils.service';

@Component({
  selector: 'ff-entity-type-add-attribute-modal',
  templateUrl: 'entity-type-add-attribute-modal.component.html',
  styleUrls: ['./entity-type-add-attribute-modal.component.scss'],
})
export class EntityTypeAddAttributeModalComponent {
  @Input() entityTypeId = '';
  @Output() add: EventEmitter<any> = new EventEmitter<any>();

  namesAlreadyUsed: string[] = [];
  showNameAlreadyUsedError = false;

  labelsAlreadyUsed: string[] = [];
  showLabelAlreadyUsedError = false;

  form: FormGroup | undefined;
  formErrors: FormsErrorsConfig | undefined;

  dataTypeDisable = false;

  constructor(private modalRef: BsModalRef, protected apiEntityType: EntityTypeService) {
    const form = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      name: {
        validators: {
          required: true,
        },
      },
      label: {
        validators: {
          required: true,
        },
      },
      weight: {
        value: 0,
        validators: {
          numericPositive: true,
        },
      },
      data_type: {
        value: 'STRING',
      },
      label_attribute: {
        value: false,
      },
      entity_view: {
        validators: {},
      },
      multiple_cardinality: {
        value: false,
      },
      is_i18n: {
        value: false,
      },
    });
    this.form = form.form;
    this.formErrors = form.errors;
  }

  send() {
    this.checkLabel();
    this.checkName();
    if (this.form) {
      FormUtilsService.markFormGroupTouched(this.form);
    }
    if (this.form?.valid && !this.showLabelAlreadyUsedError) {
      this.add.emit(this.form.value);
      this.close();
    }
  }

  close() {
    this.modalRef.hide();
  }

  checkLabel() {
    this.showLabelAlreadyUsedError = this.labelsAlreadyUsed.indexOf(this.form?.value.label) >= 0;
  }

  checkName() {
    this.showNameAlreadyUsedError = this.namesAlreadyUsed.indexOf(this.form?.value.name) >= 0;
  }

  onChangeI18n() {
    if (this.form?.value.is_i18n) {
      this.form.controls.data_type.setValue('STRING');
      this.dataTypeDisable = true;
    } else {
      this.dataTypeDisable = false;
    }
    this.form?.controls.entity_view.clearValidators();
    this.form?.controls.entity_view.updateValueAndValidity();
  }

  searchFn(key: string) {
    return this.apiEntityType.search(key).pipe(map((value) => value.filter((v) => v.id !== this.entityTypeId)));
  }

  onChangeDataType() {
    if (this.form?.value.data_type === 'ENTITY' || this.form?.value.data_type === 'EMBEDDED') {
      // eslint-disable-next-line @typescript-eslint/unbound-method
      this.form.controls.entity_view.setValidators(Validators.required);
    } else {
      this.form?.controls.entity_view.clearValidators();
    }
    if (this.form?.value.data_type === 'BOOLEAN') {
      this.form.controls.multiple_cardinality.setValue(false);
    }
    this.form?.controls.entity_view.updateValueAndValidity();
  }
}
