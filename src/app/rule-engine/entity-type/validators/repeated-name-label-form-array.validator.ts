import { FormArray, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Attribute } from '@core/models/attribute.model';

export const repeatedNameLabelFormArrayValidator: ValidatorFn = () => {
  return (control: FormArray): ValidationErrors | null => {
    const value = control.value as Attribute[];
    if (value && value.length && value.length > 1) {
      if (!areUniques('name', value)) {
        return { duplicatedname: { value: true } };
      }
      if (!areUniques('label', value)) {
        return { duplicatedlabel: { value: true } };
      }
    }
    return null;
  };
};

function areUniques(propertyToScan: keyof Attribute, arr: Attribute[]) {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  const data = new Set(arr.map((e) => e[propertyToScan]));
  const numerOfDistincts = [...data].length;
  return arr.length === numerOfDistincts;
}
