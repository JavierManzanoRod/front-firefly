/* eslint-disable @typescript-eslint/unbound-method */
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EntityType } from '@core/models/entity-type.model';
import { ERROR_RULES } from 'src/app/modules/conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { Conditions2UtilsService } from 'src/app/modules/conditions2/services/conditions2-utils.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { CategoryRule, InputCategoryDetailComponent } from '../../models/category-rule.model';
import { CategoryRuleService } from '../../services/category-rule.service';

@Component({
  selector: 'ff-category-rule-detail',
  styleUrls: ['category-rule-detail.component.scss'],
  templateUrl: 'category-rule-detail.component.html',
})
export class CategoryRuleDetailComponent implements OnChanges, OnInit {
  @Input() data!: InputCategoryDetailComponent;
  @Input() readonly = false;
  @Output() formSubmit: EventEmitter<CategoryRule> = new EventEmitter<CategoryRule>();
  @Output() delete: EventEmitter<CategoryRule> = new EventEmitter<CategoryRule>();
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  form: FormGroup;
  sended = false;

  showConditions = false;
  entityTypeSelected!: EntityType;
  loading = true;

  constructor(
    private fb: FormBuilder,
    protected router: Router,
    public toastService: ToastService,
    public categoryRulesSearchService: CategoryRuleService,
    private utilsConditions: Conditions2UtilsService
  ) {
    this.form = this.fb.group({
      name: [null, Validators.required],
      node: [null],
    });
  }

  ngOnChanges() {
    if (this.data) {
      const item = this.data.categoryRule;
      this.entityTypeSelected = this.data.entityType;
      this.form.patchValue({
        ...item,
      });
      this.loading = false;
    }
  }

  ngOnInit() {
    this.showConditions = true;
    if (this.readonly) {
      this.form.disable();
    }
  }

  onselectEntityType(entityTypeSeleted: EntityType) {
    this.entityTypeSelected = entityTypeSeleted;
  }

  handleDelete() {
    this.delete.emit(this.getDataToSend());
  }

  getFolderId(item: CategoryRule) {
    const arrayId = this.router.url.split('/') || [];
    if (item.id) {
      return arrayId[arrayId.length - 2];
    } else {
      return arrayId[arrayId.length - 1];
    }
  }

  getDataToSend(): CategoryRule {
    const formValue: CategoryRule = this.form.value as CategoryRule;
    const nodeValue = this.utilsConditions.valueToSend(this.form.controls.node.value);

    return {
      id: this.data.categoryRule.id,
      ...formValue,
      category_type: (this.data.categoryRule && this.data.categoryRule.category_type) || 'Genérico',
      folder_id: this.getFolderId(this.data.categoryRule),
      node: nodeValue as any,
    };
  }

  submit() {
    this.form.get('node')?.updateValueAndValidity();
    this.form.markAllAsTouched();
    this.form.updateValueAndValidity();
    this.sended = true;
    if (this.form.valid) {
      if (!this.form.value?.node) {
        this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.ENTER_CONDITION');
      } else if (this.form.valid) {
        const tempNodes = this.utilsConditions.valueToSend(this.form.controls.node.value);
        if (tempNodes === ERROR_RULES.ERROR_RANGE) {
          this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.RANGES_WITHOUT_CENTERS');
        } else if (tempNodes === ERROR_RULES.ERROR_CENTER) {
          this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.CENTERS_WITHOUT_RANGES');
        } else this.formSubmit.emit(this.getDataToSend());
      }
    } else {
      if (this.form.controls.node.errors?.required) {
        this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.ENTER_CONDITION');
      }
      if (this.form.controls.node.errors?.isFilled) {
        this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.INCOMPLETE_RULES');
      }
      if (this.form.controls.node.errors?.doubleNotLT) {
        this.toastService.error('CONDITIONS_BASIC_AND_ADVANCE.DOUBLE_NOT_LT_ERROR');
      }
    }

    return false;
  }
}
