import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { CategoryRuleFolderDetailComponent } from './category-rule-detail-folder.component';

describe('ContentGroupDetailFolderComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [CategoryRuleFolderDetailComponent],
        imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule],
        providers: [{ provide: CrudOperationsService, useValue: {} }],
      }).compileComponents();
    })
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(CategoryRuleFolderDetailComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });
});
