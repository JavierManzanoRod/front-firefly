/* eslint-disable @typescript-eslint/no-unsafe-call */
import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, Output, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import { CategoryRuleFolderService } from '../../services/category-rule-folder.service';

@Component({
  selector: 'ff-category-rule-folder-detail',
  templateUrl: './category-rule-detail-folder.component.html',
  styleUrls: ['./category-rule-detail-folder.component.scss'],
})
export class CategoryRuleFolderDetailComponent implements OnChanges {
  @ViewChild('inputRef') inputRef!: ElementRef<HTMLInputElement>;
  @Input() folder!: ExplodedTreeNodeComponent;
  @Input() showConfigSection = false;
  @Input() link = '/rule-engine/category-rule/';
  @Output() valueChange = new EventEmitter();
  @Output() folderChange = new EventEmitter();
  @Output() deleteFolder = new EventEmitter();
  @Output() cleanInput = new EventEmitter();
  focused = false;
  isEditable = false;
  dbclickData!: {
    timeStamp: number;
    target?: EventTarget;
  };

  constructor(
    public router: Router,
    private change: ChangeDetectorRef,
    private renderer: Renderer2,
    public apiService: CategoryRuleFolderService,
    public crud: CrudOperationsService
  ) {}

  ngOnChanges() {}

  newRule(event: MouseEvent) {
    this.folder.clickAction({ type: 'new' }, event);
  }

  newFolder() {
    if (this.folder) {
      this.folder.data.value = { child: true };
      this.cleanInput.emit(true);
      this.isEditable = !this.isEditable;
    }
    this.change.markForCheck();
    setTimeout(() => {
      this.renderer.selectRootElement(this.inputRef.nativeElement).focus();
    });
  }

  collapseParent() {
    this.isEditable = false;
  }

  save() {
    const folderToSend = {
      name: this.inputRef.nativeElement.value,
      parent_id: this.folder.data.id,
    };
    this.crud
      .updateOrSaveModal(
        {
          methodToApply: 'POST',
          apiService: this.apiService,
          captureCancel: true,
        },
        folderToSend
      )
      .subscribe((response) => {
        if (!response) {
          this.isEditable = true;
        } else {
          this.isEditable = false;
          this.folder.data.children?.unshift({
            id: response.data.id,
            name: response.data.name,
            parent_id: response.data.parent_id,
          });
          this.change.markForCheck();
          this.valueChange.emit(this.folder);
        }
      });
    this.change.markForCheck();
  }

  input(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.save();
    }
    if (event.key === 'Escape') {
      this.isEditable = false;
    }
  }

  blur() {
    this.focused = false;
    if (this.isEditable) {
      return false;
    }
    setTimeout(() => {
      this.save();
    }, 100);
  }

  ruleDetail(element: ExplodedTreeNodeComponent, event: MouseEvent) {
    element.clickAction({ type: 'detail' }, event);
  }

  deleteRule(element: ExplodedTreeNodeComponent) {
    this.deleteFolder.emit(element);
  }

  dbclick(event: MouseEvent, element: ExplodedTreeNodeComponent) {
    const { timeStamp, target } = this.dbclickData || { timeStamp: 0, target: undefined };

    if (!element.data.value) {
      if (event.timeStamp - timeStamp < 400 && target === event.target) {
        this.folderChange.emit(element);
      }
    }

    this.dbclickData = {
      timeStamp: event.timeStamp,
      target: event.target ? event.target : undefined,
    };
  }

  update() {
    this.change.markForCheck();
  }
}
