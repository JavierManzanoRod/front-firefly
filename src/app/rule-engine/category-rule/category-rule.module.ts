import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { SortablejsModule } from 'ngx-sortablejs';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ContentGroupModule } from 'src/app/catalog/content-group/content-group.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { Conditions2Module } from 'src/app/modules/conditions2/conditions2.module';
import { ExplodedTreeModule } from 'src/app/modules/exploded-tree/exploded-tree.module';
// eslint-disable-next-line max-len
import { SelectModule } from 'src/app/modules/select/select.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { GenericListComponentModule } from './../../modules/generic-list/generic-list.module';
import { CategoryRuleModuleRoutingModule } from './category-rule-routing.module';
import { CategoryRuleFolderDetailComponent } from './components/category-rule-detail-folder/category-rule-detail-folder.component';
import { CategoryRuleDetailComponent } from './components/category-rule-detail/category-rule-detail.component';
import { CategoryRuleDetailContainerComponent } from './containers/category-rule-detail-container';
import { CategoryRuleListContainerComponent } from './containers/category-rule-list-container.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CategoryRuleModuleRoutingModule,
    CommonModalModule,
    NgSelectModule,
    RouterModule,
    SelectModule,
    AccordionModule.forRoot(),
    SortablejsModule.forRoot({ animation: 150 }),
    Conditions2Module,
    SimplebarAngularModule,
    GenericListComponentModule,
    CoreModule,
    TranslateModule,
    ExplodedTreeModule,
    ContentGroupModule,
  ],
  declarations: [
    CategoryRuleListContainerComponent,
    CategoryRuleDetailContainerComponent,
    CategoryRuleDetailComponent,
    CategoryRuleFolderDetailComponent,
  ],
  providers: [],
  exports: [CategoryRuleDetailComponent],
})
export class CategoryRuleModule {}
