import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, IApiSearchable } from '@core/base/api.service';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CategoryRuleDTO } from '../models/category-rule.dto';
import { CategoryRule } from '../models/category-rule.model';
import { CategoryRuleInMapper } from './dto/category-rule/category-rule-in-mapper.class';
import { CategoryRuleOutMapper } from './dto/category-rule/category-rule-out-mapper.class';
import { FF_API_PATH_VERSION_CATEGORY_RULES } from '../../../configuration/tokens/api-versions.token';

@Injectable({
  providedIn: 'root',
})
export class CategoryRuleService extends ApiService<CategoryRule> implements IApiSearchable<CategoryRule> {
  endPoint = `products/backoffice-category-rules/${this.version}categories`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_CATEGORY_RULES) private version: string) {
    super();
  }

  search(nameToSearch: string, allItems?: any): Observable<CategoryRule[]> {
    return this.list({ name: nameToSearch, ...allItems }).pipe(map(({ content }) => content));
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<CategoryRule>> {
    return super.list(filter).pipe(
      map((response) => {
        response.content = response.content.map((r) => new CategoryRuleInMapper(r as unknown as CategoryRuleDTO).data);
        return response;
      })
    );
  }

  detail(id: string): Observable<CategoryRule> {
    return super.detail(id).pipe(map((detail) => new CategoryRuleInMapper(detail as unknown as CategoryRuleDTO).data));
  }

  post(data: CategoryRule) {
    return super.post(new CategoryRuleOutMapper(data).data as unknown as CategoryRule).pipe(
      map((response) => ({
        status: response.status,
        data: new CategoryRuleInMapper(response.data as unknown as CategoryRuleDTO).data,
      }))
    );
  }

  patch(data: CategoryRule) {
    return super.patch(new CategoryRuleOutMapper(data).data as unknown as CategoryRule).pipe(
      map((response) => ({
        status: response.status,
        data: new CategoryRuleInMapper(response.data as unknown as CategoryRuleDTO).data,
      }))
    );
  }

  update(data: CategoryRule) {
    return super.update(new CategoryRuleOutMapper(data).data as unknown as CategoryRule).pipe(
      map((response) => ({
        status: response.status,
        data: new CategoryRuleInMapper(response.data as unknown as CategoryRuleDTO).data,
      }))
    );
  }
}
