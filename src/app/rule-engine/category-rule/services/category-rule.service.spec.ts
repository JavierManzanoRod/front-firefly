import { HttpClient } from '@angular/common/http';
import { GenericApiRequest, GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { CategoryRule } from '../models/category-rule.model';
import { CategoryRuleService } from './category-rule.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: CategoryRuleService;
const version = 'v1/';

const expectedData: CategoryRule = { id: '1', name: 'name', node: {} } as CategoryRule;
const expectedList: GenericApiResponse<CategoryRule> = {
  content: [expectedData],
  page: (null as unknown) as Page,
};

describe('CategoryRuleService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it(`CategoryRuleService points to products/backoffice-category-rules/${version}categories`, () => {
    myService = new CategoryRuleService((null as unknown) as HttpClient, version);
    expect(myService.endPoint).toEqual(`products/backoffice-category-rules/${version}categories`);
  });

  it('CategoryRuleService.list calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new CategoryRuleService(httpClientSpy as any, version);
    myService.list((null as unknown) as GenericApiRequest).subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      done();
    });
  });

  it('CategoryRuleService.search calls to get api method', (done) => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new CategoryRuleService(httpClientSpy as any, version);
    myService.search('name').subscribe((response) => {
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
      expect(response.length).toBe(expectedList.content.length);
      done();
    });
  });

  it('CategoryRuleService.emptyRecord record gives me some data', () => {
    myService = new CategoryRuleService((null as unknown) as HttpClient, version);
    const record = myService.emptyRecord();
    expect(record).toBeDefined();
  });
});
