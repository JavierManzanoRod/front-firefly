import { Attribute } from '@core/models/attribute.model';
import { CategoryRuleAttributeDTO } from '../../../models/category-rule.dto';

export class AttributeInMapper {
  data = {} as Attribute;

  constructor(remoteData: CategoryRuleAttributeDTO) {
    this.data = {
      id: remoteData.identifier,
      label: remoteData.label,
      name: remoteData.name,
      weight: remoteData.weight,
      attribute_label: remoteData.attribute_data_type,
      data_type: remoteData.data_type,
      entity_type_id: remoteData.entity_type_id,
      attribute_id: remoteData.attribute_id,
      label_attribute: remoteData.is_label_attribute,
      entity_reference: remoteData.entity_reference,
      entity_reference_name: remoteData.entity_reference_name,
      multiple_cardinality: remoteData.is_multiple_cardinality,
      is_i18n: remoteData.is_i18n,
      tooltip_label: remoteData.tooltip_label,
      attribute_data_type: remoteData.attribute_data_type,
    };
  }
}
