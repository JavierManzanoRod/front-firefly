import { Attribute } from '@core/models/attribute.model';
import { CategoryRuleAttributeDTO } from '../../../models/category-rule.dto';

export class AttributeOutMapper {
  data = {} as CategoryRuleAttributeDTO;

  constructor(data: Attribute) {
    const override: CategoryRuleAttributeDTO = {
      identifier: data.id,
      is_label_attribute: data.label_attribute,
      is_multiple_cardinality: data.multiple_cardinality,
    } as CategoryRuleAttributeDTO;

    this.data = { ...data, ...override };
  }
}
