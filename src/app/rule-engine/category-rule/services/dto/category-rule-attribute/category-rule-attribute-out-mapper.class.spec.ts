import { Attribute } from '@core/models/attribute.model';
import { AttributeOutMapper } from './category-rule-attribute-out-mapper.class';

const attributes: Attribute[] = [
  {
    label: 'productId',
    name: 'productId',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: '6ec93f23-9770-435f-91e6-8940c8859b60',
    is_i18n: false,
  },

  {
    label: 'MarcaCompleja',
    name: 'MarcaCompleja',
    weight: 0,
    data_type: 'ENTITY',
    label_attribute: false,
    multiple_cardinality: false,
    entity_reference: '58b5e891-ed0b-4556-8e97-4faa329a1967',
    entity_reference_name: 'MarcaComplex',
    id: 'a642743b-fd52-4724-9a2b-7fdc0dc60df2',
    is_i18n: false,
  },
  {
    label: 'Bar Code',
    name: 'referenceDetail.bar_code',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'bar_code_attr',
    is_i18n: false,
  },
  {
    label: 'Marca',
    name: 'brand.name',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'brand_attr',
    is_i18n: false,
  },
  {
    label: 'Center',
    name: 'center',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'center_attr',
    is_i18n: false,
  },
  {
    label: 'Compañia',
    name: 'referenceDetail.company',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'company_attr',
    is_i18n: false,
  },
  {
    label: 'Departamento',
    name: 'referenceDetail.department',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'department_attr',
    is_i18n: false,
  },
  {
    label: 'Division',
    name: 'merchandise_division',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'division_attr',
    is_i18n: false,
  },
  {
    label: 'Family',
    name: 'referenceDetail.family',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'family_attr',
    is_i18n: false,
  },
  {
    label: 'Has Discount',
    name: 'prices.center_0090.discounted',
    weight: 0,
    data_type: 'BOOLEAN',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'has_discount_attr',
    is_i18n: false,
  },
  {
    label: 'Manufacturer',
    name: 'manufacturer.identifier',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'manufacturer_attr',
    is_i18n: false,
  },
  {
    label: 'Price',
    name: 'prices.center_0090.sale_price',
    weight: 0,
    data_type: 'DOUBLE',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'price_attr',
    is_i18n: false,
  },
  {
    label: 'SaleReference',
    name: 'saleReference',
    weight: 0,
    data_type: 'STRING',
    label_attribute: true,
    multiple_cardinality: false,
    id: 'sale_reference',
    is_i18n: false,
  },
  {
    label: 'Seller Company',
    name: 'seller_company',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'seller_company_attr',
    is_i18n: false,
  },
  {
    label: 'Size',
    name: 'referenceDetail.size_code',
    weight: 0,
    data_type: 'STRING',
    label_attribute: false,
    multiple_cardinality: false,
    id: 'size_attr',
    is_i18n: false,
  },
];

describe('AttributeMapper out', () => {
  it('parse a outgoing request as expected', () => {
    const attribute = attributes[0];
    const mapper = new AttributeOutMapper(attribute);

    const attributeDTO = mapper.data;
    expect(attributeDTO.label).toEqual('productId');
    expect(attributeDTO.name).toEqual('productId');
    expect(attributeDTO.weight).toEqual(0);
    expect(attributeDTO.data_type).toEqual('STRING');
    expect(attributeDTO.is_label_attribute).toEqual(false);
    expect(attributeDTO.is_multiple_cardinality).toEqual(false);
    expect(attributeDTO.entity_reference).toBeUndefined();
    expect(attributeDTO.entity_reference_name).toBeUndefined();
    expect(attributeDTO.is_multiple_cardinality).toEqual(false);
  });

  it('parse a outgoing request with entity reference as expected', () => {
    const attribute = attributes[1];
    const mapper = new AttributeOutMapper(attribute);

    const attributeDTO = mapper.data;
    expect(attributeDTO.label).toEqual('MarcaCompleja');
    expect(attributeDTO.name).toEqual('MarcaCompleja');
    expect(attributeDTO.weight).toEqual(0);
    expect(attributeDTO.data_type).toEqual('ENTITY');
    expect(attributeDTO.is_label_attribute).toEqual(false);
    expect(attributeDTO.is_multiple_cardinality).toEqual(false);
    expect(attributeDTO.entity_reference).toEqual('58b5e891-ed0b-4556-8e97-4faa329a1967');
    expect(attributeDTO.entity_reference_name).toEqual('MarcaComplex');
    expect(attributeDTO.is_multiple_cardinality).toEqual(false);
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem = {} as Attribute;

    const mapper = new AttributeOutMapper(remoteItem);
    const attribute = mapper.data;
    expect(attribute.identifier).toBeUndefined();
    expect(attribute.label).toBeUndefined();
    expect(attribute.name).toBeUndefined();
    expect(attribute.weight).toBeUndefined();
    expect(attribute.attribute_label).toBeUndefined();
    expect(attribute.data_type).toBeUndefined();
    expect(attribute.entity_type_id).toBeUndefined();
    expect(attribute.attribute_id).toBeUndefined();
    expect(attribute.is_label_attribute).toBeUndefined();
    expect(attribute.entity_reference).toBeUndefined();
    expect(attribute.entity_reference_name).toBeUndefined();
    expect(attribute.is_multiple_cardinality).toBeUndefined();
    expect(attribute.is_i18n).toBeUndefined();
    expect(attribute.tooltip_label).toBeUndefined();
    expect(attribute.attribute_data_type).toBeUndefined();
    expect(attribute.evaluation_info).toBeUndefined();
  });
});
