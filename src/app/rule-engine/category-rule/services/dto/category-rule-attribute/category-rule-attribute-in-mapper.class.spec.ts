import { CategoryRuleAttributeDTO } from '../../../models/category-rule.dto';
import { AttributeInMapper } from './category-rule-attribute-in-mapper.class';

const attributes: CategoryRuleAttributeDTO[] = [
  {
    label: 'productId',
    name: 'productId',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: '6ec93f23-9770-435f-91e6-8940c8859b60',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'MarcaCompleja',
    name: 'MarcaCompleja',
    weight: 0,
    data_type: 'ENTITY',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    entity_reference: '58b5e891-ed0b-4556-8e97-4faa329a1967',
    entity_reference_name: 'MarcaComplex',
    identifier: 'a642743b-fd52-4724-9a2b-7fdc0dc60df2',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Bar Code',
    name: 'referenceDetail.bar_code',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'bar_code_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Marca',
    name: 'brand.name',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'brand_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Center',
    name: 'center',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'center_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Compañia',
    name: 'referenceDetail.company',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'company_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Departamento',
    name: 'referenceDetail.department',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'department_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Division',
    name: 'merchandise_division',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'division_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Family',
    name: 'referenceDetail.family',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'family_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Has Discount',
    name: 'prices.center_0090.discounted',
    weight: 0,
    data_type: 'BOOLEAN',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'has_discount_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Manufacturer',
    name: 'manufacturer.identifier',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'manufacturer_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Price',
    name: 'prices.center_0090.sale_price',
    weight: 0,
    data_type: 'DOUBLE',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'price_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'SaleReference',
    name: 'saleReference',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: true,
    is_multiple_cardinality: false,
    identifier: 'sale_reference',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Seller Company',
    name: 'seller_company',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'seller_company_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
  {
    label: 'Size',
    name: 'referenceDetail.size_code',
    weight: 0,
    data_type: 'STRING',
    is_label_attribute: false,
    is_multiple_cardinality: false,
    identifier: 'size_attr',
    is_i18n: false,
    is_tree_attribute: false,
  },
];

describe('AttributeMapper out', () => {
  it('parse a outgoing request as expected', () => {
    const attributeDTO = attributes[0];
    const mapper = new AttributeInMapper(attributeDTO);

    const attribute = mapper.data;
    expect(attribute.label).toEqual('productId');
    expect(attribute.name).toEqual('productId');
    expect(attribute.weight).toEqual(0);
    expect(attribute.data_type).toEqual('STRING');
    expect(attribute.label_attribute).toEqual(false);
    expect(attribute.multiple_cardinality).toEqual(false);
    expect(attribute.entity_reference).toBeUndefined();
    expect(attribute.entity_reference_name).toBeUndefined();
    expect(attribute.is_i18n).toEqual(false);
  });

  it('parse a outgoing request with entity reference as expected', () => {
    const attributeDTO = attributes[1];
    const mapper = new AttributeInMapper(attributeDTO);

    const attribute = mapper.data;
    expect(attribute.label).toEqual('MarcaCompleja');
    expect(attribute.name).toEqual('MarcaCompleja');
    expect(attribute.weight).toEqual(0);
    expect(attribute.data_type).toEqual('ENTITY');
    expect(attribute.label_attribute).toEqual(false);
    expect(attribute.multiple_cardinality).toEqual(false);
    expect(attribute.entity_reference).toEqual('58b5e891-ed0b-4556-8e97-4faa329a1967');
    expect(attribute.entity_reference_name).toEqual('MarcaComplex');
    expect(attribute.is_i18n).toEqual(false);
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem = {} as CategoryRuleAttributeDTO;

    const mapper = new AttributeInMapper(remoteItem);
    const attribute = mapper.data;
    expect(attribute.id).toBeUndefined();
    expect(attribute.label).toBeUndefined();
    expect(attribute.name).toBeUndefined();
    expect(attribute.weight).toBeUndefined();
    expect(attribute.attribute_label).toBeUndefined();
    expect(attribute.data_type).toBeUndefined();
    expect(attribute.entity_type_id).toBeUndefined();
    expect(attribute.attribute_id).toBeUndefined();
    expect(attribute.label_attribute).toBeUndefined();
    expect(attribute.entity_reference).toBeUndefined();
    expect(attribute.entity_reference_name).toBeUndefined();
    expect(attribute.multiple_cardinality).toBeUndefined();
    expect(attribute.is_i18n).toBeUndefined();
    expect(attribute.tooltip_label).toBeUndefined();
    expect(attribute.attribute_data_type).toBeUndefined();
  });
});
