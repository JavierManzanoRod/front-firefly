import { CategoryRuleAttributeDTO, CategoryRuleEntityListDTO } from '../../../models/category-rule.dto';
import { CategoryRule } from '../../../models/category-rule.model';
import { CategoryRuleOutMapper } from './category-rule-out-mapper.class';

describe('CategoryRule mapper out', () => {
  it('parse a outgoing request as expected', () => {
    const remoteItem: CategoryRule = {
      id: '032252e3-eafb-4fa8-ac9b-a4693caa769f',
      name: 'Za_Categoria_Complex_VgKewwqAgR',
      folder_id: 'a',
      category_type: 'A',
      node: undefined,
      entity_types: [
        {
          id: '8db1bab6-8e83-4b0c-bcda-68528f03ce47',
          name: 'Za_Product_kRfFr97e4V',
          label: 'Za_Label_Producto_kRfFr97e4V',
          is_define_rules: true,
          attributes: [
            {
              label: 'Nombre',
              name: 'name',
              weight: 0,
              data_type: 'STRING',
              label_attribute: true,
              multiple_cardinality: false,
              id: 'e1ed3c54-5114-437d-ae68-c328005c82fd',
              is_i18n: false,
            },
            {
              label: 'Identificador',
              name: 'productId',
              weight: 0,
              data_type: 'STRING',
              label_attribute: true,
              multiple_cardinality: false,
              id: 'ab9132f8-f433-4544-accd-2a5e3f637c55',
              is_i18n: false,
            },
            {
              label: 'Origen',
              name: 'origin',
              weight: 0,
              data_type: 'STRING',
              label_attribute: true,
              multiple_cardinality: false,
              id: 'a4b593c2-41e0-47df-bae3-6a961611ec56',
              is_i18n: false,
            },
            {
              label: 'Marca',
              name: 'marca',
              weight: 0,
              data_type: 'ENTITY',
              label_attribute: true,
              multiple_cardinality: false,
              entity_reference: 'e2ea2837-2bf1-43bd-b2d4-4afa170ac786',
              entity_reference_name: 'Za_Label_Marca_NJmQRkC82t',
              id: '0406408e-f035-472e-9dd2-ded14500f7dd',
              is_i18n: false,
            },
          ],
        },
        {
          id: 'e2ea2837-2bf1-43bd-b2d4-4afa170ac786',
          name: 'Za_Brand_NJmQRkC82t',
          label: 'Za_Label_Marca_NJmQRkC82t',
          is_define_rules: false,
          attributes: [
            {
              label: 'Nombre_String',
              name: 'name',
              weight: 0,
              data_type: 'STRING',
              label_attribute: true,
              multiple_cardinality: false,
              id: 'f4e8d4b4-087b-4d27-b5f6-018892954c61',
              is_i18n: false,
            },
            {
              label: 'Codigo_Double',
              name: 'codigo',
              weight: 0,
              data_type: 'DOUBLE',
              label_attribute: true,
              multiple_cardinality: false,
              id: '1e8d67d8-cc05-45df-92b3-8351ed5fe50b',
              is_i18n: false,
            },
          ],
        },
      ],
    };
    const mapper = new CategoryRuleOutMapper(remoteItem);
    const categoryRuleDTO = mapper.data;
    expect(categoryRuleDTO.identifier).toEqual('032252e3-eafb-4fa8-ac9b-a4693caa769f');
    expect(categoryRuleDTO.name).toEqual('Za_Categoria_Complex_VgKewwqAgR');
    expect(categoryRuleDTO.category_type).toEqual('A');
    expect(categoryRuleDTO.folder_id).toEqual('a');
    expect(categoryRuleDTO.node).toEqual(undefined);
    const entity = (categoryRuleDTO.entity_types as CategoryRuleEntityListDTO[])[0];
    expect(entity.identifier).toEqual('8db1bab6-8e83-4b0c-bcda-68528f03ce47');
    expect(entity.name).toEqual('Za_Product_kRfFr97e4V');
    expect(entity.label).toEqual('Za_Label_Producto_kRfFr97e4V');
    expect(entity.is_define_rules).toBeTruthy();
    const attribute = (entity.attributes as CategoryRuleAttributeDTO[])[0];

    expect(attribute.label).toEqual('Nombre');
    expect(attribute.name).toEqual('name');
    expect(attribute.weight).toEqual(0);
    expect(attribute.data_type).toEqual('STRING');
    expect(attribute.is_label_attribute).toEqual(true);
    expect(attribute.is_multiple_cardinality).toEqual(false);
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const remoteItem: CategoryRule = {} as CategoryRule;

    const mapper = new CategoryRuleOutMapper(remoteItem);
    const dto = mapper.data;
    expect(dto.identifier).toEqual(undefined);
    expect(dto.name).toBeUndefined();
    expect(dto.category_type).toBeUndefined();
    expect(dto.entity_types).toEqual(undefined);
    expect(dto.identifier).toEqual(undefined);
  });
});
