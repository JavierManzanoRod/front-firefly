import { Condition2Multiple, Conditions2 } from 'src/app/modules/conditions2/models/conditions2.model';
import { CategoryRuleDTO } from '../../../models/category-rule.dto';
import { CategoryRule } from '../../../models/category-rule.model';
import { EntityListOutMapper } from '../category-rule-entity-list/category-rule-entity-list-out-mapper.class';
import { ConditionsDTO } from './../../../../../modules/conditions2/models/conditions2.model';

export class CategoryRuleOutMapper {
  data = {} as CategoryRuleDTO;

  constructor(data: CategoryRule) {
    this.data = {
      identifier: data.id,
      name: data.name,
      category_type: data.category_type,
      folder_id: data.folder_id,
      node: data.node && this.transform(data.node as Conditions2 & Condition2Multiple),
      entity_types: data.entity_types && data.entity_types.map((en) => new EntityListOutMapper(en).data),
    };
  }

  private transform({ node_type, recursive_filter, ...item }: any): ConditionsDTO {
    const response = item;
    if (node_type) {
      response.type = node_type;
    }

    if (recursive_filter) {
      response.is_recursive_filter = recursive_filter;
    }

    if (item.nodes) {
      response.nodes = response.nodes?.map((node: any) => {
        return this.transform(node);
      });
    }

    return {
      attribute: response.attribute,
      type: response.type || response.node_type,
      value: response.value,
      visual_path: response.tooltip_label || response.visual_path,
      reference_id: response.reference_id,
      is_recursive_filter: response.is_recursive_filter || response.recursive_filter,
      nodes: response.nodes,
    };
  }
}
