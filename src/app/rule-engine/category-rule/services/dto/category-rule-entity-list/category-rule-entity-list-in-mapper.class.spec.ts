import { Attribute } from '@core/models/attribute.model';
import { CategoryRuleEntityListDTO } from '../../../models/category-rule.dto';
import { EntityListInMapper } from './category-rule-entity-list-in-mapper.class';

const entity_types: CategoryRuleEntityListDTO[] = [
  {
    identifier: '8db1bab6-8e83-4b0c-bcda-68528f03ce47',
    name: 'Za_Product_kRfFr97e4V',
    label: 'Za_Label_Producto_kRfFr97e4V',
    is_define_rules: true,
    attributes: [
      {
        label: 'Nombre',
        name: 'name',
        weight: 0,
        data_type: 'STRING',
        is_label_attribute: true,
        is_multiple_cardinality: false,
        identifier: 'e1ed3c54-5114-437d-ae68-c328005c82fd',
        is_i18n: false,
        is_tree_attribute: false,
      },
      {
        label: 'Identificador',
        name: 'productId',
        weight: 0,
        data_type: 'STRING',
        is_label_attribute: true,
        is_multiple_cardinality: false,
        identifier: 'ab9132f8-f433-4544-accd-2a5e3f637c55',
        is_i18n: false,
        is_tree_attribute: false,
      },
      {
        label: 'Origen',
        name: 'origin',
        weight: 0,
        data_type: 'STRING',
        is_label_attribute: true,
        is_multiple_cardinality: false,
        identifier: 'a4b593c2-41e0-47df-bae3-6a961611ec56',
        is_i18n: false,
        is_tree_attribute: false,
      },
      {
        label: 'Marca',
        name: 'marca',
        weight: 0,
        data_type: 'ENTITY',
        is_label_attribute: true,
        is_multiple_cardinality: false,
        entity_reference: 'e2ea2837-2bf1-43bd-b2d4-4afa170ac786',
        entity_reference_name: 'Za_Label_Marca_NJmQRkC82t',
        identifier: '0406408e-f035-472e-9dd2-ded14500f7dd',
        is_i18n: false,
        is_tree_attribute: false,
      },
    ],
  },
  {
    identifier: 'e2ea2837-2bf1-43bd-b2d4-4afa170ac786',
    name: 'Za_Brand_NJmQRkC82t',
    label: 'Za_Label_Marca_NJmQRkC82t',
    is_define_rules: false,
    attributes: [
      {
        label: 'Nombre_String',
        name: 'name',
        weight: 0,
        data_type: 'STRING',
        is_label_attribute: true,
        is_multiple_cardinality: false,
        identifier: 'f4e8d4b4-087b-4d27-b5f6-018892954c61',
        is_i18n: false,
        is_tree_attribute: false,
      },
      {
        label: 'Codigo_Double',
        name: 'codigo',
        weight: 0,
        data_type: 'DOUBLE',
        is_label_attribute: true,
        is_multiple_cardinality: false,
        identifier: '1e8d67d8-cc05-45df-92b3-8351ed5fe50b',
        is_i18n: false,
        is_tree_attribute: false,
      },
    ],
  },
];
describe('EntityList in mapper out', () => {
  it('parse a outgoing request as expected', () => {
    const mapper = new EntityListInMapper(entity_types[0]);

    const entity = mapper.data;
    expect(entity.id).toEqual('8db1bab6-8e83-4b0c-bcda-68528f03ce47');
    expect(entity.name).toEqual('Za_Product_kRfFr97e4V');
    expect(entity.label).toEqual('Za_Label_Producto_kRfFr97e4V');
    expect(entity.is_define_rules).toBeTruthy();
    const attribute = (entity.attributes as Attribute[])[0];

    expect(attribute.label).toEqual('Nombre');
    expect(attribute.name).toEqual('name');
    expect(attribute.weight).toEqual(0);
    expect(attribute.data_type).toEqual('STRING');
    expect(attribute.label_attribute).toEqual(true);
    expect(attribute.multiple_cardinality).toEqual(false);
    expect(attribute.is_tree_attribute).toEqual(undefined);
  });

  it('parse an outgoing request with some fields undefined to check if it does not break anything', () => {
    const mapper = new EntityListInMapper({} as CategoryRuleEntityListDTO);
    const dto = mapper.data;
    expect(dto.id).toEqual(undefined);
    expect(dto.name).toBeUndefined();
    expect(dto.label).toEqual(undefined);
    expect(dto.attributes).toEqual(undefined);
  });
});
