import { EntityList } from '@core/models/attribute.model';
import { CategoryRuleEntityListDTO } from '../../../models/category-rule.dto';
import { AttributeInMapper } from '../category-rule-attribute/category-rule-attribute-in-mapper.class';

export class EntityListInMapper {
  data = {} as EntityList;

  constructor(remoteData: CategoryRuleEntityListDTO) {
    this.data = {
      id: remoteData.identifier,
      label: remoteData.label,
      name: remoteData.name,
      is_define_rules: remoteData.is_define_rules,
      attributes: remoteData.attributes?.map((at) => new AttributeInMapper(at).data),
    };
  }
}
