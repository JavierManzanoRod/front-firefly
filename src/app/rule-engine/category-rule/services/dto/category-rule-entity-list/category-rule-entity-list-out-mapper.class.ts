import { EntityList } from '@core/models/attribute.model';
import { CategoryRuleEntityListDTO } from '../../../models/category-rule.dto';
import { AttributeOutMapper } from '../category-rule-attribute/category-rule-attribute-out-mapper.class';

export class EntityListOutMapper {
  data = {} as CategoryRuleEntityListDTO;

  constructor(remoteData: EntityList) {
    this.data = {
      identifier: remoteData.id,
      label: remoteData.label,
      name: remoteData.name,
      is_define_rules: remoteData.is_define_rules,
      attributes: remoteData?.attributes?.map((at) => new AttributeOutMapper(at).data),
    };
  }
}
