import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { UpdateResponse } from '@core/base/api.service';
import { GenericApiRequest, GenericApiResponse } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ContentGroupDetailDTO, ContentGroupDTO } from 'src/app/catalog/content-group/model/content-group-detail-dto.models';
import { ContentGroupTree } from 'src/app/catalog/content-group/model/content-group-tree.models';
import { ContentGroup, ContentGroupDetail } from 'src/app/catalog/content-group/model/content-group.models';
import { ContentGroupDetailInMapper } from 'src/app/catalog/content-group/services/dtos/content-group-detail-in-mapper-class';
import { ContentGroupDetailOutMapper } from 'src/app/catalog/content-group/services/dtos/content-group-detail-out-mapper-class';
import { ContentGroupOutMapper } from 'src/app/catalog/content-group/services/dtos/content-group-out-mapper-class';
import { FF_API_PATH_VERSION_ADMIN_GROUP_FOLDERS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroupFoldersServiceGeneral } from 'src/app/shared/services/apis/admin-group-folders-general.service';

@Injectable({
  providedIn: 'root',
})
export class CategoryRuleFolderService extends AdminGroupFoldersServiceGeneral<ContentGroupDetail> {
  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_ADMIN_GROUP_FOLDERS) public version: string) {
    super(http, version);
  }

  listFolders(): Observable<ContentGroupTree[]> {
    return this.http.get<ContentGroup[]>(this.urlBase).pipe(
      map((response) => {
        return response.map((res) => new ContentGroupOutMapper(res as unknown as ContentGroupDTO).data);
      })
    );
  }

  list(filter: GenericApiRequest): Observable<GenericApiResponse<ContentGroupDetail>> {
    return super.list(filter).pipe(
      map((response) => {
        response.content = response.content.map((res) => new ContentGroupDetailOutMapper(res as unknown as ContentGroupDetailDTO).data);
        return response;
      })
    );
  }

  detail(id: string): Observable<ContentGroupDetail> {
    const options = {
      headers: {
        'Accept-Language': 'es-ES,es;q=0.9,en;q=0.8',
      },
      params: {
        folder_content_type: 'CATEGORY_RULES',
      },
    };
    const urlDetail = `${this.urlBase}/${id}`;
    return this.http
      .get<ContentGroupDetail>(urlDetail, options)
      .pipe(map((detail) => new ContentGroupDetailOutMapper(detail as unknown as ContentGroupDetailDTO).data));
  }

  update(data: ContentGroupDetail): Observable<UpdateResponse<ContentGroupDetail>> {
    return super.update(new ContentGroupDetailInMapper(data).data as unknown as ContentGroupDetail).pipe(
      map((res: UpdateResponse<ContentGroupDetail>) => {
        return {
          ...res,
          data: new ContentGroupDetailOutMapper(res.data as unknown as ContentGroupDetailDTO).data as unknown as ContentGroupDetail,
        };
      })
    );
  }

  post(data: ContentGroupDetail): Observable<UpdateResponse<ContentGroupDetail>> {
    return super.post(new ContentGroupDetailInMapper(data).data as unknown as ContentGroupDetail).pipe(
      map((res: UpdateResponse<ContentGroupDetail>) => {
        return {
          ...res,
          data: new ContentGroupDetailOutMapper(res.data as unknown as ContentGroupDetailDTO).data as unknown as ContentGroupDetail,
        };
      })
    );
  }
}
