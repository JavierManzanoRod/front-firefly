import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { CategoryRuleDetailContainerComponent } from './containers/category-rule-detail-container';
import { CategoryRuleListContainerComponent } from './containers/category-rule-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: CategoryRuleListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
      breadcrumb: [
        {
          label: '',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'rule/:routeId',
        component: CategoryRuleDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          breadcrumb: [
            {
              label: 'SHIPPING_COST.BREADCRUMB_LIST_TITLE',
              url: '/rule-engine/category-rule',
            },
            {
              label: 'SHIPPING_COST.SHIPPING_COST',
              url: '',
            },
          ],
        },
      },
      {
        path: 'rule/:routeId/:id',
        component: CategoryRuleDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          breadcrumb: [
            {
              label: 'SHIPPING_COST.BREADCRUMB_LIST_TITLE',
              url: '/rule-engine/category-rule',
            },
            {
              label: 'SHIPPING_COST.SHIPPING_COST',
              url: '',
            },
          ],
        },
      },
    ],
  },
  {
    path: ':routeId',
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
    },
    canDeactivate: [CanDeactivateGuard],
    component: CategoryRuleListContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoryRuleModuleRoutingModule {}
