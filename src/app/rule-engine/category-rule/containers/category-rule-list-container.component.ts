/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, QueryList, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '@core/base/api.service';
import { TranslateService } from '@ngx-translate/core';
import { first, retry } from 'rxjs/operators';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { FoldersExplodedTreeComponent } from 'src/app/modules/exploded-tree/components/adapters/folders-exploded-tree/folders-exploded-tree.component';
import { ExplodedTreeNodeComponent } from 'src/app/modules/exploded-tree/components/exploded-tree-node/exploded-tree-node.component';
import {
  ExplodedActionEvent,
  ExplodedChangeEvent,
  ExplodedEvent,
  ExplodedTree,
  ExplodedTreeAction,
  ExplodedTreeNode,
  ExplodedTreeNodeType,
} from 'src/app/modules/exploded-tree/models/exploded-tree.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { CategoryRuleFolderDetailComponent } from '../components/category-rule-detail-folder/category-rule-detail-folder.component';
import { CategoryRuleFolderService } from '../services/category-rule-folder.service';
import { CategoryRuleService } from '../services/category-rule.service';
import { CategoryRuleDetailContainerComponent } from './category-rule-detail-container';
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <router-outlet (activate)="activate($event)" (deactivate)="deactivate()"></router-outlet>
    <div [hidden]="hide">
      <ff-page-header [pageTitle]="'CATEGORY_RULE.HEADER_MENU'"></ff-page-header>
      <ngx-simplebar class="page-container ">
        <div class="row">
          <div class="col-4 h-100">
            <ff-folders-exploded-tree
              [tree]="searchResult || tree"
              (clickAction)="clickAction($event)"
              (clickNode)="clickNode($event)"
              (changeNode)="changeNode($event)"
              (searchNodes)="searchNodes($event)"
            ></ff-folders-exploded-tree>
          </div>
          <div class="col-8">
            <ff-category-rule-folder-detail
              (valueChange)="update($event)"
              (deleteFolder)="deleteFolder($event)"
              (folderChange)="folderChange($event)"
              (cleanInput)="cleanInput($event)"
              [folder]="detailSection"
              [showConfigSection]="showConfigSection"
              [hidden]="searchResult || explodedTree?.loading"
            ></ff-category-rule-folder-detail>
          </div>
        </div>
      </ngx-simplebar>
    </div>
  `,
  styleUrls: ['./category-rule-list-container.component.scss'],
  providers: [{ provide: ApiService, useClass: CategoryRuleFolderService }],
})
export class CategoryRuleListContainerComponent implements OnInit {
  @ViewChild(CategoryRuleFolderDetailComponent) detailFolder!: CategoryRuleFolderDetailComponent;
  @ViewChild(FoldersExplodedTreeComponent) explodedTree!: FoldersExplodedTreeComponent;
  @Input() link = '/rule-engine/category-rule/';

  tree: ExplodedTree[] = [];
  searchResult?: ExplodedTree[];
  detailSection?: ExplodedTreeNodeComponent;
  showConfigSection = false;
  hide = false;
  nodeClicked!: ExplodedTreeNodeComponent | undefined;
  nodeClickedMode!: 'edit' | 'new' | undefined;
  expandedPaths: string[] = [];
  activedNode!: ExplodedTreeNodeComponent | null;

  constructor(
    public apiService: CategoryRuleFolderService,
    public service: CategoryRuleService,
    public crud: CrudOperationsService,
    public router: Router,
    private change: ChangeDetectorRef,
    public toastService: ToastService,
    private translate: TranslateService
  ) {
    this.apiService.listFolders().subscribe((content) => {
      content?.sort(this.sort);
      this.tree = this.explodedTree?.defaultTree?.concat(content);

      this.change.markForCheck();
    });
  }

  ngOnInit() {}

  changeNode({ target, data, value }: ExplodedChangeEvent<ExplodedTreeNodeComponent>) {
    if ((!data.id && data.value?.create) || (!data.id && data.value?.child)) {
      data.name = value;
      if (value) {
        // create children folder
        if (typeof target.parent === 'string' || (target.parent?.data.id && target.actions.length)) {
          const folderToSend = {
            name: value ? value : null,
            parent_id: typeof target.parent === 'string' ? target.parent : target.parent.data.id,
          };
          this.crud
            .updateOrSaveModal(
              {
                methodToApply: 'POST',
                apiService: this.apiService,
                captureCancel: true,
              },
              folderToSend
            )
            .pipe(
              retry() //wait for modal response if request fails
            )
            .subscribe(
              (response) => {
                if (!response) {
                  target.editMode(true);
                } else {
                  if (target.parent?.data?.children && target.parent?.data?.children[1]?.icon === 'icon-icon-warning') {
                    target.parent?.data?.children?.splice(0, 2);
                  } else if (target.parent?.data?.children && target.parent?.data?.children[0]?.value?.child) {
                    target.parent?.data?.children?.splice(0, 1);
                  }
                  target.parent?.data?.children?.unshift(response.data);
                  this.explodedTree.update();
                }
              },
              (error) => {
                if (error) {
                  target.editMode(true);
                }
              }
            );
          this.explodedTree.update();
        } else {
          // create parent folder
          const folderToSend = {
            name: value ? value : null,
          };
          this.crud
            .updateOrSaveModal(
              {
                methodToApply: 'POST',
                apiService: this.apiService,
                captureCancel: true,
              },
              folderToSend
            )
            .pipe(
              retry() //wait for modal response if request fails
            )
            .subscribe(
              (response) => {
                if (!response) {
                  target.editMode(true);
                } else {
                  target.parent?.collapse();
                  data.name = '';
                  // target.loading = true;
                  this.tree.splice(1, 0, response.data);
                  this.explodedTree.update();
                }
              },
              (error) => {
                if (error) {
                  target.editMode(true);
                }
              }
            );
        }
      }
    } else {
      //edit folder
      const oldName = data.name;
      data.name = value || data.name;
      this.crud
        .updateOrSaveModal(
          {
            methodToApply: 'PUT',
            apiService: this.apiService,
            captureCancel: true,
          },
          data
        )
        .pipe(
          retry() //wait for modal response if request fails
        )
        .subscribe(
          (response) => {
            if (!response) {
              data.name = oldName;
              this.explodedTree.update();
            } else {
              data = response.data;
              this.explodedTree.update();
            }
          },
          (error) => {
            if (error) {
              target.editMode(true);
            }
          }
        );
    }
  }

  searchNodes(value: string) {
    this.explodedTree.loading = true;

    if (value) {
      this.expandedPaths = this.explodedTree.explodedTree.getExpandedNodes().map((node) => node.getVisualPath(true));
      this.activedNode = this.explodedTree.explodedTree.active;
      this.service.list({ name: value, size: 999999 }).subscribe((response) => {
        if (response.content.length) {
          this.searchResult = response.content.map((rule) => ({ ...rule, value: { isRule: true } }));
        } else {
          this.searchResult = [
            {
              icon: 'icon-icon-warning',
              node_type: ExplodedTreeNodeType.WARNING,
              label: this.translate.instant('COMMON_ERRORS.ERROR_404'),
            },
          ];
        }

        this.explodedTree.loading = false;
        this.change.markForCheck();
        this.showConfigSection = false;
      });
    } else {
      this.searchResult = undefined;

      if (this.expandedPaths) {
        this.explodedTree.explodedTree.updateVisualPaths(this.expandedPaths);
        this.explodedTree.explodedTree.active = this.activedNode;

        this.activedNode = null;
        this.expandedPaths = [];
      }

      this.explodedTree.loading = false;
      this.change.markForCheck();
      setTimeout(() => {
        this.explodedTree.update();
        const active = this.explodedTree.explodedTree.getActiveNode();
        if (active) {
          this.detailSection = active;
          this.showConfigSection = true;
          this.change.markForCheck();
        }
      });
    }
  }

  clickAction({ target, action }: ExplodedActionEvent<ExplodedTreeNodeComponent>) {
    if (action.type === 'edit' || action.type === 'folder') {
      if (!target.data.children) {
        this.apiService.detail(target.data.id as string).subscribe((response) => {
          target.setLoading(false);
          target.data.children = (target.data.children || []).concat(response.folders || []).concat(
            (response.category_rules || []).map((rule) => {
              return { ...rule, id: rule.id, value: { isRule: true } };
            })
          );
          target.data.children?.unshift({
            placeholder: this.translate.instant('CATEGORY_RULE.CHILD_FOLDER'),
            icon: 'icon-icon-folder-closed',
            class: 'new-folder',
            value: { child: true },
          });
          target.setLoading(false);
          setTimeout(() => {
            target.nodes?.get(0)?.editMode(true);
          });
        });
      }
      if (action.type === 'edit') {
        target.editMode(true);
      } else {
        this.detailFolder.isEditable = false;
        this.nodeClicked?.clearAllMessages();
        target.expand();
        if (
          (target.data?.children && !target.data?.children[0]?.value) ||
          (target.data?.children && target.data?.children[0]?.value.isRule)
        ) {
          target.data.value = { child: true };
          target.data.children?.unshift({
            placeholder: this.translate.instant('CATEGORY_RULE.CHILD_FOLDER'),
            icon: 'icon-icon-folder-closed',
            class: 'new-folder',
            value: { child: true },
          });
          target.setLoading(false);

          setTimeout(() => {
            target.nodes?.get(0)?.editMode(true);
          });
        }
      }
    }

    if (action.type === 'remove') {
      if (target.data.id && !target.data.value?.isRule) {
        this.crud
          .deleteActionModal(this.apiService, target.data, (e: HttpErrorResponse, defaultMessage: string) => {
            if (e.error.error.detail.error_code === 'BUS-403') {
              return 'CONTENTGROUP.ERROR.DELETE';
            }
            if (e.error.error.detail.error_code === 'BUS-404') {
              return 'COMMON_ERRORS.ERROR_CONTENT';
            }
            return defaultMessage || 'COMMON_ERRORS.DELETE_ERROR';
          })
          .subscribe(() => {
            target.remove();
            this.detailSection = undefined;
            if (!target.parent?.data.children?.length) {
              target?.parent?.setWarning('CONTENTGROUP.EMPTY');
            }
            target.parent?.click();
            target.parent?.expand();
            this.change.markForCheck();
          });
      } else {
        this.crud
          .deleteActionModal(this.service, target.data, (e: HttpErrorResponse, defaultMessage: string) => {
            if (e.error.error.detail.error_code === 'BUS-410') {
              return 'CATEGORY_RULE.BUS-410';
            }
            return defaultMessage || 'COMMON_ERRORS.DELETE_ERROR';
          })
          .subscribe(() => {
            target.remove();
            if (!target.parent?.data.children?.length) {
              target?.parent?.setWarning('CONTENTGROUP.EMPTY');
            }
            this.change.markForCheck();
          });
      }
    }

    if (action.type === 'new') {
      this.nodeClicked = target;
      this.nodeClicked?.click();
      this.nodeClickedMode = 'new';
      this.router.navigate([`${this.link}/rule/${target.data.id}`]);
    }

    if (action.type === 'detail') {
      this.nodeClicked = target;
      this.nodeClickedMode = 'edit';
      this.router.navigate([`${this.link}/rule/${target.data.folder_id || target.parent?.data.id}/${target.data.id}`]);
    }
  }

  clickNode({ target, data }: ExplodedEvent<ExplodedTreeNodeComponent>) {
    this.detailSection = target;
    this.nodeClicked = target;
    this.detailFolder.isEditable = false;

    if (target.data?.children && target.data?.children[0]?.node_type === 'WARNING' && target.parent) {
      target.parent.closestExpand();
    }

    if (data.id && !data.value?.isRule) {
      if (target.data?.children && target.data?.children[0]?.value?.child) {
        delete target.data.children;
      }

      if (!data.children) {
        target.setLoading(true);
        this.apiService.detail(data.id).subscribe((response) => {
          data.children = (data.children || []).concat(response.folders || []).concat(
            (response.category_rules || []).map((rule) => {
              return { ...rule, id: rule.identifier, value: { isRule: true }, parent_id: data.id };
            })
          );

          if (!target.data?.children?.length) {
            target.setWarning('CONTENTGROUP.EMPTY');
            target.click();
            target.parent?.expand();
          }

          target.expand();

          target.setLoading(false);
          this.explodedTree.update();
          this.showConfigSection = true;
        });
      }
    } else if (data.value?.create) {
      this.detailSection = undefined;
    }
  }

  sort(a: ExplodedTree, b: ExplodedTree) {
    if (!a.name || !b.name) {
      return 0;
    }

    if (a.name.toLowerCase() > b.name.toLowerCase()) {
      return 1;
    }
    if (a.name.toLowerCase() < b.name.toLowerCase()) {
      return -1;
    }
    return 0;
  }

  update(event: any) {
    if (event) {
      this.explodedTree.update();
      this.nodeClicked?.clearAllMessages();
    }
  }

  deleteFolder(event: ExplodedTreeNodeComponent) {
    const type = {
      type: 'remove',
      icon: 'icon-trash',
    } as ExplodedTreeAction;

    this.clickAction({ target: event, action: type } as any);
  }

  folderChange(element: ExplodedTreeNodeComponent) {
    this.clickNode({ target: element, data: element.data });
  }

  cleanInput(event: any) {
    if (event) {
      if (this.nodeClicked?.data.children && this.nodeClicked?.data?.children[0]?.class === 'new-folder') {
        this.nodeClicked.data.children.splice(0, 1);
        this.explodedTree.update();
      }
    }
  }

  activate(detail: CategoryRuleDetailContainerComponent) {
    this.hide = true;
    detail.changed.pipe(first()).subscribe((data) => {
      data = { ...data, value: { isRule: true } };

      if (this.nodeClickedMode === 'new') {
        data.parent_id = data.folder_id;
        this.nodeClicked?.clearAllMessages();

        this.nodeClicked?.data.children?.push(data);
      } else if (this.nodeClickedMode === 'edit' && this.nodeClicked && this.nodeClicked.data) {
        this.nodeClicked.data = Object.assign(this.nodeClicked.data, data);
      }

      this.explodedTree.update();
      this.nodeClicked = undefined;
      this.nodeClickedMode = undefined;
    });

    detail.deleted.pipe(first()).subscribe((v: ExplodedTreeNode) => {
      if (!this.nodeClicked && !this.nodeClickedMode) {
        const lastItemAdded = this.explodedTree.explodedTree.active?.nodes?.last;
        if (v.id === lastItemAdded?.data.id) {
          lastItemAdded?.remove();
        }
        if (!this.explodedTree.explodedTree.active?.data.children?.length) {
          this.explodedTree.explodedTree.active?.setWarning('CONTENTGROUP.EMPTY');
        }
      }
      if (this.nodeClickedMode === 'edit' && this.nodeClicked) {
        this.nodeClicked.remove();
        if (!this.nodeClicked.parent?.data.children?.length) {
          this.nodeClicked?.parent?.setWarning('CONTENTGROUP.EMPTY');
        }
        this.nodeClicked = undefined;
        this.nodeClickedMode = undefined;
      }
    });

    detail.cloned.pipe(first()).subscribe((data) => {
      if (this.nodeClickedMode === 'edit' && this.nodeClicked) {
        data = { ...data, value: { isRule: true } };
        this.nodeClicked?.parent?.data.children?.push(data);

        this.explodedTree.update();
        this.nodeClicked = undefined;
        this.nodeClickedMode = undefined;
      }
    });
  }

  deactivate() {
    this.hide = false;
  }
}
