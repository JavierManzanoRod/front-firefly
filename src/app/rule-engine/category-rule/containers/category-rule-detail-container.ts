/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { CanComponentDeactivate } from '@core/guards/can-deactivate-guard';
import { Observable, of, throwError } from 'rxjs';
import { catchError, delay, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { ConfigurationDefinition } from 'src/app/configuration/models/configuration.model';
import { FF_ENTITY_TYPE_ID_OFFER } from 'src/app/configuration/tokens/configuracion';
import { CrudOperationsService, groupType } from 'src/app/core/services/crud-operations.service';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { EntityTypeService } from 'src/app/shared/services/apis/entity-type/entity-type.service';
import { CategoryRule, InputCategoryDetailComponent } from '../models/category-rule.model';
import { CategoryRuleService } from '../services/category-rule.service';
import { CategoryRuleDetailComponent } from './../components/category-rule-detail/category-rule-detail.component';

@Component({
  selector: 'ff-category-rule-detail-container',
  template: `<ff-page-header
      [pageTitle]="id ? ('CATEGORY_RULE.HEADER_DETAIL' | translate) : ('CATEGORY_RULE.HEADER_NEW' | translate)"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'CATEGORY_RULE.BREAD_CRUMB_TITLE' | translate"
      *ngIf="loading"
    >
      <button class="btn btn-dark mr-2" (click)="clone(detail)" data-cy="clone-rule" *ngIf="id">
        {{ 'COMMON.CLONE' | translate }}
      </button>
      <button class="btn btn-dark mr-2" (click)="delete(detail)" data-cy="remove-rule" *ngIf="id">
        {{ 'COMMON.DELETE' | translate }}
      </button>
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-category-rule-detail
        (formSubmit)="handleSubmitForm($event)"
        (delete)="delete($event)"
        (cancel)="cancel()"
        [data]="data$ | async"
        #detailElement
      >
      </ff-category-rule-detail>
    </ngx-simplebar> `,
})
export class CategoryRuleDetailContainerComponent
  extends BaseDetailContainerComponent<CategoryRule>
  implements OnInit, CanComponentDeactivate
{
  @ViewChild('detailElement') categoryDetailElement!: CategoryRuleDetailComponent;
  urlListado = 'rule-engine/category-rule';
  data$!: Observable<InputCategoryDetailComponent>;
  routeId = '';

  constructor(
    public activeRoute: ActivatedRoute,
    public apiService: CategoryRuleService,
    public router: Router,
    private entityTypeService: EntityTypeService,
    public menuLeft: MenuLeftService,
    public crud: CrudOperationsService,
    public toastService: ToastService,
    @Inject(FF_ENTITY_TYPE_ID_OFFER)
    public offerId: ConfigurationDefinition['entityTypeIdOffer']
  ) {
    super(crud, menuLeft, activeRoute, router, apiService, toastService);
  }

  ngOnInit() {
    this.data$ = this.activeRoute.params.pipe(
      tap(() => (this.loading = true)),
      switchMap(({ id, routeId }: Params) => {
        this.routeId = routeId;
        if (id) {
          this.id = id;
          return this.apiService.detail(id).pipe(tap((detail) => (this.detail = detail)));
        } else {
          return of(this.apiService.emptyRecord()).pipe(delay(250));
        }
      }),
      mergeMap((categoryRule) => {
        categoryRule.folder_id = this.routeId;
        this.name = categoryRule && categoryRule.name;
        const idEntityType = (categoryRule.entity_types && categoryRule.entity_types[0] && categoryRule.entity_types[0].id) || this.offerId;
        if (idEntityType) {
          return this.entityTypeService.detail(idEntityType).pipe(
            map((entityType) => {
              return { categoryRule, entityType } as any;
            })
          );
        } else {
          return of({ categoryRule, entityType: null });
        }
      }),
      catchError((err) => {
        this.router.navigateByUrl(this.urlListado).then(() => {
          this.toastService.error('COMMON_ERRORS.NOT_FOUND_DESCRIPTION', 'COMMON_ERRORS.NOT_FOUND_TITLE');
        });
        return throwError(err);
      })
    );
  }

  handleSubmitForm(itemToSave: CategoryRule) {
    itemToSave.folder_id = this.categoryDetailElement.getFolderId(itemToSave);
    this.crudSrv
      .updateOrSaveModal(
        {
          ...this,
          methodToApply: itemToSave.id ? 'PUT' : 'POST',
        },
        itemToSave,
        groupType.RULE,
        (e, defaultMessage) => {
          if (e.error?.error?.detail?.validations?.find((c: { code: string; message: string }) => c.code === 'BUS-401')) {
            return 'COMMON_ERRORS.BUS-401';
          }
          if (e.error?.error?.detail?.validations?.find((c: { code: string; message: string }) => c.code === 'BUS-450')) {
            return 'COMMON_ERRORS.BUS-450';
          }
          return defaultMessage;
        }
      )
      .pipe(
        tap((response) => {
          this.categoryDetailElement.form.markAsPristine();
          this.changed.emit(response.data);
          if (!itemToSave.id) {
            this.router.navigate([`${this.urlListado}/rule/${itemToSave.folder_id}/${response.data?.id}`]);
          }
        }),
        catchError((err: HttpErrorResponse) => {
          if (err.status === 409) {
            this.categoryDetailElement.form.setErrors({ error_409: true });
          }
          return throwError(err);
        })
      )
      .subscribe();
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.activeRoute.parent });
  }

  clone(detail: CategoryRule | undefined) {
    if (detail) {
      detail = this.categoryDetailElement.getDataToSend();
    }
    if (this.categoryDetailElement?.form?.dirty) {
      this.toastService.error('COMMON_ERRORS.CANNOT_CLONE');
      return;
    }
    if (this.categoryDetailElement?.form?.valid) {
      super.clone(detail, `${this.urlListado}/rule/${detail?.folder_id}`);
    } else {
      this.crudSrv.toast.warning('COMMON_ERRORS.CLONE_FORM_ERROR');
    }
  }

  delete(itemToDelete: CategoryRule | undefined) {
    if (itemToDelete) {
      this.crudSrv
        .deleteActionModal(this.apiService, itemToDelete, (e: HttpErrorResponse, defaultMessage: string) => {
          if (e.error.error.detail.error_code === 'BUS-410') {
            return 'CATEGORY_RULE.BUS-410';
          }
          return defaultMessage || 'COMMON_ERRORS.DELETE_ERROR';
        })
        .pipe(
          tap(() => {
            this.deleted.emit(itemToDelete);
            if (this.detailElement) {
              this.detailElement.haveChanges = false;
            }
            this.router.navigateByUrl(this.urlListado);
          })
        )
        .subscribe();
    }
  }
  canDeactivate(): UICommonModalConfig | false {
    if (this.categoryDetailElement?.form.dirty) {
      if (!this.detail || JSON.stringify(this.detail) === '{}') {
        return {};
      }
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.categoryDetailElement?.data.categoryRule.name,
      };
    }
    return false;
  }
}
