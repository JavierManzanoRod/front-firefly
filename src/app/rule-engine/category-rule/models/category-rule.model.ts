import { EntityList } from '@core/models/attribute.model';
import { EntityType } from '@core/models/entity-type.model';
import { GenericApiResponse } from '@model/base-api.model';
import { Conditions2 } from 'src/app/modules/conditions2/models/conditions2.model';

export interface CategoryRule {
  id?: string;
  name: string;
  category_type: string;
  folder_id: string;
  node: Conditions2 | undefined;
  entity_types?: EntityList[];
  admin_group_type_id?: string;
}

export type CategoryRuleResponse = GenericApiResponse<CategoryRule>;

export interface InputCategoryDetailComponent {
  categoryRule: CategoryRule;
  entityType: EntityType;
}
