import { GenericApiResponse } from '@model/base-api.model';
import { ConditionsDTO } from 'src/app/modules/conditions2/models/conditions2.model';

export interface CategoryRuleDTO {
  identifier?: string;
  name: string;
  category_type?: string;
  folder_id: string;
  node: ConditionsDTO | undefined;
  entity_types?: CategoryRuleEntityListDTO[];
}

export interface NodeDTO {
  [key: string]: any;
  type: string;
}

export type CategoryRuleResponseDTO = GenericApiResponse<CategoryRuleDTO>;

export interface CategoryRuleEntityListDTO {
  identifier?: string;
  is_define_rules: boolean;
  name: string;
  label?: string;
  attributes?: CategoryRuleAttributeDTO[];
}

export interface CategoryRuleAttributeDTO {
  identifier: string;
  label: string;
  name: string;
  weight?: number;
  attribute_label?: string;
  data_type: 'STRING' | 'DOUBLE' | 'BOOLEAN' | 'ENTITY' | 'EMBEDDED' | 'DATE_TIME';
  entity_type_id?: string;
  attribute_id?: string;
  is_label_attribute?: boolean;
  entity_reference?: string;
  entity_reference_name?: string;
  is_multiple_cardinality?: boolean;
  is_tree_attribute: boolean;
  is_i18n?: boolean;
  tooltip_label?: string;
  attribute_data_type?: string;
  evaluation_info?: {
    path: string;
  };
  /**
   * virtual field for conditions component
   */
  source?: string;
  value?: any;
}
