import { Component, Input } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { User } from '@model/user';
import { TranslateService } from '@ngx-translate/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { of, Subject } from 'rxjs';
import { UserService } from 'src/app/shared/services/user.service';
import { DefaultLayoutComponent } from './default-layout.component';

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}

  public get(key: any): any {
    of(key);
  }
}

class OAuthServiceStub {
  public configure(data?: any) {}

  public setupAutomaticSilentRefresh(data?: any) {}

  public tryLogin(): Promise<boolean> {
    return Promise.resolve(true);
  }

  public getIdentityClaims() {
    return {} as User;
  }
}

@Component({
  selector: 'ff-menu-left',
  template: '',
})
class MockMenuLeftComponent {
  @Input() isMenuOpen!: boolean;
}

@Component({
  selector: 'ff-menu-top',
  template: '',
})
class MockMenuTopComponent {
  @Input() isMenuOpen!: boolean;
  @Input() user!: User;
}

describe('DefaultComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [DefaultLayoutComponent, MockMenuTopComponent, MockMenuLeftComponent],
        imports: [RouterTestingModule],
        providers: [
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: OAuthService, useClass: OAuthServiceStub },
          {
            provide: UserService,
            useFactory: () => ({
              userSubject: new Subject<User>(),
              getUser: () => null,
            }),
          },
        ],
      }).compileComponents();
    })
  );

  it('should create the app', () => {
    const fixture = TestBed.createComponent(DefaultLayoutComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should render the menuTop', () => {
    const fixture = TestBed.createComponent(DefaultLayoutComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('ff-menu-top')).not.toBe(null);
  });

  it('should render the menuLeft', () => {
    const fixture = TestBed.createComponent(DefaultLayoutComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('ff-menu-left')).not.toBe(null);
  });
});
