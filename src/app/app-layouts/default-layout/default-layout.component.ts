import { Component, OnInit } from '@angular/core';
import { User } from '@model/user';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'ff-root',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss'],
})
export class DefaultLayoutComponent implements OnInit {
  isMenuOpen = true;
  user: User = this.userService.getUser();

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.userSubject.subscribe((user) => {
      this.user = user;
    });
  }
}
