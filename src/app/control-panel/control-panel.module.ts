import { NgModule } from '@angular/core';
import { ControlPanelRoutingModule } from './control-panel-routing.module';

@NgModule({
  imports: [ControlPanelRoutingModule],
  declarations: [],
  providers: [],
  exports: [],
})
export class ControlPanelModule {}
