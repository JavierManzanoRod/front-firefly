import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { ToastrModule } from 'ngx-toastr';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { ConditionsBasicAndAdvanceModule } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.module';
import { Conditions2Module } from 'src/app/modules/conditions2/conditions2.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { UiModalSelectModule } from 'src/app/modules/ui-modal-select/ui-modal-select.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { GenericListComponentModule } from '../../modules/generic-list/generic-list.module';
import { ControlPanelSharedModule } from '../shared/control-panel-shared.module';
import { SizeGuideDetailContainerComponent } from './containers/size-guide-detail-container.component';
import { SizeGuideListContainerComponent } from './containers/size-guide-list-container.component';
import { SizeGuideRoutingModule } from './size-guide-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    Conditions2Module,
    SizeGuideRoutingModule,
    TypeaheadModule.forRoot(),
    CommonModalModule,
    CoreModule,
    ToastrModule.forRoot({
      autoDismiss: false,
      timeOut: 2000,
      closeButton: true,
      positionClass: 'toast-top-full-width',
      enableHtml: true,
    }),
    FormErrorModule,
    ChipsControlModule,
    SimplebarAngularModule,
    CoreModule,
    DatetimepickerModule,
    UiModalSelectModule,
    ControlPanelSharedModule,
    GenericListComponentModule,
    ConditionsBasicAndAdvanceModule,
  ],
  declarations: [SizeGuideListContainerComponent, SizeGuideDetailContainerComponent],
  providers: [],
  exports: [],
})
export class SizeGuideModule {}
