import { Component, Inject, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { SortApiType } from '@core/models/sort.model';
import { FF_SIZE_GUIDE } from 'src/app/configuration/tokens/admin-group-type.token';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { GenericListConfig, TypeSearch } from '../../../modules/generic-list/models/generic-list.model';
import { SizeGuideService } from '../services/size-guide.service';

@Component({
  selector: 'ff-spenter-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header pageTitle="APP_MENU.SIZE_GUIDE">
        <a [routerLink]="['/control-panel/size-guide/new-rule']" class="btn btn-primary">
          {{ 'SPENTER.CREATE_RULE_BUTTON' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [route]="'/control-panel/size-guide/view/'"
                [showPagination]="true"
                [arrayKeys]="listConfigs"
                [checkDateActive]="checkDateActive"
                [loading]="loading"
                [page]="page"
                [valueSearch]="searchName"
                [currentData]="currentData"
                (delete)="delete($event)"
                (pagination)="handlePagination($event)"
                [list]="list$ | async"
                [type]="type"
                [title]="''"
                [placeHolder]="'SIZE_GUIDE.SEARCH_PLACEHOLDER' | translate"
                (search)="search($event)"
                (sortEvent)="sort($event)"
              ></ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class SizeGuideListContainerComponent extends BaseListContainerComponent<AdminGroup> implements OnInit {
  hide = false;
  listConfigs: GenericListConfig[] = [
    { key: 'name', headerName: 'SIZE_GUIDE.TABLE_NAME', showOrder: true },
    { key: 'start_date', headerName: 'SIZE_GUIDE.TABLE_INIT_DATE', canFormatDate: true, formatDate: 'dd-MM-yyyy H:mm' },
    { key: 'end_date', headerName: 'SIZE_GUIDE.TABLE_END_DATE', canFormatDate: true, formatDate: 'dd-MM-yyyy H:mm' },
    { key: 'active', headerName: 'SIZE_GUIDE.TABLE_ACTIVE', canActiveClass: true },
  ];
  type = TypeSearch.simple;

  constructor(
    public apiService: SizeGuideService,
    public crudOperationsSrv: CrudOperationsService,
    @Inject(FF_SIZE_GUIDE) public typeSizeGuide: string
  ) {
    super(crudOperationsSrv, apiService);
  }

  ngOnInit() {
    this.sort({ name: 'name', type: SortApiType.asc });
  }

  delete(item: AdminGroup) {
    return super.delete(item);
  }

  search(criteria: any) {
    return super.search({
      ...criteria,
      type: this.typeSizeGuide,
    });
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
