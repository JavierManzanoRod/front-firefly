/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { SizeGuideService } from '../services/size-guide.service';
import { SizeGuideListContainerComponent } from './size-guide-list-container.component';

// starts global mocks

class TranslateServiceStub {
  public setDefaultLang() {}
  public get(key: any): any {
    of(key);
  }
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string): any {
    return query;
  }
}

@Component({
  selector: 'ff-spenter-list-container',
  template: '<p>Mock Product Editor Component</p>',
})
class MockListComponent {
  @Input() loading!: boolean;
  @Input() list: any;
  @Input() page: any;
  @Input() currentData: any;
}

@Component({
  selector: 'ff-page-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockHeaderComponent {
  @Input() pageTitle: any;
  @Input() breadcrumbTitle: any;
  @Input() breadcrumbLink: any;
}

class ToastrServiceStub {
  public success() {}
}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };
  public get errorMessage() {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return this.content?.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }
  public hide() {
    return true;
  }
}

// eslint-disable-next-line prefer-const
let apiSpy: { list: jasmine.Spy; detail: jasmine.Spy; post: jasmine.Spy; update: jasmine.Spy; delete: jasmine.Spy };
let apiToastUtils: { warning: jasmine.Spy; error: jasmine.Spy };
// end global mocks

describe('SizeGuideListContainerComponent', () => {
  beforeEach(
    waitForAsync(() => {
      apiSpy = jasmine.createSpyObj('SizeGuideService', ['list', 'detail', 'post', 'update', 'delete']);
      apiToastUtils = jasmine.createSpyObj('ToastService', ['warning', 'error']);

      TestBed.configureTestingModule({
        declarations: [
          SizeGuideListContainerComponent,
          TranslatePipeMock,
          MockListComponent,
          MockHeaderComponent,
          SizeGuideListContainerComponent,
        ],
        imports: [RouterTestingModule],
        providers: [
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: ToastrService, useClass: ToastrServiceStub },
          { provide: BsModalService, useClass: ModalServiceMock },
          { provide: SizeGuideService, useValue: apiSpy },
          {
            provide: CrudOperationsService,
            useFactory: () => ({
              updateOrSaveModal: () => of({}),
              toast: apiToastUtils,
            }),
          },
        ],
      }).compileComponents();
    })
  );

  it('should create the component', () => {
    apiSpy.list.and.returnValue(of([{ a: 1 }]));
    const fixture = TestBed.createComponent(SizeGuideListContainerComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should call the proper api.list once after ngOnInit', () => {
    apiSpy.list.and.returnValue(of([{ a: 1 }]));
    const fixture = TestBed.createComponent(SizeGuideListContainerComponent);
    const component = fixture.debugElement.componentInstance;
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    component.ngOnInit();
    expect(apiSpy.list.calls.count()).toBe(1, 'one call');
  });

  it('should call the proper api.list once after search method is use', () => {
    apiSpy.list.and.returnValue(of([{ a: 1 }]));
    const fixture = TestBed.createComponent(SizeGuideListContainerComponent);
    const component = fixture.debugElement.componentInstance;
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    component.search('name');
    expect(apiSpy.list.calls.count()).toBe(1, 'one call');
  });
});
