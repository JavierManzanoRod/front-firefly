import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FF_SIZE_GUIDE } from 'src/app/configuration/tokens/admin-group-type.token';
import { FF_API_PATH_VERSION_ADMIN_GROUPS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroupTypeService } from '../../../shared/services/apis/admin-group-type.service';
import { SizeGuide } from '../models/size-guide.model';

@Injectable({
  providedIn: 'root',
})
export class SizeGuideService extends AdminGroupTypeService {
  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS) protected version: string,
    @Inject(FF_SIZE_GUIDE) protected sizeType: string
  ) {
    super(http, version, sizeType);
  }

  detail(id: string): Observable<SizeGuide> {
    return super.detail(id);
  }

  protected parseResult(data: SizeGuide) {
    const bodyResult = JSON.stringify({
      url: data.url,
    });

    return JSON.stringify({
      url: bodyResult,
    });
  }
}
