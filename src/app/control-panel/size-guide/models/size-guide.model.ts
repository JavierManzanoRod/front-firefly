import { GenericApiResponse } from '@model/base-api.model';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';

export interface SizeGuide extends AdminGroup {
  url?: string;
}

export type SizeGuideResponse = GenericApiResponse<SizeGuide>;
