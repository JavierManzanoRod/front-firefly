import { GenericApiResponse } from '@model/base-api.model';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
export interface SpecialProduct extends AdminGroup {
  special_name?: string;
  special_type?: string;
  special_url?: string;
}

export type SpecialProductResponse = GenericApiResponse<SpecialProduct>;
