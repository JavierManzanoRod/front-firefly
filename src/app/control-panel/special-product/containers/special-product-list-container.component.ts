import { Component, Inject, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { SortApiType } from '@core/models/sort.model';
import { FF_SPECIAL_PRODUCTS } from 'src/app/configuration/tokens/admin-group-type.token';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from '../../../modules/generic-list/models/generic-list.model';
import { SpecialProductService } from '../services/special-product.service';
import { AdminGroup } from './../../../rule-engine/admin-group/models/admin-group.model';

@Component({
  selector: 'ff-spenter-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header pageTitle="APP_MENU.SPECIAL_PRODUCT">
        <a [routerLink]="['/control-panel/special-product/new-rule']" class="btn btn-primary">
          {{ 'SPENTER.CREATE_RULE_BUTTON' | translate }}
        </a>
      </ff-page-header>

      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [route]="'/control-panel/special-product/view/'"
                [showPagination]="true"
                [arrayKeys]="listConfigs"
                [checkDateActive]="checkDateActive"
                [loading]="loading"
                [page]="page"
                [valueSearch]="searchName"
                [currentData]="currentData"
                (delete)="delete($event)"
                (pagination)="handlePagination($event)"
                [list]="list$ | async"
                [type]="type"
                [title]="''"
                [placeHolder]="'SPECIAL_PRODUCT.SEARCH_PLACEHOLDER' | translate"
                (search)="search($event)"
                (sortEvent)="sort($event)"
              ></ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class SpecialProductListContainerComponent extends BaseListContainerComponent<AdminGroup> implements OnInit {
  hide = false;
  listConfigs: GenericListConfig[] = [
    { key: 'name', headerName: 'SPECIAL_PRODUCT.TABLE_NAME', showOrder: true },
    { key: 'start_date', headerName: 'SPECIAL_PRODUCT.TABLE_INIT_DATE', canFormatDate: true, formatDate: 'dd-MM-yyyy H:mm' },
    { key: 'end_date', headerName: 'SPECIAL_PRODUCT.TABLE_END_DATE', canFormatDate: true, formatDate: 'dd-MM-yyyy H:mm' },
    { key: 'active', headerName: 'SPECIAL_PRODUCT.TABLE_ACTIVE', canActiveClass: true },
  ];
  type = TypeSearch.simple;

  constructor(
    public apiService: SpecialProductService,
    public crudOperationsSrv: CrudOperationsService,
    @Inject(FF_SPECIAL_PRODUCTS) public typeSpecialProducts: string
  ) {
    super(crudOperationsSrv, apiService);
  }

  ngOnInit() {
    this.sort({ name: 'name', type: SortApiType.asc });
  }

  search(criteria: any) {
    return super.search({
      ...criteria,
      type: this.typeSpecialProducts,
    });
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
