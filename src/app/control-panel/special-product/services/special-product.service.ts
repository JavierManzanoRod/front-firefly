import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_SPECIAL_PRODUCTS } from 'src/app/configuration/tokens/admin-group-type.token';
import { FF_API_PATH_VERSION_ADMIN_GROUPS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroupTypeService } from 'src/app/shared/services/apis/admin-group-type.service';
import { SpecialProduct } from '../models/special-product.model';
@Injectable({
  providedIn: 'root',
})
export class SpecialProductService extends AdminGroupTypeService {
  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS) protected version: string,
    @Inject(FF_SPECIAL_PRODUCTS) public type: string
  ) {
    super(http, version, type);
  }

  detail(id: string): Observable<SpecialProduct> {
    return super.detail(id).pipe(
      map((detail) => {
        const specialDetail: SpecialProduct = {
          ...detail,
        };
        let tempResult = null;
        if (detail?.result) {
          const parse = JSON.parse(detail.result);
          if (parse && parse[this.type]) {
            tempResult = JSON.parse(parse[this.type]);
          }
        }
        if (tempResult && detail) {
          specialDetail.special_name = tempResult.name;
          specialDetail.special_type = tempResult.type;
          specialDetail.special_url = tempResult.url;
        }
        return specialDetail;
      })
    );
  }
  protected parseResult(data: SpecialProduct) {
    const bodyResult = JSON.stringify({
      name: data.special_name,
      type: data.special_type,
      url: data.special_url,
    });

    return JSON.stringify({
      related_services: bodyResult,
    });
  }
}
