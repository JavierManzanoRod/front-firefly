import { GenericApiRequest, GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { SpecialProduct } from '../models/special-product.model';
import { SpecialProductService } from '../services/special-product.service';

let httpClientSpy: { get: jasmine.Spy; delete: jasmine.Spy; request: jasmine.Spy };

let myService: SpecialProductService;

const version = 'v1/';

const expectedData: SpecialProduct = {
  id: 'id',
  name: 'name',
  start_date: '2020-01-01',
  start_time: '00:00:00',
  is_basic: false,
  end_date: '2020-01-01',
  end_time: '00:00:00',
  admin_group_type_id: '1',
  admin_group_type_name: 'admin_group_type_name',
  result: 'result',
} as SpecialProduct;
const expectedList: GenericApiResponse<SpecialProduct> = {
  content: [expectedData],
  page: null as unknown as Page,
};

describe('SpecialProductService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'delete', 'request']);
  });

  it('SpecialProductService.list calls to get api method', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new SpecialProductService(httpClientSpy as any, version, 'id');
    myService.list(null as unknown as GenericApiRequest).subscribe(() => {});
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('SpecialProductService.post handle detail and return one element', () => {
    const rawHttpResponse = { status: 200, body: { name: 'api-response-name' } };
    httpClientSpy.request.and.returnValue(of(rawHttpResponse));
    myService = new SpecialProductService(httpClientSpy as any, version, 'id');
    myService.post(expectedData).subscribe((response) => {
      expect(response.status).toEqual(rawHttpResponse.status, 'expected status');
      expect(response.data.name).toEqual(rawHttpResponse.body.name, 'expected status');
    });
    expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
  });

  it('SpecialProductService.update handle detail and return one element', () => {
    const rawHttpResponse = { status: 200, body: { name: 'api-response-name' } };
    httpClientSpy.request.and.returnValue(of(rawHttpResponse));
    myService = new SpecialProductService(httpClientSpy as any, version, 'id');
    myService.update(expectedData).subscribe((response) => {
      expect(response.status).toEqual(rawHttpResponse.status, 'expected status');
      expect(response.data.name).toEqual(rawHttpResponse.body.name, 'expected status');
    });
    expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
  });

  it('SpecialProductService.delete handle detail and return one element', () => {
    httpClientSpy.delete.and.returnValue(of(expectedData));
    myService = new SpecialProductService(httpClientSpy as any, version, 'id');
    myService.delete(expectedData).subscribe((response) => {
      expect(response).toEqual(expectedData, 'expected data');
    });
    expect(httpClientSpy.delete.calls.count()).toBe(1, 'one call');
  });
});
