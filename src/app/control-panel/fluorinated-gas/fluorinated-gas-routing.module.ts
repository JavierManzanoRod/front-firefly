import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { FluorinatedGasDetailContainerComponent } from './containers/fluorinated-gas-detail-containter.component';
import { FluorinatedGasListContainerComponent } from './containers/fluorinated-gas-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: FluorinatedGasListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
      breadcrumb: [
        {
          label: 'SCOPE.SCOPE_LIST',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'new-rule',
        component: FluorinatedGasDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'new',
          breadcrumb: [
            {
              label: 'SPENTER.BREAD_CRUMB_TITLE',
              url: '/control-panel/spenter',
            },
            {
              label: 'DESTINATION.DESTINATION_ADD_NEW',
              url: '',
            },
          ],
        },
      },
      {
        path: 'view/:id',
        component: FluorinatedGasDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'view',
          breadcrumb: [
            {
              label: 'SPENTER.BREAD_CRUMB_TITLE',
              url: '/control-panel/spenter',
            },
            {
              label: 'DESTINATION.DESTINATION_ADD_NEW',
              url: '',
            },
          ],
        },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FluorinatedGasRoutingModule {}
