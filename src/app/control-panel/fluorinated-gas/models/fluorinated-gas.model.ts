import { GenericApiResponse } from '@model/base-api.model';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';

export interface FluorinatedGas extends AdminGroup {
  fluor_name?: string;
  fluor_type?: string;
  fluor_url?: string;
}

export type FluorinatedGasResponse = GenericApiResponse<FluorinatedGas>;
