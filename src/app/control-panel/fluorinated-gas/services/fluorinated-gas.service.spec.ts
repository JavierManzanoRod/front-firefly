import { GenericApiRequest, GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { FluorinatedGas } from '../models/fluorinated-gas.model';
import { FluorinatedGasService } from './fluorinated-gas.service';

let httpClientSpy: { get: jasmine.Spy; delete: jasmine.Spy; request: jasmine.Spy };

let myService: FluorinatedGasService;

const version = 'v1/';

const expectedData: FluorinatedGas = {
  id: '1',
  name: 'name',
  start_date: '2020-01-01',
  start_time: '00:00:00',
  is_basic: false,
  end_date: '2020-01-01',
  end_time: '00:00:00',
  admin_group_type_id: '1',
  admin_group_type_name: 'admin_group_type_name',
  result: 'result',
} as FluorinatedGas;
const expectedList: GenericApiResponse<FluorinatedGas> = {
  content: [expectedData],
  page: null as unknown as Page,
};

describe('FluorinatedGasService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'delete', 'request']);
  });

  it('FluorinatedGasService.list calls to get api method', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new FluorinatedGasService(httpClientSpy as any, version, 'id');
    myService.list(null as unknown as GenericApiRequest).subscribe(() => {});
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('FluorinatedGasService.post handle detail and return one element', () => {
    const rawHttpResponse = { status: 200, body: { name: 'api-response-name' } };
    httpClientSpy.request.and.returnValue(of(rawHttpResponse));
    myService = new FluorinatedGasService(httpClientSpy as any, version, 'id');
    myService.post(expectedData).subscribe((response) => {
      expect(response.status).toEqual(rawHttpResponse.status, 'expected status');
      expect(response.data.name).toEqual(rawHttpResponse.body.name, 'expected status');
    });
    expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
  });

  it('FluorinatedGasService.update handle detail and return one element', () => {
    const rawHttpResponse = { status: 200, body: { name: 'api-response-name' } };
    httpClientSpy.request.and.returnValue(of(rawHttpResponse));
    myService = new FluorinatedGasService(httpClientSpy as any, version, 'id');
    myService.update(expectedData).subscribe((response) => {
      expect(response.status).toEqual(rawHttpResponse.status, 'expected status');
      expect(response.data.name).toEqual(rawHttpResponse.body.name, 'expected status');
    });
    expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
  });

  it('FluorinatedGasService.delete handle detail and return one element', () => {
    httpClientSpy.delete.and.returnValue(of(expectedData));
    myService = new FluorinatedGasService(httpClientSpy as any, version, 'id');
    myService.delete(expectedData).subscribe((response) => {
      expect(response).toEqual(expectedData, 'expected data');
    });
    expect(httpClientSpy.delete.calls.count()).toBe(1, 'one call');
  });
});
