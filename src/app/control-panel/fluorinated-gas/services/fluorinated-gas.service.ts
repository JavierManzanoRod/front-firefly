import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_FLUOR_GASES } from 'src/app/configuration/tokens/admin-group-type.token';
import { FF_API_PATH_VERSION_ADMIN_GROUPS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroupTypeService } from 'src/app/shared/services/apis/admin-group-type.service';
import { FluorinatedGas } from '../models/fluorinated-gas.model';

@Injectable({
  providedIn: 'root',
})
export class FluorinatedGasService extends AdminGroupTypeService {
  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS) protected version: string,
    @Inject(FF_FLUOR_GASES) protected typeFluorGases: string
  ) {
    super(http, version, typeFluorGases);
  }

  detail(id: string): Observable<FluorinatedGas> {
    return super.detail(id).pipe(
      map((detail) => {
        const fluoratedDetail: FluorinatedGas = {
          ...detail,
        };
        let tempResult = null;
        if (detail?.result) {
          const parse = JSON.parse(detail.result);
          if (parse && parse[this.typeFluorGases]) {
            tempResult = JSON.parse(parse[this.typeFluorGases]);
          }
        }
        if (tempResult && detail) {
          fluoratedDetail.fluor_name = tempResult.name;
          fluoratedDetail.fluor_type = tempResult.type;
          fluoratedDetail.fluor_url = tempResult.url;
        }
        return fluoratedDetail;
      })
    );
  }

  protected parseResult(x: FluorinatedGas) {
    const bodyResult = JSON.stringify({
      name: x.fluor_name,
      type: x.fluor_type,
      url: x.fluor_url,
    });

    return JSON.stringify({
      eci_fluor_gases: bodyResult,
    });
  }
}
