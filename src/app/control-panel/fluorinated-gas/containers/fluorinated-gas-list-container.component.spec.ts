import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { SpenterResponse } from '../../spenter/models/spenter.model';
import { FluorinatedGasService } from '../services/fluorinated-gas.service';
import { FluorinatedGasListContainerComponent } from './fluorinated-gas-list-container.component';

describe('AuditFiltersComponent', () => {
  let componentToTest: FluorinatedGasListContainerComponent;
  let _apiSpy: { list: jasmine.Spy; detail: jasmine.Spy; post: jasmine.Spy; update: jasmine.Spy; delete: jasmine.Spy };

  const apiResponse: SpenterResponse = {
    content: [
      {
        id: 'a1',
        name: 'Special product mock 1',
        initDate: new Date('02/26/2021'),
        endDate: new Date('03/30/2021'),
        active: true,
      },
      {
        id: 'a2',
        name: 'Special product mock 2',
        initDate: new Date('02/26/2021'),
        endDate: new Date('03/30/2021'),
        active: false,
      },
    ],
    page: {
      page_number: 0,
      page_size: 20,
      total_elements: 20,
      total_pages: 20,
    },
  };

  beforeEach(() => {
    _apiSpy = jasmine.createSpyObj('SpecialProductService', ['list']);
    _apiSpy.list.and.returnValue(of(apiResponse));

    const _crudService = jasmine.createSpyObj('CrudOperationsService', ['deleteActionModal']);
    componentToTest = new FluorinatedGasListContainerComponent(
      _apiSpy as unknown as FluorinatedGasService,
      _crudService as unknown as CrudOperationsService
    );
  });

  it('when accesing the page we call to the api and retrieve some data', (done) => {
    componentToTest.ngOnInit();
    const list$ = componentToTest.list$;

    list$.subscribe((data: any) => {
      expect(data.length).toBe(apiResponse.content.length);
      done();
    });
  });
});
