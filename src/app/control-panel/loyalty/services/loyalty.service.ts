import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { FF_API_PATH_VERSION_ADMIN_GROUPS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroupTypeService } from 'src/app/shared/services/apis/admin-group-type.service';
import { FF_LOYALTY } from '../../../configuration/tokens/admin-group-type.token';

@Injectable({
  providedIn: 'root',
})
export class LoyaltyService extends AdminGroupTypeService {
  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS) protected version: string,
    @Inject(FF_LOYALTY) protected typeLoyalty: string
  ) {
    super(http, version, typeLoyalty);
  }
}
