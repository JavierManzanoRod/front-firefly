import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { LoyaltyDetailContainerComponent } from './containers/loyalty-detail-container.component';
import { LoyaltyListContainerComponent } from './containers/loyalty-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: LoyaltyListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
    },
    children: [
      {
        path: 'new-rule',
        component: LoyaltyDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'new',
        },
      },
      {
        path: 'view/:id',
        component: LoyaltyDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'view',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoyaltyRoutingModule {}
