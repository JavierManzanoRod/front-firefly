import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { ToastrModule } from 'ngx-toastr';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { ConditionsBasicAndAdvanceModule } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.module';
import { Conditions2Module } from 'src/app/modules/conditions2/conditions2.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { UiModalSelectModule } from 'src/app/modules/ui-modal-select/ui-modal-select.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { GenericListComponentModule } from '../../modules/generic-list/generic-list.module';
import { ControlPanelSharedModule } from '../shared/control-panel-shared.module';
import { BadgeRoutingModule } from './badge-routing.module';
import { BadgeDetailContainerComponent } from './containers/badge-detail-container.component';
import { BadgeListContainerComponent } from './containers/badge-list-container.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    TypeaheadModule.forRoot(),
    CommonModalModule,
    CoreModule,
    BadgeRoutingModule,
    Conditions2Module,
    ToastrModule.forRoot({
      autoDismiss: false,
      timeOut: 2000,
      closeButton: true,
      positionClass: 'toast-top-full-width',
      enableHtml: true,
    }),
    FormErrorModule,
    ChipsControlModule,
    SimplebarAngularModule,
    CoreModule,
    DatetimepickerModule,
    UiModalSelectModule,
    ControlPanelSharedModule,
    GenericListComponentModule,
    ConditionsBasicAndAdvanceModule,
  ],
  declarations: [BadgeListContainerComponent, BadgeDetailContainerComponent],
  providers: [],
  exports: [],
})
export class BadgeModule {}
