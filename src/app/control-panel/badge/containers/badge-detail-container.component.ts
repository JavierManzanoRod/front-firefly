import { Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { EntityType } from '@core/models/entity-type.model';
import { catchError, delay, tap } from 'rxjs/operators';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { CrudOperationsService, groupType } from 'src/app/core/services/crud-operations.service';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { ConditionsBasicAndAdvanceComponent } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.component';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { RulesType } from '../../../modules/conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { forkJoin, Observable, of, throwError } from 'rxjs';
import { FF_BADGE } from '../../../configuration/tokens/admin-group-type.token';
import { BadgeService } from '../services/badge.service';

import { HttpErrorResponse } from '@angular/common/http';
import { AdminGroupTypeService } from 'src/app/rule-engine/admin-group-type/services/admin-group-type.service';
import { Badge } from '../models/badge.modal';

@Component({
  selector: 'ff-spenter-detail-container',
  template: `<ff-page-header
      [pageTitle]="!id ? ('SPENTER.NEW_RULE_TITLE' | translate) : ('SPENTER.EDIT_RULE_TITLE' | translate)"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'SPENTER.BREAD_CRUMB_TITLE' | translate"
    >
      <button class="btn btn-dark mr-2" (click)="clone(detail)" data-cy="clone-rule" *ngIf="id">
        {{ 'COMMON.CLONE' | translate }}
      </button>
      <button class="btn btn-dark mr-2" (click)="delete(detail)" data-cy="remove-rule" *ngIf="id">
        {{ 'COMMON.DELETE' | translate }}
      </button>
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-conditions-basic-and-advance
        #detailElement
        (formSubmit)="handleSubmitForm($event)"
        (cancel)="cancel()"
        [loading]="loading"
        [item]="detail"
        [idAdminGroup]="badge"
        [entityTypes]="entityTypes"
        [type]="rulesType"
      ></ff-conditions-basic-and-advance>
    </ngx-simplebar> `,
})
export class BadgeDetailContainerComponent extends BaseDetailContainerComponent<AdminGroup> implements OnInit {
  @ViewChild('detailElement') detailElement!: ConditionsBasicAndAdvanceComponent;
  @Output() changed = new EventEmitter();

  urlListado = 'control-panel/badge';
  rulesType = RulesType.badge;
  entityTypes!: EntityType[];

  constructor(
    public crudOperationsSrv: CrudOperationsService,
    public menuLeftSrv: MenuLeftService,
    public activeRoute: ActivatedRoute,
    public router: Router,
    public adminGroupTypeSrv: AdminGroupTypeService,
    public apiService: BadgeService,
    public toastService: ToastService,
    @Inject(FF_BADGE) public badge: string
  ) {
    super(crudOperationsSrv, menuLeftSrv, activeRoute, router, apiService, toastService);
    this.id = this.activeRoute.snapshot.paramMap.get('id') || '';
  }

  ngOnInit() {
    let detail$: Observable<AdminGroup>;
    if (this.id) {
      detail$ = this.apiService.detail(this.id);
    } else {
      detail$ = of({} as AdminGroup).pipe(delay(250));
    }

    forkJoin({
      detail: detail$,
      entityTypes: this.adminGroupTypeSrv.entityTypes(this.badge),
    }).subscribe(
      ({ detail, entityTypes }) => {
        this.detail = detail;
        this.entityTypes = entityTypes;
        this.loading = false;
      },
      () => {
        this.router.navigateByUrl(this.urlListado).then(() => {
          this.toastService.error('COMMON_ERRORS.NOT_FOUND_DESCRIPTION', 'COMMON_ERRORS.NOT_FOUND_TITLE');
        });
      }
    );
  }

  canDeactivate(): UICommonModalConfig | false {
    if (this.detailElement.isDirty) {
      if (!this.detail || JSON.stringify(this.detail) === '{}') {
        return {};
      }
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.detailElement?.item.name,
      };
    }
    return false;
  }

  handleSubmitForm(itemToSave: Badge) {
    itemToSave = { ...itemToSave, Categories: this.detailElement.categoriesList };
    this.crudSrv
      .updateOrSaveModal(
        {
          ...this,
          methodToApply: itemToSave.id ? 'PUT' : 'POST',
        },
        itemToSave,
        groupType.RULE
      )
      .pipe(
        tap((response) => {
          this.detailElement.haveChanges = false;
          this.changed.emit(response.data);
          if (!itemToSave.id) {
            this.router.navigate([`${this.urlListado}/view/${response.data?.id}`]);
          }
        }),
        catchError((error: HttpErrorResponse) => {
          if (error.status === 409) {
            this.detailElement.form.controls.name.setErrors({ 409: true });
          }
          return throwError(error);
        })
      )
      .subscribe();
  }

  clone(detail: Badge | undefined) {
    if (detail) {
      detail = this.detailElement.getDataToSend() as AdminGroup;
      detail = { ...detail, Categories: this.detailElement.categoriesList };
    }
    if (this.detailElement.isDirty) {
      this.toastService.error('COMMON_ERRORS.CANNOT_CLONE');
      return;
    }
    if (this.detailElement?.form?.valid) {
      super.clone(detail, this.urlListado + '/view');
    } else {
      this.crudSrv.toast.warning('COMMON_ERRORS.CLONE_FORM_ERROR');
    }
  }

  cancel() {
    this.router.navigate(['.'], { relativeTo: this.activeRoute.parent });
  }
}
