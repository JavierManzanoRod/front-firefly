import { GenericApiResponse } from '@model/base-api.model';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';

export interface Badge extends AdminGroup {
  PLP?: string;
  PDP?: string;
  Categories?: string[];
}

export type BadgeResponse = GenericApiResponse<Badge>;
