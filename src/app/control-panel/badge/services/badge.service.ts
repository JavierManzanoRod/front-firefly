import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_ADMIN_GROUPS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroupTypeService } from 'src/app/shared/services/apis/admin-group-type.service';
import { FF_BADGE } from '../../../configuration/tokens/admin-group-type.token';
import { Badge } from '../models/badge.modal';

@Injectable({
  providedIn: 'root',
})
export class BadgeService extends AdminGroupTypeService {
  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS) protected version: string,
    @Inject(FF_BADGE) protected typeBadge: string
  ) {
    super(http, version, typeBadge);
  }

  detail(id: string): Observable<Badge> {
    return super.detail(id).pipe(
      map((detail) => {
        const badgeDetail: Badge = {
          ...detail,
        };

        if (badgeDetail?.result) {
          const parse = JSON.parse(badgeDetail.result);
          badgeDetail.PLP = parse.PLP;
          badgeDetail.PDP = parse.PDP;
          badgeDetail.Categories = parse.Categories;
        }
        return badgeDetail;
      })
    );
  }

  protected parseResult(data: any) {
    const body: any = {
      PDP: Math.trunc(data.pdp),
      PLP: Math.trunc(data.plp),
      Categories: [...new Set(data.Categories)].join(','),
    };
    if (!data.Categories.length) {
      delete body.Categories;
    }
    const bodyResult = JSON.stringify(body);
    return bodyResult;
  }
}
