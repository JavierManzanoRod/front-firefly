import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { BadgeDetailContainerComponent } from './containers/badge-detail-container.component';
import { BadgeListContainerComponent } from './containers/badge-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: BadgeListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
    },
    children: [
      {
        path: 'new-rule',
        component: BadgeDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'new',
        },
      },
      {
        path: 'view/:id',
        component: BadgeDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'view',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BadgeRoutingModule {}
