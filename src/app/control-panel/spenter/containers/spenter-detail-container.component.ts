import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { Spenter } from '../models/spenter.model';
import { SpenterService } from '../services/spenter.service';

@Component({
  selector: 'ff-spenter-detail-container',
  template: `<ff-page-header
      [pageTitle]="!id ? ('SPENTER.NEW_RULE_TITLE' | translate) : ('SPENTER.EDIT_RULE_TITLE' | translate)"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'SPENTER.BREAD_CRUMB_TITLE' | translate"
    >
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-spenter-detail (formSubmit)="handleSubmitForm($event)" (delete)="delete($event)" [loading]="loading" [item]="detail$ | async">
      </ff-spenter-detail>
    </ngx-simplebar> `,
})
export class SpenterDetailContainerComponent extends BaseDetailContainerComponent<Spenter> implements OnInit {
  urlListado = 'control-panel/spenter';

  constructor(
    public crudOperationsSrv: CrudOperationsService,
    public menuLeftSrv: MenuLeftService,
    public activeRoute: ActivatedRoute,
    public router: Router,
    public apiService: SpenterService,
    public toastService: ToastService
  ) {
    super(crudOperationsSrv, menuLeftSrv, activeRoute, router, apiService, toastService);
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
