import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { SpenterService } from '../services/spenter.service';
import { SpenterListContainerComponent } from './spenter-list-container.component';

describe('SpenterDetailContainerComponent', () => {
  let fixture: ComponentFixture<SpenterListContainerComponent>;
  let component: SpenterListContainerComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SpenterListContainerComponent],
      imports: [RouterTestingModule.withRoutes([]), TranslateModule.forRoot()],
      providers: [
        {
          provide: SpenterService,
          useFactory: () => ({
            list: () => {
              return of({});
            },
          }),
        },
        { provide: CrudOperationsService, useFactory: () => ({}) },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(SpenterListContainerComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });
});
