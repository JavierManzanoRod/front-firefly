import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { GenericApiRequest } from '@model/base-api.model';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { Spenter } from '../models/spenter.model';
import { SpenterService } from '../services/spenter.service';

@Component({
  selector: 'ff-spenter-list-container',
  template: `<ff-page-header pageTitle="{{ 'SPENTER.PRODUCT_SPENTER' | translate }}">
      <a [routerLink]="['/control-panel/spenter/new-rule']" class="btn btn-primary">
        {{ 'SPENTER.CREATE_RULE_BUTTON' | translate }}
      </a>
    </ff-page-header>

    <ngx-simplebar class="page-container">
      <div class="page-container-padding">
        <ff-list-search label="{{ 'SPENTER.SEARCH_RULE' | translate }}" (search)="search($event)"></ff-list-search>
      </div>

      <div class="page-scroll-wrapper">
        <ngx-simplebar class="page-scroll">
          <div class="page-container-padding">
            <ff-spenter-list
              [list]="list$ | async"
              [loading]="loading"
              [page]="page"
              [currentData]="currentData"
              (delete)="delete($event)"
              (pagination)="handlePagination($event)"
            >
            </ff-spenter-list>
          </div>
        </ngx-simplebar>
      </div>
    </ngx-simplebar> `,
})
export class SpenterListContainerComponent extends BaseListContainerComponent<Spenter> implements OnInit {
  public filter: GenericApiRequest = { size: 10, page: 0 };

  constructor(public apiService: SpenterService, public crudOperationsSrv: CrudOperationsService) {
    super(crudOperationsSrv, apiService);
  }

  ngOnInit() {
    this.getDataList(this.filter);
  }
}
