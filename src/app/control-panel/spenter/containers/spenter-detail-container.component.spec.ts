import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { SpenterService } from '../services/spenter.service';
import { SpenterDetailContainerComponent } from './spenter-detail-container.component';

class ToastrServiceStub {
  public success() {}
}

describe('SpentderDetailContainerComponent', () => {
  let fixture: ComponentFixture<SpenterDetailContainerComponent>;
  let component: SpenterDetailContainerComponent;
  let routerService: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SpenterDetailContainerComponent],
      imports: [RouterTestingModule.withRoutes([]), TranslateModule.forRoot()],
      providers: [
        { provide: SpenterService, useFactory: () => ({}) },
        { provide: ToastService, useClass: ToastrServiceStub },
        {
          provide: CrudOperationsService,
          useFactory: () => ({
            updateOrSaveModal: () => of({}),
          }),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(SpenterDetailContainerComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });
});
