import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { Spenter } from '../models/spenter.model';
@Injectable({
  providedIn: 'root',
})
export class SpenterService extends ApiService<Spenter> {
  endPoint = 'spenter/v1/spenter';

  constructor(protected http: HttpClient) {
    super();
  }
}
