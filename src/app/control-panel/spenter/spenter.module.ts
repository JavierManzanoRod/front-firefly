import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { ToastrModule } from 'ngx-toastr';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { Conditions2Module } from 'src/app/modules/conditions2/conditions2.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { Pagination2Module } from 'src/app/modules/pagination-2/pagination-2.module';
import { UiModalSelectModule } from 'src/app/modules/ui-modal-select/ui-modal-select.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ControlPanelSharedModule } from '../shared/control-panel-shared.module';
import { SpenterDetailComponent } from './components/spenter-detail/spenter-detail.component';
import { SpenterListComponent } from './components/spenter-list/spenter-list.component';
import { SpenterDetailContainerComponent } from './containers/spenter-detail-container.component';
import { SpenterListContainerComponent } from './containers/spenter-list-container.component';
import { SpenterRoutingModule } from './spenter-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    SpenterRoutingModule,
    Pagination2Module,
    TypeaheadModule.forRoot(),
    CommonModalModule,
    CoreModule,
    Conditions2Module,
    ToastrModule.forRoot({
      autoDismiss: false,
      timeOut: 2000,
      closeButton: true,
      positionClass: 'toast-top-full-width',
      enableHtml: true,
    }),
    FormErrorModule,
    ChipsControlModule,
    SimplebarAngularModule,
    CoreModule,
    DatetimepickerModule,
    UiModalSelectModule,
    ControlPanelSharedModule,
    NgSelectModule,
  ],
  declarations: [SpenterListContainerComponent, SpenterListComponent, SpenterDetailContainerComponent, SpenterDetailComponent],
  providers: [],
  exports: [],
})
export class SpenterModule {}
