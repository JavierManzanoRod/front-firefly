import { TranslateService } from '@ngx-translate/core';
import { SpenterDetailComponent } from './spenter-detail.component';

describe('SpenterDetailComponent', () => {
  it('should create the component', () => {
    const instant = {
      instant: () => {},
    } as unknown as TranslateService;
    const comp = new SpenterDetailComponent(null as any, instant);
    expect(comp).toBeDefined();
  });
});
