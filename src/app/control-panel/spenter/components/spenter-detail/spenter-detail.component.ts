import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { IRules } from 'src/app/control-panel/shared/components/rules-form/rules-form.component';
import { ControlPanelConditions } from 'src/app/control-panel/shared/models/control-panel-conditions.model';
import { UIModalSelectSubsites } from 'src/app/modules/ui-modal-select/models/modal-select.model';
import { FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { ChipsUIModalSelectSubsites, ModalChipsTypes } from '../../../../modules/chips-control/components/chips.control.model';
import { headerSpenterForm } from '../../models/header-spenter-form';
import { Spenter } from '../../models/spenter.model';

@Component({
  selector: 'ff-spenter-detail',
  templateUrl: 'spenter-detail.component.html',
  styleUrls: ['./spenter-detail.component.scss'],
})
export class SpenterDetailComponent implements OnInit {
  @Input() loading!: boolean;
  @Input() item!: Spenter;

  headerForm!: FormGroup;
  headerFormErrors: any = {};
  rules!: IRules[];
  modalSubsitesConfig: UIModalSelectSubsites = {
    selectedSubsites: [],
    cancelBtnText: 'Cancelar',
    okBtnText: 'Agregar',
  };
  isCollapsedConfig: any = {};
  includedValue!: ControlPanelConditions;
  excludedValue!: ControlPanelConditions;

  chipsSubsitesConfig: ChipsUIModalSelectSubsites = {
    modalType: ModalChipsTypes.subsites,
    modalConfig: {},
    label: this.translateSrv.instant('SPENTER.SITE'),
  };

  constructor(private router: Router, private translateSrv: TranslateService) {}

  ngOnInit() {
    const headerConfigForm = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig(headerSpenterForm);
    this.headerForm = headerConfigForm.form;
    this.headerFormErrors = headerConfigForm.errors;
  }

  submit() {
    console.log('Submit form -> ', this.headerForm.value);
  }

  cancel() {
    this.router.navigate(['/control-panel/spenter']);
  }

  changeIncludedData(data: ControlPanelConditions) {
    this.includedValue = data;
    console.log('this.include --->', this.includedValue);
    // const newValue = this.utilsBasicsConditions.toConditions2(this.basicValue, this.entityTypes[0]);
    // this.form.controls.node.setValue(newValue);
  }

  changeExcludedData(data: ControlPanelConditions) {
    this.excludedValue = data;
    console.log('this.excluded --->', this.excludedValue);
    // const newValue = this.utilsBasicsConditions.toConditions2(this.basicValue, this.entityTypes[0]);
    // this.form.controls.node.setValue(newValue);
  }

  changeRules(event: IRules[]) {
    this.rules = event;
    console.log('this rules --->', this.rules);
  }
}
