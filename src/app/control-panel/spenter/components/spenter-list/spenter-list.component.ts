import { Component } from '@angular/core';
import { BaseListComponent } from 'src/app/shared/components/base-component-list.component';
import { Spenter } from '../../models/spenter.model';

@Component({
  selector: 'ff-spenter-list',
  templateUrl: 'spenter-list.component.html',
  styleUrls: ['./spenter-list.component.scss'],
})
export class SpenterListComponent extends BaseListComponent<Spenter> {
  constructor() {
    super();
  }
}
