import { GenericApiResponse } from '@model/base-api.model';

export interface Spenter {
  id?: string;
  name?: string;
  initDate?: string | Date;
  endDate?: string | Date;
  active?: boolean;
}

export type SpenterResponse = GenericApiResponse<Spenter>;
