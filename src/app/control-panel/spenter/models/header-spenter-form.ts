import { FormConfig } from 'src/app/shared/services/form-utils.service';

export const headerSpenterForm: FormConfig = {
  name: {
    value: null,
    validators: {
      required: true,
    },
  },
  active: {
    value: true,
  },
  unpublish: {
    value: null,
  },
  initDate: {
    value: null,
  },
  endDate: {
    value: null,
  },
  showAvailable: {
    value: true,
  },
  nightControl: {
    value: true,
  },
  minimumToShow: {
    value: null,
  },
  chipsSubsites: {
    value: [],
  },
  rules: {
    value: [],
  },
};

export const spenterRulesForm: FormConfig = {
  centers: {
    value: null,
    validators: {
      required: true,
    },
  },
  stock: {
    value: null,
    validators: {
      required: true,
    },
  },
  type: {
    value: null,
    validators: {
      required: true,
    },
  },
};
