import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SpenterDetailContainerComponent } from './containers/spenter-detail-container.component';
import { SpenterListContainerComponent } from './containers/spenter-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: SpenterListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
      breadcrumb: [
        {
          label: 'SCOPE.SCOPE_LIST',
          url: '',
        },
      ],
    },
  },
  {
    path: 'new-rule',
    component: SpenterDetailContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
      title: 'new',
      breadcrumb: [
        {
          label: 'SPENTER.BREAD_CRUMB_TITLE',
          url: '/control-panel/spenter',
        },
        {
          label: 'DESTINATION.DESTINATION_ADD_NEW',
          url: '',
        },
      ],
    },
  },
  {
    path: 'view-rule/:id',
    component: SpenterDetailContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
      title: 'new',
      breadcrumb: [
        {
          label: 'SPENTER.BREAD_CRUMB_TITLE',
          url: '/control-panel/spenter',
        },
        {
          label: 'DESTINATION.DESTINATION_ADD_NEW',
          url: '',
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SpenterRoutingModule {}
