import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
  },
  {
    path: 'spenter',
    loadChildren: () => import('./spenter/spenter.module').then((m) => m.SpenterModule),
  },
  {
    path: 'special-product',
    loadChildren: () => import('./special-product/special-product.module').then((m) => m.SpecialProductModule),
  },
  {
    path: 'size-guide',
    loadChildren: () => import('./size-guide/size-guide.module').then((m) => m.SizeGuideModule),
  },
  {
    path: 'fluorinated-gas',
    loadChildren: () => import('./fluorinated-gas/fluorinated-gas.module').then((m) => m.FluorinatedGasModule),
  },
  {
    path: 'expert-admin',
    loadChildren: () => import('./expert-admin/expert-admin.module').then((m) => m.ExpertAdminModule),
  },
  {
    path: 'expert',
    loadChildren: () => import('./expert/expert.module').then((m) => m.ExpertModule),
  },
  {
    path: 'loyalty',
    loadChildren: () => import('./loyalty/loyalty.module').then((m) => m.LoyaltyModule),
  },
  {
    path: 'badge',
    loadChildren: () => import('./badge/badge.module').then((m) => m.BadgeModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ControlPanelRoutingModule {}
