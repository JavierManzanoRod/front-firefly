import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

export interface IRules {
  centers: string;
  stock: string;
  type: string;
}

@Component({
  selector: 'ff-rules-form',
  templateUrl: './rules-form.component.html',
  styleUrls: ['./rules-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RulesFormComponent implements OnInit {
  @Output() rules = new EventEmitter<IRules[]>();

  public rulesForm!: FormGroup;

  public get addButtonDisabled(): boolean {
    const rules = this.getRules();
    const lastRule = rules[rules.length - 1];
    return lastRule?.invalid;
  }

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.rulesForm = this.formBuilder.group({
      rules: this.formBuilder.array([
        this.formBuilder.group({
          // eslint-disable-next-line @typescript-eslint/unbound-method
          centers: ['', Validators.required],
          // eslint-disable-next-line @typescript-eslint/unbound-method
          stock: ['', Validators.required],
          // eslint-disable-next-line @typescript-eslint/unbound-method
          type: ['', Validators.required],
        }),
      ]),
    });
  }

  addRule() {
    console.log('reglas ---->', this.rulesForm.value);
    this.rules.emit(this.rulesForm.get('rules')?.value);

    const rulesArray = this.rulesForm.controls.rules as FormArray;

    const newRule: FormGroup = this.formBuilder.group({
      // eslint-disable-next-line @typescript-eslint/unbound-method
      centers: new FormControl('', { validators: Validators.required }),
      // eslint-disable-next-line @typescript-eslint/unbound-method
      stock: new FormControl('', { validators: Validators.required }),
      // eslint-disable-next-line @typescript-eslint/unbound-method
      type: new FormControl('', { validators: Validators.required }),
    });

    rulesArray.insert(rulesArray.length, newRule);
  }

  removeRule(i: number) {
    console.log('index ', i);

    const rulesArray = this.rulesForm.controls.rules as FormArray;

    rulesArray.removeAt(i);
    console.log('reglas --->', this.rulesForm);
    this.rules.emit(this.rulesForm.get('rules')?.value);
  }

  getRules() {
    return (this.rulesForm.get('rules') as FormArray).controls;
  }
}
