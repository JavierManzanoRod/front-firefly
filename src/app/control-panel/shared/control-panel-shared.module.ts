import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { RulesFormComponent } from './components/rules-form/rules-form.component';

@NgModule({
  imports: [ChipsControlModule, FormsModule, ReactiveFormsModule, TranslateModule, CommonModule, NgSelectModule],
  declarations: [RulesFormComponent],
  providers: [],
  exports: [RulesFormComponent],
})
export class ControlPanelSharedModule {}
