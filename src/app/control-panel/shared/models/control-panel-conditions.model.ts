import { MayHaveIdName } from '@model/base-api.model';

export enum ControlPanelConditions {
  sites = 'sites',
  categoryRules = 'categoryRules',
  department = 'department', // Tienen variables con path
  family = 'family', // Tienen variables con path
  saleReference = 'saleReference', // Tienen variables con path
  categories = 'categories', // Tienen variables con path
  brand = 'brand', // Tienen variables con path
  classifications1 = 'classifications1', // Tienen variables con path
  classifications2 = 'classifications2', // Tienen variables con path
  classifications3 = 'classifications3', // Tienen variables con path
  classifications4 = 'classifications4', // Tienen variables con path
  classifications5 = 'classifications5', // Tienen variables con path
  classifications6 = 'classifications6', // Tienen variables con path
  managementType = 'managementType', // Tienen variables con path
  margin = 'margin',
  customisedRequest = 'customisedRequest', // Tienen variables con path
  provider = 'provider', // Tienen variables con path
  volume = 'volume',
  goodTypes = 'goodTypes', // Tienen variables con path
  atue = 'atue',
  maker = 'maker',
  serie = 'serie',
  luxuryReference = 'luxuryReference',
  internetSignal = 'internetSignal',
  referenceType = 'referenceType',
  infiniteStock = 'infiniteStock',
  barra = 'barra', // Tienen variables con path
  sizeCode = 'sizeCode', // Tienen variables con path
  product = 'product', // Tienen variables con path
  centers = 'centers',
  ranges = 'ranges',
  price = 'price',
  discount = 'discount',
  gtin = 'gtin',
}

export enum ControlPanelConditionsNonGroupped {
  sites = 'sites',
  categoryRules = 'categoryRules',
  categories = 'categories', // Tienen variables con path
  brand = 'brand', // Tienen variables con path
  classifications1 = 'classifications1', // Tienen variables con path
  classifications2 = 'classifications2', // Tienen variables con path
  classifications3 = 'classifications3', // Tienen variables con path
  classifications4 = 'classifications4', // Tienen variables con path
  classifications5 = 'classifications5', // Tienen variables con path
  classifications6 = 'classifications6', // Tienen variables con path
  managementType = 'managementType', // Tienen variables con path
  margin = 'margin',
  customisedRequest = 'customisedRequest', // Tienen variables con path
  provider = 'provider', // Tienen variables con path
  volume = 'volume',
  goodTypes = 'goodTypes', // Tienen variables con path
  atue = 'atue',
  maker = 'maker',
  serie = 'serie',
  luxuryReference = 'luxuryReference',
  internetSignal = 'internetSignal',
  referenceType = 'referenceType',
  infiniteStock = 'infiniteStock',
  centers = 'centers',
  ranges = 'ranges',
  price = 'price',
  discount = 'discount',
}

export enum EnvironmentAttToControlPanelCondition {
  ATTRIBUTE_ROUTE_BARRA = 'barra',
  ATTRIBUTE_ROUTE_BRAND_IDENTIFIER = 'brand',
  ATTRIBUTE_ROUTE_CLASSIFICATION1_6_IDENTIFIER = 'classifications6',
  ATTRIBUTE_ROUTE_DEPARTMENT = 'department',
  ATTRIBUTE_ROUTE_FAMILY = 'family',
  ATTRIBUTE_ROUTE_GOOD_TYPES = 'goodTypes',
  ATTRIBUTE_ROUTE_MANAGEMENT_TYPE = 'managementType',
  ATTRIBUTE_ROUTE_SALEREFERENCE = 'saleReference',
  ATTRIBUTE_ROUTE_SIZECODE = 'sizeCode',
  ATTRIBUTE_ROUTE_PRODUCT_ID = 'product',
  ATTRIBUTE_ROUTE_CATEGORIES = 'categories',
  ATTRIBUTE_ROUTE_PROVIDER_IDENTIFIER = 'provider',
  ATTRIBUTE_ROUTE_CUSTOMISED_REQUEST = 'customisedRequest',
}

export enum EnvironmentsKeys {
  ATTRIBUTE_ROUTE_BARRA = 'ATTRIBUTE_ROUTE_BARRA',
  ATTRIBUTE_ROUTE_BRAND_IDENTIFIER = 'ATTRIBUTE_ROUTE_BRAND_IDENTIFIER',
  ATTRIBUTE_ROUTE_CLASSIFICATION1_6 = 'ATTRIBUTE_ROUTE_CLASSIFICATION1_6',
  ATTRIBUTE_ROUTE_DEPARTMENT = 'ATTRIBUTE_ROUTE_DEPARTMENT',
  ATTRIBUTE_ROUTE_FAMILY = 'ATTRIBUTE_ROUTE_FAMILY',
  ATTRIBUTE_ROUTE_GOOD_TYPES = 'ATTRIBUTE_ROUTE_GOOD_TYPES',
  ATTRIBUTE_ROUTE_MANAGEMENT_TYPE = 'ATTRIBUTE_ROUTE_MANAGEMENT_TYPE',
  ATTRIBUTE_ROUTE_SALEREFERENCE = 'ATTRIBUTE_ROUTE_SALEREFERENCE',
  ATTRIBUTE_ROUTE_SIZECODE = 'ATTRIBUTE_ROUTE_SIZECODE',
  ATTRIBUTE_ROUTE_PRODUCT_ID = 'ATTRIBUTE_ROUTE_PRODUCT_ID',
  ATTRIBUTE_ROUTE_CATEGORIES = 'ATTRIBUTE_ROUTE_CATEGORIES',
  ATTRIBUTE_ROUTE_PROVIDER_IDENTIFIER = 'ATTRIBUTE_ROUTE_PROVIDER_IDENTIFIER',
  ATTRIBUTE_ROUTE_CUSTOMISED_REQUEST = 'ATTRIBUTE_ROUTE_CUSTOMISED_REQUEST',
}

export enum MultivaluedAttributes {
  classifications = 'classifications',
}

export type ConditionsBasic = Partial<Record<ControlPanelConditions, MayHaveIdName[] | string[] | IPricesAndDiscounts[]>>;

export interface ContentGroupCondition {
  id: string;
  name?: string;
  node?: {
    attribute_id?: string;
    node_type?: string;
    value?: string;
  };
}

export interface ConditionsToShow {
  department?: boolean;
  family?: boolean;
  saleReference?: boolean;
  categories?: boolean;
  brand?: boolean;
  classifications1?: boolean;
  classifications2?: boolean;
  classifications3?: boolean;
  classifications4?: boolean;
  classifications5?: boolean;
  classifications6?: boolean;
  managementType?: boolean;
  customisedRequest?: boolean;
  provider?: boolean;
  goodTypes?: boolean;
  barra?: boolean;
  sizeCode?: boolean;
  product?: boolean;
  centers?: boolean;
  ranges?: boolean;
  gtin?: boolean;
}

export interface IncludedExcludedConditionsBasics {
  included: ConditionsBasic;
  excluded: ConditionsBasic;
}

export interface IClassification {
  values: string[];
  key: string;
}

export enum Ranges {
  discount = 'discount',
  price = 'price',
}
export interface IPricesAndDiscounts {
  discount?: IRange;
  price?: IRange;
}

export interface IRange {
  from?: number;
  to?: number;
}
// export interface VisibleControls {
//   visible: boolean;
//   formControl: string;
//   configControl: string;
// }
