import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { SortApiType } from '@core/models/sort.model';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { GenericListConfig, TypeSearch } from '../../../modules/generic-list/models/generic-list.model';
import { ExpertAdminService } from '../services/expert-admin.service';

@Component({
  selector: 'ff-spenter-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header pageTitle="APP_MENU.EXPERT_ADMIN">
        <a [routerLink]="['/control-panel/expert-admin/new-rule']" class="btn btn-primary">
          {{ 'SPENTER.CREATE_RULE_BUTTON' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [route]="'/control-panel/expert-admin/view/'"
                [showPagination]="true"
                [checkDateActive]="checkDateActive"
                [arrayKeys]="listConfigs"
                [loading]="loading"
                [page]="page"
                [valueSearch]="searchName"
                [currentData]="currentData"
                (delete)="delete($event)"
                (pagination)="handlePagination($event)"
                [list]="list$ | async"
                [type]="type"
                [title]="''"
                [placeHolder]="'EXPERT_ADMIN.SEARCH_PLACEHOLDER' | translate"
                (search)="search($event)"
                (sortEvent)="sort($event)"
              ></ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class ExpertAdminListContainerComponent extends BaseListContainerComponent<AdminGroup> implements OnInit {
  hide = false;
  listConfigs: GenericListConfig[] = [
    { key: 'name', headerName: 'EXPERT_ADMIN.TABLE_NAME', showOrder: true },
    { key: 'start_date', headerName: 'EXPERT_ADMIN.TABLE_INIT_DATE', canFormatDate: true, formatDate: 'dd-MM-yyyy H:mm' },
    { key: 'end_date', headerName: 'EXPERT_ADMIN.TABLE_END_DATE', canFormatDate: true, formatDate: 'dd-MM-yyyy H:mm' },
    { key: 'active', headerName: 'EXPERT_ADMIN.TABLE_ACTIVE', canActiveClass: true },
  ];
  type = TypeSearch.simple;

  constructor(public apiService: ExpertAdminService, public crudOperationsSrv: CrudOperationsService) {
    super(crudOperationsSrv, apiService);
  }

  ngOnInit() {
    this.sort({ name: 'name', type: SortApiType.asc });
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
