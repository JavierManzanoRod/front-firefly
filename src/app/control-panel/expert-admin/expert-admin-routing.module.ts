import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { ExpertAdminDetailContainerComponent } from './containers/expert-admin-detail-container.component';
import { ExpertAdminListContainerComponent } from './containers/expert-admin-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: ExpertAdminListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
      breadcrumb: [
        {
          label: 'SCOPE.SCOPE_LIST',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'new-rule',
        component: ExpertAdminDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'new',
          breadcrumb: [
            {
              label: 'SPENTER.BREAD_CRUMB_TITLE',
              url: '/control-panel/spenter',
            },
            {
              label: 'DESTINATION.DESTINATION_ADD_NEW',
              url: '',
            },
          ],
        },
      },
      {
        path: 'view/:id',
        component: ExpertAdminDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'view',
          breadcrumb: [
            {
              label: 'SPENTER.BREAD_CRUMB_TITLE',
              url: '/control-panel/spenter',
            },
            {
              label: 'DESTINATION.DESTINATION_ADD_NEW',
              url: '',
            },
          ],
        },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpertAdminRoutingModule {}
