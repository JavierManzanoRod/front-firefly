import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PATH_ASSETS } from '@env/custom-variables';
import { map } from 'rxjs/operators';
import { Experts } from '../models/expert-admin.model';

@Injectable({
  providedIn: 'root',
})
export class ExpertAssetsService {
  endPoint = PATH_ASSETS + 'assets/experts/experts.json';

  constructor(protected http: HttpClient) {}

  list(criteria: Experts) {
    return this.http.get<Experts[]>(this.endPoint).pipe(
      map((list) => list.filter((expert) => expert.name?.toLowerCase()?.includes(criteria?.name?.toLowerCase() || ''))),
      map((list) => {
        return {
          content: list,
          page: {
            page_number: 1,
            page_size: list.length,
            total_elements: list.length,
            total_pages: 1,
          },
        };
      })
    );
  }
}
