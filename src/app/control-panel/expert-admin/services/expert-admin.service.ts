import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_EXPERT } from 'src/app/configuration/tokens/admin-group-type.token';
import { FF_API_PATH_VERSION_ADMIN_GROUPS } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroupTypeService } from 'src/app/shared/services/apis/admin-group-type.service';
import { ExpertAdmin } from '../models/expert-admin.model';
@Injectable({
  providedIn: 'root',
})
export class ExpertAdminService extends AdminGroupTypeService {
  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_ADMIN_GROUPS) protected version: string,
    @Inject(FF_EXPERT) protected typeExpert: string
  ) {
    super(http, version, typeExpert);
  }

  detail(id: string): Observable<ExpertAdmin> {
    return super.detail(id).pipe(
      map((detail) => {
        const expertDetail: ExpertAdmin = {
          ...detail,
        };
        let tempResult = null;
        if (detail?.result) {
          const parse = JSON.parse(detail.result);
          if (parse && parse[this.typeExpert]) {
            tempResult = JSON.parse(parse[this.typeExpert]);
          }
        }
        if (tempResult && detail && tempResult.name) {
          expertDetail.expert = tempResult;
        }
        return expertDetail;
      })
    );
  }

  protected parseResult(data: ExpertAdmin) {
    const bodyResult = JSON.stringify({
      identifier: data.expert?.identifier,
      widget_id: data.expert?.widget_id,
      is_enabled: true,
      experts_widgetid_json: data.expert?.experts_widgetid_json,
      name: data.expert?.name,
    });

    return JSON.stringify({
      experts: bodyResult,
    });
  }
}
