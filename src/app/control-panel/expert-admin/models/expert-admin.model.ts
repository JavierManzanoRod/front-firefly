import { GenericApiResponse } from '@model/base-api.model';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { Expert } from '../../expert/models/expert.model';

export interface ExpertAdmin extends AdminGroup {
  expert?: Expert;
}

export type ExpertAdminResponse = GenericApiResponse<ExpertAdmin>;
