import { GenericApiResponse } from '@model/base-api.model';

export interface Expert {
  identifier?: string;
  widget_id?: string;
  is_enabled?: boolean;
  experts_widgetid_json?: {
    ios?: string;
    android?: string;
    web?: string;
    uuid: number;
  };
  name?: string;
}
export type ExpertResponse = GenericApiResponse<Expert>;
