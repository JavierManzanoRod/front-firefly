import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { ExpertDetailContainerComponent } from './containers/expert-detail-container.component';
import { ExpertListContainerComponent } from './containers/expert-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: ExpertListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
      breadcrumb: [
        {
          label: 'SCOPE.SCOPE_LIST',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'new-rule',
        component: ExpertDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
          title: 'new',
          breadcrumb: [
            {
              label: 'SPENTER.BREAD_CRUMB_TITLE',
              url: '/control-panel/spenter',
            },
            {
              label: 'DESTINATION.DESTINATION_ADD_NEW',
              url: '',
            },
          ],
        },
      },
      {
        path: 'view/:id',
        component: ExpertDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.FIREFLY_ADMINISTRATION',
          title: 'view',
          breadcrumb: [
            {
              label: 'SPENTER.BREAD_CRUMB_TITLE',
              url: '/control-panel/spenter',
            },
            {
              label: 'DESTINATION.DESTINATION_ADD_NEW',
              url: '',
            },
          ],
        },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpertRoutingModule {}
