import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { ToastrModule } from 'ngx-toastr';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ChipsControlModule } from 'src/app/modules/chips-control/chips-control.module';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { FormErrorModule } from 'src/app/modules/form-error/form-error.module';
import { UiModalSelectModule } from 'src/app/modules/ui-modal-select/ui-modal-select.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { GenericListComponentModule } from '../../modules/generic-list/generic-list.module';
import { ControlPanelSharedModule } from '../shared/control-panel-shared.module';
import { ExpertDetailComponent } from './components/expert-detail/expert-detail.component';
import { ExpertDetailContainerComponent } from './containers/expert-detail-container.component';
import { ExpertListContainerComponent } from './containers/expert-list-container.component';
import { ExpertRoutingModule } from './expert-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ExpertRoutingModule,
    ReactiveFormsModule,
    TypeaheadModule.forRoot(),
    CommonModalModule,
    CoreModule,
    ToastrModule.forRoot({
      autoDismiss: false,
      timeOut: 2000,
      closeButton: true,
      positionClass: 'toast-top-full-width',
      enableHtml: true,
    }),
    FormErrorModule,
    ChipsControlModule,
    SimplebarAngularModule,
    CoreModule,
    DatetimepickerModule,
    UiModalSelectModule,
    ControlPanelSharedModule,
    GenericListComponentModule,
  ],
  declarations: [ExpertDetailComponent, ExpertListContainerComponent, ExpertDetailContainerComponent],
})
export class ExpertModule {}
