import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityType } from '@core/models/entity-type.model';
import { ChipsUIModalExperts, ModalChipsTypes } from 'src/app/modules/chips-control/components/chips.control.model';
import { FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { Expert } from '../../models/expert.model';

@Component({
  selector: 'ff-expert-detail',
  templateUrl: './expert-detail.component.html',
  styleUrls: ['./expert-detail.component.scss'],
})
export class ExpertDetailComponent implements OnInit, OnChanges {
  @Output() formSubmit: EventEmitter<Expert> = new EventEmitter<Expert>();
  @Input() entityType!: EntityType;
  @Input() loading!: boolean;
  @Input() item!: Expert;

  form!: FormGroup;
  formErrors: any = {};

  isCollapsedConfig: any = {};
  haveChanges = false;

  configExperts!: ChipsUIModalExperts;

  get isDirty(): boolean {
    return this.haveChanges;
  }

  constructor(private router: Router, private activatedRute: ActivatedRoute) {}

  ngOnInit() {
    const headerConfigForm = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      identifier: {
        value: null,
      },
      name: {
        value: null,
        validators: {
          required: true,
          error_409: true,
        },
      },
      widget_id: {
        value: null,
        validators: {
          required: true,
        },
      },
      experts_widgetid_json: {
        value: {
          android: '',
          ios: '',
        },
      },
    });

    this.form = headerConfigForm.form;
    this.formErrors = headerConfigForm.errors;
    this.form.valueChanges.subscribe(() => (this.haveChanges = true));
    this.configExperts = {
      modalType: ModalChipsTypes.experts,
      label: 'EXPERT.DETAIL.EXPERTS_WIDGETID_JSON',
      modalConfig: {
        title: 'EXPERT.PLACEHOLDER.EXPERTS_WIDGETID_JSON',
      },
    };
  }

  ngOnChanges() {
    this.item = this.item ? JSON.parse(JSON.stringify(this.item)) : this.item;
    this.form?.patchValue(
      {
        ...this.item,
        experts_widgetid_json: {
          ios: this.item.experts_widgetid_json?.ios || '',
          android: this.item.experts_widgetid_json?.android || '',
        },
      },
      { emitEvent: false }
    );
  }
  getDataForSave(): Expert {
    return {
      identifier: this.form.value.identifier,
      name: this.form.value.name,
      widget_id: this.form.value.widget_id,
      experts_widgetid_json: {
        ...this.form.value.experts_widgetid_json,
        uuid: this.item.experts_widgetid_json ? this.item.experts_widgetid_json?.uuid : null,
        web: this.form.value.widget_id,
      },
    };
  }

  submit() {
    FormUtilsService.markFormGroupTouched(this.form);
    if (this.form.valid) {
      this.formSubmit.emit(this.getDataForSave());
    }
  }

  cancel() {
    this.form.markAsPristine();
    this.router.navigate(['.'], { relativeTo: this.activatedRute.parent });
  }
}
