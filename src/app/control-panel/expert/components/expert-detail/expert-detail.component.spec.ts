import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { ExpertDetailComponent } from './expert-detail.component';

describe('ExpertAdminDetailComponent', () => {
  let component: ExpertDetailComponent;
  let fixture: ComponentFixture<ExpertDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExpertDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useFactory: () => ({}),
        },
        {
          provide: Router,
          useFactory: () => ({}),
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ExpertDetailComponent);
    component = fixture.componentInstance;

    const headerConfigForm = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      id: {
        value: null,
      },
      name: {
        value: null,
        validators: {
          required: true,
        },
      },
      widget_id: {
        value: null,
        validators: {
          required: true,
        },
      },
    });

    component.form = headerConfigForm.form;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('init form', () => {
    component.ngOnInit();
    expect(component.form).toBeTruthy();
  });

  it('init form values', () => {
    component.item = {
      identifier: '1234567890',
      name: 'Paco Merte',
      widget_id: '3123123123',
    };
    component.ngOnInit();
    expect(component.form).toBeTruthy();
  });
});
