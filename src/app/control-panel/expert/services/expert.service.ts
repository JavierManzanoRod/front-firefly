import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { GenericApiRequest } from '@model/base-api.model';
import { Observable } from 'rxjs';
import { FF_API_PATH_VERSION_EXPERT } from 'src/app/configuration/tokens/api-versions.token';
import { ApiService, IApiService2, UpdateResponse } from '../../../core/base/api.service';
import { Expert } from '../models/expert.model';

@Injectable({
  providedIn: 'root',
})
export class ExpertService extends ApiService<Expert> implements IApiService2<Expert> {
  endPoint = `products/backoffice-group-expert/${this.apiVersion}expert-groups`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_EXPERT) private apiVersion: string) {
    super();
  }

  list(filter: GenericApiRequest) {
    return super.list(filter);
  }

  detail(name: string) {
    return super.detail(name);
  }

  post(item: Expert) {
    return super.post(item);
  }

  delete(data: any): Observable<any> {
    const url = `${this.urlBase}/${data.identifier}`;
    return this.http.delete<any>(url);
  }

  update(item: Expert): Observable<UpdateResponse<Expert>> {
    return super.update(item);
  }
}
