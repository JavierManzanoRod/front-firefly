import { Component } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from '../../../modules/generic-list/models/generic-list.model';
import { Expert } from '../models/expert.model';
import { ExpertService } from '../services/expert.service';

@Component({
  selector: 'ff-spenter-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header pageTitle="APP_MENU.EXPERT">
        <a [routerLink]="['/control-panel/expert/new-rule']" class="btn btn-primary">
          {{ 'EXPERT.CREATE_EXPERT' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [route]="routePage"
                [showPagination]="true"
                [arrayKeys]="listConfigs"
                [loading]="loading"
                [page]="page"
                [valueSearch]="searchName"
                [routerData]="routerData"
                [currentData]="currentData"
                (delete)="delete($event)"
                (pagination)="handlePagination($event)"
                [list]="list$ | async"
                [type]="type"
                [title]="''"
                [placeHolder]="'EXPERT.SEARCH_PLACEHOLDER' | translate"
                (search)="search($event)"
              ></ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class ExpertListContainerComponent extends BaseListContainerComponent<Expert> {
  hide = false;
  listConfigs: GenericListConfig[] = [
    { key: 'name', headerName: 'EXPERT.TABLE_NAME' },
    { key: 'widget_id', headerName: 'EXPERT.TABLE_WIDGET_ID' },
  ];
  type = TypeSearch.simple;
  routePage = '/control-panel/expert/view/';
  constructor(public apiService: ExpertService, public crudOperationsSrv: CrudOperationsService) {
    super(crudOperationsSrv, apiService);
  }

  routerData = (item: Expert): string => {
    return `${this.routePage}${item.identifier}`;
  };

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
