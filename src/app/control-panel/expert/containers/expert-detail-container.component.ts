import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { EntityType } from '@core/models/entity-type.model';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { CrudOperationsService, groupType } from 'src/app/core/services/crud-operations.service';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { ExpertDetailComponent } from '../components/expert-detail/expert-detail.component';
import { Expert } from '../models/expert.model';
import { ExpertService } from '../services/expert.service';
import { catchError, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ff-spenter-detail-container',
  template: `<ff-page-header
      [pageTitle]="!id ? ('EXPERT.NEW_EXPERT' | translate) : ('EXPERT.EDIT_EXPERT' | translate)"
      [breadcrumbLink]="urlListado"
      [breadcrumbTitle]="'SPENTER.BREAD_CRUMB_TITLE' | translate"
    >
      <button class="btn btn-dark mr-2" (click)="clone(detail)" data-cy="expert-clone-btn" *ngIf="id">
        {{ 'COMMON.CLONE' | translate }}
      </button>
      <button class="btn btn-dark mr-2" (click)="delete(detail)" data-cy="expert-delete-btn" *ngIf="id">
        {{ 'COMMON.DELETE' | translate }}
      </button>
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-expert-detail
        #detailElement
        (formSubmit)="handleSubmitForm($event)"
        [entityType]="entityType"
        [loading]="loading"
        [item]="detail$ | async"
      >
      </ff-expert-detail>
    </ngx-simplebar> `,
})
export class ExpertDetailContainerComponent extends BaseDetailContainerComponent<Expert> implements OnInit {
  @ViewChild('detailElement') detailElement!: ExpertDetailComponent;
  @Output() changed = new EventEmitter();

  urlListado = 'control-panel/expert';
  entityType!: EntityType;

  constructor(
    public crudOperationsSrv: CrudOperationsService,
    public menuLeftSrv: MenuLeftService,
    public activeRoute: ActivatedRoute,
    public router: Router,
    public apiService: ExpertService,
    public toastService: ToastService
  ) {
    super(crudOperationsSrv, menuLeftSrv, activeRoute, router, apiService, toastService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  canDeactivate(): UICommonModalConfig | false {
    if (this.detailElement.isDirty) {
      if (JSON.stringify(this.detail) === '{}') {
        return {};
      }
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.detailElement?.item.name,
      };
    }
    return false;
  }

  delete(itemToDelete: Expert | undefined) {
    if (itemToDelete) {
      this.crudSrv
        .deleteActionModal(this.apiService, itemToDelete)
        .pipe(
          tap(() => {
            this.deleted.emit(itemToDelete);
            if (this.detailElement) {
              this.detailElement.haveChanges = false;
            }
          }),
          tap(() => {
            this.router.navigate([this.urlListado]);
          })
        )
        .subscribe();
    }
  }

  handleSubmitForm(itemToSave: Expert) {
    this.crudSrv
      .updateOrSaveModal(
        {
          ...this,
          methodToApply: itemToSave.identifier ? 'PUT' : 'POST',
        },
        itemToSave,
        groupType.EXPERT
      )
      .pipe(
        tap((response) => {
          this.detailElement.haveChanges = false;
          this.changed.emit(response.data);
          if (!itemToSave.identifier) {
            this.router.navigate([`${this.urlListado}/view/${response.data?.identifier}`]);
          }
        }),
        catchError((e: HttpErrorResponse) => {
          if (e.status === 409) {
            this.detailElement?.form?.controls.name.setErrors({ error_409: true });
          }
          return of(null);
        })
      )
      .subscribe();
  }

  clone(detail: Expert | undefined) {
    if (detail) {
      detail = this.detailElement.getDataForSave() as unknown as Expert;
    }
    if (this.detailElement.isDirty) {
      this.toastService.error('COMMON_ERRORS.CANNOT_CLONE');
      return;
    }
    if (this.detailElement?.form?.valid) {
      super.clone(detail, this.urlListado + '/view');
    } else {
      this.crudSrv.toast.warning('COMMON_ERRORS.CLONE_FORM_ERROR');
    }
  }
}
