import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultLayoutComponent } from './app-layouts/default-layout/default-layout.component';

const routes: Routes = [
  {
    path: 'docs',
    runGuardsAndResolvers: 'always',
    loadChildren: () => import('./docs/docs.module').then((m) => m.DocsModule),
  },
  {
    path: 'unauthorized',
    runGuardsAndResolvers: 'always',
    loadChildren: () => import('./unauthorized/unauthorized.module').then((m) => m.UnauthorizedModule),
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    children: [
      {
        path: 'examples',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./examples/examples.module').then((m) => m.ExamplesModule),
      },
      {
        path: 'stock',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./stock/stock.module').then((m) => m.StockModule),
      },
      {
        path: 'promotions',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./promotions/promotions.module').then((m) => m.PromotionsModule),
      },
      {
        path: 'rule-engine',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./rule-engine/rule-engine.module').then((m) => m.RuleEngineModule),
      },
      {
        path: 'crosselling',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./marketplace/crosselling/crosselling.module').then((m) => m.CrossellingModule),
      },
      {
        path: 'sites',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./sites/sites.module').then((m) => m.SitesModule),
      },
      {
        path: 'seller',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./marketplace/seller/seller.module').then((m) => m.SellerModule),
      },
      {
        path: 'categories',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./marketplace/categories/categories.module').then((m) => m.CategoriesModule),
      },
      {
        path: 'catalog/products-mkp',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./catalog/product-mkp/product-mkp.module').then((m) => m.ProductMkpModule),
      },
      {
        path: 'facets',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./catalog/facet/facet.module').then((m) => m.FacetModule),
      },
      {
        path: 'rule-query',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./rule-query/rule-query.module').then((m) => m.RuleQueryModule),
      },
      {
        path: 'delivery',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./delivery/delivery.module').then((m) => m.DeliveryModule),
      },
      {
        path: 'facet-management',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./catalog/facet-management/facet-management.module').then((m) => m.FacetManagementModule),
      },
      {
        path: 'catalog/technical-characteristics',
        runGuardsAndResolvers: 'always',
        loadChildren: () =>
          import('./catalog/technical-characteristics/technical-characteristics.module').then((m) => m.TechnicalCharacteristicsModule),
      },
      {
        path: 'catalog/content-group',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./catalog/content-group/content-group.module').then((m) => m.ContentGroupModule),
      },
      {
        path: 'attribute-translation',
        runGuardsAndResolvers: 'always',
        loadChildren: () =>
          import('./catalog/attribute-translation/attribute-translation.module').then((m) => m.AttributeTranslationModule),
      },
      {
        path: 'searcher',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./searcher/searcher.module').then((m) => m.SearcherModule),
      },
      {
        path: 'control-panel',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./control-panel/control-panel.module').then((m) => m.ControlPanelModule),
      },
      {
        path: 'sitemap',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./sitemap/sitemap.module').then((m) => m.SitemapModule),
      },
      {
        path: 'audits',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./audits/audits.module').then((m) => m.AuditsModule),
      },
      {
        path: 'dashboard',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./dashboard/dashboard-query/dashboard-query.module').then((m) => m.DashboardQueryModule),
      },
      {
        path: 'price-inheritance',
        runGuardsAndResolvers: 'always',
        loadChildren: () => import('./rule-engine/price-inheritance/price-inheritance.module').then((m) => m.PriceInheritanceModule),
      },
      {
        path: '',
        redirectTo: 'sitemap',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false, useHash: false, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
