import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

export const authConfig: AuthConfig = {
  clientId: environment.OAUTH_CLIENT_ID,
  dummyClientSecret: environment.OAUTH_CLIENT_SECRET,
  scope: environment.OAUTH_SCOPE,
  issuer: environment.OAUTH_ISSUER_URL,
  loginUrl: environment.OAUTH_LOGIN_URL,
  logoutUrl: environment.OAUTH_LOGOUT_URL,
  tokenEndpoint: environment.OAUTH_TOKEN_URL,
  userinfoEndpoint: environment.OAUTH_USER_INFO_URL,
  responseType: environment.OAUTH_RESPONSE_TYPE,
  skipIssuerCheck: true,
  redirectUri: environment.OAUTH_CALLBACK,
  silentRefreshRedirectUri: environment.OAUTH_CALLBACK,
  requireHttps: 'remoteOnly',
  options: null,
  useSilentRefresh: !!environment.OAUTH_IS_ACTIVE_SILENT_REFRESH,
  showDebugInformation: false,
  disableAtHashCheck: false,
  skipSubjectCheck: false,
  clearHashAfterLogin: false,
  sessionChecksEnabled: !!environment.OAUTH_IS_ACTIVE_SILENT_REFRESH,
  sessionCheckIFrameUrl:
    environment.OAUTH_SESSION_CHECK_URL + '?client_id=' + environment.OAUTH_CLIENT_ID + '&redirect_uri=' + environment.OAUTH_CALLBACK,
  sessionCheckIntervall: 5000,
  // timeoutFactor: 0.1
};
