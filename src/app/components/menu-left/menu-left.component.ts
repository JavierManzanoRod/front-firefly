import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { PATH_ASSETS } from '@env/custom-variables';
import { filter } from 'rxjs/operators';
import { SearchProvider } from 'src/app/shared/components/list-search/providers/search-provider';
import packageInfo from '../../../../package.json';
import { UiStateService } from '../../shared/services/ui-state.service';
import { MenuLeft } from './model/menu-left-model';
import { MenuLeftService } from './services/menu-left.service';

@Component({
  selector: 'ff-menu-left',
  templateUrl: './menu-left.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./menu-left.component.scss'],
})
export class MenuLeftComponent implements OnInit {
  @Input() items!: MenuLeft[];
  @Output() isMenuOpenChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() isMenuOpen = true;
  urlActive = '';
  routeLogo = PATH_ASSETS + 'assets/images/logo.svg';

  menuActive: MenuLeft | undefined;
  versionStr = packageInfo.version;
  init = false;

  constructor(
    protected menuItemsService: MenuLeftService,
    public router: Router,
    private searchProvider: SearchProvider,
    private uiStateService: UiStateService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.uiStateService.subscribeMenuIsOpen((isOpen: boolean) => this.menuChangeIsOpen(isOpen));
  }

  @HostListener('window:resize', ['$event'])
  rePosition() {
    setTimeout(() => {
      const items = document.querySelectorAll('.menu.closed .content-menu .content-item.activeColapse');
      items.forEach((value: any) => {
        this.position(value);
      });
    }, 10);
  }

  @HostListener('document:click', ['$event'])
  onClick(event: any) {
    if (!this.isMenuOpen && event.path) {
      let onMenu = false;
      event.path.forEach((element: HTMLElement) => {
        if (element && element.classList && element.classList.contains('menu') && element.classList.contains('closed')) {
          onMenu = true;
        }
      });
      if (!onMenu) {
        this.items.forEach((item) => {
          item.expanded = false;
        });
      }
    }
  }

  ngOnInit() {
    const url = this.router.routerState.snapshot.url;

    this.router.events.pipe(filter((e) => e instanceof NavigationEnd)).subscribe(() => {
      if (this.menuActive) {
        this.menuItemsService.canBack = true;
      }

      const urlActive = this.parseUrl(this.router.url);
      this.searchItemActive(urlActive);

      if (!this.isMenuOpen) {
        this.items.forEach((item) => (item.expanded = false));
      }
    });

    if (!this.items) {
      this.items = this.menuItemsService.getMenuItem();
    }
    setTimeout(() => {
      const urlActive = this.parseUrl(url);
      this.searchItemActive(urlActive);
      this.init = true;
    }, 500);
  }

  getMenuActive(items: MenuLeft[], url: string): MenuLeft | undefined {
    return items.find((value: MenuLeft) => {
      return (value.link.trim() && url.includes(value.link)) || value.subitems?.find((v: MenuLeft) => v.link && url.includes(v.link));
    });
  }

  toggleMenu() {
    this.uiStateService.toggleMenu();
  }

  toogleMenuItem(item: MenuLeft) {
    if (item?.subitems?.length) {
      if (!item.expanded) {
        this.items.forEach((menuItem: MenuLeft) => {
          if (menuItem.label !== this.menuActive?.label || !this.isMenuOpen) {
            menuItem.expanded = false;
          }
        });
      }
      item.expanded = true;
    } else {
      this.go(item, item);
    }
    this.rePosition();
  }

  go(item: MenuLeft, root: MenuLeft) {
    if (this.isMenuOpen) {
      this.items.forEach((v) => {
        if (v.link !== item.link && !v?.subitems?.find((a) => a.link === item.link)) {
          v.expanded = false;
        }
      });
    }

    this.menuActive = root;
    this.searchProvider.data = null;
    this.router.navigate([item.link]);
  }

  parseUrl(url: string): string {
    const temp = url.split('/');
    return temp.length >= 3 ? `/${temp[1]}/${temp[2]}` : `/${temp[1]}`;
  }

  isSubItemActive(itemLink: string): boolean {
    if (!this.urlActive) {
      return false;
    }

    return this.parseUrl(this.urlActive) === itemLink;
  }

  position(menuItem: HTMLElement) {
    if (menuItem.classList.contains('activeColapse')) {
      const nameItem: HTMLElement | null = menuItem.querySelector('.icon-and-name');
      if (nameItem) {
        const dropdownItem: HTMLElement | null = nameItem.querySelector('.collapse');
        if (dropdownItem) {
          dropdownItem.style.display = 'block';
          nameItem.style.transform = 'translateY(0px)';

          const top = menuItem.getBoundingClientRect().top;
          const totalHeight = nameItem.getBoundingClientRect().height + dropdownItem.getBoundingClientRect().height;
          if (top + totalHeight > window.innerHeight) {
            const diff = top + totalHeight - window.innerHeight;
            // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
            nameItem.style.transform = 'translateY(-' + diff + 'px)';
          }
          menuItem.style.top = `${top}px`;
        } else {
          // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
          menuItem.style.top = menuItem.getBoundingClientRect().top + 'px';
        }
      }
    }
  }
  private menuChangeIsOpen(isOpen: boolean) {
    if (this.init) {
      this.isMenuOpen = isOpen;
      this.isMenuOpenChange.emit(isOpen);
      if (!this.isMenuOpen) {
        this.items.forEach((item) => (item.expanded = false));
      } else {
        if (this.menuActive) {
          this.toogleMenuItem(this.menuActive);
        }
      }
      this.rePosition();
    } else {
      setTimeout(() => {
        this.menuChangeIsOpen(isOpen);
      }, 1400);
    }
  }

  private searchItemActive(url: string) {
    const urlActive = this.parseUrl(url);
    this.urlActive = urlActive;
    this.menuActive = this.getMenuActive(this.items, urlActive);
    if (this.menuActive && this.menuActive.subitems && this.menuActive.subitems?.length > 0) {
      this.toogleMenuItem(this.menuActive);
    }

    this.changeDetectorRef.markForCheck();
  }
}
