import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { FF_ACTIVE_FUNCIONALITIES } from 'src/app/configuration/tokens/functionality.token';
import { Functionality, MenuLeft } from '../model/menu-left-model';

@Injectable({
  providedIn: 'root',
})
export class MenuLeftService {
  _canBack = false;

  get canBack() {
    return this._canBack;
  }

  set canBack(v: boolean) {
    this._canBack = v;
  }

  constructor(private router: Router, @Inject(FF_ACTIVE_FUNCIONALITIES) public allowedOptions: Functionality[]) {}

  showOptionMenu(options: any): boolean {
    return options.every((item: any) => this.allowedOptions.indexOf(item) >= 0);
  }

  dataMenuItem(): MenuLeft[] {
    const lastItems: MenuLeft[] = [
      {
        label: 'APP_MENU.SITEMAP',
        link: '/sitemap',
        image: 'icon-sitemap',
        show: false,
        id: 'site-map',
      },
    ];

    const menuItems: MenuLeft[] = [
      {
        label: 'APP_MENU.HOME',
        link: '/sitemap',
        image: 'icon-house',
        show: true,
        id: 'site-map',
      },
      {
        label: 'APP_MENU.BUSINESS_RULES',
        link: '',
        id: 'business_rules',
        image: 'icon-dashboard',
        show: true,
        subitems: [
          {
            label: 'APP_MENU.PRODUCT_EXHAUSTER',
            link: '/control-panel/spenter',
            show: false,
            id: 'spenter',
          },
          {
            label: 'APP_MENU.PRODUCT_GROUPING',
            link: '/rule-engine/category-rule',
            show: this.showOptionMenu([Functionality.entityType, Functionality.categoryRule]),
            id: 'category-rule',
          },
          {
            label: 'APP_MENU.BADGE',
            link: '/control-panel/badge',
            id: 'badge',
            show: this.showOptionMenu([Functionality.badge]),
          },
          {
            label: 'APP_MENU.EXPERT_ADMIN',
            id: 'expert-admin',
            link: '/control-panel/expert-admin',
            show: this.showOptionMenu([Functionality.expertRule]),
          },
          {
            label: 'APP_MENU.LOYALTY',
            id: 'loyalty',
            link: '/control-panel/loyalty',
            show: this.showOptionMenu([Functionality.loyalty]),
          },
          {
            label: 'APP_MENU.FLUORINATED_GAS',
            id: 'fluorinated-gas',
            link: '/control-panel/fluorinated-gas',
            show: this.showOptionMenu([Functionality.fluorinatedGasRule]),
          },
          {
            label: 'APP_MENU.CONTENT_GROUP',
            link: '/catalog/content-group',
            id: 'content-group',
            show: this.showOptionMenu([Functionality.contentGroup]),
          },
          {
            label: 'APP_MENU.SIZE_GUIDE',
            link: '/control-panel/size-guide',
            show: this.showOptionMenu([Functionality.sizeGuide]),
            id: 'size-guide',
          },
          {
            label: 'APP_MENU.PRICE_INHERITANCE',
            id: 'price-inheritance',
            link: '/rule-engine/price-inheritance',
            show: this.showOptionMenu([Functionality.priceInheritance]),
          },
          {
            label: 'APP_MENU.LIMIT_SALES',
            id: 'limit-sale',
            link: '/stock/limit-sale',
            show: this.showOptionMenu([Functionality.limitSale]),
          },
          {
            label: 'APP_MENU.EXCLUDE_SEARCH',
            id: 'exclude-search',
            link: '/stock/exclude-search',
            show: this.showOptionMenu([Functionality.excludeSearch]),
          },
          {
            label: 'APP_MENU.SPECIAL_PRODUCT',
            link: '/control-panel/special-product',
            show: this.showOptionMenu([Functionality.specialProduct]),
            id: 'special-product',
          },
        ],
      },
      {
        label: 'APP_MENU.CATALOG',
        link: '/catalogo',
        id: 'catalog',
        image: 'icon-t-shirt',
        show: true,
        subitems: [
          {
            label: 'APP_MENU.ATTRIBUTE_MANAGEMENT',
            short: 'APP_MENU_SHORT.ATTRIBUTE_MANAGEMENT',
            id: 'attribute-translation',
            link: '/attribute-translation',
            show: this.showOptionMenu([Functionality.catalogFacetsTranslations, Functionality.entityType]),
          },
          {
            label: 'APP_MENU.FACETS_CONSULTATION',
            link: '/facet-management',
            id: 'facet-management',
            show: this.showOptionMenu([Functionality.catalogFacetsManagment]),
          },
          {
            label: 'APP_MENU.FACETS',
            id: 'facets',
            link: '/facets',
            show: this.showOptionMenu([Functionality.catalogFacets]),
          },
          {
            label: 'APP_MENU.TECHNICAL_SHEET',
            id: 'technical-characteristics',
            link: '/catalog/technical-characteristics',
            show: this.showOptionMenu([Functionality.technicalCharacteristics]),
          },
        ],
      },
      {
        label: 'APP_MENU.MKP_ADMINISTRATOR',
        link: '',
        id: 'mkp',
        show: this.showOptionMenu([Functionality.mkpSeller]),
        image: 'icon-mk-vendor',
        subitems: [
          {
            label: 'APP_MENU.QUERY',
            id: 'crosselling-query',
            link: '/crosselling/query',
            show: this.showOptionMenu([Functionality.mkpRelation]),
          },
          {
            label: 'APP_MENU.CROSSELLING_MANAGEMENT',
            id: 'crosselling-management',
            link: '/crosselling/management',
            show: this.showOptionMenu([Functionality.mkpRelation]),
          },
          {
            label: 'APP_MENU.CROSSELLING_LOAD_HISTORY',
            id: 'crosselling-historic',
            short: 'APP_MENU_SHORT.CROSSELLING_LOAD_HISTORY',
            link: '/crosselling/historic',
            show: this.showOptionMenu([Functionality.mkpRelation]),
          },
          {
            label: 'APP_MENU.PRODUCTS_TYPES_MKP',
            id: 'products-mkp',
            link: '/catalog/products-mkp',
            show: this.showOptionMenu([Functionality.mkpProduct]),
          },
          {
            label: 'APP_MENU.VENDORS',
            link: '/seller',
            id: 'seller',
            show: this.showOptionMenu([Functionality.mkpSeller]),
          },
        ],
      },
      {
        label: 'APP_MENU.QUERIES',
        link: '/dashboard',
        show: true,
        id: 'queries',
        image: 'icon-cuadro-de-mando',
        subitems: [
          {
            label: 'APP_MENU.APPLICATION_OF_RULES',
            link: '/rule-query/evaluate-rules',
            id: 'evaluate-rules',
            show: this.showOptionMenu([Functionality.evaluateRules]),
          },
          {
            label: 'APP_MENU.CATALOG_STATUS',
            link: '/dashboard',
            id: 'catalog-status',
            show: this.showOptionMenu([Functionality.dashboard]),
          },
          {
            label: 'APP_MENU.PROMOTIONS_BY_ACTIONS',
            short: 'APP_MENU_SHORT.PROMO_BY_ACTIONS',
            id: 'promotions-actions',
            link: '/promotions/actions',
            show: this.showOptionMenu([Functionality.promo]),
          },
          {
            label: 'APP_MENU.PROMOTIONS_BY_REFERENCE',
            short: 'APP_MENU_SHORT.PROMO_BY_REFERENCE',
            id: 'promotions-reference',
            link: '/promotions/products',
            show: this.showOptionMenu([Functionality.promo]),
          },
          {
            label: 'APP_MENU.RULES_PER_PRODUCT',
            link: '/rule-query/satisfied-rules',
            id: 'satisfied-rules',
            show: this.showOptionMenu([Functionality.satisfiedRules]),
          },
        ],
      },
      {
        label: 'APP_MENU.AUDIT',
        link: '/audits',
        id: 'audits',
        show: this.showOptionMenu([Functionality.audits]),
        image: 'icon-auditorias',
      },
      {
        label: 'APP_MENU.FIREFLY_ADMINISTRATION',
        short: 'APP_MENU_SHORT.FIREFLY_ADMINISTRATION',
        link: '',
        id: 'firefly-administration',
        show: true,
        image: 'icon-icon-consulta-regla',
        subitems: [
          {
            label: 'APP_MENU.ADMIN_GROUP_TYPE',
            id: 'admin-group-type',
            link: '/rule-engine/admin-group-type',
            show: this.showOptionMenu([Functionality.adminGroupType]),
          },
          {
            label: 'APP_MENU.ADMIN_GROUPS',
            id: 'admin-group',
            link: '/rule-engine/admin-group',
            show: this.showOptionMenu([Functionality.adminGroup]),
          },
          {
            label: 'APP_MENU.ENTITY_TYPES',
            id: 'entity-types',
            link: '/rule-engine/entity-types',
            show: this.showOptionMenu([Functionality.entityType]),
          },
          {
            label: 'APP_MENU.EXPERT',
            link: '/control-panel/expert',
            id: 'expert',
            show: this.showOptionMenu([Functionality.expert]),
          },
          {
            label: 'APP_MENU.IMPORT_SCHEMA',
            id: 'searcher',
            link: '/searcher',
            show: this.showOptionMenu([Functionality.schema]),
          },
          {
            label: 'APP_MENU.SITES',
            id: 'site',
            link: '/sites/site',
            show: this.showOptionMenu([Functionality.sites, Functionality.subsites]),
          },
        ],
      },
      {
        label: 'APP_MENU.SHIPPING',
        link: '',
        show: true,
        id: 'shipping',
        image: 'icon-truck',
        subitems: [
          {
            label: 'APP_MENU.AMBITS',
            id: 'scopes',
            link: '/delivery/scopes',
            show: this.showOptionMenu([Functionality.scopes, Functionality.entity]),
          },
          {
            label: 'APP_MENU.DESTINATIONS',
            id: 'targets',
            link: '/delivery/targets',
            show: this.showOptionMenu([Functionality.targets, Functionality.scopes]),
          },
        ],
      },
    ];
    return [...menuItems, ...lastItems];
  }

  getMenuItem(): MenuLeft[] {
    const menu = this.dataMenuItem();
    const menuToShow: MenuLeft[] = [];

    menu.forEach((item) => {
      const withoutSubitems: MenuLeft = { ...item };
      withoutSubitems.subitems = [];

      // Solo incluimos opciones principales si tienen el show a true y no tienen subitems o si tienen
      // el show a true y alguno de sus subitems tienen el show a true
      if (item.show) {
        if (!item.subitems) {
          menuToShow.push(withoutSubitems);
        } else {
          const showMainOption = item.subitems.some((subitem) => subitem.show);
          if (showMainOption) {
            menuToShow.push(withoutSubitems);
          }
        }
      }

      // Añadimos solo los subitems que deben mostrarse en caso de que la operativa de la que dependan esté activa
      if (item.show && item.subitems) {
        item.subitems.forEach((subitem) => {
          if (subitem.show) {
            menuToShow[menuToShow.length - 1].subitems?.push(subitem);
          }
        });
      }
    });

    return menuToShow;
  }
}
