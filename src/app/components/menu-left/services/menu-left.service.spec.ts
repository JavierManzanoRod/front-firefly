import { Router } from '@angular/router';
import { Functionality, MenuLeft } from '../model/menu-left-model';
import { MenuLeftService } from './menu-left.service';

describe('MenuLeftService', () => {
  function filterVisibles(xs: MenuLeft[]) {
    const ys = xs.map((x) => {
      if (!x.subitems) {
        return x;
      }
      const subitems = x.subitems.filter((e) => e.show);
      return {
        ...x,
        subitems: subitems?.length ? subitems : null,
      };
    });
    return ys.filter((e) => e.subitems || e.link);
  }

  let serviceToTest: MenuLeftService;

  it('UAT,providing adminGroup, entityType only show Incio an Motor de Reglas ', () => {
    serviceToTest = new MenuLeftService({ url: '' } as unknown as Router, [Functionality.adminGroup, Functionality.entityType]);
    const menuItems = serviceToTest.getMenuItem();
    const customFilter = filterVisibles(menuItems);
    expect(customFilter.length).toBe(2);
  });

  it('UAT,providing adminGroup, entityType and a wrong value only show Incio an Motor de Reglas ', () => {
    serviceToTest = new MenuLeftService(
      { url: '' } as unknown as Router,
      [Functionality.adminGroup, Functionality.entityType, 'wrong'] as unknown as Functionality[]
    );
    const menuItems = serviceToTest.getMenuItem();
    const customFilter = filterVisibles(menuItems);
    expect(customFilter.length).toBe(2);
  });

  it('UAT,providing promo Inicio and Promos ', () => {
    serviceToTest = new MenuLeftService({ url: '' } as unknown as Router, [Functionality.promo]);

    const menuItems = serviceToTest.getMenuItem();
    const customFilter = filterVisibles(menuItems);
    expect(customFilter.length).toBe(2);
  });
});
