export interface MenuLeft {
  link: string;
  id?: string;
  label: string;
  short?: string;
  image?: string;
  subitems?: MenuLeft[];
  expanded?: boolean;
  show?: boolean;
}

export enum Functionality {
  adminGroup = 'adminGroup',
  adminGroupType = 'adminGroupType',
  audits = 'audits',
  badge = 'badge',
  catalogFacets = 'catalogFacets',
  catalogFacetsManagment = 'catalogFacetsManagment',
  catalogFacetsTranslations = 'catalogFacetsTranslations',
  categoryRule = 'categoryRule',
  contentGroup = 'contentGroup',
  dashboard = 'dashboard',
  entity = 'entity',
  entityType = 'entityType',
  evaluateRules = 'evaluateRules',
  excludeSearch = 'excludeSearch',
  expert = 'expert',
  expertAdmin = 'expertAdmin',
  expertRule = 'expertRule',
  fluorinatedGasRule = 'fluorinatedGasRule',
  limitSale = 'limitSale',
  loyalty = 'loyalty',
  mkpProduct = 'mkpProduct',
  mkpRelation = 'mkpRelation',
  mkpSeller = 'mkpSeller',
  priceInheritance = 'priceInheritance',
  promo = 'promo',
  satisfiedRules = 'satisfiedRules',
  schema = 'schema',
  scopes = 'scopes',
  sites = 'sites',
  sizeGuide = 'sizeGuide',
  specialProduct = 'specialProduct',
  subsites = 'subsites',
  targets = 'targets',
  technicalCharacteristics = 'technicalCharacteristics',
}
