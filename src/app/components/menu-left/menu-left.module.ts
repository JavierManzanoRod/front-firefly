import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MenuLeftComponent } from './menu-left.component';
import { MenuLeftService } from './services/menu-left.service';

@NgModule({
  declarations: [MenuLeftComponent],
  imports: [SharedModule],
  providers: [MenuLeftService],
  exports: [MenuLeftComponent],
})
export class MenuLeftModule {}
