import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MenuTopComponent } from './menu-top.component';
import { MenuTopService } from './services/menu-top.service';

@NgModule({
  declarations: [MenuTopComponent],
  imports: [SharedModule],
  providers: [MenuTopService],
  exports: [MenuTopComponent],
})
export class MenuTopModule {}
