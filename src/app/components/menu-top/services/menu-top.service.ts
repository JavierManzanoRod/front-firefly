import { Injectable } from '@angular/core';
import { Profile } from '../model/menu-top.model';

@Injectable({
  providedIn: 'root',
})
export class MenuTopService {
  getProfile() {
    return dataProfile();
  }
}

function dataProfile(): Profile {
  const response: Profile = {
    image: '',
    user_name: 'Ángelo Ángelo Panez Panezlo Panez PanezÁngelo Panezz',
  };

  return response;
}
