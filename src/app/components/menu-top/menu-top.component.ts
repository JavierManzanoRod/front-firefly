import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { User } from '@model/user';
import { filter } from 'rxjs/operators';
import { UserService } from 'src/app/shared/services/user.service';
import { Profile } from './model/menu-top.model';
import { MenuTopService } from './services/menu-top.service';

@Component({
  selector: 'ff-menu-top',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './menu-top.component.html',
  styleUrls: ['./menu-top.component.scss'],
})
export class MenuTopComponent implements OnInit, OnChanges {
  @Input() isMenuOpen = true;
  @Input() user!: User;
  routeData: any;

  item!: Profile;
  userName!: string;

  constructor(
    private apiService: MenuTopService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private userService: UserService,
    private ref: ChangeDetectorRef
  ) {
    this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe(() => {
      this.routeData = this.getRouteData();
      this.ref.markForCheck(); // Update the view (ChangeDetectionStrategy.OnPush)
    });
  }

  ngOnInit() {
    this.item = this.apiService.getProfile();
    this.getUser();
  }

  ngOnChanges(): void {
    this.getUser();
  }

  getUser() {
    if (this.user) {
      this.userName = this.user.given_name || this.user.sub || '';
    } else {
      this.userName = '';
    }
  }

  getRouteData() {
    let route: any = this.activatedRoute;
    while (route.firstChild) {
      route = route.firstChild;
    }
    return route.data.value;
  }

  logOut() {
    this.router.navigate(['/']).then((canNavigate: boolean) => {
      if (canNavigate || this.router.url === '/') {
        this.userService.logOut();
        this.userService.userSubject.next(this.userService.getUser());
      }
    });
  }
}
