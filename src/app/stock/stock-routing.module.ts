import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'limit-sale',
    loadChildren: () => import('./limit-sale/limit-sale.module').then((m) => m.LimitSaleModule),
  },
  {
    path: 'exclude-search',
    loadChildren: () => import('./exclude-search/exclude-search.module').then((m) => m.ExcludeSearchModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StockRoutingModule {}
