import { GenericApiResponse } from '@model/base-api.model';
import { Conditions2DTO } from 'src/app/catalog/content-group/model/content-group-dto-model';
import { Conditions2 } from 'src/app/modules/conditions2/models/conditions2.model';

export interface ExcludeSearch {
  id?: string;
  name: string;
  active: boolean;
  start_date?: string | Date;
  is_basic: boolean;
  start_time?: string | Date;
  end_date?: string | Date;
  end_time?: string | Date;
  node?: Conditions2;
  admin_group_type_id?: string;
}

export interface ExcludeSearchDTO {
  identifier?: string;
  name: string;
  is_active: boolean;
  is_basic: boolean;
  start_date?: string | Date;
  start_time?: string | Date;
  end_date?: string | Date;
  end_time?: string | Date;
  node?: Conditions2DTO;
}

export type ExcludeSearchHeaderView = Omit<ExcludeSearch, 'node'>;

export type ExcludeSearchResponse = GenericApiResponse<ExcludeSearch>;

export type ExcludeSearchResponseDTO = GenericApiResponse<ExcludeSearchDTO>;
