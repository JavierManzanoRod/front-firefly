import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { SortApiType } from '@core/models/sort.model';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { ExcludeSearch } from '../models/exclude-search.model';
import { ExcludeSearchService } from '../services/exclude-search.service';
import { GenericListConfig } from './../../../modules/generic-list/models/generic-list.model';

@Component({
  selector: 'ff-exclude-search-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header [pageTitle]="'EXCLUDE_SEARCH.HEADER_LIST' | translate">
        <a [routerLink]="['new']" class="btn btn-primary">
          {{ 'EXCLUDE_SEARCH.CREATE_RULE' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [list]="list$ | async"
                [loading]="loading"
                [page]="page"
                [checkDateActive]="checkDateActive"
                [valueSearch]="searchName"
                (search)="search($event)"
                [currentData]="currentData"
                (delete)="delete($event)"
                (sortEvent)="sort($event)"
                (pagination)="handlePagination($event)"
                [placeHolder]="'EXCLUDE_SEARCH.SEARCH' | translate"
                [type]="type"
                [title]="''"
                [arrayKeys]="arrayKeys"
                [route]="'/stock/exclude-search/view/'"
              >
              </ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class ExcludeSearchListContainerComponent extends BaseListContainerComponent<ExcludeSearch> implements OnInit {
  hide = false;
  type = TypeSearch.simple;
  arrayKeys: GenericListConfig[] = [
    {
      key: 'name',
      headerName: 'EXCLUDE_SEARCH.NAME',
      showOrder: true,
    },
    {
      key: 'start_date',
      headerName: 'EXCLUDE_SEARCH.START_DATE',
      textCenter: true,
      canFormatDate: true,
      formatDate: 'dd-MM-yyyy H:mm',
    },
    {
      key: 'end_date',
      headerName: 'EXCLUDE_SEARCH.END_DATE',
      textCenter: true,
      canFormatDate: true,
      formatDate: 'dd-MM-yyyy H:mm',
    },
    {
      key: 'active',
      headerName: 'EXCLUDE_SEARCH.ACTIVE',
      textCenter: true,
      canActiveClass: true,
    },
  ];

  constructor(public apiService: ExcludeSearchService, public crudSrv: CrudOperationsService) {
    super(crudSrv, apiService);
  }

  ngOnInit() {
    this.sort({ name: 'name', type: SortApiType.asc });
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
