import { AdminGroup } from './../../../rule-engine/admin-group/models/admin-group.model';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { EntityType } from '@core/models/entity-type.model';
import { CrudOperationsService } from '@core/services/crud-operations.service';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { ConditionsBasicAndAdvanceComponent } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.component';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { ExcludeSearch } from '../models/exclude-search.model';
import { ExcludeSearchService } from '../services/exclude-search.service';
import { ExcludeSearchDetailContainerComponent } from './exclude-search-detail-container.component';

const textToShowWhenError = 'Se ha producido un error inesperado: el sistema no ha podido guardar el registro.';
const textToShowWhenError409 = 'Error, el registro está siendo usado en otra entidad';

//  starts global mocks
class TranslateServiceStub {
  public setDefaultLang(data?: any) {}
  public get(key: any): any {
    of(key);
  }
}
class ToastrServiceStub {
  public success() {}
}

class BsModalServiceStub {}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Component({
  selector: 'ff-page-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockHeaderComponent {
  @Input() pageTitle: any;
  @Input() breadcrumbTitle: any;
  @Input() breadcrumbLink: any;
}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };
  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }
  public hide() {
    return true;
  }
}

@Component({
  selector: 'ff-exclude-search-detail',
  template: '<p>Mock Product Editor Component</p>',
})
class MockDetailComponent {
  @Input() loading!: boolean;
  @Input() item: any;
  @Input() entityTypes!: EntityType[];
}

describe('ExcludeSearchDetailContainerComponent', () => {
  let itemServiceSpy: jasmine.SpyObj<ExcludeSearchService>;
  let modalMockInstance: ModalServiceMock;
  let router: Router;

  beforeEach(
    waitForAsync(() => {
      const spy: jasmine.SpyObj<ExcludeSearchService> = jasmine.createSpyObj('ExcludeSearchService', [
        'detail',
        'update',
        'post',
        'delete',
        'emptyRecord',
        'entityTypes',
      ]);

      TestBed.configureTestingModule({
        declarations: [
          ExcludeSearchDetailContainerComponent,
          MockDetailComponent,
          TranslatePipeMock,
          MockHeaderComponent,
          ConditionsBasicAndAdvanceComponent,
        ],
        imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule, ReactiveFormsModule],
        providers: [
          {
            provide: CrudOperationsService,
          },
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: ToastService, useClass: ToastrServiceStub },
          { provide: BsModalService, useClass: ModalServiceMock },
          { provide: ExcludeSearchService, useValue: spy },
        ],
      }).compileComponents();

      // eslint-disable-next-line , import/no-deprecated
      router = TestBed.get(Router);

      // eslint-disable-next-line , import/no-deprecated
      itemServiceSpy = TestBed.get(ExcludeSearchService);
      itemServiceSpy.update.and.returnValue(of(null as any));
      itemServiceSpy.delete.and.returnValue(of(null as any));

      // eslint-disable-next-line import/no - deprecated;
      modalMockInstance = TestBed.get(BsModalService);
    })
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(ExcludeSearchDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should handle updates existing items', () => {
    itemServiceSpy.update.and.returnValue(of({ status: 200, data: null as any }));
    const fixture = TestBed.createComponent(ExcludeSearchDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const data: ExcludeSearch = {
      name: 'name',
      id: '1',
    } as ExcludeSearch;
    component.detailElement = {};
    component.handleSubmitForm(data);
    expect(itemServiceSpy.update.calls.count()).toBe(1, 'spy update method was called once');
  });

  it('should handle posts of new items and then redirect to the list the list page', () => {
    itemServiceSpy.post.and.returnValue(of({ status: 200, data: null as any }));
    const fixture = TestBed.createComponent(ExcludeSearchDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const data: ExcludeSearch = {
      name: 'name',
    } as ExcludeSearch;
    component.detailElement = {};
    component.handleSubmitForm(data);
    expect(itemServiceSpy.post.calls.count()).toBe(1, 'spy post method was called once');
  });

  it('should handle deleting items and then redirect to the list page', () => {
    const navigateSpy = spyOn(router, 'navigate');
    const fixture = TestBed.createComponent(ExcludeSearchDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    component.detailElement = {};
    const item: ExcludeSearch = {
      id: '1',
      name: 'name',
    } as ExcludeSearch;

    component.delete(item);
    expect(itemServiceSpy.delete.calls.count()).toBe(1, 'spy delete method was called once');
    expect(navigateSpy).toHaveBeenCalledWith(['/stock/exclude-search']);
  });

  it('ngOnInit initialize Exclude search', () => {
    const item: ExcludeSearch = {
      id: '1',
      name: 'name',
      node: null as any,
    } as ExcludeSearch;

    itemServiceSpy.detail.and.returnValue(of(item as AdminGroup));

    const fixture = TestBed.createComponent(ExcludeSearchDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    component.id = 4;
    component.detailElement = {};
    component.ngOnInit();
    expect(itemServiceSpy.detail.calls.count()).toBe(1, 'spy delete method was called once');
  });
});
