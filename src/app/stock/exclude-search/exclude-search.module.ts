import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@core/core.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ControlPanelSharedModule } from 'src/app/control-panel/shared/control-panel-shared.module';
import { ConditionsBasicAndAdvanceModule } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.module';
import { ConditionsBasicModule } from 'src/app/modules/conditions-basic/conditions-basic.module';
import { Conditions2Module } from 'src/app/modules/conditions2/conditions2.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModalModule } from '../../modules/common-modal/common-modal.module';
import { GenericListComponentModule } from './../../modules/generic-list/generic-list.module';
import { ExcludeSearchDetailContainerComponent } from './containers/exclude-search-detail-container.component';
import { ExcludeSearchListContainerComponent } from './containers/exclude-search-list-container.component';
import { ExcludeSearchRoutingModule } from './exclude-search-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ExcludeSearchRoutingModule,
    TimepickerModule,
    CommonModalModule,
    Conditions2Module,
    ConditionsBasicModule,
    TabsModule.forRoot(),
    DatetimepickerModule,
    GenericListComponentModule,
    SimplebarAngularModule,
    CoreModule,
    ControlPanelSharedModule,
    ConditionsBasicAndAdvanceModule,
  ],
  declarations: [ExcludeSearchListContainerComponent, ExcludeSearchDetailContainerComponent],
  providers: [],
  exports: [ExcludeSearchListContainerComponent, ExcludeSearchDetailContainerComponent],
})
export class ExcludeSearchModule {}
