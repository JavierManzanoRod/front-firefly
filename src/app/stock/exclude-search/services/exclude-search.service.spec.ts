import { EntityType } from '@core/models/entity-type.model';
import { GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { AdminGroupTypeService } from 'src/app/rule-engine/admin-group-type/services/admin-group-type.service';
import { ExcludeSearch } from '../models/exclude-search.model';
import { ExcludeSearchService } from './exclude-search.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: ExcludeSearchService;
const version = 'v1/';

const expectedData: ExcludeSearch = { id: '1', name: 'name' } as ExcludeSearch;
const expectedList: GenericApiResponse<ExcludeSearch> = {
  content: [expectedData],
  page: null as unknown as Page,
};

const expectedEntityTypesData: EntityType = { id: '1', name: 'name', labe: 'label', attributes: [], is_master: false } as EntityType;
const expectedEntityTypeList: EntityType[] = [expectedEntityTypesData];

describe('ExcludeSearchService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it('ExcludeSearchService.list calls to get api method', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new ExcludeSearchService(httpClientSpy as any, version, 'hidden');
    myService.list(null as any).subscribe((response) => {});
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
