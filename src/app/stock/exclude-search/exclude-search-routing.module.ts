import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { ExcludeSearchDetailContainerComponent } from './containers/exclude-search-detail-container.component';
import { ExcludeSearchListContainerComponent } from './containers/exclude-search-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: ExcludeSearchListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
      breadcrumb: [
        {
          label: 'EXCLUDE_SEARCH.LIST_TITLE',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'new',
        component: ExcludeSearchDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          title: 'new',
          header_title: 'MENU_LEFT.BUSINESS_RULES',
        },
      },
      {
        path: 'advanced',
        component: ExcludeSearchDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          title: 'new',
          header_title: 'MENU_LEFT.BUSINESS_RULES',
        },
      },
      {
        path: 'view/:id',
        component: ExcludeSearchDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          title: 'view',
          header_title: 'MENU_LEFT.BUSINESS_RULES',
        },
      },
      {
        path: 'view/:id/advanced',
        component: ExcludeSearchDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          title: 'view',
          header_title: 'MENU_LEFT.BUSINESS_RULES',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExcludeSearchRoutingModule {}
