import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@core/guards/can-deactivate-guard';
import { LimitSaleDetailContainerComponent } from './containers/limit-sale-detail-container.component';
import { LimitSaleListContainerComponent } from './containers/limit-sale-list-container.component';

const routes: Routes = [
  {
    path: '',
    component: LimitSaleListContainerComponent,
    data: {
      header_title: 'MENU_LEFT.BUSINESS_RULES',
      breadcrumb: [
        {
          label: 'LIMIT_SALE.LIST_TITLE',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'list',
        component: LimitSaleListContainerComponent,
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          breadcrumb: [
            {
              label: 'LIMIT_SALE.LIST_TITLE',
              url: '',
            },
          ],
        },
      },
      {
        path: 'new',
        component: LimitSaleDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'new',
          breadcrumb: [
            {
              label: 'LIMIT_SALE.BREADCRUMB_LIST_TITLE',
              url: '/stock/limit-sale',
            },
            {
              label: 'LIMIT_SALE.BREADCRUMB_ADD_NEW',
              url: '',
            },
          ],
        },
      },
      {
        path: 'view/:id',
        component: LimitSaleDetailContainerComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          header_title: 'MENU_LEFT.BUSINESS_RULES',
          title: 'view',
          breadcrumb: [
            {
              label: 'LIMIT_SALE.BREADCRUMB_LIST_TITLE',
              url: '/stock/limit-sale',
            },
            {
              label: 'LIMIT_SALE.LIMIT_SALE',
              url: '',
            },
          ],
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LimitSaleRoutingModule {}
