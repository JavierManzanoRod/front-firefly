import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { ToastModule } from '../../../modules/toast/toast.module';
import { LimitSaleService } from '../services/limit-sale.service';
import { LimitSaleDetailContainerComponent } from './limit-sale-detail-container.component';

// starts global mocks

class TranslateServiceStub {
  public setDefaultLang() {}

  public get(key: any): any {
    of(key);
  }
}

class ToastrServiceStub {
  public success() {}
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Component({
  selector: 'ff-page-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockHeaderComponent {
  @Input() pageTitle: any;
  @Input() breadcrumbTitle: any;
  @Input() breadcrumbLink: any;
}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };

  public get errorMessage() {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }

  public hide() {
    return true;
  }
}

@Component({
  selector: 'ff-limit-sale-detail',
  template: '<p>Mock Product Editor Component</p>',
})
class MockDetailComponent {
  @Input() loading!: boolean;
  @Input() item: any;
  @Input() isFromDestination!: boolean;
}

let apiSpy: {
  list: jasmine.Spy;
  detail: jasmine.Spy;
  post: jasmine.Spy;
  update: jasmine.Spy;
  delete: jasmine.Spy;
  entityTypes: jasmine.Spy;
};

describe('LimitSaleDetailContainerComponent', () => {
  let router: Router;

  beforeEach(
    waitForAsync(() => {
      apiSpy = jasmine.createSpyObj('LimitSaleService', ['list', 'detail', 'post', 'update', 'delete', 'entityTypes']);
      apiSpy.list.and.returnValue(of([{ a: 1 }]));
      apiSpy.entityTypes.and.returnValue(of([{ a: 1 }]));

      TestBed.configureTestingModule({
        declarations: [LimitSaleDetailContainerComponent, MockDetailComponent, TranslatePipeMock, MockHeaderComponent],
        imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule, ToastModule],
        providers: [
          { provide: TranslateService, useClass: TranslateServiceStub },
          { provide: ToastrService, useClass: ToastrServiceStub },
          { provide: BsModalService, useClass: ModalServiceMock },
          { provide: LimitSaleService, useValue: apiSpy },
          {
            provide: CrudOperationsService,
            useFactory: () => ({
              updateOrSaveModal: () => of({}),
            }),
          },
        ],
      }).compileComponents();

      router = TestBed.inject(Router);
    })
  );

  it('should create the component', () => {
    const fixture = TestBed.createComponent(LimitSaleDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should has loading setted to true when is created', () => {
    const fixture = TestBed.createComponent(LimitSaleDetailContainerComponent);
    const component = fixture.debugElement.componentInstance;
    const loading = component?.loading;
    console.log('loading is', loading);
    expect(loading).toBeTruthy();
  });
});
