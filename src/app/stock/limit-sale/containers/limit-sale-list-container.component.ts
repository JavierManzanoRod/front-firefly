import { Component, OnInit } from '@angular/core';
import { BaseListContainerComponent } from '@core/base-containers/base-list-container.component';
import { SortApiType } from '@core/models/sort.model';
import { CrudOperationsService } from 'src/app/core/services/crud-operations.service';
import { GenericListConfig, TypeSearch } from 'src/app/modules/generic-list/models/generic-list.model';
import { LimitSale } from '../models/limit-sale.model';
import { LimitSaleService } from '../services/limit-sale.service';

@Component({
  selector: 'ff-limit-sale-list-container',
  template: `<router-outlet (activate)="activate()" (deactivate)="deactivate()"></router-outlet>
    <div *ngIf="!hide">
      <ff-page-header [pageTitle]="'LIMIT_SALE.HEADER_LIST' | translate">
        <a [routerLink]="['/stock/limit-sale/new']" class="btn btn-primary">
          {{ 'LIMIT_SALE.CREATE_RULE' | translate }}
        </a>
      </ff-page-header>
      <ngx-simplebar class="page-container">
        <div class="page-scroll-wrapper">
          <ngx-simplebar class="page-scroll">
            <div class="page-container-padding">
              <ff-generic-list
                [list]="list$ | async"
                [loading]="loading"
                [page]="page"
                [valueSearch]="searchName"
                [currentData]="currentData"
                (delete)="delete($event)"
                (pagination)="handlePagination($event)"
                [checkDateActive]="checkDateActive"
                (search)="search($event)"
                [type]="type"
                [placeHolder]="'LIMIT_SALE.PLACEHOLDER_SEARCH' | translate"
                [title]="''"
                [arrayKeys]="arrayKeys"
                (sortEvent)="sort($event)"
                [route]="'/stock/limit-sale/view/'"
              >
              </ff-generic-list>
            </div>
          </ngx-simplebar>
        </div>
      </ngx-simplebar>
    </div> `,
})
export class LimitSaleListContainerComponent extends BaseListContainerComponent<LimitSale> implements OnInit {
  hide = false;
  type = TypeSearch.simple;
  arrayKeys: GenericListConfig[] = [
    {
      key: 'name',
      headerName: 'LIMIT_SALE.NAME',
      showOrder: true,
    },
    {
      key: 'start_date',
      headerName: 'LIMIT_SALE.START_DATE',
      textCenter: true,
      canFormatDate: true,
      formatDate: 'dd-MM-yyyy H:mm',
    },
    {
      key: 'end_date',
      headerName: 'LIMIT_SALE.END_DATE',
      textCenter: true,
      canFormatDate: true,
      formatDate: 'dd-MM-yyyy H:mm',
    },
    {
      key: 'active',
      headerName: 'LIMIT_SALE.ACTIVE',
      textCenter: true,
      canActiveClass: true,
    },
  ];

  constructor(public apiService: LimitSaleService, public crudSrv: CrudOperationsService) {
    super(crudSrv, apiService);
  }

  ngOnInit() {
    this.sort({ name: 'name', type: SortApiType.asc });
  }

  activate() {
    this.hide = true;
  }

  deactivate() {
    this.hide = false;
    this.search({
      name: this.searchName || '',
    });
  }
}
