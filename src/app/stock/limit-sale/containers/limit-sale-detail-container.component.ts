import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseDetailContainerComponent } from '@core/base-containers/base-detail-container.component';
import { EntityType } from '@core/models/entity-type.model';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, delay, tap } from 'rxjs/operators';
import { MenuLeftService } from 'src/app/components/menu-left/services/menu-left.service';
import { FF_LIMIT_SALE } from 'src/app/configuration/tokens/admin-group-type.token';
import { CrudOperationsService, groupType } from 'src/app/core/services/crud-operations.service';
import { UICommonModalConfig } from 'src/app/modules/common-modal/components/modal-warning/modal-warning.model';
import { ConditionsBasicAndAdvanceComponent } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.component';
import { AdminGroupTypeService } from 'src/app/rule-engine/admin-group-type/services/admin-group-type.service';
import { ToastService } from '../../../modules/toast/toast.service';
import { LimitSale } from '../models/limit-sale.model';
import { LimitSaleService } from '../services/limit-sale.service';

@Component({
  selector: 'ff-limit-sale-detail-container',
  template: `<ff-page-header
      [pageTitle]="id ? ('LIMIT_SALE.HEADER_DETAIL' | translate) : ('LIMIT_SALE.HEADER_NEW' | translate)"
      breadcrumbLink="/stock/limit-sale"
      [breadcrumbTitle]="'COMMON.RETURN_TO_LIST' | translate"
    >
      <button class="btn btn-dark mr-2" (click)="clone(detail)" data-cy="clone-rule" *ngIf="id">
        {{ 'COMMON.CLONE' | translate }}
      </button>
      <button class="btn btn-dark mr-2" (click)="delete(detail)" data-cy="remove-rule" *ngIf="id">
        {{ 'COMMON.DELETE' | translate }}
      </button>
    </ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-conditions-basic-and-advance
        #detailElement
        (formSubmit)="handleSubmitForm($event)"
        (cancel)="cancel()"
        [loading]="loading"
        [item]="detail"
        [idAdminGroup]="idLimitSale"
        [entityTypes]="entityTypes"
      ></ff-conditions-basic-and-advance>
    </ngx-simplebar> `,
})
export class LimitSaleDetailContainerComponent extends BaseDetailContainerComponent<LimitSale> implements OnInit {
  @ViewChild('detailElement') detailElement!: ConditionsBasicAndAdvanceComponent;
  @Output() changed = new EventEmitter();

  entityTypes!: EntityType[];
  urlListado = '/stock/limit-sale';

  constructor(
    public activeRoute: ActivatedRoute,
    public apiService: LimitSaleService,
    public adminGroupTypeSrv: AdminGroupTypeService,
    public router: Router,
    public crudSrv: CrudOperationsService,
    public menuLeft: MenuLeftService,
    public toastService: ToastService,
    @Inject(FF_LIMIT_SALE) public idLimitSale: string
  ) {
    super(crudSrv, menuLeft, activeRoute, router, apiService, toastService);
    this.id = this.activeRoute.snapshot.paramMap.get('id') || '';
  }

  ngOnInit() {
    let detail$: Observable<LimitSale>;
    if (this.id) {
      detail$ = this.apiService.detail(this.id);
    } else {
      detail$ = of({} as LimitSale).pipe(delay(250));
    }

    forkJoin({
      detail: detail$,
      entityTypes: this.adminGroupTypeSrv.entityTypes(this.idLimitSale),
    }).subscribe(
      ({ detail, entityTypes }) => {
        this.detail = detail;
        this.entityTypes = entityTypes;
        this.loading = false;
      },
      () => {
        this.router.navigateByUrl(this.urlListado).then(() => {
          this.toastService.error('COMMON_ERRORS.NOT_FOUND_DESCRIPTION', 'COMMON_ERRORS.NOT_FOUND_TITLE');
        });
      }
    );
  }
  cancel() {
    this.router.navigate(['.'], { relativeTo: this.activeRoute.parent });
  }

  handleSubmitForm(itemToSave: LimitSale) {
    this.crudSrv
      .updateOrSaveModal(
        {
          ...this,
          methodToApply: itemToSave.id ? 'PUT' : 'POST',
        },
        itemToSave,
        groupType.RULE
      )
      .pipe(
        tap((response) => {
          this.detailElement.haveChanges = false;
          this.changed.emit(response.data);
          if (!itemToSave.id) {
            this.router.navigate([`${this.urlListado}/view/${response.data?.id}`]);
          }
        }),
        catchError((e: HttpErrorResponse) => {
          if (e.status === 409) {
            this.detailElement.form.controls.name.setErrors({ 409: true });
          }
          return of(null);
        })
      )
      .subscribe();
  }

  canDeactivate(): UICommonModalConfig | false {
    if (this.detailElement.isDirty) {
      if (!this.detail || Object.keys(this.detail || {}).length === 0) {
        return {};
      }
      return {
        bodyMessage: 'COMMON_MODALS.UNSAVED_ATTS_ID',
        name: this.detailElement?.item.name,
      };
    }
    return false;
  }

  clone(detail: LimitSale | undefined) {
    if (detail) {
      detail = this.detailElement.getDataToSend() as LimitSale;
    }
    if (this.detailElement.isDirty) {
      this.toastService.error('COMMON_ERRORS.CANNOT_CLONE');
      return;
    }
    if (this.detailElement?.form?.valid) {
      super.clone(detail, this.urlListado + '/view');
    } else {
      this.crudSrv.toast.warning('COMMON_ERRORS.CLONE_FORM_ERROR');
    }
  }
}
