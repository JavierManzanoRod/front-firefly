import { GenericApiResponse } from '@model/base-api.model';
import { Conditions2, ConditionsDTO } from 'src/app/modules/conditions2/models/conditions2.model';

export interface LimitSale {
  id?: string;
  name: string;
  start_date?: string | Date;
  start_time?: string | Date;
  end_date?: string | Date;
  end_time?: string | Date;
  active: boolean;
  is_basic: boolean;
  node?: Conditions2;
  admin_group_type_id?: string;
}

export interface LimitSaleDTO {
  identifier?: string;
  name: string;
  start_date?: string | Date;
  start_time?: string | Date;
  is_basic: boolean;
  end_date?: string | Date;
  end_time?: string | Date;
  is_active: boolean;
  node?: ConditionsDTO;
}

export type LimitSaleResponse = GenericApiResponse<LimitSale>;

export type LimitSaleResponseDTO = GenericApiResponse<LimitSaleDTO>;
