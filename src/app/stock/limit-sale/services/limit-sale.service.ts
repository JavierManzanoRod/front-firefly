import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { FF_API_PATH_VERSION_LIMIT_SALE } from 'src/app/configuration/tokens/api-versions.token';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { AdminGroupTypeService } from 'src/app/shared/services/apis/admin-group-type.service';
import { FF_LIMIT_SALE } from './../../../configuration/tokens/admin-group-type.token';

@Injectable({
  providedIn: 'root',
})
export class LimitSaleService extends AdminGroupTypeService {
  constructor(
    protected http: HttpClient,
    @Inject(FF_API_PATH_VERSION_LIMIT_SALE) protected version: string,
    @Inject(FF_LIMIT_SALE) protected idLimitSale: string
  ) {
    super(http, version, idLimitSale);
  }

  protected parseResult(_data: AdminGroup): string {
    return '{"is_limit_sale":true}';
  }
}
