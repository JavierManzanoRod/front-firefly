import { HttpClient } from '@angular/common/http';
import { EntityType } from '@core/models/entity-type.model';
import { GenericApiRequest, GenericApiResponse, Page } from '@model/base-api.model';
import { of } from 'rxjs';
import { LimitSale } from '../models/limit-sale.model';
import { LimitSaleService } from './limit-sale.service';

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy };

let myService: LimitSaleService;

const version = 'v1/';

const expectedData: LimitSale = { id: '1', name: 'name', active: true, node: {} } as LimitSale;
const expectedList: GenericApiResponse<LimitSale> = {
  content: [expectedData],
  page: null as unknown as Page,
};

const expectedEntityTypesData: EntityType = { id: '1', name: 'name', labe: 'label', attributes: [], is_master: false } as EntityType;
const expectedEntityTypeList: EntityType[] = [expectedEntityTypesData];

describe('RegionService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  });

  it('LimitSaleService.list calls to get api method', () => {
    httpClientSpy.get.and.returnValue(of(expectedList));
    myService = new LimitSaleService(httpClientSpy as any, 'v1', 'limit');
    myService.list(null as unknown as GenericApiRequest).subscribe(() => {});
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
