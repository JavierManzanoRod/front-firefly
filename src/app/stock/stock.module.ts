import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ControlPanelSharedModule } from '../control-panel/shared/control-panel-shared.module';
import { StockRoutingModule } from './stock-routing.module';

@NgModule({
  imports: [CommonModule, FormsModule, StockRoutingModule, ControlPanelSharedModule],
  declarations: [],
  providers: [],
  exports: [],
})
export class StockModule {}
