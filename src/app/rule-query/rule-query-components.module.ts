import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AccordionListModule } from '../modules/accordion-list/accordion-list.module';
import { ProductDataComponent } from './components/product-data/product-data.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [CommonModule, AccordionModule.forRoot(), TranslateModule, ReactiveFormsModule, AccordionListModule, SharedModule],
  declarations: [ProductDataComponent],
  providers: [],
  exports: [ProductDataComponent, AccordionListModule],
})
export class RuleQueryComponentsModule {}
