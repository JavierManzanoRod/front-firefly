export type TypeCode = 'sale_reference' | 'item_code' | 'ean';

interface DescriptionIdentifierNameParentProductType {
  description: [
    {
      locale: string;
      value: string;
    }
  ];
  identifier: string;
  name: [
    {
      locale: string;
      value: string;
    }
  ];
  parent_product_type?: {
    description: [
      {
        locale: string;
        value: string;
      }
    ];
    identifier: string;
    name: [
      {
        locale: string;
        value: string;
      }
    ];
  };
}
export interface ProductRule {
  site: string;
  subsite: string;
  seller: string;
  gtin: string;
  unique_code: string;
  product_id: string;
  sales_reference: string;
  attributes: {
    seller: {
      identifier: string;
    };
    identifier: string;
    site: {
      identifier: string;
      parent: {
        identifier: string;
      };
    };
    item_offered: {
      color: {
        color_code: string;
        color_description: {
          locale: string;
          value: string;
        }[];
        glossary: {
          identifier: string;
          media_objects: {
            media_type: string;
            url: string;
          }[];
          term: {
            locale: string;
            value: string;
          }[];
        };
        identifier: string;
      };
      gtin: string;
      identifier: string;
      is_variant_of: {
        brand: {
          identifier: string;
          name: [
            {
              locale: string;
              value: string;
            }
          ];
        };
        description: {
          locale: string;
          value: string;
        }[];
        digital_store: string;
        identifier: string;
        is_digital: boolean;
        product_id: string;
        type: DescriptionIdentifierNameParentProductType;
        types_all: DescriptionIdentifierNameParentProductType[];
        varies_by: string[];
      };
      sizes: {
        main_size: {
          identifier: string;
          name: {
            locale: string;
            value: string;
          }[];
          size_type: {
            identifier: string;
            name: {
              locale: string;
              value: string;
            }[];
          };
        };
        name: string;
      }[];
      unique_code: string;
    };
    sales_reference: string;
    delivery_type_badges: any;
    company: string;
    section: string;
    mixing_information: any;
  };
}

type MultiLocaleValue = {
  locale: string;
  value: string;
}[];

export interface ProductRuleFlatten {
  site: string;
  subsite: string;
  seller: string;
  gtin: string;
  unique_code: string;
  product_id: string;
  sales_reference: string;
  attributes: any;
}

export interface ProductRuleView extends ProductRuleFlatten {
  name?: string;
  description?: string;
}
