import { Inject, InjectionToken, Optional, Provider } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { shareReplay, switchMap } from 'rxjs/operators';
import { RuleWithIdentifier } from '../evaluate-rules/models/evaluate-rules-request.model';
import { EvaluateRulesSearchForm } from '../evaluate-rules/models/evaluate-rules-search.model';
import { RuleSearchService } from '../evaluate-rules/services/rule-search.service';
import { EVALUATE_RULES_FILTER } from './search-filters.tokens';

export const RULE_SEARCH_TOKEN = new InjectionToken<Observable<RuleWithIdentifier>>('A stream that provides a RuleWithIdentifier');

export const RULE_SEARCH_PROVIDER: Provider[] = [
  {
    provide: RULE_SEARCH_TOKEN,
    deps: [Router, RuleSearchService, [new Optional(), new Inject(EVALUATE_RULES_FILTER)]],
    useFactory: ruleFactory,
  },
];

export function ruleFactory(
  router: Router,
  ruleSearch: RuleSearchService,
  filter: Observable<EvaluateRulesSearchForm>
): Observable<RuleWithIdentifier> {
  return filter.pipe(
    switchMap((ruleFilter) => {
      const navigation = router.getCurrentNavigation();
      const hasRule = navigation?.extras?.state?.rule;
      if (hasRule) {
        const rule: RuleWithIdentifier = hasRule;
        return of(rule);
      }
      if (ruleFilter) {
        const { ruleId, ruleType } = ruleFilter;
        return ruleSearch.findById(ruleId, ruleType);
      }
      return of(null as unknown as RuleWithIdentifier);
    }),
    shareReplay(1)
  );
}
