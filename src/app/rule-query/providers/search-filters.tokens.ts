import { InjectionToken } from '@angular/core';
import { EvaluateRulesSearchForm } from '../evaluate-rules/models/evaluate-rules-search.model';
import { SatisfiedRuleSearchForm } from '../satisfied-rules/models/satisfied-rules.model';

export const SATISFIED_RULES_FILTER = new InjectionToken<SatisfiedRuleSearchForm>('stream of satisfied rules form');

export const EVALUATE_RULES_FILTER = new InjectionToken<EvaluateRulesSearchForm>('stream of id from route param');
