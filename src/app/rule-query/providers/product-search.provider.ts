import { Inject, InjectionToken, Optional, Provider } from '@angular/core';
import { Router } from '@angular/router';
import { combineLatest, isObservable, Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { EvaluateRulesSearchForm } from '../evaluate-rules/models/evaluate-rules-search.model';
import { ProductRuleView } from '../models/product.model';
import { SatisfiedRuleSearchForm } from '../satisfied-rules/models/satisfied-rules.model';
import { ProductSearcherService } from '../services/product-searcher.service';
import { EVALUATE_RULES_FILTER, SATISFIED_RULES_FILTER } from './search-filters.tokens';

export const PRODUCT_SEARCH_TOKEN = new InjectionToken<Observable<ProductRuleView[]>>('A stream with products search result');

export const PRODUCT_SEARCH_PROVIDER: Provider[] = [
  {
    provide: PRODUCT_SEARCH_TOKEN,
    deps: [
      Router,
      ProductSearcherService,
      [new Optional(), new Inject(EVALUATE_RULES_FILTER)],
      [new Optional(), new Inject(SATISFIED_RULES_FILTER)],
    ],
    useFactory: productFactory,
  },
];

function productFactory(
  router: Router,
  productSearch: ProductSearcherService,
  erFilter: Observable<EvaluateRulesSearchForm>,
  srFilter: Observable<SatisfiedRuleSearchForm>
): Observable<ProductRuleView[] | undefined> {
  return combineLatest([toObs(erFilter), toObs(srFilter)]).pipe(
    switchMap(([evaluateFilter, satisfiedFilter]: [EvaluateRulesSearchForm, SatisfiedRuleSearchForm]) => {
      const navigation = router.getCurrentNavigation();
      const hasStateProducts = navigation?.extras?.state?.products;
      if (hasStateProducts) {
        const products: ProductRuleView[] = hasStateProducts;
        return of(products);
      }
      if (evaluateFilter || satisfiedFilter) {
        const { productId, productType } = evaluateFilter || satisfiedFilter;
        return productSearch.listPeeked({ [productType]: productId });
      }
      return of([]);
    })
  );
}

function toObs(param: Observable<any> | any) {
  return isObservable(param) ? param : of(param);
}
