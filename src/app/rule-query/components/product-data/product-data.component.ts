import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ProductRuleView } from '../../models/product.model';

type ProductRuleViewData = Pick<ProductRuleView, 'name' | 'description' | 'sales_reference' | 'product_id' | 'gtin'>;

@Component({
  selector: 'ff-product-data',
  templateUrl: './product-data.component.html',
  styleUrls: ['./product-data.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductDataComponent implements OnInit {
  @Input() set product(product: ProductRuleViewData) {
    if (product) {
      this._product = this.removeEmpty(product);
    }
  }
  get product(): ProductRuleViewData {
    return this._product;
  }
  _product!: ProductRuleViewData;

  isCollapsedConfig: any = {};

  constructor() {}

  ngOnInit(): void {}

  private removeEmpty({ name, description, sales_reference, product_id, gtin }: ProductRuleViewData): ProductRuleViewData {
    return {
      name: this.nullOrUnd(name),
      description: this.nullOrUnd(description),
      sales_reference: this.nullOrUnd(sales_reference),
      product_id: this.nullOrUnd(product_id),
      gtin: this.nullOrUnd(gtin),
    };
  }

  private nullOrUnd(val: string | undefined) {
    return val === 'null' || val === 'undefined' || !val ? '' : val;
  }
}
