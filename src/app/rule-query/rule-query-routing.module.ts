import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'evaluate-rules',
    loadChildren: () => import('./evaluate-rules/evaluate-rules.module').then((m) => m.EvaluateRulesModule),
  },
  {
    path: 'satisfied-rules',
    loadChildren: () => import('./satisfied-rules/satisfied-rules.module').then((m) => m.SatisfiedRulesModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RuleQueryRoutingModule {}
