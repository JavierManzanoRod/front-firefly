import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { queryParamFactory } from '../../providers/activated-route.factories';
import { RULE_SEARCH_PROVIDER } from '../../providers/rule-search.provider';
import { EVALUATE_RULES_FILTER } from '../../providers/search-filters.tokens';
import { EvaluateRulesSearchComponent } from '../components/evaluate-rules-search/evaluate-rules-search.component';
import { RuleWithIdentifier } from '../models/evaluate-rules-request.model';
import { EvaluateRulesSearchForm } from '../models/evaluate-rules-search.model';
@Component({
  selector: 'ff-evaluate-rules-search-container',
  template: `<ff-page-header [pageTitle]="'RULE_QUERY.EVALUATE_RULES.HEADER_TITLE' | translate"></ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-evaluate-rules-search [messageError]="messageToError" (formSubmit)="handleSubmitForm($event)"></ff-evaluate-rules-search>
      <router-outlet></router-outlet>
    </ngx-simplebar> `,
  providers: [
    {
      provide: EVALUATE_RULES_FILTER,
      useFactory: queryParamFactory<EvaluateRulesSearchForm>(new EvaluateRulesSearchForm()),
      deps: [ActivatedRoute],
    },
    RULE_SEARCH_PROVIDER,
  ],
})
export class EvaluateRulesSearchContainerComponent {
  @ViewChild(EvaluateRulesSearchComponent) search!: EvaluateRulesSearchComponent;
  messageToError = '';
  constructor(public router: Router, private route: ActivatedRoute) {}

  handleSubmitForm([search, rule]: [EvaluateRulesSearchForm, RuleWithIdentifier]) {
    this.router.navigate(['result'], {
      relativeTo: this.route,
      queryParams: search,
      state: {
        rule,
      },
    });
  }
}
