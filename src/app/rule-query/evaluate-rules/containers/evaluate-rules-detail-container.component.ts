import { Component, Inject } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { notifyRequestStatus } from '../../../shared/utils/operators/notify-operator';
import { ProductRuleView } from '../../models/product.model';
import { PRODUCT_SEARCH_PROVIDER, PRODUCT_SEARCH_TOKEN } from '../../providers/product-search.provider';
import { RULE_SEARCH_TOKEN } from '../../providers/rule-search.provider';
import { RuleWithIdentifier } from '../models/evaluate-rules-request.model';

@Component({
  selector: 'ff-evaluate-rules-detail-container',
  template: `
    <div *ngIf="data$ | async as data; else loading" class="detail">
      <div *ngIf="data[0]?.length">
        <ff-product-data [product]="getMainProduct(data[0])"></ff-product-data>
      </div>
      <ff-evaluate-rules-list [data]="[data[0], data[1]]"></ff-evaluate-rules-list>
    </div>
    <ng-template #loading>
      <div class="text-center py-5">
        <div class="loader"></div>
      </div>
    </ng-template>
  `,
  styleUrls: ['./evaluate-rules-detail-container.component.scss'],
  providers: [PRODUCT_SEARCH_PROVIDER],
})
export class EvaluateRulesDetailContainerComponent {
  public data$ = combineLatest([this.products$.pipe(notifyRequestStatus(this.toastService, [] as ProductRuleView[])), this.rule$]);
  constructor(
    private toastService: ToastService,
    @Inject(PRODUCT_SEARCH_TOKEN) public products$: Observable<ProductRuleView[]>,
    @Inject(RULE_SEARCH_TOKEN) public rule$: Observable<RuleWithIdentifier>
  ) {}

  getMainProduct(products: ProductRuleView[]) {
    return products?.find((p) => Object.keys(p.attributes).length > 0);
  }
}
