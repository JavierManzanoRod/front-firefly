import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EvaluateRulesDetailContainerComponent } from './containers/evaluate-rules-detail-container.component';
import { EvaluateRulesSearchContainerComponent } from './containers/evaluate-rules-search-container';
const routes: Routes = [
  {
    path: '',
    component: EvaluateRulesSearchContainerComponent,
    data: {
      header_title: 'MENU_LEFT.QUERIES',
      breadcrumb: [
        {
          label: 'DEPARTMENTS.LIST_TITLE',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'result',
        component: EvaluateRulesDetailContainerComponent,
        data: {
          header_title: 'MENU_LEFT.QUERIES',
          breadcrumb: [
            {
              label: 'DEPARTMENTS.LIST_TITLE',
              url: '',
            },
          ],
        },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EvaluateRulesRoutingModule {}
