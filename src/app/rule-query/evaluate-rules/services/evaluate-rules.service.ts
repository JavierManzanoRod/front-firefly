import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, UpdateResponse } from '@core/base/api.service';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_EVALUATE_RULES } from 'src/app/configuration/tokens/api-versions.token';
import { EvaluateRulesRequest } from '../models/evaluate-rules-request.model';
import { EvaluateRulesResponse } from '../models/evaluate-rules-response.model';

const urlBase = `${environment.API_URL_BACKOFFICE}`;
@Injectable({
  providedIn: 'root',
})
export class EvaluateRulesService extends ApiService<EvaluateRulesResponse> {
  endPoint = `products/backoffice-rules/${this.version}evaluate-rules`;

  get urlBase() {
    return urlBase + '/' + this.endPoint;
  }

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_EVALUATE_RULES) private version: string) {
    super();
  }

  evaluateRule(data: EvaluateRulesRequest): Observable<UpdateResponse<EvaluateRulesResponse[]>> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .request('post', this.urlBase, {
        body: data,
        headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            status: res.status,
            data: res.body,
          };
        })
      );
  }
}
