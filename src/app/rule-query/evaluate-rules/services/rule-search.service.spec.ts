import { fakeAsync } from '@angular/core/testing';
import { GenericApiResponse, MayHaveLabel } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { ContentGroupService } from 'src/app/catalog/content-group/services/content-group.service';
import { ExpertAdminService } from 'src/app/control-panel/expert-admin/services/expert-admin.service';
import { FluorinatedGasService } from 'src/app/control-panel/fluorinated-gas/services/fluorinated-gas.service';
import { LoyaltyService } from 'src/app/control-panel/loyalty/services/loyalty.service';
import { SizeGuideService } from 'src/app/control-panel/size-guide/services/size-guide.service';
import { SpecialProductService } from 'src/app/control-panel/special-product/services/special-product.service';
import { AdminGroupService } from 'src/app/rule-engine/admin-group/services/admin-group.service';
import { PriceInheritanceService } from 'src/app/rule-engine/price-inheritance/services/price-inheritance.service';
import { ExcludeSearchService } from 'src/app/stock/exclude-search/services/exclude-search.service';
import { LimitSaleService } from 'src/app/stock/limit-sale/services/limit-sale.service';
import { RuleType } from '../models/rule-type.enum';
import { BadgeService } from './../../../control-panel/badge/services/badge.service';
import { RuleSearchService } from './rule-search.service';

let productGroupingServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let sizeGuideServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let excludeSearchServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let contentGroupServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let specialProductServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let fluorinatedGasServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let expertAdminServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let limitSaleServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let adminGroupServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let priceInheritanceServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let loyaltyServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let badgeServiceSpy: { list: jasmine.Spy; detail: jasmine.Spy; update: jasmine.Spy; post: jasmine.Spy };
let translateSpy: { instant: jasmine.Spy };

let myService: RuleSearchService;
const expectedData: MayHaveLabel = { label: 'label', name: 'name', icon: 'icon' } as MayHaveLabel;
let expectedList: GenericApiResponse<MayHaveLabel>;
let types = {
  [RuleType.price_inheritance]: true,
  [RuleType.product_grouping]: true,
  [RuleType.size_guide]: true,
  [RuleType.exclude_search]: true,
  [RuleType.content_group]: true,
  [RuleType.special_product]: true,
  [RuleType.fluorinated_gas]: true,
  [RuleType.expert_admin]: true,
  [RuleType.limit_sale]: true,
  [RuleType.admin_group]: true,
  [RuleType.loyalty]: true,
  [RuleType.badge]: true,
};
describe('RuleSearchService', () => {
  beforeAll(() => {
    (priceInheritanceServiceSpy = jasmine.createSpyObj('PriceInheritanceService', ['list', 'detail', 'update', 'post'])),
      (productGroupingServiceSpy = jasmine.createSpyObj('CategoryRuleService', ['list', 'detail', 'update', 'post'])),
      (sizeGuideServiceSpy = jasmine.createSpyObj('SizeGuideService', ['list', 'detail', 'update', 'post'])),
      (excludeSearchServiceSpy = jasmine.createSpyObj('ExcludeSearchService', ['list', 'detail', 'update', 'post'])),
      (contentGroupServiceSpy = jasmine.createSpyObj('AdminGroupTypeService', ['list', 'detail', 'update', 'post'])),
      (specialProductServiceSpy = jasmine.createSpyObj('SpecialProductService', ['list', 'detail', 'update', 'post'])),
      (fluorinatedGasServiceSpy = jasmine.createSpyObj('FluorinatedGasService', ['list', 'detail', 'update', 'post'])),
      (expertAdminServiceSpy = jasmine.createSpyObj('ExpertAdminService', ['list', 'detail', 'update', 'post'])),
      (limitSaleServiceSpy = jasmine.createSpyObj('LimitSaleService', ['list', 'detail', 'update', 'post'])),
      (adminGroupServiceSpy = jasmine.createSpyObj('AdminGroupService', ['list', 'detail', 'update', 'post'])),
      (loyaltyServiceSpy = jasmine.createSpyObj('LoyaltyService', ['list', 'detail', 'update', 'post'])),
      (badgeServiceSpy = jasmine.createSpyObj('badge', ['list', 'detail', 'update', 'post'])),
      (translateSpy = jasmine.createSpyObj('TranslateService', ['instant']));

    translateSpy.instant
      .withArgs('RULE_QUERY.PRODUCT.PRICE_INHERITANCE')
      .and.returnValue('"Herencia de precio mkp')
      .withArgs('RULE_QUERY.PRODUCT.PRODUCT_GROUPING')
      .and.returnValue('"Agrupaciones de producto')
      .withArgs('RULE_QUERY.PRODUCT.SIZE_GUIDE')
      .and.returnValue('"Guia de tallas')
      .withArgs('RULE_QUERY.PRODUCT.EXCLUDE_SEARCH')
      .and.returnValue('Ocultar en buscador')
      .withArgs('RULE_QUERY.PRODUCT.CONTENT_GROUP')
      .and.returnValue('Content Group')
      .withArgs('RULE_QUERY.PRODUCT.SPECIAL_PRODUCT')
      .and.returnValue('Productos especiales')
      .withArgs('RULE_QUERY.PRODUCT.FLUORINATED_GAS')
      .and.returnValue('Gases fluorados')
      .withArgs('RULE_QUERY.PRODUCT.EXPERT_ADMIN')
      .and.returnValue('Consulta de experto')
      .withArgs('RULE_QUERY.PRODUCT.LIMIT_SALES')
      .and.returnValue('Limitar venta')
      .withArgs('RULE_QUERY.PRODUCT.ADMIN_GROUP')
      .and.returnValue('Admin-groups')
      .withArgs('RULE_QUERY.PRODUCT.LOYALTY')
      .and.returnValue('Fidelización')
      .withArgs('RULE_QUERY.PRODUCT.BADGE')
      .and.returnValue('Badge');
  });
  beforeEach(() => {
    myService = new RuleSearchService(
      priceInheritanceServiceSpy as unknown as PriceInheritanceService,
      sizeGuideServiceSpy as unknown as SizeGuideService,
      excludeSearchServiceSpy as unknown as ExcludeSearchService,
      contentGroupServiceSpy as unknown as ContentGroupService,
      specialProductServiceSpy as unknown as SpecialProductService,
      fluorinatedGasServiceSpy as unknown as FluorinatedGasService,
      expertAdminServiceSpy as unknown as ExpertAdminService,
      limitSaleServiceSpy as unknown as LimitSaleService,
      adminGroupServiceSpy as unknown as AdminGroupService,
      badgeServiceSpy as unknown as BadgeService,
      loyaltyServiceSpy as unknown as LoyaltyService,
      translateSpy as unknown as TranslateService
    );
    types = {
      [RuleType.price_inheritance]: true,
      [RuleType.product_grouping]: true,
      [RuleType.size_guide]: true,
      [RuleType.exclude_search]: true,
      [RuleType.content_group]: true,
      [RuleType.special_product]: true,
      [RuleType.fluorinated_gas]: true,
      [RuleType.expert_admin]: true,
      [RuleType.limit_sale]: true,
      [RuleType.admin_group]: true,
      [RuleType.loyalty]: true,
      [RuleType.badge]: true,
    };
    expectedList = {
      content: [expectedData],
      page: {
        total_pages: 1,
        total_elements: 10,
        page_size: 10,
        page_number: 1,
      },
    };
  });

  it('should search paginate AdminGroup', fakeAsync(async () => {
    adminGroupServiceSpy.list.and.returnValue(of(expectedList));
    let type = { [RuleType.admin_group]: true };
    const expectedResponse = {
      content: [{ icon: 'icon', idCustom: 1, label: 'Admin-groups: name', name: 'Admin-groups: name', ruleType: 'ADMIN_GROUP' }],
      page: { page_number: 0, page_size: 10, total_elements: 10, total_pages: 1 },
    };
    myService.searchPaginate({}, type).subscribe((response) => {
      expect(response).toEqual(expectedResponse);
    });
  }));

  it('should search paginate LimitSale', fakeAsync(async () => {
    limitSaleServiceSpy.list.and.returnValue(of({ ...expectedList }));
    let type = { [RuleType.limit_sale]: true };
    const expectedResponse = {
      content: [{ icon: 'icon', idCustom: 1, label: 'Limitar venta: name', name: 'Limitar venta: name', ruleType: 'LIMIT_SALE' }],
      page: { page_number: 0, page_size: 10, total_elements: 10, total_pages: 1 },
    };
    myService.searchPaginate({}, type).subscribe((response) => {
      expect(response).toEqual(expectedResponse);
    });
  }));

  it('should search paginate ExcludeSearch', fakeAsync(async () => {
    excludeSearchServiceSpy.list.and.returnValue(of({ ...expectedList }));
    let type = { [RuleType.exclude_search]: true };
    const expectedResponse = {
      content: [{ icon: 'icon', idCustom: 1, label: 'Ocultar en buscador: name', name: 'Ocultar en buscador: name', ruleType: 'HIDDEN' }],
      page: { page_number: 0, page_size: 10, total_elements: 10, total_pages: 1 },
    };
    myService.searchPaginate({}, type).subscribe((response) => {
      expect(response).toEqual(expectedResponse);
    });
  }));

  it('should search in all services', fakeAsync(async () => {
    excludeSearchServiceSpy.list.and.returnValue(of({ ...expectedList }));
    limitSaleServiceSpy.list.and.returnValue(of({ ...expectedList }));
    adminGroupServiceSpy.list.and.returnValue(of({ ...expectedList }));
    let type = { [RuleType.exclude_search]: true, [RuleType.limit_sale]: true, [RuleType.admin_group]: true };

    const expectedResponse = {
      content: [
        { icon: 'icon', idCustom: 1, label: 'Ocultar en buscador: name', name: 'Ocultar en buscador: name', ruleType: 'HIDDEN' },
        { icon: 'icon', idCustom: 2, label: 'Limitar venta: name', name: 'Limitar venta: name', ruleType: 'LIMIT_SALE' },
        { icon: 'icon', idCustom: 3, label: 'Admin-groups: name', name: 'Admin-groups: name', ruleType: 'ADMIN_GROUP' },
      ],
      page: { page_number: 0, page_size: 10, total_elements: 30, total_pages: 3 },
    };
    myService.searchPaginate({}, type).subscribe((response) => {
      expect(response).toEqual(expectedResponse);
    });
  }));
});
