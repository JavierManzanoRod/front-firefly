import { Injectable } from '@angular/core';
import { ApiService, IApiService2 } from '@core/base/api.service';
import { GenericApiRequest, GenericApiResponse, MayHaveIdName, MayHaveLabel, Page } from '@model/base-api.model';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Observable, of } from 'rxjs';
import { finalize, map, switchMap, tap } from 'rxjs/operators';
import { ContentGroupService } from 'src/app/catalog/content-group/services/content-group.service';
import { ExpertAdminService } from 'src/app/control-panel/expert-admin/services/expert-admin.service';
import { FluorinatedGasService } from 'src/app/control-panel/fluorinated-gas/services/fluorinated-gas.service';
import { LoyaltyService } from 'src/app/control-panel/loyalty/services/loyalty.service';
import { SizeGuideService } from 'src/app/control-panel/size-guide/services/size-guide.service';
import { SpecialProductService } from 'src/app/control-panel/special-product/services/special-product.service';
import { AdminGroupService } from 'src/app/rule-engine/admin-group/services/admin-group.service';
import { PriceInheritanceService } from 'src/app/rule-engine/price-inheritance/services/price-inheritance.service';
import { ExcludeSearchService } from 'src/app/stock/exclude-search/services/exclude-search.service';
import { LimitSaleService } from 'src/app/stock/limit-sale/services/limit-sale.service';
import { RuleWithIdentifier } from '../models/evaluate-rules-request.model';
import { RuleType } from '../models/rule-type.enum';
import { BadgeService } from './../../../control-panel/badge/services/badge.service';

interface ResultPagination {
  items: MayHaveLabel[];
  total_elements: number;
  total_pages: number;
  product_grouping?: Page;
  size_guide?: Page;
  price_inheritance?: Page;
  exclude_search?: Page;
  content_group?: Page;
  special_product?: Page;
  fluorinated_gas?: Page;
  expert_admin?: Page;
  limit_sale?: Page;
  loyalty?: Page;
  badge?: Page;
  admin_group?: Page;
  page_number: number;
}
interface ServiceDesc {
  service: ApiService<any> | IApiService2;
  preffix: string;
  ruleType: RuleType;
}

@Injectable({
  providedIn: 'root',
})
export class RuleSearchService {
  results: { [key: string]: ResultPagination } = {};
  id = 0;
  private readonly PRODUCT_GROUPING = this.translate.instant('RULE_QUERY.PRODUCT.PRODUCT_GROUPING');
  private readonly SIZE_GUIDE = this.translate.instant('RULE_QUERY.PRODUCT.SIZE_GUIDE');
  private readonly EXCLUDE_SEARCH = this.translate.instant('RULE_QUERY.PRODUCT.EXCLUDE_SEARCH');
  private readonly CONTENT_GROUP = this.translate.instant('RULE_QUERY.PRODUCT.CONTENT_GROUP');
  private readonly SPECIAL_PRODUCT = this.translate.instant('RULE_QUERY.PRODUCT.SPECIAL_PRODUCT');
  private readonly FLUORINATED_GAS = this.translate.instant('RULE_QUERY.PRODUCT.FLUORINATED_GAS');
  private readonly PRICE_INHERITANCE = this.translate.instant('RULE_QUERY.PRODUCT.PRICE_INHERITANCE');
  private readonly EXPERT_ADMIN = this.translate.instant('RULE_QUERY.PRODUCT.EXPERT_ADMIN');
  private readonly LIMIT_SALES = this.translate.instant('RULE_QUERY.PRODUCT.LIMIT_SALES');
  private readonly ADMIN_GROUP = this.translate.instant('RULE_QUERY.PRODUCT.ADMIN_GROUP');
  private readonly LOYALTY = this.translate.instant('RULE_QUERY.PRODUCT.LOYALTY');
  private readonly BADGE = this.translate.instant('RULE_QUERY.PRODUCT.BADGE');
  private servicesData: Record<string, ServiceDesc> = {
    [RuleType.price_inheritance]: {
      service: this.priceInheritanceService,
      preffix: this.PRICE_INHERITANCE,
      ruleType: RuleType.price_inheritance,
    },
    [RuleType.size_guide]: {
      service: this.sizeGuideService,
      preffix: this.SIZE_GUIDE,
      ruleType: RuleType.size_guide,
    },
    [RuleType.exclude_search]: {
      service: this.excludeSearchService,
      preffix: this.EXCLUDE_SEARCH,
      ruleType: RuleType.exclude_search,
    },
    [RuleType.content_group]: {
      service: this.contentGroupService,
      preffix: this.CONTENT_GROUP,
      ruleType: RuleType.content_group,
    },
    [RuleType.special_product]: {
      service: this.specialProductService,
      preffix: this.SPECIAL_PRODUCT,
      ruleType: RuleType.special_product,
    },
    [RuleType.fluorinated_gas]: {
      service: this.fluorinatedGasService,
      preffix: this.FLUORINATED_GAS,
      ruleType: RuleType.fluorinated_gas,
    },
    [RuleType.expert_admin]: {
      service: this.expertAdminService,
      preffix: this.EXPERT_ADMIN,
      ruleType: RuleType.expert_admin,
    },
    [RuleType.limit_sale]: {
      service: this.limitSaleService,
      preffix: this.LIMIT_SALES,
      ruleType: RuleType.limit_sale,
    },
    [RuleType.admin_group]: {
      service: this.adminGroupService,
      preffix: this.ADMIN_GROUP,
      ruleType: RuleType.admin_group,
    },
    [RuleType.loyalty]: {
      service: this.loyaltiService,
      preffix: this.LOYALTY,
      ruleType: RuleType.loyalty,
    },
    [RuleType.badge]: {
      service: this.badgeService,
      preffix: this.BADGE,
      ruleType: RuleType.badge,
    },
  };
  constructor(
    private priceInheritanceService: PriceInheritanceService,
    private sizeGuideService: SizeGuideService,
    private excludeSearchService: ExcludeSearchService,
    private contentGroupService: ContentGroupService,
    private specialProductService: SpecialProductService,
    private fluorinatedGasService: FluorinatedGasService,
    private expertAdminService: ExpertAdminService,
    private limitSaleService: LimitSaleService,
    private adminGroupService: AdminGroupService,
    private badgeService: BadgeService,
    private loyaltiService: LoyaltyService,
    //private categoryRuleService: CategoryRuleService,
    private translate: TranslateService
  ) {}

  searchPaginate(x: GenericApiRequest, types: { [key: string]: boolean }): Observable<GenericApiResponse<MayHaveLabel>> {
    // Define cuando es la primera vez que se lanza esa busqueda
    let isNewSearch = false;
    let result_name = x.name ? x.name : '';
    Object.keys(types).forEach((val) => (result_name += val.toString()));
    if (!this.results[result_name]) {
      isNewSearch = true;
      this.results[result_name] = {
        items: [],
        total_pages: 0,
        total_elements: 0,
        page_number: 0,
      };
    }
    const results = this.results[result_name] as any;
    const page = x.page ? x.page : 0;
    const size = x.size ? x.size : 10;

    if (page < results.total_pages || isNewSearch) {
      const needItems = page * size + size;

      if (needItems > results.items.length) {
        let launchSearch = false;
        const toSearch: { [key: string]: any } = {};
        Object.entries(types).forEach(([key, val]) => {
          if (key || val) {
            if (isNewSearch || results[key].page_number < results[key].total_pages) {
              launchSearch = true;
              const serviceData = this.servicesData[key];
              this.createSearch(
                x,
                results,
                page,
                toSearch,
                serviceData.service,
                this.mapper.bind(this),
                serviceData.preffix,
                serviceData.ruleType
              );
            }
          }
        });
        let fetching = true;

        return of(launchSearch).pipe(
          switchMap((shouldSearch) => {
            return !shouldSearch
              ? of(null)
              : forkJoin(toSearch).pipe(
                  tap(() => (fetching = false)),
                  finalize(() => {
                    if (fetching) {
                      //destroy cache if failed
                      this.results[result_name] = undefined as unknown as ResultPagination;
                    }
                  })
                );
          }),
          tap((response: any) => {
            if (response) {
              Object.entries(response).forEach(([key, resp]) => {
                const value = resp as GenericApiResponse<any>;
                results.items = results.items.concat(value.content);
                if (value.content.length) {
                  value.page.page_number++;
                }
                results[key] = value.page;
                if (isNewSearch) {
                  results.total_pages += value.page.total_pages;
                  results.total_elements += value.page.total_elements;
                }
              });
            }
          }),
          map(() => {
            results.total_pages = Math.ceil(results.total_elements / size);

            const resp = {
              content: results.items.slice(page * size, page * size + size),
              page: {
                page_number: page,
                page_size: size,
                total_pages: results.total_pages,
                total_elements: results.total_elements,
              },
            };
            return resp;
          })
        );
      }
    }

    results.total_pages = Math.ceil(results.total_elements / size);

    const result = {
      content: results.items.slice(page * size, page * size + size),
      page: {
        page_number: page,
        page_size: size,
        total_pages: results.total_pages,
        total_elements: results.total_elements,
      },
    };

    return of(result);
  }

  public findById(ruleId: string, ruleType: string): Observable<RuleWithIdentifier> {
    const resp: Observable<RuleWithIdentifier> = this.servicesData[ruleType].service.detail(ruleId);
    return resp.pipe(tap((val) => (val.ruleType = ruleType as RuleType)));
  }

  private createSearch(
    x: unknown,
    results: { [key: string]: ResultPagination },
    page: number,
    toSearch: { [key: string]: any },
    service: ApiService<any> | IApiService2,
    mapper: (a: any, b: any, c: any) => any,
    preffix: string,
    ruleType: RuleType
  ) {
    const query = JSON.parse(JSON.stringify(x));

    // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
    query.page = results[ruleType] ? results[ruleType].page_number : page;
    toSearch[ruleType] = service.list(query).pipe(
      map((data) => {
        data.content = data.content.map((item) => mapper(item, preffix, ruleType));
        return data;
      })
    );
  }

  private mapper(item: MayHaveIdName, prefix: string, ruleType: RuleType) {
    this.id++;
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return {
      ...item,
      idCustom: this.id,
      name: prefix + ': ' + (item.name as string),
      label: prefix + ': ' + (item.name as string),
      ruleType,
    } as any;
  }
}
