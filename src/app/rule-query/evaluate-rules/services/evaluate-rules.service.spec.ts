import { HttpClient } from '@angular/common/http';
import { fakeAsync } from '@angular/core/testing';
import { GenericApiResponse, MayHaveLabel } from '@model/base-api.model';
import { of } from 'rxjs';
import { EvaluateRulesRequest } from '../models/evaluate-rules-request.model';
import { EvaluateRulesService } from './evaluate-rules.service';

let myService: EvaluateRulesService;
const expectedData: MayHaveLabel = { label: 'label', name: 'name', icon: 'icon' } as MayHaveLabel;
let expectedList: GenericApiResponse<MayHaveLabel>;

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy; request: jasmine.Spy };
// let serviceToTest: ProductTypeService;

describe('EvaluateRulesService', () => {
  beforeAll(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete', 'request']);
  });
  beforeEach(() => {
    myService = new EvaluateRulesService(httpClientSpy as unknown as HttpClient, '');
  });

  it('should post data', fakeAsync(async () => {
    httpClientSpy.request.and.returnValue(of({ status: 200, body: [] }));
    myService.evaluateRule({} as unknown as EvaluateRulesRequest).subscribe((val) => {
      expect(val).toEqual({ status: 200, data: [] });
    });
  }));
});
