import { ContentGroupRule } from 'src/app/catalog/content-group/model/content-group.models';
import { ExpertAdmin } from 'src/app/control-panel/expert-admin/models/expert-admin.model';
import { AdminGroupType } from 'src/app/rule-engine/admin-group-type/models/admin-group-type.model';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { CategoryRule } from 'src/app/rule-engine/category-rule/models/category-rule.model';
import { ExcludeSearch } from 'src/app/stock/exclude-search/models/exclude-search.model';
import { LimitSale } from 'src/app/stock/limit-sale/models/limit-sale.model';
import { RuleType } from './rule-type.enum';

export interface TypeCodesApiRequest {
  ean?: string;
  saleReference?: string;
  itemCode?: string;
}

export type RuleWithIdentifier = (
  | ExcludeSearch
  | AdminGroup
  | CategoryRule
  | LimitSale
  | AdminGroupType
  | ExpertAdmin
  | ContentGroupRule
) & {
  identifier?: string;
  ruleType?: RuleType;
};

export interface EvaluateRulesRequest {
  attributes?: any;
  rule: RuleWithIdentifier;
}

export type TypeCode = 'sale_reference' | 'item_code' | 'ean';
