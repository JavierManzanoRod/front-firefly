export enum RuleType {
  price_inheritance = 'PRICE_INHERITANCE',
  product_grouping = 'PRODUCT_GROUPING',
  size_guide = 'SIZE_GUIDE',
  exclude_search = 'HIDDEN',
  content_group = 'CONTENT_GROUP',
  special_product = 'SPECIAL_PRODUCT',
  fluorinated_gas = 'ECI_FLUOR_GASES',
  expert_admin = 'EXPERT_ADMIN',
  limit_sale = 'LIMIT_SALE',
  admin_group = 'ADMIN_GROUP',
  loyalty = 'LOYALTY',
  badge = 'BADGE',
}
