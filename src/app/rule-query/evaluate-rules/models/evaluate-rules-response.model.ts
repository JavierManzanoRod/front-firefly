export interface EvaluateRulesResponse {
  is_apply: boolean;
  resolved_rule: EvaluateRulesConditionSimple | EvaluateRulesConditionNode;
}

export interface EvaluateRulesConditionSimple {
  type: string;
  attribute: string;
  value?: string;
  values?: unknown[];
  entities_info?: { identifier: string; labels: string[]; name: string }[];
  value_product?: unknown | unknown[];
  value_products?: unknown | unknown[];
  has_result?: boolean;
  nodes?: EvaluateRulesConditionNode[];
  data_type: 'STRING' | 'DOUBLE' | 'BOOLEAN' | 'ENTITY' | 'EMBEDDED' | 'DATE_TIME';
}

export interface EvaluateRulesConditionNode {
  type: string;
  attribute?: string;
  has_result: boolean;
  value?: string;
  nodes: (EvaluateRulesConditionSimple | EvaluateRulesConditionNode)[];
}

export function isEvaluateRulesConditionSimple(
  x: EvaluateRulesConditionSimple | EvaluateRulesConditionNode
): x is EvaluateRulesConditionSimple {
  return x && 'attribute' in x;
}

export function isEvaluateRulesConditionNode(
  x: EvaluateRulesConditionSimple | EvaluateRulesConditionNode
): x is EvaluateRulesConditionNode {
  return x && 'nodes' in x;
}

// CONDITIONS

export type RuleCondition = RuleConditionSimple | RuleConditionNode;
export interface RuleConditionSimple {
  [key: string]: unknown;
  node_type: string;
  attribute: {
    [key: string]: any;
    identifier: string;
  };
  value: unknown;
}
export interface RuleConditionNode {
  type: string;
  nodes: (RuleConditionSimple | RuleConditionNode)[];
}

export function isRuleConditionSimple(x: RuleConditionSimple | RuleConditionNode): x is RuleConditionSimple {
  return 'attribute' in x;
}
