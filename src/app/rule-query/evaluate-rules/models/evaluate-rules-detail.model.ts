import { EvaluateRulesConditionNode, EvaluateRulesConditionSimple } from './evaluate-rules-response.model';

export interface EvaluateRulesDetail {
  variant: {
    productId: string;
    name: string;
    eciReference: string;
    uniqueCode: string;
  };
  apply: boolean;
  resolved_rule: EvaluateRulesConditionSimple | EvaluateRulesConditionNode;
}
