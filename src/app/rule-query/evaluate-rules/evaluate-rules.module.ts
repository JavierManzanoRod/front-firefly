import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { ConditionsBasicAndAdvanceModule } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.module';
import { Conditions2Module } from 'src/app/modules/conditions2/conditions2.module';
import { UiModalSelectModule } from 'src/app/modules/ui-modal-select/ui-modal-select.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { RuleQueryComponentsModule } from '../rule-query-components.module';
import { EllipsisPipe } from './components/evaluate-rules-info/ellipsis.pipe';
import { EvaluateRulesInfoRuleContainerComponent } from './components/evaluate-rules-info/evaluate-rules-info-rule-container/evaluate-rules-info-rule-container.component';
import { EvaluateRulesInfoRuleItemComponent } from './components/evaluate-rules-info/evaluate-rules-info-rule-item/evaluate-rules-info-rule-item.component';
import { EvaluateRulesInfoRuleComponent } from './components/evaluate-rules-info/evaluate-rules-info-rule/evaluate-rules-info-rule.component';
import { EvaluateRulesListComponent } from './components/evaluate-rules-list/evaluate-rules-list.component';
import { EvaluateRulesSearchComponent } from './components/evaluate-rules-search/evaluate-rules-search.component';
import { EvaluateRulesSelectRuleComponent } from './components/evaluate-rules-select-rule/evaluate-rules-select-rule.component';
import { EvaluateRulesDetailContainerComponent } from './containers/evaluate-rules-detail-container.component';
import { EvaluateRulesSearchContainerComponent } from './containers/evaluate-rules-search-container';
import { EvaluateRulesRoutingModule } from './evaluate-rules-routing.module';

@NgModule({
  declarations: [
    EvaluateRulesSearchContainerComponent,
    EvaluateRulesDetailContainerComponent,
    EvaluateRulesInfoRuleComponent,
    EvaluateRulesInfoRuleItemComponent,
    EvaluateRulesSearchComponent,
    EvaluateRulesSelectRuleComponent,
    EllipsisPipe,
    EvaluateRulesListComponent,
    EvaluateRulesInfoRuleContainerComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EvaluateRulesRoutingModule,
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    TranslateModule,
    TooltipModule,
    CommonModalModule,
    NgSelectModule,
    UiModalSelectModule,
    Conditions2Module,
    SimplebarAngularModule,
    RuleQueryComponentsModule,
    ConditionsBasicAndAdvanceModule,
  ],
  providers: [DatePipe],
})
export class EvaluateRulesModule {}
