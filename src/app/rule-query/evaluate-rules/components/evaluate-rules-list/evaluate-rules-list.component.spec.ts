import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { ProductRuleView } from 'src/app/rule-query/models/product.model';
import { RuleWithIdentifier } from '../../models/evaluate-rules-request.model';
import { EvaluateRulesService } from '../../services/evaluate-rules.service';
import { EvaluateRulesListComponent } from './evaluate-rules-list.component';

describe('EvaluateRulesListComponent', () => {
  let component: EvaluateRulesListComponent;
  let fixture: ComponentFixture<EvaluateRulesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [EvaluateRulesListComponent],
      providers: [
        {
          provide: EvaluateRulesService,
          useValue: {
            evaluateRule() {
              return of([{}]);
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateRulesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create rows', () => {
    component.data = [[{ attributes: {} }] as ProductRuleView[], {} as unknown as RuleWithIdentifier];
    expect(component).toBeTruthy();
  });
});
