/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/member-ordering */
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ProductRuleView } from 'src/app/rule-query/models/product.model';
import { RuleWithIdentifier } from '../../models/evaluate-rules-request.model';
import { EvaluateRulesService } from '../../services/evaluate-rules.service';

@Component({
  selector: 'ff-evaluate-rules-list',
  templateUrl: './evaluate-rules-list.component.html',
  styleUrls: ['./evaluate-rules-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvaluateRulesListComponent implements OnInit {
  @Input() set data(data: [ProductRuleView[], RuleWithIdentifier]) {
    if (data) {
      this.createRows(data[1], data[0]);
    }
  }
  public items!: [any, Observable<any>][];
  private cacheSize = 10;
  constructor(private evalauteRulesService: EvaluateRulesService) {}

  ngOnInit(): void {}

  get hasItems() {
    return this.items && this.items?.length;
  }
  createRows(rule: RuleWithIdentifier, products: ProductRuleView[]) {
    const monoRequest = this.makeRequest(
      rule,
      products.map((p) => p.attributes)
    ).pipe(shareReplay(1));
    // cache all request in one if products size < cachesize
    this.items = products.map((product, i) => [
      product,
      products.length < this.cacheSize
        ? monoRequest.pipe(map((resp) => resp.data[i]))
        : this.makeRequest(rule, [product.attributes]).pipe(map((resp) => resp.data[0])),
    ]);
  }

  makeRequest(rule: RuleWithIdentifier, attributes: any[]) {
    const payloadPost = {
      attributes,
      rule: {
        identifier: rule.id,
        ...rule,
      },
    };
    return this.evalauteRulesService.evaluateRule(payloadPost);
  }
}
