import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { GenericApiResponse } from '@model/base-api.model';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { AdminGroup } from 'src/app/rule-engine/admin-group/models/admin-group.model';
import { AdminGroupService } from 'src/app/rule-engine/admin-group/services/admin-group.service';
import { RULE_SEARCH_TOKEN } from 'src/app/rule-query/providers/rule-search.provider';
import { EVALUATE_RULES_FILTER } from 'src/app/rule-query/providers/search-filters.tokens';
import { RuleQueryComponentsModule } from 'src/app/rule-query/rule-query-components.module';
import { ProductSearcherService } from 'src/app/rule-query/services/product-searcher.service';
import { RuleSearchService } from '../../services/rule-search.service';
import { EvaluateRulesSelectRuleComponent } from '../evaluate-rules-select-rule/evaluate-rules-select-rule.component';
import { EvaluateRulesSearchComponent } from './evaluate-rules-search.component';

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}
  public get(key: any): any {
    of(key);
  }
  public instant(s: string) {
    return s;
  }
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}
let component: EvaluateRulesSearchComponent;
let fixture: ComponentFixture<EvaluateRulesSearchComponent>;
describe('EvaluateRulesSearchComponent', () => {
  let adminGroupServiceSpy: jasmine.SpyObj<AdminGroupService>;

  beforeEach(() => {
    const apyAdminGroupSpy: jasmine.SpyObj<AdminGroupService> = jasmine.createSpyObj('AdminGroupService', ['list']);

    TestBed.configureTestingModule({
      declarations: [TranslatePipeMock, EvaluateRulesSearchComponent, EvaluateRulesSelectRuleComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule, NgSelectModule, RouterTestingModule, RuleQueryComponentsModule],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceStub },
        { provide: AdminGroupService, useValue: apyAdminGroupSpy },
        { provide: BsModalService, useValue: {} },
        {
          provide: ProductSearcherService,
          useValue: {
            listPeeked() {
              return of({});
            },
          },
        },
        {
          provide: RuleSearchService,
          useValue: {
            findById() {
              return of({});
            },
          },
        },
        {
          provide: EVALUATE_RULES_FILTER,
          useValue: of({
            productId: '123',
            productType: 'sale_reference',
            ruleId: '1234',
            ruleType: 'LIMIT_SALE',
          }),
        },
        {
          provide: RULE_SEARCH_TOKEN,
          useValue: of({}),
        },
      ],
    }).compileComponents();

    adminGroupServiceSpy = TestBed.get(AdminGroupService);
    const resp = {} as unknown as GenericApiResponse<AdminGroup>;
    adminGroupServiceSpy.list.and.returnValue(of(resp));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateRulesSearchComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });
  it('it creates EvaluateRulesSearchComponent', () => {
    expect(component).toBeTruthy();
  });

  it('changeProductCode', fakeAsync(() => {
    component.changeProductCode('A001')?.subscribe(() => {
      expect(component.typeCode).toBe('sale_reference');
    });
  }));

  it('submit', () => {
    component.form.patchValue({
      textCodeProduct: 'asdf',
      typeSelect: 'b',
      product: 'asd',
      rule: 'a',
    });
    component.submit();
    expect(component.sended).toBeTruthy();
  });
});
