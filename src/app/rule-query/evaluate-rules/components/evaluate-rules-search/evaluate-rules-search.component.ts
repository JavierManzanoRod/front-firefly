import { Location } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericApiRequest } from '@model/base-api.model';
import { combineLatest, from, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil, startWith } from 'rxjs/operators';
import { UIModalSelect, UIModalSelectTextsView } from 'src/app/modules/ui-modal-select/models/modal-select.model';
import { EVALUATE_RULES_FILTER } from 'src/app/rule-query/providers/search-filters.tokens';
import { ProductSearcherService } from 'src/app/rule-query/services/product-searcher.service';
import { DestroyService } from 'src/app/shared/services/destroy.service';
import { ProductRuleView, TypeCode } from '../../../models/product.model';
import { RULE_SEARCH_TOKEN } from '../../../providers/rule-search.provider';
import { RuleWithIdentifier } from '../../models/evaluate-rules-request.model';
import { EvaluateRulesSearchForm } from '../../models/evaluate-rules-search.model';
import { RuleType } from '../../models/rule-type.enum';
import { RuleSearchService } from '../../services/rule-search.service';
import { validateSelectedProduct } from '../../validators/evalute-rules-validator-field';

interface ConfigModal {
  modalConfig: UIModalSelect | UIModalSelectTextsView;
  modalType?: 'select' | 'free';
  label?: string;
}

@Component({
  selector: 'ff-evaluate-rules-search',
  templateUrl: './evaluate-rules-search.component.html',
  styleUrls: ['./evaluate-rules-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DestroyService],
})
export class EvaluateRulesSearchComponent implements OnInit {
  @Input() messageError!: string;
  @Output() formSubmit: EventEmitter<[EvaluateRulesSearchForm, RuleWithIdentifier]> = new EventEmitter<
    [EvaluateRulesSearchForm, RuleWithIdentifier]
  >();

  item$!: Observable<ProductRuleView[] | null>;
  itemInput$ = new Subject<string>();

  typeCode: TypeCode = 'sale_reference';
  form!: FormGroup;
  itemLoading = false;
  value: any = null;
  configModal!: ConfigModal;
  isCollapsedConfig: any = {};
  sended = false;
  public productLabel = 'PRODUCT_CODE';
  public salesReferences = [
    {
      value: 'sale_reference',
      label: 'TYPES_CODE.ECI_REF',
    },
    {
      value: 'item_code',
      label: 'TYPES_CODE.CODE_A',
    },
    {
      value: 'ean',
      label: 'TYPES_CODE.EAN',
    },
  ];
  private again$ = new Subject<boolean>();
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiProductSearcher: ProductSearcherService,
    private ruleSearchService: RuleSearchService,
    private changeRef: ChangeDetectorRef,
    @Inject(DestroyService) private readonly destroy$: Observable<void>,
    @Inject(EVALUATE_RULES_FILTER) private filter: Observable<EvaluateRulesSearchForm>,
    @Inject(RULE_SEARCH_TOKEN) private ruleToken: Observable<RuleWithIdentifier>,
    private location: Location
  ) {}
  ngOnInit() {
    this.initForm();
    this.loadAdminGroupList();
    this.form
      .get('typeSelect')
      ?.valueChanges.pipe(distinctUntilChanged())
      .subscribe((val) => (this.productLabel = this.salesReferences.find((sale) => sale.value === val)?.label || ''));

    combineLatest([this.filter, this.ruleToken, this.again$.pipe(startWith(false))])
      .pipe(distinctUntilChanged(), takeUntil(this.destroy$))
      .subscribe(([filter, ruleData]) => {
        if (filter) {
          this.form.patchValue({
            typeSelect: filter.productType,
            product: filter.productId,
          });
          this.typeCode = this.form.controls.typeSelect.value;
          const isResult = this.route.snapshot?.firstChild?.url[0].path === 'result';
          if (isResult) {
            this.sended = true;
            this.form.disable();
          } else {
            this.form.enable();
            this.sended = false;
          }
        } else {
          this.reset();
        }
        if (ruleData) {
          this.form.patchValue({
            rule: ruleData,
          });
        }
      });
  }

  back() {
    this.router.navigate(['/rule-query/evaluate-rules'], { queryParamsHandling: 'preserve' }).then(() => {
      this.again$.next(false);
    });
  }

  reset() {
    this.sended = false;
    this.form?.reset({
      typeSelect: this.salesReferences[0].value,
      product: null,
      rule: null,
    });
    this.form.enable();
    this.changeRef.markForCheck();
  }
  changeProductCode = (e: string) => {
    if (e) {
      return this.apiProductSearcher.listPeeked({
        [this.typeCode]: e.trim(),
      });
    }
  };

  loadAdminGroupList() {
    this.configModal = {
      label: 'RULE_QUERY.PRODUCT.MODAL_RULE_TITLE',
      modalType: 'select',
      modalConfig: {
        lg: true,
        title: 'RULE_QUERY.PRODUCT.SELECT_RULE', // RULE_QUERY.PRODUCT.MODAL_RULE_TITLE
        multiple: false,
        itemValueKey: 'idCustom',
        searchFn: (x: GenericApiRequest, chips) => {
          return from(this.ruleSearchService.searchPaginate(x, chips as any));
        },
        filterFn: (val) => val.length > 0,
        inputFilterLabel: 'RULE_QUERY.PRODUCT.MODAL_INPUT_LABEL',
        nItemsLabel: 'RULE_QUERY.PRODUCT.MODAL_N_ITEMS_LABEL',
        checkboxsLabel: 'RULE_QUERY.PRODUCT.MODAL_RULE_TYPE',
        inputFilterPlaceholder: 'RULE_QUERY.PRODUCT.MODAL_PLACEHOLDDER',
        chips: [
          {
            value: true,
            key: RuleType.admin_group,
            name: 'RULE_QUERY.PRODUCT.ADMIN_GROUP',
          },
          {
            value: true,
            key: RuleType.badge,
            name: 'RULE_QUERY.PRODUCT.BADGE',
          },
          {
            value: true,
            key: RuleType.content_group,
            name: 'RULE_QUERY.PRODUCT.CONTENT_GROUP',
          },
          {
            value: true,
            key: RuleType.expert_admin,
            name: 'RULE_QUERY.PRODUCT.EXPERT_ADMIN',
          },
          {
            value: true,
            key: RuleType.loyalty,
            name: 'RULE_QUERY.PRODUCT.LOYALTY',
          },
          {
            value: true,
            key: RuleType.fluorinated_gas,
            name: 'RULE_QUERY.PRODUCT.FLUORINATED_GAS',
          },
          {
            value: true,
            key: RuleType.size_guide,
            name: 'RULE_QUERY.PRODUCT.SIZE_GUIDE',
          },
          {
            value: true,
            key: RuleType.price_inheritance,
            name: 'RULE_QUERY.PRODUCT.PRICE_INHERITANCE',
          },
          {
            value: true,
            key: RuleType.limit_sale,
            name: 'RULE_QUERY.PRODUCT.LIMIT_SALES',
          },
          {
            value: true,
            key: RuleType.exclude_search,
            name: 'RULE_QUERY.PRODUCT.EXCLUDE_SEARCH',
          },
          {
            value: true,
            key: RuleType.special_product,
            name: 'RULE_QUERY.PRODUCT.SPECIAL_PRODUCT',
          },
        ],
      },
    };
  }

  get product() {
    return this.form && this.form.controls.product;
  }

  get productValue(): string {
    return this.product.value;
  }
  get ruleValue(): RuleWithIdentifier {
    return this.form && this.form.controls.rule.value;
  }

  get productErrors() {
    return Object.keys(this.product.errors || {}).length > 0;
  }

  get invalidField() {
    return this.product.errors?.invalidField !== undefined;
  }

  changeCodeType() {
    this.typeCode = this.form.controls.typeSelect.value;
    this.form.controls.product.setValue(null);
  }

  submit() {
    this.form.markAllAsTouched();

    if (this.form.valid) {
      this.form.disable();
      this.sended = true;
      this.formSubmit.emit([
        new EvaluateRulesSearchForm(this.typeCode, this.productValue, this.ruleValue.ruleType, this.ruleValue.id),
        this.ruleValue,
      ]);
    }
  }

  /**
   *  Navigate to rule detail, we need all the params in the form
   *
   * @param url
   */
  navigate(url: string) {
    const urlTree = this.router.createUrlTree([], {
      queryParams: new EvaluateRulesSearchForm(this.typeCode, this.productValue, this.ruleValue.ruleType, this.ruleValue.id),
      queryParamsHandling: 'merge',
      preserveFragment: true,
    });
    // Set url params to load form when we go back
    this.location.go(urlTree.toString());
    // Go to detail
    this.router.navigate([url], { state: { goBack: true, backMessage: 'RULE_QUERY.EVALUATE_RULES.BACKMESSAGE' } });
  }

  private initForm() {
    this.form = this.fb.group({
      typeSelect: [this.salesReferences[0].value],
      // eslint-disable-next-line @typescript-eslint/unbound-method
      product: [null, [Validators.required, validateSelectedProduct]],
      // eslint-disable-next-line @typescript-eslint/unbound-method
      rule: [null, Validators.required],
    });
  }
}
