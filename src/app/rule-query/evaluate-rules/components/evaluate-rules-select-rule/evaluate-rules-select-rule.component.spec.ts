import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Pipe, PipeTransform } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { RuleSearchService } from '../../services/rule-search.service';
import { EvaluateRulesSelectRuleComponent } from './evaluate-rules-select-rule.component';

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}
  public get(key: any): any {
    of(key);
  }
}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

class ModalServiceMock {
  public content: any = {
    confirm: of({}),
    errorMessage: '',
  };
  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }
  public hide() {
    return true;
  }
}

describe('EvaluateRulesSelectRuleComponent', () => {
  // let itemServiceSpy: jasmine.SpyObj<ProductSearcherService>;
  // let adminGroupServiceSpy: jasmine.SpyObj<AdminGroupService>;

  let modalMockInstance: ModalServiceMock;

  beforeEach(() => {
    // const apySpy: jasmine.SpyObj<ProductSearcherService> = jasmine.createSpyObj('ProductSearcherService', ['listPeeked']);
    // const apyAdminGroupSpy: jasmine.SpyObj<AdminGroupService> = jasmine.createSpyObj('AdminGroupService', ['list']);

    TestBed.configureTestingModule({
      declarations: [TranslatePipeMock, EvaluateRulesSelectRuleComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceStub },
        { provide: BsModalService, useClass: ModalServiceMock },
        {
          provide: RuleSearchService,
          useValue: {
            findByid() {
              return of({});
            },
          },
        },
        // { provide: AdminGroupService, useValue: apyAdminGroupSpy },
      ],
    }).compileComponents();

    // eslint-disable-next-line import/no-deprecated
    // adminGroupServiceSpy = TestBed.get(AdminGroupService);
    // adminGroupServiceSpy.list.and.returnValue(of(null));

    // eslint-disable-next-line import/no-deprecated
    modalMockInstance = TestBed.get(BsModalService);
  });

  it('it creates ProductQuerySelectRuleComponent', () => {
    const fixture = TestBed.createComponent(EvaluateRulesSelectRuleComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });
});
