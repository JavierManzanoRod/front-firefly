import { identifierName } from '@angular/compiler';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Inject,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { switchMap } from 'rxjs';
import { ContentGroupRule } from 'src/app/catalog/content-group/model/content-group.models';
import { AdminGroupTypeMaster } from 'src/app/configuration/models/configuration.model';
import { FF_ALL_ADMIN_GROUP_TYPE } from 'src/app/configuration/tokens/admin-group-type.token';
import { RulesType } from 'src/app/modules/conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { UIModalSelect, UIModalSelectTextsView } from 'src/app/modules/ui-modal-select/models/modal-select.model';
import { UiModalSelectService } from 'src/app/modules/ui-modal-select/ui-modal-select.service';
import { SatisfiedRules } from 'src/app/rule-query/satisfied-rules/models/satisfied-rules.model';
import { SatisfiedRulesUtilsService } from 'src/app/rule-query/satisfied-rules/services/satisfied-rules-utils.service';
import { RuleWithIdentifier } from '../../models/evaluate-rules-request.model';
import { RuleSearchService } from '../../services/rule-search.service';

interface ConfigModal {
  modalConfig: UIModalSelect | UIModalSelectTextsView;
  modalType?: 'select' | 'free';
  label?: string;
}

@Component({
  selector: 'ff-evaluate-rules-select-rule',
  templateUrl: './evaluate-rules-select-rule.component.html',
  styleUrls: ['./evaluate-rules-select-rule.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EvaluateRulesSelectRuleComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvaluateRulesSelectRuleComponent implements ControlValueAccessor {
  @Output() navigate = new EventEmitter<string>();
  @ViewChild('conditionsModalTemplate') conditionModalElement!: ElementRef;
  @Input() config!: ConfigModal;

  value: RuleWithIdentifier | undefined;
  disabled = false;
  modalRef!: BsModalRef;
  constructor(
    private readonly modalService: BsModalService,
    private readonly uiModalSelectService: UiModalSelectService,
    private ref: ChangeDetectorRef,
    private satisfiedRulesUtilsService: SatisfiedRulesUtilsService,
    @Inject(FF_ALL_ADMIN_GROUP_TYPE) public adminGroupsType: { [key in AdminGroupTypeMaster]: string },
    private ruleSearchService: RuleSearchService
  ) {}

  public get ruleTypeFormat(): RulesType {
    return this.satisfiedRulesUtilsService.ruleTypeFormat(this.value?.admin_group_type_id?.toLowerCase() as string);
  }

  writeValue(data: RuleWithIdentifier | undefined): void {
    this.value = data;
    this.onChange(data);
    this.ref.markForCheck();
  }

  registerOnChange(fn: (data: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(): void {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange(data: any) {
    // don't delete!
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
    this.ref.markForCheck();
  }

  remove() {
    this.value = undefined;
    this.onChange(this.value);
  }

  select() {
    (this.config.modalConfig as any).selected = this.value;
    this.uiModalSelectService
      .showSelectRules(this.config.modalConfig as any, this.modalService)
      .pipe(switchMap(({ id, ruleType }: RuleWithIdentifier) => this.ruleSearchService.findById(id || '', ruleType || '')))
      .subscribe(
        (selectedItem: RuleWithIdentifier) => {
          if (selectedItem) {
            this.value = selectedItem;
            this.writeValue(selectedItem);
          }
        },
        (err) => console.log(err)
      );
  }

  showDetailModal() {
    this.modalRef = this.modalService.show(this.conditionModalElement as any, { class: 'modal-lg' });
    console.log('Value', this.value);
    this.ref.markForCheck();
  }

  go() {
    if (!this.value) {
      return;
    }
    const { ruleType, admin_group_type_id, id } = this.value;
    const folder_id = (this.value as ContentGroupRule)?.folder_id;
    const urlBase = this.satisfiedRulesUtilsService.ruleRoute({ rule_type: admin_group_type_id?.toLowerCase() } as SatisfiedRules);
    const isContentGroup = ruleType?.toLowerCase() === this.adminGroupsType.contentGroup;
    this.navigate.emit(`${urlBase}/${isContentGroup && folder_id ? folder_id + '/' : ''}${id}`);
  }

  closeModal() {
    this.modalRef.hide();
  }
}
