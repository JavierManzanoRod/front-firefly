/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, Input } from '@angular/core';
import {
  EvaluateRulesConditionNode,
  EvaluateRulesConditionSimple,
  isEvaluateRulesConditionNode,
} from '../../../models/evaluate-rules-response.model';

@Component({
  selector: 'ff-evaluate-rules-info-rule-container',
  templateUrl: './evaluate-rules-info-rule-container.component.html',
  styleUrls: ['./evaluate-rules-info-rule-container.component.scss'],
})
export class EvaluateRulesInfoRuleContainerComponent {
  @Input() set node(node: EvaluateRulesConditionSimple | EvaluateRulesConditionNode) {
    if (node) {
      this._node = { ...node };
    }
  }

  get node() {
    return this._node;
  }

  get typeName() {
    return this.convertToTranslationKey(this.node.type);
  }
  private _node!: EvaluateRulesConditionSimple | EvaluateRulesConditionNode;
  constructor() {}

  get isSimple() {
    return !isEvaluateRulesConditionNode(this.node);
  }

  get itemNodes() {
    return (this.node as EvaluateRulesConditionNode).nodes;
  }

  getIcon(item: EvaluateRulesConditionSimple | EvaluateRulesConditionNode) {
    return item && item.has_result ? 'icon-success' : item && item.has_result === false ? 'icon-fail' : 'icon-unevaluated';
  }

  private convertToTranslationKey(key: string) {
    return `RULE_QUERY.EVALUATE_RULES.OPERATOR.${key?.toUpperCase()}`;
  }
}
