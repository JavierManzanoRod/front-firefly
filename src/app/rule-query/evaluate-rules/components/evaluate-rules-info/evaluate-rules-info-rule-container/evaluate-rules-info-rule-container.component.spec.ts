import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EvaluateRulesInfoRuleContainerComponent } from './evaluate-rules-info-rule-container.component';

describe('EvaluateRulesInfoRuleContainerComponent', () => {
  let component: EvaluateRulesInfoRuleContainerComponent;
  let fixture: ComponentFixture<EvaluateRulesInfoRuleContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EvaluateRulesInfoRuleContainerComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateRulesInfoRuleContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
