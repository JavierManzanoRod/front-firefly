/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/member-ordering */
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { EvaluateRulesConditionSimple } from '../../../models/evaluate-rules-response.model';

@Component({
  selector: 'ff-evaluate-rules-info-rule',
  templateUrl: './evaluate-rules-info-rule.component.html',
  styleUrls: ['./evaluate-rules-info-rule.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvaluateRulesInfoRuleComponent {
  type!: string;
  @Input() set node(node: EvaluateRulesConditionSimple) {
    if (node) {
      this._node = { ...node };
      this.type = this.convertToTranslationKey(this.node.type);
    }
  }
  get node() {
    return this._node;
  }
  private _node!: EvaluateRulesConditionSimple;

  getIcon(item: EvaluateRulesConditionSimple) {
    return item?.has_result ? 'icon-success' : item?.has_result === false ? 'icon-fail' : 'icon-unevaluated';
  }

  get isDefined() {
    return this.node?.type === 'IS_DEFINED';
  }

  get ruleValue(): unknown {
    return this._node?.value as unknown;
  }

  get ruleMultipleValues() {
    return this._node?.values;
  }

  get productValue() {
    return this.node?.value_products || this.node?.value_product;
  }

  private convertToTranslationKey(key: string) {
    return `RULE_QUERY.EVALUATE_RULES.OPERATOR.${key.toUpperCase()}`;
  }
}
