import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { EvaluateRulesInfoRuleComponent } from './evaluate-rules-info-rule.component';

describe('EvaluateRulesInfoRuleItemComponent', () => {
  let component: EvaluateRulesInfoRuleComponent;
  let fixture: ComponentFixture<EvaluateRulesInfoRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EvaluateRulesInfoRuleComponent],
      imports: [TranslateModule.forRoot()],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateRulesInfoRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
