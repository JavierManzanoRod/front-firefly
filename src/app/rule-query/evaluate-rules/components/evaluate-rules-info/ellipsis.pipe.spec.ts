import { TranslateService } from '@ngx-translate/core';
import { EllipsisPipe } from './ellipsis.pipe';

describe('EllipsisPipe', () => {
  it('create an instance', () => {
    const pipe = new EllipsisPipe(({} as unknown) as TranslateService);
    expect(pipe).toBeTruthy();
  });
});
