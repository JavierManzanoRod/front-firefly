import { DatePipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { UiModalSelectService } from 'src/app/modules/ui-modal-select/ui-modal-select.service';
import { EvaluateRulesConditionSimple } from '../../../models/evaluate-rules-response.model';

@Component({
  selector: 'ff-evaluate-rules-info-rule-item',
  templateUrl: './evaluate-rules-info-rule-item.component.html',
  styleUrls: ['./evaluate-rules-info-rule-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvaluateRulesInfoRuleItemComponent {
  @Input() data: unknown[] = [];
  @Input() node!: EvaluateRulesConditionSimple;
  @Input() success!: boolean;
  @Input() rounded = false;
  @Input() dataType: 'STRING' | 'DOUBLE' | 'BOOLEAN' | 'ENTITY' | 'EMBEDDED' | 'DATE_TIME' | undefined;
  @Input() tooltipText = '';

  public dateFormat = 'dd-MM-yyyy HH:mm';

  constructor(
    private datePipe: DatePipe,
    private uiModalSelectService: UiModalSelectService,
    private readonly modalService: BsModalService
  ) {}

  isArray(val: any): val is any[] {
    return Array.isArray(val);
  }

  get dataFormart() {
    if (this.dataType === 'DATE_TIME' && !this.isArray(this.data)) {
      return this.datePipe.transform(this.data, this.dateFormat);
    }
    return this.data;
  }

  showMore() {
    this.uiModalSelectService.showInfo(
      {
        title: 'VALUES',
        prefix: this.node?.attribute,
        searchFn: () => {
          return of(
            this.data.map((v: any) => {
              return { key: v, id: v };
            })
          );
        },
      },
      this.modalService
    );
  }
}
