import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { UiModalSelectService } from 'src/app/modules/ui-modal-select/ui-modal-select.service';
import { EvaluateRulesInfoRuleItemComponent } from './evaluate-rules-info-rule-item.component';

describe('EvaluateRulesInfoRuleItemComponent', () => {
  let component: EvaluateRulesInfoRuleItemComponent;
  let fixture: ComponentFixture<EvaluateRulesInfoRuleItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EvaluateRulesInfoRuleItemComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: DatePipe,
          useFactory: () => ({}),
        },
        {
          provide: BsModalService,
          useFactory: () => ({}),
        },
        {
          provide: UiModalSelectService,
          useFactory: () => ({}),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateRulesInfoRuleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
