import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'ellipsis',
})
export class EllipsisPipe implements PipeTransform {
  constructor(private translateService: TranslateService) {}
  transform(value: unknown[], ending: string, itemsToShow = 2): string {
    const response: string[] = [];
    const stringArray = value.map((v) => String(v));
    if (value.length > itemsToShow) {
      const leftItems = value.length - itemsToShow;
      response.push(...stringArray.slice(0, itemsToShow));
      return `${response.join(', ')}  ${this.translateService.instant(ending, { amount: leftItems })}`;
    } else {
      response.push(...stringArray);
      return response.join(', ');
    }
  }
}
