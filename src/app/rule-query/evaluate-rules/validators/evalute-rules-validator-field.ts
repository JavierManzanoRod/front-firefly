import { FormControl, ValidationErrors } from '@angular/forms';

export function validateSelectedProduct(input: FormControl): ValidationErrors | null {
  if (input.value) {
    if (input.value.length > 0) {
      return null;
    } else if (input.value.length === 0) {
      return {
        invalidField: { message: 'El código de producto no existe' },
      };
    }
    return null;
  } else {
    return {
      invalidField: { message: 'El código del producto es requerido' },
    };
  }
}
