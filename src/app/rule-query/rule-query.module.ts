import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RuleQueryRoutingModule } from './rule-query-routing.module';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, RuleQueryRoutingModule],
  declarations: [],
  providers: [],
  exports: [],
})
export class RuleQueryModule {}
