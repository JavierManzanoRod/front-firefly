import { HttpClient, HttpParams } from '@angular/common/http';
import { fakeAsync } from '@angular/core/testing';
import { GenericApiResponse } from '@model/base-api.model';
import { of } from 'rxjs';
import { ProductRuleFlatten, ProductRuleView } from '../models/product.model';
import { ProductSearcherService } from './product-searcher.service';

let myService: ProductSearcherService;
let expectedList: GenericApiResponse<ProductRuleView>;

let httpClientSpy: { get: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy; post: jasmine.Spy; request: jasmine.Spy };
// let serviceToTest: ProductTypeService;

describe('ProductSearcherService', () => {
  beforeAll(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete', 'request']);
  });
  beforeEach(() => {
    myService = new ProductSearcherService(httpClientSpy as unknown as HttpClient, 'v1');
  });

  it('should get query params', () => {
    const params = myService.getQueryParams({
      sale_reference: true,
      item_code: true,
      ean: true,
    });
    const result = new HttpParams()
      .set('sales_reference', 'true')
      .set('product_id', 'true')
      .set('gtin', 'true')
      .set('is_attributes_flatten', 'true');

    expect(params).toEqual(result);
  });

  it('should get list', fakeAsync(() => {
    const response = [
      {
        attributes: {
          'item_offered.name[0].value': 'Name',
        },
      },
    ];
    httpClientSpy.get.and.returnValue(of(response));
    const expectedResponse = [{ attributes: { 'item_offered.name[0].value': 'Name' }, name: 'Name' }] as unknown as ProductRuleFlatten[];
    myService.listPeeked({}).subscribe((val) => {
      expect(val).toEqual(expectedResponse);
    });
  }));
});
