import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService } from '@core/base/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_ASSORTMENT_PRODUCT_SEARCHERS } from 'src/app/configuration/tokens/api-versions.token';
import { ProductRuleFlatten, ProductRuleView } from '../models/product.model';
@Injectable({
  providedIn: 'root',
})
export class ProductSearcherService extends ApiService<ProductRuleView> {
  endPoint = `products/backoffice-assortment/${this.version}product-searchers`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_ASSORTMENT_PRODUCT_SEARCHERS) private version: string) {
    super();
  }

  getQueryParams(filter: any) {
    let params: HttpParams = new HttpParams();
    if (filter) {
      params = filter.sale_reference ? params.set('sales_reference', (filter.sale_reference as string).toString()) : params;
      params = filter.item_code ? params.set('product_id', (filter.item_code as string).toString()) : params;
      params = filter.ean ? params.set('gtin', (filter.ean as string).toString()) : params;
    }
    params = params.set('is_attributes_flatten', 'true');
    return params;
  }

  listPeeked(queryFilter: any): Observable<ProductRuleView[]> {
    return this.http.get<ProductRuleFlatten[]>(this.urlBase, { params: this.getQueryParams(queryFilter) }).pipe(
      map((productList) => {
        return productList.map((product) => {
          let name;
          try {
            name = product.attributes['item_offered.name[0].value'] ?? 'Sin nombre registrado';
          } catch (e) {
            name ??= 'Sin nombre registrado';
          }

          return {
            ...product,
            name,
          };
        });
      })
    );
  }
}
