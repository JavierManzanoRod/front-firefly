import { GenericApiResponse } from '@model/base-api.model';

export interface TypeCodesApiRequest {
  ean?: string;
  saleReference?: string;
  itemCode?: string;
}

export interface ResolvedRules {
  attribute: string;
  value: string;
  node_type:
    | 'eq'
    | 'like'
    | 'distinct'
    | 'gt'
    | 'lt'
    | 'in'
    | 'notin'
    | 'NOT_LIKE'
    | 'INCLUDE_ALL'
    | 'INCLUDE_ANY'
    | 'NOT_INCLUDE_ALL'
    | 'NOT_INCLUDE_ANY'
    | 'START_WITH'
    | 'START_WITH_IGNORE_CASE'
    | 'IS_DEFINED'
    | 'IS_NOT_DEFINED'
    | 'LIKE_IGNORE_CASE'
    | 'NOT_LIKE_IGNORE_CASE';
}

export interface SatisfiedRules {
  rule_id: string;
  rule_name: string;
  rule_type: 'ADMIN_GROUP' | 'EXCLUDE_SEARCH' | 'LIMIT_SALE' | 'CATEGORY_RULE';
  folder?: string;
  effective_date: string;
  expiration_date: string;
  is_active: boolean;
  resolved_rules: ResolvedRules[];
}

export type SatisfiedRulesResponse = GenericApiResponse<SatisfiedRules>;

export interface SatisfiedRulesRequest {
  attribute_map: Partial<any>;
  rule_name?: string;
  effective_date?: string;
  expiration_date?: string;
  is_active?: boolean;
}

export interface PageChanged {
  page: number;
  size: number;
}

export interface SatisfiedRulesParam {
  ruleType: string;
  ruleId: string;
}

export class SatisfiedRuleSearchForm {
  constructor(
    public requestType = '',
    public productType = '',
    public productId = '',
    public effective_date = '',
    public expiration_date = '',
    public is_active = '',
    public rule_name = ''
  ) {}
}
