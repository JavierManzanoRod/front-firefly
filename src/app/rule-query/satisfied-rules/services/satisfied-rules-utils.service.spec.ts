import { HttpClient } from '@angular/common/http';
import { AdminGroupService } from 'src/app/rule-engine/admin-group/services/admin-group.service';
import { SatisfiedRulesUtilsService } from './satisfied-rules-utils.service';

let myService: SatisfiedRulesUtilsService;

let adminGroupService: any;

describe('SatisfiedRulesUtilsService', () => {
  beforeEach(() => {
    adminGroupService = new AdminGroupService(null as unknown as HttpClient, '') as any;
  });

  it('Checking observable to rules', () => {
    spyOn(adminGroupService, 'detail');

    myService = new SatisfiedRulesUtilsService(
      adminGroupService,
      adminGroupService,
      adminGroupService,
      adminGroupService,
      adminGroupService,
      adminGroupService,
      adminGroupService,
      adminGroupService,
      adminGroupService,
      adminGroupService,
      adminGroupService,
      {} as any
    );
    myService.getRuleService('otro', '2');

    expect(adminGroupService.detail).toHaveBeenCalled();
  });
});
