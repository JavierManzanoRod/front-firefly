import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiService, UpdateResponse } from '@core/base/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FF_API_PATH_VERSION_EVALUATE_RULES } from 'src/app/configuration/tokens/api-versions.token';
import { SatisfiedRulesRequest, SatisfiedRulesResponse } from '../models/satisfied-rules.model';

@Injectable({
  providedIn: 'root',
})
export class SatisfiedRulesService extends ApiService<SatisfiedRulesRequest> {
  endPoint = `products/backoffice-rules/${this.version}satisfied-rules`;

  constructor(protected http: HttpClient, @Inject(FF_API_PATH_VERSION_EVALUATE_RULES) private version: string) {
    super();
  }

  find(body: any, pagination: any): Observable<UpdateResponse<SatisfiedRulesResponse>> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .request('post', this.urlBase, {
        body,
        headers,
        observe: 'response',
        responseType: 'json',
        params: this.getParams(pagination),
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            status: res.status,
            data: res.body,
          };
        })
      );
  }

  postWithPagination(attributes: any, pagination: any): Observable<UpdateResponse<SatisfiedRulesResponse>> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .request('post', this.urlBase, {
        body: { attribute_map: attributes },
        headers,
        observe: 'response',
        responseType: 'json',
        params: this.getParams(pagination),
      })
      .pipe(
        map((res: HttpResponse<any>) => {
          return {
            status: res.status,
            data: res.body,
          };
        })
      );
  }
}
