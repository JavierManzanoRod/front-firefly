import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ContentGroupService } from 'src/app/catalog/content-group/services/content-group.service';
import { AdminGroupTypeMaster } from 'src/app/configuration/models/configuration.model';
import { FF_ALL_ADMIN_GROUP_TYPE } from 'src/app/configuration/tokens/admin-group-type.token';
import { ExpertAdminService } from 'src/app/control-panel/expert-admin/services/expert-admin.service';
import { LoyaltyService } from 'src/app/control-panel/loyalty/services/loyalty.service';
import { SizeGuideService } from 'src/app/control-panel/size-guide/services/size-guide.service';
import { RulesType } from 'src/app/modules/conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { AdminGroupService } from 'src/app/rule-engine/admin-group/services/admin-group.service';
import { ExcludeSearchService } from 'src/app/stock/exclude-search/services/exclude-search.service';
import { LimitSaleService } from 'src/app/stock/limit-sale/services/limit-sale.service';
import { SatisfiedRules } from '../models/satisfied-rules.model';
import { BadgeService } from './../../../control-panel/badge/services/badge.service';
import { FluorinatedGasService } from './../../../control-panel/fluorinated-gas/services/fluorinated-gas.service';
import { SpecialProductService } from './../../../control-panel/special-product/services/special-product.service';
import { PriceInheritanceService } from './../../../rule-engine/price-inheritance/services/price-inheritance.service';

@Injectable({
  providedIn: 'root',
})
export class SatisfiedRulesUtilsService {
  constructor(
    private apiAdminGroupService: AdminGroupService,
    private apiExcludeSearchService: ExcludeSearchService,
    private apiLimitSaleService: LimitSaleService,
    private apiBadgeService: BadgeService,
    private apiContentGroupService: ContentGroupService,
    private apiExpertService: ExpertAdminService,
    private apiFluorGases: FluorinatedGasService,
    private apiLoyaltiService: LoyaltyService,
    private apiPrieInheritanceService: PriceInheritanceService,
    private apiSizeGuideService: SizeGuideService,
    private apiSpecialProductsService: SpecialProductService,
    @Inject(FF_ALL_ADMIN_GROUP_TYPE) public adminGroupsType: { [key in AdminGroupTypeMaster]: string }
  ) {}

  getRuleService(ruleType: string, ruleId: string): Observable<any> {
    switch (ruleType) {
      case this.adminGroupsType.badge:
        return this.apiBadgeService.detail(ruleId);

      case this.adminGroupsType.contentGroup:
        return this.apiContentGroupService.detail(ruleId);

      case this.adminGroupsType.excludeSearch:
        return this.apiExcludeSearchService.detail(ruleId);

      case this.adminGroupsType.expert:
        return this.apiExpertService.detail(ruleId);

      case this.adminGroupsType.fluorGases:
        return this.apiFluorGases.detail(ruleId);

      case this.adminGroupsType.limitSale:
        return this.apiLimitSaleService.detail(ruleId);

      case this.adminGroupsType.loyalty:
        return this.apiLoyaltiService.detail(ruleId);

      case this.adminGroupsType.priceInheritance:
        return this.apiPrieInheritanceService.detail(ruleId);

      case this.adminGroupsType.sizeGuide:
        return this.apiSizeGuideService.detail(ruleId);

      case this.adminGroupsType.specialProducts:
        return this.apiSpecialProductsService.detail(ruleId);

      default:
        return this.apiAdminGroupService.detail(ruleId);
    }
  }

  parseRuleType(ruleType: string) {
    switch (ruleType) {
      case this.adminGroupsType.badge:
        return 'RULE_QUERY.GROUPS.BADGE';

      case this.adminGroupsType.contentGroup:
        return 'RULE_QUERY.GROUPS.CONTENT_GROUP';

      case this.adminGroupsType.excludeSearch:
        return 'RULE_QUERY.GROUPS.EXCLUDE_SEARCH';

      case this.adminGroupsType.expert:
        return 'RULE_QUERY.GROUPS.EXPERT';

      case this.adminGroupsType.fluorGases:
        return 'RULE_QUERY.GROUPS.FLUORGASES';

      case this.adminGroupsType.limitSale:
        return 'RULE_QUERY.GROUPS.LIMIT_SALE';

      case this.adminGroupsType.loyalty:
        return 'RULE_QUERY.GROUPS.LOYALTI';

      case this.adminGroupsType.priceInheritance:
        return 'RULE_QUERY.GROUPS.PRICE_INHERITANCE';

      case this.adminGroupsType.sizeGuide:
        return 'RULE_QUERY.GROUPS.SIZE_GUIDE';

      case this.adminGroupsType.specialProducts:
        return 'RULE_QUERY.GROUPS.SPECIAL_PRODUCTS';

      default:
        return 'RULE_QUERY.GROUPS.GROUP';
    }
  }

  ruleTypeFormat(ruleType: string) {
    switch (ruleType) {
      case this.adminGroupsType.badge:
        return RulesType.badge;
      case this.adminGroupsType.contentGroup:
        return RulesType.content_group;
      case this.adminGroupsType.excludeSearch:
        return RulesType.base_conditions;
      case this.adminGroupsType.expert:
        return RulesType.expert_admin;
      case this.adminGroupsType.fluorGases:
        return RulesType.fluorinated_gas;
      case this.adminGroupsType.limitSale:
        return RulesType.base_conditions;
      case this.adminGroupsType.loyalty:
        return RulesType.base_conditions;
      case this.adminGroupsType.priceInheritance:
        return RulesType.price_inheritance;
      case this.adminGroupsType.sizeGuide:
        return RulesType.size_guide;
      case this.adminGroupsType.specialProducts:
        return RulesType.special_product;
    }

    return RulesType.admin_group;
  }

  ruleRoute({ rule_type }: SatisfiedRules) {
    let urlBase;
    switch (rule_type) {
      case this.adminGroupsType.badge:
        urlBase = '/control-panel/badge/view/';
        break;
      case this.adminGroupsType.contentGroup:
        urlBase = '/catalog/content-group/rule/';
        break;
      case this.adminGroupsType.excludeSearch:
        urlBase = '/stock/exclude-search/view';
        break;
      case this.adminGroupsType.expert:
        urlBase = '/control-panel/expert-admin/view/';
        break;
      case this.adminGroupsType.fluorGases:
        urlBase = '/control-panel/fluorinated-gas/view/';
        break;
      case this.adminGroupsType.limitSale:
        urlBase = '/stock/limit-sale/view';
        break;
      case this.adminGroupsType.loyalty:
        urlBase = '/control-panel/loyalty/view/';
        break;
      case this.adminGroupsType.priceInheritance:
        urlBase = '/rule-engine/price-inheritance/view/';
        break;
      case this.adminGroupsType.sizeGuide:
        urlBase = '/control-panel/size-guide/view/';
        break;
      case this.adminGroupsType.specialProducts:
        urlBase = '/control-panel/special-product/view/';
        break;
      default:
        urlBase = '/rule-engine/admin-group/view';
        break;
    }
    return urlBase;
  }
}
