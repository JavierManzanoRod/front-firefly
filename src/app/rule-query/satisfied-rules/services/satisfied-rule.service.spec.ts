import { of } from 'rxjs';
import { SatisfiedRulesService } from './satisfied-rule.service';
let httpClientSpy: {
  get: jasmine.Spy;
  put: jasmine.Spy;
  delete: jasmine.Spy;
  post: jasmine.Spy;
  postWithPagination: jasmine.Spy;
  request: jasmine.Spy;
};

let myService: SatisfiedRulesService;

describe('SatisfiedRuleService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete', 'postWithPagination', 'request']);
  });

  it('postWithPagination is called', () => {
    const attributes = {
      description: 'description',
    };
    const page = { page: 1, size: 10 };

    const rawHttpResponse = { status: 200, body: { name: 'api-response-name' } };
    httpClientSpy.postWithPagination.and.returnValue(of({}));
    httpClientSpy.request.and.returnValue(of(rawHttpResponse));

    myService = new SatisfiedRulesService(httpClientSpy as any, '');
    myService.postWithPagination(attributes, page).subscribe((response: any) => {
      expect(response.status).toEqual(rawHttpResponse.status, 'expected status');
      expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
    });
  });

  it('find is called', () => {
    const attributes = {
      description: 'description',
    };
    const page = { page: 1, size: 10 };

    const rawHttpResponse = { status: 200, body: { name: 'api-response-name' } };
    httpClientSpy.request.and.returnValue(of(rawHttpResponse));

    myService = new SatisfiedRulesService(httpClientSpy as any, '');
    myService.find(attributes, page).subscribe((response) => {
      expect(response.status).toEqual(rawHttpResponse.status, 'expected status');
      expect(httpClientSpy.request.calls.count()).toBe(1, 'one call');
    });
  });
});
