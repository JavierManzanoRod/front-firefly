import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SimplebarAngularModule } from 'simplebar-angular';
import { CommonModalModule } from 'src/app/modules/common-modal/common-modal.module';
import { ConditionsBasicAndAdvanceModule } from 'src/app/modules/conditions-basic-and-advance/conditions-basic-and-advance.module';
import { DatetimepickerModule } from 'src/app/modules/datetimepicker/datetimepicker.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { Conditions2Module } from '../../modules/conditions2/conditions2.module';
import { FormErrorModule } from '../../modules/form-error/form-error.module';
import { LoadingModule } from '../../modules/loading/loading.module';
import { RuleQueryComponentsModule } from '../rule-query-components.module';
import { SatisfiedRuleRowComponent } from './components/satisfied-rule-row/satisfied-rule-row.component';
import { SatisfiedRulesListComponent } from './components/satisfied-rules-list/satisfied-rules-list.component';
import { SatisfiedRulesSearchComponent } from './components/satisfied-rules-search/satisfied-rules-search.component';
import { SatisfiedRulesDetailContainerComponent } from './containers/satisfied-rules-detail-container';
import { SatisfiedRulesSearchContainerComponent } from './containers/satisfied-rules-search-container';
import { SatisfiedRulesRoutingModule } from './satisfied-rules-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SatisfiedRulesRoutingModule,
    ModalModule.forRoot(),
    CommonModalModule,
    LoadingModule,
    FormErrorModule,
    ConditionsBasicAndAdvanceModule,
    Conditions2Module,
    DatetimepickerModule,
    SimplebarAngularModule,
    NgSelectModule,
    RuleQueryComponentsModule,
  ],
  declarations: [
    SatisfiedRulesSearchContainerComponent,
    SatisfiedRulesDetailContainerComponent,
    SatisfiedRulesListComponent,
    SatisfiedRulesSearchComponent,
    SatisfiedRuleRowComponent,
  ],
})
export class SatisfiedRulesModule {}
