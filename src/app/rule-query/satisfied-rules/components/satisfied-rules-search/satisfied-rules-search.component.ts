import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup, NgControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTimeService } from '@core/base/date-time.service';
import { map, Observable, startWith, Subject, switchMap } from 'rxjs';
import { SATISFIED_RULES_FILTER } from 'src/app/rule-query/providers/search-filters.tokens';
import { ProductSearcherService } from 'src/app/rule-query/services/product-searcher.service';
import { FormUtilsService } from 'src/app/shared/services/form-utils.service';
import { dateRangeInitValidator } from 'src/app/shared/validations/date-range-init.validator';
import { dateTimeValidator } from 'src/app/shared/validations/date-time.validator';
import { TypeCode } from '../../../models/product.model';
import { SatisfiedRuleSearchForm } from '../../models/satisfied-rules.model';

@Component({
  selector: 'ff-satisfied-rules-search',
  templateUrl: './satisfied-rules-search.component.html',
  styleUrls: ['./satisfied-rules-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SatisfiedRulesSearchComponent implements OnInit {
  @Output() searchParams = new EventEmitter<SatisfiedRuleSearchForm>();
  @ViewChild('product') productTemplate!: NgControl;
  form!: FormGroup;
  type = 'product';
  productSearch!: string;
  public productLabel = 'ECI_REF';
  typeCode: TypeCode = 'sale_reference';
  send = false;
  isCollapsedConfig: any = {};
  private again$ = new Subject<boolean>();
  constructor(
    private productSearcherService: ProductSearcherService,
    private router: Router,
    private dateTimeService: DateTimeService,
    private route: ActivatedRoute,
    private changeRef: ChangeDetectorRef,
    @Inject(SATISFIED_RULES_FILTER) private filter: Observable<SatisfiedRuleSearchForm>
  ) {
    this.initializeForm();
  }

  get errorInvalidStartDate() {
    const start = this.form.controls.effective_date;
    return start.touched && start?.errors?.invalidDate;
  }

  get errorInvalidEndDate() {
    const end = this.form.controls.expiration_date;
    return end.touched && end?.errors?.invalidDate;
  }

  get activeValue() {
    return this.form.controls.is_active.value;
  }
  ngOnInit() {
    this.filter
      .pipe(
        switchMap((form) =>
          this.again$.pipe(
            startWith(false),
            map((r) => [form, r] as [SatisfiedRuleSearchForm, boolean])
          )
        )
      )
      .subscribe(([filterData, repeat]) => {
        if (filterData) {
          const { is_active, productType, expiration_date, effective_date, rule_name, productId, requestType } = filterData;
          this.type = requestType;
          this.typeCode = productType as TypeCode;
          this.productSearch = productId;
          this.setValueSafe('effective_date', effective_date && new Date(effective_date));
          this.setValueSafe('expiration_date', expiration_date && new Date(expiration_date));
          this.setValueSafe('is_active', is_active ? JSON.parse(is_active) : is_active);
          this.setValueSafe('rule_name', rule_name);
          const isResult = this.route.snapshot?.firstChild?.url[0].path === 'result';
          if (!repeat && isResult) {
            this.send = true;
            this.form.disable();
          } else {
            this.form.enable();
            this.send = false;
          }
        } else {
          this.resetForm();
        }
      });
  }

  back() {
    this.router
      .navigate(['/rule-query/satisfied-rules'], {
        queryParamsHandling: 'preserve',
      })
      .then(() => {
        this.again$.next(true);
      });
  }

  public resetForm() {
    this.initializeForm();
    this.send = false;
    this.type = 'product';
    this.typeCode = 'sale_reference';
    this.productSearch = '';
    this.productTemplate?.control?.reset();
    this.changeRef.markForCheck();
  }

  changeType() {
    this.form.controls.effective_date.setValue(null);
    this.form.controls.expiration_date.setValue(null);
    this.form.controls.is_active.setValue('ALL');
  }

  changeCodeType() {
    this.productSearch = '';
    this.changeLabel();
  }

  changeLabel() {
    if (this.typeCode === 'sale_reference') {
      this.productLabel = 'ECI_REF';
    } else if (this.typeCode === 'item_code') {
      this.productLabel = 'CODE_A';
    } else if (this.typeCode === 'ean') {
      this.productLabel = 'EAN';
    }
  }

  searchProduct = (val: string) => {
    return this.productSearcherService.listPeeked({ [this.typeCode]: val.trim() });
  };

  submit() {
    FormUtilsService.markFormGroupTouched(this.form);
    this.productTemplate.control?.markAllAsTouched();
    if (this.productSearch?.length && this.form.valid) {
      const { effective_date, expiration_date, is_active, rule_name } = this.form.value;
      const isInfo = this.type === 'info';
      const satisfiedRuleForm = new SatisfiedRuleSearchForm(
        this.type,
        this.typeCode,
        this.productSearch,
        this.formatDateToSend(effective_date, isInfo, this.yesterday),
        this.formatDateToSend(expiration_date, isInfo, this.today),
        is_active !== 'ALL' ? is_active : undefined,
        rule_name
      );

      this.searchParams.emit(satisfiedRuleForm);
      this.send = true;
      this.form.disable();
    }
  }

  formatDateToSend(currentDate: string, isInfo: boolean, alternativeDate: Date) {
    let dateToSend: Date | undefined;
    if (isInfo && currentDate) {
      dateToSend = new Date(currentDate);
    } else if (isInfo && !currentDate) {
      dateToSend = undefined;
    } else {
      dateToSend = alternativeDate;
    }
    return dateToSend ? this.dateTimeService.convertUTCDateToSend(dateToSend) : undefined;
  }

  get yesterday(): Date {
    const yesterday = this.dateTimeService.subDay(new Date(), 1);
    return this.dateTimeService.startOfDay(yesterday);
  }
  get today(): Date {
    return this.dateTimeService.endOfDay(new Date());
  }
  private setValueSafe(name: string, val: unknown) {
    if (val !== undefined) {
      this.form.get(name)?.setValue(val);
    }
  }
  private initializeForm() {
    const config = FormUtilsService.generateFormGroupAndHisGeneralErrorsFromConfig({
      effective_date: {
        value: null,
      },
      expiration_date: {
        value: null,
      },
      is_active: {
        value: 'ALL',
      },
      rule_name: {
        value: null,
      },
    });
    this.form = config.form;
    this.form.controls.effective_date.setValidators(dateTimeValidator());
    this.form.controls.expiration_date.setValidators(dateTimeValidator());
    this.form.setValidators([dateRangeInitValidator('effective_date', 'expiration_date') as any]);
  }
}
