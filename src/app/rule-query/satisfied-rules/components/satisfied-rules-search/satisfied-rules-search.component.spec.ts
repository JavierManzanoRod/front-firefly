import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { DateTimeService } from '@core/base/date-time.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { of, throwError } from 'rxjs';
import { SATISFIED_RULES_FILTER } from 'src/app/rule-query/providers/search-filters.tokens';
import { RuleQueryComponentsModule } from 'src/app/rule-query/rule-query-components.module';
import { ProductSearcherService } from 'src/app/rule-query/services/product-searcher.service';
import { SatisfiedRulesSearchComponent } from './satisfied-rules-search.component';

describe('SatisfiedRulesSearchComponent', () => {
  let component: SatisfiedRulesSearchComponent;
  let fixture: ComponentFixture<SatisfiedRulesSearchComponent>;

  describe('Manual search', () => {
    beforeEach(() => {
      configureComponent();
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(SatisfiedRulesSearchComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create component', () => {
      expect(component).toBeTruthy();
    });

    it('checkIfProductCodeExist search with api and initialized product and fails', () => {
      component.submit();
      const service: ProductSearcherService = TestBed.get<ProductSearcherService>(ProductSearcherService);
      spyOn(service, 'listPeeked').and.returnValue(throwError({ status: 404 }));
      expect(component.productSearch?.length).toEqual(15);
    });
  });

  describe('Reset search', () => {
    beforeEach(() => {
      TestBed.overrideProvider(SATISFIED_RULES_FILTER, {
        useValue: of(null),
      });
      configureComponent();
    });
    beforeEach(() => {
      fixture = TestBed.createComponent(SatisfiedRulesSearchComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
    it('submit data', fakeAsync(() => {
      component.productSearch = 'searched';
      component.submit();
      expect(component.send).toBeTruthy();
    }));
  });
});

const configureComponent = () => {
  TestBed.configureTestingModule({
    declarations: [SatisfiedRulesSearchComponent],
    imports: [
      RouterTestingModule,
      ReactiveFormsModule,
      FormsModule,
      TranslateModule.forRoot({}),
      RuleQueryComponentsModule,
      NgSelectModule,
    ],
    providers: [
      {
        provide: ProductSearcherService,
        useValue: {
          listPeeked() {
            return of([{}]);
          },
        },
      },
      {
        provide: DateTimeService,
        useValue: {
          subDay() {
            return new Date();
          },
          startOfDay() {
            return new Date();
          },
          convertUTCDateToSend() {
            return '';
          },
          endOfDay() {
            return new Date();
          },
        },
      },
      {
        provide: SATISFIED_RULES_FILTER,
        useValue: of({
          requestType: 'product',
          codeType: 'sale_reference',
          productId: '080781597823052',
          // effective_date: undefined,
          // expiration_date: undefined,
          // is_active: undefined,
        }),
      },
    ],
  }).compileComponents();
};
