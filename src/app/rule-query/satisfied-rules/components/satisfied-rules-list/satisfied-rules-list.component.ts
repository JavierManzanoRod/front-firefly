import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';
import { ProductRuleView } from 'src/app/rule-query/models/product.model';
import { SatisfiedRuleSearchForm } from '../../models/satisfied-rules.model';
import { SatisfiedRulesService } from '../../services/satisfied-rule.service';

@Component({
  selector: 'ff-satisfied-rules-list',
  templateUrl: './satisfied-rules-list.component.html',
  styleUrls: ['./satisfied-rules-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SatisfiedRulesListComponent {
  @Input()
  public set products(data: [ProductRuleView[], SatisfiedRuleSearchForm]) {
    if (data) {
      this.createRows(data[1], data[0]);
    }
  }
  delayMessage$ = timer(2000).pipe(mapTo(true));
  items!: [ProductRuleView, Observable<any>][];
  constructor(private satisfiedRulesService: SatisfiedRulesService) {}

  get hasItems() {
    return this.items && this.items.length;
  }
  createRows(filter: SatisfiedRuleSearchForm, products: ProductRuleView[]) {
    this.items = products.map((product) => [product, this.makeRequest(filter, product.attributes)]);
  }

  makeRequest(filter: SatisfiedRuleSearchForm, attributes: any) {
    const payloadPost = {
      attribute_map: attributes,
      effective_date: filter.effective_date,
      expiration_date: filter.expiration_date,
      is_active: filter.requestType === 'info' ? filter.is_active : true,
      rule_name: filter.rule_name,
    };
    return this.satisfiedRulesService
      .find(payloadPost, {
        page: 0,
        size: 10,
      })
      .pipe(map((response) => response.data.content));
  }
}
