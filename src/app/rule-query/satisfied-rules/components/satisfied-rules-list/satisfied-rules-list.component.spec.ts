import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { UpdateResponse } from '@core/base/api.service';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable, of } from 'rxjs';
import { ProductRuleView } from 'src/app/rule-query/models/product.model';
import { SatisfiedRuleSearchForm, SatisfiedRulesResponse } from '../../models/satisfied-rules.model';
import { SatisfiedRulesService } from '../../services/satisfied-rule.service';
import { SatisfiedRulesListComponent } from './satisfied-rules-list.component';

// Globals variables
const attributes = {
  description: 'description product',
  name: 'name product',
};

class TranslateServiceStub {
  public setDefaultLang(data?: any) {}

  public get(key: any): any {
    of(key);
  }
}

class BsModalServiceStub {}

@Pipe({
  name: 'translate',
})
class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Component({
  selector: 'ff-page-header',
  template: '<p>Mock Product Editor Component</p>',
})
class MockHeaderComponent {
  @Input() pageTitle: any;
  @Input() breadcrumbTitle: any;
  @Input() breadcrumbLink: any;
}

class ModalServiceMock {
  public content: any = {
    confirm: of('data to modal'),
    errorMessage: '',
  };

  public get errorMessage() {
    return this.content.errorMessage;
  }

  public show() {
    return {
      content: this.content,
      hide() {},
    };
  }

  public hide() {
    return true;
  }

  public close() {
    return true;
  }
}
let component: SatisfiedRulesListComponent;
let fixture: ComponentFixture<SatisfiedRulesListComponent>;
const spySatisfiedRuleService: jasmine.SpyObj<SatisfiedRulesService> = jasmine.createSpyObj('SatisfiedRulesService', [
  'getRuleService',
  'find',
]);
describe('SatisfiedRulesListComponent', () => {
  beforeAll(() => {
    spySatisfiedRuleService.find.and.returnValue(
      of({
        data: {
          content: [
            {
              effective_date: '2021-08-26T22:00Z',
              expiration_date: '2021-08-26T22:00Z',
            },
          ],
        },
      }) as unknown as Observable<UpdateResponse<SatisfiedRulesResponse>>
    );
  });
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SatisfiedRulesListComponent, TranslatePipeMock],
      imports: [ReactiveFormsModule, FormsModule, RouterTestingModule],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceStub },
        { provide: BsModalService, useClass: ModalServiceMock },
        { provide: SatisfiedRulesService, useValue: spySatisfiedRuleService },
      ],
    }).compileComponents();

    // modalMockInstance = TestBed.get(BsModalService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SatisfiedRulesListComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });
  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should create rows', () => {
    spySatisfiedRuleService.find.and.returnValue(of({ status: 200, data: {} as unknown as SatisfiedRulesResponse }));
    const row = {} as unknown as ProductRuleView;
    component.products = [[row, row], {} as unknown as SatisfiedRuleSearchForm];
    expect(component.items.length).toEqual(2);
  });
});
