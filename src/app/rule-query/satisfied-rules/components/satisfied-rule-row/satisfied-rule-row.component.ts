import { ChangeDetectionStrategy, Component, ElementRef, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AdminGroupTypeMaster } from 'src/app/configuration/models/configuration.model';
import { FF_ALL_ADMIN_GROUP_TYPE } from 'src/app/configuration/tokens/admin-group-type.token';
import { appearOppacity } from 'src/app/modules/accordion-list/animations/appear-oppacity';
import { RulesType } from 'src/app/modules/conditions-basic-and-advance/models/conditions-basic-and-advance.model';
import { checkDateActive } from 'src/app/shared/utils/utils';
import { SatisfiedRules } from '../../models/satisfied-rules.model';
import { SatisfiedRulesUtilsService } from '../../services/satisfied-rules-utils.service';

@Component({
  selector: 'ff-satisfied-rule-row',
  templateUrl: './satisfied-rule-row.component.html',
  styleUrls: ['./satisfied-rule-row.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [appearOppacity()],
})
export class SatisfiedRuleRowComponent implements OnInit {
  @ViewChild('conditionsModalTemplate') conditionModalElement!: ElementRef;
  @Input() satisfiedRules!: SatisfiedRules[];
  item!: any;
  ruleType!: string;
  loadingModal = false;
  modalRef!: BsModalRef;
  selectedRule!: { rule: string };
  dateFormat = 'dd-MM-yyyy HH:mm';
  constructor(
    private router: Router,
    private modalService: BsModalService,
    private satisfiedRulesUtilsService: SatisfiedRulesUtilsService,
    @Inject(FF_ALL_ADMIN_GROUP_TYPE) public adminGroupsType: { [key in AdminGroupTypeMaster]: string }
  ) {}

  ngOnInit(): void {}

  checkDate(rule: SatisfiedRules): boolean {
    return checkDateActive(rule.effective_date, rule.expiration_date, rule.is_active);
  }

  go(rule: SatisfiedRules) {
    const { rule_type, rule_id } = rule;
    const urlBase = this.satisfiedRulesUtilsService.ruleRoute(rule);
    // excepcion para content group
    if (rule_type === this.adminGroupsType.contentGroup) {
      this.router.navigate([`${urlBase}/${rule.folder}/${rule_id}`], {
        state: { goBack: true, backMessage: 'RULE_QUERY.SATISFIED_RULES.BACKMESSAGE' },
      });
    } else {
      this.router.navigate([`${urlBase}/${rule_id}`], { state: { goBack: true, backMessage: 'RULE_QUERY.SATISFIED_RULES.BACKMESSAGE' } });
    }
  }

  showDetailModal(rule: SatisfiedRules) {
    this.ruleType = rule.rule_type;
    this.loadingModal = true;
    this.modalRef = this.modalService.show(this.conditionModalElement as any, { class: 'modal-lg' });
    this.selectedRule = { rule: rule.rule_name };
    this.satisfiedRulesUtilsService.getRuleService(rule.rule_type, rule.rule_id).subscribe(
      (data) => {
        if (data) {
          this.item = data;
          this.loadingModal = false;
        }
      },
      () => (this.loadingModal = false)
    );
  }

  public parseRuleType(ruleType: string) {
    return this.satisfiedRulesUtilsService.parseRuleType(ruleType);
  }

  public get ruleTypeFormat(): RulesType {
    return this.satisfiedRulesUtilsService.ruleTypeFormat(this.ruleType);
  }

  closeModal() {
    this.modalRef.hide();
  }
}
