import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of } from 'rxjs';
import { SatisfiedRules } from '../../models/satisfied-rules.model';
import { SatisfiedRulesUtilsService } from '../../services/satisfied-rules-utils.service';
import { SatisfiedRuleRowComponent } from './satisfied-rule-row.component';

describe('SatisfiedRuleRowComponent', () => {
  let component: SatisfiedRuleRowComponent;
  let fixture: ComponentFixture<SatisfiedRuleRowComponent>;
  let router: Router;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule, TranslateModule.forRoot()],
      declarations: [SatisfiedRuleRowComponent],
      providers: [
        {
          provide: BsModalService,
          useValue: {
            show() {},
          },
        },
        {
          provide: SatisfiedRulesUtilsService,
          useValue: {
            getRuleService() {
              return of({});
            },
            ruleRoute() {
              return '/stock/limit-sale/view';
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SatisfiedRuleRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show modal', () => {
    component.showDetailModal({} as unknown as SatisfiedRules);
    expect(component.loadingModal).toBeFalsy();
  });

  it('should navigate to  rule', fakeAsync(() => {
    const navigateSpy = spyOn(router, 'navigate');
    component.go({ rule_type: 'limit_sale', rule_id: '1' } as unknown as SatisfiedRules);
    expect(navigateSpy).toHaveBeenCalledWith(['/stock/limit-sale/view/1'], {
      state: { goBack: true, backMessage: 'RULE_QUERY.SATISFIED_RULES.BACKMESSAGE' },
    });
  }));
});
