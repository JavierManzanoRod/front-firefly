import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SatisfiedRulesDetailContainerComponent } from './containers/satisfied-rules-detail-container';
import { SatisfiedRulesSearchContainerComponent } from './containers/satisfied-rules-search-container';

const routes: Routes = [
  {
    path: '',
    component: SatisfiedRulesSearchContainerComponent,
    data: {
      header_title: 'RULE_QUERY.HEADER_TITLE',
      breadcrumb: [
        {
          label: 'DEPARTMENTS.LIST_TITLE',
          url: '',
        },
      ],
    },
    children: [
      {
        path: 'result',
        component: SatisfiedRulesDetailContainerComponent,
        data: {
          header_title: 'RULE_QUERY.HEADER_TITLE',
          breadcrumb: [
            {
              label: 'DEPARTMENTS.LIST_TITLE',
              url: '',
            },
          ],
        },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SatisfiedRulesRoutingModule {}
