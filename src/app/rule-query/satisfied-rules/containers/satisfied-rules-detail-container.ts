import { Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ToastService } from 'src/app/modules/toast/toast.service';
import { notifyRequestStatus } from '../../../shared/utils/operators/notify-operator';
import { ProductRuleView } from '../../models/product.model';
import { queryParamSnapshotFactory } from '../../providers/activated-route.factories';
import { PRODUCT_SEARCH_PROVIDER, PRODUCT_SEARCH_TOKEN } from '../../providers/product-search.provider';
import { SATISFIED_RULES_FILTER } from '../../providers/search-filters.tokens';
import { SatisfiedRuleSearchForm } from '../models/satisfied-rules.model';

@Component({
  selector: 'ff-satisfied-rule-detail-container',
  template: ` <div *ngIf="products$ | async as products; else loading" class="detail">
      <div>
        <ff-product-data *ngIf="products?.length" [product]="getMainProduct(products)"></ff-product-data>
      </div>
      <ff-satisfied-rules-list [products]="[products, form]"></ff-satisfied-rules-list>
    </div>
    <ng-template #loading>
      <div class="text-center py-5">
        <div class="loader"></div>
      </div>
    </ng-template>`,
  styleUrls: ['./satisfied-rules-detail-container.scss'],
  providers: [
    PRODUCT_SEARCH_PROVIDER,
    {
      provide: SATISFIED_RULES_FILTER,
      useFactory: queryParamSnapshotFactory<SatisfiedRuleSearchForm>(new SatisfiedRuleSearchForm()),
      deps: [ActivatedRoute],
    },
  ],
})
export class SatisfiedRulesDetailContainerComponent {
  constructor(
    private router: Router,
    private toastService: ToastService,
    @Inject(PRODUCT_SEARCH_TOKEN) public products$: Observable<ProductRuleView[]>,
    @Inject(SATISFIED_RULES_FILTER) public form: SatisfiedRuleSearchForm
  ) {
    this.products$ = this.products$.pipe<ProductRuleView[]>(notifyRequestStatus(this.toastService, [] as ProductRuleView[]));
  }

  getMainProduct(products: ProductRuleView[]) {
    return products.find((p) => Object.keys(p.attributes).length > 0) || products[0];
  }

  cancel() {
    this.router.navigate(['/rule-query/satisfied-rules']);
  }
}
