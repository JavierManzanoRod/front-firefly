import { Component, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { queryParamFactory } from '../../providers/activated-route.factories';
import { PRODUCT_SEARCH_PROVIDER } from '../../providers/product-search.provider';
import { SATISFIED_RULES_FILTER } from '../../providers/search-filters.tokens';
import { SatisfiedRulesSearchComponent } from '../components/satisfied-rules-search/satisfied-rules-search.component';
import { SatisfiedRuleSearchForm } from '../models/satisfied-rules.model';

@Component({
  selector: 'ff-satisfied-rules-search-container',
  template: `<ff-page-header [pageTitle]="'RULE_QUERY.SATISFIED_RULES.HEADER_TITLE' | translate"></ff-page-header>
    <ngx-simplebar class="page-container page-container-padding">
      <ff-satisfied-rules-search (searchParams)="handleSearch($event)"></ff-satisfied-rules-search>
      <router-outlet></router-outlet>
    </ngx-simplebar> `,
  providers: [
    {
      provide: SATISFIED_RULES_FILTER,
      useFactory: queryParamFactory<SatisfiedRuleSearchForm>(new SatisfiedRuleSearchForm()),
      deps: [ActivatedRoute],
    },
    PRODUCT_SEARCH_PROVIDER,
  ],
})
export class SatisfiedRulesSearchContainerComponent implements OnDestroy {
  @ViewChild(SatisfiedRulesSearchComponent) search!: SatisfiedRulesSearchComponent;
  private destroy$ = new Subject();
  constructor(public router: Router, private route: ActivatedRoute) {}

  public handleSearch(form: SatisfiedRuleSearchForm) {
    this.router.navigate(['result'], {
      relativeTo: this.route,
      queryParams: form,
    });
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
