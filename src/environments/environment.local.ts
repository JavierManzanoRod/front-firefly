import { Environment } from './environment.model';
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/plugins/zone-error'; // Included with Angular CLI.import { Environment } from './environment.model';

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// El primer idioma sera el obligatorio a rellenar en los formularios de multi idioma
const LANGUAGES_TXT = 'es_ES,ca_ES,en_US,en_GB,pt_PT,de_DE,fr_FR';

export const environmentLocal: Environment = {
  production: false,
  WELCOME_MESSAGE: 'HELLO FROM LOCAL',
  ACTIVE_FUNCTIONALITIES: '*',
  // API_URL_BACKOFFICE: 'http://localhost:3000', // Mocks
  // API_URL_BACKOFFICE: 'https://wso2-apigw.uat.eci.geci:8243',
  API_URL_BACKOFFICE: 'https://api-manager.uat.elcorteingles.es',
  // PI_URL_BACKOFFICE: 'api', // AVE
  API_PATH_LIMIT_SALES_VERSION: 'v1/',
  API_PATH_SITES_VERSION: 'v2/',
  API_PATH_SUBSITES_VERSION: 'v2/',
  API_PATH_TECHNICAL_CHARACTERISTICS_VERSION: 'v2/',
  API_PATH_ATTRIBUTE_TRANSLATION_VERSION: 'v2/',
  API_PATH_FACETS_VERSION: 'v1/',
  API_PATH_ENTITY_TYPES_VERSION: 'v1/',
  API_PATH_ENTITIES_VERSION: 'v1/',
  API_PATH_ADMIN_GROUP_FOLDERS_VERSION: 'v1/',
  API_PATH_ADMIN_GROUPS_TYPES_VERSION: 'v1/',
  API_PATH_ADMIN_GROUPS_VERSION: 'v1/',
  API_PATH_ASSORMENT_PRODUCT_SEARCHERS_VERSION: 'v2/',
  API_PATH_ATTRIBUTE_PRODUCT_SEARCH: 'v2/',
  API_PATH_ATTRIBUTE_TREE_NODE_VERSION: 'v1/',
  API_PATH_AUDIT_CATALOG_DASHBOARD_VERSION: 'v1/',
  API_PATH_CATEGORY_RULES_VERSION: 'v1/',
  API_PATH_CONTENT_GROUPS_VERSION: 'v1/',
  API_PATH_EVALUATE_RULES_VERSION: 'v1/',
  API_PATH_EXCLUDE_SEARCHES_VERSION: 'v1/',
  API_PATH_EXPLODEDE_TREE_VERSION: 'v1/',
  API_PATH_FACET_ATTRIBUTES_VERSION: 'v1/',
  API_PATH_FACET_LINKS_VERSION: 'v1/',
  API_PATH_GEOZONES_SCOPES_VERSION: 'v1/',
  API_PATH_GEOZONES_TARGETS_VERSION: 'v1/',
  API_PATH_MARKETPLACE_PRODUCT_RELATIONS_VERSION: 'v1/',
  API_PATH_EXPERT_VERSION: 'v1/',
  API_PATH_MARKETPLACE_PRODUCT_TYPES_VERSION: 'v1/',
  API_PATH_MARKETPLACE_SELLERS_VERSION: 'v1/',
  API_PATH_PRODUCT_TYPES_VERSION: 'v1/',
  API_PATH_PRODUCTS_MANAGER_SCHEMA_VERSION: 'v1/',
  API_PATH_PROMOTIONS_VERSION: 'v1/',
  API_PATH_LOGGER_VERSION: 'v1/',
  LANGUAGES: LANGUAGES_TXT,
  ID_COUNTRIES: 'f87a6ec9-739c-4c67-a54f-e5349e2d652f',
  ID_CITIES: '439bf23f-0fc5-46a7-8bd3-a7cd46befb94',
  OAUTH_CLIENT_ID: 'BY3YQfbfZa_Q7wmBfGFN3VXYhmca', // Client id default
  OAUTH_CLIENT_SECRET: 'ywI48cCZZLXuIe6UUesTldpN99Qa', // Client secret default
  OAUTH_BASE_URL: 'https://identity-services.uat.elcorteingles.es/',
  OAUTH_TOKEN_URL: 'https://identity-services.uat.elcorteingles.es/oauth2/token',
  OAUTH_CALLBACK: 'http://localhost:4444',
  OAUTH_ISSUER_URL: 'https://identity-services.uat.elcorteingles.es/oauth2/token',
  OAUTH_LOGIN_URL: 'https://identity-services.uat.elcorteingles.es/oauth2/authorize',
  OAUTH_LOGOUT_URL: 'https://identity-services.uat.elcorteingles.es/oidc/logout',
  OAUTH_SESSION_CHECK_URL: 'https://identity-services.uat.elcorteingles.es/oidc/checksession',
  OAUTH_POST_LOGOUT_URL: '',
  OAUTH_IS_ACTIVE_SILENT_REFRESH: '',
  OAUTH_USER_INFO_URL: 'https://identity-services.uat.elcorteingles.es/oauth2/userinfo',
  OAUTH_RESPONSE_TYPE: 'code',
  OAUTH_SILENT_REFRESH_URL: 'http://localhost:4444/assets/silent-refresh.html',
  OAUTH_SCOPE: 'openid profile email voucher admin-groups_admin products_global_admin',
  ATTRIBUTE_ROUTE_BARRA: '1acae7ff-49ec-4bf7-a770-8e0674017462',
  ATTRIBUTE_ROUTE_BRAND_IDENTIFIER:
    'c37662e9-6a4d-45ee-84b3-58b28f3a2bf0/bdb531a9-ef07-4c5b-8ddb-a046ccfafd0d/2943aa63-b7ca-4324-a744-7860d131be88',
  ATTRIBUTE_ROUTE_DEPARTMENT: '1f5f5cfa-e64f-48ce-b5d2-c1e189718f49',
  ATTRIBUTE_ROUTE_FAMILY: '361e5946-14b2-406e-a972-10cb26f04ec5',
  ATTRIBUTE_ROUTE_GOOD_TYPES: '1e9c8896-acdc-11eb-8529-0242ac130003',
  ATTRIBUTE_ROUTE_MANAGEMENT_TYPE:
    'c37662e9-6a4d-45ee-84b3-58b28f3a2bf0/5a027fc2-b2ee-11eb-8529-0242ac130003/0c7e94ce-acc8-11eb-8529-0242ac130003',
  ATTRIBUTE_ROUTE_SALEREFERENCE: '9777b49c-096e-4aec-9826-5de56a465027',
  ATTRIBUTE_ROUTE_SIZECODE: '5e70053d-96e0-4a7f-a750-210b611aadbd',
  ATTRIBUTE_ROUTE_PRODUCT_ID:
    'c37662e9-6a4d-45ee-84b3-58b28f3a2bf0/bdb531a9-ef07-4c5b-8ddb-a046ccfafd0d/0ec48092-5185-47f2-a1db-3feba03e6b2d',
  ATTRIBUTE_ROUTE_CATEGORIES:
    'c37662e9-6a4d-45ee-84b3-58b28f3a2bf0/bdb531a9-ef07-4c5b-8ddb-a046ccfafd0d/bc80c24b-f50e-4c19-9bce-219f9e3e9bf2',
  ATTRIBUTE_ROUTE_PROVIDER_IDENTIFIER:
    'c37662e9-6a4d-45ee-84b3-58b28f3a2bf0/bdb531a9-ef07-4c5b-8ddb-a046ccfafd0d/2ca60578-ace8-11eb-8529-0242ac130003',
  ATTRIBUTE_ROUTE_CUSTOMISED_REQUEST: 'c37662e9-6a4d-45ee-84b3-58b28f3a2bf0/2d7ab9ce-b2ee-11eb-8529-0242ac130003',
  ATTRIBUTE_ROUTE_CLASSIFICATION1_6:
    'c37662e9-6a4d-45ee-84b3-58b28f3a2bf0/5a027fc2-b2ee-11eb-8529-0242ac130003/09c3be26-acc8-11eb-8529-0242ac130003',
  ATTRIBUTE_ROUTE_CLASSIFICATION1_6_IDENTIFIER: '64a3d6da-acac-11eb-8529-0242ac130003',
  ATTRIBUTE_ROUTE_CLASSIFICATION1_6_VALUE: '6af347dc-acac-11eb-8529-0242ac130003',
  ATTRIBUTE_ROUTE_PRICE: '4cc07d76-ab84-401e-ac68-f41ebdacf490',
  ATTRIBUTE_ROUTE_GTIN: 'c37662e9-6a4d-45ee-84b3-58b28f3a2bf0/4dec47cb-a034-4946-8006-b0f857f98379',
  ATTRIBUTE_ROUTE_DISCOUNT: '9ef049a9-82b7-488c-b576-4b14693ec294',
  ATTRIBUTE_ROUTE_CENTERS: '507d31e0-963e-4d06-9c41-a72f6be7d658',
  ATTRIBUTE_ROUTE_SITES: 'a4b48759-2675-4b8f-8762-5a87d40757fb',
  ADMIN_GROUP_CONTENT_GROUP: 'content_group',
  ADMIN_GROUP_LIMIT_SALE: 'limit_sale',

  ADMIN_GROUP_PRICE_INHERITANCE: 'price_inheritance',
  ADMIN_GROUP_EXCLUDE_SEARCH: 'hidden',
  ENTITY_TYPE_ID_BRAND: '81b640b8-f620-4533-b2c7-a13bd94e87e3',
  ENTITY_TYPE_ID_BUNDLE_OF: '9c3c52cb-7d55-443c-9c80-8bfa3fc3d7f0',
  ENTITY_TYPE_ID_OFFER: '2a27030a-573a-4a4a-b234-15edf2eee679',
  ENTITY_TYPE_ID_ITEM_OFFERED: 'c37662e9-6a4d-45ee-84b3-58b28f3a2bf0',
  ENTITY_TYPE_ID_CATEGORIES: 'd6af2dd4-6506-4357-ab7e-7debfa5b451a',
  ENTITY_TYPE_ID_GOOD_TYPE: '6610b5c6-acdc-11eb-8529-0242ac130003',
  ENTITY_TYPE_ID_SITES: '765ba9fb-5034-42f3-a4a3-026978f72753',
  ENTITY_TYPE_ID_CENTERS: 'd76fa2ef-50f3-4a0c-ac1f-3754d030d993',
  ENTITY_TYPE_ID_SIZE_GUIDE: 'size_guide_url',
  ENTITY_TYPE_ID_SPECIAL_PRODUTCS: 'related_services',
  ENTITY_TYPE_ID_FLUOR_GASES: 'eci_fluor_gases',
  ENTITY_TYPE_ID_PROVIDER: 'fac1ff24-acc6-11eb-8529-0242ac130003',
  ENTITY_TYPE_ID_EXPERT: 'experts',
  ENTITY_TYPE_ID_LOYALTY: 'loyalty',
  ENTITY_TYPE_ID_BADGE: 'badge',
  ENTITY_TYPE_ID_PRICE_SPECIFICATIONS: 'daa015a8-e3d6-493a-897a-125a67a9df30',
  LOG_LEVEL: 'ERROR',
};
