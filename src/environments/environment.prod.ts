import { Environment } from './environment.model';

const accumulatorCM: any = {};
console.log('<% for  @item in @config %>');
accumulatorCM['<%= @item["#property#"] %>'] = '<%= @item["_#property#"] %>';
console.log('<% end  %>');
accumulatorCM.production = true;
export const environment: Environment = accumulatorCM as unknown as Environment;

console.log('welcome', environment.WELCOME_MESSAGE);
