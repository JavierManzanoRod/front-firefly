// lib/app.ts
import express = require('express');
const bodyParser = require('body-parser');

const cors = require('cors');
// import excludeSearchController = require('./stock/exclude-search');
// Create a new express application instance
const app: express.Application = express();
app.use(cors());
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
// tslint:disable-next-line: only-arrow-functions
app.use(function (req, res, next) {
  setTimeout(next, 1000);
});

// tslint:disable-next-line: only-arrow-functions
app.get('/', function (req, res) {
  res.send('firefly-mocks responding');
});

// Change this value to use a different json
process.env.USECASE = '';
console.log('🚀 process.env.USECASE', process.env.USECASE);

import excludeSearchRoutes from './routes/stocks/exclude-search';
import limitSalesRoutes from './routes/stocks/limit-sales';
import entityTypeRoutes from './routes/rule-engine/entity-type';
import attributeRoutes from './routes/rule-engine/attribute';
import adminGroupTypeRoutes from './routes/rule-engine/admin-group-type';
import adminGroupRoutes from './routes/rule-engine/admin-group';
import priceInheritanceRoutes from './routes/rule-engine/price-inheritance';
import categoryRulesRoutes from './routes/category-rules';
import entitiesRoutes from './routes/rule-engine/entity';
import contentGroupRoutes from './routes/catalog/content-group';
import orderRoutes from './routes/orders';
import sitesRoutes from './routes/sites';
import subSitesRoutes from './routes/subsites';
import sellerRoutes from './routes/seller';
import productTypesRoutes from './routes/catalog/facet/product-type';
import techincalCharacteristicsRoutes from './routes/catalog/facet/technical-characteristics';
import attributeTreeNodeRoutes from './routes/catalog/facet/attribute-tree-node';
import entityTypesRoutes from './routes/catalog/facet/entity-type';
import facetRoutes from './routes/catalog/facets';
import facetLinkRoutes from './routes/catalog/facet-link';
import producSearcherRoutes from './routes/product-searcher';
import evalauteRulesRoutes from './routes/rule-query/evaluate-rules';
import satisfiedRulesRoutes from './routes/rule-query/satisfied-rules';
import scopesRoutes from './routes/delivery/scopes';
import targetRoutes from './routes/delivery/targets';
import promotionsRoutes from './routes/promotions/products';
import dashboardRoutes from './routes/dashboard';
import facetManagementRoutes from './routes/facet-management';
import marketplaceRoutes from './routes/catalog/facet/marketplace';
import facetSearcherRoutes from './routes/facet-searcher';
import crosselingQueryRoutes from './routes/crosselling/query';
import crosselingHistoricRoutes from './routes/crosselling/historic';
import attributesTranslationRoutes from './routes/catalog/facet/attribute-translations';
import spenterRoutes from './routes/control-panel/spenter';
import sizeGuideRoutes from './routes/control-panel/size-guide';
import specialProductRoutes from './routes/control-panel/special-product';
import auditRoutes from './routes/audits/audits';
import offerRoutes from './routes/offers';
import expertRoutes from './routes/control-panel/expert';
import badge from './routes/control-panel/badge';

const routes = [
  excludeSearchRoutes,
  limitSalesRoutes,
  entityTypeRoutes,
  attributeRoutes,
  adminGroupTypeRoutes,
  adminGroupRoutes,
  categoryRulesRoutes,
  entitiesRoutes,
  contentGroupRoutes,
  orderRoutes,
  sitesRoutes,
  priceInheritanceRoutes,
  subSitesRoutes,
  sellerRoutes,
  productTypesRoutes,
  techincalCharacteristicsRoutes,
  attributeTreeNodeRoutes,
  entityTypesRoutes,
  facetRoutes,
  facetLinkRoutes,
  producSearcherRoutes,
  evalauteRulesRoutes,
  satisfiedRulesRoutes,
  scopesRoutes,
  targetRoutes,
  promotionsRoutes,
  dashboardRoutes,
  facetManagementRoutes,
  marketplaceRoutes,
  facetSearcherRoutes,
  crosselingQueryRoutes,
  crosselingHistoricRoutes,
  attributesTranslationRoutes,
  spenterRoutes,
  sizeGuideRoutes,
  specialProductRoutes,
  auditRoutes,
  offerRoutes,
  expertRoutes,
  badge,
];

// Añadir todas las routas a la aplicación
routes.forEach((route) => app.use('/', route));

// Uncomment to print all routes registeered
// let route = [];
// let routes2 = [];
// app._router.stack.forEach(function (middleware) {
//   if (middleware.route) {
//     // routes registered directly on the app
//     routes2.push(middleware.route);
//   } else if (middleware.name === 'router') {
//     // router middleware
//     middleware.handle.stack.forEach(function (handler) {
//       route = handler.route;
//       route && routes2.push(route);
//     });
//   }
// });
//console.log('🚀 ~ file: app.ts ~ line 108 ~ routes2', routes2);

// tslint:disable-next-line: only-arrow-functions
app.listen(3000, function () {
  console.log('firefly-mocks responding');
});

export default app;
