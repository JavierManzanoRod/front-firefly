import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../utils/router';

class OfferController extends CrudApiDefaultHandler<any> {
  detail(req: Request, res: Response) {
    const data = this.getFileReader('detail')(null);
    return res.status(200).send(data[0]);
  }
}

export default createRouterCrud(`/products/backoffice-exploded-trees/:version/exploded-trees`, new OfferController('offers', 'list'));
