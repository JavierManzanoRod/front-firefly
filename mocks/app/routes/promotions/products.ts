import { Router } from 'express';
import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
const router = Router();

const controller = new CrudApiDefaultHandler('promotions/products', 'products');
const detail = new CrudApiDefaultHandler('promotions/products', 'products-detail');

router.get('/promotions/backoffice-promotions/:version/promotional-actions/:id/offers', controller.list.bind(controller));
router.get('/promotions/backoffice-promotions/:version/promotional-actions', detail.detail.bind(detail));

router.get('/promotions/backoffice-promotions/:version/promo/find', detail.detail.bind(detail));

export default router;
