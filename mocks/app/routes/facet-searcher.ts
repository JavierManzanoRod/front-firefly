import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../utils/router';

export default createRouterCrud('/facet-attribute', new CrudApiDefaultHandler('facet-searcher', ''));
