import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export default createRouterCrud(
  `/products/backoffice-geozones/:version/geo-zones/scopes`,
  new CrudApiDefaultHandler('delivery/scopes', 'scopes')
);
