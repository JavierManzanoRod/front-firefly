import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export default createRouterCrud(
  `/products/backoffice-geozones/:version/geo-zones/targets`,
  new CrudApiDefaultHandler('delivery/target', 'target')
);
