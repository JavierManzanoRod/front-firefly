import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

class SatisfiedRulesController extends CrudApiDefaultHandler<any> {
  list(req: Request, res: Response) {
    return res.status(200).send(this.data);
  }
  create(req: Request, res: Response) {
    return this.list(req, res);
  }
}
export default createRouterCrud(
  `/products/backoffice-rules/:version/satisfied-rules`,
  new SatisfiedRulesController('rule-query/satisfied-rules', 'satisfied-rules')
);
