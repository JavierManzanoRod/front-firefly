import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../utils/router';

export default createRouterCrud('/orders-business/orders/query', new CrudApiDefaultHandler('orders', 'orders'));
