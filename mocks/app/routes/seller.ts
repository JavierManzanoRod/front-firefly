import { SellerDTO } from '@app/marketplace/seller/models/seller.model';
import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';
import { wrapBussinessApiResponse } from '../../utils/readFile';
import { createRouterCrud } from '../../utils/router';

class SellerController extends CrudApiDefaultHandler<SellerDTO> {
  list(req: Request, res: Response) {
    return res.status(200).send(wrapBussinessApiResponse(this.data));
  }
}
export default createRouterCrud(`/products/marketplace-sellers/:version/`, new SellerController('seller', ''));
