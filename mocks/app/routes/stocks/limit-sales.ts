import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export class LimitSalesController extends CrudApiDefaultHandler<any> {
  attributes(req: Request, res: Response) {
    const data = this.getFileReader('attributes')(null);
    return res.status(200).send(data);
  }
}

const controller = new LimitSalesController('stock/limit-sales', 'attributes');
const router = createRouterCrud(`/products/backoffice-item-flags/:version/limit-sales`, controller);
router.get(`/products/backoffice-item-flags/:version/limit-sales/types/attributes`, controller.attributes.bind(controller));
export default router;
