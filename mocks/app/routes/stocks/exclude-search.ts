import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export class ExcludeSearchController extends CrudApiDefaultHandler<any> {
  attributes(req: Request, res: Response) {
    const data = this.getFileReader('attributes')(null);
    return res.status(200).send(data);
  }
}
const controller = new ExcludeSearchController('stock/exclude-search', 'exclude-search');
const router = createRouterCrud(`/products/backoffice-item-flags/:version/exclude-searches`, controller);
router.get(`/products/backoffice-item-flags/:version/exclude-searches/types/attributes`, controller.attributes.bind(controller));
export default router;
