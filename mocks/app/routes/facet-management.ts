import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../utils/router';

export default createRouterCrud(
  `/products/backoffice-facet-attributes/:version/facet-attributes`,
  new CrudApiDefaultHandler('facet-management')
);
