import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../utils/router';

export default createRouterCrud(`/products/backoffice-sites-subsites/:version/sites`, new CrudApiDefaultHandler('sites'));
