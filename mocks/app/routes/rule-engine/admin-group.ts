import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export default createRouterCrud(
  `/products/backoffice-admin-groups/:version/admin-groups`,
  new CrudApiDefaultHandler('rule-engine/admin-group', 'admin-group')
);
