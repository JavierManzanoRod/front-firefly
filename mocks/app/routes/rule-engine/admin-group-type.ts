import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export default createRouterCrud(
  `/products/backoffice-admin-groups/:version/types`,
  new CrudApiDefaultHandler('rule-engine/admin-group-type', 'admin-group-type')
);
