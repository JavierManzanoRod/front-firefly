import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

const controller = new CrudApiDefaultHandler('rule-engine/entity', 'entity');
const router = createRouterCrud(`/products/backoffice-entities/:version/entities`, controller);
router.get('/entities', controller.list.bind(controller));
router.get(`/products/backoffice-entities/:version/entities/type/:id`, controller.list.bind(controller));
export default router;
