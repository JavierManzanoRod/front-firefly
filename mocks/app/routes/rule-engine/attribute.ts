import { Router } from 'express';
import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';

const router = Router();
const controller = new CrudApiDefaultHandler('rule-engine/attribute', 'attribute');
router.get('/attributes/:version/attributes/:id', controller.detail.bind(controller));
export default router;
