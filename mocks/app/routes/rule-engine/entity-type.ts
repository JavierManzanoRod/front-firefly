import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

const controller = new CrudApiDefaultHandler('rule-engine/entity-type', 'entity-type');
const router = createRouterCrud(`/products/backoffice-entity-types/:version/entity-types`, controller);
router.get('/entities', controller.list.bind(controller));

export default router;
