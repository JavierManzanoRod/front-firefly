import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export class PriceInheritanceController extends CrudApiDefaultHandler<any> {
  attributes(req: Request, res: Response) {
    const data = this.getFileReader('attributes')(null);
    return res.status(200).send(data);
  }
}
const controller = new PriceInheritanceController('rule-engine/price-inheritance', 'price-inheritance');
const router = createRouterCrud(`/products/backoffice-admin-groups/:version/price-inheritance`, controller);
router.get(`/products/backoffice-admin-groups/:version/size-guide/types/attributes`, controller.attributes.bind(controller));
export default router;
