import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export default createRouterCrud(
  `/products/backoffice-admin-group-types/:version/types/size-guide`,
  new CrudApiDefaultHandler('control-panel/size-guide', 'size-guide')
);
