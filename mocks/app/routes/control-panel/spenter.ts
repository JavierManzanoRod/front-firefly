import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export default createRouterCrud('/spenter/:version/spenter', new CrudApiDefaultHandler('control-panel/spenter', 'spenter'));
