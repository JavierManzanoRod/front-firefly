import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export default createRouterCrud(
  `/products/backoffice-admin-group-types/:version/types/badge`,
  new CrudApiDefaultHandler('control-panel/badge', 'badge')
);
