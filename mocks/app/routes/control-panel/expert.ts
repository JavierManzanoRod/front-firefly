import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';
import { Request, Response, Router } from 'express';
import { wrapGenericApiResponse, wrapSingleElement } from '../../../utils/readFile';

// export default createRouterCrud(
//   `/products/backoffice-expert/:version/expert-groups`,
//   new CrudApiDefaultHandler('control-panel/expert', 'expert')
// );

const router = Router();

class ExpertController extends CrudApiDefaultHandler<any> {
  list(req: Request, res: Response) {
    return res.status(200).send(wrapGenericApiResponse(this.data));
  }

  detail(req: Request, res: Response) {
    const expert = this.data.find((exp) => exp.name === req.params.name);
    return res.status(200).send(expert);
  }

  findItem(element: any, id: string) {
    return element['name'] === id;
  }

  post(req: Request, res: Response) {
    const newExpert = { id: Math.round(Math.random() * 99999), name: req.body.name };
    this.data.push(newExpert);
    console.log(req);
    return res.status(200).send(newExpert);
  }

  delete(req: Request, res: Response) {
    const name = req.params.name;
    const index = this.data.findIndex((element) => this.findItem(element, name));
    const resp = this.data.splice(index, 1);
    return res.status(200).send(wrapSingleElement(resp));
  }
}
const controller = new ExpertController('control-panel/expert', 'expert');
router.get(`/products/backoffice-expert/:version/expert-groups`, controller.list.bind(controller));
router.get(`/products/backoffice-expert/:version/expert-groups/:name`, controller.detail.bind(controller));
router.post(`/products/backoffice-expert/:version/expert-groups`, controller.post.bind(controller));
router.delete(`/products/backoffice-expert/:version/expert-groups/:name`, controller.delete.bind(controller));

export default router;
