import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export default createRouterCrud(
  `/products/backoffice-admin-group-types/:version/types/special-product`,
  new CrudApiDefaultHandler('control-panel/special-product', 'special-product')
);
