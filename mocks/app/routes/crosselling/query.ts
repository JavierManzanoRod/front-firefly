import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { wrapBussinessApiResponse } from '../../../utils/readFile';
import { createRouterCrud } from '../../../utils/router';

class QueryController extends CrudApiDefaultHandler<any> {
  list(req: Request, res: Response) {
    return res.status(200).send(wrapBussinessApiResponse(this.data));
  }
}

const controller = new QueryController('crosselling/query', 'query');
const router = createRouterCrud(`/products/marketplace-product-relations/:version/`, controller);
router.post(`/products/marketplace-product-relations/:version/upload/:site/:type`, controller.list.bind(this));
export default router;
