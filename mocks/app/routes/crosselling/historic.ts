import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

class HistoricController extends CrudApiDefaultHandler<any> {
  file(req: Request, res: Response) {
    return res.status(200).attachment('Principal;EAN_1;EAN_2;EAN_3;EAN_4;EAN_5;EAN_6;EAN_7;EAN_8;EAN_9;EAN_10').send();
  }
}
const controller = new HistoricController('crosselling/historic', 'historic');
const router = createRouterCrud(`/products/marketplace-product-relations/:version/product_relation/history`, controller);
router.get('/product_relation/files/:id', controller.file.bind(this));
router.get(`/products/marketplace-product-relations/:version/product_relation/files/:fileName`, controller.file.bind(this));

export default router;
