import { Request, Response, Router } from 'express';
import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';

const router = Router();

class DashboardController extends CrudApiDefaultHandler<any> {
  list(req: Request, res: Response) {
    let data: any;
    const type = req.query.type;
    if (type === 'sale_reference') {
      const read = this.getFileReader('sale-reference')(null);
      data = { references: read };
    } else if (type === 'item_code') {
      const read = this.getFileReader('item-code')(null);
      data = { products: read };
    } else if (type === 'ean' || type === 'unique-code') {
      const read = this.getFileReader('ean')(null);
      data = { product_variants: read };
    }
    return res.status(200).send(data);
  }
}
const controller = new DashboardController('dashboard', 'sale-reference');

router.get(`/dashboard/:version/products/:id/offers`, controller.list.bind(controller));
router.get(`/dashboard/:version/products-variants/eans/:id/offers`, controller.list.bind(controller));
router.get(`/dashboard/:version/products-variants/unique-codes/:id/offers`, controller.list.bind(controller));
router.get(`/dashboard/:version/offers/:id`, controller.list.bind(controller));

export default router;
