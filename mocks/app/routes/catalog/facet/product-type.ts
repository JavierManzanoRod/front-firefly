import { ProductTypeResponseIn } from '@app/catalog/product-type/models/product-type.model';
import { CrudApiDefaultHandler } from '../../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../../utils/router';

class ProductTypeController extends CrudApiDefaultHandler<ProductTypeResponseIn> {
  public detailProductCPS = (req: Request, res: Response) => {
    return super.getFileReader('CPS')(null);
  };
}
const controller = new ProductTypeController('catalog/facets/product-type', 'product-type');
const router = createRouterCrud(`/products/backoffice-product-types/:version/product-types`, controller);
router.get(`/products/backoffice-product-types/:version/product-types/pqf3likkmcilns`, controller.detailProductCPS.bind(controller));
export default router;
