import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../../utils/router';

class TechnicalController extends CrudApiDefaultHandler<any> {
  list(req: Request, res: Response) {
    return super.detail(req, res);
  }
}
export default createRouterCrud(
  `/products/templates/:version/products/templates`,
  new TechnicalController('catalog/facets/technical-characteristics', 'technical-characteristics')
);
