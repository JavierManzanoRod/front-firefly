import { CrudApiDefaultHandler } from '../../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../../utils/router';

class AttributeTreeHandler extends CrudApiDefaultHandler<any> {
  detail(req: Request, res: Response) {
    return this.list(req, res);
  }
}

export default createRouterCrud(
  `/products/backoffice-attribute-tree-nodes/:version/tree-nodes-types`,
  new AttributeTreeHandler('catalog/facets/attribute-tree-node', 'attribute-tree-node')
);
