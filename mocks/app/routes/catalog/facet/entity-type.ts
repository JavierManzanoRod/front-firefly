import { CrudApiDefaultHandler } from '../../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../../utils/router';

export default createRouterCrud(
  `/products/backoffice-entity-types/:version/entity-types`,
  new CrudApiDefaultHandler('catalog/facets/entity-type', 'entity-type')
);
