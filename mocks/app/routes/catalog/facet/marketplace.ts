import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../../utils/CrudApiDefaultHandler';
import { wrapBussinessApiResponse } from '../../../../utils/readFile';
import { createRouterCrud } from '../../../../utils/router';

class MarketplaceController extends CrudApiDefaultHandler<any> {
  list(req: Request, res: Response) {
    return res.status(200).send(wrapBussinessApiResponse(this.data));
  }
  empty(req: Request, res: Response) {
    return res.status(200).send([]);
  }
}
const controller = new MarketplaceController('catalog/facets/marketplace', 'marketplace');
const router = createRouterCrud(`/products/marketplace-product-types/:version/`, controller);
router.patch(`/products/marketplace-product-types/:version/:id`, controller.empty.bind(this));
export default router;
