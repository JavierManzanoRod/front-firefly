import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../../utils/router';

class AttributeTranslationsController extends CrudApiDefaultHandler<any> {
  list(req: Request, res: Response) {
    return res.status(200).send([]);
  }
  update(req: Request, res: Response) {
    return res.status(200).send([]);
  }
}
const controller = new AttributeTranslationsController('catalog/facets/attribute-translations', 'attribute-translations');
export default createRouterCrud(`/products/backoffice-attribute-translates/:version/attributes-translations`, controller);
