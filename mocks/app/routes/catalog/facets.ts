import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export default createRouterCrud(`/products/backoffice-facets/:version/facets`, new CrudApiDefaultHandler('catalog/facets', 'facets'));
