import { Request, Response } from 'express';
import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

class ContentGroupController extends CrudApiDefaultHandler<any> {
  list(req: Request, res: Response) {
    return res.status(200).send(this.data);
  }
  detail(req: Request, res: Response) {
    const reader = this.getFileReader('detail-data')(null);
    return res.status(200).send(reader[0]);
  }
}
const controller = new ContentGroupController('catalog/content-group', 'content-group');
const router = createRouterCrud(`/products/backoffice-admin-group-folders/:version/admin-group-folders`, controller);
router.delete(`/products/backoffice-content-groups/:version/content-groups/:id`, controller.detail.bind(controller));

export default router;
