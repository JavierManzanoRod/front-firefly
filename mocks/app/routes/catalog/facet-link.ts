import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

export default createRouterCrud(
  `/products/backoffice-facet-links/:version/facet-links`,
  new CrudApiDefaultHandler('catalog/facets', 'facet-link')
);
