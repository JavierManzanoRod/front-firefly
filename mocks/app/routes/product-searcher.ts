import { ProductRuleFlatten } from '@app/rule-query/realtime-query/models/product.model.ts';
import { Request, Response, Router } from 'express';
import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';

const router = Router();

class ProductSearchController extends CrudApiDefaultHandler<ProductRuleFlatten> {
  list(req: Request, res: Response) {
    return res.status(200).send(this.data);
  }
}
const controller = new ProductSearchController('product-searcher', 'product-searcher');
router.get(`/products/backoffice-assortment/:version/product_searchers`, controller.list.bind(controller));
export default router;
