import { CrudApiDefaultHandler } from '../../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../../utils/router';

createRouterCrud('/products/audits/:version/admin-group', new CrudApiDefaultHandler('audits/admin-group', 'admin-group'));
createRouterCrud('/products/audits/:version/admin-group-type', new CrudApiDefaultHandler('audits/admin-group-type', 'admin-group-type'));
createRouterCrud('/products/audits/:version/entity-types', new CrudApiDefaultHandler('audits/entity-types', 'entity-types'));
createRouterCrud('/products/audits/:version/category-rules', new CrudApiDefaultHandler('audits/category-rules', 'category-rules'));
createRouterCrud('/products/audits/:version/limit-sales', new CrudApiDefaultHandler('audits/limit-sales', 'limit-sales'));
export default createRouterCrud(
  '/products/audits/:version/exclude-search',
  new CrudApiDefaultHandler('audits/exclude-search', 'exclude-search')
);
