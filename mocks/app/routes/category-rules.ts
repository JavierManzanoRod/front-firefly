import { CrudApiDefaultHandler } from '../../utils/CrudApiDefaultHandler';
import { createRouterCrud } from '../../utils/router';

const categoryController = new CrudApiDefaultHandler('rule-engine/category-rules', 'category-rules');

const router = createRouterCrud(`/products/backoffice-category-rules/:version/categories`, categoryController);
router.get('/category-rules/:version', categoryController.list.bind(categoryController));
export default router;
