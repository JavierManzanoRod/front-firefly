import { Request, Response } from 'express';
import { readFileSafely, wrapGenericApiResponse, wrapSingleElement, wrapUpdateError, wrapUpdateSuccess } from './readFile';

export class CrudApiDefaultHandler<T> {
  public _data: T[];
  private id = 12345;
  constructor(public url: string, public filename: string = '') {
    this._data = readFileSafely<T>(this.url, this.filename);
  }

  get data() {
    return this._data;
  }

  set data(newItems: T[]) {
    this._data = newItems;
  }

  list(req: Request, res: Response) {
    return res.status(200).send(wrapGenericApiResponse(this.data));
  }

  detail(req: Request, res: Response): T {
    const id = req.params.routeId;
    const found = this.data.find((element) => this.findItem(element, id));
    return res.status(200).send(found || wrapSingleElement(this.data));
  }

  create(req: Request, res: Response) {
    const body = req.body;
    body.id = this.id++;
    body.identifier = this.id++;
    this.data.push(body);
    return res.status(200).send(wrapUpdateSuccess(this.data[0]));
  }

  update(req: Request, res: Response) {
    const id = req.params.routeId;
    const body = req.body;
    const index = this.data.findIndex((element) => this.findItem(element, id));
    const newArray = [...this.data];
    newArray[index] = { ...newArray[index], ...body };
    this.data = newArray;
    return res.status(200).send(wrapUpdateSuccess(this.data[0]));
  }

  patch(req: Request, res: Response) {
    return this.update(req, res);
  }

  updateFail(req: Request, res: Response) {
    return res.status(200).send(wrapUpdateError(this.data[0]));
  }

  delete(req: Request, res: Response) {
    const id = req.params.routeId;
    const index = this.data.findIndex((element) => this.findItem(element, id));
    const resp = this.data.splice(index, 1);
    return res.status(200).send(wrapSingleElement(resp));
  }

  getFileReader(filename = '') {
    return (wrap) => readFileSafely<T>(this.url, filename || this.filename, wrap);
  }

  findItem(element: any, id: string) {
    return element['id'] === id || element['identifier'] === id;
  }
}
