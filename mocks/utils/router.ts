import { CrudApiDefaultHandler } from './CrudApiDefaultHandler';
import { Router } from 'express';
const expressRouter = Router();
export function createRouterCrud(route: string, controller: CrudApiDefaultHandler<any>, router = expressRouter): Router {
  const routeWithId = route.substr(route.length - 1) === '/' ? `${route}:routeId` : `${route}/:routeId`;
  router.get(route, controller.list.bind(controller));
  router.get(routeWithId, controller.detail.bind(controller));
  router.post(route, controller.create.bind(controller));
  router.put(routeWithId, controller.update.bind(controller));
  router.patch(routeWithId, controller.patch.bind(controller));
  router.delete(routeWithId, controller.delete.bind(controller));
  return router;
}
