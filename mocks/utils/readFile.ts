import { BusinessApiResponse, GenericApiResponse } from '@app/model/base-api.model';
import { UpdateResponse } from '@app/core/base/api.service';
export function readFileSafely<T>(endpoint: string, fileName?: string, wrapper?: ((T) => T) | undefined): T[];
export function readFileSafely<T, W>(endpoint: string, fileName?: string, wrapper?: ((T) => W) | undefined): W;
export function readFileSafely<T, W = void>(endpoint: string, fileName = '', wrapper = undefined): T[] | W {
  const fileNameDef = fileName || endpoint;
  let file: T[] | GenericApiResponse<T> | unknown = null;
  const useCase = process.env.USECASE;
  const url = createUrl(endpoint, fileNameDef, useCase);
  try {
    file = require(url);
    console.error(`🟢🟢🟢 Loading file ${url}`);
  } catch (e) {
    console.error(`🚩🚩🚩 Couldn't use file ${url}, using default instead`);
    file = require(createUrl(endpoint, fileNameDef, ''));
  }
  const parsedFile = Array.isArray(file) ? file : isGenericApiResponse(file) ? file.content : [file as T];
  return wrapper ? (wrapper(parsedFile) as W) : parsedFile;
}
const defaultSrc = '../repository';
export function createUrl(folder: string, fileName: string, useCase: string) {
  return useCase ? `${defaultSrc}/${folder}/${useCase.trim()}/${fileName}.json` : `${defaultSrc}/${folder}/${fileName}.json`;
}

export function isGenericApiResponse(data: any): data is GenericApiResponse<any> {
  return 'content' in data;
}

export function wrapGenericApiResponse<T>(content: T[]): GenericApiResponse<T> {
  return {
    content,
    page: {
      page_size: 10,
      page_number: 0,
      total_pages: 1,
      total_elements: content.length,
    },
  };
}
export function wrapBussinessApiResponse<T>(business_object: T[]): BusinessApiResponse<T> {
  return {
    business_object,
    pagination_object: {
      page_size: 10,
      page_number: 0,
      total_pages: 1,
      total_elements: business_object.length,
    },
  };
}
export function wrapSingleElement<T>(content: T[]) {
  return content[0];
}

export function wrapSingleElementGenericApiResponse<T>(content: T[]) {
  return {
    content: wrapSingleElement(content),
    page: {
      page_size: 10,
      page_number: 0,
      total_pages: 1,
      total_elements: content.length,
    },
  };
}

export function wrapUpdateSuccess<T>(content: T): UpdateResponse<T> {
  return {
    status: 200,
    data: content,
  };
}

export function wrapUpdateError<T>(content: T): UpdateResponse<T> {
  return {
    status: 400,
    data: content,
    error: { error: 'Error updating' },
  };
}
