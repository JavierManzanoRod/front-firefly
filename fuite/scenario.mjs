// export async function setup(page) {
//   ///await page.waitForSelector('#username', { visible: true, timeout: 0 });
//   //   await page.waitForTimeout(1000);
//   //   await page.type('#username', 'X31041FL');
//   //   await page.type('#password', 'X31041FL');
//   //   await page.click('.login-btn>.btn');
//   console.log('Scenario Setup Start!');

//   await clickFirstVisible(page, 'Sitemap');
//   await page.waitForTimeout(1000);
//   console.log('Scenario Setup End!');
// }
export async function createTests(page) {
  // Code to run once on the page to determine which tests to run
  const reglasNegocio = [
    {
      description: 'Category Rule',
      data: {
        name: 'Agrupaciones de producto',
      },
    },
    {
      description: 'Badge',
      data: {
        name: 'Badge',
      },
    },
    {
      description: 'Expert admin',
      data: {
        name: 'Consulta de experto',
      },
    },
    {
      description: 'Content group',
      data: {
        name: 'Content Group',
      },
    },
    {
      description: 'Loyalty',
      data: {
        name: 'Fidelización',
      },
    },
    {
      description: 'Fluorinated gas',
      data: {
        name: 'Gases fluorados',
      },
    },
    {
      description: 'Size guide',
      data: {
        name: 'Guía de tallas',
      },
    },
    {
      description: 'Price inheritance',
      data: {
        name: 'Herencia de precio MKP',
      },
    },
    {
      description: 'Limit sales',
      data: {
        name: 'Limitar ventas',
      },
    },
    {
      description: 'Exclude search',
      data: {
        name: 'Ocultar en buscador',
      },
    },
    {
      description: 'Special product',
      data: {
        name: 'Productos especiales',
      },
    },
  ];

  const catalogo = [
    {
      description: 'Attribute translation',
      data: {
        name: 'Administración de atributos',
      },
    },
    {
      description: 'Facet management',
      data: {
        name: 'Consulta de Facets',
      },
    },
    {
      description: 'Facets',
      data: {
        name: 'Facetado',
      },
    },
    {
      description: 'Technical characteristics',
      data: {
        name: 'Ficha Técnica',
      },
    },
  ];
  const administradorMKP = [
    {
      description: 'Crossselling/Query',
      data: {
        name: 'Crosselling Consulta',
      },
    },
    {
      description: 'Crosselling/Management',
      data: {
        name: 'Crosselling Gestión',
      },
    },
    {
      description: 'Crosselling/Historic',
      data: {
        name: 'Crosselling Histórico carga',
      },
    },
    {
      description: 'Products MKP',
      data: {
        name: 'Tipos de producto Mkp',
      },
    },
    {
      description: 'Seller',
      data: {
        name: 'Vendedores',
      },
    },
  ];
  const consultas = [
    {
      description: 'Evaluate rule',
      data: {
        name: 'Aplicación de reglas',
      },
    },
    {
      description: 'Dashboard',
      data: {
        name: 'Estado del catálogo',
      },
    },
    {
      description: 'Promotions/actions',
      data: {
        name: 'Promociones por acciones',
      },
    },
    {
      description: 'Promotions/reference',
      data: {
        name: 'Promociones por referencia',
      },
    },
    {
      description: 'Satisifed rules',
      data: {
        name: 'Reglas por producto',
      },
    },
  ];
  const auditoria = [
    {
      description: 'Audits',
      data: {
        name: 'Auditoría',
      },
    },
  ];
  const administracionFirefly = [
    {
      description: 'Admin group type',
      data: {
        name: 'Admin-group-type',
      },
    },
    {
      description: 'Admin Group',
      data: {
        name: 'Admin-groups',
      },
    },
    {
      description: 'Entity types',
      data: {
        name: 'Entity-types',
      },
    },
    {
      description: 'Expert',
      data: {
        name: 'Expertos',
      },
    },
    {
      description: 'Searcher',
      data: {
        name: 'Importar esquema',
      },
    },
    {
      description: 'Sites',
      data: {
        name: 'Sites',
      },
    },
  ];
  const envios = [
    {
      description: 'Scopes',
      data: {
        name: 'Ámbitos',
      },
    },
    {
      description: 'Target',
      data: {
        name: 'Destinos',
      },
    },
  ];
  const sitemap = [
    {
      description: 'Sitemap',
      data: {
        name: 'Sitemap',
      },
    },
  ];
  return [...reglasNegocio]; // ...catalogo, ...administradorMKP, ...consultas, ...auditoria, ...administracionFirefly, ...envios, ...sitemap];
}

export async function iteration(page, { name }) {
  //  console.log('🚀 ~ file: scenario.mjs ~ line 17 ~ iteration ~ page', page?._target?.url());
  await clickFirstVisible(page, name);
  await page.waitForTimeout(900);
  await page.goBack();
  await page.waitForTimeout(900);
}

async function clickFirstVisible(page, selector) {
  const divs = await page.$x(`//div[contains(text(), '${selector}')]`);
  // console.log('🚀 ~ file: scenario.mjs ~ line 30 ~ clickFirstVisible ~ div', selector, divs.length);
  const div = divs[0];
  try {
    try {
      await await div.evaluate((b) => b.click());
    } catch (err) {
      console.error(err, `Element ${selector} is not clickable or is not an in-page SPA navigation`);
    }
  } finally {
    await div?.dispose();
  }
}
