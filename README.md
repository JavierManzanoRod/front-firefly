![](./readme/assets/firefly.png)

<h3 align="center">Front Firefly Backoffice</h3>

<p align="center">
 Interfaz web de control del backoffice de firefly,
</p>


## Contenidos

- [Quick start](#quick-start)
- [Vistazo breve](#vistazo-breve)
- [Documentation](#documentation)
- [Gestión del cambio](#gestión-del-cambio)
- [Versioning](#versioning)


## Quick start
Firefly backoffice es una herrmienta visual programada en angular.
Para poder desarrollar hay que:

- Clonar el repo
- Instalarlo con `npm install`

#test
## Vistazo breve

La aplicación está separada en bloques funcionales y operativas, como por ejemplo: el bloque funcional stock tiene la operativa limit-sales.

La operativa tendrá la siguiente url: `/stock/limit-sales`


```text
app/
└── stock/
    ├── limit-sale/
    │   ├── components
    │   ├── containers
    │   ├── models
    │   ├── services
    │   ├── limit-sale-routing.module.ts
    │   ├── limit-sale.module.ts
    └── otra funcionalidad/

```

## Documentation
La documentación del proyecto está en el confluence en la url:
<a href="https://confluence.azure.firefly.elcorteingles.es/display/FIR">https://confluence.azure.firefly.elcorteingles.es/display/FIR</a>


### Running documentation locally

Es posible generar documentación del proyecto. Ejecutar en la terminal en la raiz del  proyecto el comando: `npm run doc`

## Running with environments

Actualmente al ejecutar npm run start los servicios apuntan directamente al WSO2

Para apuntar al WSO2, abrir el archivo src/environments/environment.local.ts, descomentar
`API_URL_BACKOFFICE: 'https://gateway.wso2.dev.azure.firefly.elcorteingles.es/backoffice',
Y ejecutar el comando rpm run start

Para apuntar a la API, abrir el archivo src/environments/environment.local.ts, descomentar
`API_URL_BACKOFFICE: 'api',
Y usar el comando rpm run start:swagger

Para levantar con mocks, abrir el archivo src/environments/environment.local.ts, descomentar
`API_URL_BACKOFFICE: 'http://localhost:3000',
Y usar el comando rpm run start:mocks


Los diferentes endpoints estan definidos en el archivo proxy.config.json.

Todo cambio ha de subirse con los servicios apuntando al WSO2 para no generar conflictos.

## Gestión del cambio

### Git

Usamos git-flow. Las ramas las generamos desde el ticket de JIRA.

Es mandatorio que los commits han de tener el siguiente formato:

`(feature| bugfix | config | chore | config) (nº ticket entre paréntesis) : funcionalidad : descripción de cambio `

Ejemplo:

feature(19260):  limit-sales/exclude-search - se agrega skeleton en condiciones. Se configuran los servicios y el y proxy

### QA
La gestión del cambio usa jenkins y es necesario que el código esté linteado y pase tests unitarios.
No olvidar antes de commitear ejecutar `ng lint` y `ng test`

### PipeLine

Job de jenkings:
<a href="https://jenkins.azure.firefly.elcorteingles.es/service/jenkins/job/FFR-firefly-front-firefly-backoffice/">https://jenkins.azure.firefly.elcorteingles.es/service/jenkins/job/FFR-firefly-front-firefly-backoffice/</a>

#### Sonar
La url del proyecto en sonar es:
<a href="https://sonar.azure.firefly.elcorteingles.es/dashboard?id=firefly-front-firefly-backoffice">https://sonar.azure.firefly.elcorteingles.es/dashboard?id=firefly-front-firefly-backoffice</a>
## Versioning

Firefly front no está aún en producción!

De manera que estamos versionando de la siguiente manera: `0.numeroSprintScrum.x`

Por ejemplo: 0.42.0
