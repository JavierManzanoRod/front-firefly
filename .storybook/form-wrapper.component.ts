import { Component, ContentChildren, ElementRef, forwardRef, Inject, Input, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';

@Component({
  selector: 'ff-form-wrapper',
  template: ` <section style="display:flex;margin: 3em;gap:1rem">
    <form #f="ngForm" style=" width:400px">
      <ng-content></ng-content>
    </form>
    <article
      style=" display: block;
    width: 400px;
    background: white;
    padding: 1rem;"
    >
      <h1 style="margin:1rem">Datos del formulario</h1>
      <pre
        >{{ f.value | json }}
      </pre
      >
      <details style="margin:1rem 0">
        <summary>Errores</summary>
        <section *ngFor="let name of controlNames">
          <pre> {{ f?.controls?.[name]?.errors | json }}</pre>
        </section>
      </details>

      <button type="button" (click)="f.reset()">Reset</button>
    </article>
  </section>`,
})
export class FormWrapper<T> {
  @ContentChildren(NgModel) public models!: QueryList<NgModel>;
  @ViewChild(NgForm) public form!: NgForm;
  public controlNames: string[] = [];
  public ngAfterViewInit(): void {
    let ngContentModels = this.models.toArray();
    ngContentModels.forEach((model) => {
      this.controlNames.push(model.name);
      this.form.addControl(model);
    });
  }
}
