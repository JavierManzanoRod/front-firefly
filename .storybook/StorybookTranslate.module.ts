import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpLoaderFactoryWithPath } from '../src/app/app.module';

@NgModule({
  imports: [
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactoryWithPath,
        deps: [HttpClient],
      },
    }),
  ],
  exports: [TranslateModule],
})
export class StorybookTranslateModule {
  constructor(translateService: TranslateService) {
    console.log('Configuring the translation service: ', translateService);
    console.log('Translations: ', translateService.translations);
    translateService.setDefaultLang('es');
    translateService.use('es');
  }
}
