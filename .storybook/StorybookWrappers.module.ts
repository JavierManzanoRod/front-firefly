import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FormWrapper } from './form-wrapper.component';

@NgModule({
  declarations: [FormWrapper],
  imports: [CommonModule, FormsModule],
  exports: [FormWrapper],
})
export class StorybookWrappersModule {}

export const generateFormAttributes = (val: object): string => {
  return Object.entries(val)
    .map(([k, v]) => `[${k}]='${JSON.stringify(v)}'`)
    .concat(['ngModel', 'name="example"'])
    .join(' ');
};
